#ifndef TGBOT_TYPE_H
#define TGBOT_TYPE_H

#include <memory>
#include <string>

namespace tgbot
{
    /**
     * @class Type
     * @brief Generic JSON object.
     */
    class Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<Type>;

        //Constructors
        Type();

        Type(Type &);

        Type(Type &&) noexcept;

        Type &operator=(const Type &);

        Type &operator=(Type &&) noexcept;

        //Destructors
        virtual ~Type();

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        virtual std::string serialise() const;
    };
} //namespace tgbot

#endif
