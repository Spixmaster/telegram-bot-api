#ifndef TGBOT_TYPES_GAME_H
#define TGBOT_TYPES_GAME_H

#include <memory>
#include <optional>
#include <string>
#include <vector>

#include "tgbot/Type.h"
#include "tgbot/types/Animation.h"
#include "tgbot/types/MessageEntity.h"
#include "tgbot/types/PhotoSize.h"

namespace tgbot
{
    /**
     * @class Game
     * @brief This object represents a game.
     * @details Use BotFather to create and edit games, their short names will act as unique identifiers.
     */
    class Game : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<Game>;

    private:
        //Member variables
        /**
         * @var m_animation
         * @brief Optional. Animation that will be displayed in the game message in chats.
         * @details Upload via BotFather.
         */
        std::optional<Animation::ptr> m_animation;
        /**
         * @var m_description
         * @brief Description of the game.
         */
        std::string m_description;
        /**
         * @var m_photo
         * @brief Photo that will be displayed in the game message in chats.
         */
        std::vector<PhotoSize::ptr> m_photo;
        /**
         * @var m_text
         * @brief Optional. Brief description of the game or high scores included in the game message.
         * @details Can be automatically edited to include current high scores for the game when the bot calls setGameScore, or manually edited using editMessageText.
         * @details 0-4096 characters.
         */
        std::optional<std::string> m_text;
        /**
         * @var m_text_entities
         * @brief Optional. Special entities that appear in text, such as usernames, URLs, bot commands, etc.
         */
        std::optional<std::vector<MessageEntity::ptr>> m_text_entities;
        /**
         * @var m_title
         * @brief Title of the game.
         */
        std::string m_title;

    public:
        //Constructors
        Game();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit Game(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_animation
         */
        std::optional<Animation::ptr> get_animation() const;

        /**
         * @brief Getter.
         * @return m_description
         */
        std::string get_description() const;

        /**
         * @brief Getter.
         * @return m_photo
         */
        std::vector<PhotoSize::ptr> get_photo() const;

        /**
         * @brief Getter.
         * @return m_text
         */
        std::optional<std::string> get_text() const;

        /**
         * @brief Getter.
         * @return m_text_entities
         */
        std::optional<std::vector<MessageEntity::ptr>> get_text_entities() const;

        /**
         * @brief Getter.
         * @return m_title
         */
        std::string get_title() const;

        //Setter
        /**
         * @brief Setter for m_animation.
         * @param[in] animation See the member variable.
         */
        void set_animation(const std::optional<Animation::ptr> &animation);

        /**
         * @brief Setter for m_description.
         * @param[in] description See the member variable.
         */
        void set_description(const std::string &description);

        /**
         * @brief Setter for m_photo.
         * @param[in] photo See the member variable.
         */
        void set_photo(const std::vector<PhotoSize::ptr> &photo);

        /**
         * @brief Setter for m_text.
         * @param[in] text See the member variable.
         */
        void set_text(const std::optional<std::string> &text);

        /**
         * @brief Setter for m_text_entities.
         * @param[in] text_entities See the member variable.
         */
        void set_text_entities(const std::optional<std::vector<MessageEntity::ptr>> &text_entities);

        /**
         * @brief Setter for m_title.
         * @param[in] title See the member variable.
         */
        void set_title(const std::string &title);
    };
} //namespace tgbot

#endif
