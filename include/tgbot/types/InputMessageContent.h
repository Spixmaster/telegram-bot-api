#ifndef TGBOT_TYPES_INPUTMESSAGECONTENT_H
#define TGBOT_TYPES_INPUTMESSAGECONTENT_H

#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class InputMessageContent
     * @brief This object represents the content of a message to be sent as a result of an inline query.
     * @details Telegram clients currently support the following 5 types:
     * InputTextMessageContent
     * InputLocationMessageContent
     * InputVenueMessageContent
     * InputContactMessageContent
     * InputInvoiceMessageContent
     */
    class InputMessageContent : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<InputMessageContent>;

        //Constructors
        InputMessageContent();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit InputMessageContent(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;
    };
} //namespace tgbot

#endif
