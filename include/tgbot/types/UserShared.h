#ifndef TGBOT_TYPES_USERSHARED_H
#define TGBOT_TYPES_USERSHARED_H

#include <cstdint>
#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class UserShared
     * @brief This object contains information about the user whose identifier was shared with the bot using a KeyboardButtonRequestUser button.
     */
    class UserShared : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<UserShared>;

    private:
        //Member variables
        /**
         * @var m_request_id
         * @brief Identifier of the request.
         */
        std::int32_t m_request_id;
        /**
         * @var m_user_id
         * @brief Identifier of the shared user.
         * @details This number may have more than 32 significant bits and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a 64-bit integer or double-precision float type are safe for storing this identifier.
         * @details The bot may not have access to the user and could be unable to use this identifier, unless the user is already known to the bot by some other means.
         */
        std::int64_t m_user_id;

    public:
        //Constructors
        UserShared();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit UserShared(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_request_id
         */
        std::int32_t get_request_id() const;

        /**
         * @brief Getter.
         * @return m_user_id
         */
        std::int64_t get_user_id() const;

        //Setter
        /**
         * @brief Setter for m_request_id.
         * @param[in] request_id See the member variable.
         */
        void set_request_id(const std::int32_t &request_id);

        /**
         * @brief Setter for m_user_id.
         * @param[in] user_id See the member variable.
         */
        void set_user_id(const std::int64_t &user_id);
    };
} //namespace tgbot

#endif
