#ifndef TGBOT_TYPES_KEYBOARDBUTTON_H
#define TGBOT_TYPES_KEYBOARDBUTTON_H

#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"
#include "tgbot/types/KeyboardButtonPollType.h"
#include "tgbot/types/KeyboardButtonRequestChat.h"
#include "tgbot/types/KeyboardButtonRequestUser.h"
#include "tgbot/types/WebAppInfo.h"

namespace tgbot
{
    /**
     * @class KeyboardButton
     * @brief This object represents one button of the reply keyboard.
     * @details For simple text buttons, String can be used instead of this object to specify the button text.
     * @details The optional fields web_app, request_user, request_chat, request_contact, request_location, and request_poll are mutually exclusive.
     * @note request_contact and request_location options will only work in Telegram versions released after 9 April, 2016. Older clients will display unsupported message.
     * @note request_poll option will only work in Telegram versions released after 23 January, 2020. Older clients will display unsupported message.
     * @note web_app option will only work in Telegram versions released after 16 April, 2022. Older clients will display unsupported message.
     * @note request_user and request_chat options will only work in Telegram versions released after 3 February, 2023. Older clients will display unsupported message.
     */
    class KeyboardButton : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<KeyboardButton>;

    private:
        //Member variables
        /**
         * @var m_request_chat
         * @brief Optional. If specified, pressing the button will open a list of suitable chats.
         * @details Tapping on a chat will send its identifier to the bot in a “chat_shared” service message.
         * @details Available in private chats only.
         */
        std::optional<KeyboardButtonRequestChat::ptr> m_request_chat;
        /**
         * @var m_request_contact
         * @brief Optional. If True, the user's phone number will be sent as a contact when the button is pressed.
         * @details Available in private chats only.
         */
        std::optional<bool> m_request_contact;
        /**
         * @var m_request_location
         * @brief Optional. If True, the user's current location will be sent when the button is pressed.
         * @details Available in private chats only.
         */
        std::optional<bool> m_request_location;
        /**
         * @var m_request_poll
         * @brief Optional. If specified, the user will be asked to create a poll and send it to the bot when the button is pressed.
         * @details Available in private chats only.
         */
        std::optional<KeyboardButtonPollType::ptr> m_request_poll;
        /**
         * @var m_request_user
         * @brief Optional. If specified, pressing the button will open a list of suitable users.
         * @details Tapping on any user will send their identifier to the bot in a “user_shared” service message.
         * @details Available in private chats only.
         */
        std::optional<KeyboardButtonRequestUser::ptr> m_request_user;
        /**
         * @var m_text
         * @brief Text of the button.
         * @details If none of the optional fields are used, it will be sent as a message when the button is pressed.
         */
        std::string m_text;
        /**
         * @var m_web_app
         * @brief Optional. If specified, the described Web App will be launched when the button is pressed.
         * @details The Web App will be able to send a “web_app_data” service message.
         * @details Available in private chats only.
         */
        std::optional<WebAppInfo::ptr> m_web_app;

    public:
        //Constructors
        KeyboardButton();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit KeyboardButton(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_request_chat
         */
        std::optional<KeyboardButtonRequestChat::ptr> get_request_chat() const;

        /**
         * @brief Getter.
         * @return m_request_contact
         */
        std::optional<bool> get_request_contact() const;

        /**
         * @brief Getter.
         * @return m_request_location
         */
        std::optional<bool> get_request_location() const;

        /**
         * @brief Getter.
         * @return m_request_poll
         */
        std::optional<KeyboardButtonPollType::ptr> get_request_poll() const;

        /**
         * @brief Getter.
         * @return m_request_user
         */
        std::optional<KeyboardButtonRequestUser::ptr> get_request_user() const;

        /**
         * @brief Getter.
         * @return m_text
         */
        std::string get_text() const;

        /**
         * @brief Getter.
         * @return m_web_app
         */
        std::optional<WebAppInfo::ptr> get_web_app() const;

        //Setter
        /**
         * @brief Setter for m_request_chat.
         * @param[in] request_chat See the member variable.
         */
        void set_request_chat(const std::optional<KeyboardButtonRequestChat::ptr> &request_chat);

        /**
         * @brief Setter for m_request_contact.
         * @param[in] request_contact See the member variable.
         */
        void set_request_contact(const std::optional<bool> &request_contact);

        /**
         * @brief Setter for m_request_location.
         * @param[in] request_location See the member variable.
         */
        void set_request_location(const std::optional<bool> &request_location);

        /**
         * @brief Setter for m_request_poll.
         * @param[in] request_poll See the member variable.
         */
        void set_request_poll(const std::optional<KeyboardButtonPollType::ptr> &request_poll);

        /**
         * @brief Setter for m_request_user.
         * @param[in] request_user See the member variable.
         */
        void set_request_user(const std::optional<KeyboardButtonRequestUser::ptr> &request_user);

        /**
         * @brief Setter for m_text.
         * @param[in] text See the member variable.
         */
        void set_text(const std::string &text);

        /**
         * @brief Setter for m_web_app.
         * @param[in] web_app See the member variable.
         */
        void set_web_app(const std::optional<WebAppInfo::ptr> &web_app);
    };
} //namespace tgbot

#endif
