#ifndef TGBOT_TYPES_INLINEQUERYRESULTCONTACT_H
#define TGBOT_TYPES_INLINEQUERYRESULTCONTACT_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>

#include "InlineKeyboardMarkup.h"
#include "InlineQueryResult.h"
#include "InputMessageContent.h"

namespace tgbot
{
    /**
     * @class InlineQueryResultContact
     * @brief Represents a contact with a phone number.
     * @details By default, this contact will be sent by the user.
     * @details Alternatively, you can use input_message_content to send a message with the specified content instead of the contact.
     * @note This will only work in Telegram versions released after 9 April, 2016. Older clients will ignore them.
     */
    class InlineQueryResultContact : public InlineQueryResult
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<InlineQueryResultContact>;

    private:
        //Member variables
        /**
         * @var m_first_name
         * @brief Contact's first name.
         */
        std::string m_first_name;
        /**
         * @var m_id
         * @brief Unique identifier for this result, 1-64 Bytes.
         */
        std::string m_id;
        /**
         * @var m_input_message_content
         * @brief Optional. Content of the message to be sent instead of the contact.
         */
        std::optional<InputMessageContent::ptr> m_input_message_content;
        /**
         * @var m_last_name
         * @brief Optional. Contact's last name.
         */
        std::optional<std::string> m_last_name;
        /**
         * @var m_phone_number
         * @brief Contact's phone number.
         */
        std::string m_phone_number;
        /**
         * @var m_reply_markup
         * @brief Optional. Inline keyboard attached to the message.
         */
        std::optional<InlineKeyboardMarkup::ptr> m_reply_markup;
        /**
         * @var m_thumbnail_height
         * @brief Optional. Thumbnail height.
         */
        std::optional<std::int32_t> m_thumbnail_height;
        /**
         * @var m_thumbnail_url
         * @brief Optional. Url of the thumbnail for the result.
         */
        std::optional<std::string> m_thumbnail_url;
        /**
         * @var m_thumbnail_width
         * @brief Optional. Thumbnail width.
         */
        std::optional<std::int32_t> m_thumbnail_width;
        /**
         * @var m_type
         * @brief Type of the result, must be contact.
         */
        std::string m_type;
        /**
         * @var m_vcard
         * @brief Optional. Additional data about the contact in the form of a vCard, 0-2048 bytes.
         */
        std::optional<std::string> m_vcard;

    public:
        //Constructors
        InlineQueryResultContact();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit InlineQueryResultContact(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_first_name
         */
        std::string get_first_name() const;

        /**
         * @brief Getter.
         * @return m_id
         */
        std::string get_id() const;

        /**
         * @brief Getter.
         * @return m_input_message_content
         */
        std::optional<InputMessageContent::ptr> get_input_message_content() const;

        /**
         * @brief Getter.
         * @return m_last_name
         */
        std::optional<std::string> get_last_name() const;

        /**
         * @brief Getter.
         * @return m_phone_number
         */
        std::string get_phone_number() const;

        /**
         * @brief Getter.
         * @return m_reply_markup
         */
        std::optional<InlineKeyboardMarkup::ptr> get_reply_markup() const;

        /**
         * @brief Getter.
         * @return m_thumbnail_height
         */
        std::optional<std::int32_t> get_thumbnail_height() const;

        /**
         * @brief Getter.
         * @return m_thumbnail_url
         */
        std::optional<std::string> get_thumbnail_url() const;

        /**
         * @brief Getter.
         * @return m_thumbnail_width
         */
        std::optional<std::int32_t> get_thumbnail_width() const;

        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        /**
         * @brief Getter.
         * @return m_vcard
         */
        std::optional<std::string> get_vcard() const;

        //Setter
        /**
         * @brief Setter for m_first_name.
         * @param[in] first_name See the member variable.
         */
        void set_first_name(const std::string &first_name);

        /**
         * @brief Setter for m_id.
         * @param[in] id See the member variable.
         */
        void set_id(const std::string &id);

        /**
         * @brief Setter for m_input_message_content.
         * @param[in] input_message_content See the member variable.
         */
        void set_input_message_content(const std::optional<InputMessageContent::ptr> &input_message_content);

        /**
         * @brief Setter for m_last_name.
         * @param[in] last_name See the member variable.
         */
        void set_last_name(const std::optional<std::string> &last_name);

        /**
         * @brief Setter for m_phone_number.
         * @param[in] phone_number See the member variable.
         */
        void set_phone_number(const std::string &phone_number);

        /**
         * @brief Setter for m_reply_markup.
         * @param[in] reply_markup See the member variable.
         */
        void set_reply_markup(const std::optional<InlineKeyboardMarkup::ptr> &reply_markup);

        /**
         * @brief Setter for m_thumbnail_height.
         * @param[in] thumbnail_height See the member variable.
         */
        void set_thumbnail_height(const std::optional<std::int32_t> &thumbnail_height);

        /**
         * @brief Setter for m_thumbnail_url.
         * @param[in] thumbnail_url See the member variable.
         */
        void set_thumbnail_url(const std::optional<std::string> &thumbnail_url);

        /**
         * @brief Setter for m_thumbnail_width.
         * @param[in] thumbnail_width See the member variable.
         */
        void set_thumbnail_width(const std::optional<std::int32_t> &thumbnail_width);

        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);

        /**
         * @brief Setter for m_vcard.
         * @param[in] vcard See the member variable.
         */
        void set_vcard(const std::optional<std::string> &vcard);
    };
} //namespace tgbot

#endif
