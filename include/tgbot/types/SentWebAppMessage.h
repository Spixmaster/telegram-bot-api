#ifndef TGBOT_TYPES_SENTWEBAPPMESSAGE_H
#define TGBOT_TYPES_SENTWEBAPPMESSAGE_H

#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class SentWebAppMessage
     * @brief Describes an inline message sent by a Web App on behalf of a user.
     */
    class SentWebAppMessage : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<SentWebAppMessage>;

    private:
        //Member variables
        /**
         * @var m_inline_message_id
         * @brief Optional. Identifier of the sent inline message.
         * @details Available only if there is an inline keyboard attached to the message.
         */
        std::optional<std::string> m_inline_message_id;

    public:
        //Constructors
        SentWebAppMessage();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit SentWebAppMessage(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_inline_message_id
         */
        std::optional<std::string> get_inline_message_id() const;

        //Setter
        /**
         * @brief Setter for m_inline_message_id.
         * @param[in] inline_message_id See the member variable.
         */
        void set_inline_message_id(const std::optional<std::string> &inline_message_id);
    };
} //namespace tgbot

#endif
