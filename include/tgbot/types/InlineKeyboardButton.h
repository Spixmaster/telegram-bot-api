#ifndef TGBOT_TYPES_INLINEKEYBOARDBUTTON_H
#define TGBOT_TYPES_INLINEKEYBOARDBUTTON_H

#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"
#include "tgbot/types/CallbackGame.h"
#include "tgbot/types/LoginUrl.h"
#include "tgbot/types/WebAppInfo.h"

namespace tgbot
{
    /**
     * @class InlineKeyboardButton
     * @brief This object represents one button of an inline keyboard.
     * @details You must use exactly one of the optional fields.
     */
    class InlineKeyboardButton : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<InlineKeyboardButton>;

    private:
        //Member variables
        /**
         * @var m_callback_data
         * @brief Optional. Data to be sent in a callback query to the bot when button is pressed, 1-64 bytes.
         */
        std::optional<std::string> m_callback_data;
        /**
         * @var m_callback_game
         * @brief Optional. Description of the game that will be launched when the user presses the button.
         * @note This type of button must always be the first button in the first row.
         */
        std::optional<CallbackGame::ptr> m_callback_game;
        /**
         * @var m_login_url
         * @brief Optional. An HTTP URL used to automatically authorize the user.
         * @details Can be used as a replacement for the Telegram Login Widget.
         */
        std::optional<LoginUrl::ptr> m_login_url;
        /**
         * @var m_pay
         * @brief Optional. Specify True, to send a Pay button.
         * @note This type of button must always be the first button in the first row and can only be used in invoice messages.
         */
        std::optional<bool> m_pay;
        /**
         * @var m_switch_inline_query
         * @brief Optional. If set, pressing the button will prompt the user to select one of their chats, open that chat and insert the bot's username and the specified inline query in the input field.
         * @details May be empty, in which case just the bot's username will be inserted.
         * @note This offers an easy way for users to start using your bot in inline mode when they are currently in a private chat with it. Especially useful when combined with switch_pm… actions - in this case the user will be automatically returned to the chat they switched from, skipping the chat selection screen.
         */
        std::optional<std::string> m_switch_inline_query;
        /**
         * @var m_switch_inline_query_current_chat
         * @brief Optional. If set, pressing the button will insert the bot's username and the specified inline query in the current chat's input field.
         * @details May be empty, in which case only the bot's username will be inserted.
         * @details This offers a quick way for the user to open your bot in inline mode in the same chat - good for selecting something from multiple options.
         */
        std::optional<std::string> m_switch_inline_query_current_chat;
        /**
         * @var m_text
         * @brief Label text on the button.
         */
        std::string m_text;
        /**
         * @var m_url
         * @brief Optional. HTTP or tg:// URL to be opened when the button is pressed.
         * @details Links tg://user?id=\<user_id> can be used to mention a user by their ID without using a username, if this is allowed by their privacy settings.
         */
        std::optional<std::string> m_url;
        /**
         * @var m_web_app
         * @brief Optional. Description of the Web App that will be launched when the user presses the button.
         * @details The Web App will be able to send an arbitrary message on behalf of the user using the method answerWebAppQuery.
         * @details Available only in private chats between a user and the bot.
         */
        std::optional<WebAppInfo::ptr> m_web_app;

    public:
        //Constructors
        InlineKeyboardButton();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit InlineKeyboardButton(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_callback_data
         */
        std::optional<std::string> get_callback_data() const;

        /**
         * @brief Getter.
         * @return m_callback_game
         */
        std::optional<CallbackGame::ptr> get_callback_game() const;

        /**
         * @brief Getter.
         * @return m_login_url
         */
        std::optional<LoginUrl::ptr> get_login_url() const;

        /**
         * @brief Getter.
         * @return m_pay
         */
        std::optional<bool> get_pay() const;

        /**
         * @brief Getter.
         * @return m_switch_inline_query
         */
        std::optional<std::string> get_switch_inline_query() const;

        /**
         * @brief Getter.
         * @return m_switch_inline_query_current_chat
         */
        std::optional<std::string> get_switch_inline_query_current_chat() const;

        /**
         * @brief Getter.
         * @return m_text
         */
        std::string get_text() const;

        /**
         * @brief Getter.
         * @return m_url
         */
        std::optional<std::string> get_url() const;

        /**
         * @brief Getter.
         * @return m_web_app
         */
        std::optional<WebAppInfo::ptr> get_web_app() const;

        //Setter
        /**
         * @brief Setter for m_callback_data.
         * @param[in] callback_data See the member variable.
         */
        void set_callback_data(const std::optional<std::string> &callback_data);

        /**
         * @brief Setter for m_callback_game.
         * @param[in] callback_game See the member variable.
         */
        void set_callback_game(const std::optional<CallbackGame::ptr> &callback_game);

        /**
         * @brief Setter for m_login_url.
         * @param[in] login_url See the member variable.
         */
        void set_login_url(const std::optional<LoginUrl::ptr> &login_url);

        /**
         * @brief Setter for m_pay.
         * @param[in] pay See the member variable.
         */
        void set_pay(const std::optional<bool> &pay);

        /**
         * @brief Setter for m_switch_inline_query.
         * @param[in] switch_inline_query See the member variable.
         */
        void set_switch_inline_query(const std::optional<std::string> &switch_inline_query);

        /**
         * @brief Setter for m_switch_inline_query_current_chat.
         * @param[in] switch_inline_query_current_chat See the member variable.
         */
        void set_switch_inline_query_current_chat(const std::optional<std::string> &switch_inline_query_current_chat);

        /**
         * @brief Setter for m_text.
         * @param[in] text See the member variable.
         */
        void set_text(const std::string &text);

        /**
         * @brief Setter for m_url.
         * @param[in] url See the member variable.
         */
        void set_url(const std::optional<std::string> &url);

        /**
         * @brief Setter for m_web_app.
         * @param[in] web_app See the member variable.
         */
        void set_web_app(const std::optional<WebAppInfo::ptr> &web_app);
    };
} //namespace tgbot

#endif
