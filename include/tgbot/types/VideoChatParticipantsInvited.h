#ifndef TGBOT_TYPES_VIDEOCHATPARTICIPANTSINVITED_H
#define TGBOT_TYPES_VIDEOCHATPARTICIPANTSINVITED_H

#include <memory>
#include <string>
#include <vector>

#include "tgbot/Type.h"
#include "tgbot/types/User.h"

namespace tgbot
{
    /**
     * @class VideoChatParticipantsInvited
     * @brief This object represents a service message about new members invited to a video chat.
     */
    class VideoChatParticipantsInvited : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<VideoChatParticipantsInvited>;

    private:
        //Member variables
        /**
         * @var m_users
         * @brief New members that were invited to the video chat.
         */
        std::vector<User::ptr> m_users;

    public:
        //Constructors
        VideoChatParticipantsInvited();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit VideoChatParticipantsInvited(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_users
         */
        std::vector<User::ptr> get_users() const;

        //Setter
        /**
         * @brief Setter for m_users.
         * @param[in] users See the member variable.
         */
        void set_users(const std::vector<User::ptr> &users);
    };
} //namespace tgbot

#endif
