#ifndef TGBOT_TYPES_MESSAGEAUTODELETETIMERCHANGED_H
#define TGBOT_TYPES_MESSAGEAUTODELETETIMERCHANGED_H

#include <cstdint>
#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class MessageAutoDeleteTimerChanged
     * @brief This object represents a service message about a change in auto-delete timer settings.
     */
    class MessageAutoDeleteTimerChanged : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<MessageAutoDeleteTimerChanged>;

    private:
        //Member variables
        /**
         * @var m_message_auto_delete_time
         * @brief New auto-delete time for messages in the chat; in seconds.
         */
        std::int32_t m_message_auto_delete_time;

    public:
        //Constructors
        MessageAutoDeleteTimerChanged();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit MessageAutoDeleteTimerChanged(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_message_auto_delete_time
         */
        std::int32_t get_message_auto_delete_time() const;

        //Setter
        /**
         * @brief Setter for m_message_auto_delete_time.
         * @param[in] message_auto_delete_time See the member variable.
         */
        void set_message_auto_delete_time(const std::int32_t &message_auto_delete_time);
    };
} //namespace tgbot

#endif
