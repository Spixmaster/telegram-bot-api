#ifndef TGBOT_TYPES_VENUE_H
#define TGBOT_TYPES_VENUE_H

#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"
#include "tgbot/types/Location.h"

namespace tgbot
{
    /**
     * @class Venue
     * @brief This object represents a venue.
     */
    class Venue : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<Venue>;

    private:
        //Member variables
        /**
         * @var m_address
         * @brief Address of the venue.
         */
        std::string m_address;
        /**
         * @var m_foursquare_id
         * @brief Optional. Foursquare identifier of the venue.
         */
        std::optional<std::string> m_foursquare_id;
        /**
         * @var m_foursquare_type
         * @brief Optional. Foursquare type of the venue.
         * @details (For example, “arts_entertainment/default”, “arts_entertainment/aquarium” or “food/icecream”.)
         */
        std::optional<std::string> m_foursquare_type;
        /**
         * @var m_google_place_id
         * @brief Optional. Google Places identifier of the venue.
         */
        std::optional<std::string> m_google_place_id;
        /**
         * @var m_google_place_type
         * @brief Optional. Google Places type of the venue.
         * @details (See supported types.)
         */
        std::optional<std::string> m_google_place_type;
        /**
         * @var m_location
         * @brief Venue location.
         * @details Can't be a live location.
         */
        Location::ptr m_location;
        /**
         * @var m_title
         * @brief Name of the venue.
         */
        std::string m_title;

    public:
        //Constructors
        Venue();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit Venue(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_address
         */
        std::string get_address() const;

        /**
         * @brief Getter.
         * @return m_foursquare_id
         */
        std::optional<std::string> get_foursquare_id() const;

        /**
         * @brief Getter.
         * @return m_foursquare_type
         */
        std::optional<std::string> get_foursquare_type() const;

        /**
         * @brief Getter.
         * @return m_google_place_id
         */
        std::optional<std::string> get_google_place_id() const;

        /**
         * @brief Getter.
         * @return m_google_place_type
         */
        std::optional<std::string> get_google_place_type() const;

        /**
         * @brief Getter.
         * @return m_location
         */
        Location::ptr get_location() const;

        /**
         * @brief Getter.
         * @return m_title
         */
        std::string get_title() const;

        //Setter
        /**
         * @brief Setter for m_address.
         * @param[in] address See the member variable.
         */
        void set_address(const std::string &address);

        /**
         * @brief Setter for m_foursquare_id.
         * @param[in] foursquare_id See the member variable.
         */
        void set_foursquare_id(const std::optional<std::string> &foursquare_id);

        /**
         * @brief Setter for m_foursquare_type.
         * @param[in] foursquare_type See the member variable.
         */
        void set_foursquare_type(const std::optional<std::string> &foursquare_type);

        /**
         * @brief Setter for m_google_place_id.
         * @param[in] google_place_id See the member variable.
         */
        void set_google_place_id(const std::optional<std::string> &google_place_id);

        /**
         * @brief Setter for m_google_place_type.
         * @param[in] google_place_type See the member variable.
         */
        void set_google_place_type(const std::optional<std::string> &google_place_type);

        /**
         * @brief Setter for m_location.
         * @param[in] location See the member variable.
         */
        void set_location(const Location::ptr &location);

        /**
         * @brief Setter for m_title.
         * @param[in] title See the member variable.
         */
        void set_title(const std::string &title);
    };
} //namespace tgbot

#endif
