#ifndef TGBOT_TYPES_INLINEQUERYRESULTARTICLE_H
#define TGBOT_TYPES_INLINEQUERYRESULTARTICLE_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>

#include "tgbot/types/InlineKeyboardMarkup.h"
#include "tgbot/types/InlineQueryResult.h"
#include "tgbot/types/InputMessageContent.h"

namespace tgbot
{
    /**
     * @class InlineQueryResultArticle
     * @brief Represents a link to an article or web page.
     */
    class InlineQueryResultArticle : public InlineQueryResult
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<InlineQueryResultArticle>;

    private:
        //Member variables
        /**
         * @var m_description
         * @brief Optional. Short description of the result.
         */
        std::optional<std::string> m_description;
        /**
         * @var m_hide_url
         * @brief Optional. Pass True if you don't want the URL to be shown in the message.
         */
        std::optional<bool> m_hide_url;
        /**
         * @var m_id
         * @brief Unique identifier for this result, 1-64 Bytes.
         */
        std::string m_id;
        /**
         * @var m_input_message_content
         * @brief Content of the message to be sent.
         */
        InputMessageContent::ptr m_input_message_content;
        /**
         * @var m_reply_markup
         * @brief Optional. Inline keyboard attached to the message.
         */
        std::optional<InlineKeyboardMarkup::ptr> m_reply_markup;
        /**
         * @var m_thumbnail_height
         * @brief Optional. Thumbnail height.
         */
        std::optional<std::int32_t> m_thumbnail_height;
        /**
         * @var m_thumbnail_url
         * @brief Optional. Url of the thumbnail for the result.
         */
        std::optional<std::string> m_thumbnail_url;
        /**
         * @var m_thumbnail_width
         * @brief Optional. Thumbnail width.
         */
        std::optional<std::int32_t> m_thumbnail_width;
        /**
         * @var m_title
         * @brief Title of the result.
         */
        std::string m_title;
        /**
         * @var m_type
         * @brief Type of the result, must be article.
         */
        std::string m_type;
        /**
         * @var m_url
         * @brief Optional. URL of the result.
         */
        std::optional<std::string> m_url;

    public:
        //Constructors
        InlineQueryResultArticle();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit InlineQueryResultArticle(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_description
         */
        std::optional<std::string> get_description() const;

        /**
         * @brief Getter.
         * @return m_hide_url
         */
        std::optional<bool> get_hide_url() const;

        /**
         * @brief Getter.
         * @return m_id
         */
        std::string get_id() const;

        /**
         * @brief Getter.
         * @return m_input_message_content
         */
        InputMessageContent::ptr get_input_message_content() const;

        /**
         * @brief Getter.
         * @return m_reply_markup
         */
        std::optional<InlineKeyboardMarkup::ptr> get_reply_markup() const;

        /**
         * @brief Getter.
         * @return m_thumbnail_height
         */
        std::optional<std::int32_t> get_thumbnail_height() const;

        /**
         * @brief Getter.
         * @return m_thumbnail_url
         */
        std::optional<std::string> get_thumbnail_url() const;

        /**
         * @brief Getter.
         * @return m_thumbnail_width
         */
        std::optional<std::int32_t> get_thumbnail_width() const;

        /**
         * @brief Getter.
         * @return m_title
         */
        std::string get_title() const;

        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        /**
         * @brief Getter.
         * @return m_url
         */
        std::optional<std::string> get_url() const;

        //Setter
        /**
         * @brief Setter for m_description.
         * @param[in] description See the member variable.
         */
        void set_description(const std::optional<std::string> &description);

        /**
         * @brief Setter for m_hide_url.
         * @param[in] hide_url See the member variable.
         */
        void set_hide_url(const std::optional<bool> &hide_url);

        /**
         * @brief Setter for m_id.
         * @param[in] id See the member variable.
         */
        void set_id(const std::string &id);

        /**
         * @brief Setter for m_input_message_content.
         * @param[in] input_message_content See the member variable.
         */
        void set_input_message_content(const InputMessageContent::ptr &input_message_content);

        /**
         * @brief Setter for m_reply_markup.
         * @param[in] reply_markup See the member variable.
         */
        void set_reply_markup(const std::optional<InlineKeyboardMarkup::ptr> &reply_markup);

        /**
         * @brief Setter for m_thumbnail_height.
         * @param[in] thumbnail_height See the member variable.
         */
        void set_thumbnail_height(const std::optional<std::int32_t> &thumbnail_height);

        /**
         * @brief Setter for m_thumbnail_url.
         * @param[in] thumbnail_url See the member variable.
         */
        void set_thumbnail_url(const std::optional<std::string> &thumbnail_url);

        /**
         * @brief Setter for m_thumbnail_width.
         * @param[in] thumbnail_width See the member variable.
         */
        void set_thumbnail_width(const std::optional<std::int32_t> &thumbnail_width);

        /**
         * @brief Setter for m_title.
         * @param[in] title See the member variable.
         */
        void set_title(const std::string &title);

        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);

        /**
         * @brief Setter for m_url.
         * @param[in] url See the member variable.
         */
        void set_url(const std::optional<std::string> &url);
    };
} //namespace tgbot

#endif
