#ifndef TGBOT_TYPES_VIDEOCHATENDED_H
#define TGBOT_TYPES_VIDEOCHATENDED_H

#include <cstdint>
#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class VideoChatEnded
     * @brief This object represents a service message about a video chat ended in the chat.
     */
    class VideoChatEnded : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<VideoChatEnded>;

    private:
        //Member variables
        /**
         * @var m_duration
         * @brief Video chat duration in seconds.
         */
        std::int32_t m_duration;

    public:
        //Constructors
        VideoChatEnded();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit VideoChatEnded(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_duration
         */
        std::int32_t get_duration() const;

        //Setter
        /**
         * @brief Setter for m_duration.
         * @param[in] duration See the member variable.
         */
        void set_duration(const std::int32_t &duration);
    };
} //namespace tgbot

#endif
