#ifndef TGBOT_TYPES_MESSAGE_H
#define TGBOT_TYPES_MESSAGE_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>
#include <vector>

#include "tgbot/Type.h"
#include "tgbot/types/Animation.h"
#include "tgbot/types/Audio.h"
#include "tgbot/types/Chat.h"
#include "tgbot/types/ChatShared.h"
#include "tgbot/types/Contact.h"
#include "tgbot/types/Dice.h"
#include "tgbot/types/Document.h"
#include "tgbot/types/ForumTopicClosed.h"
#include "tgbot/types/ForumTopicCreated.h"
#include "tgbot/types/ForumTopicEdited.h"
#include "tgbot/types/ForumTopicReopened.h"
#include "tgbot/types/Game.h"
#include "tgbot/types/GeneralForumTopicHidden.h"
#include "tgbot/types/GeneralForumTopicUnhidden.h"
#include "tgbot/types/InlineKeyboardMarkup.h"
#include "tgbot/types/Invoice.h"
#include "tgbot/types/Location.h"
#include "tgbot/types/MessageAutoDeleteTimerChanged.h"
#include "tgbot/types/MessageEntity.h"
#include "tgbot/types/PassportData.h"
#include "tgbot/types/PhotoSize.h"
#include "tgbot/types/Poll.h"
#include "tgbot/types/ProximityAlertTriggered.h"
#include "tgbot/types/Sticker.h"
#include "tgbot/types/SuccessfulPayment.h"
#include "tgbot/types/User.h"
#include "tgbot/types/UserShared.h"
#include "tgbot/types/Venue.h"
#include "tgbot/types/Video.h"
#include "tgbot/types/VideoChatEnded.h"
#include "tgbot/types/VideoChatParticipantsInvited.h"
#include "tgbot/types/VideoChatScheduled.h"
#include "tgbot/types/VideoChatStarted.h"
#include "tgbot/types/VideoNote.h"
#include "tgbot/types/Voice.h"
#include "tgbot/types/WebAppData.h"
#include "tgbot/types/WriteAccessAllowed.h"

namespace tgbot
{
    /**
     * @class Message
     * @brief This object represents a message.
     */
    class Message : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<Message>;

    private:
        //Member variables
        /**
         * @var m_animation
         * @brief Optional. Message is an animation, information about the animation.
         * @details For backward compatibility, when this field is set, the document field will also be set.
         */
        std::optional<Animation::ptr> m_animation;
        /**
         * @var m_audio
         * @brief Optional. Message is an audio file, information about the file.
         */
        std::optional<Audio::ptr> m_audio;
        /**
         * @var m_author_signature
         * @brief Optional. Signature of the post author for messages in channels, or the custom title of an anonymous group administrator.
         */
        std::optional<std::string> m_author_signature;
        /**
         * @var m_caption
         * @brief Optional. Caption for the animation, audio, document, photo, video or voice.
         */
        std::optional<std::string> m_caption;
        /**
         * @var m_caption_entities
         * @brief Optional. For messages with a caption, special entities like usernames, URLs, bot commands, etc. that appear in the caption.
         */
        std::optional<std::vector<MessageEntity::ptr>> m_caption_entities;
        /**
         * @var m_channel_chat_created
         * @brief Optional. Service message: the channel has been created.
         * @details This field can't be received in a message coming through updates, because bot can't be a member of a channel when it is created.
         * @details It can only be found in reply_to_message if someone replies to a very first message in a channel.
         */
        std::optional<bool> m_channel_chat_created;
        /**
         * @var m_chat
         * @brief Conversation the message belongs to.
         */
        Chat::ptr m_chat;
        /**
         * @var m_chat_shared
         * @brief Optional. Service message: a chat was shared with the bot.
         */
        std::optional<ChatShared::ptr> m_chat_shared;
        /**
         * @var m_connected_website
         * @brief Optional. The domain name of the website on which the user has logged in.
         * @details More about Telegram Login ».
         */
        std::optional<std::string> m_connected_website;
        /**
         * @var m_contact
         * @brief Optional. Message is a shared contact, information about the contact.
         */
        std::optional<Contact::ptr> m_contact;
        /**
         * @var m_date
         * @brief Date the message was sent in Unix time.
         */
        std::uint64_t m_date;
        /**
         * @var m_delete_chat_photo
         * @brief Optional. Service message: the chat photo was deleted.
         */
        std::optional<bool> m_delete_chat_photo;
        /**
         * @var m_dice
         * @brief Optional. Message is a dice with random value.
         */
        std::optional<Dice::ptr> m_dice;
        /**
         * @var m_document
         * @brief Optional. Message is a general file, information about the file.
         */
        std::optional<Document::ptr> m_document;
        /**
         * @var m_edit_date
         * @brief Optional. Date the message was last edited in Unix time.
         */
        std::optional<std::uint64_t> m_edit_date;
        /**
         * @var m_entities
         * @brief Optional. For text messages, special entities like usernames, URLs, bot commands, etc. that appear in the text.
         */
        std::optional<std::vector<MessageEntity::ptr>> m_entities;
        /**
         * @var m_forum_topic_closed
         * @brief Optional. Service message: forum topic closed.
         */
        std::optional<ForumTopicClosed::ptr> m_forum_topic_closed;
        /**
         * @var m_forum_topic_created
         * @brief Optional. Service message: forum topic created.
         */
        std::optional<ForumTopicCreated::ptr> m_forum_topic_created;
        /**
         * @var m_forum_topic_edited
         * @brief Optional. Service message: forum topic edited.
         */
        std::optional<ForumTopicEdited::ptr> m_forum_topic_edited;
        /**
         * @var m_forum_topic_reopened
         * @brief Optional. Service message: forum topic reopened.
         */
        std::optional<ForumTopicReopened::ptr> m_forum_topic_reopened;
        /**
         * @var m_forward_date
         * @brief Optional. For forwarded messages, date the original message was sent in Unix time.
         */
        std::optional<std::uint64_t> m_forward_date;
        /**
         * @var m_forward_from
         * @brief Optional. For forwarded messages, sender of the original message.
         */
        std::optional<User::ptr> m_forward_from;
        /**
         * @var m_forward_from_chat
         * @brief Optional. For messages forwarded from channels or from anonymous administrators, information about the original sender chat.
         */
        std::optional<Chat::ptr> m_forward_from_chat;
        /**
         * @var m_forward_from_message_id
         * @brief Optional. For messages forwarded from channels, identifier of the original message in the channel.
         */
        std::optional<std::int32_t> m_forward_from_message_id;
        /**
         * @var m_forward_sender_name
         * @brief Optional. Sender's name for messages forwarded from users who disallow adding a link to their account in forwarded messages.
         */
        std::optional<std::string> m_forward_sender_name;
        /**
         * @var m_forward_signature
         * @brief Optional. For messages forwarded from channels, signature of the post author if present.
         */
        std::optional<std::string> m_forward_signature;
        /**
         * @var m_from
         * @brief Optional. Sender of the message; empty for messages sent to channels.
         * @details For backward compatibility, the field contains a fake sender user in non-channel chats, if the message was sent on behalf of a chat.
         */
        std::optional<User::ptr> m_from;
        /**
         * @var m_game
         * @brief Optional. Message is a game, information about the game.
         * @details More about games ».
         */
        std::optional<Game::ptr> m_game;
        /**
         * @var m_general_forum_topic_hidden
         * @brief Optional. Service message: the 'General' forum topic hidden.
         */
        std::optional<GeneralForumTopicHidden::ptr> m_general_forum_topic_hidden;
        /**
         * @var m_general_forum_topic_unhidden
         * @brief Optional. Service message: the 'General' forum topic unhidden.
         */
        std::optional<GeneralForumTopicUnhidden::ptr> m_general_forum_topic_unhidden;
        /**
         * @var m_group_chat_created
         * @brief Optional. Service message: the group has been created.
         */
        std::optional<bool> m_group_chat_created;
        /**
         * @var m_has_media_spoiler
         * @brief Optional. True, if the message media is covered by a spoiler animation.
         */
        std::optional<bool> m_has_media_spoiler;
        /**
         * @var m_has_protected_content
         * @brief Optional. True, if the message can't be forwarded.
         */
        std::optional<bool> m_has_protected_content;
        /**
         * @var m_invoice
         * @brief Optional. Message is an invoice for a payment, information about the invoice.
         * @details More about payments ».
         */
        std::optional<Invoice::ptr> m_invoice;
        /**
         * @var m_is_automatic_forward
         * @brief Optional. True, if the message is a channel post that was automatically forwarded to the connected discussion group.
         */
        std::optional<bool> m_is_automatic_forward;
        /**
         * @var m_is_topic_message
         * @brief Optional. True, if the message is sent to a forum topic.
         */
        std::optional<bool> m_is_topic_message;
        /**
         * @var m_left_chat_member
         * @brief Optional. A member was removed from the group, information about them (this member may be the bot itself).
         */
        std::optional<User::ptr> m_left_chat_member;
        /**
         * @var m_location
         * @brief Optional. Message is a shared location, information about the location.
         */
        std::optional<Location::ptr> m_location;
        /**
         * @var m_media_group_id
         * @brief Optional. The unique identifier of a media message group this message belongs to.
         */
        std::optional<std::string> m_media_group_id;
        /**
         * @var m_message_auto_delete_timer_changed
         * @brief Optional. Service message: auto-delete timer settings changed in the chat.
         */
        std::optional<MessageAutoDeleteTimerChanged::ptr> m_message_auto_delete_timer_changed;
        /**
         * @var m_message_id
         * @brief Unique message identifier inside this chat.
         */
        std::int32_t m_message_id;
        /**
         * @var m_message_thread_id
         * @brief Optional. Unique identifier of a message thread to which the message belongs; for supergroups only.
         */
        std::int32_t m_message_thread_id;
        /**
         * @var m_migrate_from_chat_id
         * @brief Optional. The supergroup has been migrated from a group with the specified identifier.
         * @details This number may have more than 32 significant bits and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a signed 64-bit integer or double-precision float type are safe for storing this identifier.
         */
        std::optional<std::int64_t> m_migrate_from_chat_id;
        /**
         * @var m_migrate_to_chat_id
         * @brief Optional. The group has been migrated to a supergroup with the specified identifier.
         * @details This number may have more than 32 significant bits and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a signed 64-bit integer or double-precision float type are safe for storing this identifier.
         */
        std::optional<std::int64_t> m_migrate_to_chat_id;
        /**
         * @var m_new_chat_members
         * @brief Optional. New members that were added to the group or supergroup and information about them (the bot itself may be one of these members).
         */
        std::optional<std::vector<User::ptr>> m_new_chat_members;
        /**
         * @var m_new_chat_photo
         * @brief Optional. A chat photo was change to this value.
         */
        std::optional<std::vector<PhotoSize::ptr>> m_new_chat_photo;
        /**
         * @var m_new_chat_title
         * @brief Optional. A chat title was changed to this value.
         */
        std::optional<std::string> m_new_chat_title;
        /**
         * @var m_passport_data
         * @brief Optional. Telegram Passport data.
         */
        std::optional<PassportData::ptr> m_passport_data;
        /**
         * @var m_photo
         * @brief Optional. Message is a photo, available sizes of the photo.
         */
        std::optional<std::vector<PhotoSize::ptr>> m_photo;
        /**
         * @var m_pinned_message
         * @brief Optional. Specified message was pinned.
         * @details Note that the Message object in this field will not contain further reply_to_message fields even if it is itself a reply.
         */
        std::optional<Message::ptr> m_pinned_message;
        /**
         * @var m_poll
         * @brief Optional. Message is a native poll, information about the poll.
         */
        std::optional<Poll::ptr> m_poll;
        /**
         * @var m_proximity_alert_triggered
         * @brief Optional. Service message.
         * @details A user in the chat triggered another user's proximity alert while sharing Live Location.
         */
        std::optional<ProximityAlertTriggered::ptr> m_proximity_alert_triggered;
        /**
         * @var m_reply_markup
         * @brief Optional. Inline keyboard attached to the message.
         * @details login_url buttons are represented as ordinary url buttons.
         */
        std::optional<InlineKeyboardMarkup::ptr> m_reply_markup;
        /**
         * @var m_reply_to_message
         * @brief Optional. For replies, the original message.
         * @details Note that the Message object in this field will not contain further reply_to_message fields even if it itself is a reply.
         */
        std::optional<Message::ptr> m_reply_to_message;
        /**
         * @var m_sender_chat
         * @brief Optional. Sender of the message, sent on behalf of a chat.
         * @details For example, the channel itself for channel posts, the supergroup itself for messages from anonymous group administrators, the linked channel for messages automatically forwarded to the discussion group.
         * @details For backward compatibility, the field from contains a fake sender user in non-channel chats, if the message was sent on behalf of a chat.
         */
        std::optional<Chat::ptr> m_sender_chat;
        /**
         * @var m_sticker
         * @brief Optional. Message is a sticker, information about the sticker.
         */
        std::optional<Sticker::ptr> m_sticker;
        /**
         * @var m_successful_payment
         * @brief Optional. Message is a service message about a successful payment, information about the payment.
         * @details More about payments ».
         */
        std::optional<SuccessfulPayment::ptr> m_successful_payment;
        /**
         * @var m_supergroup_chat_created
         * @brief Optional. Service message: the supergroup has been created.
         * @details This field can't be received in a message coming through updates, because bot can't be a member of a supergroup when it is created.
         * @details It can only be found in reply_to_message if someone replies to a very first message in a directly created supergroup.
         */
        std::optional<bool> m_supergroup_chat_created;
        /**
         * @var m_text
         * @brief Optional. For text messages, the actual UTF-8 text of the message.
         */
        std::optional<std::string> m_text;
        /**
         * @var m_user_shared
         * @brief Optional. Service message: a user was shared with the bot.
         */
        std::optional<UserShared::ptr> m_user_shared;
        /**
         * @var m_venue
         * @brief Optional. Message is a venue, information about the venue.
         * @details For backward compatibility, when this field is set, the location field will also be set.
         */
        std::optional<Venue::ptr> m_venue;
        /**
         * @var m_via_bot
         * @brief Optional. Bot through which the message was sent.
         */
        std::optional<User::ptr> m_via_bot;
        /**
         * @var m_video
         * @brief Optional. Message is a video, information about the video.
         */
        std::optional<Video::ptr> m_video;
        /**
         * @var m_video_chat_ended
         * @brief Optional. Service message: video chat ended.
         */
        std::optional<VideoChatEnded::ptr> m_video_chat_ended;
        /**
         * @var m_video_chat_participants_invited
         * @brief Optional. Service message: new participants invited to a video chat.
         */
        std::optional<VideoChatParticipantsInvited::ptr> m_video_chat_participants_invited;
        /**
         * @var m_video_chat_scheduled
         * @brief Optional. Service message: video chat scheduled.
         */
        std::optional<VideoChatScheduled::ptr> m_video_chat_scheduled;
        /**
         * @var m_video_chat_started
         * @brief Optional. Service message: video chat started.
         */
        std::optional<VideoChatStarted::ptr> m_video_chat_started;
        /**
         * @var m_video_note
         * @brief Optional. Message is a video note, information about the video message.
         */
        std::optional<VideoNote::ptr> m_video_note;
        /**
         * @var m_voice
         * @brief Optional. Message is a voice message, information about the file.
         */
        std::optional<Voice::ptr> m_voice;
        /**
         * @var m_web_app_data
         * @brief Optional. Service message: data sent by a Web App.
         */
        std::optional<WebAppData::ptr> m_web_app_data;
        /**
         * @var m_write_access_allowed
         * @brief Optional. Service message: the user allowed the bot added to the attachment menu to write messages.
         */
        std::optional<WriteAccessAllowed::ptr> m_write_access_allowed;

    public:
        //Constructors
        Message();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit Message(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_animation
         */
        std::optional<Animation::ptr> get_animation() const;

        /**
         * @brief Getter.
         * @return m_audio
         */
        std::optional<Audio::ptr> get_audio() const;

        /**
         * @brief Getter.
         * @return m_author_signature
         */
        std::optional<std::string> get_author_signature() const;

        /**
         * @brief Getter.
         * @return m_caption
         */
        std::optional<std::string> get_caption() const;

        /**
         * @brief Getter.
         * @return m_caption_entities
         */
        std::optional<std::vector<MessageEntity::ptr>> get_caption_entities() const;

        /**
         * @brief Getter.
         * @return m_channel_chat_created
         */
        std::optional<bool> get_channel_chat_created() const;

        /**
         * @brief Getter.
         * @return m_chat
         */
        Chat::ptr get_chat() const;

        /**
         * @brief Getter.
         * @return m_chat_shared
         */
        std::optional<ChatShared::ptr> get_chat_shared() const;

        /**
         * @brief Getter.
         * @return m_connected_website
         */
        std::optional<std::string> get_connected_website() const;

        /**
         * @brief Getter.
         * @return m_contact
         */
        std::optional<Contact::ptr> get_contact() const;

        /**
         * @brief Getter.
         * @return m_date
         */
        std::uint64_t get_date() const;

        /**
         * @brief Getter.
         * @return m_delete_chat_photo
         */
        std::optional<bool> get_delete_chat_photo() const;

        /**
         * @brief Getter.
         * @return m_dice
         */
        std::optional<Dice::ptr> get_dice() const;

        /**
         * @brief Getter.
         * @return m_document
         */
        std::optional<Document::ptr> get_document() const;

        /**
         * @brief Getter.
         * @return m_edit_date
         */
        std::optional<std::uint64_t> get_edit_date() const;

        /**
         * @brief Getter.
         * @return m_entities
         */
        std::optional<std::vector<MessageEntity::ptr>> get_entities() const;

        /**
         * @brief Getter.
         * @return m_forum_topic_closed
         */
        std::optional<ForumTopicClosed::ptr> get_forum_topic_closed() const;

        /**
         * @brief Getter.
         * @return m_forum_topic_created
         */
        std::optional<ForumTopicCreated::ptr> get_forum_topic_created() const;

        /**
         * @brief Getter.
         * @return m_forum_topic_edited
         */
        std::optional<ForumTopicEdited::ptr> get_forum_topic_edited() const;

        /**
         * @brief Getter.
         * @return m_forum_topic_reopened
         */
        std::optional<ForumTopicReopened::ptr> get_forum_topic_reopened() const;

        /**
         * @brief Getter.
         * @return m_forward_date
         */
        std::optional<std::uint64_t> get_forward_date() const;

        /**
         * @brief Getter.
         * @return m_forward_from
         */
        std::optional<User::ptr> get_forward_from() const;

        /**
         * @brief Getter.
         * @return forward_from_chat
         */
        std::optional<Chat::ptr> get_forward_from_chat() const;

        /**
         * @brief Getter.
         * @return m_forward_from_message_id
         */
        std::optional<std::int32_t> get_forward_from_message_id() const;

        /**
         * @brief Getter.
         * @return m_forward_sender_name
         */
        std::optional<std::string> get_forward_sender_name() const;

        /**
         * @brief Getter.
         * @return m_forward_signature
         */
        std::optional<std::string> get_forward_signature() const;

        /**
         * @brief Getter.
         * @return m_from
         */
        std::optional<User::ptr> get_from() const;

        /**
         * @brief Getter.
         * @return m_game
         */
        std::optional<Game::ptr> get_game() const;

        /**
         * @brief Getter.
         * @return m_general_forum_topic_hidden
         */
        std::optional<GeneralForumTopicHidden::ptr> get_general_forum_topic_hidden() const;

        /**
         * @brief Getter.
         * @return m_general_forum_topic_unhidden
         */
        std::optional<GeneralForumTopicUnhidden::ptr> get_general_forum_topic_unhidden() const;

        /**
         * @brief Getter.
         * @return m_group_chat_created
         */
        std::optional<bool> get_group_chat_created() const;

        /**
         * @brief Getter.
         * @return m_has_media_spoiler
         */
        std::optional<bool> get_has_media_spoiler() const;

        /**
         * @brief Getter.
         * @return m_has_protected_content
         */
        std::optional<bool> get_has_protected_content() const;

        /**
         * @brief Getter.
         * @return m_invoice
         */
        std::optional<Invoice::ptr> get_invoice() const;

        /**
         * @brief Getter.
         * @return m_is_automatic_forward
         */
        std::optional<bool> get_is_automatic_forward() const;

        /**
         * @brief Getter.
         * @return m_is_topic_message
         */
        std::optional<bool> get_is_topic_message() const;

        /**
         * @brief Getter.
         * @return m_left_chat_member
         */
        std::optional<User::ptr> get_left_chat_member() const;

        /**
         * @brief Getter.
         * @return m_location
         */
        std::optional<Location::ptr> get_location() const;

        /**
         * @brief Getter.
         * @return m_media_group_id
         */
        std::optional<std::string> get_media_group_id() const;

        /**
         * @brief Getter.
         * @return m_message_auto_delete_timer_changed
         */
        std::optional<MessageAutoDeleteTimerChanged::ptr> get_message_auto_delete_timer_changed() const;

        /**
         * @brief Getter.
         * @return m_message_id
         */
        std::int32_t get_message_id() const;

        /**
         * @brief Getter.
         * @return m_message_thread_id
         */
        std::int32_t get_message_thread_id() const;

        /**
         * @brief Getter.
         * @return m_migrate_from_chat_id
         */
        std::optional<std::int64_t> get_migrate_from_chat_id() const;

        /**
         * @brief Getter.
         * @return m_migrate_to_chat_id
         */
        std::optional<std::int64_t> get_migrate_to_chat_id() const;

        /**
         * @brief Getter.
         * @return m_new_chat_members
         */
        std::optional<std::vector<User::ptr>> get_new_chat_members() const;

        /**
         * @brief Getter.
         * @return m_new_chat_photo
         */
        std::optional<std::vector<PhotoSize::ptr>> get_new_chat_photo() const;

        /**
         * @brief Getter.
         * @return m_new_chat_title
         */
        std::optional<std::string> get_new_chat_title() const;

        /**
         * @brief Getter.
         * @return m_passport_data
         */
        std::optional<PassportData::ptr> get_passport_data() const;

        /**
         * @brief Getter.
         * @return m_photo
         */
        std::optional<std::vector<PhotoSize::ptr>> get_photo() const;

        /**
         * @brief Getter.
         * @return m_pinned_message
         */
        std::optional<Message::ptr> get_pinned_message() const;

        /**
         * @brief Getter.
         * @return m_poll
         */
        std::optional<Poll::ptr> get_poll() const;

        /**
         * @brief Getter.
         * @return m_proximity_alert_triggered
         */
        std::optional<ProximityAlertTriggered::ptr> get_proximity_alert_triggered() const;

        /**
         * @brief Getter.
         * @return m_reply_markup
         */
        std::optional<InlineKeyboardMarkup::ptr> get_reply_markup() const;

        /**
         * @brief Getter.
         * @return m_reply_to_message
         */
        std::optional<Message::ptr> get_reply_to_message() const;

        /**
         * @brief Getter.
         * @return m_sender_chat
         */
        std::optional<Chat::ptr> get_sender_chat() const;

        /**
         * @brief Getter.
         * @return m_sticker
         */
        std::optional<Sticker::ptr> get_sticker() const;

        /**
         * @brief Getter.
         * @return m_successful_payment
         */
        std::optional<SuccessfulPayment::ptr> get_successful_payment() const;

        /**
         * @brief Getter.
         * @return m_supergroup_chat_created
         */
        std::optional<bool> get_supergroup_chat_created() const;

        /**
         * @brief Getter.
         * @return m_text
         */
        std::optional<std::string> get_text() const;

        /**
         * @brief Getter.
         * @return m_user_shared
         */
        std::optional<UserShared::ptr> get_user_shared() const;

        /**
         * @brief Getter.
         * @return m_venue
         */
        std::optional<Venue::ptr> get_venue() const;

        /**
         * @brief Getter.
         * @return m_via_bot
         */
        std::optional<User::ptr> get_via_bot() const;

        /**
         * @brief Getter.
         * @return m_video
         */
        std::optional<Video::ptr> get_video() const;

        /**
         * @brief Getter.
         * @return m_video_chat_ended
         */
        std::optional<VideoChatEnded::ptr> get_video_chat_ended() const;

        /**
         * @brief Getter.
         * @return m_video_chat_participants_invited
         */
        std::optional<VideoChatParticipantsInvited::ptr> get_video_chat_participants_invited() const;

        /**
         * @brief Getter.
         * @return m_video_chat_scheduled
         */
        std::optional<VideoChatScheduled::ptr> get_video_chat_scheduled() const;

        /**
         * @brief Getter.
         * @return m_video_chat_started
         */
        std::optional<VideoChatStarted::ptr> get_video_chat_started() const;

        /**
         * @brief Getter.
         * @return m_video_note
         */
        std::optional<VideoNote::ptr> get_video_note() const;

        /**
         * @brief Getter.
         * @return m_voice
         */
        std::optional<Voice::ptr> get_voice() const;

        /**
         * @brief Getter.
         * @return m_web_app_data
         */
        std::optional<WebAppData::ptr> get_web_app_data() const;

        /**
         * @brief Getter.
         * @return m_write_access_allowed
         */
        std::optional<WriteAccessAllowed::ptr> get_write_access_allowed() const;

        //Setter
        /**
         * @brief Setter for m_animation.
         * @param[in] animation See the member variable.
         */
        void set_animation(const std::optional<Animation::ptr> &animation);

        /**
         * @brief Setter for m_audio.
         * @param[in] audio See the member variable.
         */
        void set_audio(const std::optional<Audio::ptr> &audio);

        /**
         * @brief Setter for m_author_signature.
         * @param[in] author_signature See the member variable.
         */
        void set_author_signature(const std::optional<std::string> &author_signature);

        /**
         * @brief Setter for m_caption.
         * @param[in] caption See the member variable.
         */
        void set_caption(const std::optional<std::string> &caption);

        /**
         * @brief Setter for m_caption_entities.
         * @param[in] caption_entities See the member variable.
         */
        void set_caption_entities(const std::optional<std::vector<MessageEntity::ptr>> &caption_entities);

        /**
         * @brief Setter for m_channel_chat_created.
         * @param[in] channel_chat_created See the member variable.
         */
        void set_channel_chat_created(const std::optional<bool> &channel_chat_created);

        /**
         * @brief Setter for m_chat.
         * @param[in] chat See the member variable.
         */
        void set_chat(const Chat::ptr &chat);

        /**
         * @brief Setter for m_chat_shared.
         * @param[in] chat_shared See the member variable.
         */
        void set_chat_shared(const std::optional<ChatShared::ptr> &chat_shared);

        /**
         * @brief Setter for m_connected_website.
         * @param[in] connected_website See the member variable.
         */
        void set_connected_website(const std::optional<std::string> &connected_website);

        /**
         * @brief Setter for m_contact.
         * @param[in] contact See the member variable.
         */
        void set_contact(const std::optional<Contact::ptr> &contact);

        /**
         * @brief Setter for m_date.
         * @param[in] date See the member variable.
         */
        void set_date(const std::uint64_t &date);

        /**
         * @brief Setter for m_delete_chat_photo.
         * @param[in] delete_chat_photo See the member variable.
         */
        void set_delete_chat_photo(const std::optional<bool> &delete_chat_photo);

        /**
         * @brief Setter for m_dice.
         * @param[in] dice See the member variable.
         */
        void set_dice(const std::optional<Dice::ptr> &dice);

        /**
         * @brief Setter for m_document.
         * @param[in] document See the member variable.
         */
        void set_document(const std::optional<Document::ptr> &document);

        /**
         * @brief Setter for m_edit_date.
         * @param[in] edit_date See the member variable.
         */
        void set_edit_date(const std::optional<std::uint64_t> &edit_date);

        /**
         * @brief Setter for m_entities.
         * @param[in] entities See the member variable.
         */
        void set_entities(const std::optional<std::vector<MessageEntity::ptr>> &entities);

        /**
         * @brief Setter for m_forum_topic_closed.
         * @param[in] forum_topic_closed See the member variable.
         */
        void set_forum_topic_closed(const std::optional<ForumTopicClosed::ptr> &forum_topic_closed);

        /**
         * @brief Setter for m_forum_topic_created.
         * @param[in] forum_topic_created See the member variable.
         */
        void set_forum_topic_created(const std::optional<ForumTopicCreated::ptr> &forum_topic_created);

        /**
         * @brief Setter for m_forum_topic_edited.
         * @param[in] forum_topic_edited See the member variable.
         */
        void set_forum_topic_edited(const std::optional<ForumTopicEdited::ptr> &forum_topic_edited);

        /**
         * @brief Setter for m_forum_topic_reopened.
         * @param[in] forum_topic_reopened See the member variable.
         */
        void set_forum_topic_reopened(const std::optional<ForumTopicReopened::ptr> &forum_topic_reopened);

        /**
         * @brief Setter for m_forward_date.
         * @param[in] forward_date See the member variable.
         */
        void set_forward_date(const std::optional<std::uint64_t> &forward_date);

        /**
         * @brief Setter for m_forward_from.
         * @param[in] forward_from See the member variable.
         */
        void set_forward_from(const std::optional<User::ptr> &forward_from);

        /**
         * @brief Setter for m_forward_from_chat.
         * @param[in] forward_from_chat See the member variable.
         */
        void set_forward_from_chat(const std::optional<Chat::ptr> &forward_from_chat);

        /**
         * @brief Setter for m_forward_from_message_id.
         * @param[in] forward_from_message_id See the member variable.
         */
        void set_forward_from_message_id(const std::optional<std::int32_t> &forward_from_message_id);

        /**
         * @brief Setter for m_forward_sender_name.
         * @param[in] forward_sender_name See the member variable.
         */
        void set_forward_sender_name(const std::optional<std::string> &forward_sender_name);

        /**
         * @brief Setter for m_forward_signature.
         * @param[in] forward_signature See the member variable.
         */
        void set_forward_signature(const std::optional<std::string> &forward_signature);

        /**
         * @brief Setter for m_from.
         * @param[in] from See the member variable.
         */
        void set_from(const std::optional<User::ptr> &from);

        /**
         * @brief Setter for m_game.
         * @param[in] game See the member variable.
         */
        void set_game(const std::optional<Game::ptr> &game);

        /**
         * @brief Setter for m_general_forum_topic_hidden.
         * @param[in] general_forum_topic_hidden See the member variable.
         */
        void
          set_general_forum_topic_hidden(const std::optional<GeneralForumTopicHidden::ptr> &general_forum_topic_hidden);

        /**
         * @brief Setter for m_general_forum_topic_unhidden.
         * @param[in] general_forum_topic_unhidden See the member variable.
         */
        void set_general_forum_topic_unhidden(
          const std::optional<GeneralForumTopicUnhidden::ptr> &general_forum_topic_unhidden);

        /**
         * @brief Setter for m_group_chat_created.
         * @param[in] group_chat_created See the member variable.
         */
        void set_group_chat_created(const std::optional<bool> &group_chat_created);

        /**
         * @brief Setter for m_has_media_spoiler.
         * @param[in] has_media_spoiler See the member variable.
         */
        void set_has_media_spoiler(const std::optional<bool> &has_media_spoiler);

        /**
         * @brief Setter for m_has_protected_content.
         * @param[in] has_protected_content See the member variable.
         */
        void set_has_protected_content(const std::optional<bool> &has_protected_content);

        /**
         * @brief Setter for m_invoice.
         * @param[in] invoice See the member variable.
         */
        void set_invoice(const std::optional<Invoice::ptr> &invoice);

        /**
         * @brief Setter for m_is_automatic_forward.
         * @param[in] is_automatic_forward See the member variable.
         */
        void set_is_automatic_forward(const std::optional<bool> &is_automatic_forward);
        /**
         * @brief Setter for m_is_topic_message.
         * @param[in] is_topic_message See the member variable.
         */
        void set_is_topic_message(const std::optional<bool> &is_topic_message);

        /**
         * @brief Setter for m_left_chat_member.
         * @param[in] left_chat_member See the member variable.
         */
        void set_left_chat_member(const std::optional<User::ptr> &left_chat_member);

        /**
         * @brief Setter for m_location.
         * @param[in] location See the member variable.
         */
        void set_location(const std::optional<Location::ptr> &location);

        /**
         * @brief Setter for m_media_group_id.
         * @param[in] media_group_id See the member variable.
         */
        void set_media_group_id(const std::optional<std::string> &media_group_id);

        /**
         * @brief Setter for m_message_auto_delete_timer_changed.
         * @param[in] message_auto_delete_timer_changed See the member variable.
         */
        void set_message_auto_delete_timer_changed(
          const std::optional<MessageAutoDeleteTimerChanged::ptr> &message_auto_delete_timer_changed);

        /**
         * @brief Setter for m_message_id.
         * @param[in] message_id See the member variable.
         */
        void set_message_id(const std::int32_t &message_id);
        /**
         * @brief Setter for m_message_thread_id.
         * @param[in] message_thread_id See the member variable.
         */
        void set_message_thread_id(const std::int32_t &message_thread_id);

        /**
         * @brief Setter for m_migrate_from_chat_id.
         * @param[in] migrate_from_chat_id See the member variable.
         */
        void set_migrate_from_chat_id(const std::optional<std::int64_t> &migrate_from_chat_id);

        /**
         * @brief Setter for m_migrate_to_chat_id.
         * @param[in] migrate_to_chat_id See the member variable.
         */
        void set_migrate_to_chat_id(const std::optional<std::int64_t> &migrate_to_chat_id);

        /**
         * @brief Setter for m_new_chat_members.
         * @param[in] new_chat_members See the member variable.
         */
        void set_new_chat_members(const std::optional<std::vector<User::ptr>> &new_chat_members);

        /**
         * @brief Setter for m_new_chat_photo.
         * @param[in] new_chat_photo See the member variable.
         */
        void set_new_chat_photo(const std::optional<std::vector<PhotoSize::ptr>> &new_chat_photo);

        /**
         * @brief Setter for m_new_chat_title.
         * @param[in] new_chat_title See the member variable.
         */
        void set_new_chat_title(const std::optional<std::string> &new_chat_title);

        /**
         * @brief Setter for m_passport_data.
         * @param[in] passport_data See the member variable.
         */
        void set_passport_data(const std::optional<PassportData::ptr> &passport_data);

        /**
         * @brief Setter for m_photo.
         * @param[in] photo See the member variable.
         */
        void set_photo(const std::optional<std::vector<PhotoSize::ptr>> &photo);

        /**
         * @brief Setter for m_pinned_message.
         * @param[in] pinned_message See the member variable.
         */
        void set_pinned_message(const std::optional<Message::ptr> &pinned_message);

        /**
         * @brief Setter for m_poll.
         * @param[in] poll See the member variable.
         */
        void set_poll(const std::optional<Poll::ptr> &poll);

        /**
         * @brief Setter for m_proximity_alert_triggered.
         * @param[in] proximity_alert_triggered See the member variable.
         */
        void set_proximity_alert_triggered(const std::optional<ProximityAlertTriggered::ptr> &proximity_alert_triggered);

        /**
         * @brief Setter for m_reply_markup.
         * @param[in] reply_markup See the member variable.
         */
        void set_reply_markup(const std::optional<InlineKeyboardMarkup::ptr> &reply_markup);
        /**
         * @brief Setter for m_reply_to_message.
         * @param[in] reply_to_message See the member variable.
         */
        void set_reply_to_message(const std::optional<Message::ptr> &reply_to_message);

        /**
         * @brief Setter for m_sender_chat.
         * @param[in] sender_chat See the member variable.
         */
        void set_sender_chat(const std::optional<Chat::ptr> &sender_chat);

        /**
         * @brief Setter for m_sticker.
         * @param[in] sticker See the member variable.
         */
        void set_sticker(const std::optional<Sticker::ptr> &sticker);

        /**
         * @brief Setter for m_successful_payment.
         * @param[in] successful_payment See the member variable.
         */
        void set_successful_payment(const std::optional<SuccessfulPayment::ptr> &successful_payment);

        /**
         * @brief Setter for m_supergroup_chat_created.
         * @param[in] supergroup_chat_created See the member variable.
         */
        void set_supergroup_chat_created(const std::optional<bool> &supergroup_chat_created);

        /**
         * @brief Setter for m_text.
         * @param[in] text See the member variable.
         */
        void set_text(const std::optional<std::string> &text);

        /**
         * @brief Setter for m_user_shared.
         * @param[in] user_shared See the member variable.
         */
        void set_user_shared(const std::optional<UserShared::ptr> &user_shared);

        /**
         * @brief Setter for m_venue.
         * @param[in] venue See the member variable.
         */
        void set_venue(const std::optional<Venue::ptr> &venue);

        /**
         * @brief Setter for m_via_bot.
         * @param[in] via_bot See the member variable.
         */
        void set_via_bot(const std::optional<User::ptr> &via_bot);

        /**
         * @brief Setter for m_video.
         * @param[in] video See the member variable.
         */
        void set_video(const std::optional<Video::ptr> &video);

        /**
         * @brief Setter for m_video_chat_ended.
         * @param[in] video_chat_ended See the member variable.
         */
        void set_video_chat_ended(const std::optional<VideoChatEnded::ptr> &video_chat_ended);

        /**
         * @brief Setter for m_video_chat_participants_invited.
         * @param[in] video_chat_participants_invited See the member variable.
         */
        void set_video_chat_participants_invited(
          const std::optional<VideoChatParticipantsInvited::ptr> &video_chat_participants_invited);

        /**
         * @brief Setter for m_video_chat_scheduled.
         * @param[in] video_chat_scheduled See the member variable.
         */
        void set_video_chat_scheduled(const std::optional<VideoChatScheduled::ptr> &video_chat_scheduled);

        /**
         * @brief Setter for m_video_chat_started.
         * @param[in] video_chat_started See the member variable.
         */
        void set_video_chat_started(const std::optional<VideoChatStarted::ptr> &video_chat_started);

        /**
         * @brief Setter for m_video_note.
         * @param[in] video_note See the member variable.
         */
        void set_video_note(const std::optional<VideoNote::ptr> &video_note);

        /**
         * @brief Setter for m_voice.
         * @param[in] voice See the member variable.
         */
        void set_voice(const std::optional<Voice::ptr> &voice);

        /**
         * @brief Setter for m_web_app_data.
         * @param[in] web_app_data See the member variable.
         */
        void set_web_app_data(const std::optional<WebAppData::ptr> &web_app_data);

        /**
         * @brief Setter for m_write_access_allowed.
         * @param[in] write_access_allowed See the member variable.
         */
        void set_write_access_allowed(const std::optional<WriteAccessAllowed::ptr> &write_access_allowed);
    };
} //namespace tgbot

#endif
