#ifndef TGBOT_TYPES_POLLANSWER_H
#define TGBOT_TYPES_POLLANSWER_H

#include <cstdint>
#include <memory>
#include <string>
#include <vector>

#include "tgbot/Type.h"
#include "tgbot/types/User.h"

namespace tgbot
{
    /**
     * @class PollAnswer
     * @brief This object represents an answer of a user in a non-anonymous poll.
     */
    class PollAnswer : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<PollAnswer>;

    private:
        //Member variables
        /**
         * @var m_option_ids
         * @brief 0-based identifiers of answer options, chosen by the user.
         * @details May be empty if the user retracted their vote.
         */
        std::vector<std::int32_t> m_option_ids;
        /**
         * @var m_poll_id
         * @brief Unique poll identifier.
         */
        std::string m_poll_id;
        /**
         * @var m_user
         * @brief The user, who changed the answer to the poll.
         */
        User::ptr m_user;

    public:
        //Constructors
        PollAnswer();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit PollAnswer(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_option_ids
         */
        std::vector<std::int32_t> get_option_ids() const;

        /**
         * @brief Getter.
         * @return m_poll_id
         */
        std::string get_poll_id() const;

        /**
         * @brief Getter.
         * @return m_user
         */
        User::ptr get_user() const;

        //Setter
        /**
         * @brief Setter for m_option_ids.
         * @param[in] option_ids See the member variable.
         */
        void set_option_ids(const std::vector<std::int32_t> &option_ids);

        /**
         * @brief Setter for m_poll_id.
         * @param[in] poll_id See the member variable.
         */
        void set_poll_id(const std::string &poll_id);

        /**
         * @brief Setter for m_user.
         * @param[in] user See the member variable.
         */
        void set_user(const User::ptr &user);
    };
} //namespace tgbot

#endif
