#ifndef TGBOT_TYPES_VIDEOCHATSTARTED_H
#define TGBOT_TYPES_VIDEOCHATSTARTED_H

#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class VideoChatStarted
     * @brief This object represents a service message about a video chat started in the chat.
     * @details Currently holds no information.
     */
    class VideoChatStarted : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<VideoChatStarted>;

        //Constructors
        VideoChatStarted();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit VideoChatStarted(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;
    };
} //namespace tgbot

#endif
