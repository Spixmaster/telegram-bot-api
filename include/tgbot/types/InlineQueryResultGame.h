#ifndef TGBOT_TYPES_INLINEQUERYRESULTGAME_H
#define TGBOT_TYPES_INLINEQUERYRESULTGAME_H

#include <memory>
#include <optional>
#include <string>

#include "tgbot/types/InlineKeyboardMarkup.h"
#include "tgbot/types/InlineQueryResult.h"

namespace tgbot
{
    /**
     * @class InlineQueryResultGame
     * @brief Represents a Game.
     * @note This will only work in Telegram versions released after October 1, 2016. Older clients will not display any inline results if a game result is among them.
     */
    class InlineQueryResultGame : public InlineQueryResult
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<InlineQueryResultGame>;

    private:
        //Member variables
        /**
         * @var m_game_short_name
         * @brief Short name of the game.
         */
        std::string m_game_short_name;
        /**
         * @var m_id
         * @brief Unique identifier for this result, 1-64 bytes.
         */
        std::string m_id;
        /**
         * @var m_reply_markup
         * @brief Optional. Inline keyboard attached to the message.
         */
        std::optional<InlineKeyboardMarkup::ptr> m_reply_markup;
        /**
         * @var m_type
         * @brief Type of the result, must be game.
         */
        std::string m_type;

    public:
        //Constructors
        InlineQueryResultGame();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit InlineQueryResultGame(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_game_short_name
         */
        std::string get_game_short_name() const;

        /**
         * @brief Getter.
         * @return m_id
         */
        std::string get_id() const;

        /**
         * @brief Getter.
         * @return m_reply_markup
         */
        std::optional<InlineKeyboardMarkup::ptr> get_reply_markup() const;

        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        //Setter
        /**
         * @brief Setter for m_game_short_name.
         * @param[in] game_short_name See the member variable.
         */
        void set_game_short_name(const std::string &game_short_name);

        /**
         * @brief Setter for m_id.
         * @param[in] id See the member variable.
         */
        void set_id(const std::string &id);

        /**
         * @brief Setter for m_reply_markup.
         * @param[in] reply_markup See the member variable.
         */
        void set_reply_markup(const std::optional<InlineKeyboardMarkup::ptr> &reply_markup);

        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);
    };
} //namespace tgbot

#endif
