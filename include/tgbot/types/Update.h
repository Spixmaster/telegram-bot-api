#ifndef TGBOT_TYPES_UPDATE_H
#define TGBOT_TYPES_UPDATE_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"
#include "tgbot/types/CallbackQuery.h"
#include "tgbot/types/ChatJoinRequest.h"
#include "tgbot/types/ChatMemberUpdated.h"
#include "tgbot/types/ChosenInlineResult.h"
#include "tgbot/types/InlineQuery.h"
#include "tgbot/types/Message.h"
#include "tgbot/types/Poll.h"
#include "tgbot/types/PollAnswer.h"
#include "tgbot/types/PreCheckoutQuery.h"
#include "tgbot/types/ShippingQuery.h"

namespace tgbot
{
    /**
     * @class Update
     * @brief This object represents an incoming update.
     * @note At most one of the optional parameters can be present in any given update.
     */
    class Update : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<Update>;

    private:
        //Member variables
        /**
         * @var m_callback_query
         * @brief Optional. New incoming callback query.
         */
        std::optional<CallbackQuery::ptr> m_callback_query;
        /**
         * @var m_channel_post
         * @brief Optional. New incoming channel post of any kind - text, photo, sticker, etc..
         */
        std::optional<Message::ptr> m_channel_post;
        /**
         * @var m_chat_join_request
         * @brief Optional. A request to join the chat has been sent.
         * @details The bot must have the can_invite_users administrator right in the chat to receive these updates.
         */
        std::optional<ChatJoinRequest::ptr> m_chat_join_request;
        /**
         * @var m_chat_member
         * @brief Optional. A chat member's status was updated in a chat.
         * @details The bot must be an administrator in the chat and must explicitly specify “chat_member” in the list of allowed_updates to receive these updates.
         */
        std::optional<ChatMemberUpdated::ptr> m_chat_member;
        /**
         * @var m_chosen_inline_result
         * @brief Optional. The result of an inline query that was chosen by a user and sent to their chat partner.
         * @details Please see our documentation on the feedback collecting for details on how to enable these updates for your bot.
         */
        std::optional<ChosenInlineResult::ptr> m_chosen_inline_result;
        /**
         * @var m_edited_channel_post
         * @brief Optional. New version of a channel post that is known to the bot and was edited.
         */
        std::optional<Message::ptr> m_edited_channel_post;
        /**
         * @var m_edited_message
         * @brief Optional. New version of a message that is known to the bot and was edited.
         */
        std::optional<Message::ptr> m_edited_message;
        /**
         * @var m_inline_query
         * @brief Optional. New incoming inline query.
         */
        std::optional<InlineQuery::ptr> m_inline_query;
        /**
         * @var m_message
         * @brief Optional. New incoming message of any kind - text, photo, sticker, etc..
         */
        std::optional<Message::ptr> m_message;
        /**
         * @var m_my_chat_member
         * @brief Optional. The bot's chat member status was updated in a chat.
         * @details For private chats, this update is received only when the bot is blocked or unblocked by the user.
         */
        std::optional<ChatMemberUpdated::ptr> m_my_chat_member;
        /**
         * @var m_poll
         * @brief Optional. New poll state.
         * @details Bots receive only updates about stopped polls and polls, which are sent by the bot.
         */
        std::optional<Poll::ptr> m_poll;
        /**
         * @var m_poll_answer
         * @brief Optional. A user changed their answer in a non-anonymous poll.
         * @details Bots receive new votes only in polls that were sent by the bot itself.
         */
        std::optional<PollAnswer::ptr> m_poll_answer;
        /**
         * @var m_pre_checkout_query
         * @brief Optional. New incoming pre-checkout query.
         * @details Contains full information about checkout.
         */
        std::optional<PreCheckoutQuery::ptr> m_pre_checkout_query;
        /**
         * @var m_shipping_query
         * @brief Optional. New incoming shipping query.
         * @details Only for invoices with flexible price.
         */
        std::optional<ShippingQuery::ptr> m_shipping_query;
        /**
         * @var m_update_id
         * @brief The update's unique identifier.
         * @details Update identifiers start from a certain positive number and increase sequentially.
         * @details This ID becomes especially handy if you're using webhooks, since it allows you to ignore repeated updates or to restore the correct update sequence, should they get out of order.
         * @details If there are no new updates for at least a week, then identifier of the next update will be chosen randomly instead of sequentially.
         */
        std::int32_t m_update_id;

    public:
        //Constructors
        Update();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit Update(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_callback_query
         */
        std::optional<CallbackQuery::ptr> get_callback_query() const;

        /**
         * @brief Getter.
         * @return m_channel_post
         */
        std::optional<Message::ptr> get_channel_post() const;

        /**
         * @brief Getter.
         * @return m_chat_join_request
         */
        std::optional<ChatJoinRequest::ptr> get_chat_join_request() const;

        /**
         * @brief Getter.
         * @return m_chat_member
         */
        std::optional<ChatMemberUpdated::ptr> get_chat_member() const;

        /**
         * @brief Getter.
         * @return m_chosen_inline_result
         */
        std::optional<ChosenInlineResult::ptr> get_chosen_inline_result() const;

        /**
         * @brief Getter.
         * @return m_edited_channel_post
         */
        std::optional<Message::ptr> get_edited_channel_post() const;

        /**
         * @brief Getter.
         * @return m_edited_message
         */
        std::optional<Message::ptr> get_edited_message() const;

        /**
         * @brief Getter.
         * @return m_inline_query
         */
        std::optional<InlineQuery::ptr> get_inline_query() const;

        /**
         * @brief Getter.
         * @return m_message
         */
        std::optional<Message::ptr> get_message() const;

        /**
         * @brief Getter.
         * @return m_my_chat_member
         */
        std::optional<ChatMemberUpdated::ptr> get_my_chat_member() const;

        /**
         * @brief Getter.
         * @return m_poll
         */
        std::optional<Poll::ptr> get_poll() const;

        /**
         * @brief Getter.
         * @return m_poll_answer
         */
        std::optional<PollAnswer::ptr> get_poll_answer() const;

        /**
         * @brief Getter.
         * @return m_pre_checkout_query
         */
        std::optional<PreCheckoutQuery::ptr> get_pre_checkout_query() const;

        /**
         * @brief Getter.
         * @return m_shipping_query
         */
        std::optional<ShippingQuery::ptr> get_shipping_query() const;

        /**
         * @brief Getter.
         * @return m_update_id
         */
        std::int32_t get_update_id() const;

        //Setter
        /**
         * @brief Setter for m_callback_query.
         * @param[in] callback_query See the member variable.
         */
        void set_callback_query(const std::optional<CallbackQuery::ptr> &callback_query);

        /**
         * @brief Setter for m_channel_post.
         * @param[in] channel_post See the member variable.
         */
        void set_channel_post(const std::optional<Message::ptr> &channel_post);

        /**
         * @brief Setter for m_chat_join_request.
         * @param[in] chat_join_request See the member variable.
         */
        void set_chat_join_request(const std::optional<ChatJoinRequest::ptr> &chat_join_request);

        /**
         * @brief Setter for m_chat_member.
         * @param[in] chat_member See the member variable.
         */
        void set_chat_member(const std::optional<ChatMemberUpdated::ptr> &chat_member);

        /**
         * @brief Setter for m_chosen_inline_result.
         * @param[in] chosen_inline_result See the member variable.
         */
        void set_chosen_inline_result(const std::optional<ChosenInlineResult::ptr> &chosen_inline_result);

        /**
         * @brief Setter for m_edited_channel_post.
         * @param[in] edited_channel_post See the member variable.
         */
        void set_edited_channel_post(const std::optional<Message::ptr> &edited_channel_post);

        /**
         * @brief Setter for m_edited_message.
         * @param[in] edited_message See the member variable.
         */
        void set_edited_message(const std::optional<Message::ptr> &edited_message);

        /**
         * @brief Setter for m_inline_query.
         * @param[in] inline_query See the member variable.
         */
        void set_inline_query(const std::optional<InlineQuery::ptr> &inline_query);

        /**
         * @brief Setter for m_message.
         * @param[in] message See the member variable.
         */
        void set_message(const std::optional<Message::ptr> &message);

        /**
         * @brief Setter for m_my_chat_member.
         * @param[in] my_chat_member See the member variable.
         */
        void set_my_chat_member(const std::optional<ChatMemberUpdated::ptr> &my_chat_member);

        /**
         * @brief Setter for m_poll.
         * @param[in] poll See the member variable.
         */
        void set_poll(const std::optional<Poll::ptr> &poll);

        /**
         * @brief Setter for m_poll_answer.
         * @param[in] poll_answer See the member variable.
         */
        void set_poll_answer(const std::optional<PollAnswer::ptr> &poll_answer);

        /**
         * @brief Setter for m_pre_checkout_query.
         * @param[in] pre_checkout_query See the member variable.
         */
        void set_pre_checkout_query(const std::optional<PreCheckoutQuery::ptr> &pre_checkout_query);

        /**
         * @brief Setter for m_shipping_query.
         * @param[in] shipping_query See the member variable.
         */
        void set_shipping_query(const std::optional<ShippingQuery::ptr> &shipping_query);

        /**
         * @brief Setter for m_update_id.
         * @param[in] update_id See the member variable.
         */
        void set_update_id(const std::int32_t &update_id);
    };
} //namespace tgbot

#endif
