#ifndef TGBOT_TYPES_BOTCOMMAND_H
#define TGBOT_TYPES_BOTCOMMAND_H

#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class BotCommand
     * @brief This object represents a bot command.
     */
    class BotCommand : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<BotCommand>;

    private:
        //Member variables
        /**
         * @var m_command
         * @brief Text of the command, 1-32 characters.
         * @details Can contain only lowercase English letters, digits and underscores.
         */
        std::string m_command;
        /**
         * @var m_description
         * @brief Description of the command, 3-256 characters.
         */
        std::string m_description;

    public:
        //Constructors
        BotCommand();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit BotCommand(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_command
         */
        std::string get_command() const;

        /**
         * @brief Getter.
         * @return m_description
         */
        std::string get_description() const;

        //Setter
        /**
         * @brief Setter for m_command.
         * @param[in] command See the member variable.
         */
        void set_command(const std::string &command);

        /**
         * @brief Setter for m_description.
         * @param[in] description See the member variable.
         */
        void set_description(const std::string &description);
    };
} //namespace tgbot

#endif
