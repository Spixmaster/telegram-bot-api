#ifndef TGBOT_TYPES_CHATADMINISTRATORRIGHTS_H
#define TGBOT_TYPES_CHATADMINISTRATORRIGHTS_H

#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class ChatAdministratorRights
     * @brief Represents the rights of an administrator in a chat.
     */
    class ChatAdministratorRights : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<ChatAdministratorRights>;

    private:
        //Member variables
        /**
         * @var m_can_change_info
         * @brief True, if the user is allowed to change the chat title, photo and other settings.
         */
        bool m_can_change_info;
        /**
         * @var m_can_delete_messages
         * @brief True, if the administrator can delete messages of other users.
         */
        bool m_can_delete_messages;
        /**
         * @var m_can_edit_messages
         * @brief Optional. True, if the administrator can edit messages of other users and can pin messages; channels only.
         */
        std::optional<bool> m_can_edit_messages;
        /**
         * @var m_can_invite_users
         * @brief True, if the user is allowed to invite new users to the chat.
         */
        bool m_can_invite_users;
        /**
         * @var m_can_manage_chat
         * @brief True, if the administrator can access the chat event log, chat statistics, message statistics in channels, see channel members, see anonymous administrators in supergroups and ignore slow mode.
         * @details Implied by any other administrator privilege.
         */
        bool m_can_manage_chat;
        /**
         * @var m_can_manage_topics
         * @brief Optional. True, if the user is allowed to create, rename, close, and reopen forum topics; supergroups only.
         */
        std::optional<bool> m_can_manage_topics;
        /**
         * @var m_can_manage_video_chats
         * @brief True, if the administrator can manage video chats.
         */
        bool m_can_manage_video_chats;
        /**
         * @var m_can_pin_messages
         * @brief Optional. True, if the user is allowed to pin messages; groups and supergroups only.
         */
        std::optional<bool> m_can_pin_messages;
        /**
         * @var m_can_post_messages
         * @brief Optional. True, if the administrator can post in the channel; channels only.
         */
        std::optional<bool> m_can_post_messages;
        /**
         * @var m_can_promote_members
         * @brief True, if the administrator can add new administrators with a subset of their own privileges or demote administrators that they have promoted, directly or indirectly (promoted by administrators that were appointed by the user).
         */
        bool m_can_promote_members;
        /**
         * @var m_can_restrict_members
         * @brief True, if the administrator can restrict, ban or unban chat members.
         */
        bool m_can_restrict_members;
        /**
         * @var m_is_anonymous
         * @brief True, if the user's presence in the chat is hidden.
         */
        bool m_is_anonymous;

    public:
        //Constructors
        ChatAdministratorRights();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit ChatAdministratorRights(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_can_change_info
         */
        bool get_can_change_info() const;

        /**
         * @brief Getter.
         * @return m_can_delete_messages
         */
        bool get_can_delete_messages() const;

        /**
         * @brief Getter.
         * @return m_can_edit_messages
         */
        std::optional<bool> get_can_edit_messages() const;

        /**
         * @brief Getter.
         * @return m_can_invite_users
         */
        bool get_can_invite_users() const;

        /**
         * @brief Getter.
         * @return m_can_manage_chat
         */
        bool get_can_manage_chat() const;

        /**
         * @brief Getter.
         * @return m_can_manage_topics
         */
        std::optional<bool> get_can_manage_topics() const;

        /**
         * @brief Getter.
         * @return m_can_manage_video_chats
         */
        bool get_can_manage_video_chats() const;

        /**
         * @brief Getter.
         * @return m_can_pin_messages
         */
        std::optional<bool> get_can_pin_messages() const;

        /**
         * @brief Getter.
         * @return m_can_post_messages
         */
        std::optional<bool> get_can_post_messages() const;

        /**
         * @brief Getter.
         * @return m_can_promote_members
         */
        bool get_can_promote_members() const;

        /**
         * @brief Getter.
         * @return m_can_restrict_members
         */
        bool get_can_restrict_members() const;

        /**
         * @brief Getter.
         * @return m_is_anonymous
         */
        bool get_is_anonymous() const;

        //Setter
        /**
         * @brief Setter for m_can_change_info.
         * @param[in] can_change_info See the member variable.
         */
        void set_can_change_info(const bool &can_change_info);

        /**
         * @brief Setter for m_can_delete_messages.
         * @param[in] can_delete_messages See the member variable.
         */
        void set_can_delete_messages(const bool &can_delete_messages);

        /**
         * @brief Setter for m_can_edit_messages.
         * @param[in] can_edit_messages See the member variable.
         */
        void set_can_edit_messages(const std::optional<bool> &can_edit_messages);

        /**
         * @brief Setter for m_can_invite_users.
         * @param[in] can_invite_users See the member variable.
         */
        void set_can_invite_users(const bool &can_invite_users);

        /**
         * @brief Setter for m_can_manage_chat.
         * @param[in] can_manage_chat See the member variable.
         */
        void set_can_manage_chat(const bool &can_manage_chat);

        /**
         * @brief Setter for m_can_manage_topics.
         * @param[in] can_manage_topics See the member variable.
         */
        void set_can_manage_topics(const std::optional<bool> &can_manage_topics);

        /**
         * @brief Setter for m_can_manage_video_chats.
         * @param[in] can_manage_video_chats See the member variable.
         */
        void set_can_manage_video_chats(const bool &can_manage_video_chats);

        /**
         * @brief Setter for m_can_pin_messages.
         * @param[in] can_pin_messages See the member variable.
         */
        void set_can_pin_messages(const std::optional<bool> &can_pin_messages);

        /**
         * @brief Setter for m_can_post_messages.
         * @param[in] can_post_messages See the member variable.
         */
        void set_can_post_messages(const std::optional<bool> &can_post_messages);

        /**
         * @brief Setter for m_can_promote_members.
         * @param[in] can_promote_members See the member variable.
         */
        void set_can_promote_members(const bool &can_promote_members);

        /**
         * @brief Setter for m_can_restrict_members.
         * @param[in] can_restrict_members See the member variable.
         */
        void set_can_restrict_members(const bool &can_restrict_members);

        /**
         * @brief Setter for m_is_anonymous.
         * @param[in] is_anonymous See the member variable.
         */
        void set_is_anonymous(const bool &is_anonymous);
    };
} //namespace tgbot

#endif
