#ifndef TGBOT_TYPES_PASSPORTELEMENTERRORUNSPECIFIED_H
#define TGBOT_TYPES_PASSPORTELEMENTERRORUNSPECIFIED_H

#include <memory>
#include <string>

#include "PassportElementError.h"

namespace tgbot
{
    /**
     * @class PassportElementErrorUnspecified
     * @brief Represents an issue in an unspecified place.
     * @details The error is considered resolved when new data is added.
     */
    class PassportElementErrorUnspecified : public PassportElementError
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<PassportElementErrorUnspecified>;

    private:
        //Member variables
        /**
         * @var m_element_hash
         * @brief Base64-encoded element hash.
         */
        std::string m_element_hash;
        /**
         * @var m_message
         * @brief Error message.
         */
        std::string m_message;
        /**
         * @var m_source
         * @brief Error source, must be unspecified.
         */
        std::string m_source;
        /**
         * @var m_type
         * @brief Type of element of the user's Telegram Passport which has the issue.
         */
        std::string m_type;

    public:
        //Constructors
        PassportElementErrorUnspecified();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit PassportElementErrorUnspecified(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_element_hash
         */
        std::string get_element_hash() const;

        /**
         * @brief Getter.
         * @return m_message
         */
        std::string get_message() const;

        /**
         * @brief Getter.
         * @return m_source
         */
        std::string get_source() const;

        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        //Setter
        /**
         * @brief Setter for m_element_hash.
         * @param[in] element_hash See the member variable.
         */
        void set_element_hash(const std::string &element_hash);

        /**
         * @brief Setter for m_message.
         * @param[in] message See the member variable.
         */
        void set_message(const std::string &message);

        /**
         * @brief Setter for m_source.
         * @param[in] source See the member variable.
         */
        void set_source(const std::string &source);

        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);
    };
} //namespace tgbot

#endif
