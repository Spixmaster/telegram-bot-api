#ifndef TGBOT_TYPES_CHATMEMBERMEMBER_H
#define TGBOT_TYPES_CHATMEMBERMEMBER_H

#include <memory>
#include <string>

#include "tgbot/types/ChatMember.h"
#include "tgbot/types/User.h"

namespace tgbot
{
    /**
     * @class ChatMemberMember
     * @brief Represents a chat member that has no additional privileges or restrictions.
     */
    class ChatMemberMember : public ChatMember
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<ChatMemberMember>;

    private:
        //Member variables
        /**
         * @var m_status
         * @brief The member's status in the chat, always “member”.
         */
        std::string m_status;
        /**
         * @var m_user
         * @brief Information about the user.
         */
        User::ptr m_user;

    public:
        //Constructors
        ChatMemberMember();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit ChatMemberMember(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_status
         */
        std::string get_status() const;

        /**
         * @brief Getter.
         * @return m_user
         */
        User::ptr get_user() const;

        //Setter
        /**
         * @brief Setter for m_status.
         * @param[in] status See the member variable.
         */
        void set_status(const std::string &status);

        /**
         * @brief Setter for m_user.
         * @param[in] user See the member variable.
         */
        void set_user(const User::ptr &user);
    };
} //namespace tgbot

#endif
