#ifndef TGBOT_TYPES_REPLYKEYBOARDREMOVE_H
#define TGBOT_TYPES_REPLYKEYBOARDREMOVE_H

#include <memory>
#include <optional>
#include <string>

#include "tgbot/types/Reply.h"

namespace tgbot
{
    /**
     * @class ReplyKeyboardRemove
     * @brief Upon receiving a message with this object, Telegram clients will remove the current custom keyboard and display the default letter-keyboard.
     * @details By default, custom keyboards are displayed until a new keyboard is sent by a bot.
     * @details An exception is made for one-time keyboards that are hidden immediately after the user presses a button (see ReplyKeyboardMarkup).
     */
    class ReplyKeyboardRemove : public Reply
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<ReplyKeyboardRemove>;

    private:
        //Member variables
        /**
         * @var m_remove_keyboard
         * @brief Requests clients to remove the custom keyboard (user will not be able to summon this keyboard; if you want to hide the keyboard from sight but keep it accessible, use one_time_keyboard in ReplyKeyboardMarkup).
         */
        bool m_remove_keyboard;
        /**
         * @var m_selective
         * @brief Optional. Use this parameter if you want to remove the keyboard for specific users only.
         * @details Targets: 1) users that are \@mentioned in the text of the Message object; 2) if the bot's message is a reply (has reply_to_message_id), sender of the original message.
         * @details Example: A user votes in a poll, bot returns confirmation message in reply to the vote and removes the keyboard for that user, while still showing the keyboard with poll options to users who haven't voted yet.
         */
        std::optional<bool> m_selective;

    public:
        //Constructors
        ReplyKeyboardRemove();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit ReplyKeyboardRemove(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_remove_keyboard
         */
        bool get_remove_keyboard() const;

        /**
         * @brief Getter.
         * @return m_selective
         */
        std::optional<bool> get_selective() const;

        //Setter
        /**
         * @brief Setter for m_remove_keyboard.
         * @param[in] remove_keyboard See the member variable.
         */
        void set_remove_keyboard(const bool &remove_keyboard);

        /**
         * @brief Setter for m_selective.
         * @param[in] selective See the member variable.
         */
        void set_selective(const std::optional<bool> &selective);
    };
} //namespace tgbot

#endif
