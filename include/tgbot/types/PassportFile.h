#ifndef TGBOT_TYPES_PASSPORTFILE_H
#define TGBOT_TYPES_PASSPORTFILE_H

#include <cstdint>
#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class PassportFile
     * @brief This object represents a file uploaded to Telegram Passport.
     * @details Currently all Telegram Passport files are in JPEG format when decrypted and don't exceed 10MB.
     */
    class PassportFile : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<PassportFile>;

    private:
        //Member variables
        /**
         * @var m_file_date
         * @brief Unix time when the file was uploaded.
         */
        std::uint64_t m_file_date;
        /**
         * @var m_file_id
         * @brief Identifier for this file, which can be used to download or reuse the file.
         */
        std::string m_file_id;
        /**
         * @var m_file_size
         * @brief File size in bytes.
         */
        std::int32_t m_file_size;
        /**
         * @var m_file_unique_id
         * @brief Unique identifier for this file, which is supposed to be the same over time and for different bots.
         * @details Can't be used to download or reuse the file.
         */
        std::string m_file_unique_id;

    public:
        //Constructors
        PassportFile();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit PassportFile(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_file_date
         */
        std::uint64_t get_file_date() const;

        /**
         * @brief Getter.
         * @return m_file_id
         */
        std::string get_file_id() const;

        /**
         * @brief Getter.
         * @return m_file_size
         */
        std::int32_t get_file_size() const;

        /**
         * @brief Getter.
         * @return m_file_unique_id
         */
        std::string get_file_unique_id() const;

        //Setter
        /**
         * @brief Setter for m_file_date.
         * @param[in] file_date See the member variable.
         */
        void set_file_date(const std::uint64_t &file_date);

        /**
         * @brief Setter for m_file_id.
         * @param[in] file_id See the member variable.
         */
        void set_file_id(const std::string &file_id);

        /**
         * @brief Setter for m_file_size.
         * @param[in] file_size See the member variable.
         */
        void set_file_size(const std::int32_t &file_size);

        /**
         * @brief Setter for m_file_unique_id.
         * @param[in] file_unique_id See the member variable.
         */
        void set_file_unique_id(const std::string &file_unique_id);
    };
} //namespace tgbot

#endif
