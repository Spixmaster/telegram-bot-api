#ifndef TGBOT_TYPES_WRITEACCESSALLOWED_H
#define TGBOT_TYPES_WRITEACCESSALLOWED_H

#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class WriteAccessAllowed
     * @brief This object represents a service message about a user allowing a bot added to the attachment menu to write messages.
     * @details Currently holds no information.
     */
    class WriteAccessAllowed : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<WriteAccessAllowed>;

        //Constructors
        WriteAccessAllowed();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit WriteAccessAllowed(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;
    };
} //namespace tgbot

#endif
