#ifndef TGBOT_TYPES_BOTCOMMANDSCOPECHAT_H
#define TGBOT_TYPES_BOTCOMMANDSCOPECHAT_H

#include <cstdint>
#include <memory>
#include <string>
#include <variant>

#include "tgbot/types/BotCommandScope.h"

namespace tgbot
{
    /**
     * @class BotCommandScopeChat
     * @brief Represents the scope of bot commands, covering a specific chat.
     */
    class BotCommandScopeChat : public BotCommandScope
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<BotCommandScopeChat>;

    private:
        //Member variables
        /**
         * @var m_chat_id
         * @brief Unique identifier for the target chat or username of the target supergroup (in the format \@supergroupusername).
         */
        std::variant<std::int64_t, std::string> m_chat_id;
        /**
         * @var m_type
         * @brief Scope type, must be chat.
         */
        std::string m_type;

    public:
        //Constructors
        BotCommandScopeChat();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit BotCommandScopeChat(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_chat_id
         */
        std::variant<std::int64_t, std::string> get_chat_id() const;

        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        //Setter
        /**
         * @brief Setter for m_chat_id.
         * @param[in] chat_id See the member variable.
         */
        void set_chat_id(const std::variant<std::int64_t, std::string> &chat_id);

        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);
    };
} //namespace tgbot

#endif
