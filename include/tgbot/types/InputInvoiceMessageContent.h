#ifndef TGBOT_TYPES_INPUTINVOICEMESSAGECONTENT_H
#define TGBOT_TYPES_INPUTINVOICEMESSAGECONTENT_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>
#include <vector>

#include "tgbot/types/InputMessageContent.h"
#include "tgbot/types/LabeledPrice.h"

namespace tgbot
{
    /**
     * @class InputInvoiceMessageContent
     * @brief Represents the content of an invoice message to be sent as the result of an inline query.
     */
    class InputInvoiceMessageContent : public InputMessageContent
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<InputInvoiceMessageContent>;

    private:
        //Member variables
        /**
         * @var m_currency
         * @brief Three-letter ISO 4217 currency code, see more on currencies.
         */
        std::string m_currency;
        /**
         * @var m_description
         * @brief Product description, 1-255 characters.
         */
        std::string m_description;
        /**
         * @var m_is_flexible
         * @brief Optional. Pass True if the final price depends on the shipping method.
         */
        std::optional<bool> m_is_flexible;
        /**
         * @var m_max_tip_amount
         * @brief Optional. The maximum accepted amount for tips in the smallest units of the currency (integer, not float/double).
         * @details For example, for a maximum tip of US$ 1.45 pass max_tip_amount = 145.
         * @details See the exp parameter in currencies.json, it shows the number of digits past the decimal point for each currency (2 for the majority of currencies).
         * @details Defaults to 0.
         */
        std::optional<std::int32_t> m_max_tip_amount;
        /**
         * @var m_need_email
         * @brief Optional. Pass True if you require the user's email address to complete the order.
         */
        std::optional<bool> m_need_email;
        /**
         * @var m_need_name
         * @brief Optional. Pass True if you require the user's full name to complete the order.
         */
        std::optional<bool> m_need_name;
        /**
         * @var m_need_phone_number
         * @brief Optional. Pass True if you require the user's phone number to complete the order.
         */
        std::optional<bool> m_need_phone_number;
        /**
         * @var m_need_shipping_address
         * @brief Optional. Pass True if you require the user's shipping address to complete the order.
         */
        std::optional<bool> m_need_shipping_address;
        /**
         * @var m_payload
         * @brief Bot-defined invoice payload, 1-128 bytes.
         * @details This will not be displayed to the user, use for your internal processes.
         */
        std::string m_payload;
        /**
         * @var m_photo_height
         * @brief Optional. Photo height.
         */
        std::optional<std::int32_t> m_photo_height;
        /**
         * @var m_photo_size
         * @brief Optional. Photo size in bytes.
         */
        std::optional<std::int32_t> m_photo_size;
        /**
         * @var m_photo_url
         * @brief Optional. URL of the product photo for the invoice.
         * @details Can be a photo of the goods or a marketing image for a service.
         */
        std::optional<std::string> m_photo_url;
        /**
         * @var m_photo_width
         * @brief Optional. Photo width.
         */
        std::optional<std::int32_t> m_photo_width;
        /**
         * @var m_prices
         * @brief Price breakdown, a JSON-serialized list of components (e.g. product price, tax, discount, delivery cost, delivery tax, bonus, etc.).
         */
        std::vector<LabeledPrice::ptr> m_prices;
        /**
         * @var m_provider_data
         * @brief Optional. A JSON-serialized object for data about the invoice, which will be shared with the payment provider.
         * @details A detailed description of the required fields should be provided by the payment provider.
         */
        std::optional<std::string> m_provider_data;
        /**
         * @var m_provider_token
         * @brief Payment provider token, obtained via \@BotFather.
         */
        std::string m_provider_token;
        /**
         * @var m_send_email_to_provider
         * @brief Optional. Pass True if the user's email address should be sent to provider.
         */
        std::optional<bool> m_send_email_to_provider;
        /**
         * @var m_send_phone_number_to_provider
         * @brief Optional. Pass True if the user's phone number should be sent to provider.
         */
        std::optional<bool> m_send_phone_number_to_provider;
        /**
         * @var m_suggested_tip_amounts
         * @brief Optional. A JSON-serialized array of suggested amounts of tip in the smallest units of the currency (integer, not float/double).
         * @details At most 4 suggested tip amounts can be specified.
         * @details The suggested tip amounts must be positive, passed in a strictly increased order and must not exceed max_tip_amount.
         */
        std::optional<std::vector<std::int32_t>> m_suggested_tip_amounts;
        /**
         * @var m_title
         * @brief Product name, 1-32 characters.
         */
        std::string m_title;

    public:
        //Constructors
        InputInvoiceMessageContent();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit InputInvoiceMessageContent(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_currency
         */
        std::string get_currency() const;

        /**
         * @brief Getter.
         * @return m_description
         */
        std::string get_description() const;

        /**
         * @brief Getter.
         * @return m_is_flexible
         */
        std::optional<bool> get_is_flexible() const;

        /**
         * @brief Getter.
         * @return m_max_tip_amount
         */
        std::optional<std::int32_t> get_max_tip_amount() const;

        /**
         * @brief Getter.
         * @return m_need_email
         */
        std::optional<bool> get_need_email() const;

        /**
         * @brief Getter.
         * @return m_need_name
         */
        std::optional<bool> get_need_name() const;

        /**
         * @brief Getter.
         * @return m_need_phone_number
         */
        std::optional<bool> get_need_phone_number() const;

        /**
         * @brief Getter.
         * @return m_need_shipping_address
         */
        std::optional<bool> get_need_shipping_address() const;

        /**
         * @brief Getter.
         * @return m_payload
         */
        std::string get_payload() const;

        /**
         * @brief Getter.
         * @return m_photo_height
         */
        std::optional<std::int32_t> get_photo_height() const;

        /**
         * @brief Getter.
         * @return m_photo_size
         */
        std::optional<std::int32_t> get_photo_size() const;

        /**
         * @brief Getter.
         * @return m_photo_url
         */
        std::optional<std::string> get_photo_url() const;

        /**
         * @brief Getter.
         * @return m_photo_width
         */
        std::optional<std::int32_t> get_photo_width() const;

        /**
         * @brief Getter.
         * @return m_prices
         */
        std::vector<LabeledPrice::ptr> get_prices() const;

        /**
         * @brief Getter.
         * @return m_provider_data
         */
        std::optional<std::string> get_provider_data() const;

        /**
         * @brief Getter.
         * @return m_provider_token
         */
        std::string get_provider_token() const;

        /**
         * @brief Getter.
         * @return m_send_email_to_provider
         */
        std::optional<bool> get_send_email_to_provider() const;

        /**
         * @brief Getter.
         * @return m_send_phone_number_to_provider
         */
        std::optional<bool> get_send_phone_number_to_provider() const;

        /**
         * @brief Getter.
         * @return m_suggested_tip_amounts
         */
        std::optional<std::vector<std::int32_t>> get_suggested_tip_amounts() const;

        /**
         * @brief Getter.
         * @return m_title
         */
        std::string get_title() const;

        //Setter
        /**
         * @brief Setter for m_currency.
         * @param[in] currency See the member variable.
         */
        void set_currency(const std::string &currency);

        /**
         * @brief Setter for m_description.
         * @param[in] description See the member variable.
         */
        void set_description(const std::string &description);

        /**
         * @brief Setter for m_is_flexible.
         * @param[in] is_flexible See the member variable.
         */
        void set_is_flexible(const std::optional<bool> &is_flexible);
        /**
         * @brief Setter for m_max_tip_amount.
         * @param[in] max_tip_amount See the member variable.
         */
        void set_max_tip_amount(const std::optional<std::int32_t> &max_tip_amount);

        /**
         * @brief Setter for m_need_email.
         * @param[in] need_email See the member variable.
         */
        void set_need_email(const std::optional<bool> &need_email);

        /**
         * @brief Setter for m_need_name.
         * @param[in] need_name See the member variable.
         */
        void set_need_name(const std::optional<bool> &need_name);

        /**
         * @brief Setter for m_need_phone_number.
         * @param[in] need_phone_number See the member variable.
         */
        void set_need_phone_number(const std::optional<bool> &need_phone_number);

        /**
         * @brief Setter for m_need_shipping_address.
         * @param[in] need_shipping_address See the member variable.
         */
        void set_need_shipping_address(const std::optional<bool> &need_shipping_address);

        /**
         * @brief Setter for m_payload.
         * @param[in] payload See the member variable.
         */
        void set_payload(const std::string &payload);

        /**
         * @brief Setter for m_photo_height.
         * @param[in] photo_height See the member variable.
         */
        void set_photo_height(const std::optional<std::int32_t> &photo_height);

        /**
         * @brief Setter for m_photo_size.
         * @param[in] photo_size See the member variable.
         */
        void set_photo_size(const std::optional<std::int32_t> &photo_size);

        /**
         * @brief Setter for m_photo_url.
         * @param[in] photo_url See the member variable.
         */
        void set_photo_url(const std::optional<std::string> &photo_url);

        /**
         * @brief Setter for m_photo_width.
         * @param[in] photo_width See the member variable.
         */
        void set_photo_width(const std::optional<std::int32_t> &photo_width);

        /**
         * @brief Setter for m_prices.
         * @param[in] prices See the member variable.
         */
        void set_prices(const std::vector<LabeledPrice::ptr> &prices);

        /**
         * @brief Setter for m_provider_data.
         * @param[in] provider_data See the member variable.
         */
        void set_provider_data(const std::optional<std::string> &provider_data);

        /**
         * @brief Setter for m_provider_token.
         * @param[in] provider_token See the member variable.
         */
        void set_provider_token(const std::string &provider_token);

        /**
         * @brief Setter for m_send_email_to_provider.
         * @param[in] send_email_to_provider See the member variable.
         */
        void set_send_email_to_provider(const std::optional<bool> &send_email_to_provider);

        /**
         * @brief Setter for m_send_phone_number_to_provider.
         * @param[in] send_phone_number_to_provider See the member variable.
         */
        void set_send_phone_number_to_provider(const std::optional<bool> &send_phone_number_to_provider);

        /**
         * @brief Setter for m_suggested_tip_amounts.
         * @param[in] suggested_tip_amounts See the member variable.
         */
        void set_suggested_tip_amounts(const std::optional<std::vector<std::int32_t>> &suggested_tip_amounts);

        /**
         * @brief Setter for m_title.
         * @param[in] title See the member variable.
         */
        void set_title(const std::string &title);
    };
} //namespace tgbot

#endif
