#ifndef TGBOT_TYPES_STICKERSET_H
#define TGBOT_TYPES_STICKERSET_H

#include <memory>
#include <optional>
#include <string>
#include <vector>

#include "tgbot/Type.h"
#include "tgbot/types/PhotoSize.h"
#include "tgbot/types/Sticker.h"

namespace tgbot
{
    /**
     * @class StickerSet
     * @brief This object represents a sticker set.
     */
    class StickerSet : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<StickerSet>;

    private:
        //Member variables
        /**
         * @var m_is_animated
         * @brief True, if the sticker set contains animated stickers.
         */
        bool m_is_animated;
        /**
         * @var m_is_video
         * @brief True, if the sticker set contains video stickers.
         */
        bool m_is_video;
        /**
         * @var m_name
         * @brief Sticker set name.
         */
        std::string m_name;
        /**
         * @var m_sticker_type
         * @brief Type of stickers in the set, currently one of “regular”, “mask”, “custom_emoji”.
         */
        std::string m_sticker_type;
        /**
         * @var m_stickers
         * @brief List of all set stickers.
         */
        std::vector<Sticker::ptr> m_stickers;
        /**
         * @var m_thumbnail
         * @brief Optional. Sticker set thumbnail in the .WEBP, .TGS, or .WEBM format.
         */
        std::optional<PhotoSize::ptr> m_thumbnail;
        /**
         * @var m_title
         * @brief Sticker set title.
         */
        std::string m_title;

    public:
        //Constructors
        StickerSet();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit StickerSet(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_is_animated
         */
        bool get_is_animated() const;

        /**
         * @brief Getter.
         * @return m_is_video
         */
        bool get_is_video() const;

        /**
         * @brief Getter.
         * @return m_name
         */
        std::string get_name() const;

        /**
         * @brief Getter.
         * @return m_sticker_type
         */
        std::string get_sticker_type() const;

        /**
         * @brief Getter.
         * @return m_stickers
         */
        std::vector<Sticker::ptr> get_stickers() const;

        /**
         * @brief Getter.
         * @return m_thumbnail
         */
        std::optional<PhotoSize::ptr> get_thumbnail() const;

        /**
         * @brief Getter.
         * @return m_title
         */
        std::string get_title() const;

        //Setter
        /**
         * @brief Setter for m_is_animated.
         * @param[in] is_animated See the member variable.
         */
        void set_is_animated(const bool &is_animated);

        /**
         * @brief Setter for m_is_video.
         * @param[in] is_video See the member variable.
         */
        void set_is_video(const bool &is_video);

        /**
         * @brief Setter for m_name.
         * @param[in] name See the member variable.
         */
        void set_name(const std::string &name);

        /**
         * @brief Setter for m_sticker_type.
         * @param[in] sticker_type See the member variable.
         */
        void set_sticker_type(const std::string &sticker_type);

        /**
         * @brief Setter for m_stickers.
         * @param[in] stickers See the member variable.
         */
        void set_stickers(const std::vector<Sticker::ptr> &stickers);

        /**
         * @brief Setter for m_thumbnail.
         * @param[in] thumbnail See the member variable.
         */
        void set_thumbnail(const std::optional<PhotoSize::ptr> &thumbnail);

        /**
         * @brief Setter for m_title.
         * @param[in] title See the member variable.
         */
        void set_title(const std::string &title);
    };
} //namespace tgbot

#endif
