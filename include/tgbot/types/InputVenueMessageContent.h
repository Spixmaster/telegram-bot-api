#ifndef TGBOT_TYPES_INPUTVENUEMESSAGECONTENT_H
#define TGBOT_TYPES_INPUTVENUEMESSAGECONTENT_H

#include <memory>
#include <optional>
#include <string>

#include "tgbot/types/InputMessageContent.h"

namespace tgbot
{
    /**
     * @class InputVenueMessageContent
     * @brief Represents the content of a venue message to be sent as the result of an inline query.
     */
    class InputVenueMessageContent : public InputMessageContent
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<InputVenueMessageContent>;

    private:
        //Member variables
        /**
         * @var m_address
         * @brief Address of the venue.
         */
        std::string m_address;
        /**
         * @var m_foursquare_id
         * @brief Optional. Foursquare identifier of the venue, if known.
         */
        std::optional<std::string> m_foursquare_id;
        /**
         * @var m_foursquare_type
         * @brief Optional. Foursquare type of the venue, if known.
         * @details (For example, “arts_entertainment/default”, “arts_entertainment/aquarium” or “food/icecream”.)
         */
        std::optional<std::string> m_foursquare_type;
        /**
         * @var m_google_place_id
         * @brief Optional. Google Places identifier of the venue.
         */
        std::optional<std::string> m_google_place_id;
        /**
         * @var m_google_place_type
         * @brief Optional. Google Places type of the venue.
         * @details (See supported types.)
         */
        std::optional<std::string> m_google_place_type;
        /**
         * @var m_latitude
         * @brief Latitude of the venue in degrees.
         */
        float m_latitude;
        /**
         * @var m_longitude
         * @brief Longitude of the venue in degrees.
         */
        float m_longitude;
        /**
         * @var m_title
         * @brief Name of the venue.
         */
        std::string m_title;

    public:
        //Constructors
        InputVenueMessageContent();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit InputVenueMessageContent(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_address
         */
        std::string get_address() const;

        /**
         * @brief Getter.
         * @return m_foursquare_id
         */
        std::optional<std::string> get_foursquare_id() const;

        /**
         * @brief Getter.
         * @return m_foursquare_type
         */
        std::optional<std::string> get_foursquare_type() const;

        /**
         * @brief Getter.
         * @return m_google_place_id
         */
        std::optional<std::string> get_google_place_id() const;

        /**
         * @brief Getter.
         * @return m_google_place_type
         */
        std::optional<std::string> get_google_place_type() const;

        /**
         * @brief Getter.
         * @return m_latitude
         */
        float get_latitude() const;

        /**
         * @brief Getter.
         * @return m_longitude
         */
        float get_longitude() const;

        /**
         * @brief Getter.
         * @return m_title
         */
        std::string get_title() const;

        //Setter
        /**
         * @brief Setter for m_address.
         * @param[in] address See the member variable.
         */
        void set_address(const std::string &address);

        /**
         * @brief Setter for m_foursquare_id.
         * @param[in] foursquare_id See the member variable.
         */
        void set_foursquare_id(const std::optional<std::string> &foursquare_id);

        /**
         * @brief Setter for m_foursquare_type.
         * @param[in] foursquare_type See the member variable.
         */
        void set_foursquare_type(const std::optional<std::string> &foursquare_type);

        /**
         * @brief Setter for m_google_place_id.
         * @param[in] google_place_id See the member variable.
         */
        void set_google_place_id(const std::optional<std::string> &google_place_id);

        /**
         * @brief Setter for m_google_place_type.
         * @param[in] google_place_type See the member variable.
         */
        void set_google_place_type(const std::optional<std::string> &google_place_type);

        /**
         * @brief Setter for m_latitude.
         * @param[in] latitude See the member variable.
         */
        void set_latitude(const float &latitude);

        /**
         * @brief Setter for m_longitude.
         * @param[in] longitude See the member variable.
         */
        void set_longitude(const float &longitude);

        /**
         * @brief Setter for m_title.
         * @param[in] title See the member variable.
         */
        void set_title(const std::string &title);
    };
} //namespace tgbot

#endif
