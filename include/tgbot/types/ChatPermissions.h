#ifndef TGBOT_TYPES_CHATPERMISSIONS_H
#define TGBOT_TYPES_CHATPERMISSIONS_H

#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class ChatPermissions
     * @brief Describes actions that a non-administrator user is allowed to take in a chat.
     */
    class ChatPermissions : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<ChatPermissions>;

    private:
        //Member variables
        /**
         * @var m_can_add_web_page_previews
         * @brief Optional. True, if the user is allowed to add web page previews to their messages.
         */
        std::optional<bool> m_can_add_web_page_previews;
        /**
         * @var m_can_change_info
         * @brief Optional. True, if the user is allowed to change the chat title, photo and other settings.
         * @details Ignored in public supergroups.
         */
        std::optional<bool> m_can_change_info;
        /**
         * @var m_can_invite_users
         * @brief Optional. True, if the user is allowed to invite new users to the chat.
         */
        std::optional<bool> m_can_invite_users;
        /**
         * @var m_can_manage_topics
         * @brief Optional. True, if the user is allowed to create forum topics.
         * @details If omitted defaults to the value of can_pin_messages.
         */
        std::optional<bool> m_can_manage_topics;
        /**
         * @var m_can_pin_messages
         * @brief Optional. True, if the user is allowed to pin messages.
         * @details Ignored in public supergroups.
         */
        std::optional<bool> m_can_pin_messages;
        /**
         * @var m_can_send_audios
         * @brief Optional. True, if the user is allowed to send audios.
         */
        std::optional<bool> m_can_send_audios;
        /**
         * @var m_can_send_documents
         * @brief Optional. True, if the user is allowed to send documents.
         */
        std::optional<bool> m_can_send_documents;
        /**
         * @var m_can_send_messages
         * @brief Optional. True, if the user is allowed to send text messages, contacts, invoices, locations and venues.
         */
        std::optional<bool> m_can_send_messages;
        /**
         * @var m_can_send_other_messages
         * @brief Optional. True, if the user is allowed to send animations, games, stickers and use inline bots.
         */
        std::optional<bool> m_can_send_other_messages;
        /**
         * @var m_can_send_photos
         * @brief Optional. True, if the user is allowed to send photos.
         */
        std::optional<bool> m_can_send_photos;
        /**
         * @var m_can_send_polls
         * @brief Optional. True, if the user is allowed to send polls.
         */
        std::optional<bool> m_can_send_polls;
        /**
         * @var m_can_send_video_notes
         * @brief Optional. True, if the user is allowed to send video notes.
         */
        std::optional<bool> m_can_send_video_notes;
        /**
         * @var m_can_send_videos
         * @brief Optional. True, if the user is allowed to send videos.
         */
        std::optional<bool> m_can_send_videos;
        /**
         * @var m_can_send_voice_notes
         * @brief Optional. True, if the user is allowed to send voice notes.
         */
        std::optional<bool> m_can_send_voice_notes;

    public:
        //Constructors
        ChatPermissions();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit ChatPermissions(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_can_add_web_page_previews
         */
        std::optional<bool> get_can_add_web_page_previews() const;

        /**
         * @brief Getter.
         * @return m_can_change_info
         */
        std::optional<bool> get_can_change_info() const;

        /**
         * @brief Getter.
         * @return m_can_invite_users
         */
        std::optional<bool> get_can_invite_users() const;

        /**
         * @brief Getter.
         * @return m_can_manage_topics
         */
        std::optional<bool> get_can_manage_topics() const;

        /**
         * @brief Getter.
         * @return m_can_pin_messages
         */
        std::optional<bool> get_can_pin_messages() const;

        /**
         * @brief Getter.
         * @return m_can_send_audios
         */
        std::optional<bool> get_can_send_audios() const;

        /**
         * @brief Getter.
         * @return m_can_send_documents
         */
        std::optional<bool> get_can_send_documents() const;

        /**
         * @brief Getter.
         * @return m_can_send_messages
         */
        std::optional<bool> get_can_send_messages() const;

        /**
         * @brief Getter.
         * @return m_can_send_other_messages
         */
        std::optional<bool> get_can_send_other_messages() const;

        /**
         * @brief Getter.
         * @return m_can_send_photos
         */
        std::optional<bool> get_can_send_photos() const;

        /**
         * @brief Getter.
         * @return m_can_send_polls
         */
        std::optional<bool> get_can_send_polls() const;

        /**
         * @brief Getter.
         * @return m_can_send_video_notes
         */
        std::optional<bool> get_can_send_video_notes() const;

        /**
         * @brief Getter.
         * @return m_can_send_videos
         */
        std::optional<bool> get_can_send_videos() const;

        /**
         * @brief Getter.
         * @return m_can_send_voice_notes
         */
        std::optional<bool> get_can_send_voice_notes() const;

        //Setter
        /**
         * @brief Setter for m_can_add_web_page_previews.
         * @param[in] can_add_web_page_previews See the member variable.
         */
        void set_can_add_web_page_previews(const std::optional<bool> &can_add_web_page_previews);

        /**
         * @brief Setter for m_can_change_info.
         * @param[in] can_change_info See the member variable.
         */
        void set_can_change_info(const std::optional<bool> &can_change_info);

        /**
         * @brief Setter for m_can_invite_users.
         * @param[in] can_invite_users See the member variable.
         */
        void set_can_invite_users(const std::optional<bool> &can_invite_users);

        /**
         * @brief Setter for m_can_manage_topics.
         * @param[in] can_manage_topics See the member variable.
         */
        void set_can_manage_topics(const std::optional<bool> &can_manage_topics);

        /**
         * @brief Setter for m_can_pin_messages.
         * @param[in] can_pin_messages See the member variable.
         */
        void set_can_pin_messages(const std::optional<bool> &can_pin_messages);

        /**
         * @brief Setter for m_can_send_audios.
         * @param[in] can_send_audios See the member variable.
         */
        void set_can_send_audios(const std::optional<bool> &can_send_audios);

        /**
         * @brief Setter for m_can_send_documents.
         * @param[in] can_send_documents See the member variable.
         */
        void set_can_send_documents(const std::optional<bool> &can_send_documents);

        /**
         * @brief Setter for m_can_send_messages.
         * @param[in] can_send_messages See the member variable.
         */
        void set_can_send_messages(const std::optional<bool> &can_send_messages);

        /**
         * @brief Setter for m_can_send_other_messages.
         * @param[in] can_send_other_messages See the member variable.
         */
        void set_can_send_other_messages(const std::optional<bool> &can_send_other_messages);

        /**
         * @brief Setter for m_can_send_photos.
         * @param[in] can_send_photos See the member variable.
         */
        void set_can_send_photos(const std::optional<bool> &can_send_photos);

        /**
         * @brief Setter for m_can_send_polls.
         * @param[in] can_send_polls See the member variable.
         */
        void set_can_send_polls(const std::optional<bool> &can_send_polls);

        /**
         * @brief Setter for m_can_send_video_notes.
         * @param[in] can_send_video_notes See the member variable.
         */
        void set_can_send_video_notes(const std::optional<bool> &can_send_video_notes);

        /**
         * @brief Setter for m_can_send_videos.
         * @param[in] can_send_videos See the member variable.
         */
        void set_can_send_videos(const std::optional<bool> &can_send_videos);

        /**
         * @brief Setter for m_can_send_voice_notes.
         * @param[in] can_send_voice_notes See the member variable.
         */
        void set_can_send_voice_notes(const std::optional<bool> &can_send_voice_notes);
    };
} //namespace tgbot

#endif
