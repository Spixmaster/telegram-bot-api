#ifndef TGBOT_TYPES_KEYBOARDBUTTONREQUESTCHAT_H
#define TGBOT_TYPES_KEYBOARDBUTTONREQUESTCHAT_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"
#include "tgbot/types/ChatAdministratorRights.h"

namespace tgbot
{
    /**
     * @class KeyboardButtonRequestChat
     * @brief This object defines the criteria used to request a suitable chat.
     * @details The identifier of the selected chat will be shared with the bot when the corresponding button is pressed.
     */
    class KeyboardButtonRequestChat : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<KeyboardButtonRequestChat>;

    private:
        //Member variables
        /**
         * @var m_bot_administrator_rights
         * @brief Optional. A JSON-serialized object listing the required administrator rights of the bot in the chat.
         * @details The rights must be a subset of user_administrator_rights.
         * @details If not specified, no additional restrictions are applied.
         */
        std::optional<ChatAdministratorRights::ptr> m_bot_administrator_rights;
        /**
         * @var m_bot_is_member
         * @brief Optional. Pass True to request a chat with the bot as a member.
         * @details Otherwise, no additional restrictions are applied.
         */
        std::optional<bool> m_bot_is_member;
        /**
         * @var m_chat_has_username
         * @brief Optional. Pass True to request a supergroup or a channel with a username, pass False to request a chat without a username.
         * @details If not specified, no additional restrictions are applied.
         */
        std::optional<bool> m_chat_has_username;
        /**
         * @var m_chat_is_channel
         * @brief Pass True to request a channel chat, pass False to request a group or a supergroup chat.
         */
        bool m_chat_is_channel;
        /**
         * @var m_chat_is_created
         * @brief Optional. Pass True to request a chat owned by the user.
         * @details Otherwise, no additional restrictions are applied.
         */
        std::optional<bool> m_chat_is_created;
        /**
         * @var m_chat_is_forum
         * @brief Optional. Pass True to request a forum supergroup, pass False to request a non-forum chat.
         * @details If not specified, no additional restrictions are applied.
         */
        std::optional<bool> m_chat_is_forum;
        /**
         * @var m_request_id
         * @brief Signed 32-bit identifier of the request, which will be received back in the ChatShared object.
         * @details Must be unique within the message.
         */
        std::int32_t m_request_id;
        /**
         * @var m_user_administrator_rights
         * @brief Optional. A JSON-serialized object listing the required administrator rights of the user in the chat.
         * @details The rights must be a superset of bot_administrator_rights.
         * @details If not specified, no additional restrictions are applied.
         */
        std::optional<ChatAdministratorRights::ptr> m_user_administrator_rights;

    public:
        //Constructors
        KeyboardButtonRequestChat();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit KeyboardButtonRequestChat(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_bot_administrator_rights
         */
        std::optional<ChatAdministratorRights::ptr> get_bot_administrator_rights() const;

        /**
         * @brief Getter.
         * @return m_bot_is_member
         */
        std::optional<bool> get_bot_is_member() const;

        /**
         * @brief Getter.
         * @return m_chat_has_username
         */
        std::optional<bool> get_chat_has_username() const;

        /**
         * @brief Getter.
         * @return m_chat_is_channel
         */
        bool get_chat_is_channel() const;

        /**
         * @brief Getter.
         * @return m_chat_is_created
         */
        std::optional<bool> get_chat_is_created() const;

        /**
         * @brief Getter.
         * @return m_chat_is_forum
         */
        std::optional<bool> get_chat_is_forum() const;

        /**
         * @brief Getter.
         * @return m_request_id
         */
        std::int32_t get_request_id() const;

        /**
         * @brief Getter.
         * @return m_user_administrator_rights
         */
        std::optional<ChatAdministratorRights::ptr> get_user_administrator_rights() const;

        //Setter
        /**
         * @brief Setter for m_bot_administrator_rights.
         * @param[in] bot_administrator_rights See the member variable.
         */
        void set_bot_administrator_rights(const std::optional<ChatAdministratorRights::ptr> &bot_administrator_rights);

        /**
         * @brief Setter for m_bot_is_member.
         * @param[in] bot_is_member See the member variable.
         */
        void set_bot_is_member(const std::optional<bool> &bot_is_member);

        /**
         * @brief Setter for m_chat_has_username.
         * @param[in] chat_has_username See the member variable.
         */
        void set_chat_has_username(const std::optional<bool> &chat_has_username);

        /**
         * @brief Setter for m_chat_is_channel.
         * @param[in] chat_is_channel See the member variable.
         */
        void set_chat_is_channel(const bool &chat_is_channel);

        /**
         * @brief Setter for m_chat_is_created.
         * @param[in] chat_is_created See the member variable.
         */
        void set_chat_is_created(const std::optional<bool> &chat_is_created);

        /**
         * @brief Setter for m_chat_is_forum.
         * @param[in] chat_is_forum See the member variable.
         */
        void set_chat_is_forum(const std::optional<bool> &chat_is_forum);

        /**
         * @brief Setter for m_request_id.
         * @param[in] request_id See the member variable.
         */
        void set_request_id(const std::int32_t &request_id);

        /**
         * @brief Setter for m_user_administrator_rights.
         * @param[in] user_administrator_rights See the member variable.
         */
        void set_user_administrator_rights(const std::optional<ChatAdministratorRights::ptr> &user_administrator_rights);
    };
} //namespace tgbot

#endif
