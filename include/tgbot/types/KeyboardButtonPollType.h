#ifndef TGBOT_TYPES_KEYBOARDBUTTONPOLLTYPE_H
#define TGBOT_TYPES_KEYBOARDBUTTONPOLLTYPE_H

#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class KeyboardButtonPollType
     * @brief This object represents type of a poll, which is allowed to be created and sent when the corresponding button is pressed.
     */
    class KeyboardButtonPollType : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<KeyboardButtonPollType>;

    private:
        //Member variables
        /**
         * @var m_type
         * @brief Optional. If quiz is passed, the user will be allowed to create only polls in the quiz mode. If regular is passed, only regular polls will be allowed. Otherwise, the user will be allowed to create a poll of any type.
         */
        std::optional<std::string> m_type;

    public:
        //Constructors
        KeyboardButtonPollType();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit KeyboardButtonPollType(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_type
         */
        std::optional<std::string> get_type() const;

        //Setter
        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::optional<std::string> &type);
    };
} //namespace tgbot

#endif
