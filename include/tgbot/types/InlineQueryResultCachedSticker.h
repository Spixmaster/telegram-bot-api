#ifndef TGBOT_TYPES_INLINEQUERYRESULTCACHEDSTICKER_H
#define TGBOT_TYPES_INLINEQUERYRESULTCACHEDSTICKER_H

#include <memory>
#include <optional>
#include <string>

#include "InlineKeyboardMarkup.h"
#include "InlineQueryResult.h"
#include "InputMessageContent.h"

namespace tgbot
{
    /**
     * @class InlineQueryResultCachedSticker
     * @brief Represents a link to a sticker stored on the Telegram servers.
     * @details By default, this sticker will be sent by the user.
     * @details Alternatively, you can use input_message_content to send a message with the specified content instead of the sticker.
     * @note This will only work in Telegram versions released after 9 April, 2016 for static stickers and after 06 July, 2019 for animated stickers. Older clients will ignore them.
     */
    class InlineQueryResultCachedSticker : public InlineQueryResult
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<InlineQueryResultCachedSticker>;

    private:
        //Member variables
        /**
         * @var m_id
         * @brief Unique identifier for this result, 1-64 bytes.
         */
        std::string m_id;
        /**
         * @var m_input_message_content
         * @brief Optional. Content of the message to be sent instead of the sticker.
         */
        std::optional<InputMessageContent::ptr> m_input_message_content;
        /**
         * @var m_reply_markup
         * @brief Optional. Inline keyboard attached to the message.
         */
        std::optional<InlineKeyboardMarkup::ptr> m_reply_markup;
        /**
         * @var m_sticker_file_id
         * @brief A valid file identifier of the sticker.
         */
        std::string m_sticker_file_id;
        /**
         * @var m_type
         * @brief Type of the result, must be sticker.
         */
        std::string m_type;

    public:
        //Constructors
        InlineQueryResultCachedSticker();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit InlineQueryResultCachedSticker(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_id
         */
        std::string get_id() const;

        /**
         * @brief Getter.
         * @return m_input_message_content
         */
        std::optional<InputMessageContent::ptr> get_input_message_content() const;

        /**
         * @brief Getter.
         * @return m_reply_markup
         */
        std::optional<InlineKeyboardMarkup::ptr> get_reply_markup() const;

        /**
         * @brief Getter.
         * @return m_sticker_file_id
         */
        std::string get_sticker_file_id() const;

        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        //Setter
        /**
         * @brief Setter for m_id.
         * @param[in] id See the member variable.
         */
        void set_id(const std::string &id);

        /**
         * @brief Setter for m_input_message_content.
         * @param[in] input_message_content See the member variable.
         */
        void set_input_message_content(const std::optional<InputMessageContent::ptr> &input_message_content);

        /**
         * @brief Setter for m_reply_markup.
         * @param[in] reply_markup See the member variable.
         */
        void set_reply_markup(const std::optional<InlineKeyboardMarkup::ptr> &reply_markup);

        /**
         * @brief Setter for m_sticker_file_id.
         * @param[in] sticker_file_id See the member variable.
         */
        void set_sticker_file_id(const std::string &sticker_file_id);

        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);
    };
} //namespace tgbot

#endif
