#ifndef TGBOT_TYPES_PASSPORTELEMENTERROR_H
#define TGBOT_TYPES_PASSPORTELEMENTERROR_H

#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class PassportElementError
     * @brief This object represents an error in the Telegram Passport element which was submitted that should be resolved by the user.
     * @details It should be one of:
     * PassportElementErrorDataField
     * PassportElementErrorFrontSide
     * PassportElementErrorReverseSide
     * PassportElementErrorSelfie
     * PassportElementErrorFile
     * PassportElementErrorFiles
     * PassportElementErrorTranslationFile
     * PassportElementErrorTranslationFiles
     * PassportElementErrorUnspecified
     */
    class PassportElementError : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<PassportElementError>;

        //Constructors
        PassportElementError();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit PassportElementError(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;
    };
} //namespace tgbot

#endif
