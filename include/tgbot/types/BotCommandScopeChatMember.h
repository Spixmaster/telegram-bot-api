#ifndef TGBOT_TYPES_BOTCOMMANDSCOPECHATMEMBER_H
#define TGBOT_TYPES_BOTCOMMANDSCOPECHATMEMBER_H

#include <cstdint>
#include <memory>
#include <string>
#include <variant>

#include "tgbot/types/BotCommandScope.h"

namespace tgbot
{
    /**
     * @class BotCommandScopeChatMember
     * @brief Represents the scope of bot commands, covering a specific member of a group or supergroup chat.
     */
    class BotCommandScopeChatMember : public BotCommandScope
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<BotCommandScopeChatMember>;

    private:
        //Member variables
        /**
         * @var m_chat_id
         * @brief Unique identifier for the target chat or username of the target supergroup (in the format \@supergroupusername).
         */
        std::variant<std::int64_t, std::string> m_chat_id;
        /**
         * @var m_type
         * @brief Scope type, must be chat_member.
         */
        std::string m_type;
        /**
         * @var m_user_id
         * @brief Unique identifier of the target user.
         */
        std::int64_t m_user_id;

    public:
        //Constructors
        BotCommandScopeChatMember();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit BotCommandScopeChatMember(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_chat_id
         */
        std::variant<std::int64_t, std::string> get_chat_id() const;

        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        /**
         * @brief Getter.
         * @return m_user_id
         */
        std::int64_t get_user_id() const;

        //Setter
        /**
         * @brief Setter for m_chat_id.
         * @param[in] chat_id See the member variable.
         */
        void set_chat_id(const std::variant<std::int64_t, std::string> &chat_id);

        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);

        /**
         * @brief Setter for m_user_id.
         * @param[in] user_id See the member variable.
         */
        void set_user_id(const std::int64_t &user_id);
    };
} //namespace tgbot

#endif
