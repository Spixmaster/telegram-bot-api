#ifndef TGBOT_TYPES_INLINEQUERYRESULTDOCUMENT_H
#define TGBOT_TYPES_INLINEQUERYRESULTDOCUMENT_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>
#include <vector>

#include "tgbot/types/InlineKeyboardMarkup.h"
#include "tgbot/types/InlineQueryResult.h"
#include "tgbot/types/InputMessageContent.h"
#include "tgbot/types/MessageEntity.h"

namespace tgbot
{
    /**
     * @class InlineQueryResultDocument
     * @brief Represents a link to a file.
     * @details By default, this file will be sent by the user with an optional caption.
     * @details Alternatively, you can use input_message_content to send a message with the specified content instead of the file.
     * @details Currently, only .PDF and .ZIP files can be sent using this method.
     * @note This will only work in Telegram versions released after 9 April, 2016. Older clients will ignore them.
     */
    class InlineQueryResultDocument : public InlineQueryResult
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<InlineQueryResultDocument>;

    private:
        //Member variables
        /**
         * @var m_caption
         * @brief Optional. Caption of the document to be sent, 0-1024 characters after entities parsing.
         */
        std::optional<std::string> m_caption;
        /**
         * @var m_caption_entities
         * @brief Optional. List of special entities that appear in the caption, which can be specified instead of parse_mode.
         */
        std::optional<std::vector<MessageEntity::ptr>> m_caption_entities;
        /**
         * @var m_description
         * @brief Optional. Short description of the result.
         */
        std::optional<std::string> m_description;
        /**
         * @var m_document_url
         * @brief A valid URL for the file.
         */
        std::string m_document_url;
        /**
         * @var m_id
         * @brief Unique identifier for this result, 1-64 bytes.
         */
        std::string m_id;
        /**
         * @var m_input_message_content
         * @brief Optional. Content of the message to be sent instead of the file.
         */
        std::optional<InputMessageContent::ptr> m_input_message_content;
        /**
         * @var m_mime_type
         * @brief MIME type of the content of the file, either “application/pdf” or “application/zip”.
         */
        std::string m_mime_type;
        /**
         * @var m_parse_mode
         * @brief Optional. Mode for parsing entities in the document caption.
         * @details See formatting options for more details.
         */
        std::optional<std::string> m_parse_mode;
        /**
         * @var m_reply_markup
         * @brief Optional. Inline keyboard attached to the message.
         */
        std::optional<InlineKeyboardMarkup::ptr> m_reply_markup;
        /**
         * @var m_thumbnail_height
         * @brief Optional. Thumbnail height.
         */
        std::optional<std::int32_t> m_thumbnail_height;
        /**
         * @var m_thumbnail_url
         * @brief Optional. URL of the thumbnail (JPEG only) for the file.
         */
        std::optional<std::string> m_thumbnail_url;
        /**
         * @var m_thumbnail_width
         * @brief Optional. Thumbnail width.
         */
        std::optional<std::int32_t> m_thumbnail_width;
        /**
         * @var m_title
         * @brief Title for the result.
         */
        std::string m_title;
        /**
         * @var m_type
         * @brief Type of the result, must be document.
         */
        std::string m_type;

    public:
        //Constructors
        InlineQueryResultDocument();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit InlineQueryResultDocument(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_caption
         */
        std::optional<std::string> get_caption() const;

        /**
         * @brief Getter.
         * @return m_caption_entities
         */
        std::optional<std::vector<MessageEntity::ptr>> get_caption_entities() const;

        /**
         * @brief Getter.
         * @return m_description
         */
        std::optional<std::string> get_description() const;

        /**
         * @brief Getter.
         * @return m_document_url
         */
        std::string get_document_url() const;

        /**
         * @brief Getter.
         * @return m_id
         */
        std::string get_id() const;

        /**
         * @brief Getter.
         * @return m_input_message_content
         */
        std::optional<InputMessageContent::ptr> get_input_message_content() const;

        /**
         * @brief Getter.
         * @return m_mime_type
         */
        std::string get_mime_type() const;

        /**
         * @brief Getter.
         * @return m_parse_mode
         */
        std::optional<std::string> get_parse_mode() const;

        /**
         * @brief Getter.
         * @return m_reply_markup
         */
        std::optional<InlineKeyboardMarkup::ptr> get_reply_markup() const;

        /**
         * @brief Getter.
         * @return m_thumbnail_height
         */
        std::optional<std::int32_t> get_thumbnail_height() const;

        /**
         * @brief Getter.
         * @return m_thumbnail_url
         */
        std::optional<std::string> get_thumbnail_url() const;

        /**
         * @brief Getter.
         * @return m_thumbnail_width
         */
        std::optional<std::int32_t> get_thumbnail_width() const;

        /**
         * @brief Getter.
         * @return m_title
         */
        std::string get_title() const;

        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        //Setter
        /**
         * @brief Setter for m_caption.
         * @param[in] caption See the member variable.
         */
        void set_caption(const std::optional<std::string> &caption);

        /**
         * @brief Setter for m_caption_entities.
         * @param[in] caption_entities See the member variable.
         */
        void set_caption_entities(const std::optional<std::vector<MessageEntity::ptr>> &caption_entities);

        /**
         * @brief Setter for m_description.
         * @param[in] description See the member variable.
         */
        void set_description(const std::optional<std::string> &description);

        /**
         * @brief Setter for m_document_url.
         * @param[in] document_url See the member variable.
         */
        void set_document_url(const std::string &document_url);

        /**
         * @brief Setter for m_id.
         * @param[in] id See the member variable.
         */
        void set_id(const std::string &id);

        /**
         * @brief Setter for m_input_message_content.
         * @param[in] input_message_content See the member variable.
         */
        void set_input_message_content(const std::optional<InputMessageContent::ptr> &input_message_content);

        /**
         * @brief Setter for m_mime_type.
         * @param[in] mime_type See the member variable.
         */
        void set_mime_type(const std::string &mime_type);

        /**
         * @brief Setter for m_parse_mode.
         * @param[in] parse_mode See the member variable.
         */
        void set_parse_mode(const std::optional<std::string> &parse_mode);

        /**
         * @brief Setter for m_reply_markup.
         * @param[in] reply_markup See the member variable.
         */
        void set_reply_markup(const std::optional<InlineKeyboardMarkup::ptr> &reply_markup);

        /**
         * @brief Setter for m_thumbnail_height.
         * @param[in] thumbnail_height See the member variable.
         */
        void set_thumbnail_height(const std::optional<std::int32_t> &thumbnail_height);

        /**
         * @brief Setter for m_thumbnail_url.
         * @param[in] thumbnail_url See the member variable.
         */
        void set_thumbnail_url(const std::optional<std::string> &thumbnail_url);

        /**
         * @brief Setter for m_thumbnail_width.
         * @param[in] thumbnail_width See the member variable.
         */
        void set_thumbnail_width(const std::optional<std::int32_t> &thumbnail_width);

        /**
         * @brief Setter for m_title.
         * @param[in] title See the member variable.
         */
        void set_title(const std::string &title);

        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);
    };
} //namespace tgbot

#endif
