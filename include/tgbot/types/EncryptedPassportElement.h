#ifndef TGBOT_TYPES_ENCRYPTEDPASSPORTELEMENT_H
#define TGBOT_TYPES_ENCRYPTEDPASSPORTELEMENT_H

#include <memory>
#include <optional>
#include <string>
#include <vector>

#include "tgbot/Type.h"
#include "tgbot/types/PassportFile.h"

namespace tgbot
{
    /**
     * @class EncryptedPassportElement
     * @brief Describes documents or other Telegram Passport elements shared with the bot by the user.
     */
    class EncryptedPassportElement : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<EncryptedPassportElement>;

    private:
        //Member variables
        /**
         * @var m_data
         * @brief Optional. Base64-encoded encrypted Telegram Passport element data provided by the user, available for “personal_details”, “passport”, “driver_license”, “identity_card”, “internal_passport” and “address” types.
         * @details Can be decrypted and verified using the accompanying EncryptedCredentials.
         */
        std::optional<std::string> m_data;
        /**
         * @var m_email
         * @brief Optional. User's verified email address, available only for “email” type.
         */
        std::optional<std::string> m_email;
        /**
         * @var m_files
         * @brief Optional. Array of encrypted files with documents provided by the user, available for “utility_bill”, “bank_statement”, “rental_agreement”, “passport_registration” and “temporary_registration” types.
         * @details Files can be decrypted and verified using the accompanying EncryptedCredentials.
         */
        std::optional<std::vector<PassportFile::ptr>> m_files;
        /**
         * @var m_front_side
         * @brief Optional. Encrypted file with the front side of the document, provided by the user.
         * @details Available for “passport”, “driver_license”, “identity_card” and “internal_passport”.
         * @details The file can be decrypted and verified using the accompanying EncryptedCredentials.
         */
        std::optional<PassportFile::ptr> m_front_side;
        /**
         * @var m_hash
         * @brief Base64-encoded element hash for using in PassportElementErrorUnspecified.
         */
        std::string m_hash;
        /**
         * @var m_phone_number
         * @brief Optional. User's verified phone number, available only for “phone_number” type.
         */
        std::optional<std::string> m_phone_number;
        /**
         * @var m_reverse_side
         * @brief Optional. Encrypted file with the reverse side of the document, provided by the user.
         * @details Available for “driver_license” and “identity_card”.
         * @details The file can be decrypted and verified using the accompanying EncryptedCredentials.
         */
        std::optional<PassportFile::ptr> m_reverse_side;
        /**
         * @var m_selfie
         * @brief Optional. Encrypted file with the selfie of the user holding a document, provided by the user; available for “passport”, “driver_license”, “identity_card” and “internal_passport”.
         * @details The file can be decrypted and verified using the accompanying EncryptedCredentials.
         */
        std::optional<PassportFile::ptr> m_selfie;
        /**
         * @var m_translation
         * @brief Optional. Array of encrypted files with translated versions of documents provided by the user.
         * @details Available if requested for “passport”, “driver_license”, “identity_card”, “internal_passport”, “utility_bill”, “bank_statement”, “rental_agreement”, “passport_registration” and “temporary_registration” types.
         * @details Files can be decrypted and verified using the accompanying EncryptedCredentials.
         */
        std::optional<std::vector<PassportFile::ptr>> m_translation;
        /**
         * @var m_type
         * @brief Element type.
         * @details One of “personal_details”, “passport”, “driver_license”, “identity_card”, “internal_passport”, “address”, “utility_bill”, “bank_statement”, “rental_agreement”, “passport_registration”, “temporary_registration”, “phone_number”, “email”.
         */
        std::string m_type;

    public:
        //Constructors
        EncryptedPassportElement();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit EncryptedPassportElement(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_data
         */
        std::optional<std::string> get_data() const;

        /**
         * @brief Getter.
         * @return m_email
         */
        std::optional<std::string> get_email() const;

        /**
         * @brief Getter.
         * @return m_files
         */
        std::optional<std::vector<PassportFile::ptr>> get_files() const;

        /**
         * @brief Getter.
         * @return m_front_side
         */
        std::optional<PassportFile::ptr> get_front_side() const;

        /**
         * @brief Getter.
         * @return m_hash
         */
        std::string get_hash() const;

        /**
         * @brief Getter.
         * @return m_phone_number
         */
        std::optional<std::string> get_phone_number() const;

        /**
         * @brief Getter.
         * @return m_reverse_side
         */
        std::optional<PassportFile::ptr> get_reverse_side() const;

        /**
         * @brief Getter.
         * @return m_selfie
         */
        std::optional<PassportFile::ptr> get_selfie() const;

        /**
         * @brief Getter.
         * @return m_translation
         */
        std::optional<std::vector<PassportFile::ptr>> get_translation() const;

        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        //Setter
        /**
         * @brief Setter for m_data.
         * @param[in] data See the member variable.
         */
        void set_data(const std::optional<std::string> &data);

        /**
         * @brief Setter for m_email.
         * @param[in] email See the member variable.
         */
        void set_email(const std::optional<std::string> &email);

        /**
         * @brief Setter for m_files.
         * @param[in] files See the member variable.
         */
        void set_files(const std::optional<std::vector<PassportFile::ptr>> &files);

        /**
         * @brief Setter for m_front_side.
         * @param[in] front_side See the member variable.
         */
        void set_front_side(const std::optional<PassportFile::ptr> &front_side);

        /**
         * @brief Setter for m_hash.
         * @param[in] hash See the member variable.
         */
        void set_hash(const std::string &hash);

        /**
         * @brief Setter for m_phone_number.
         * @param[in] phone_number See the member variable.
         */
        void set_phone_number(const std::optional<std::string> &phone_number);

        /**
         * @brief Setter for m_reverse_side.
         * @param[in] reverse_side See the member variable.
         */
        void set_reverse_side(const std::optional<PassportFile::ptr> &reverse_side);

        /**
         * @brief Setter for m_selfie.
         * @param[in] selfie See the member variable.
         */
        void set_selfie(const std::optional<PassportFile::ptr> &selfie);

        /**
         * @brief Setter for m_translation.
         * @param[in] translation See the member variable.
         */
        void set_translation(const std::optional<std::vector<PassportFile::ptr>> &translation);

        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);
    };
} //namespace tgbot

#endif
