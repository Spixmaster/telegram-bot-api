#ifndef TGBOT_TYPES_CHATMEMBERBANNED_H
#define TGBOT_TYPES_CHATMEMBERBANNED_H

#include <cstdint>
#include <memory>
#include <string>

#include "tgbot/types/ChatMember.h"
#include "tgbot/types/User.h"

namespace tgbot
{
    /**
     * @class ChatMemberBanned
     * @brief Represents a chat member that was banned in the chat and can't return to the chat or view chat messages.
     */
    class ChatMemberBanned : public ChatMember
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<ChatMemberBanned>;

    private:
        //Member variables
        /**
         * @var m_status
         * @brief The member's status in the chat, always “kicked”.
         */
        std::string m_status;
        /**
         * @var m_until_date
         * @brief Date when restrictions will be lifted for this user; unix time.
         * @details If 0, then the user is banned forever.
         */
        std::uint64_t m_until_date;
        /**
         * @var m_user
         * @brief Information about the user.
         */
        User::ptr m_user;

    public:
        //Constructors
        ChatMemberBanned();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit ChatMemberBanned(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_status
         */
        std::string get_status() const;

        /**
         * @brief Getter.
         * @return m_until_date
         */
        std::uint64_t get_until_date() const;

        /**
         * @brief Getter.
         * @return m_user
         */
        User::ptr get_user() const;

        //Setter
        /**
         * @brief Setter for m_status.
         * @param[in] status See the member variable.
         */
        void set_status(const std::string &status);

        /**
         * @brief Setter for m_until_date.
         * @param[in] until_date See the member variable.
         */
        void set_until_date(const std::uint64_t &until_date);

        /**
         * @brief Setter for m_user.
         * @param[in] user See the member variable.
         */
        void set_user(const User::ptr &user);
    };
} //namespace tgbot

#endif
