#ifndef TGBOT_TYPES_WEBHOOKINFO_H
#define TGBOT_TYPES_WEBHOOKINFO_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>
#include <vector>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class WebhookInfo
     * @brief Describes the current status of a webhook.
     */
    class WebhookInfo : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<WebhookInfo>;

    private:
        //Member variables
        /**
         * @var m_allowed_updates
         * @brief Optional. A list of update types the bot is subscribed to.
         * @details Defaults to all update types except chat_member.
         */
        std::optional<std::vector<std::string>> m_allowed_updates;
        /**
         * @var m_has_custom_certificate
         * @brief True, if a custom certificate was provided for webhook certificate checks.
         */
        bool m_has_custom_certificate;
        /**
         * @var m_ip_address
         * @brief Optional. Currently used webhook IP address.
         */
        std::optional<std::string> m_ip_address;
        /**
         * @var m_last_error_date
         * @brief Optional. Unix time for the most recent error that happened when trying to deliver an update via webhook.
         */
        std::optional<std::uint64_t> m_last_error_date;
        /**
         * @var m_last_error_message
         * @brief Optional. Error message in human-readable format for the most recent error that happened when trying to deliver an update via webhook.
         */
        std::optional<std::string> m_last_error_message;
        /**
         * @var m_last_synchronization_error_date
         * @brief Optional. Unix time of the most recent error that happened when trying to synchronize available updates with Telegram datacenters.
         */
        std::optional<std::uint64_t> m_last_synchronization_error_date;
        /**
         * @var m_max_connections
         * @brief Optional. The maximum allowed number of simultaneous HTTPS connections to the webhook for update delivery.
         */
        std::optional<std::int32_t> m_max_connections;
        /**
         * @var m_pending_update_count
         * @brief Number of updates awaiting delivery.
         */
        std::int32_t m_pending_update_count;
        /**
         * @var m_url
         * @brief Webhook URL, may be empty if webhook is not set up.
         */
        std::string m_url;

    public:
        //Constructors
        WebhookInfo();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit WebhookInfo(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_allowed_updates
         */
        std::optional<std::vector<std::string>> get_allowed_updates() const;

        /**
         * @brief Getter.
         * @return m_has_custom_certificate
         */
        bool get_has_custom_certificate() const;

        /**
         * @brief Getter.
         * @return m_ip_address
         */
        std::optional<std::string> get_ip_address() const;

        /**
         * @brief Getter.
         * @return m_last_error_date
         */
        std::optional<std::uint64_t> get_last_error_date() const;

        /**
         * @brief Getter.
         * @return m_last_error_message
         */
        std::optional<std::string> get_last_error_message() const;

        /**
         * @brief Getter.
         * @return m_last_synchronization_error_date
         */
        std::optional<std::uint64_t> get_last_synchronization_error_date() const;

        /**
         * @brief Getter.
         * @return m_max_connections
         */
        std::optional<std::int32_t> get_max_connections() const;

        /**
         * @brief Getter.
         * @return m_pending_update_count
         */
        std::int32_t get_pending_update_count() const;

        /**
         * @brief Getter.
         * @return m_url
         */
        std::string get_url() const;

        //Setter
        /**
         * @brief Setter for m_allowed_updates.
         * @param[in] allowed_updates See the member variable.
         */
        void set_allowed_updates(const std::optional<std::vector<std::string>> &allowed_updates);

        /**
         * @brief Setter for m_has_custom_certificate.
         * @param[in] has_custom_certificate See the member variable.
         */
        void set_has_custom_certificate(const bool &has_custom_certificate);

        /**
         * @brief Setter for m_ip_address.
         * @param[in] ip_address See the member variable.
         */
        void set_ip_address(const std::optional<std::string> &ip_address);

        /**
         * @brief Setter for m_last_error_date.
         * @param[in] last_error_date See the member variable.
         */
        void set_last_error_date(const std::optional<std::uint64_t> &last_error_date);

        /**
         * @brief Setter for m_last_error_message.
         * @param[in] last_error_message See the member variable.
         */
        void set_last_error_message(const std::optional<std::string> &last_error_message);

        /**
         * @brief Setter for m_last_synchronization_error_date.
         * @param[in] last_synchronization_error_date See the member variable.
         */
        void set_last_synchronization_error_date(const std::optional<std::uint64_t> &last_synchronization_error_date);

        /**
         * @brief Setter for m_max_connections.
         * @param[in] max_connections See the member variable.
         */
        void set_max_connections(const std::optional<std::int32_t> &max_connections);

        /**
         * @brief Setter for m_pending_update_count.
         * @param[in] pending_update_count See the member variable.
         */
        void set_pending_update_count(const std::int32_t &pending_update_count);

        /**
         * @brief Setter for m_url.
         * @param[in] url See the member variable.
         */
        void set_url(const std::string &url);
    };
} //namespace tgbot

#endif
