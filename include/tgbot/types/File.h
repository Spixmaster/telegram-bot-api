#ifndef TGBOT_TYPES_FILE_H
#define TGBOT_TYPES_FILE_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class File
     * @brief This object represents a file ready to be downloaded.
     * @details The file can be downloaded via the link https://api.telegram.org/file/bot\<token>/\<file_path>.
     * @details It is guaranteed that the link will be valid for at least 1 hour. When the link expires, a new one can be requested by calling getFile.
     * @note The maximum file size to download is 20 MB.
     */
    class File : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<File>;

    private:
        //Member variables
        /**
         * @var m_file_id
         * @brief Identifier for this file, which can be used to download or reuse the file.
         */
        std::string m_file_id;
        /**
         * @var m_file_path
         * @brief Optional. File path.
         * @details Use https://api.telegram.org/file/bot\<token>/\<file_path> to get the file.
         */
        std::optional<std::string> m_file_path;
        /**
         * @var m_file_size
         * @brief Optional. File size in bytes, if known.
         * @details It can be bigger than 2^31 and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a signed 64-bit integer or double-precision float type are safe for storing this value.
         */
        std::optional<std::int64_t> m_file_size;
        /**
         * @var m_file_unique_id
         * @brief Unique identifier for this file, which is supposed to be the same over time and for different bots.
         * @details Can't be used to download or reuse the file.
         */
        std::string m_file_unique_id;

    public:
        //Constructors
        File();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit File(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_file_id
         */
        std::string get_file_id() const;

        /**
         * @brief Getter.
         * @return m_file_path
         */
        std::optional<std::string> get_file_path() const;

        /**
         * @brief Getter.
         * @return m_file_size
         */
        std::optional<std::int64_t> get_file_size() const;

        /**
         * @brief Getter.
         * @return m_file_unique_id
         */
        std::string get_file_unique_id() const;

        //Setter
        /**
         * @brief Setter for m_file_id.
         * @param[in] file_id See the member variable.
         */
        void set_file_id(const std::string &file_id);

        /**
         * @brief Setter for m_file_path.
         * @param[in] file_path See the member variable.
         */
        void set_file_path(const std::optional<std::string> &file_path);

        /**
         * @brief Setter for m_file_size.
         * @param[in] file_size See the member variable.
         */
        void set_file_size(const std::optional<std::int64_t> &file_size);

        /**
         * @brief Setter for m_file_unique_id.
         * @param[in] file_unique_id See the member variable.
         */
        void set_file_unique_id(const std::string &file_unique_id);
    };
} //namespace tgbot

#endif
