#ifndef TGBOT_TYPES_USERPROFILEPHOTOS_H
#define TGBOT_TYPES_USERPROFILEPHOTOS_H

#include <cstdint>
#include <memory>
#include <string>
#include <vector>

#include "tgbot/Type.h"
#include "tgbot/types/PhotoSize.h"

namespace tgbot
{
    /**
     * @class UserProfilePhotos
     * @brief This object represent a user's profile pictures.
     */
    class UserProfilePhotos : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<UserProfilePhotos>;

    private:
        //Member variables
        /**
         * @var m_photos
         * @brief Requested profile pictures (in up to 4 sizes each).
         */
        std::vector<std::vector<PhotoSize::ptr>> m_photos;
        /**
         * @var m_total_count
         * @brief Total number of profile pictures the target user has.
         */
        std::int32_t m_total_count;

    public:
        //Constructors
        UserProfilePhotos();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit UserProfilePhotos(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_photos
         */
        std::vector<std::vector<PhotoSize::ptr>> get_photos() const;

        /**
         * @brief Getter.
         * @return m_total_count
         */
        std::int32_t get_total_count() const;

        //Setter
        /**
         * @brief Setter for m_photos.
         * @param[in] photos See the member variable.
         */
        void set_photos(const std::vector<std::vector<PhotoSize::ptr>> &photos);

        /**
         * @brief Setter for m_total_count.
         * @param[in] total_count See the member variable.
         */
        void set_total_count(const std::int32_t &total_count);
    };
} //namespace tgbot

#endif
