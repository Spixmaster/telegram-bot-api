#ifndef TGBOT_TYPES_PASSPORTELEMENTERRORDATAFIELD_H
#define TGBOT_TYPES_PASSPORTELEMENTERRORDATAFIELD_H

#include <memory>
#include <string>

#include "tgbot/types/PassportElementError.h"

namespace tgbot
{
    /**
     * @class PassportElementErrorDataField
     * @brief Represents an issue in one of the data fields that was provided by the user.
     * @details The error is considered resolved when the field's value changes.
     */
    class PassportElementErrorDataField : public PassportElementError
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<PassportElementErrorDataField>;

    private:
        //Member variables
        /**
         * @var m_data_hash
         * @brief Base64-encoded data hash.
         */
        std::string m_data_hash;
        /**
         * @var m_field_name
         * @brief Name of the data field which has the error.
         */
        std::string m_field_name;
        /**
         * @var m_message
         * @brief Error message.
         */
        std::string m_message;
        /**
         * @var m_source
         * @brief Error source, must be data.
         */
        std::string m_source;
        /**
         * @var m_type
         * @brief The section of the user's Telegram Passport which has the error, one of “personal_details”, “passport”, “driver_license”, “identity_card”, “internal_passport”, “address”.
         */
        std::string m_type;

    public:
        //Constructors
        PassportElementErrorDataField();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit PassportElementErrorDataField(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_data_hash
         */
        std::string get_data_hash() const;

        /**
         * @brief Getter.
         * @return m_field_name
         */
        std::string get_field_name() const;

        /**
         * @brief Getter.
         * @return m_message
         */
        std::string get_message() const;

        /**
         * @brief Getter.
         * @return m_source
         */
        std::string get_source() const;

        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        //Setter
        /**
         * @brief Setter for m_data_hash.
         * @param[in] data_hash See the member variable.
         */
        void set_data_hash(const std::string &data_hash);

        /**
         * @brief Setter for m_field_name.
         * @param[in] field_name See the member variable.
         */
        void set_field_name(const std::string &field_name);

        /**
         * @brief Setter for m_message.
         * @param[in] message See the member variable.
         */
        void set_message(const std::string &message);

        /**
         * @brief Setter for m_source.
         * @param[in] source See the member variable.
         */
        void set_source(const std::string &source);

        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);
    };
} //namespace tgbot

#endif
