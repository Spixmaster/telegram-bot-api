#ifndef TGBOT_TYPES_CHATJOINREQUEST_H
#define TGBOT_TYPES_CHATJOINREQUEST_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"
#include "tgbot/types/Chat.h"
#include "tgbot/types/ChatInviteLink.h"
#include "tgbot/types/User.h"

namespace tgbot
{
    /**
     * @class ChatJoinRequest
     * @brief Represents a join request sent to a chat.
     */
    class ChatJoinRequest : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<ChatJoinRequest>;

    private:
        //Member variables
        /**
         * @var m_bio
         * @brief Optional. Bio of the user.
         */
        std::optional<std::string> m_bio;
        /**
         * @var m_chat
         * @brief Chat to which the request was sent.
         */
        Chat::ptr m_chat;
        /**
         * @var m_date
         * @brief Date the request was sent in Unix time.
         */
        std::uint64_t m_date;
        /**
         * @var m_from
         * @brief User that sent the join request.
         */
        User::ptr m_from;
        /**
         * @var m_invite_link
         * @brief Optional. Chat invite link that was used by the user to send the join request.
         */
        std::optional<ChatInviteLink::ptr> m_invite_link;
        /**
         * @var m_user_chat_id
         * @brief Identifier of a private chat with the user who sent the join request.
         * @details This number may have more than 32 significant bits and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a 64-bit integer or double-precision float type are safe for storing this identifier.
         * @details The bot can use this identifier for 24 hours to send messages until the join request is processed, assuming no other administrator contacted the user.
         */
        std::int64_t m_user_chat_id;

    public:
        //Constructors
        ChatJoinRequest();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit ChatJoinRequest(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_bio
         */
        std::optional<std::string> get_bio() const;

        /**
         * @brief Getter.
         * @return m_chat
         */
        Chat::ptr get_chat() const;

        /**
         * @brief Getter.
         * @return m_date
         */
        std::uint64_t get_date() const;

        /**
         * @brief Getter.
         * @return m_from
         */
        User::ptr get_from() const;

        /**
         * @brief Getter.
         * @return m_invite_link
         */
        std::optional<ChatInviteLink::ptr> get_invite_link() const;

        /**
         * @brief Getter.
         * @return m_user_chat_id
         */
        std::int64_t get_user_chat_id() const;

        //Setter
        /**
         * @brief Setter for m_bio.
         * @param[in] bio See the member variable.
         */
        void set_bio(const std::optional<std::string> &bio);

        /**
         * @brief Setter for m_chat.
         * @param[in] chat See the member variable.
         */
        void set_chat(const Chat::ptr &chat);

        /**
         * @brief Setter for m_date.
         * @param[in] date See the member variable.
         */
        void set_date(const std::uint64_t &date);

        /**
         * @brief Setter for m_from.
         * @param[in] from See the member variable.
         */
        void set_from(const User::ptr &from);

        /**
         * @brief Setter for m_invite_link.
         * @param[in] invite_link See the member variable.
         */
        void set_invite_link(const std::optional<ChatInviteLink::ptr> &invite_link);

        /**
         * @brief Setter for m_user_chat_id.
         * @param[in] user_chat_id See the member variable.
         */
        void set_user_chat_id(const std::int64_t &user_chat_id);
    };
} //namespace tgbot

#endif
