#ifndef TGBOT_TYPES_SHIPPINGADDRESS_H
#define TGBOT_TYPES_SHIPPINGADDRESS_H

#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class ShippingAddress
     * @brief This object represents a shipping address.
     */
    class ShippingAddress : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<ShippingAddress>;

    private:
        //Member variables
        /**
         * @var m_city
         * @brief City.
         */
        std::string m_city;
        /**
         * @var m_country_code
         * @brief Two-letter ISO 3166-1 alpha-2 country code.
         */
        std::string m_country_code;
        /**
         * @var m_post_code
         * @brief Address post code.
         */
        std::string m_post_code;
        /**
         * @var m_state
         * @brief State, if applicable.
         */
        std::string m_state;
        /**
         * @var m_street_line1
         * @brief First line for the address.
         */
        std::string m_street_line1;
        /**
         * @var m_street_line2
         * @brief Second line for the address.
         */
        std::string m_street_line2;

    public:
        //Constructors
        ShippingAddress();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit ShippingAddress(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_city
         */
        std::string get_city() const;

        /**
         * @brief Getter.
         * @return m_country_code
         */
        std::string get_country_code() const;

        /**
         * @brief Getter.
         * @return m_post_code
         */
        std::string get_post_code() const;

        /**
         * @brief Getter.
         * @return m_state
         */
        std::string get_state() const;

        /**
         * @brief Getter.
         * @return m_street_line1
         */
        std::string get_street_line1() const;

        /**
         * @brief Getter.
         * @return m_street_line2
         */
        std::string get_street_line2() const;

        //Setter
        /**
         * @brief Setter for m_city.
         * @param[in] city See the member variable.
         */
        void set_city(const std::string &city);

        /**
         * @brief Setter for m_country_code.
         * @param[in] country_code See the member variable.
         */
        void set_country_code(const std::string &country_code);

        /**
         * @brief Setter for m_post_code.
         * @param[in] post_code See the member variable.
         */
        void set_post_code(const std::string &post_code);

        /**
         * @brief Setter for m_state.
         * @param[in] state See the member variable.
         */
        void set_state(const std::string &state);

        /**
         * @brief Setter for m_street_line1.
         * @param[in] street_line1 See the member variable.
         */
        void set_street_line1(const std::string &street_line1);

        /**
         * @brief Setter for m_street_line2.
         * @param[in] street_line2 See the member variable.
         */
        void set_street_line2(const std::string &street_line2);
    };
} //namespace tgbot

#endif
