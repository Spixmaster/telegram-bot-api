#ifndef TGBOT_TYPES_PASSPORTELEMENTERRORSELFIE_H
#define TGBOT_TYPES_PASSPORTELEMENTERRORSELFIE_H

#include <memory>
#include <string>

#include "PassportElementError.h"

namespace tgbot
{
    /**
     * @class PassportElementErrorSelfie
     * @brief Represents an issue with the selfie with a document.
     * @details The error is considered resolved when the file with the selfie changes.
     */
    class PassportElementErrorSelfie : public PassportElementError
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<PassportElementErrorSelfie>;

    private:
        //Member variables
        /**
         * @var m_file_hash
         * @brief Base64-encoded hash of the file with the selfie.
         */
        std::string m_file_hash;
        /**
         * @var m_message
         * @brief Error message.
         */
        std::string m_message;
        /**
         * @var m_source
         * @brief Error source, must be selfie.
         */
        std::string m_source;
        /**
         * @var m_type
         * @brief The section of the user's Telegram Passport which has the issue, one of “passport”, “driver_license”, “identity_card”, “internal_passport”.
         */
        std::string m_type;

    public:
        //Constructors
        PassportElementErrorSelfie();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit PassportElementErrorSelfie(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_file_hash
         */
        std::string get_file_hash() const;

        /**
         * @brief Getter.
         * @return m_message
         */
        std::string get_message() const;

        /**
         * @brief Getter.
         * @return m_source
         */
        std::string get_source() const;

        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        //Setter
        /**
         * @brief Setter for m_file_hash.
         * @param[in] file_hash See the member variable.
         */
        void set_file_hash(const std::string &file_hash);

        /**
         * @brief Setter for m_message.
         * @param[in] message See the member variable.
         */
        void set_message(const std::string &message);

        /**
         * @brief Setter for m_source.
         * @param[in] source See the member variable.
         */
        void set_source(const std::string &source);

        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);
    };
} //namespace tgbot

#endif
