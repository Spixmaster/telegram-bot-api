#ifndef TGBOT_TYPES_INLINEQUERYRESULTVENUE_H
#define TGBOT_TYPES_INLINEQUERYRESULTVENUE_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>

#include "tgbot/types/InlineKeyboardMarkup.h"
#include "tgbot/types/InlineQueryResult.h"
#include "tgbot/types/InputMessageContent.h"

namespace tgbot
{
    /**
     * @class InlineQueryResultVenue
     * @brief Represents a venue.
     * @details By default, the venue will be sent by the user.
     * @details Alternatively, you can use input_message_content to send a message with the specified content instead of the venue.
     * @note This will only work in Telegram versions released after 9 April, 2016. Older clients will ignore them.
     */
    class InlineQueryResultVenue : public InlineQueryResult
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<InlineQueryResultVenue>;

    private:
        //Member variables
        /**
         * @var m_address
         * @brief Address of the venue.
         */
        std::string m_address;
        /**
         * @var m_foursquare_id
         * @brief Optional. Foursquare identifier of the venue if known.
         */
        std::optional<std::string> m_foursquare_id;
        /**
         * @var m_foursquare_type
         * @brief Optional. Foursquare type of the venue, if known.
         * @details (For example, “arts_entertainment/default”, “arts_entertainment/aquarium” or “food/icecream”.)
         */
        std::optional<std::string> m_foursquare_type;
        /**
         * @var m_google_place_id
         * @brief Optional. Google Places identifier of the venue.
         */
        std::optional<std::string> m_google_place_id;
        /**
         * @var m_google_place_type
         * @brief Optional. Google Places type of the venue.
         * @details (See supported types.)
         */
        std::optional<std::string> m_google_place_type;
        /**
         * @var m_id
         * @brief Unique identifier for this result, 1-64 Bytes.
         */
        std::string m_id;
        /**
         * @var m_input_message_content
         * @brief Optional. Content of the message to be sent instead of the venue.
         */
        std::optional<InputMessageContent::ptr> m_input_message_content;
        /**
         * @var m_latitude
         * @brief Latitude of the venue location in degrees.
         */
        float m_latitude;
        /**
         * @var m_longitude
         * @brief Longitude of the venue location in degrees.
         */
        float m_longitude;
        /**
         * @var m_reply_markup
         * @brief Optional. Inline keyboard attached to the message.
         */
        std::optional<InlineKeyboardMarkup::ptr> m_reply_markup;
        /**
         * @var m_thumbnail_height
         * @brief Optional. Thumbnail height.
         */
        std::optional<std::int32_t> m_thumbnail_height;
        /**
         * @var m_thumbnail_url
         * @brief Optional. Url of the thumbnail for the result.
         */
        std::optional<std::string> m_thumbnail_url;
        /**
         * @var m_thumbnail_width
         * @brief Optional. Thumbnail width.
         */
        std::optional<std::int32_t> m_thumbnail_width;
        /**
         * @var m_title
         * @brief Title of the venue.
         */
        std::string m_title;
        /**
         * @var m_type
         * @brief Type of the result, must be venue.
         */
        std::string m_type;

    public:
        //Constructors
        InlineQueryResultVenue();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit InlineQueryResultVenue(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_address
         */
        std::string get_address() const;

        /**
         * @brief Getter.
         * @return m_foursquare_id
         */
        std::optional<std::string> get_foursquare_id() const;

        /**
         * @brief Getter.
         * @return m_foursquare_type
         */
        std::optional<std::string> get_foursquare_type() const;

        /**
         * @brief Getter.
         * @return m_google_place_id
         */
        std::optional<std::string> get_google_place_id() const;

        /**
         * @brief Getter.
         * @return m_google_place_type
         */
        std::optional<std::string> get_google_place_type() const;

        /**
         * @brief Getter.
         * @return m_id
         */
        std::string get_id() const;

        /**
         * @brief Getter.
         * @return m_input_message_content
         */
        std::optional<InputMessageContent::ptr> get_input_message_content() const;

        /**
         * @brief Getter.
         * @return m_latitude
         */
        float get_latitude() const;

        /**
         * @brief Getter.
         * @return m_longitude
         */
        float get_longitude() const;

        /**
         * @brief Getter.
         * @return m_reply_markup
         */
        std::optional<InlineKeyboardMarkup::ptr> get_reply_markup() const;

        /**
         * @brief Getter.
         * @return m_thumbnail_height
         */
        std::optional<std::int32_t> get_thumbnail_height() const;

        /**
         * @brief Getter.
         * @return m_thumbnail_url
         */
        std::optional<std::string> get_thumbnail_url() const;

        /**
         * @brief Getter.
         * @return m_thumbnail_width
         */
        std::optional<std::int32_t> get_thumbnail_width() const;

        /**
         * @brief Getter.
         * @return m_title
         */
        std::string get_title() const;

        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        //Setter
        /**
         * @brief Setter for m_address.
         * @param[in] address See the member variable.
         */
        void set_address(const std::string &address);

        /**
         * @brief Setter for m_foursquare_id.
         * @param[in] foursquare_id See the member variable.
         */
        void set_foursquare_id(const std::optional<std::string> &foursquare_id);

        /**
         * @brief Setter for m_foursquare_type.
         * @param[in] foursquare_type See the member variable.
         */
        void set_foursquare_type(const std::optional<std::string> &foursquare_type);

        /**
         * @brief Setter for m_google_place_id.
         * @param[in] google_place_id See the member variable.
         */
        void set_google_place_id(const std::optional<std::string> &google_place_id);

        /**
         * @brief Setter for m_google_place_type.
         * @param[in] google_place_type See the member variable.
         */
        void set_google_place_type(const std::optional<std::string> &google_place_type);

        /**
         * @brief Setter for m_id.
         * @param[in] id See the member variable.
         */
        void set_id(const std::string &id);

        /**
         * @brief Setter for m_input_message_content.
         * @param[in] input_message_content See the member variable.
         */
        void set_input_message_content(const std::optional<InputMessageContent::ptr> &input_message_content);

        /**
         * @brief Setter for m_latitude.
         * @param[in] latitude See the member variable.
         */
        void set_latitude(const float &latitude);

        /**
         * @brief Setter for m_longitude.
         * @param[in] longitude See the member variable.
         */
        void set_longitude(const float &longitude);

        /**
         * @brief Setter for m_reply_markup.
         * @param[in] reply_markup See the member variable.
         */
        void set_reply_markup(const std::optional<InlineKeyboardMarkup::ptr> &reply_markup);

        /**
         * @brief Setter for m_thumbnail_height.
         * @param[in] thumbnail_height See the member variable.
         */
        void set_thumbnail_height(const std::optional<std::int32_t> &thumbnail_height);

        /**
         * @brief Setter for m_thumbnail_url.
         * @param[in] thumbnail_url See the member variable.
         */
        void set_thumbnail_url(const std::optional<std::string> &thumbnail_url);

        /**
         * @brief Setter for m_thumbnail_width.
         * @param[in] thumbnail_width See the member variable.
         */
        void set_thumbnail_width(const std::optional<std::int32_t> &thumbnail_width);

        /**
         * @brief Setter for m_title.
         * @param[in] title See the member variable.
         */
        void set_title(const std::string &title);

        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);
    };
} //namespace tgbot

#endif
