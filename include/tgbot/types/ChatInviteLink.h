#ifndef TGBOT_TYPES_CHATINVITELINK_H
#define TGBOT_TYPES_CHATINVITELINK_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"
#include "tgbot/types/User.h"

namespace tgbot
{
    /**
     * @class ChatInviteLink
     * @brief Represents an invite link for a chat.
     */
    class ChatInviteLink : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<ChatInviteLink>;

    private:
        //Member variables
        /**
         * @var m_creates_join_request
         * @brief True, if users joining the chat via the link need to be approved by chat administrators.
         */
        bool m_creates_join_request;
        /**
         * @var m_creator
         * @brief Creator of the link.
         */
        User::ptr m_creator;
        /**
         * @var m_expire_date
         * @brief Optional. Point in time (Unix timestamp) when the link will expire or has been expired.
         */
        std::optional<std::uint64_t> m_expire_date;
        /**
         * @var m_invite_link
         * @brief The invite link.
         * @details If the link was created by another chat administrator, then the second part of the link will be replaced with “…”.
         */
        std::string m_invite_link;
        /**
         * @var m_is_primary
         * @brief True, if the link is primary.
         */
        bool m_is_primary;
        /**
         * @var m_is_revoked
         * @brief True, if the link is revoked.
         */
        bool m_is_revoked;
        /**
         * @var m_member_limit
         * @brief Optional. The maximum number of users that can be members of the chat simultaneously after joining the chat via this invite link; 1-99999.
         */
        std::optional<std::int32_t> m_member_limit;
        /**
         * @var m_name
         * @brief Optional. Invite link name.
         */
        std::optional<std::string> m_name;
        /**
         * @var m_pending_join_request_count
         * @brief Optional. Number of pending join requests created using this link.
         */
        std::optional<std::int32_t> m_pending_join_request_count;

    public:
        //Constructors
        ChatInviteLink();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit ChatInviteLink(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_creates_join_request
         */
        bool get_creates_join_request() const;

        /**
         * @brief Getter.
         * @return m_creator
         */
        User::ptr get_creator() const;

        /**
         * @brief Getter.
         * @return m_expire_date
         */
        std::optional<std::uint64_t> get_expire_date() const;

        /**
         * @brief Getter.
         * @return m_invite_link
         */
        std::string get_invite_link() const;

        /**
         * @brief Getter.
         * @return m_is_primary
         */
        bool get_is_primary() const;

        /**
         * @brief Getter.
         * @return m_is_revoked
         */
        bool get_is_revoked() const;

        /**
         * @brief Getter.
         * @return m_member_limit
         */
        std::optional<std::int32_t> get_member_limit() const;

        /**
         * @brief Getter.
         * @return m_name
         */
        std::optional<std::string> get_name() const;

        /**
         * @brief Getter.
         * @return m_pending_join_request_count
         */
        std::optional<std::int32_t> get_pending_join_request_count() const;

        //Setter
        /**
         * @brief Setter for m_creates_join_request.
         * @param[in] creates_join_request See the member variable.
         */
        void set_creates_join_request(const bool &creates_join_request);

        /**
         * @brief Setter for m_creator.
         * @param[in] creator See the member variable.
         */
        void set_creator(const User::ptr &creator);

        /**
         * @brief Setter for m_expire_date.
         * @param[in] expire_date See the member variable.
         */
        void set_expire_date(const std::optional<std::uint64_t> &expire_date);

        /**
         * @brief Setter for m_invite_link.
         * @param[in] invite_link See the member variable.
         */
        void set_invite_link(const std::string &invite_link);

        /**
         * @brief Setter for m_is_primary.
         * @param[in] is_primary See the member variable.
         */
        void set_is_primary(const bool &is_primary);

        /**
         * @brief Setter for m_is_revoked.
         * @param[in] is_revoked See the member variable.
         */
        void set_is_revoked(const bool &is_revoked);

        /**
         * @brief Setter for m_member_limit.
         * @param[in] member_limit See the member variable.
         */
        void set_member_limit(const std::optional<std::int32_t> &member_limit);

        /**
         * @brief Setter for m_name.
         * @param[in] name See the member variable.
         */
        void set_name(const std::optional<std::string> &name);

        /**
         * @brief Setter for m_pending_join_request_count.
         * @param[in] pending_join_request_count See the member variable.
         */
        void set_pending_join_request_count(const std::optional<std::int32_t> &pending_join_request_count);
    };
} //namespace tgbot

#endif
