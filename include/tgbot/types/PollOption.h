#ifndef TGBOT_TYPES_POLLOPTION_H
#define TGBOT_TYPES_POLLOPTION_H

#include <cstdint>
#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class PollOption
     * @brief This object contains information about one answer option in a poll.
     */
    class PollOption : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<PollOption>;

    private:
        //Member variables
        /**
         * @var m_text
         * @brief Option text, 1-100 characters.
         */
        std::string m_text;
        /**
         * @var m_voter_count
         * @brief Number of users that voted for this option.
         */
        std::int32_t m_voter_count;

    public:
        //Constructors
        PollOption();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit PollOption(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_text
         */
        std::string get_text() const;

        /**
         * @brief Getter.
         * @return m_voter_count
         */
        std::int32_t get_voter_count() const;

        //Setter
        /**
         * @brief Setter for m_text.
         * @param[in] text See the member variable.
         */
        void set_text(const std::string &text);

        /**
         * @brief Setter for m_voter_count.
         * @param[in] voter_count See the member variable.
         */
        void set_voter_count(const std::int32_t &voter_count);
    };
} //namespace tgbot

#endif
