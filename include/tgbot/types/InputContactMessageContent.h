#ifndef TGBOT_TYPES_INPUTCONTACTMESSAGECONTENT_H
#define TGBOT_TYPES_INPUTCONTACTMESSAGECONTENT_H

#include <memory>
#include <optional>
#include <string>

#include "tgbot/types/InputMessageContent.h"

namespace tgbot
{
    /**
     * @class InputContactMessageContent
     * @brief Represents the content of a contact message to be sent as the result of an inline query.
     */
    class InputContactMessageContent : public InputMessageContent
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<InputContactMessageContent>;

    private:
        //Member variables
        /**
         * @var m_first_name
         * @brief Contact's first name.
         */
        std::string m_first_name;
        /**
         * @var m_last_name
         * @brief Optional. Contact's last name.
         */
        std::optional<std::string> m_last_name;
        /**
         * @var m_phone_number
         * @brief Contact's phone number.
         */
        std::string m_phone_number;
        /**
         * @var m_vcard
         * @brief Optional. Additional data about the contact in the form of a vCard, 0-2048 bytes.
         */
        std::optional<std::string> m_vcard;

    public:
        //Constructors
        InputContactMessageContent();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit InputContactMessageContent(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_first_name
         */
        std::string get_first_name() const;

        /**
         * @brief Getter.
         * @return m_last_name
         */
        std::optional<std::string> get_last_name() const;

        /**
         * @brief Getter.
         * @return m_phone_number
         */
        std::string get_phone_number() const;

        /**
         * @brief Getter.
         * @return m_vcard
         */
        std::optional<std::string> get_vcard() const;

        //Setter
        /**
         * @brief Setter for m_first_name.
         * @param[in] first_name See the member variable.
         */
        void set_first_name(const std::string &first_name);

        /**
         * @brief Setter for m_last_name.
         * @param[in] last_name See the member variable.
         */
        void set_last_name(const std::optional<std::string> &last_name);

        /**
         * @brief Setter for m_phone_number.
         * @param[in] phone_number See the member variable.
         */
        void set_phone_number(const std::string &phone_number);

        /**
         * @brief Setter for m_vcard.
         * @param[in] vcard See the member variable.
         */
        void set_vcard(const std::optional<std::string> &vcard);
    };
} //namespace tgbot

#endif
