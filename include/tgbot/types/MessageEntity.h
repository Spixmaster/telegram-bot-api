#ifndef TGBOT_TYPES_MESSAGEENTITY_H
#define TGBOT_TYPES_MESSAGEENTITY_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"
#include "tgbot/types/User.h"

namespace tgbot
{
    /**
     * @class MessageEntity
     * @brief This object represents one special entity in a text message.
     * @details For example, hashtags, usernames, URLs, etc.
     */
    class MessageEntity : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<MessageEntity>;

    private:
        //Member variables
        /**
         * @var m_custom_emoji
         * @brief Optional. For “custom_emoji” only, unique identifier of the custom emoji.
         * @details Use getCustomEmojiStickers to get full information about the sticker.
         */
        std::optional<std::string> m_custom_emoji;
        /**
         * @var m_language
         * @brief Optional. For “pre” only, the programming language of the entity text.
         */
        std::optional<std::string> m_language;
        /**
         * @var m_length
         * @brief Length of the entity in UTF-16 code units.
         */
        std::int32_t m_length;
        /**
         * @var m_offset
         * @brief Offset in UTF-16 code units to the start of the entity.
         */
        std::int32_t m_offset;
        /**
         * @var m_type
         * @brief Type of the entity.
         * @details Currently, can be “mention” (\@username), “hashtag” (\#hashtag), “cashtag” ($USD), “bot_command” (/start\@jobs_bot), “url” (https://telegram.org), “email” (do-not-reply@telegram.org), “phone_number” (+1-212-555-0123), “bold” (bold text), “italic” (italic text), “underline” (underlined text), “strikethrough” (strikethrough text), “spoiler” (spoiler message), “code” (monowidth string), “pre” (monowidth block), “text_link” (for clickable text URLs), “text_mention” (for users without usernames), “custom_emoji” (for inline custom emoji stickers).
         */
        std::string m_type;
        /**
         * @var m_url
         * @brief Optional. For “text_link” only, URL that will be opened after user taps on the text.
         */
        std::optional<std::string> m_url;
        /**
         * @var m_user
         * @brief Optional. For “text_mention” only, the mentioned user.
         */
        std::optional<User::ptr> m_user;

    public:
        //Constructors
        MessageEntity();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit MessageEntity(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_custom_emoji
         */
        std::optional<std::string> get_custom_emoji() const;

        /**
         * @brief Getter.
         * @return m_language
         */
        std::optional<std::string> get_language() const;

        /**
         * @brief Getter.
         * @return m_length
         */
        std::int32_t get_length() const;

        /**
         * @brief Getter.
         * @return m_offset
         */
        std::int32_t get_offset() const;

        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        /**
         * @brief Getter.
         * @return m_url
         */
        std::optional<std::string> get_url() const;

        /**
         * @brief Getter.
         * @return m_user
         */
        std::optional<User::ptr> get_user() const;

        //Setter
        /**
         * @brief Setter for m_custom_emoji.
         * @param[in] custom_emoji See the member variable.
         */
        void set_custom_emoji(const std::optional<std::string> &custom_emoji);

        /**
         * @brief Setter for m_language.
         * @param[in] language See the member variable.
         */
        void set_language(const std::optional<std::string> &language);

        /**
         * @brief Setter for m_length.
         * @param[in] length See the member variable.
         */
        void set_length(const std::int32_t &length);

        /**
         * @brief Setter for m_offset.
         * @param[in] offset See the member variable.
         */
        void set_offset(const std::int32_t &offset);

        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);

        /**
         * @brief Setter for m_url.
         * @param[in] url See the member variable.
         */
        void set_url(const std::optional<std::string> &url);

        /**
         * @brief Setter for m_user.
         * @param[in] user See the member variable.
         */
        void set_user(const std::optional<User::ptr> &user);
    };
} //namespace tgbot

#endif
