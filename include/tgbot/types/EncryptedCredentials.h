#ifndef TGBOT_TYPES_ENCRYPTEDCREDENTIALS_H
#define TGBOT_TYPES_ENCRYPTEDCREDENTIALS_H

#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class EncryptedCredentials
     * @brief Describes data required for decrypting and authenticating EncryptedPassportElement.
     * @details See the Telegram Passport Documentation for a complete description of the data decryption and authentication processes.
     */
    class EncryptedCredentials : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<EncryptedCredentials>;

    private:
        //Member variables
        /**
         * @var m_data
         * @brief Base64-encoded encrypted JSON-serialized data with unique user's payload, data hashes and secrets required for EncryptedPassportElement decryption and authentication.
         */
        std::string m_data;
        /**
         * @var m_hash
         * @brief Base64-encoded data hash for data authentication.
         */
        std::string m_hash;
        /**
         * @var m_secret
         * @brief Base64-encoded secret, encrypted with the bot's public RSA key, required for data decryption.
         */
        std::string m_secret;

    public:
        //Constructors
        EncryptedCredentials();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit EncryptedCredentials(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_data
         */
        std::string get_data() const;

        /**
         * @brief Getter.
         * @return m_hash
         */
        std::string get_hash() const;

        /**
         * @brief Getter.
         * @return m_secret
         */
        std::string get_secret() const;

        //Setter
        /**
         * @brief Setter for m_data.
         * @param[in] data See the member variable.
         */
        void set_data(const std::string &data);

        /**
         * @brief Setter for m_hash.
         * @param[in] hash See the member variable.
         */
        void set_hash(const std::string &hash);

        /**
         * @brief Setter for m_secret.
         * @param[in] secret See the member variable.
         */
        void set_secret(const std::string &secret);
    };
} //namespace tgbot

#endif
