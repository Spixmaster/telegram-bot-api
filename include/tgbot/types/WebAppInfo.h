#ifndef TGBOT_TYPES_WEBAPPINFO_H
#define TGBOT_TYPES_WEBAPPINFO_H

#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class WebAppInfo
     * @brief Describes a Web App.
     */
    class WebAppInfo : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<WebAppInfo>;

    private:
        //Member variables
        /**
         * @var m_url
         * @brief An HTTPS URL of a Web App to be opened with additional data as specified in Initializing Web Apps.
         */
        std::string m_url;

    public:
        //Constructors
        WebAppInfo();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit WebAppInfo(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_url
         */
        std::string get_url() const;

        //Setter
        /**
         * @brief Setter for m_url.
         * @param[in] url See the member variable.
         */
        void set_url(const std::string &url);
    };
} //namespace tgbot

#endif
