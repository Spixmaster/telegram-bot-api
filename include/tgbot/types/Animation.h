#ifndef TGBOT_TYPES_ANIMATION_H
#define TGBOT_TYPES_ANIMATION_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"
#include "tgbot/types/PhotoSize.h"

namespace tgbot
{
    /**
     * @class Animation
     * @brief This object represents an animation file (GIF or H.264/MPEG-4 AVC video without sound).
     */
    class Animation : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<Animation>;

    private:
        //Member variables
        /**
         * @var m_duration
         * @brief Duration of the video in seconds as defined by sender.
         */
        std::int32_t m_duration;
        /**
         * @var m_file_id
         * @brief Identifier for this file, which can be used to download or reuse the file.
         */
        std::string m_file_id;
        /**
         * @var m_file_name
         * @brief Optional. Original animation filename as defined by sender.
         */
        std::optional<std::string> m_file_name;
        /**
         * @var m_file_size
         * @brief Optional. File size in bytes.
         * @details It can be bigger than 2^31 and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a signed 64-bit integer or double-precision float type are safe for storing this value.
         */
        std::optional<std::int64_t> m_file_size;
        /**
         * @var m_file_unique_id
         * @brief Unique identifier for this file, which is supposed to be the same over time and for different bots.
         * @details Can't be used to download or reuse the file.
         */
        std::string m_file_unique_id;
        /**
         * @var m_height
         * @brief Video height as defined by sender.
         */
        std::int32_t m_height;
        /**
         * @var m_mime_type
         * @brief Optional. MIME type of the file as defined by sender.
         */
        std::optional<std::string> m_mime_type;
        /**
         * @var m_thumbnail
         * @brief Optional. Animation thumbnail as defined by sender.
         */
        std::optional<PhotoSize::ptr> m_thumbnail;
        /**
         * @var m_width
         * @brief Video width as defined by sender.
         */
        std::int32_t m_width;

    public:
        //Constructors
        Animation();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit Animation(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_duration
         */
        std::int32_t get_duration() const;

        /**
         * @brief Getter.
         * @return m_file_id
         */
        std::string get_file_id() const;

        /**
         * @brief Getter.
         * @return m_file_name
         */
        std::optional<std::string> get_file_name() const;

        /**
         * @brief Getter.
         * @return m_file_size
         */
        std::optional<std::int64_t> get_file_size() const;

        /**
         * @brief Getter.
         * @return m_file_unique_id
         */
        std::string get_file_unique_id() const;

        /**
         * @brief Getter.
         * @return m_height
         */
        std::int32_t get_height() const;

        /**
         * @brief Getter.
         * @return m_mime_type
         */
        std::optional<std::string> get_mime_type() const;

        /**
         * @brief Getter.
         * @return m_thumbnail
         */
        std::optional<PhotoSize::ptr> get_thumbnail() const;

        /**
         * @brief Getter.
         * @return m_width
         */
        std::int32_t get_width() const;

        //Setter
        /**
         * @brief Setter for m_duration.
         * @param[in] duration See the member variable.
         */
        void set_duration(const std::int32_t &duration);

        /**
         * @brief Setter for m_file_id.
         * @param[in] file_id See the member variable.
         */
        void set_file_id(const std::string &file_id);

        /**
         * @brief Setter for m_file_name.
         * @param[in] file_name See the member variable.
         */
        void set_file_name(const std::optional<std::string> &file_name);

        /**
         * @brief Setter for m_file_size.
         * @param[in] file_size See the member variable.
         */
        void set_file_size(const std::optional<std::int64_t> &file_size);

        /**
         * @brief Setter for m_file_unique_id.
         * @param[in] file_unique_id See the member variable.
         */
        void set_file_unique_id(const std::string &file_unique_id);

        /**
         * @brief Setter for m_height.
         * @param[in] height See the member variable.
         */
        void set_height(const std::int32_t &height);

        /**
         * @brief Setter for m_mime_type.
         * @param[in] mime_type See the member variable.
         */
        void set_mime_type(const std::optional<std::string> &mime_type);

        /**
         * @brief Setter for m_thumbnail.
         * @param[in] thumbnail See the member variable.
         */
        void set_thumbnail(const std::optional<PhotoSize::ptr> &thumbnail);

        /**
         * @brief Setter for m_width.
         * @param[in] width See the member variable.
         */
        void set_width(const std::int32_t &width);
    };
} //namespace tgbot

#endif
