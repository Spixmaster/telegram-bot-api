#ifndef TGBOT_TYPES_MENUBUTTONWEBAPP_H
#define TGBOT_TYPES_MENUBUTTONWEBAPP_H

#include <memory>
#include <string>

#include "tgbot/types/MenuButton.h"
#include "tgbot/types/WebAppInfo.h"

namespace tgbot
{
    /**
     * @class MenuButtonWebApp
     * @brief Represents a menu button, which launches a Web App.
     */
    class MenuButtonWebApp : public MenuButton
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<MenuButtonWebApp>;

    private:
        //Member variables
        /**
         * @var m_text
         * @brief Text on the button.
         */
        std::string m_text;
        /**
         * @var m_type
         * @brief Type of the button, must be web_app.
         */
        std::string m_type;
        /**
         * @var m_web_app
         * @brief Description of the Web App that will be launched when the user presses the button.
         * @details The Web App will be able to send an arbitrary message on behalf of the user using the method answerWebAppQuery.
         */
        WebAppInfo::ptr m_web_app;

    public:
        //Constructors
        MenuButtonWebApp();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit MenuButtonWebApp(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_text
         */
        std::string get_text() const;

        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        /**
         * @brief Getter.
         * @return m_web_app
         */
        WebAppInfo::ptr get_web_app() const;

        //Setter
        /**
         * @brief Setter for m_text.
         * @param[in] text See the member variable.
         */
        void set_text(const std::string &text);

        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);

        /**
         * @brief Setter for m_web_app.
         * @param[in] web_app See the member variable.
         */
        void set_web_app(const WebAppInfo::ptr &web_app);
    };
} //namespace tgbot

#endif
