#ifndef TGBOT_TYPES_INPUTMEDIADOCUMENT_H
#define TGBOT_TYPES_INPUTMEDIADOCUMENT_H

#include <memory>
#include <optional>
#include <string>
#include <vector>

#include "tgbot/types/InputMedia.h"
#include "tgbot/types/MessageEntity.h"

namespace tgbot
{
    /**
     * @class InputMediaDocument
     * @brief Represents a general file to be sent.
     */
    class InputMediaDocument : public InputMedia
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<InputMediaDocument>;

    private:
        //Member variables
        /**
         * @var m_caption
         * @brief Optional. Caption of the document to be sent, 0-1024 characters after entities parsing.
         */
        std::optional<std::string> m_caption;
        /**
         * @var m_caption_entities
         * @brief Optional. List of special entities that appear in the caption, which can be specified instead of parse_mode.
         */
        std::optional<std::vector<MessageEntity::ptr>> m_caption_entities;
        /**
         * @var m_disable_content_type_detection
         * @brief Optional. Disables automatic server-side content type detection for files uploaded using multipart/form-data.
         * @details Always True, if the document is sent as part of an album.
         */
        std::optional<bool> m_disable_content_type_detection;
        /**
         * @var m_media
         * @brief File to send.
         * @details Pass a file_id to send a file that exists on the Telegram servers (recommended), pass an HTTP URL for Telegram to get a file from the Internet, or pass “attach://\<file_attach_name>” to upload a new one using multipart/form-data under \<file_attach_name> name.
         * @details More information on Sending Files ».
         */
        std::string m_media;
        /**
         * @var m_parse_mode
         * @brief Optional. Mode for parsing entities in the document caption.
         * @details See formatting options for more details.
         */
        std::optional<std::string> m_parse_mode;
        /**
         * @var m_thumbnail
         * @brief Optional. Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side.
         * @details The thumbnail should be in JPEG format and less than 200 kB in size.
         * @details A thumbnail's width and height should not exceed 320.
         * @details Ignored if the file is not uploaded using multipart/form-data.
         * @details Thumbnails can't be reused and can be only uploaded as a new file, so you can pass “attach://\<file_attach_name>” if the thumbnail was uploaded using multipart/form-data under \<file_attach_name>.
         * @details More information on Sending Files ».
         */
        std::optional<std::string> m_thumbnail;
        /**
         * @var m_type
         * @brief Type of the result, must be document.
         */
        std::string m_type;

    public:
        //Constructors
        InputMediaDocument();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit InputMediaDocument(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_caption
         */
        std::optional<std::string> get_caption() const;

        /**
         * @brief Getter.
         * @return m_caption_entities
         */
        std::optional<std::vector<MessageEntity::ptr>> get_caption_entities() const;

        /**
         * @brief Getter.
         * @return m_disable_content_type_detection
         */
        std::optional<bool> get_disable_content_type_detection() const;

        /**
         * @brief Getter.
         * @return m_media
         */
        std::string get_media() const;

        /**
         * @brief Getter.
         * @return m_parse_mode
         */
        std::optional<std::string> get_parse_mode() const;

        /**
         * @brief Getter.
         * @return m_thumbnail
         */
        std::optional<std::string> get_thumbnail() const;

        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        //Setter
        /**
         * @brief Setter for m_caption.
         * @param[in] caption See the member variable.
         */
        void set_caption(const std::optional<std::string> &caption);

        /**
         * @brief Setter for m_caption_entities.
         * @param[in] caption_entities See the member variable.
         */
        void set_caption_entities(const std::optional<std::vector<MessageEntity::ptr>> &caption_entities);

        /**
         * @brief Setter for m_disable_content_type_detection.
         * @param[in] disable_content_type_detection See the member variable.
         */
        void set_disable_content_type_detection(const std::optional<bool> &disable_content_type_detection);

        /**
         * @brief Setter for m_media.
         * @param[in] media See the member variable.
         */
        void set_media(const std::string &media);

        /**
         * @brief Setter for m_parse_mode.
         * @param[in] parse_mode See the member variable.
         */
        void set_parse_mode(const std::optional<std::string> &parse_mode);

        /**
         * @brief Setter for m_thumbnail.
         * @param[in] thumbnail See the member variable.
         */
        void set_thumbnail(const std::optional<std::string> &thumbnail);

        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);
    };
} //namespace tgbot

#endif
