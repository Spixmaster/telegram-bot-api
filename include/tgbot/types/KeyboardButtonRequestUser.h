#ifndef TGBOT_TYPES_KEYBOARDBUTTONREQUESTUSER_H
#define TGBOT_TYPES_KEYBOARDBUTTONREQUESTUSER_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class KeyboardButtonRequestUser
     * @brief This object defines the criteria used to request a suitable user.
     * @details The identifier of the selected user will be shared with the bot when the corresponding button is pressed.
     */
    class KeyboardButtonRequestUser : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<KeyboardButtonRequestUser>;

    private:
        //Member variables
        /**
         * @var m_request_id
         * @brief Signed 32-bit identifier of the request, which will be received back in the UserShared object.
         * @details Must be unique within the message.
         */
        std::int32_t m_request_id;
        /**
         * @var m_user_is_bot
         * @brief Optional. Pass True to request a bot, pass False to request a regular user.
         * @details If not specified, no additional restrictions are applied.
         */
        std::optional<bool> m_user_is_bot;
        /**
         * @var m_user_is_premium
         * @brief Optional. Pass True to request a premium user, pass False to request a non-premium user.
         * @details If not specified, no additional restrictions are applied.
         */
        std::optional<bool> m_user_is_premium;

    public:
        //Constructors
        KeyboardButtonRequestUser();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit KeyboardButtonRequestUser(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_request_id
         */
        std::int32_t get_request_id() const;

        /**
         * @brief Getter.
         * @return m_user_is_bot
         */
        std::optional<bool> get_user_is_bot() const;

        /**
         * @brief Getter.
         * @return m_user_is_premium
         */
        std::optional<bool> get_user_is_premium() const;

        //Setter
        /**
         * @brief Setter for m_request_id.
         * @param[in] request_id See the member variable.
         */
        void set_request_id(const std::int32_t &request_id);

        /**
         * @brief Setter for m_user_is_bot.
         * @param[in] user_is_bot See the member variable.
         */
        void set_user_is_bot(const std::optional<bool> &user_is_bot);

        /**
         * @brief Setter for m_user_is_premium.
         * @param[in] user_is_premium See the member variable.
         */
        void set_user_is_premium(const std::optional<bool> &user_is_premium);
    };
} //namespace tgbot

#endif
