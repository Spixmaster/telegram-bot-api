#ifndef TGBOT_TYPES_BOTSHORTDESCRIPTION_H
#define TGBOT_TYPES_BOTSHORTDESCRIPTION_H

#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class BotShortDescription
     * @brief This object represents the bot's short description.
     */
    class BotShortDescription : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<BotShortDescription>;

    private:
        //Member variables
        /**
         * @var m_short_description
         * @brief The bot's short description.
         */
        std::string m_short_description;

    public:
        //Constructors
        BotShortDescription();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit BotShortDescription(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_short_description
         */
        std::string get_short_description() const;

        //Setter
        /**
         * @brief Setter for m_short_description.
         * @param[in] short_description See the member variable.
         */
        void set_short_description(const std::string &short_description);
    };
} //namespace tgbot

#endif
