#ifndef TGBOT_TYPES_INLINEQUERYRESULTLOCATION_H
#define TGBOT_TYPES_INLINEQUERYRESULTLOCATION_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>

#include "tgbot/types/InlineKeyboardMarkup.h"
#include "tgbot/types/InlineQueryResult.h"
#include "tgbot/types/InputMessageContent.h"

namespace tgbot
{
    /**
     * @class InlineQueryResultLocation
     * @brief Represents a location on a map.
     * @details By default, the location will be sent by the user.
     * @details Alternatively, you can use input_message_content to send a message with the specified content instead of the location.
     * @note This will only work in Telegram versions released after 9 April, 2016. Older clients will ignore them.
     */
    class InlineQueryResultLocation : public InlineQueryResult
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<InlineQueryResultLocation>;

    private:
        //Member variables
        /**
         * @var m_heading
         * @brief Optional. For live locations, a direction in which the user is moving, in degrees.
         * @details Must be between 1 and 360 if specified.
         */
        std::optional<std::int32_t> m_heading;
        /**
         * @var m_horizontal_accuracy
         * @brief Optional. The radius of uncertainty for the location, measured in meters; 0-1500.
         */
        std::optional<float> m_horizontal_accuracy;
        /**
         * @var m_id
         * @brief Unique identifier for this result, 1-64 Bytes.
         */
        std::string m_id;
        /**
         * @var m_input_message_content
         * @brief Optional. Content of the message to be sent instead of the location.
         */
        std::optional<InputMessageContent::ptr> m_input_message_content;
        /**
         * @var m_latitude
         * @brief Location latitude in degrees.
         */
        float m_latitude;
        /**
         * @var m_live_period
         * @brief Optional. Period in seconds for which the location can be updated, should be between 60 and 86400.
         */
        std::optional<std::int32_t> m_live_period;
        /**
         * @var m_longitude
         * @brief Location longitude in degrees.
         */
        float m_longitude;
        /**
         * @var m_proximity_alert_radius
         * @brief Optional. For live locations, a maximum distance for proximity alerts about approaching another chat member, in meters.
         * @details Must be between 1 and 100000 if specified.
         */
        std::optional<std::int32_t> m_proximity_alert_radius;
        /**
         * @var m_reply_markup
         * @brief Optional. Inline keyboard attached to the message.
         */
        std::optional<InlineKeyboardMarkup::ptr> m_reply_markup;
        /**
         * @var m_thumbnail_height
         * @brief Optional. Thumbnail height.
         */
        std::optional<std::int32_t> m_thumbnail_height;
        /**
         * @var m_thumbnail_url
         * @brief Optional. Url of the thumbnail for the result.
         */
        std::optional<std::string> m_thumbnail_url;
        /**
         * @var m_thumbnail_width
         * @brief Optional. Thumbnail width.
         */
        std::optional<std::int32_t> m_thumbnail_width;
        /**
         * @var m_title
         * @brief Location title.
         */
        std::string m_title;
        /**
         * @var m_type
         * @brief Type of the result, must be location.
         */
        std::string m_type;

    public:
        //Constructors
        InlineQueryResultLocation();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit InlineQueryResultLocation(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_heading
         */
        std::optional<std::int32_t> get_heading() const;

        /**
         * @brief Getter.
         * @return m_horizontal_accuracy
         */
        std::optional<float> get_horizontal_accuracy() const;

        /**
         * @brief Getter.
         * @return m_id
         */
        std::string get_id() const;

        /**
         * @brief Getter.
         * @return m_input_message_content
         */
        std::optional<InputMessageContent::ptr> get_input_message_content() const;

        /**
         * @brief Getter.
         * @return m_latitude
         */
        float get_latitude() const;

        /**
         * @brief Getter.
         * @return m_live_period
         */
        std::optional<std::int32_t> get_live_period() const;

        /**
         * @brief Getter.
         * @return m_longitude
         */
        float get_longitude() const;

        /**
         * @brief Getter.
         * @return m_proximity_alert_radius
         */
        std::optional<std::int32_t> get_proximity_alert_radius() const;

        /**
         * @brief Getter.
         * @return m_reply_markup
         */
        std::optional<InlineKeyboardMarkup::ptr> get_reply_markup() const;

        /**
         * @brief Getter.
         * @return m_thumbnail_height
         */
        std::optional<std::int32_t> get_thumbnail_height() const;

        /**
         * @brief Getter.
         * @return m_thumbnail_url
         */
        std::optional<std::string> get_thumbnail_url() const;

        /**
         * @brief Getter.
         * @return m_thumbnail_width
         */
        std::optional<std::int32_t> get_thumbnail_width() const;

        /**
         * @brief Getter.
         * @return m_title
         */
        std::string get_title() const;

        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        //Setter
        /**
         * @brief Setter for m_heading.
         * @param[in] heading See the member variable.
         */
        void set_heading(const std::optional<std::int32_t> &heading);

        /**
         * @brief Setter for m_horizontal_accuracy.
         * @param[in] horizontal_accuracy See the member variable.
         */
        void set_horizontal_accuracy(const std::optional<float> &horizontal_accuracy);

        /**
         * @brief Setter for m_id.
         * @param[in] id See the member variable.
         */
        void set_id(const std::string &id);

        /**
         * @brief Setter for m_input_message_content.
         * @param[in] input_message_content See the member variable.
         */
        void set_input_message_content(const std::optional<InputMessageContent::ptr> &input_message_content);

        /**
         * @brief Setter for m_latitude.
         * @param[in] latitude See the member variable.
         */
        void set_latitude(const float &latitude);

        /**
         * @brief Setter for m_live_period.
         * @param[in] live_period See the member variable.
         */
        void set_live_period(const std::optional<std::int32_t> &live_period);

        /**
         * @brief Setter for m_longitude.
         * @param[in] longitude See the member variable.
         */
        void set_longitude(const float &longitude);

        /**
         * @brief Setter for m_proximity_alert_radius.
         * @param[in] proximity_alert_radius See the member variable.
         */
        void set_proximity_alert_radius(const std::optional<std::int32_t> &proximity_alert_radius);

        /**
         * @brief Setter for m_reply_markup.
         * @param[in] reply_markup See the member variable.
         */
        void set_reply_markup(const std::optional<InlineKeyboardMarkup::ptr> &reply_markup);

        /**
         * @brief Setter for m_thumbnail_height.
         * @param[in] thumbnail_height See the member variable.
         */
        void set_thumbnail_height(const std::optional<std::int32_t> &thumbnail_height);

        /**
         * @brief Setter for m_thumbnail_url.
         * @param[in] thumbnail_url See the member variable.
         */
        void set_thumbnail_url(const std::optional<std::string> &thumbnail_url);

        /**
         * @brief Setter for m_thumbnail_width.
         * @param[in] thumbnail_width See the member variable.
         */
        void set_thumbnail_width(const std::optional<std::int32_t> &thumbnail_width);

        /**
         * @brief Setter for m_title.
         * @param[in] title See the member variable.
         */
        void set_title(const std::string &title);

        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);
    };
} //namespace tgbot

#endif
