#ifndef TGBOT_TYPES_LOGINURL_H
#define TGBOT_TYPES_LOGINURL_H

#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class LoginUrl
     * @brief This object represents a parameter of the inline keyboard button used to automatically authorize a user.
     * @details Serves as a great replacement for the Telegram Login Widget when the user is coming from Telegram.
     * @details All the user needs to do is tap/click a button and confirm that they want to log in:
     * @details Telegram apps support these buttons as of version 5.7.
     * @details Sample bot: \@discussbot
     */
    class LoginUrl : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<LoginUrl>;

    private:
        //Member variables
        /**
         * @var m_bot_username
         * @brief Optional. Username of a bot, which will be used for user authorization.
         * @details See Setting up a bot for more details.
         * @details If not specified, the current bot's username will be assumed.
         * @details The url's domain must be the same as the domain linked with the bot.
         * @details See Linking your domain to the bot for more details.
         */
        std::optional<std::string> m_bot_username;
        /**
         * @var m_forward_text
         * @brief Optional. New text of the button in forwarded messages.
         */
        std::optional<std::string> m_forward_text;
        /**
         * @var m_request_write_access
         * @brief Optional. Pass True to request the permission for your bot to send messages to the user.
         */
        std::optional<bool> m_request_write_access;
        /**
         * @var m_url
         * @brief An HTTPS URL to be opened with user authorization data added to the query string when the button is pressed.
         * @details If the user refuses to provide authorization data, the original URL without information about the user will be opened.
         * @details The data added is the same as described in Receiving authorization data.
         * @note You must always check the hash of the received data to verify the authentication and the integrity of the data as described in Checking authorization.
         */
        std::string m_url;

    public:
        //Constructors
        LoginUrl();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit LoginUrl(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_bot_username
         */
        std::optional<std::string> get_bot_username() const;

        /**
         * @brief Getter.
         * @return m_forward_text
         */
        std::optional<std::string> get_forward_text() const;

        /**
         * @brief Getter.
         * @return m_request_write_access
         */
        std::optional<bool> get_request_write_access() const;

        /**
         * @brief Getter.
         * @return m_url
         */
        std::string get_url() const;

        //Setter
        /**
         * @brief Setter for m_bot_username.
         * @param[in] bot_username See the member variable.
         */
        void set_bot_username(const std::optional<std::string> &bot_username);

        /**
         * @brief Setter for m_forward_text.
         * @param[in] forward_text See the member variable.
         */
        void set_forward_text(const std::optional<std::string> &forward_text);

        /**
         * @brief Setter for m_request_write_access.
         * @param[in] request_write_access See the member variable.
         */
        void set_request_write_access(const std::optional<bool> &request_write_access);

        /**
         * @brief Setter for m_url.
         * @param[in] url See the member variable.
         */
        void set_url(const std::string &url);
    };
} //namespace tgbot

#endif
