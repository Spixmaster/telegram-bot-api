#ifndef TGBOT_TYPES_REPLYKEYBOARDMARKUP_H
#define TGBOT_TYPES_REPLYKEYBOARDMARKUP_H

#include <memory>
#include <optional>
#include <string>
#include <vector>

#include "tgbot/types/KeyboardButton.h"
#include "tgbot/types/Reply.h"

namespace tgbot
{
    /**
     * @class ReplyKeyboardMarkup
     * @brief This object represents a custom keyboard with reply options (see Introduction to bots for details and examples).
     */
    class ReplyKeyboardMarkup : public Reply
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<ReplyKeyboardMarkup>;

    private:
        //Member variables
        /**
         * @var m_input_field_placeholder
         * @brief Optional. The placeholder to be shown in the input field when the keyboard is active; 1-64 characters.
         */
        std::optional<std::string> m_input_field_placeholder;
        /**
         * @var m_is_persistent
         * @brief Optional. Requests clients to always show the keyboard when the regular keyboard is hidden.
         * @details Defaults to false, in which case the custom keyboard can be hidden and opened with a keyboard icon.
         */
        std::optional<bool> m_is_persistent;
        /**
         * @var m_keyboard
         * @brief Array of button rows, each represented by an Array of KeyboardButton objects.
         */
        std::vector<std::vector<KeyboardButton::ptr>> m_keyboard;
        /**
         * @var m_one_time_keyboard
         * @brief Optional. Requests clients to hide the keyboard as soon as it's been used.
         * @details The keyboard will still be available, but clients will automatically display the usual letter-keyboard in the chat - the user can press a special button in the input field to see the custom keyboard again.
         * @details Defaults to false.
         */
        std::optional<bool> m_one_time_keyboard;
        /**
         * @var m_resize_keyboard
         * @brief Optional. Requests clients to resize the keyboard vertically for optimal fit (e.g., make the keyboard smaller if there are just two rows of buttons).
         * @details Defaults to false, in which case the custom keyboard is always of the same height as the app's standard keyboard.
         */
        std::optional<bool> m_resize_keyboard;
        /**
         * @var m_selective
         * @brief Optional. Use this parameter if you want to show the keyboard to specific users only.
         * @details Targets: 1) users that are \@mentioned in the text of the Message object; 2) if the bot's message is a reply (has reply_to_message_id), sender of the original message.
         * @details Example: A user requests to change the bot's language, bot replies to the request with a keyboard to select the new language. Other users in the group don't see the keyboard.
         */
        std::optional<bool> m_selective;

    public:
        //Constructors
        ReplyKeyboardMarkup();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit ReplyKeyboardMarkup(const std::string &json);

        /**
         * @param[in] keyboard Vector of vectors of buttons represents a keyboard. One vector makes up a line.
         */
        explicit ReplyKeyboardMarkup(std::vector<std::vector<KeyboardButton::ptr>> keyboard);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_input_field_placeholder
         */
        std::optional<std::string> get_input_field_placeholder() const;

        /**
         * @brief Getter.
         * @return m_is_persistent
         */
        std::optional<bool> get_is_persistent() const;

        /**
         * @brief Getter.
         * @return m_keyboard
         */
        std::vector<std::vector<KeyboardButton::ptr>> get_keyboard() const;

        /**
         * @brief Getter.
         * @return m_one_time_keyboard
         */
        std::optional<bool> get_one_time_keyboard() const;

        /**
         * @brief Getter.
         * @return m_resize_keyboard
         */
        std::optional<bool> get_resize_keyboard() const;

        /**
         * @brief Getter.
         * @return m_selective
         */
        std::optional<bool> get_selective() const;

        //Setter
        /**
         * @brief Setter for m_input_field_placeholder.
         * @param[in] input_field_placeholder See the member variable.
         */
        void set_input_field_placeholder(const std::optional<std::string> &input_field_placeholder);

        /**
         * @brief Setter for m_is_persistent.
         * @param[in] is_persistent See the member variable.
         */
        void set_is_persistent(const std::optional<bool> &is_persistent);

        /**
         * @brief Setter for m_keyboard.
         * @param[in] keyboard See the member variable.
         */
        void set_keyboard(const std::vector<std::vector<KeyboardButton::ptr>> &keyboard);

        /**
         * @brief Setter for m_one_time_keyboard.
         * @param[in] one_time_keyboard See the member variable.
         */
        void set_one_time_keyboard(const std::optional<bool> &one_time_keyboard);

        /**
         * @brief Setter for m_resize_keyboard.
         * @param[in] resize_keyboard See the member variable.
         */
        void set_resize_keyboard(const std::optional<bool> &resize_keyboard);

        /**
         * @brief Setter for m_selective.
         * @param[in] selective See the member variable.
         */
        void set_selective(const std::optional<bool> &selective);
    };
} //namespace tgbot

#endif
