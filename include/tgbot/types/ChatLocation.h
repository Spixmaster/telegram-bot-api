#ifndef TGBOT_TYPES_CHATLOCATION_H
#define TGBOT_TYPES_CHATLOCATION_H

#include <memory>
#include <string>

#include "tgbot/Type.h"
#include "tgbot/types/Location.h"

namespace tgbot
{
    /**
     * @class ChatLocation
     * @brief Represents a location to which a chat is connected.
     */
    class ChatLocation : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<ChatLocation>;

    private:
        //Member variables
        /**
         * @var m_address
         * @brief Location address; 1-64 characters, as defined by the chat owner.
         */
        std::string m_address;
        /**
         * @var m_location
         * @brief The location to which the supergroup is connected.
         * @details Can't be a live location.
         */
        Location::ptr m_location;

    public:
        //Constructors
        ChatLocation();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit ChatLocation(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_address
         */
        std::string get_address() const;

        /**
         * @brief Getter.
         * @return m_location
         */
        Location::ptr get_location() const;

        //Setter
        /**
         * @brief Setter for m_address.
         * @param[in] address See the member variable.
         */
        void set_address(const std::string &address);

        /**
         * @brief Setter for m_location.
         * @param[in] location See the member variable.
         */
        void set_location(const Location::ptr &location);
    };
} //namespace tgbot

#endif
