#ifndef TGBOT_TYPES_CHATMEMBERUPDATED_H
#define TGBOT_TYPES_CHATMEMBERUPDATED_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"
#include "tgbot/types/Chat.h"
#include "tgbot/types/ChatInviteLink.h"
#include "tgbot/types/ChatMember.h"
#include "tgbot/types/User.h"

namespace tgbot
{
    /**
     * @class ChatMemberUpdated
     * @brief This object represents changes in the status of a chat member.
     */
    class ChatMemberUpdated : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<ChatMemberUpdated>;

    private:
        //Member variables
        /**
         * @var m_chat
         * @brief Chat the user belongs to.
         */
        Chat::ptr m_chat;
        /**
         * @var m_date
         * @brief Date the change was done in Unix time.
         */
        std::uint64_t m_date;
        /**
         * @var m_from
         * @brief Performer of the action, which resulted in the change.
         */
        User::ptr m_from;
        /**
         * @var m_invite_link
         * @brief Optional. Chat invite link, which was used by the user to join the chat; for joining by invite link events only.
         */
        std::optional<ChatInviteLink::ptr> m_invite_link;
        /**
         * @var m_new_chat_member
         * @brief New information about the chat member.
         */
        ChatMember::ptr m_new_chat_member;
        /**
         * @var m_old_chat_member
         * @brief Previous information about the chat member.
         */
        ChatMember::ptr m_old_chat_member;

    public:
        //Constructors
        ChatMemberUpdated();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit ChatMemberUpdated(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_chat
         */
        Chat::ptr get_chat() const;

        /**
         * @brief Getter.
         * @return m_date
         */
        std::uint64_t get_date() const;

        /**
         * @brief Getter.
         * @return m_from
         */
        User::ptr get_from() const;

        /**
         * @brief Getter.
         * @return m_invite_link
         */
        std::optional<ChatInviteLink::ptr> get_invite_link() const;

        /**
         * @brief Getter.
         * @return m_new_chat_member
         */
        ChatMember::ptr get_new_chat_member() const;

        /**
         * @brief Getter.
         * @return m_old_chat_member
         */
        ChatMember::ptr get_old_chat_member() const;

        //Setter
        /**
         * @brief Setter for m_chat.
         * @param[in] chat See the member variable.
         */
        void set_chat(const Chat::ptr &chat);

        /**
         * @brief Setter for m_date.
         * @param[in] date See the member variable.
         */
        void set_date(const std::uint64_t &date);

        /**
         * @brief Setter for m_from.
         * @param[in] from See the member variable.
         */
        void set_from(const User::ptr &from);

        /**
         * @brief Setter for m_invite_link.
         * @param[in] invite_link See the member variable.
         */
        void set_invite_link(const std::optional<ChatInviteLink::ptr> &invite_link);

        /**
         * @brief Setter for m_new_chat_member.
         * @param[in] new_chat_member See the member variable.
         */
        void set_new_chat_member(const ChatMember::ptr &new_chat_member);

        /**
         * @brief Setter for m_old_chat_member.
         * @param[in] old_chat_member See the member variable.
         */
        void set_old_chat_member(const ChatMember::ptr &old_chat_member);
    };
} //namespace tgbot

#endif
