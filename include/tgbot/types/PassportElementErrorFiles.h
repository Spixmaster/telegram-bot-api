#ifndef TGBOT_TYPES_PASSPORTELEMENTERRORFILES_H
#define TGBOT_TYPES_PASSPORTELEMENTERRORFILES_H

#include <memory>
#include <string>
#include <vector>

#include "tgbot/types/PassportElementError.h"

namespace tgbot
{
    /**
     * @class PassportElementErrorFiles
     * @brief Represents an issue with a list of scans.
     * @details The error is considered resolved when the list of files containing the scans changes.
     */
    class PassportElementErrorFiles : public PassportElementError
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<PassportElementErrorFiles>;

    private:
        //Member variables
        /**
         * @var m_file_hashes
         * @brief List of base64-encoded file hashes.
         */
        std::vector<std::string> m_file_hashes;
        /**
         * @var m_message
         * @brief Error message.
         */
        std::string m_message;
        /**
         * @var m_source
         * @brief Error source, must be files.
         */
        std::string m_source;
        /**
         * @var m_type
         * @brief The section of the user's Telegram Passport which has the issue, one of “utility_bill”, “bank_statement”, “rental_agreement”, “passport_registration”, “temporary_registration”.
         */
        std::string m_type;

    public:
        //Constructors
        PassportElementErrorFiles();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit PassportElementErrorFiles(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_file_hashes
         */
        std::vector<std::string> get_file_hashes() const;

        /**
         * @brief Getter.
         * @return m_message
         */
        std::string get_message() const;

        /**
         * @brief Getter.
         * @return m_source
         */
        std::string get_source() const;

        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        //Setter
        /**
         * @brief Setter for m_file_hashes.
         * @param[in] file_hashes See the member variable.
         */
        void set_file_hashes(const std::vector<std::string> &file_hashes);

        /**
         * @brief Setter for m_message.
         * @param[in] message See the member variable.
         */
        void set_message(const std::string &message);

        /**
         * @brief Setter for m_source.
         * @param[in] source See the member variable.
         */
        void set_source(const std::string &source);

        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);
    };
} //namespace tgbot

#endif
