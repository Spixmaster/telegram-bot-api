#ifndef TGBOT_TYPES_SUCCESSFULPAYMENT_H
#define TGBOT_TYPES_SUCCESSFULPAYMENT_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"
#include "tgbot/types/OrderInfo.h"

namespace tgbot
{
    /**
     * @class SuccessfulPayment
     * @brief This object contains basic information about a successful payment.
     */
    class SuccessfulPayment : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<SuccessfulPayment>;

    private:
        //Member variables
        /**
         * @var m_currency
         * @brief Three-letter ISO 4217 currency code.
         */
        std::string m_currency;
        /**
         * @var m_invoice_payload
         * @brief Bot specified invoice payload.
         */
        std::string m_invoice_payload;
        /**
         * @var m_order_info
         * @brief Optional. Order information provided by the user.
         */
        std::optional<OrderInfo::ptr> m_order_info;
        /**
         * @var m_provider_payment_charge_id
         * @brief Provider payment identifier.
         */
        std::string m_provider_payment_charge_id;
        /**
         * @var m_shipping_option_id
         * @brief Optional. Identifier of the shipping option chosen by the user.
         */
        std::optional<std::string> m_shipping_option_id;
        /**
         * @var m_telegram_payment_charge_id
         * @brief Telegram payment identifier.
         */
        std::string m_telegram_payment_charge_id;
        /**
         * @var m_total_amount
         * @brief Total price in the smallest units of the currency (integer, not float/double).
         * @details For example, for a price of US$ 1.45 pass amount = 145.
         * @details See the exp parameter in currencies.json, it shows the number of digits past the decimal point for each currency (2 for the majority of currencies).
         */
        std::int32_t m_total_amount;

    public:
        //Constructors
        SuccessfulPayment();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit SuccessfulPayment(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_currency
         */
        std::string get_currency() const;

        /**
         * @brief Getter.
         * @return m_invoice_payload
         */
        std::string get_invoice_payload() const;

        /**
         * @brief Getter.
         * @return m_order_info
         */
        std::optional<OrderInfo::ptr> get_order_info() const;

        /**
         * @brief Getter.
         * @return m_provider_payment_charge_id
         */
        std::string get_provider_payment_charge_id() const;

        /**
         * @brief Getter.
         * @return m_shipping_option_id
         */
        std::optional<std::string> get_shipping_option_id() const;

        /**
         * @brief Getter.
         * @return m_telegram_payment_charge_id
         */
        std::string get_telegram_payment_charge_id() const;

        /**
         * @brief Getter.
         * @return m_total_amount
         */
        std::int32_t get_total_amount() const;

        //Setter
        /**
         * @brief Setter for m_currency.
         * @param[in] currency See the member variable.
         */
        void set_currency(const std::string &currency);

        /**
         * @brief Setter for m_invoice_payload.
         * @param[in] invoice_payload See the member variable.
         */
        void set_invoice_payload(const std::string &invoice_payload);

        /**
         * @brief Setter for m_order_info.
         * @param[in] order_info See the member variable.
         */
        void set_order_info(const std::optional<OrderInfo::ptr> &order_info);

        /**
         * @brief Setter for m_provider_payment_charge_id.
         * @param[in] provider_payment_charge_id See the member variable.
         */
        void set_provider_payment_charge_id(const std::string &provider_payment_charge_id);

        /**
         * @brief Setter for m_shipping_option_id.
         * @param[in] shipping_option_id See the member variable.
         */
        void set_shipping_option_id(const std::optional<std::string> &shipping_option_id);

        /**
         * @brief Setter for m_telegram_payment_charge_id.
         * @param[in] telegram_payment_charge_id See the member variable.
         */
        void set_telegram_payment_charge_id(const std::string &telegram_payment_charge_id);

        /**
         * @brief Setter for m_total_amount.
         * @param[in] total_amount See the member variable.
         */
        void set_total_amount(const std::int32_t &total_amount);
    };
} //namespace tgbot

#endif
