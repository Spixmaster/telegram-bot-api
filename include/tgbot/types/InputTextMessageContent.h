#ifndef TGBOT_TYPES_INPUTTEXTMESSAGECONTENT_H
#define TGBOT_TYPES_INPUTTEXTMESSAGECONTENT_H

#include <memory>
#include <optional>
#include <string>
#include <vector>

#include "tgbot/types/InputMessageContent.h"
#include "tgbot/types/MessageEntity.h"

namespace tgbot
{
    /**
     * @class InputTextMessageContent
     * @brief Represents the content of a text message to be sent as the result of an inline query.
     */
    class InputTextMessageContent : public InputMessageContent
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<InputTextMessageContent>;

    private:
        //Member variables
        /**
         * @var m_disable_web_page_preview
         * @brief Optional. Disables link previews for links in the sent message.
         */
        std::optional<bool> m_disable_web_page_preview;
        /**
         * @var m_entities
         * @brief Optional. List of special entities that appear in message text, which can be specified instead of parse_mode.
         */
        std::optional<std::vector<MessageEntity::ptr>> m_entities;
        /**
         * @var m_message_text
         * @brief Text of the message to be sent, 1-4096 characters.
         */
        std::string m_message_text;
        /**
         * @var m_parse_mode
         * @brief Optional. Mode for parsing entities in the message text.
         * @details See formatting options for more details.
         */
        std::optional<std::string> m_parse_mode;

    public:
        //Constructors
        InputTextMessageContent();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit InputTextMessageContent(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_disable_web_page_preview
         */
        std::optional<bool> get_disable_web_page_preview() const;

        /**
         * @brief Getter.
         * @return m_entities
         */
        std::optional<std::vector<MessageEntity::ptr>> get_entities() const;

        /**
         * @brief Getter.
         * @return m_message_text
         */
        std::string get_message_text() const;

        /**
         * @brief Getter.
         * @return m_parse_mode
         */
        std::optional<std::string> get_parse_mode() const;

        //Setter
        /**
         * @brief Setter for m_disable_web_page_preview.
         * @param[in] disable_web_page_preview See the member variable.
         */
        void set_disable_web_page_preview(const std::optional<bool> &disable_web_page_preview);

        /**
         * @brief Setter for m_entities.
         * @param[in] entities See the member variable.
         */
        void set_entities(const std::optional<std::vector<MessageEntity::ptr>> &entities);

        /**
         * @brief Setter for m_message_text.
         * @param[in] message_text See the member variable.
         */
        void set_message_text(const std::string &message_text);

        /**
         * @brief Setter for m_parse_mode.
         * @param[in] parse_mode See the member variable.
         */
        void set_parse_mode(const std::optional<std::string> &parse_mode);
    };
} //namespace tgbot

#endif
