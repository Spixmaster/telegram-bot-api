#ifndef TGBOT_TYPES_INLINEQUERYRESULTPHOTO_H
#define TGBOT_TYPES_INLINEQUERYRESULTPHOTO_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>
#include <vector>

#include "tgbot/types/InlineKeyboardMarkup.h"
#include "tgbot/types/InlineQueryResult.h"
#include "tgbot/types/InputMessageContent.h"
#include "tgbot/types/MessageEntity.h"

namespace tgbot
{
    /**
     * @class InlineQueryResultPhoto
     * @brief Represents a link to a photo.
     * @details By default, this photo will be sent by the user with optional caption.
     * @details Alternatively, you can use input_message_content to send a message with the specified content instead of the photo.
     */
    class InlineQueryResultPhoto : public InlineQueryResult
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<InlineQueryResultPhoto>;

    private:
        //Member variables
        /**
         * @var m_caption
         * @brief Optional. Caption of the photo to be sent, 0-1024 characters after entities parsing.
         */
        std::optional<std::string> m_caption;
        /**
         * @var m_caption_entities
         * @brief Optional. List of special entities that appear in the caption, which can be specified instead of parse_mode.
         */
        std::optional<std::vector<MessageEntity::ptr>> m_caption_entities;
        /**
         * @var m_description
         * @brief Optional. Short description of the result.
         */
        std::optional<std::string> m_description;
        /**
         * @var m_id
         * @brief Unique identifier for this result, 1-64 bytes.
         */
        std::string m_id;
        /**
         * @var m_input_message_content
         * @brief Optional. Content of the message to be sent instead of the photo.
         */
        std::optional<InputMessageContent::ptr> m_input_message_content;
        /**
         * @var m_parse_mode
         * @brief Optional. Mode for parsing entities in the photo caption.
         * @details See formatting options for more details.
         */
        std::optional<std::string> m_parse_mode;
        /**
         * @var m_photo_height
         * @brief Optional. Height of the photo.
         */
        std::optional<std::int32_t> m_photo_height;
        /**
         * @var m_photo_url
         * @brief A valid URL of the photo.
         * @details Photo must be in JPEG format.
         * @details Photo size must not exceed 5MB.
         */
        std::string m_photo_url;
        /**
         * @var m_photo_width
         * @brief Optional. Width of the photo.
         */
        std::optional<std::int32_t> m_photo_width;
        /**
         * @var m_reply_markup
         * @brief Optional. Inline keyboard attached to the message.
         */
        std::optional<InlineKeyboardMarkup::ptr> m_reply_markup;
        /**
         * @var m_thumbnail_url
         * @brief URL of the thumbnail for the photo.
         */
        std::string m_thumbnail_url;
        /**
         * @var m_title
         * @brief Optional. Title for the result.
         */
        std::optional<std::string> m_title;
        /**
         * @var m_type
         * @brief Type of the result, must be photo.
         */
        std::string m_type;

    public:
        //Constructors
        InlineQueryResultPhoto();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit InlineQueryResultPhoto(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_caption
         */
        std::optional<std::string> get_caption() const;

        /**
         * @brief Getter.
         * @return m_caption_entities
         */
        std::optional<std::vector<MessageEntity::ptr>> get_caption_entities() const;

        /**
         * @brief Getter.
         * @return m_description
         */
        std::optional<std::string> get_description() const;

        /**
         * @brief Getter.
         * @return m_id
         */
        std::string get_id() const;

        /**
         * @brief Getter.
         * @return m_input_message_content
         */
        std::optional<InputMessageContent::ptr> get_input_message_content() const;

        /**
         * @brief Getter.
         * @return m_parse_mode
         */
        std::optional<std::string> get_parse_mode() const;

        /**
         * @brief Getter.
         * @return m_photo_height
         */
        std::optional<std::int32_t> get_photo_height() const;

        /**
         * @brief Getter.
         * @return m_photo_url
         */
        std::string get_photo_url() const;

        /**
         * @brief Getter.
         * @return m_photo_width
         */
        std::optional<std::int32_t> get_photo_width() const;

        /**
         * @brief Getter.
         * @return m_reply_markup
         */
        std::optional<InlineKeyboardMarkup::ptr> get_reply_markup() const;

        /**
         * @brief Getter.
         * @return m_thumbnail_url
         */
        std::string get_thumbnail_url() const;

        /**
         * @brief Getter.
         * @return m_title
         */
        std::optional<std::string> get_title() const;

        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        //Setter
        /**
         * @brief Setter for m_caption.
         * @param[in] caption See the member variable.
         */
        void set_caption(const std::optional<std::string> &caption);

        /**
         * @brief Setter for m_caption_entities.
         * @param[in] caption_entities See the member variable.
         */
        void set_caption_entities(const std::optional<std::vector<MessageEntity::ptr>> &caption_entities);

        /**
         * @brief Setter for m_description.
         * @param[in] description See the member variable.
         */
        void set_description(const std::optional<std::string> &description);

        /**
         * @brief Setter for m_id.
         * @param[in] id See the member variable.
         */
        void set_id(const std::string &id);

        /**
         * @brief Setter for m_input_message_content.
         * @param[in] input_message_content See the member variable.
         */
        void set_input_message_content(const std::optional<InputMessageContent::ptr> &input_message_content);

        /**
         * @brief Setter for m_parse_mode.
         * @param[in] parse_mode See the member variable.
         */
        void set_parse_mode(const std::optional<std::string> &parse_mode);

        /**
         * @brief Setter for m_photo_height.
         * @param[in] photo_height See the member variable.
         */
        void set_photo_height(const std::optional<std::int32_t> &photo_height);

        /**
         * @brief Setter for m_photo_url.
         * @param[in] photo_url See the member variable.
         */
        void set_photo_url(const std::string &photo_url);

        /**
         * @brief Setter for m_photo_width.
         * @param[in] photo_width See the member variable.
         */
        void set_photo_width(const std::optional<std::int32_t> &photo_width);

        /**
         * @brief Setter for m_reply_markup.
         * @param[in] reply_markup See the member variable.
         */
        void set_reply_markup(const std::optional<InlineKeyboardMarkup::ptr> &reply_markup);

        /**
         * @brief Setter for m_thumbnail_url.
         * @param[in] thumbnail_url See the member variable.
         */
        void set_thumbnail_url(const std::string &thumbnail_url);

        /**
         * @brief Setter for m_title.
         * @param[in] title See the member variable.
         */
        void set_title(const std::optional<std::string> &title);

        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);
    };
} //namespace tgbot

#endif
