#ifndef TGBOT_TYPES_POLL_H
#define TGBOT_TYPES_POLL_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>
#include <vector>

#include "tgbot/Type.h"
#include "tgbot/types/MessageEntity.h"
#include "tgbot/types/PollOption.h"

namespace tgbot
{
    /**
     * @class Poll
     * @brief This object contains information about a poll.
     */
    class Poll : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<Poll>;

    private:
        //Member variables
        /**
         * @var m_allows_multiple_answers
         * @brief True, if the poll allows multiple answers.
         */
        bool m_allows_multiple_answers;
        /**
         * @var m_close_date
         * @brief Optional. Point in time (Unix timestamp) when the poll will be automatically closed.
         */
        std::optional<std::uint64_t> m_close_date;
        /**
         * @var m_correct_option_id
         * @brief Optional. 0-based identifier of the correct answer option.
         * @details Available only for polls in the quiz mode, which are closed, or was sent (not forwarded) by the bot or to the private chat with the bot.
         */
        std::optional<std::int32_t> m_correct_option_id;
        /**
         * @var m_explanation
         * @brief Optional. Text that is shown when a user chooses an incorrect answer or taps on the lamp icon in a quiz-style poll, 0-200 characters.
         */
        std::optional<std::string> m_explanation;
        /**
         * @var m_explanation_entities
         * @brief Optional. Special entities like usernames, URLs, bot commands, etc. that appear in the explanation.
         */
        std::optional<std::vector<MessageEntity::ptr>> m_explanation_entities;
        /**
         * @var m_id
         * @brief Unique poll identifier.
         */
        std::string m_id;
        /**
         * @var m_is_anonymous
         * @brief True, if the poll is anonymous.
         */
        bool m_is_anonymous;
        /**
         * @var m_is_closed
         * @brief True, if the poll is closed.
         */
        bool m_is_closed;
        /**
         * @var m_open_period
         * @brief Optional. Amount of time in seconds the poll will be active after creation.
         */
        std::optional<std::int32_t> m_open_period;
        /**
         * @var m_options
         * @brief List of poll options.
         */
        std::vector<PollOption::ptr> m_options;
        /**
         * @var m_question
         * @brief Poll question, 1-300 characters.
         */
        std::string m_question;
        /**
         * @var m_total_voter_count
         * @brief Total number of users that voted in the poll.
         */
        std::int32_t m_total_voter_count;
        /**
         * @var m_type
         * @brief Poll type, currently can be “regular” or “quiz”.
         */
        std::string m_type;

    public:
        //Constructors
        Poll();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit Poll(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_allows_multiple_answers
         */
        bool get_allows_multiple_answers() const;

        /**
         * @brief Getter.
         * @return m_close_date
         */
        std::optional<std::uint64_t> get_close_date() const;

        /**
         * @brief Getter.
         * @return m_correct_option_id
         */
        std::optional<std::int32_t> get_correct_option_id() const;

        /**
         * @brief Getter.
         * @return m_explanation
         */
        std::optional<std::string> get_explanation() const;

        /**
         * @brief Getter.
         * @return m_explanation_entities
         */
        std::optional<std::vector<MessageEntity::ptr>> get_explanation_entities() const;

        /**
         * @brief Getter.
         * @return m_id
         */
        std::string get_id() const;

        /**
         * @brief Getter.
         * @return m_is_anonymous
         */
        bool get_is_anonymous() const;

        /**
         * @brief Getter.
         * @return m_is_closed
         */
        bool get_is_closed() const;

        /**
         * @brief Getter.
         * @return m_open_period
         */
        std::optional<std::int32_t> get_open_period() const;

        /**
         * @brief Getter.
         * @return m_options
         */
        std::vector<PollOption::ptr> get_options() const;

        /**
         * @brief Getter.
         * @return m_question
         */
        std::string get_question() const;

        /**
         * @brief Getter.
         * @return m_total_voter_count
         */
        std::int32_t get_total_voter_count() const;

        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        //Setter
        /**
         * @brief Setter for m_allows_multiple_answers.
         * @param[in] allows_multiple_answers See the member variable.
         */
        void set_allows_multiple_answers(const bool &allows_multiple_answers);

        /**
         * @brief Setter for m_close_date.
         * @param[in] close_date See the member variable.
         */
        void set_close_date(const std::optional<std::uint64_t> &close_date);

        /**
         * @brief Setter for m_correct_option_id.
         * @param[in] correct_option_id See the member variable.
         */
        void set_correct_option_id(const std::optional<std::int32_t> &correct_option_id);

        /**
         * @brief Setter for m_explanation.
         * @param[in] explanation See the member variable.
         */
        void set_explanation(const std::optional<std::string> &explanation);

        /**
         * @brief Setter for explanation_entities.
         * @param[in] explanation_entities See the member variable.
         */
        void set_explanation_entities(const std::optional<std::vector<MessageEntity::ptr>> &explanation_entities);

        /**
         * @brief Setter for m_id.
         * @param[in] id See the member variable.
         */
        void set_id(const std::string &id);

        /**
         * @brief Setter for m_is_anonymous.
         * @param[in] is_anonymous See the member variable.
         */
        void set_is_anonymous(const bool &is_anonymous);

        /**
         * @brief Setter for m_is_closed.
         * @param[in] is_closed See the member variable.
         */
        void set_is_closed(const bool &is_closed);

        /**
         * @brief Setter for m_open_period.
         * @param[in] open_period See the member variable.
         */
        void set_open_period(const std::optional<std::int32_t> &open_period);

        /**
         * @brief Setter for m_options.
         * @param[in] options See the member variable.
         */
        void set_options(const std::vector<PollOption::ptr> &options);

        /**
         * @brief Setter for m_question.
         * @param[in] question See the member variable.
         */
        void set_question(const std::string &question);

        /**
         * @brief Setter for m_total_voter_count.
         * @param[in] total_voter_count See the member variable.
         */
        void set_total_voter_count(const std::int32_t &total_voter_count);

        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);
    };
} //namespace tgbot

#endif
