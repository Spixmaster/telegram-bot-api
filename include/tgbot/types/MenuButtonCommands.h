#ifndef TGBOT_TYPES_MENUBUTTONCOMMANDS_H
#define TGBOT_TYPES_MENUBUTTONCOMMANDS_H

#include <memory>
#include <string>

#include "tgbot/types/MenuButton.h"

namespace tgbot
{
    /**
     * @class MenuButtonCommands
     * @brief Represents a menu button, which opens the bot's list of commands.
     */
    class MenuButtonCommands : public MenuButton
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<MenuButtonCommands>;

    private:
        //Member variables
        /**
         * @var m_type
         * @brief Type of the button, must be commands.
         */
        std::string m_type;

    public:
        //Constructors
        MenuButtonCommands();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit MenuButtonCommands(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        //Setter
        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);
    };
} //namespace tgbot

#endif
