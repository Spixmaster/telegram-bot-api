#ifndef TGBOT_TYPES_SHIPPINGQUERY_H
#define TGBOT_TYPES_SHIPPINGQUERY_H

#include <memory>
#include <string>

#include "tgbot/Type.h"
#include "tgbot/types/ShippingAddress.h"
#include "tgbot/types/User.h"

namespace tgbot
{
    /**
     * @class ShippingQuery
     * @brief This object contains information about an incoming shipping query.
     */
    class ShippingQuery : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<ShippingQuery>;

    private:
        //Member variables
        /**
         * @var m_from
         * @brief User who sent the query.
         */
        User::ptr m_from;
        /**
         * @var m_id
         * @brief Unique query identifier.
         */
        std::string m_id;
        /**
         * @var m_invoice_payload
         * @brief Bot specified invoice payload.
         */
        std::string m_invoice_payload;
        /**
         * @var m_shipping_address
         * @brief User specified shipping address.
         */
        ShippingAddress::ptr m_shipping_address;

    public:
        //Constructors
        ShippingQuery();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit ShippingQuery(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_from
         */
        User::ptr get_from() const;

        /**
         * @brief Getter.
         * @return m_id
         */
        std::string get_id() const;

        /**
         * @brief Getter.
         * @return m_invoice_payload
         */
        std::string get_invoice_payload() const;

        /**
         * @brief Getter.
         * @return m_shipping_address
         */
        ShippingAddress::ptr get_shipping_address() const;

        //Setter
        /**
         * @brief Setter for m_from.
         * @param[in] from See the member variable.
         */
        void set_from(const User::ptr &from);

        /**
         * @brief Setter for m_id.
         * @param[in] id See the member variable.
         */
        void set_id(const std::string &id);

        /**
         * @brief Setter for m_invoice_payload.
         * @param[in] invoice_payload See the member variable.
         */
        void set_invoice_payload(const std::string &invoice_payload);

        /**
         * @brief Setter for m_shipping_address.
         * @param[in] shipping_address See the member variable.
         */
        void set_shipping_address(const ShippingAddress::ptr &shipping_address);
    };
} //namespace tgbot

#endif
