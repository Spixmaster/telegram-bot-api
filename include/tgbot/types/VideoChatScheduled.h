#ifndef TGBOT_TYPES_VIDEOCHATSCHEDULED_H
#define TGBOT_TYPES_VIDEOCHATSCHEDULED_H

#include <cstdint>
#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class VideoChatScheduled
     * @brief This object represents a service message about a video chat scheduled in the chat.
     */
    class VideoChatScheduled : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<VideoChatScheduled>;

    private:
        //Member variables
        /**
         * @var m_start_date
         * @brief Point in time (Unix timestamp) when the video chat is supposed to be started by a chat administrator.
         */
        std::uint64_t m_start_date;

    public:
        //Constructors
        VideoChatScheduled();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit VideoChatScheduled(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_start_date
         */
        std::uint64_t get_start_date() const;

        //Setter
        /**
         * @brief Setter for m_start_date.
         * @param[in] start_date See the member variable.
         */
        void set_start_date(const std::uint64_t &start_date);
    };
} //namespace tgbot

#endif
