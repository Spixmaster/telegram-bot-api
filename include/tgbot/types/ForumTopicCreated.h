#ifndef TGBOT_TYPES_FORUMTOPICCREATED_H
#define TGBOT_TYPES_FORUMTOPICCREATED_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class ForumTopicCreated
     * @brief This object represents a service message about a new forum topic created in the chat.
     */
    class ForumTopicCreated : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<ForumTopicCreated>;

    private:
        //Member variables
        /**
         * @var m_icon_color
         * @brief Color of the topic icon in RGB format.
         */
        std::int32_t m_icon_color;
        /**
         * @var m_icon_custom_emoji_id
         * @brief Optional. Unique identifier of the custom emoji shown as the topic icon.
         */
        std::optional<std::string> m_icon_custom_emoji_id;
        /**
         * @var m_name
         * @brief Name of the topic.
         */
        std::string m_name;

    public:
        //Constructors
        ForumTopicCreated();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit ForumTopicCreated(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_icon_color
         */
        std::int32_t get_icon_color() const;

        /**
         * @brief Getter.
         * @return m_icon_custom_emoji_id
         */
        std::optional<std::string> get_icon_custom_emoji_id() const;

        /**
         * @brief Getter.
         * @return m_name
         */
        std::string get_name() const;

        //Setter
        /**
         * @brief Setter for m_icon_color.
         * @param[in] icon_color See the member variable.
         */
        void set_icon_color(const std::int32_t &icon_color);

        /**
         * @brief Setter for m_icon_custom_emoji_id.
         * @param[in] icon_custom_emoji_id See the member variable.
         */
        void set_icon_custom_emoji_id(const std::optional<std::string> &icon_custom_emoji_id);

        /**
         * @brief Setter for m_name.
         * @param[in] name See the member variable.
         */
        void set_name(const std::string &name);
    };
} //namespace tgbot

#endif
