#ifndef TGBOT_TYPES_CHATMEMBERLEFT_H
#define TGBOT_TYPES_CHATMEMBERLEFT_H

#include <memory>
#include <string>

#include "tgbot/types/ChatMember.h"
#include "tgbot/types/User.h"

namespace tgbot
{
    /**
     * @class ChatMemberLeft
     * @brief Represents a chat member that isn't currently a member of the chat, but may join it themselves.
     */
    class ChatMemberLeft : public ChatMember
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<ChatMemberLeft>;

    private:
        //Member variables
        /**
         * @var m_status
         * @brief The member's status in the chat, always “left”.
         */
        std::string m_status;
        /**
         * @var m_user
         * @brief Information about the user.
         */
        User::ptr m_user;

    public:
        //Constructors
        ChatMemberLeft();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit ChatMemberLeft(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_status
         */
        std::string get_status() const;

        /**
         * @brief Getter.
         * @return m_user
         */
        User::ptr get_user() const;

        //Setter
        /**
         * @brief Setter for m_status.
         * @param[in] status See the member variable.
         */
        void set_status(const std::string &status);

        /**
         * @brief Setter for m_user.
         * @param[in] user See the member variable.
         */
        void set_user(const User::ptr &user);
    };
} //namespace tgbot

#endif
