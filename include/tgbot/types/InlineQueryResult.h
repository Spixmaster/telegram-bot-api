#ifndef TGBOT_TYPES_INLINEQUERYRESULT_H
#define TGBOT_TYPES_INLINEQUERYRESULT_H

#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class InlineQueryResult
     * @brief This object represents one result of an inline query.
     * @details Telegram clients currently support results of the following 20 types:
     * InlineQueryResultCachedAudio
     * InlineQueryResultCachedDocument
     * InlineQueryResultCachedGif
     * InlineQueryResultCachedMpeg4Gif
     * InlineQueryResultCachedPhoto
     * InlineQueryResultCachedSticker
     * InlineQueryResultCachedVideo
     * InlineQueryResultCachedVoice
     * InlineQueryResultArticle
     * InlineQueryResultAudio
     * InlineQueryResultContact
     * InlineQueryResultGame
     * InlineQueryResultDocument
     * InlineQueryResultGif
     * InlineQueryResultLocation
     * InlineQueryResultMpeg4Gif
     * InlineQueryResultPhoto
     * InlineQueryResultVenue
     * InlineQueryResultVideo
     * InlineQueryResultVoice
     * @note All URLs passed in inline query results will be available to end users and therefore must be assumed to be public.
     */
    class InlineQueryResult : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<InlineQueryResult>;

        //Constructors
        InlineQueryResult();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit InlineQueryResult(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;
    };
} //namespace tgbot

#endif
