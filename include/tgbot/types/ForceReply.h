#ifndef TGBOT_TYPES_FORCEREPLY_H
#define TGBOT_TYPES_FORCEREPLY_H

#include <memory>
#include <optional>
#include <string>

#include "tgbot/types/Reply.h"

namespace tgbot
{
    /**
     * @class ForceReply
     * @brief Upon receiving a message with this object, Telegram clients will display a reply interface to the user (act as if the user has selected the bot's message and tapped 'Reply').
     * @details This can be extremely useful if you want to create user-friendly step-by-step interfaces without having to sacrifice privacy mode.
     */
    class ForceReply : public Reply
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<ForceReply>;

    private:
        //Member variables
        /**
         * @var m_force_reply
         * @brief Shows reply interface to the user, as if they manually selected the bot's message and tapped 'Reply'.
         */
        bool m_force_reply;
        /**
         * @var m_input_field_placeholder
         * @brief Optional. The placeholder to be shown in the input field when the reply is active; 1-64 characters.
         */
        std::optional<std::string> m_input_field_placeholder;
        /**
         * @var m_selective
         * @brief Optional. Use this parameter if you want to force reply from specific users only.
         * @details Targets: 1) users that are \@mentioned in the text of the Message object; 2) if the bot's message is a reply (has reply_to_message_id), sender of the original message.
         */
        std::optional<bool> m_selective;

    public:
        //Constructors
        ForceReply();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit ForceReply(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_force_reply
         */
        bool get_force_reply() const;

        /**
         * @brief Getter.
         * @return m_input_field_placeholder
         */
        std::optional<std::string> get_input_field_placeholder() const;

        /**
         * @brief Getter.
         * @return m_selective
         */
        std::optional<bool> get_selective() const;

        //Setter
        /**
         * @brief Setter for m_force_reply.
         * @param[in] force_reply See the member variable.
         */
        void set_force_reply(const bool &force_reply);

        /**
         * @brief Setter for m_input_field_placeholder.
         * @param[in] input_field_placeholder See the member variable.
         */
        void set_input_field_placeholder(const std::optional<std::string> &input_field_placeholder);

        /**
         * @brief Setter for m_selective.
         * @param[in] selective See the member variable.
         */
        void set_selective(const std::optional<bool> &selective);
    };
} //namespace tgbot

#endif
