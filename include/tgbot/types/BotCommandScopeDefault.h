#ifndef TGBOT_TYPES_BOTCOMMANDSCOPEDEFAULT_H
#define TGBOT_TYPES_BOTCOMMANDSCOPEDEFAULT_H

#include <memory>
#include <string>

#include "tgbot/types/BotCommandScope.h"

namespace tgbot
{
    /**
     * @class BotCommandScopeDefault
     * @brief Represents the default scope of bot commands.
     * @details Default commands are used if no commands with a narrower scope are specified for the user.
     */
    class BotCommandScopeDefault : public BotCommandScope
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<BotCommandScopeDefault>;

    private:
        //Member variables
        /**
         * @var m_type
         * @brief Scope type, must be default.
         */
        std::string m_type;

    public:
        //Constructors
        BotCommandScopeDefault();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit BotCommandScopeDefault(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        //Setter
        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);
    };
} //namespace tgbot

#endif
