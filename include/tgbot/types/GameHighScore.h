#ifndef TGBOT_TYPES_GAMEHIGHSCORE_H
#define TGBOT_TYPES_GAMEHIGHSCORE_H

#include <cstdint>
#include <memory>
#include <string>

#include "tgbot/Type.h"
#include "tgbot/types/User.h"

namespace tgbot
{
    /**
     * @class GameHighScore
     * @brief This object represents one row of the high scores table for a game.
     */
    class GameHighScore : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<GameHighScore>;

    private:
        //Member variables
        /**
         * @var m_position
         * @brief Position in high score table for the game.
         */
        std::int32_t m_position;
        /**
         * @var m_score
         * @brief Score.
         */
        std::int32_t m_score;
        /**
         * @var m_user
         * @brief User.
         */
        User::ptr m_user;

    public:
        //Constructors
        GameHighScore();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit GameHighScore(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_position
         */
        std::int32_t get_position() const;

        /**
         * @brief Getter.
         * @return m_score
         */
        std::int32_t get_score() const;

        /**
         * @brief Getter.
         * @return m_user
         */
        User::ptr get_user() const;

        //Setter
        /**
         * @brief Setter for m_position.
         * @param[in] position See the member variable.
         */
        void set_position(const std::int32_t &position);

        /**
         * @brief Setter for m_score.
         * @param[in] score See the member variable.
         */
        void set_score(const std::int32_t &score);

        /**
         * @brief Setter for m_user.
         * @param[in] user See the member variable.
         */
        void set_user(const User::ptr &user);
    };
} //namespace tgbot

#endif
