#ifndef TGBOT_TYPES_INLINEKEYBOARDMARKUP_H
#define TGBOT_TYPES_INLINEKEYBOARDMARKUP_H

#include <memory>
#include <string>
#include <vector>

#include "tgbot/types/InlineKeyboardButton.h"
#include "tgbot/types/Reply.h"

namespace tgbot
{
    /**
     * @class InlineKeyboardMarkup
     * @brief This object represents one button of an inline keyboard.
     * @details You must use exactly one of the optional fields.
     * @note This will only work in Telegram versions released after 9 April, 2016. Older clients will display unsupported message.
     */
    class InlineKeyboardMarkup : public Reply
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<InlineKeyboardMarkup>;

    private:
        //Member variables
        /**
         * @var m_inline_keyboard
         * @brief Array of button rows, each represented by an Array of InlineKeyboardButton objects.
         */
        std::vector<std::vector<InlineKeyboardButton::ptr>> m_inline_keyboard;

    public:
        //Constructors
        InlineKeyboardMarkup();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit InlineKeyboardMarkup(const std::string &json);

        /**
         * @param[in] inline_keyboard Inline keyboard.
         */
        explicit InlineKeyboardMarkup(std::vector<std::vector<InlineKeyboardButton::ptr>> inline_keyboard);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_inline_keyboard
         */
        std::vector<std::vector<InlineKeyboardButton::ptr>> get_inline_keyboard() const;

        //Setter
        /**
         * @brief Setter for m_inline_keyboard.
         * @param[in] inline_keyboard See the member variable.
         */
        void set_inline_keyboard(const std::vector<std::vector<InlineKeyboardButton::ptr>> &inline_keyboard);
    };
} //namespace tgbot

#endif
