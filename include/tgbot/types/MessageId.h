#ifndef TGBOT_TYPES_MESSAGEID_H
#define TGBOT_TYPES_MESSAGEID_H

#include <cstdint>
#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class MessageId
     * @brief This object represents a unique message identifier.
     */
    class MessageId : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<MessageId>;

    private:
        //Member variables
        /**
         * @var m_message_id
         * @brief Unique message identifier.
         */
        std::int32_t m_message_id;

    public:
        //Constructors
        MessageId();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit MessageId(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_message_id
         */
        std::int32_t get_message_id() const;

        //Setter
        /**
         * @brief Setter for m_message_id.
         * @param[in] message_id See the member variable.
         */
        void set_message_id(const std::int32_t &message_id);
    };
} //namespace tgbot

#endif
