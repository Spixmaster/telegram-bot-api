#ifndef TGBOT_TYPES_MASKPOSITION_H
#define TGBOT_TYPES_MASKPOSITION_H

#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class MaskPosition
     * @brief This object describes the position on faces where a mask should be placed by default.
     */
    class MaskPosition : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<MaskPosition>;

    private:
        //Member variables
        /**
         * @var m_point
         * @brief The part of the face relative to which the mask should be placed.
         * @details One of “forehead”, “eyes”, “mouth”, or “chin”.
         */
        std::string m_point;
        /**
         * @var m_scale
         * @brief Mask scaling coefficient.
         * @details For example, 2.0 means double size.
         */
        float m_scale;
        /**
         * @var m_x_shift
         * @brief Shift by X-axis measured in widths of the mask scaled to the face size, from left to right.
         * @details For example, choosing -1.0 will place mask just to the left of the default mask position.
         */
        float m_x_shift;
        /**
         * @var m_y_shift
         * @brief Shift by Y-axis measured in heights of the mask scaled to the face size, from top to bottom.
         * @details For example, 1.0 will place the mask just below the default mask position.
         */
        float m_y_shift;

    public:
        //Constructors
        MaskPosition();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit MaskPosition(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_point
         */
        std::string get_point() const;

        /**
         * @brief Getter.
         * @return m_scale
         */
        float get_scale() const;

        /**
         * @brief Getter.
         * @return m_x_shift
         */
        float get_x_shift() const;

        /**
         * @brief Getter.
         * @return m_y_shift
         */
        float get_y_shift() const;

        //Setter
        /**
         * @brief Setter for m_point.
         * @param[in] point See the member variable.
         */
        void set_point(const std::string &point);

        /**
         * @brief Setter for m_scale.
         * @param[in] scale See the member variable.
         */
        void set_scale(const float &scale);

        /**
         * @brief Setter for m_x_shift.
         * @param[in] x_shift See the member variable.
         */
        void set_x_shift(const float &x_shift);

        /**
         * @brief Setter for m_y_shift.
         * @param[in] y_shift See the member variable.
         */
        void set_y_shift(const float &y_shift);
    };
} //namespace tgbot

#endif
