#ifndef TGBOT_TYPES_MENUBUTTON_H
#define TGBOT_TYPES_MENUBUTTON_H

#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class MenuButton
     * @brief This object describes the bot's menu button in a private chat.
     * @details It should be one of
     * MenuButtonCommands
     * MenuButtonWebApp
     * MenuButtonDefault
     * @details If a menu button other than MenuButtonDefault is set for a private chat, then it is applied in the chat. Otherwise the default menu button is applied.
     * @details By default, the menu button opens the list of bot commands.
     */
    class MenuButton : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<MenuButton>;

        //Constructors
        MenuButton();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit MenuButton(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;
    };
} //namespace tgbot

#endif
