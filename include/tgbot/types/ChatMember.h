#ifndef TGBOT_TYPES_CHATMEMBER_H
#define TGBOT_TYPES_CHATMEMBER_H

#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class ChatMember
     * @brief This object contains information about one member of a chat.
     * @details Currently, the following 6 types of chat members are supported:
     * ChatMemberOwner
     * ChatMemberAdministrator
     * ChatMemberMember
     * ChatMemberRestricted
     * ChatMemberLeft
     * ChatMemberBanned
     */
    class ChatMember : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<ChatMember>;

        //Constructors
        ChatMember();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit ChatMember(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;
    };
} //namespace tgbot

#endif
