#ifndef TGBOT_TYPES_LABELEDPRICE_H
#define TGBOT_TYPES_LABELEDPRICE_H

#include <cstdint>
#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class LabeledPrice
     * @brief This object represents a portion of the price for goods or services.
     */
    class LabeledPrice : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<LabeledPrice>;

    private:
        //Member variables
        /**
         * @var m_amount
         * @brief Price of the product in the smallest units of the currency (integer, not float/double).
         * @details For example, for a price of US$ 1.45 pass amount = 145.
         * @details See the exp parameter in currencies.json, it shows the number of digits past the decimal point for each currency (2 for the majority of currencies).
         */
        std::int32_t m_amount;

        /**
         * @var m_label
         * @brief Portion label.
         */
        std::string m_label;

    public:
        //Constructors
        LabeledPrice();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit LabeledPrice(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_amount
         */
        std::int32_t get_amount() const;

        /**
         * @brief Getter.
         * @return m_label
         */
        std::string get_label() const;

        //Setter
        /**
         * @brief Setter for m_amount.
         * @param[in] amount See the member variable.
         */
        void set_amount(const std::int32_t &amount);

        /**
         * @brief Setter for m_label.
         * @param[in] label See the member variable.
         */
        void set_label(const std::string &label);
    };
} //namespace tgbot

#endif
