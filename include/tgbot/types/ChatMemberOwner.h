#ifndef TGBOT_TYPES_CHATMEMBEROWNER_H
#define TGBOT_TYPES_CHATMEMBEROWNER_H

#include <memory>
#include <optional>
#include <string>

#include "tgbot/types/ChatMember.h"
#include "tgbot/types/User.h"

namespace tgbot
{
    /**
     * @class ChatMemberOwner
     * @brief Represents a chat member that owns the chat and has all administrator privileges.
     */
    class ChatMemberOwner : public ChatMember
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<ChatMemberOwner>;

    private:
        //Member variables
        /**
         * @var m_custom_title
         * @brief Optional. Custom title for this user.
         */
        std::optional<std::string> m_custom_title;
        /**
         * @var m_is_anonymous
         * @brief True, if the user's presence in the chat is hidden.
         */
        bool m_is_anonymous;
        /**
         * @var m_status
         * @brief The member's status in the chat, always “creator”.
         */
        std::string m_status;
        /**
         * @var m_user
         * @brief Information about the user.
         */
        User::ptr m_user;

    public:
        //Constructors
        ChatMemberOwner();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit ChatMemberOwner(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_custom_title
         */
        std::optional<std::string> get_custom_title() const;

        /**
         * @brief Getter.
         * @return m_is_anonymous
         */
        bool get_is_anonymous() const;

        /**
         * @brief Getter.
         * @return m_status
         */
        std::string get_status() const;

        /**
         * @brief Getter.
         * @return m_user
         */
        User::ptr get_user() const;

        //Setter
        /**
         * @brief Setter for m_custom_title.
         * @param[in] custom_title See the member variable.
         */
        void set_custom_title(const std::optional<std::string> &custom_title);

        /**
         * @brief Setter for m_is_anonymous.
         * @param[in] is_anonymous See the member variable.
         */
        void set_is_anonymous(const bool &is_anonymous);

        /**
         * @brief Setter for m_status.
         * @param[in] status See the member variable.
         */
        void set_status(const std::string &status);

        /**
         * @brief Setter for m_user.
         * @param[in] user See the member variable.
         */
        void set_user(const User::ptr &user);
    };
} //namespace tgbot

#endif
