#ifndef TGBOT_TYPES_INPUTMEDIA_H
#define TGBOT_TYPES_INPUTMEDIA_H

#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class InputMedia
     * @brief This object represents the content of a media message to be sent.
     * @details It should be one of
     * InputMediaAnimation
     * InputMediaDocument
     * InputMediaAudio
     * InputMediaPhoto
     * InputMediaVideo
     */
    class InputMedia : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<InputMedia>;

        //Constructors
        InputMedia();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit InputMedia(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;
    };
} //namespace tgbot

#endif
