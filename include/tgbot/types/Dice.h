#ifndef TGBOT_TYPES_DICE_H
#define TGBOT_TYPES_DICE_H

#include <cstdint>
#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class Dice
     * @brief This object represents an animated emoji that displays a random value.
     */
    class Dice : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<Dice>;

    private:
        //Member variables
        /**
         * @var m_emoji
         * @brief Emoji on which the dice throw animation is based.
         */
        std::string m_emoji;
        /**
         * @var m_value
         * @brief Value of the dice, 1-6 for “🎲”, “🎯” and “🎳” base emoji, 1-5 for “🏀” and “⚽” base emoji, 1-64 for “🎰” base emoji
         */
        std::int32_t m_value;

    public:
        //Constructors
        Dice();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit Dice(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_emoji
         */
        std::string get_emoji() const;

        /**
         * @brief Getter.
         * @return m_value
         */
        std::int32_t get_value() const;

        //Setter
        /**
         * @brief Setter for m_emoji.
         * @param[in] emoji See the member variable.
         */
        void set_emoji(const std::string &emoji);

        /**
         * @brief Setter for m_value.
         * @param[in] value See the member variable.
         */
        void set_value(const std::int32_t &value);
    };
} //namespace tgbot

#endif
