#ifndef TGBOT_TYPES_PASSPORTDATA_H
#define TGBOT_TYPES_PASSPORTDATA_H

#include <memory>
#include <string>
#include <vector>

#include "tgbot/Type.h"
#include "tgbot/types/EncryptedCredentials.h"
#include "tgbot/types/EncryptedPassportElement.h"

namespace tgbot
{
    /**
     * @class PassportData
     * @brief Describes Telegram Passport data shared with the bot by the user.
     */
    class PassportData : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<PassportData>;

    private:
        //Member variables
        /**
         * @var m_credentials
         * @brief Encrypted credentials required to decrypt the data.
         */
        EncryptedCredentials::ptr m_credentials;
        /**
         * @var m_data
         * @brief Array with information about documents and other Telegram Passport elements that was shared with the bot.
         */
        std::vector<EncryptedPassportElement::ptr> m_data;

    public:
        //Constructors
        PassportData();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit PassportData(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_credentials
         */
        EncryptedCredentials::ptr get_credentials() const;

        /**
         * @brief Getter.
         * @return m_data
         */
        std::vector<EncryptedPassportElement::ptr> get_data() const;

        //Setter
        /**
         * @brief Setter for m_credentials.
         * @param[in] credentials See the member variable.
         */
        void set_credentials(const EncryptedCredentials::ptr &credentials);

        /**
         * @brief Setter for m_data.
         * @param[in] data See the member variable.
         */
        void set_data(const std::vector<EncryptedPassportElement::ptr> &data);
    };
} //namespace tgbot

#endif
