#ifndef TGBOT_TYPES_CHATPHOTO_H
#define TGBOT_TYPES_CHATPHOTO_H

#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class ChatPhoto
     * @brief This object represents a chat photo.
     */
    class ChatPhoto : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<ChatPhoto>;

    private:
        //Member variables
        /**
         * @var m_big_file_id
         * @brief File identifier of big (640x640) chat photo.
         * @details This file_id can be used only for photo download and only for as long as the photo is not changed.
         */
        std::string m_big_file_id;
        /**
         * @var m_big_file_unique_id
         * @brief Unique file identifier of big (640x640) chat photo, which is supposed to be the same over time and for different bots.
         * @details Can't be used to download or reuse the file.
         */
        std::string m_big_file_unique_id;
        /**
         * @var m_small_file_id
         * @brief File identifier of small (160x160) chat photo.
         * @details This file_id can be used only for photo download and only for as long as the photo is not changed.
         */
        std::string m_small_file_id;
        /**
         * @var m_small_file_unique_id
         * @brief Unique file identifier of small (160x160) chat photo, which is supposed to be the same over time and for different bots.
         * @details Can't be used to download or reuse the file.
         */
        std::string m_small_file_unique_id;

    public:
        //Constructors
        ChatPhoto();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit ChatPhoto(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_big_file_id
         */
        std::string get_big_file_id() const;

        /**
         * @brief Getter.
         * @return m_big_file_unique_id
         */
        std::string get_big_file_unique_id() const;

        /**
         * @brief Getter.
         * @return m_small_file_id
         */
        std::string get_small_file_id() const;

        /**
         * @brief Getter.
         * @return m_small_file_unique_id
         */
        std::string get_small_file_unique_id() const;

        //Setter
        /**
         * @brief Setter for m_big_file_id.
         * @param[in] big_file_id See the member variable.
         */
        void set_big_file_id(const std::string &big_file_id);

        /**
         * @brief Setter for m_big_file_unique_id.
         * @param[in] big_file_unique_id See the member variable.
         */
        void set_big_file_unique_id(const std::string &big_file_unique_id);

        /**
         * @brief Setter for m_small_file_id.
         * @param[in] small_file_id See the member variable.
         */
        void set_small_file_id(const std::string &small_file_id);

        /**
         * @brief Setter for m_small_file_unique_id.
         * @param[in] small_file_unique_id See the member variable.
         */
        void set_small_file_unique_id(const std::string &small_file_unique_id);
    };
} //namespace tgbot

#endif
