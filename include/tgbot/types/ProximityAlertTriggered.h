#ifndef TGBOT_TYPES_PROXIMITYALERTTRIGGERED_H
#define TGBOT_TYPES_PROXIMITYALERTTRIGGERED_H

#include <cstdint>
#include <memory>
#include <string>

#include "tgbot/Type.h"
#include "tgbot/types/User.h"

namespace tgbot
{
    /**
     * @class ProximityAlertTriggered
     * @brief This object represents the content of a service message, sent whenever a user in the chat triggers a proximity alert set by another user.
     */
    class ProximityAlertTriggered : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<ProximityAlertTriggered>;

    private:
        //Member variables
        /**
         * @var m_distance
         * @brief The distance between the users.
         */
        std::int32_t m_distance;
        /**
         * @var m_traveler
         * @brief User that triggered the alert.
         */
        User::ptr m_traveler;
        /**
         * @var m_watcher
         * @brief User that set the alert.
         */
        User::ptr m_watcher;

    public:
        //Constructors
        ProximityAlertTriggered();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit ProximityAlertTriggered(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_distance
         */
        std::int32_t get_distance() const;

        /**
         * @brief Getter.
         * @return m_traveler
         */
        User::ptr get_traveler() const;

        /**
         * @brief Getter.
         * @return m_watcher
         */
        User::ptr get_watcher() const;

        //Setter
        /**
         * @brief Setter for m_distance.
         * @param[in] distance See the member variable.
         */
        void set_distance(const std::int32_t &distance);

        /**
         * @brief Setter for m_traveler.
         * @param[in] traveler See the member variable.
         */
        void set_traveler(const User::ptr &traveler);

        /**
         * @brief Setter for m_watcher.
         * @param[in] watcher See the member variable.
         */
        void set_watcher(const User::ptr &watcher);
    };
} //namespace tgbot

#endif
