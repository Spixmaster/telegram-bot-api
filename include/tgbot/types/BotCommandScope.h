#ifndef TGBOT_TYPES_BOTCOMMANDSCOPE_H
#define TGBOT_TYPES_BOTCOMMANDSCOPE_H

#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class BotCommandScope
     * @brief This object represents the scope to which bot commands are applied.
     * @details Currently, the following 7 scopes are supported:
     * BotCommandScopeDefault
     * BotCommandScopeAllPrivateChats
     * BotCommandScopeAllGroupChats
     * BotCommandScopeAllChatAdministrators
     * BotCommandScopeChat
     * BotCommandScopeChatAdministrators
     * BotCommandScopeChatMembe
     *
     * @section determining-list-of-commands Determining list of commands
     * The following algorithm is used to determine the list of commands for a particular user viewing the bot menu. The
     * first list of commands which is set is returned:
     *
     * @subsection determining-list-of-commands-commands-in-the-chat-with-the-bot Commands in the chat with the bot
     * botCommandScopeChat + language_code
     * botCommandScopeChat
     * botCommandScopeAllPrivateChats + language_code
     * botCommandScopeAllPrivateChats
     * botCommandScopeDefault + language_code
     * botCommandScopeDefault
     *
     * @subsection determining-list-of-commands-commands-in-group-and-supergroup-chats Commands in group and supergroup chats
     * botCommandScopeChatMember + language_code
     * botCommandScopeChatMember
     * botCommandScopeChatAdministrators + language_code (administrators only)
     * botCommandScopeChatAdministrators (administrators only)
     * botCommandScopeChat + language_code
     * botCommandScopeChat
     * botCommandScopeAllChatAdministrators + language_code (administrators only)
     * botCommandScopeAllChatAdministrators (administrators only)
     * botCommandScopeAllGroupChats + language_code
     * botCommandScopeAllGroupChats
     * botCommandScopeDefault + language_code
     * botCommandScopeDefault
     */
    class BotCommandScope : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<BotCommandScope>;

        //Constructors
        BotCommandScope();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit BotCommandScope(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;
    };
} //namespace tgbot

#endif
