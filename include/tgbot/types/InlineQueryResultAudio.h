#ifndef TGBOT_TYPES_INLINEQUERYRESULTAUDIO_H
#define TGBOT_TYPES_INLINEQUERYRESULTAUDIO_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>
#include <vector>

#include "tgbot/types/InlineKeyboardMarkup.h"
#include "tgbot/types/InlineQueryResult.h"
#include "tgbot/types/InputMessageContent.h"
#include "tgbot/types/MessageEntity.h"

namespace tgbot
{
    /**
     * @class InlineQueryResultAudio
     * @brief Represents a link to an MP3 audio file.
     * @details By default, this audio file will be sent by the user.
     * @details Alternatively, you can use input_message_content to send a message with the specified content instead of the audio.
     */
    class InlineQueryResultAudio : public InlineQueryResult
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<InlineQueryResultAudio>;

    private:
        //Member variables
        /**
         * @var m_audio_duration
         * @brief Optional. Audio duration in seconds.
         */
        std::optional<std::int32_t> m_audio_duration;
        /**
         * @var m_audio_url
         * @brief A valid URL for the audio file.
         */
        std::string m_audio_url;
        /**
         * @var m_caption
         * @brief Optional. Caption, 0-1024 characters after entities parsing.
         */
        std::optional<std::string> m_caption;
        /**
         * @var m_caption_entities
         * @brief Optional. List of special entities that appear in the caption, which can be specified instead of parse_mode.
         */
        std::optional<std::vector<MessageEntity::ptr>> m_caption_entities;
        /**
         * @var m_id
         * @brief Unique identifier for this result, 1-64 bytes.
         */
        std::string m_id;
        /**
         * @var m_input_message_content
         * @brief Optional. Content of the message to be sent instead of the audio.
         */
        std::optional<InputMessageContent::ptr> m_input_message_content;
        /**
         * @var m_parse_mode
         * @brief Optional. Mode for parsing entities in the audio caption.
         * @details See formatting options for more details.
         */
        std::optional<std::string> m_parse_mode;
        /**
         * @var m_performer
         * @brief Optional. Performer.
         */
        std::optional<std::string> m_performer;
        /**
         * @var m_reply_markup
         * @brief Optional. Inline keyboard attached to the message.
         */
        std::optional<InlineKeyboardMarkup::ptr> m_reply_markup;
        /**
         * @var m_title
         * @brief Title.
         */
        std::string m_title;
        /**
         * @var m_type
         * @brief Type of the result, must be audio.
         */
        std::string m_type;

    public:
        //Constructors
        InlineQueryResultAudio();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit InlineQueryResultAudio(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_audio_duration
         */
        std::optional<std::int32_t> get_audio_duration() const;

        /**
         * @brief Getter.
         * @return m_audio_url
         */
        std::string get_audio_url() const;

        /**
         * @brief Getter.
         * @return m_caption
         */
        std::optional<std::string> get_caption() const;

        /**
         * @brief Getter.
         * @return m_caption_entities
         */
        std::optional<std::vector<MessageEntity::ptr>> get_caption_entities() const;

        /**
         * @brief Getter.
         * @return m_id
         */
        std::string get_id() const;

        /**
         * @brief Getter.
         * @return m_input_message_content
         */
        std::optional<InputMessageContent::ptr> get_input_message_content() const;

        /**
         * @brief Getter.
         * @return m_parse_mode
         */
        std::optional<std::string> get_parse_mode() const;

        /**
         * @brief Getter.
         * @return m_performer
         */
        std::optional<std::string> get_performer() const;

        /**
         * @brief Getter.
         * @return m_reply_markup
         */
        std::optional<InlineKeyboardMarkup::ptr> get_reply_markup() const;

        /**
         * @brief Getter.
         * @return m_title
         */
        std::string get_title() const;

        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        //Setter
        /**
         * @brief Setter for m_audio_duration.
         * @param[in] audio_duration See the member variable.
         */
        void set_audio_duration(const std::optional<std::int32_t> &audio_duration);

        /**
         * @brief Setter for m_audio_url.
         * @param[in] audio_url See the member variable.
         */
        void set_audio_url(const std::string &audio_url);

        /**
         * @brief Setter for m_caption.
         * @param[in] caption See the member variable.
         */
        void set_caption(const std::optional<std::string> &caption);

        /**
         * @brief Setter for m_caption_entities.
         * @param[in] caption_entities See the member variable.
         */
        void set_caption_entities(const std::optional<std::vector<MessageEntity::ptr>> &caption_entities);

        /**
         * @brief Setter for m_id.
         * @param[in] id See the member variable.
         */
        void set_id(const std::string &id);

        /**
         * @brief Setter for m_input_message_content.
         * @param[in] input_message_content See the member variable.
         */
        void set_input_message_content(const std::optional<InputMessageContent::ptr> &input_message_content);
        /**
         * @brief Setter for m_parse_mode.
         * @param[in] parse_mode See the member variable.
         */
        void set_parse_mode(const std::optional<std::string> &parse_mode);

        /**
         * @brief Setter for m_performer.
         * @param[in] performer See the member variable.
         */
        void set_performer(const std::optional<std::string> &performer);

        /**
         * @brief Setter for m_reply_markup.
         * @param[in] reply_markup See the member variable.
         */
        void set_reply_markup(const std::optional<InlineKeyboardMarkup::ptr> &reply_markup);

        /**
         * @brief Setter for m_title.
         * @param[in] title See the member variable.
         */
        void set_title(const std::string &title);

        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);
    };
} //namespace tgbot

#endif
