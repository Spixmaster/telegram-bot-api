#ifndef TGBOT_TYPES_INLINEQUERYRESULTVIDEO_H
#define TGBOT_TYPES_INLINEQUERYRESULTVIDEO_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>
#include <vector>

#include "tgbot/types/InlineKeyboardMarkup.h"
#include "tgbot/types/InlineQueryResult.h"
#include "tgbot/types/InputMessageContent.h"
#include "tgbot/types/MessageEntity.h"

namespace tgbot
{
    /**
     * @class InlineQueryResultVideo
     * @brief Represents a link to a page containing an embedded video player or a video file.
     * @details By default, this video file will be sent by the user with an optional caption.
     * @details Alternatively, you can use input_message_content to send a message with the specified content instead of the video.
     * @details If an InlineQueryResultVideo message contains an embedded video (e.g., YouTube), you must replace its content using input_message_content.
     */
    class InlineQueryResultVideo : public InlineQueryResult
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<InlineQueryResultVideo>;

    private:
        //Member variables
        /**
         * @var m_caption
         * @brief Optional. Caption of the video to be sent, 0-1024 characters after entities parsing.
         */
        std::optional<std::string> m_caption;
        /**
         * @var m_caption_entities
         * @brief Optional. List of special entities that appear in the caption, which can be specified instead of parse_mode.
         */
        std::optional<std::vector<MessageEntity::ptr>> m_caption_entities;
        /**
         * @var m_description
         * @brief Optional. Short description of the result.
         */
        std::optional<std::string> m_description;
        /**
         * @var m_id
         * @brief Type of the result, must be video.
         */
        std::string m_id;
        /**
         * @var m_input_message_content
         * @brief Optional. Content of the message to be sent instead of the video.
         * @details This field is required if InlineQueryResultVideo is used to send an HTML-page as a result (e.g., a YouTube video).
         */
        std::optional<InputMessageContent::ptr> m_input_message_content;
        /**
         * @var m_mime_type
         * @brief MIME type of the content of the video URL, “text/html” or “video/mp4”.
         */
        std::string m_mime_type;
        /**
         * @var m_parse_mode
         * @brief Optional. Mode for parsing entities in the video caption.
         * @details See formatting options for more details.
         */
        std::optional<std::string> m_parse_mode;
        /**
         * @var m_reply_markup
         * @brief Optional. Inline keyboard attached to the message.
         */
        std::optional<InlineKeyboardMarkup::ptr> m_reply_markup;
        /**
         * @var m_thumbnail_url
         * @brief URL of the thumbnail (JPEG only) for the video.
         */
        std::string m_thumbnail_url;
        /**
         * @var m_title
         * @brief Title for the result.
         */
        std::string m_title;
        /**
         * @var m_type
         * @brief Type of the result, must be video.
         */
        std::string m_type;
        /**
         * @var m_video_duration
         * @brief Optional. Video duration in seconds.
         */
        std::optional<std::int32_t> m_video_duration;
        /**
         * @var m_video_height
         * @brief Optional. Video height.
         */
        std::optional<std::int32_t> m_video_height;
        /**
         * @var m_video_url
         * @brief A valid URL for the embedded video player or video file.
         */
        std::string m_video_url;
        /**
         * @var m_video_width
         * @brief Optional. Video width.
         */
        std::optional<std::int32_t> m_video_width;

    public:
        //Constructors
        InlineQueryResultVideo();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit InlineQueryResultVideo(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_caption
         */
        std::optional<std::string> get_caption() const;

        /**
         * @brief Getter.
         * @return m_caption_entities
         */
        std::optional<std::vector<MessageEntity::ptr>> get_caption_entities() const;

        /**
         * @brief Getter.
         * @return m_description
         */
        std::optional<std::string> get_description() const;

        /**
         * @brief Getter.
         * @return m_id
         */
        std::string get_id() const;

        /**
         * @brief Getter.
         * @return m_input_message_content
         */
        std::optional<InputMessageContent::ptr> get_input_message_content() const;

        /**
         * @brief Getter.
         * @return m_mime_type
         */
        std::string get_mime_type() const;

        /**
         * @brief Getter.
         * @return m_parse_mode
         */
        std::optional<std::string> get_parse_mode() const;

        /**
         * @brief Getter.
         * @return m_reply_markup
         */
        std::optional<InlineKeyboardMarkup::ptr> get_reply_markup() const;

        /**
         * @brief Getter.
         * @return m_thumbnail_url
         */
        std::string get_thumbnail_url() const;

        /**
         * @brief Getter.
         * @return m_title
         */
        std::string get_title() const;

        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        /**
         * @brief Getter.
         * @return m_video_duration
         */
        std::optional<std::int32_t> get_video_duration() const;

        /**
         * @brief Getter.
         * @return m_video_height
         */
        std::optional<std::int32_t> get_video_height() const;

        /**
         * @brief Getter.
         * @return m_video_url
         */
        std::string get_video_url() const;

        /**
         * @brief Getter.
         * @return m_video_width
         */
        std::optional<std::int32_t> get_video_width() const;

        //Setter
        /**
         * @brief Setter for m_caption.
         * @param[in] caption See the member variable.
         */
        void set_caption(const std::optional<std::string> &caption);

        /**
         * @brief Setter for m_caption_entities.
         * @param[in] caption_entities See the member variable.
         */
        void set_caption_entities(const std::optional<std::vector<MessageEntity::ptr>> &caption_entities);

        /**
         * @brief Setter for m_description.
         * @param[in] description See the member variable.
         */
        void set_description(const std::optional<std::string> &description);

        /**
         * @brief Setter for m_id.
         * @param[in] id See the member variable.
         */
        void set_id(const std::string &id);

        /**
         * @brief Setter for m_input_message_content.
         * @param[in] input_message_content See the member variable.
         */
        void set_input_message_content(const std::optional<InputMessageContent::ptr> &input_message_content);

        /**
         * @brief Setter for m_mime_type.
         * @param[in] mime_type See the member variable.
         */
        void set_mime_type(const std::string &mime_type);

        /**
         * @brief Setter for m_parse_mode.
         * @param[in] parse_mode See the member variable.
         */
        void set_parse_mode(const std::optional<std::string> &parse_mode);

        /**
         * @brief Setter for m_reply_markup.
         * @param[in] reply_markup See the member variable.
         */
        void set_reply_markup(const std::optional<InlineKeyboardMarkup::ptr> &reply_markup);

        /**
         * @brief Setter for m_thumbnail_url.
         * @param[in] thumbnail_url See the member variable.
         */
        void set_thumbnail_url(const std::string &thumbnail_url);

        /**
         * @brief Setter for m_title.
         * @param[in] title See the member variable.
         */
        void set_title(const std::string &title);

        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);

        /**
         * @brief Setter for m_video_duration.
         * @param[in] video_duration See the member variable.
         */
        void set_video_duration(const std::optional<std::int32_t> &video_duration);

        /**
         * @brief Setter for video_height.
         * @param[in] video_height See the member variable.
         */
        void set_video_height(const std::optional<std::int32_t> &video_height);

        /**
         * @brief Setter for m_video_url.
         * @param[in] video_url See the member variable.
         */
        void set_video_url(const std::string &video_url);

        /**
         * @brief Setter for m_video_width.
         * @param[in] video_width See the member variable.
         */
        void set_video_width(const std::optional<std::int32_t> &video_width);
    };
} //namespace tgbot

#endif
