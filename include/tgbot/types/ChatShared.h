#ifndef TGBOT_TYPES_CHATSHARED_H
#define TGBOT_TYPES_CHATSHARED_H

#include <cstdint>
#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class ChatShared
     * @brief This object contains information about the chat whose identifier was shared with the bot using a KeyboardButtonRequestChat button.
     */
    class ChatShared : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<ChatShared>;

    private:
        //Member variables
        /**
         * @var m_chat_id
         * @brief Identifier of the shared chat.
         * @details This number may have more than 32 significant bits and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a 64-bit integer or double-precision float type are safe for storing this identifier.
         * @details The bot may not have access to the chat and could be unable to use this identifier, unless the chat is already known to the bot by some other means.
         */
        std::int64_t m_chat_id;
        /**
         * @var m_request_id
         * @brief Identifier of the request.
         */
        std::int32_t m_request_id;

    public:
        //Constructors
        ChatShared();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit ChatShared(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_chat_id
         */
        std::int64_t get_chat_id() const;

        /**
         * @brief Getter.
         * @return m_request_id
         */
        std::int32_t get_request_id() const;

        //Setter
        /**
         * @brief Setter for m_chat_id.
         * @param[in] chat_id See the member variable.
         */
        void set_chat_id(const std::int64_t &chat_id);

        /**
         * @brief Setter for m_request_id.
         * @param[in] request_id See the member variable.
         */
        void set_request_id(const std::int32_t &request_id);
    };
} //namespace tgbot

#endif
