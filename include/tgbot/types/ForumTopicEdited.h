#ifndef TGBOT_TYPES_FORUMTOPICEDITED_H
#define TGBOT_TYPES_FORUMTOPICEDITED_H

#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class ForumTopicEdited
     * @brief This object represents a service message about an edited forum topic.
     */
    class ForumTopicEdited : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<ForumTopicEdited>;

    private:
        //Member variables
        /**
         * @var m_icon_custom_emoji_id
         * @brief Optional. New identifier of the custom emoji shown as the topic icon, if it was edited; an empty string if the icon was removed.
         */
        std::optional<std::string> m_icon_custom_emoji_id;
        /**
         * @var m_name
         * @brief Optional. New name of the topic, if it was edited.
         */
        std::optional<std::string> m_name;

    public:
        //Constructors
        ForumTopicEdited();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit ForumTopicEdited(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_icon_custom_emoji_id
         */
        std::optional<std::string> get_icon_custom_emoji_id() const;

        /**
         * @brief Getter.
         * @return m_name
         */
        std::optional<std::string> get_name() const;

        //Setter
        /**
         * @brief Setter for m_icon_custom_emoji_id.
         * @param[in] icon_custom_emoji_id See the member variable.
         */
        void set_icon_custom_emoji_id(const std::optional<std::string> &icon_custom_emoji_id);

        /**
         * @brief Setter for m_name.
         * @param[in] name See the member variable.
         */
        void set_name(const std::optional<std::string> &name);
    };
} //namespace tgbot

#endif
