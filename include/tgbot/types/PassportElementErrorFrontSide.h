#ifndef TGBOT_TYPES_PASSPORTELEMENTERRORFRONTSIDE_H
#define TGBOT_TYPES_PASSPORTELEMENTERRORFRONTSIDE_H

#include <memory>
#include <string>

#include "tgbot/types/PassportElementError.h"

namespace tgbot
{
    /**
     * @class PassportElementErrorFrontSide
     * @brief Represents an issue with the front side of a document.
     * @details The error is considered resolved when the file with the front side of the document changes.
     */
    class PassportElementErrorFrontSide : public PassportElementError
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<PassportElementErrorFrontSide>;

    private:
        //Member variables
        /**
         * @var m_file_hash
         * @brief Base64-encoded hash of the file with the front side of the document.
         */
        std::string m_file_hash;
        /**
         * @var m_message
         * @brief Error message.
         */
        std::string m_message;
        /**
         * @var m_source
         * @brief Error source, must be front_side.
         */
        std::string m_source;
        /**
         * @var m_type
         * @brief The section of the user's Telegram Passport which has the issue, one of “passport”, “driver_license”, “identity_card”, “internal_passport”.
         */
        std::string m_type;

    public:
        //Constructors
        PassportElementErrorFrontSide();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit PassportElementErrorFrontSide(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_file_hash
         */
        std::string get_file_hash() const;

        /**
         * @brief Getter.
         * @return m_message
         */
        std::string get_message() const;

        /**
         * @brief Getter.
         * @return m_source
         */
        std::string get_source() const;

        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        //Setter
        /**
         * @brief Setter for m_file_hash.
         * @param[in] file_hash See the member variable.
         */
        void set_file_hash(const std::string &file_hash);

        /**
         * @brief Setter for m_message.
         * @param[in] message See the member variable.
         */
        void set_message(const std::string &message);

        /**
         * @brief Setter for m_source.
         * @param[in] source See the member variable.
         */
        void set_source(const std::string &source);

        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);
    };
} //namespace tgbot

#endif
