#ifndef TGBOT_TYPES_STICKER_H
#define TGBOT_TYPES_STICKER_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"
#include "tgbot/types/File.h"
#include "tgbot/types/MaskPosition.h"
#include "tgbot/types/PhotoSize.h"

namespace tgbot
{
    /**
     * @class Sticker
     * @brief This object represents a sticker.
     */
    class Sticker : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<Sticker>;

    private:
        //Member variables
        /**
         * @var m_custom_emoji_id
         * @brief Optional. For custom emoji stickers, unique identifier of the custom emoji.
         */
        std::optional<std::string> m_custom_emoji_id;
        /**
         * @var m_emoji
         * @brief Optional. Emoji associated with the sticker.
         */
        std::optional<std::string> m_emoji;
        /**
         * @var m_file_id
         * @brief Identifier for this file, which can be used to download or reuse the file.
         */
        std::string m_file_id;
        /**
         * @var m_file_size
         * @brief Optional. File size in bytes.
         */
        std::optional<std::int32_t> m_file_size;
        /**
         * @var m_file_unique_id
         * @brief Unique identifier for this file, which is supposed to be the same over time and for different bots.
         * @details Can't be used to download or reuse the file.
         */
        std::string m_file_unique_id;
        /**
         * @var m_height
         * @brief Sticker height.
         */
        std::int32_t m_height;
        /**
         * @var m_is_animated
         * @brief True, if the sticker is animated.
         */
        bool m_is_animated;
        /**
         * @var m_is_video
         * @brief True, if the sticker is a video sticker.
         */
        bool m_is_video;
        /**
         * @var m_mask_position
         * @brief Optional. For mask stickers, the position where the mask should be placed.
         */
        std::optional<MaskPosition::ptr> m_mask_position;
        /**
         * @var m_needs_repainting
         * @brief Optional. True, if the sticker must be repainted to a text color in messages, the color of the Telegram Premium badge in emoji status, white color on chat photos, or another appropriate color in other places.
         */
        std::optional<bool> m_needs_repainting;
        /**
         * @var m_premium_animation
         * @brief Optional. For premium regular stickers, premium animation for the sticker.
         */
        std::optional<File::ptr> m_premium_animation;
        /**
         * @var m_set_name
         * @brief Optional. Name of the sticker set to which the sticker belongs.
         */
        std::optional<std::string> m_set_name;
        /**
         * @var m_thumbnail
         * @brief Optional. Sticker thumbnail in the .WEBP or .JPG format.
         */
        std::optional<PhotoSize::ptr> m_thumbnail;
        /**
         * @var m_type
         * @brief Type of the sticker, currently one of “regular”, “mask”, “custom_emoji”.
         * @details The type of the sticker is independent from its format, which is determined by the fields is_animated and is_video.
         */
        std::string m_type;
        /**
         * @var m_width
         * @brief Sticker width.
         */
        std::int32_t m_width;

    public:
        //Constructors
        Sticker();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit Sticker(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_custom_emoji_id
         */
        std::optional<std::string> get_custom_emoji_id() const;

        /**
         * @brief Getter.
         * @return m_emoji
         */
        std::optional<std::string> get_emoji() const;

        /**
         * @brief Getter.
         * @return m_file_id
         */
        std::string get_file_id() const;

        /**
         * @brief Getter.
         * @return m_file_size
         */
        std::optional<std::int32_t> get_file_size() const;

        /**
         * @brief Getter.
         * @return m_file_unique_id
         */
        std::string get_file_unique_id() const;

        /**
         * @brief Getter.
         * @return m_height
         */
        std::int32_t get_height() const;

        /**
         * @brief Getter.
         * @return m_is_animated
         */
        bool get_is_animated() const;

        /**
         * @brief Getter.
         * @return m_is_video
         */
        bool get_is_video() const;

        /**
         * @brief Getter.
         * @return m_mask_position
         */
        std::optional<MaskPosition::ptr> get_mask_position() const;

        /**
         * @brief Getter.
         * @return m_needs_repainting
         */
        std::optional<bool> get_needs_repainting() const;

        /**
         * @brief Getter.
         * @return m_premium_animation
         */
        std::optional<File::ptr> get_premium_animation() const;

        /**
         * @brief Getter.
         * @return m_set_name
         */
        std::optional<std::string> get_set_name() const;

        /**
         * @brief Getter.
         * @return m_thumbnail
         */
        std::optional<PhotoSize::ptr> get_thumbnail() const;

        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        /**
         * @brief Getter.
         * @return m_width
         */
        std::int32_t get_width() const;

        //Setter
        /**
         * @brief Setter for m_custom_emoji_id.
         * @param[in] custom_emoji_id See the member variable.
         */
        void set_custom_emoji_id(const std::optional<std::string> &custom_emoji_id);

        /**
         * @brief Setter for m_emoji.
         * @param[in] emoji See the member variable.
         */
        void set_emoji(const std::optional<std::string> &emoji);

        /**
         * @brief Setter for m_file_id.
         * @param[in] file_id See the member variable.
         */
        void set_file_id(const std::string &file_id);

        /**
         * @brief Setter for m_file_size.
         * @param[in] file_size See the member variable.
         */
        void set_file_size(const std::optional<std::int32_t> &file_size);

        /**
         * @brief Setter for m_file_unique_id.
         * @param[in] file_unique_id See the member variable.
         */
        void set_file_unique_id(const std::string &file_unique_id);

        /**
         * @brief Setter for m_height.
         * @param[in] height See the member variable.
         */
        void set_height(const std::int32_t &height);

        /**
         * @brief Setter for m_is_animated.
         * @param[in] is_animated See the member variable.
         */
        void set_is_animated(const bool &is_animated);

        /**
         * @brief Setter for m_is_video.
         * @param[in] is_video See the member variable.
         */
        void set_is_video(const bool &is_video);

        /**
         * @brief Setter for m_mask_position.
         * @param[in] mask_position See the member variable.
         */
        void set_mask_position(const std::optional<MaskPosition::ptr> &mask_position);

        /**
         * @brief Setter for m_needs_repainting.
         * @param[in] needs_repainting See the member variable.
         */
        void set_needs_repainting(const std::optional<bool> &needs_repainting);

        /**
         * @brief Setter for m_premium_animation.
         * @param[in] premium_animation See the member variable.
         */
        void set_premium_animation(const std::optional<File::ptr> &premium_animation);

        /**
         * @brief Setter for m_set_name.
         * @param[in] set_name See the member variable.
         */
        void set_set_name(const std::optional<std::string> &set_name);

        /**
         * @brief Setter for m_thumbnail.
         * @param[in] thumbnail See the member variable.
         */
        void set_thumbnail(const std::optional<PhotoSize::ptr> &thumbnail);

        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);

        /**
         * @brief Setter for m_width.
         * @param[in] width See the member variable.
         */
        void set_width(const std::int32_t &width);
    };
} //namespace tgbot

#endif
