#ifndef TGBOT_TYPES_GENERALFORUMTOPICHIDDEN_H
#define TGBOT_TYPES_GENERALFORUMTOPICHIDDEN_H

#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class GeneralForumTopicHidden
     * @brief This object represents a service message about General forum topic hidden in the chat.
     * @details Currently holds no information.
     */
    class GeneralForumTopicHidden : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<GeneralForumTopicHidden>;

        //Constructors
        GeneralForumTopicHidden();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit GeneralForumTopicHidden(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;
    };
} //namespace tgbot

#endif
