#ifndef TGBOT_TYPES_INPUTSTICKER_H
#define TGBOT_TYPES_INPUTSTICKER_H

#include <memory>
#include <optional>
#include <string>
#include <vector>

#include "tgbot/Type.h"
#include "tgbot/types/MaskPosition.h"

namespace tgbot
{
    /**
     * @class InputSticker
     * @brief This object describes a sticker to be added to a sticker set.
     */
    class InputSticker : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<InputSticker>;

    private:
        //Member variables
        /**
         * @var m_emoji_list
         * @brief List of 1-20 emoji associated with the sticker.
         */
        std::vector<std::string> m_emoji_list;
        /**
         * @var m_keywords
         * @brief Optional. List of 0-20 search keywords for the sticker with total length of up to 64 characters.
         * @details For “regular” and “custom_emoji” stickers only.
         */
        std::vector<std::string> m_keywords;
        /**
         * @var m_mask_position
         * @brief Optional. Position where the mask should be placed on faces.
         * @details For “mask” stickers only.
         */
        std::optional<MaskPosition::ptr> m_mask_position;
        /**
         * @var m_sticker
         * @brief The added sticker.
         * @details Pass a file_id as a String to send a file that already exists on the Telegram servers, pass an HTTP URL as a String for Telegram to get a file from the Internet, upload a new one using multipart/form-data, or pass “attach://\<file_attach_name>” to upload a new one using multipart/form-data under \<file_attach_name> name.
         * @details Animated and video stickers can't be uploaded via HTTP URL.
         * @details More information on Sending Files ».
         */
        std::string m_sticker;

    public:
        //Constructors
        InputSticker();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit InputSticker(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_emoji_list
         */
        std::vector<std::string> get_emoji_list() const;

        /**
         * @brief Getter.
         * @return m_keywords
         */
        std::vector<std::string> get_keywords() const;

        /**
         * @brief Getter.
         * @return m_mask_position
         */
        std::optional<MaskPosition::ptr> get_mask_position() const;

        /**
         * @brief Getter.
         * @return m_sticker
         */
        std::string get_sticker() const;

        //Setter
        /**
         * @brief Setter for m_emoji_list.
         * @param[in] emoji_list See the member variable.
         */
        void set_emoji_list(const std::vector<std::string> &emoji_list);

        /**
         * @brief Setter for m_keywords.
         * @param[in] keywords See the member variable.
         */
        void set_keywords(const std::vector<std::string> &keywords);

        /**
         * @brief Setter for m_mask_position.
         * @param[in] mask_position See the member variable.
         */
        void set_mask_position(const std::optional<MaskPosition::ptr> &mask_position);

        /**
         * @brief Setter for m_sticker.
         * @param[in] sticker See the member variable.
         */
        void set_sticker(const std::string &sticker);
    };
} //namespace tgbot

#endif
