#ifndef TGBOT_TYPES_CALLBACKGAME_H
#define TGBOT_TYPES_CALLBACKGAME_H

#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class CallbackGame
     * @brief A placeholder, currently holds no information.
     * @details Use BotFather to set up your game.
     */
    class CallbackGame : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<CallbackGame>;

        //Constructors
        CallbackGame();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit CallbackGame(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;
    };
} //namespace tgbot

#endif
