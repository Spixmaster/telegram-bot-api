#ifndef TGBOT_TYPES_CHAT_H
#define TGBOT_TYPES_CHAT_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>
#include <vector>

#include "tgbot/Type.h"
#include "tgbot/types/ChatLocation.h"
#include "tgbot/types/ChatPermissions.h"
#include "tgbot/types/ChatPhoto.h"

namespace tgbot
{
    class Message;
} //namespace tgbot

namespace tgbot
{
    /**
     * @class Chat
     * @brief This object represents a chat.
     */
    class Chat : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<Chat>;

    private:
        //Member variables
        /**
         * @var m_active_usernames
         * @brief Optional. If non-empty, the list of all active chat usernames; for private chats, supergroups and channels.
         * @details Returned only in getChat.
         */
        std::optional<std::vector<std::string>> m_active_usernames;
        /**
         * @var m_bio
         * @brief Optional. Bio of the other party in a private chat.
         * @return Returned only in getChat.
         */
        std::optional<std::string> m_bio;
        /**
         * @var m_can_set_sticker_set
         * @brief Optional. True, if the bot can change the group sticker set.
         * @details Returned only in getChat.
         */
        std::optional<bool> m_can_set_sticker_set;
        /**
         * @var m_description
         * @brief Optional. Description, for groups, supergroups and channel chats.
         * @details Returned only in getChat.
         */
        std::optional<std::string> m_description;
        /**
         * @var m_emoji_status_custom_emoji_id
         * @brief Optional. Custom emoji identifier of emoji status of the other party in a private chat.
         * @details Returned only in getChat.
         */
        std::optional<std::string> m_emoji_status_custom_emoji_id;
        /**
         * @var m_first_name
         * @brief Optional. First name of the other party in a private chat.
         */
        std::optional<std::string> m_first_name;
        /**
         * @var m_has_aggressive_anti_spam_enabled
         * @brief Optional. True, if aggressive anti-spam checks are enabled in the supergroup.
         * @details The field is only available to chat administrators.
         * @details Returned only in getChat.
         */
        std::optional<bool> m_has_aggressive_anti_spam_enabled;
        /**
         * @var m_has_hidden_members
         * @brief Optional. True, if non-administrators can only get the list of bots and administrators in the chat.
         * @details Returned only in getChat.
         */
        std::optional<bool> m_has_hidden_members;
        /**
         * @var m_has_private_forwards
         * @brief Optional. True, if privacy settings of the other party in the private chat allows to use tg://user?id=\<user_id> links only in chats with the user.
         * @return Returned only in getChat.
         */
        std::optional<bool> m_has_private_forwards;
        /**
         * @var m_has_protected_content
         * @brief Optional. True, if messages from the chat can't be forwarded to other chats.
         * @details Returned only in getChat.
         */
        std::optional<bool> m_has_protected_content;
        /**
         * @var m_has_restricted_voice_and_video_messages
         * @brief Optional. True, if the privacy settings of the other party restrict sending voice and video note messages in the private chat.
         * @details Returned only in getChat.
         */
        std::optional<bool> m_has_restricted_voice_and_video_messages;
        /**
         * @var m_id
         * @brief Unique identifier for this chat.
         * @details This number may have more than 32 significant bits and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a signed 64-bit integer or double-precision float type are safe for storing this identifier.
         */
        std::int64_t m_id;
        /**
         * @var m_invite_link
         * @brief Optional. Primary invite link, for groups, supergroups and channel chats.
         * @details Returned only in getChat.
         */
        std::optional<std::string> m_invite_link;
        /**
         * @var m_is_forum
         * @brief Optional. True, if the supergroup chat is a forum (has topics enabled).
         */
        std::optional<bool> m_is_forum;
        /**
         * @var m_join_by_request
         * @brief Optional. True, if all users directly joining the supergroup need to be approved by supergroup administrators.
         * @details Returned only in getChat.
         */
        std::optional<bool> m_join_by_request;
        /**
         * @var m_join_to_send_messages
         * @brief Optional. True, if users need to join the supergroup before they can send messages.
         * @details Returned only in getChat.
         */
        std::optional<bool> m_join_to_send_messages;
        /**
         * @var m_last_name
         * @brief Optional. Last name of the other party in a private chat.
         */
        std::optional<std::string> m_last_name;
        /**
         * @var m_linked_chat_id
         * @brief Optional. Unique identifier for the linked chat, i.e. the discussion group identifier for a channel and vice versa; for supergroups and channel chats.
         * @details This identifier may be greater than 32 bits and some programming languages may have difficulty/silent defects in interpreting it. But it is smaller than 52 bits, so a signed 64 bit integer or double-precision float type are safe for storing this identifier.
         * @details Returned only in getChat.
         */
        std::optional<std::int64_t> m_linked_chat_id;
        /**
         * @var m_location
         * @brief Optional. For supergroups, the location to which the supergroup is connected.
         * @details Returned only in getChat.
         */
        std::optional<ChatLocation::ptr> m_location;
        /**
         * @var m_message_auto_delete_time
         * @brief Optional. The time after which all messages sent to the chat will be automatically deleted; in seconds.
         * @details Returned only in getChat.
         */
        std::optional<std::int32_t> m_message_auto_delete_time;
        /**
         * @var m_permissions
         * @brief Optional. Default chat member permissions, for groups and supergroups.
         * @details Returned only in getChat.
         */
        std::optional<ChatPermissions::ptr> m_permissions;
        /**
         * @var m_photo
         * @brief Optional. Chat photo.
         * @details Returned only in getChat.
         */
        std::optional<ChatPhoto::ptr> m_photo;
        /**
         * @var m_pinned_message
         * @brief Optional. The most recent pinned message (by sending date).
         * @details Returned only in getChat.
         */
        std::optional<std::shared_ptr<Message>> m_pinned_message;
        /**
         * @var m_slow_mode_delay
         * @brief Optional. For supergroups, the minimum allowed delay between consecutive messages sent by each unpriviledged user; in seconds.
         * @details Returned only in getChat.
         */
        std::optional<std::int32_t> m_slow_mode_delay;
        /**
         * @var m_sticker_set_name
         * @brief Optional. For supergroups, name of group sticker set.
         * @details Returned only in getChat.
         */
        std::optional<std::string> m_sticker_set_name;
        /**
         * @var m_title
         * @brief Optional. Title, for supergroups, channels and group chats.
         */
        std::optional<std::string> m_title;
        /**
         * @var m_type
         * @brief Type of chat, can be either “private”, “group”, “supergroup” or “channel”.
         */
        std::string m_type;
        /**
         * @var m_username
         * @brief Optional. Username, for private chats, supergroups and channels if available.
         */
        std::optional<std::string> m_username;

    public:
        //Constructors
        Chat();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit Chat(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_active_usernames
         */
        std::optional<std::vector<std::string>> get_active_usernames() const;

        /**
         * @brief Getter.
         * @return m_bio
         */
        std::optional<std::string> get_bio() const;

        /**
         * @brief Getter.
         * @return m_can_set_sticker_set
         */
        std::optional<bool> get_can_set_sticker_set() const;

        /**
         * @brief Getter.
         * @return m_description
         */
        std::optional<std::string> get_description() const;

        /**
         * @brief Getter.
         * @return m_emoji_status_custom_emoji_id
         */
        std::optional<std::string> get_emoji_status_custom_emoji_id() const;

        /**
         * @brief Getter.
         * @return m_first_name
         */
        std::optional<std::string> get_first_name() const;

        /**
         * @brief Getter.
         * @return m_has_aggressive_anti_spam_enabled
         */
        std::optional<bool> get_has_aggressive_anti_spam_enabled() const;

        /**
         * @brief Getter.
         * @return m_has_hidden_members
         */
        std::optional<bool> get_has_hidden_members() const;

        /**
         * @brief Getter.
         * @return m_has_private_forwards
         */
        std::optional<bool> get_has_private_forwards() const;

        /**
         * @brief Getter.
         * @return m_has_protected_content
         */
        std::optional<bool> get_has_protected_content() const;

        /**
         * @brief Getter.
         * @return m_has_restricted_voice_and_video_messages
         */
        std::optional<bool> get_has_restricted_voice_and_video_messages() const;

        /**
         * @brief Getter.
         * @return m_id
         */
        std::int64_t get_id() const;

        /**
         * @brief Getter.
         * @return m_invite_link
         */
        std::optional<std::string> get_invite_link() const;

        /**
         * @brief Getter.
         * @return m_is_forum
         */
        std::optional<bool> get_is_forum() const;

        /**
         * @brief Getter.
         * @return m_join_by_request
         */
        std::optional<bool> get_join_by_request() const;

        /**
         * @brief Getter.
         * @return m_join_to_send_messages
         */
        std::optional<bool> get_join_to_send_messages() const;

        /**
         * @brief Getter.
         * @return m_last_name
         */
        std::optional<std::string> get_last_name() const;

        /**
         * @brief Getter.
         * @return m_linked_chat_id
         */
        std::optional<std::int64_t> get_linked_chat_id() const;

        /**
         * @brief Getter.
         * @return m_location
         */
        std::optional<ChatLocation::ptr> get_location() const;

        /**
         * @brief Getter.
         * @return m_message_auto_delete_time
         */
        std::optional<std::int32_t> get_message_auto_delete_time() const;

        /**
         * @brief Getter.
         * @return m_permissions
         */
        std::optional<ChatPermissions::ptr> get_permissions() const;

        /**
         * @brief Getter.
         * @return m_photo
         */
        std::optional<ChatPhoto::ptr> get_photo() const;

        /**
         * @brief Getter.
         * @return m_pinned_message
         */
        std::optional<std::shared_ptr<Message>> get_pinned_message() const;

        /**
         * @brief Getter.
         * @return m_slow_mode_delay
         */
        std::optional<std::int32_t> get_slow_mode_delay() const;

        /**
         * @brief Getter.
         * @return m_sticker_set_name
         */
        std::optional<std::string> get_sticker_set_name() const;

        /**
         * @brief Getter.
         * @return m_title
         */
        std::optional<std::string> get_title() const;

        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        /**
         * @brief Getter.
         * @return m_username
         */
        std::optional<std::string> get_username() const;

        //Setter
        /**
         * @brief Setter for m_active_usernames.
         * @param[in] active_usernames See the member variable.
         */
        void set_active_usernames(const std::optional<std::vector<std::string>> &active_usernames);

        /**
         * @brief Setter for m_bio.
         * @param[in] bio See the member variable.
         */
        void set_bio(const std::optional<std::string> &bio);

        /**
         * @brief Setter for m_can_set_sticker_set.
         * @param[in] can_set_sticker_set See the member variable.
         */
        void set_can_set_sticker_set(const std::optional<bool> &can_set_sticker_set);

        /**
         * @brief Setter for m_description.
         * @param[in] description See the member variable.
         */
        void set_description(const std::optional<std::string> &description);

        /**
         * @brief Setter for m_emoji_status_custom_emoji_id.
         * @param[in] emoji_status_custom_emoji_id See the member variable.
         */
        void set_emoji_status_custom_emoji_id(const std::optional<std::string> &emoji_status_custom_emoji_id);

        /**
         * @brief Setter for m_first_name.
         * @param[in] first_name See the member variable.
         */
        void set_first_name(const std::optional<std::string> &first_name);

        /**
         * @brief Setter for m_has_aggressive_anti_spam_enabled.
         * @param[in] has_aggressive_anti_spam_enabled See the member variable.
         */
        void set_has_aggressive_anti_spam_enabled(const std::optional<bool> &has_aggressive_anti_spam_enabled);

        /**
         * @brief Setter for m_has_hidden_members.
         * @param[in] has_hidden_members See the member variable.
         */
        void set_has_hidden_members(const std::optional<bool> &has_hidden_members);

        /**
         * @brief Setter for m_has_private_forwards.
         * @param[in] has_private_forwards See the member variable.
         */
        void set_has_private_forwards(const std::optional<bool> &has_private_forwards);

        /**
         * @brief Setter for m_has_protected_content.
         * @param[in] has_protected_content See the member variable.
         */
        void set_has_protected_content(const std::optional<bool> &has_protected_content);

        /**
         * @brief Setter m_has_restricted_voice_and_video_messages.
         * @param[in] has_restricted_voice_and_video_messages See the member variable.
         */
        void set_has_restricted_voice_and_video_messages(
          const std::optional<bool> &has_restricted_voice_and_video_messages);

        /**
         * @brief Setter for m_id.
         * @param[in] id See the member variable.
         */
        void set_id(const std::int64_t &id);

        /**
         * @brief Setter for m_invite_link.
         * @param[in] invite_link See the member variable.
         */
        void set_invite_link(const std::optional<std::string> &invite_link);

        /**
         * @brief Setter for m_is_forum.
         * @param[in] is_forum See the member variable.
         */
        void set_is_forum(const std::optional<bool> &is_forum);

        /**
         * @brief Setter for m_join_by_request.
         * @param[in] join_by_request See the member variable.
         */
        void set_join_by_request(const std::optional<bool> &join_by_request);

        /**
         * @brief Setter for m_join_to_send_messages.
         * @param[in] join_to_send_messages See the member variable.
         */
        void set_join_to_send_messages(const std::optional<bool> &join_to_send_messages);

        /**
         * @brief Setter for m_last_name.
         * @param[in] last_name See the member variable.
         */
        void set_last_name(const std::optional<std::string> &last_name);

        /**
         * @brief Setter for m_linked_chat_id.
         * @param[in] linked_chat_id See the member variable.
         */
        void set_linked_chat_id(const std::optional<std::int64_t> &linked_chat_id);

        /**
         * @brief Setter for m_location.
         * @param[in] location See the member variable.
         */
        void set_location(const std::optional<ChatLocation::ptr> &location);

        /**
         * @brief Setter for m_message_auto_delete_time.
         * @param[in] message_auto_delete_time See the member variable.
         */
        void set_message_auto_delete_time(const std::optional<std::int32_t> &message_auto_delete_time);

        /**
         * @brief Setter for m_permissions.
         * @param[in] permissions See the member variable.
         */
        void set_permissions(const std::optional<ChatPermissions::ptr> &permissions);

        /**
         * @brief Setter for m_photo.
         * @param[in] photo See the member variable.
         */
        void set_photo(const std::optional<ChatPhoto::ptr> &photo);

        /**
         * @brief Setter for m_pinned_message.
         * @param[in] pinned_message See the member variable.
         */
        void set_pinned_message(const std::optional<std::shared_ptr<Message>> &pinned_message);

        /**
         * @brief Setter for m_slow_mode_delay.
         * @param[in] slow_mode_delay See the member variable.
         */
        void set_slow_mode_delay(const std::optional<std::int32_t> &slow_mode_delay);

        /**
         * @brief Setter for m_sticker_set_name.
         * @param[in] sticker_set_name See the member variable.
         */
        void set_sticker_set_name(const std::optional<std::string> &sticker_set_name);

        /**
         * @brief Setter for m_title.
         * @param[in] title See the member variable.
         */
        void set_title(const std::optional<std::string> &title);

        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);

        /**
         * @brief Setter for m_username.
         * @param[in] username See the member variable.
         */
        void set_username(const std::optional<std::string> &username);
    };
} //namespace tgbot

#endif
