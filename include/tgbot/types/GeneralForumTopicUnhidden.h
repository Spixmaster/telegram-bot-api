#ifndef TGBOT_TYPES_GENERALFORUMTOPICUNHIDDEN_H
#define TGBOT_TYPES_GENERALFORUMTOPICUNHIDDEN_H

#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class GeneralForumTopicUnhidden
     * @brief This object represents a service message about General forum topic unhidden in the chat.
     * @details Currently holds no information.
     */
    class GeneralForumTopicUnhidden : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<GeneralForumTopicUnhidden>;

        //Constructors
        GeneralForumTopicUnhidden();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit GeneralForumTopicUnhidden(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;
    };
} //namespace tgbot

#endif
