#ifndef TGBOT_TYPES_LOCATION_H
#define TGBOT_TYPES_LOCATION_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class Location
     * @brief This object represents a point on the map.
     */
    class Location : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<Location>;

    private:
        //Member variables
        /**
         * @var m_heading
         * @brief Optional. The direction in which user is moving, in degrees; 1-360.
         * @details For active live locations only.
         */
        std::optional<std::int32_t> m_heading;
        /**
         * @var m_horizontal_accuracy
         * @brief Optional. The radius of uncertainty for the location, measured in meters; 0-1500.
         */
        std::optional<float> m_horizontal_accuracy;
        /**
         * @var m_latitude
         * @brief Latitude as defined by sender.
         */
        float m_latitude;
        /**
         * @var m_live_period
         * @brief Optional. Time relative to the message sending date, during which the location can be updated; in seconds.
         * @details For active live locations only.
         */
        std::optional<std::int32_t> m_live_period;
        /**
         * @var m_longitude
         * @brief Longitude as defined by sender.
         */
        float m_longitude;
        /**
         * @var m_proximity_alert_radius
         * @brief Optional. The maximum distance for proximity alerts about approaching another chat member, in meters.
         * @details For sent live locations only.
         */
        std::optional<std::int32_t> m_proximity_alert_radius;

    public:
        //Constructors
        Location();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit Location(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_heading
         */
        std::optional<std::int32_t> get_heading() const;

        /**
         * @brief Getter.
         * @return m_horizontal_accuracy
         */
        std::optional<float> get_horizontal_accuracy() const;

        /**
         * @brief Getter.
         * @return m_latitude
         */
        float get_latitude() const;

        /**
         * @brief Getter.
         * @return m_live_period
         */
        std::optional<std::int32_t> get_live_period() const;

        /**
         * @brief Getter.
         * @return m_longitude
         */
        float get_longitude() const;

        /**
         * @brief Getter.
         * @return m_proximity_alert_radius
         */
        std::optional<std::int32_t> get_proximity_alert_radius() const;

        //Setter
        /**
         * @brief Setter for m_heading.
         * @param[in] heading See the member variable.
         */
        void set_heading(const std::optional<std::int32_t> &heading);

        /**
         * @brief Setter for m_horizontal_accuracy.
         * @param[in] horizontal_accuracy See the member variable.
         */
        void set_horizontal_accuracy(const std::optional<float> &horizontal_accuracy);

        /**
         * @brief Setter for m_latitude.
         * @param[in] latitude See the member variable.
         */
        void set_latitude(const float &latitude);

        /**
         * @brief Setter for m_live_period.
         * @param[in] live_period See the member variable.
         */
        void set_live_period(const std::optional<std::int32_t> &live_period);

        /**
         * @brief Setter for m_longitude.
         * @param[in] longitude See the member variable.
         */
        void set_longitude(const float &longitude);

        /**
         * @brief Setter for m_proximity_alert_radius.
         * @param[in] proximity_alert_radius See the member variable.
         */
        void set_proximity_alert_radius(const std::optional<std::int32_t> &proximity_alert_radius);
    };
} //namespace tgbot

#endif
