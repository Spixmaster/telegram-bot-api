#ifndef TGBOT_TYPES_VIDEONOTE_H
#define TGBOT_TYPES_VIDEONOTE_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"
#include "tgbot/types/PhotoSize.h"

namespace tgbot
{
    /**
     * @class VideoNote
     * @brief This object represents a video message (available in Telegram apps as of v.4.0).
     */
    class VideoNote : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<VideoNote>;

    private:
        //Member variables
        /**
         * @var m_duration
         * @brief Duration of the video in seconds as defined by sender.
         */
        std::int32_t m_duration;
        /**
         * @var m_file_id
         * @brief Identifier for this file, which can be used to download or reuse the file.
         */
        std::string m_file_id;
        /**
         * @var m_file_size
         * @brief Optional. File size in bytes.
         */
        std::optional<std::int32_t> m_file_size;
        /**
         * @var m_file_unique_id
         * @brief Unique identifier for this file, which is supposed to be the same over time and for different bots.
         * @details Can't be used to download or reuse the file.
         */
        std::string m_file_unique_id;
        /**
         * @var m_length
         * @brief Video width and height (diameter of the video message) as defined by sender.
         */
        std::int32_t m_length;
        /**
         * @var m_thumbnail
         * @brief Optional. Video thumbnail.
         */
        std::optional<PhotoSize::ptr> m_thumbnail;

    public:
        //Constructors
        VideoNote();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit VideoNote(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_duration
         */
        std::int32_t get_duration() const;

        /**
         * @brief Getter.
         * @return m_file_id
         */
        std::string get_file_id() const;

        /**
         * @brief Getter.
         * @return m_file_size
         */
        std::optional<std::int32_t> get_file_size() const;

        /**
         * @brief Getter.
         * @return m_file_unique_id
         */
        std::string get_file_unique_id() const;

        /**
         * @brief Getter.
         * @return m_length
         */
        std::int32_t get_length() const;

        /**
         * @brief Getter.
         * @return m_thumbnail
         */
        std::optional<PhotoSize::ptr> get_thumbnail() const;

        //Setter
        /**
         * @brief Setter for m_duration.
         * @param[in] duration See the member variable.
         */
        void set_duration(const std::int32_t &duration);

        /**
         * @brief Setter for m_file_id.
         * @param[in] file_id See the member variable.
         */
        void set_file_id(const std::string &file_id);

        /**
         * @brief Setter for m_file_size.
         * @param[in] file_size See the member variable.
         */
        void set_file_size(const std::optional<std::int32_t> &file_size);

        /**
         * @brief Setter for m_file_unique_id.
         * @param[in] file_unique_id See the member variable.
         */
        void set_file_unique_id(const std::string &file_unique_id);

        /**
         * @brief Setter for m_length.
         * @param[in] length See the member variable.
         */
        void set_length(const std::int32_t &length);

        /**
         * @brief Setter for m_thumbnail.
         * @param[in] thumbnail See the member variable.
         */
        void set_thumbnail(const std::optional<PhotoSize::ptr> &thumbnail);
    };
} //namespace tgbot

#endif
