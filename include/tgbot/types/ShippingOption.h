#ifndef TGBOT_TYPES_SHIPPINGOPTION_H
#define TGBOT_TYPES_SHIPPINGOPTION_H

#include <memory>
#include <string>
#include <vector>

#include "tgbot/Type.h"
#include "tgbot/types/LabeledPrice.h"

namespace tgbot
{
    /**
     * @class ShippingOption
     * @brief This object represents one shipping option.
     */
    class ShippingOption : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<ShippingOption>;

    private:
        //Member variables
        /**
         * @var m_id
         * @brief Shipping option identifier.
         */
        std::string m_id;
        /**
         * @var m_prices
         * @brief List of price portions.
         */
        std::vector<LabeledPrice::ptr> m_prices;
        /**
         * @var m_title
         * @brief Option title.
         */
        std::string m_title;

    public:
        //Constructors
        ShippingOption();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit ShippingOption(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_id
         */
        std::string get_id() const;

        /**
         * @brief Getter.
         * @return m_prices
         */
        std::vector<LabeledPrice::ptr> get_prices() const;

        /**
         * @brief Getter.
         * @return m_title
         */
        std::string get_title() const;

        //Setter
        /**
         * @brief Setter for m_id.
         * @param[in] id See the member variable.
         */
        void set_id(const std::string &id);

        /**
         * @brief Setter for m_prices.
         * @param[in] prices See the member variable.
         */
        void set_prices(const std::vector<LabeledPrice::ptr> &prices);

        /**
         * @brief Setter for m_title.
         * @param[in] title See the member variable.
         */
        void set_title(const std::string &title);
    };
} //namespace tgbot

#endif
