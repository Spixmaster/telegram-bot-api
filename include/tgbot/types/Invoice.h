#ifndef TGBOT_TYPES_INVOICE_H
#define TGBOT_TYPES_INVOICE_H

#include <cstdint>
#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class Invoice
     * @brief This object contains basic information about an invoice.
     */
    class Invoice : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<Invoice>;

    private:
        //Member variables
        /**
         * @var m_currency
         * @brief Three-letter ISO 4217 currency code.
         */
        std::string m_currency;
        /**
         * @var m_description
         * @brief Product description.
         */
        std::string m_description;
        /**
         * @var m_start_parameter
         * @brief Unique bot deep-linking parameter that can be used to generate this invoice.
         */
        std::string m_start_parameter;
        /**
         * @var m_title
         * @brief Product name.
         */
        std::string m_title;
        /**
         * @var m_total_amount
         * @brief Total price in the smallest units of the currency (integer, not float/double).
         * @details For example, for a price of US$ 1.45 pass amount = 145.
         * @details See the exp parameter in currencies.json, it shows the number of digits past the decimal point for each currency (2 for the majority of currencies).
         */
        std::int32_t m_total_amount;

    public:
        //Constructors
        Invoice();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit Invoice(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_currency
         */
        std::string get_currency() const;

        /**
         * @brief Getter.
         * @return m_description
         */
        std::string get_description() const;

        /**
         * @brief Getter.
         * @return m_start_parameter
         */
        std::string get_start_parameter() const;

        /**
         * @brief Getter.
         * @return m_title
         */
        std::string get_title() const;

        /**
         * @brief Getter.
         * @return m_total_amount
         */
        std::int32_t get_total_amount() const;

        //Setter
        /**
         * @brief Setter for m_currency.
         * @param[in] currency See the member variable.
         */
        void set_currency(const std::string &currency);

        /**
         * @brief Setter for m_description.
         * @param[in] description See the member variable.
         */
        void set_description(const std::string &description);

        /**
         * @brief Setter for m_start_parameter.
         * @param[in] start_parameter See the member variable.
         */
        void set_start_parameter(const std::string &start_parameter);

        /**
         * @brief Setter for m_title.
         * @param[in] title See the member variable.
         */
        void set_title(const std::string &title);

        /**
         * @brief Setter for m_total_amount.
         * @param[in] total_amount See the member variable.
         */
        void set_total_amount(const std::int32_t &total_amount);
    };
} //namespace tgbot

#endif
