#ifndef TGBOT_TYPES_CALLBACKQUERY_H
#define TGBOT_TYPES_CALLBACKQUERY_H

#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"
#include "tgbot/types/Message.h"
#include "tgbot/types/User.h"

namespace tgbot
{
    /**
     * @class CallbackQuery
     * @brief This object represents an incoming callback query from a callback button in an inline keyboard.
     * @details If the button that originated the query was attached to a message sent by the bot, the field message will be present.
     * @details If the button was attached to a message sent via the bot (in inline mode), the field inline_message_id will be present.
     * @details Exactly one of the fields data or game_short_name will be present.
     * @note After the user presses a callback button, Telegram clients will display a progress bar until you call answerCallbackQuery. It is, therefore, necessary to react by calling answerCallbackQuery even if no notification to the user is needed (e.g., without specifying any of the optional parameters).
     */
    class CallbackQuery : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<CallbackQuery>;

    private:
        //Member variables
        /**
         * @var m_chat_instance
         * @brief Global identifier, uniquely corresponding to the chat to which the message with the callback button was sent.
         * @details Useful for high scores in games.
         */
        std::string m_chat_instance;
        /**
         * @var m_data
         * @brief Optional. Data associated with the callback button.
         * @note Be aware that the message originated the query can contain no callback buttons with this data.
         */
        std::optional<std::string> m_data;
        /**
         * @var m_from
         * @brief Sender.
         */
        User::ptr m_from;
        /**
         * @var m_game_short_name
         * @brief Optional. Short name of a Game to be returned, serves as the unique identifier for the game.
         */
        std::optional<std::string> m_game_short_name;
        /**
         * @var m_id
         * @brief Unique identifier for this query.
         */
        std::string m_id;
        /**
         * @var m_inline_message_id
         * @brief Optional. Identifier of the message sent via the bot in inline mode, that originated the query.
         */
        std::optional<std::string> m_inline_message_id;
        /**
         * @var m_message
         * @brief Optional. Message with the callback button that originated the query.
         * @note Note that message content and message date will not be available if the message is too old.
         */
        std::optional<Message::ptr> m_message;

    public:
        //Constructors
        CallbackQuery();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit CallbackQuery(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_chat_instance
         */
        std::string get_chat_instance() const;

        /**
         * @brief Getter.
         * @return m_data
         */
        std::optional<std::string> get_data() const;

        /**
         * @brief Getter.
         * @return m_from
         */
        User::ptr get_from() const;

        /**
         * @brief Getter.
         * @return m_game_short_name
         */
        std::optional<std::string> get_game_short_name() const;

        /**
         * @brief Getter.
         * @return m_id
         */
        std::string get_id() const;

        /**
         * @brief Getter.
         * @return m_inline_message_id
         */
        std::optional<std::string> get_inline_message_id() const;

        /**
         * @brief Getter.
         * @return m_message
         */
        std::optional<Message::ptr> get_message() const;

        //Setter
        /**
         * @brief Setter for m_chat_instance.
         * @param[in] chat_instance See the member variable.
         */
        void set_chat_instance(const std::string &chat_instance);

        /**
         * @brief Setter for m_data.
         * @param[in] data See the member variable.
         */
        void set_data(const std::optional<std::string> &data);

        /**
         * @brief Setter for m_from.
         * @param[in] from See the member variable.
         */
        void set_from(const User::ptr &from);

        /**
         * @brief Setter for m_game_short_name.
         * @param[in] game_short_name See the member variable.
         */
        void set_game_short_name(const std::optional<std::string> &game_short_name);

        /**
         * @brief Setter for m_id.
         * @param[in] id See the member variable.
         */
        void set_id(const std::string &id);

        /**
         * @brief Setter for m_inline_message_id.
         * @param[in] inline_message_id See the member variable.
         */
        void set_inline_message_id(const std::optional<std::string> &inline_message_id);

        /**
         * @brief Setter for m_message.
         * @param[in] message See the member variable.
         */
        void set_message(const std::optional<Message::ptr> &message);
    };
} //namespace tgbot

#endif
