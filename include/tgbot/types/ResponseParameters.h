#ifndef TGBOT_TYPES_RESPONSEPARAMETERS_H
#define TGBOT_TYPES_RESPONSEPARAMETERS_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class ResponseParameters
     * @brief Describes why a request was unsuccessful.
     */
    class ResponseParameters : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<ResponseParameters>;

    private:
        //Member variables
        /**
         * @var m_migrate_to_chat_id
         * @brief Optional. The group has been migrated to a supergroup with the specified identifier.
         * @details This number may have more than 32 significant bits and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a signed 64-bit integer or double-precision float type are safe for storing this identifier.
         */
        std::optional<std::int64_t> m_migrate_to_chat_id;
        /**
         * @var m_retry_after
         * @brief Optional. In case of exceeding flood control, the number of seconds left to wait before the request can be repeated.
         */
        std::optional<std::int32_t> m_retry_after;

    public:
        //Constructors
        ResponseParameters();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit ResponseParameters(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_migrate_to_chat_id
         */
        std::optional<std::int64_t> get_migrate_to_chat_id() const;

        /**
         * @brief Getter.
         * @return m_retry_after
         */
        std::optional<std::int32_t> get_retry_after() const;

        //Setter
        /**
         * @brief Setter for m_migrate_to_chat_id.
         * @param[in] migrate_to_chat_id See the member variable.
         */
        void set_migrate_to_chat_id(const std::optional<std::int64_t> &migrate_to_chat_id);

        /**
         * @brief Setter for m_retry_after.
         * @param[in] retry_after See the member variable.
         */
        void set_retry_after(const std::optional<std::int32_t> &retry_after);
    };
} //namespace tgbot

#endif
