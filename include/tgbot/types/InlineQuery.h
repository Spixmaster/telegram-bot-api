#ifndef TGBOT_TYPES_INLINEQUERY_H
#define TGBOT_TYPES_INLINEQUERY_H

#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"
#include "tgbot/types/Location.h"
#include "tgbot/types/User.h"

namespace tgbot
{
    /**
     * @class InlineQuery
     * @brief This object represents an incoming inline query.
     * @details When the user sends an empty query, your bot could return some default or trending results.
     */
    class InlineQuery : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<InlineQuery>;

    private:
        //Member variables
        /**
         * @var m_chat_type
         * @brief Optional. Type of the chat from which the inline query was sent.
         * @details Can be either “sender” for a private chat with the inline query sender, “private”, “group”, “supergroup”, or “channel”.
         * @details The chat type should be always known for requests sent from official clients and most third-party clients, unless the request was sent from a secret chat.
         */
        std::optional<std::string> m_chat_type;
        /**
         * @var m_from
         * @brief Sender.
         */
        User::ptr m_from;
        /**
         * @var m_id
         * @brief Unique identifier for this query.
         */
        std::string m_id;
        /**
         * @var m_location
         * @brief Optional. Sender location, only for bots that request user location.
         */
        std::optional<Location::ptr> m_location;
        /**
         * @var m_offset
         * @brief Offset of the results to be returned, can be controlled by the bot.
         */
        std::string m_offset;
        /**
         * @var m_query
         * @brief Text of the query (up to 256 characters).
         */
        std::string m_query;

    public:
        //Constructors
        InlineQuery();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit InlineQuery(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_chat_type
         */
        std::optional<std::string> get_chat_type() const;

        /**
         * @brief Getter.
         * @return m_from
         */
        User::ptr get_from() const;

        /**
         * @brief Getter.
         * @return m_id
         */
        std::string get_id() const;

        /**
         * @brief Getter.
         * @return m_location
         */
        std::optional<Location::ptr> get_location() const;

        /**
         * @brief Getter.
         * @return m_offset
         */
        std::string get_offset() const;

        /**
         * @brief Getter.
         * @return m_query
         */
        std::string get_query() const;

        //Setter
        /**
         * @brief Setter for m_chat_type.
         * @param[in] chat_type See the member variable.
         */
        void set_chat_type(const std::optional<std::string> &chat_type);

        /**
         * @brief Setter for m_from.
         * @param[in] from See the member variable.
         */
        void set_from(const User::ptr &from);

        /**
         * @brief Setter for m_id.
         * @param[in] id See the member variable.
         */
        void set_id(const std::string &id);

        /**
         * @brief Setter for m_location.
         * @param[in] location See the member variable.
         */
        void set_location(const std::optional<Location::ptr> &location);

        /**
         * @brief Setter for m_offset.
         * @param[in] offset See the member variable.
         */
        void set_offset(const std::string &offset);

        /**
         * @brief Setter for m_query.
         * @param[in] query See the member variable.
         */
        void set_query(const std::string &query);
    };
} //namespace tgbot

#endif
