#ifndef TGBOT_TYPES_USER_H
#define TGBOT_TYPES_USER_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class User
     * @brief This object represents a Telegram user or bot.
     */
    class User : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<User>;

    private:
        //Member variables
        /**
         * @var m_added_to_attachment_menu
         * @brief Optional. True, if this user added the bot to the attachment menu.
         */
        std::optional<bool> m_added_to_attachment_menu;
        /**
         * @var m_can_join_groups
         * @brief Optional. True, if the bot can be invited to groups.
         * @details Returned only in getMe.
         */
        std::optional<bool> m_can_join_groups;
        /**
         * @var m_can_read_all_group_messages
         * @brief Optional. True, if privacy mode is disabled for the bot.
         * @details Returned only in getMe.
         */
        std::optional<bool> m_can_read_all_group_messages;
        /**
         * @var m_first_name
         * @brief User's or bot's first name.
         */
        std::string m_first_name;
        /**
         * @var m_id
         * @brief Unique identifier for this user or bot.
         * @details This number may have more than 32 significant bits and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a 64-bit integer or double-precision float type are safe for storing this identifier.
         */
        std::int64_t m_id;
        /**
         * @var m_is_bot
         * @brief True, if this user is a bot.
         */
        bool m_is_bot;
        /**
         * @var m_is_premium
         * @brief Optional. True, if this user is a Telegram Premium user.
         */
        std::optional<bool> m_is_premium;
        /**
         * @var m_language_code
         * @brief Optional. IETF language tag of the user's language.
         */
        std::optional<std::string> m_language_code;
        /**
         * @var m_last_name
         * @brief Optional. User's or bot's last name.
         */
        std::optional<std::string> m_last_name;
        /**
         * @var m_supports_inline_queries
         * @brief Optional. True, if the bot supports inline queries.
         * @details Returned only in getMe.
         */
        std::optional<bool> m_supports_inline_queries;
        /**
         * @var m_username
         * @brief Optional. User's or bot's username.
         */
        std::optional<std::string> m_username;

    public:
        //Constructors
        User();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit User(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_added_to_attachment_menu
         */
        std::optional<bool> get_added_to_attachment_menu() const;

        /**
         * @brief Getter.
         * @return m_can_join_groups
         */
        std::optional<bool> get_can_join_groups() const;

        /**
         * @brief Getter.
         * @return m_can_read_all_group_messages
         */
        std::optional<bool> get_can_read_all_group_messages() const;

        /**
         * @brief Getter.
         * @return m_first_name
         */
        std::string get_first_name() const;

        /**
         * @brief Getter.
         * @return m_id
         */
        std::int64_t get_id() const;

        /**
         * @brief Getter.
         * @return m_is_bot
         */
        bool get_is_bot() const;

        /**
         * @brief Getter.
         * @return m_is_premium
         */
        std::optional<bool> get_is_premium() const;

        /**
         * @brief Getter.
         * @return m_language_code
         */
        std::optional<std::string> get_language_code() const;

        /**
         * @brief Getter.
         * @return m_last_name
         */
        std::optional<std::string> get_last_name() const;

        /**
         * @brief Getter.
         * @return m_supports_inline_queries
         */
        std::optional<bool> get_supports_inline_queries() const;

        /**
         * @brief Getter.
         * @return m_username
         */
        std::optional<std::string> get_username() const;

        //Setter
        /**
         * @brief Setter for m_added_to_attachment_menu.
         * @param[in] added_to_attachment_menu See the member variable.
         */
        void set_added_to_attachment_menu(const std::optional<bool> &added_to_attachment_menu);

        /**
         * @brief Setter for m_can_join_groups.
         * @param[in] can_join_groups See the member variable.
         */
        void set_can_join_groups(const std::optional<bool> &can_join_groups);

        /**
         * @brief Setter for m_can_read_all_group_messages.
         * @param[in] can_read_all_group_messages See the member variable.
         */
        void set_can_read_all_group_messages(const std::optional<bool> &can_read_all_group_messages);

        /**
         * @brief Setter for m_first_name.
         * @param[in] first_name See the member variable.
         */
        void set_first_name(const std::string &first_name);

        /**
         * @brief Setter for m_id.
         * @param[in] id See the member variable.
         */
        void set_id(const std::int64_t &id);

        /**
         * @brief Setter for m_is_bot.
         * @param[in] is_bot See the member variable.
         */
        void set_is_bot(const bool &is_bot);

        /**
         * @brief Setter for m_is_premium.
         * @param[in] is_premium See the member variable.
         */
        void set_is_premium(const std::optional<bool> &is_premium);

        /**
         * @brief Setter for m_language_code.
         * @param[in] language_code See the member variable.
         */
        void set_language_code(const std::optional<std::string> &language_code);

        /**
         * @brief Setter for m_last_name.
         * @param[in] last_name See the member variable.
         */
        void set_last_name(const std::optional<std::string> &last_name);

        /**
         * @brief Setter for m_supports_inline_queries.
         * @param[in] supports_inline_queries See the member variable.
         */
        void set_supports_inline_queries(const std::optional<bool> &supports_inline_queries);

        /**
         * @brief Setter for m_username.
         * @param[in] username See the member variable.
         */
        void set_username(const std::optional<std::string> &username);
    };
} //namespace tgbot

#endif
