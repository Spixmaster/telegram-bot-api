#ifndef TGBOT_TYPES_AUDIO_H
#define TGBOT_TYPES_AUDIO_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"
#include "tgbot/types/PhotoSize.h"

namespace tgbot
{
    /**
     * @class Audio
     * @brief This object represents an audio file to be treated as music by the Telegram clients.
     */
    class Audio : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<Audio>;

    private:
        //Member variables
        /**
         * @var m_duration
         * @brief Duration of the audio in seconds as defined by sender.
         */
        std::int32_t m_duration;
        /**
         * @var m_file_id
         * @brief Identifier for this file, which can be used to download or reuse the file.
         */
        std::string m_file_id;
        /**
         * @var m_file_name
         * @brief Optional. Original filename as defined by sender.
         */
        std::optional<std::string> m_file_name;
        /**
         * @var m_file_size
         * @brief Optional. File size in bytes.
         * @details It can be bigger than 2^31 and some programming languages may have difficulty/silent defects in interpreting it. But it has at most 52 significant bits, so a signed 64-bit integer or double-precision float type are safe for storing this value.
         */
        std::optional<std::int64_t> m_file_size;
        /**
         * @var m_file_unique_id
         * @brief Unique identifier for this file, which is supposed to be the same over time and for different bots.
         * @details Can't be used to download or reuse the file.
         */
        std::string m_file_unique_id;
        /**
         * @var m_mime_type
         * @brief Optional. MIME type of the file as defined by sender.
         */
        std::optional<std::string> m_mime_type;
        /**
         * @var m_performer
         * @brief Optional. Performer of the audio as defined by sender or by audio tags.
         */
        std::optional<std::string> m_performer;
        /**
         * @var m_thumbnail
         * @brief Optional. Thumbnail of the album cover to which the music file belongs.
         */
        std::optional<PhotoSize::ptr> m_thumbnail;
        /**
         * @var m_title
         * @brief Optional. Title of the audio as defined by sender or by audio tags.
         */
        std::optional<std::string> m_title;

    public:
        //Constructors
        Audio();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit Audio(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_duration
         */
        std::int32_t get_duration() const;

        /**
         * @brief Getter.
         * @return m_file_id
         */
        std::string get_file_id() const;

        /**
         * @brief Getter.
         * @return m_file_name
         */
        std::optional<std::string> get_file_name() const;

        /**
         * @brief Getter.
         * @return m_file_size
         */
        std::optional<std::int64_t> get_file_size() const;

        /**
         * @brief Getter.
         * @return m_file_unique_id
         */
        std::string get_file_unique_id() const;

        /**
         * @brief Getter.
         * @return m_mime_type
         */
        std::optional<std::string> get_mime_type() const;

        /**
         * @brief Getter.
         * @return m_performer
         */
        std::optional<std::string> get_performer() const;

        /**
         * @brief Getter.
         * @return m_thumbnail
         */
        std::optional<PhotoSize::ptr> get_thumbnail() const;

        /**
         * @brief Getter.
         * @return m_title
         */
        std::optional<std::string> get_title() const;

        //Setter
        /**
         * @brief Setter for m_duration.
         * @param[in] duration See the member variable.
         */
        void set_duration(const std::int32_t &duration);

        /**
         * @brief Setter for m_file_id.
         * @param[in] file_id See the member variable.
         */
        void set_file_id(const std::string &file_id);

        /**
         * @brief Setter for m_file_name.
         * @param[in] file_name See the member variable.
         */
        void set_file_name(const std::optional<std::string> &file_name);

        /**
         * @brief Setter for m_file_size.
         * @param[in] file_size See the member variable.
         */
        void set_file_size(const std::optional<std::int64_t> &file_size);

        /**
         * @brief Setter for m_file_unique_id.
         * @param[in] file_unique_id See the member variable.
         */
        void set_file_unique_id(const std::string &file_unique_id);

        /**
         * @brief Setter for m_mime_type.
         * @param[in] mime_type See the member variable.
         */
        void set_mime_type(const std::optional<std::string> &mime_type);

        /**
         * @brief Setter for m_performer.
         * @param[in] performer See the member variable.
         */
        void set_performer(const std::optional<std::string> &performer);

        /**
         * @brief Setter for m_thumbnail.
         * @param[in] thumbnail See the member variable.
         */
        void set_thumbnail(const std::optional<PhotoSize::ptr> &thumbnail);

        /**
         * @brief Setter for m_title.
         * @param[in] title See the member variable.
         */
        void set_title(const std::optional<std::string> &title);
    };
} //namespace tgbot

#endif
