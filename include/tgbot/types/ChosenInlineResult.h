#ifndef TGBOT_TYPES_CHOSENINLINERESULT_H
#define TGBOT_TYPES_CHOSENINLINERESULT_H

#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"
#include "tgbot/types/Location.h"
#include "tgbot/types/User.h"

namespace tgbot
{
    /**
     * @class ChosenInlineResult
     * @brief Represents a result of an inline query that was chosen by the user and sent to their chat partner.
     */
    class ChosenInlineResult : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<ChosenInlineResult>;

    private:
        //Member variables
        /**
         * @var m_from
         * @brief The user that chose the result.
         */
        User::ptr m_from;
        /**
         * @var m_inline_message_id
         * @brief Optional. Identifier of the sent inline message.
         * @details Available only if there is an inline keyboard attached to the message.
         * @details Will be also received in callback queries and can be used to edit the message.
         */
        std::optional<std::string> m_inline_message_id;
        /**
         * @var m_location
         * @brief Optional. Sender location, only for bots that require user location.
         */
        std::optional<Location::ptr> m_location;
        /**
         * @var m_query
         * @brief The query that was used to obtain the result.
         */
        std::string m_query;
        /**
         * @var m_result_id
         * @brief The unique identifier for the result that was chosen.
         */
        std::string m_result_id;

    public:
        //Constructors
        ChosenInlineResult();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit ChosenInlineResult(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_from
         */
        User::ptr get_from() const;

        /**
         * @brief Getter.
         * @return m_inline_message_id
         */
        std::optional<std::string> get_inline_message_id() const;

        /**
         * @brief Getter.
         * @return m_location
         */
        std::optional<Location::ptr> get_location() const;

        /**
         * @brief Getter.
         * @return m_query
         */
        std::string get_query() const;

        /**
         * @brief Getter.
         * @return m_result_id
         */
        std::string get_result_id() const;

        //Setter
        /**
         * @brief Setter for m_from.
         * @param[in] from See the member variable.
         */
        void set_from(const User::ptr &from);

        /**
         * @brief Setter for m_inline_message_id.
         * @param[in] inline_message_id See the member variable.
         */
        void set_inline_message_id(const std::optional<std::string> &inline_message_id);

        /**
         * @brief Setter for m_location.
         * @param[in] location See the member variable.
         */
        void set_location(const std::optional<Location::ptr> &location);

        /**
         * @brief Setter for m_query.
         * @param[in] query See the member variable.
         */
        void set_query(const std::string &query);

        /**
         * @brief Setter for m_result_id.
         * @param[in] result_id See the member variable.
         */
        void set_result_id(const std::string &result_id);
    };
} //namespace tgbot

#endif
