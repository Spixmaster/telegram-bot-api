#ifndef TGBOT_TYPES_ORDERINFO_H
#define TGBOT_TYPES_ORDERINFO_H

#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"
#include "tgbot/types/ShippingAddress.h"

namespace tgbot
{
    /**
     * @class OrderInfo
     * @brief This object represents information about an order.
     */
    class OrderInfo : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<OrderInfo>;

    private:
        //Member variables
        /**
         * @var m_email
         * @brief Optional. User email.
         */
        std::optional<std::string> m_email;
        /**
         * @var m_name
         * @brief Optional. User name.
         */
        std::optional<std::string> m_name;
        /**
         * @var m_phone_number
         * @brief Optional. User's phone number.
         */
        std::optional<std::string> m_phone_number;
        /**
         * @var m_shipping_address
         * @brief Optional. User shipping address.
         */
        std::optional<ShippingAddress::ptr> m_shipping_address;

    public:
        //Constructors
        OrderInfo();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit OrderInfo(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_email
         */
        std::optional<std::string> get_email() const;

        /**
         * @brief Getter.
         * @return m_name
         */
        std::optional<std::string> get_name() const;

        /**
         * @brief Getter.
         * @return m_phone_number
         */
        std::optional<std::string> get_phone_number() const;

        /**
         * @brief Getter.
         * @return m_shipping_address
         */
        std::optional<ShippingAddress::ptr> get_shipping_address() const;

        //Setter
        /**
         * @brief Setter for m_email.
         * @param[in] email See the member variable.
         */
        void set_email(const std::optional<std::string> &email);

        /**
         * @brief Setter for m_name.
         * @param[in] name See the member variable.
         */
        void set_name(const std::optional<std::string> &name);

        /**
         * @brief Setter for m_phone_number.
         * @param[in] phone_number See the member variable.
         */
        void set_phone_number(const std::optional<std::string> &phone_number);

        /**
         * @brief Setter for m_shipping_address.
         * @param[in] shipping_address See the member variable.
         */
        void set_shipping_address(const std::optional<ShippingAddress::ptr> &shipping_address);
    };
} //namespace tgbot

#endif
