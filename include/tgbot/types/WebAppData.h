#ifndef TGBOT_TYPES_WEBAPPDATA_H
#define TGBOT_TYPES_WEBAPPDATA_H

#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class WebAppData
     * @brief Describes data sent from a Web App to the bot.
     */
    class WebAppData : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<WebAppData>;

    private:
        //Member variables
        /**
         * @var m_button_text
         * @brief Text of the web_app keyboard button from which the Web App was opened.
         * @note Be aware that a bad client can send arbitrary data in this field.
         */
        std::string m_button_text;
        /**
         * @var m_data
         * @brief The data.
         * @note Be aware that a bad client can send arbitrary data in this field.
         */
        std::string m_data;

    public:
        //Constructors
        WebAppData();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit WebAppData(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_button_text
         */
        std::string get_button_text() const;

        /**
         * @brief Getter.
         * @return m_data
         */
        std::string get_data() const;

        //Setter
        /**
         * @brief Setter for m_button_text.
         * @param[in] button_text See the member variable.
         */
        void set_button_text(const std::string &button_text);

        /**
         * @brief Setter for m_data.
         * @param[in] data See the member variable.
         */
        void set_data(const std::string &data);
    };
} //namespace tgbot

#endif
