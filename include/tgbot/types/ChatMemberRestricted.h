#ifndef TGBOT_TYPES_CHATMEMBERRESTRICTED_H
#define TGBOT_TYPES_CHATMEMBERRESTRICTED_H

#include <cstdint>
#include <memory>
#include <string>

#include "tgbot/types/ChatMember.h"
#include "tgbot/types/User.h"

namespace tgbot
{
    /**
     * @class ChatMemberRestricted
     * @brief Represents a chat member that is under certain restrictions in the chat.
     * @details Supergroups only.
     */
    class ChatMemberRestricted : public ChatMember
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<ChatMemberRestricted>;

    private:
        //Member variables
        /**
         * @var m_can_add_web_page_previews
         * @brief True, if the user is allowed to add web page previews to their messages.
         */
        bool m_can_add_web_page_previews;
        /**
         * @var m_can_change_info
         * @brief True, if the user is allowed to change the chat title, photo and other settings.
         */
        bool m_can_change_info;
        /**
         * @var m_can_invite_users
         * @brief True, if the user is allowed to invite new users to the chat.
         */
        bool m_can_invite_users;
        /**
         * @var m_can_manage_topics
         * @brief True, if the user is allowed to create forum topics.
         */
        bool m_can_manage_topics;
        /**
         * @var m_can_pin_messages
         * @brief True, if the user is allowed to pin messages.
         */
        bool m_can_pin_messages;
        /**
         * @var m_can_send_audios
         * @brief True, if the user is allowed to send audios.
         */
        bool m_can_send_audios;
        /**
         * @var m_can_send_documents
         * @brief True, if the user is allowed to send documents.
         */
        bool m_can_send_documents;
        /**
         * @var m_can_send_messages
         * @brief True, if the user is allowed to send text messages, contacts, locations and venues.
         */
        bool m_can_send_messages;
        /**
         * @var m_can_send_other_messages
         * @brief True, if the user is allowed to send animations, games, stickers and use inline bots.
         */
        bool m_can_send_other_messages;
        /**
         * @var m_can_send_photos
         * @brief True, if the user is allowed to send photos.
         */
        bool m_can_send_photos;
        /**
         * @var m_can_send_polls
         * @brief True, if the user is allowed to send polls.
         */
        bool m_can_send_polls;
        /**
         * @var m_can_send_video_notes
         * @brief True, if the user is allowed to send video notes.
         */
        bool m_can_send_video_notes;
        /**
         * @var m_can_send_videos
         * @brief True, if the user is allowed to send videos.
         */
        bool m_can_send_videos;
        /**
         * @var m_can_send_voice_notes
         * @brief True, if the user is allowed to send voice notes.
         */
        bool m_can_send_voice_notes;
        /**
         * @var m_is_member
         * @brief True, if the user is a member of the chat at the moment of the request.
         */
        bool m_is_member;
        /**
         * @var m_status
         * @brief The member's status in the chat, always “restricted”.
         */
        std::string m_status;
        /**
         * @var m_until_date
         * @brief Date when restrictions will be lifted for this user; unix time.
         * @details If 0, then the user is restricted forever.
         */
        std::uint64_t m_until_date;
        /**
         * @var m_user
         * @brief Information about the user.
         */
        User::ptr m_user;

    public:
        //Constructors
        ChatMemberRestricted();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit ChatMemberRestricted(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_can_add_web_page_previews
         */
        bool get_can_add_web_page_previews() const;

        /**
         * @brief Getter.
         * @return m_can_change_info
         */
        bool get_can_change_info() const;

        /**
         * @brief Getter.
         * @return m_can_invite_users
         */
        bool get_can_invite_users() const;

        /**
         * @brief Getter.
         * @return m_can_manage_topics
         */
        bool get_can_manage_topics() const;

        /**
         * @brief Getter.
         * @return m_can_pin_messages
         */
        bool get_can_pin_messages() const;

        /**
         * @brief Getter.
         * @return m_can_send_audios
         */
        bool get_can_send_audios() const;

        /**
         * @brief Getter.
         * @return m_can_send_documents
         */
        bool get_can_send_documents() const;

        /**
         * @brief Getter.
         * @return m_can_send_messages
         */
        bool get_can_send_messages() const;

        /**
         * @brief Getter.
         * @return m_can_send_other_messages
         */
        bool get_can_send_other_messages() const;

        /**
         * @brief Getter.
         * @return m_can_send_photos
         */
        bool get_can_send_photos() const;

        /**
         * @brief Getter.
         * @return m_can_send_polls
         */
        bool get_can_send_polls() const;

        /**
         * @brief Getter.
         * @return m_can_send_video_notes
         */
        bool get_can_send_video_notes() const;

        /**
         * @brief Getter.
         * @return m_can_send_videos
         */
        bool get_can_send_videos() const;

        /**
         * @brief Getter.
         * @return m_can_send_voice_notes
         */
        bool get_can_send_voice_notes() const;

        /**
         * @brief Getter.
         * @return m_is_member
         */
        bool get_is_member() const;

        /**
         * @brief Getter.
         * @return m_status
         */
        std::string get_status() const;

        /**
         * @brief Getter.
         * @return m_until_date
         */
        std::uint64_t get_until_date() const;

        /**
         * @brief Getter.
         * @return m_user
         */
        User::ptr get_user() const;

        //Setter
        /**
         * @brief Setter for m_can_add_web_page_previews.
         * @param[in] can_add_web_page_previews See the member variable.
         */
        void set_can_add_web_page_previews(const bool &can_add_web_page_previews);

        /**
         * @brief Setter for m_can_change_info.
         * @param[in] can_change_info See the member variable.
         */
        void set_can_change_info(const bool &can_change_info);

        /**
         * @brief Setter for m_can_invite_users.
         * @param[in] can_invite_users See the member variable.
         */
        void set_can_invite_users(const bool &can_invite_users);

        /**
         * @brief Setter for m_can_manage_topics.
         * @param[in] can_manage_topics See the member variable.
         */
        void set_can_manage_topics(const bool &can_manage_topics);

        /**
         * @brief Setter for m_can_pin_messages.
         * @param[in] can_pin_messages See the member variable.
         */
        void set_can_pin_messages(const bool &can_pin_messages);

        /**
         * @brief Setter for m_can_send_audios.
         * @param[in] can_send_audios See the member variable.
         */
        void set_can_send_audios(const bool &can_send_audios);

        /**
         * @brief Setter for m_can_send_documents.
         * @param[in] can_send_documents See the member variable.
         */
        void set_can_send_documents(const bool &can_send_documents);

        /**
         * @brief Setter for m_can_send_messages.
         * @param[in] can_send_messages See the member variable.
         */
        void set_can_send_messages(const bool &can_send_messages);

        /**
         * @brief Setter for m_can_send_other_messages.
         * @param[in] can_send_other_messages See the member variable.
         */
        void set_can_send_other_messages(const bool &can_send_other_messages);

        /**
         * @brief Setter for m_can_send_photos.
         * @param[in] can_send_photos See the member variable.
         */
        void set_can_send_photos(const bool &can_send_photos);

        /**
         * @brief Setter for m_can_send_polls.
         * @param[in] can_send_polls See the member variable.
         */
        void set_can_send_polls(const bool &can_send_polls);

        /**
         * @brief Setter for m_can_send_video_notes.
         * @param[in] can_send_video_notes See the member variable.
         */
        void set_can_send_video_notes(const bool &can_send_video_notes);

        /**
         * @brief Setter for m_can_send_videos.
         * @param[in] can_send_videos See the member variable.
         */
        void set_can_send_videos(const bool &can_send_videos);

        /**
         * @brief Setter for m_can_send_voice_notes.
         * @param[in] can_send_voice_notes See the member variable.
         */
        void set_can_send_voice_notes(const bool &can_send_voice_notes);

        /**
         * @brief Setter for m_is_member.
         * @param[in] is_member See the member variable.
         */
        void set_is_member(const bool &is_member);

        /**
         * @brief Setter for m_status.
         * @param[in] status See the member variable.
         */
        void set_status(const std::string &status);

        /**
         * @brief Setter for m_until_date.
         * @param[in] until_date See the member variable.
         */
        void set_until_date(const std::uint64_t &until_date);

        /**
         * @brief Setter for m_user.
         * @param[in] user See the member variable.
         */
        void set_user(const User::ptr &user);
    };
} //namespace tgbot

#endif
