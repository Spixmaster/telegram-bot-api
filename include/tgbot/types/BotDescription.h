#ifndef TGBOT_TYPES_BOTDESCRIPTION_H
#define TGBOT_TYPES_BOTDESCRIPTION_H

#include <memory>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class BotDescription
     * @brief This object represents the bot's description.
     */
    class BotDescription : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<BotDescription>;

    private:
        //Member variables
        /**
         * @var m_description
         * @brief The bot's description.
         */
        std::string m_description;

    public:
        //Constructors
        BotDescription();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit BotDescription(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_description
         */
        std::string get_description() const;

        //Setter
        /**
         * @brief Setter for m_description.
         * @param[in] description See the member variable.
         */
        void set_description(const std::string &description);
    };
} //namespace tgbot

#endif
