#ifndef TGBOT_TYPES_BOTCOMMANDSCOPEALLCHATADMINISTRATORS_H
#define TGBOT_TYPES_BOTCOMMANDSCOPEALLCHATADMINISTRATORS_H

#include <memory>
#include <string>

#include "tgbot/types/BotCommandScope.h"

namespace tgbot
{
    /**
     * @class BotCommandScopeAllChatAdministrators
     * @brief Represents the scope of bot commands, covering all group and supergroup chat administrators.
     */
    class BotCommandScopeAllChatAdministrators : public BotCommandScope
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<BotCommandScopeAllChatAdministrators>;

    private:
        //Member variables
        /**
         * @var m_type
         * @brief Scope type, must be all_chat_administrators.
         */
        std::string m_type;

    public:
        //Constructors
        BotCommandScopeAllChatAdministrators();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit BotCommandScopeAllChatAdministrators(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_type
         */
        std::string get_type() const;

        //Setter
        /**
         * @brief Setter for m_type.
         * @param[in] type See the member variable.
         */
        void set_type(const std::string &type);
    };
} //namespace tgbot

#endif
