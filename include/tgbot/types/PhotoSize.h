#ifndef TGBOT_TYPES_PHOTOSIZE_H
#define TGBOT_TYPES_PHOTOSIZE_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>

#include "tgbot/Type.h"

namespace tgbot
{
    /**
     * @class PhotoSize
     * @brief This object represents one size of a photo or a file / sticker thumbnail.
     */
    class PhotoSize : public Type
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<PhotoSize>;

    private:
        //Member variables
        /**
         * @var m_file_id
         * @brief Identifier for this file, which can be used to download or reuse the file.
         */
        std::string m_file_id;
        /**
         * @var m_file_size
         * @brief Optional. File size in bytes.
         */
        std::optional<std::int32_t> m_file_size;
        /**
         * @var m_file_unique_id
         * @brief Unique identifier for this file, which is supposed to be the same over time and for different bots.
         * @details Can't be used to download or reuse the file.
         */
        std::string m_file_unique_id;
        /**
         * @var m_height
         * @brief Photo height.
         */
        std::int32_t m_height;
        /**
         * @var m_width
         * @brief Photo width.
         */
        std::int32_t m_width;

    public:
        //Constructors
        PhotoSize();

        /**
         * @throw std::invalid_argument An argument is invalid.
         * @param[in] json Serialised JSON object from which this class is constructed.
         */
        explicit PhotoSize(const std::string &json);

        //Member functions
        /**
         * @brief Serialises the class.
         * @return Serialised JSON object.
         */
        std::string serialise() const override;

        //Getter
        /**
         * @brief Getter.
         * @return m_file_id
         */
        std::string get_file_id() const;

        /**
         * @brief Getter.
         * @return m_file_size
         */
        std::optional<std::int32_t> get_file_size() const;

        /**
         * @brief Getter.
         * @return m_file_unique_id
         */
        std::string get_file_unique_id() const;

        /**
         * @brief Getter.
         * @return m_height
         */
        std::int32_t get_height() const;

        /**
         * @brief Getter.
         * @return m_width
         */
        std::int32_t get_width() const;

        //Setter
        /**
         * @brief Setter for m_file_id.
         * @param[in] file_id See the member variable.
         */
        void set_file_id(const std::string &file_id);

        /**
         * @brief Setter for m_file_size.
         * @param[in] file_size See the member variable.
         */
        void set_file_size(const std::optional<std::int32_t> &file_size);

        /**
         * @brief Setter for m_file_unique_id.
         * @param[in] file_unique_id See the member variable.
         */
        void set_file_unique_id(const std::string &file_unique_id);

        /**
         * @brief Setter for m_height.
         * @param[in] height See the member variable.
         */
        void set_height(const std::int32_t &height);

        /**
         * @brief Setter for m_width.
         * @param[in] width See the member variable.
         */
        void set_width(const std::int32_t &width);
    };
} //namespace tgbot

#endif
