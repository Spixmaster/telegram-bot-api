#ifndef TGBOT_EVENTHANDLER_H
#define TGBOT_EVENTHANDLER_H

#include <cstdint>
#include <functional>
#include <memory>
#include <optional>
#include <string>
#include <unordered_map>
#include <vector>

#include "tgbot/Bot.h"
#include "tgbot/Endpoints.h"
#include "tgbot/types/CallbackQuery.h"
#include "tgbot/types/ChatJoinRequest.h"
#include "tgbot/types/ChatMemberUpdated.h"
#include "tgbot/types/ChosenInlineResult.h"
#include "tgbot/types/InlineQuery.h"
#include "tgbot/types/Message.h"
#include "tgbot/types/Poll.h"
#include "tgbot/types/PollAnswer.h"
#include "tgbot/types/PreCheckoutQuery.h"
#include "tgbot/types/ShippingQuery.h"
#include "tgbot/types/Update.h"

namespace tgbot
{
    /**
     * @class EventHandler
     * @brief Handles incoming updates.
     */
    class EventHandler
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<EventHandler>;

    private:
        //Member variables
        /**
         * @var m_endpoints
         * @brief Sends HTTP requests.
         */
        Endpoints::ptr m_endpoints;
        /**
         * @var m_offset
         * @brief Used for the getUpdates() function of the class Endpoints.
         */
        std::optional<std::int32_t> m_offset;
        //Listeners
        //Listeners which handle only one update attribute at once.
        /**
         * @var m_callback_query_listeners
         * @brief Collection of all functions which are executed when a certain event is triggered.
         */
        std::vector<std::function<void(const Bot::ptr &, const CallbackQuery::ptr &)>> m_callback_query_listeners;
        /**
         * @var m_channel_post_listeners
         * @brief Collection of all functions which are executed when a certain event is triggered.
         */
        std::vector<std::function<void(const Bot::ptr &, const Message::ptr &)>> m_channel_post_listeners;
        /**
         * @var m_chat_join_request_listeners
         * @brief Collection of all functions which are executed when a certain event is triggered.
         */
        std::vector<std::function<void(const Bot::ptr &, const ChatJoinRequest::ptr &)>> m_chat_join_request_listeners;
        /**
         * @var m_chat_member_listeners
         * @brief Collection of all functions which are executed when a certain event is triggered.
         */
        std::vector<std::function<void(const Bot::ptr &, const ChatMemberUpdated::ptr &)>> m_chat_member_listeners;
        /**
         * @var m_chosen_inline_result_listeners
         * @brief Collection of all functions which are executed when a certain event is triggered.
         */
        std::vector<std::function<void(const Bot::ptr &, const ChosenInlineResult::ptr &)>>
          m_chosen_inline_result_listeners;
        /**
         * @var m_edited_channel_post_listeners
         * @brief Collection of all functions which are executed when a certain event is triggered.
         */
        std::vector<std::function<void(const Bot::ptr &, const Message::ptr &)>> m_edited_channel_post_listeners;
        /**
         * @var m_edited_message_listeners
         * @brief Collection of all functions which are executed when a certain event is triggered.
         */
        std::vector<std::function<void(const Bot::ptr &, const Message::ptr &)>> m_edited_message_listeners;
        /**
         * @var m_inline_query_listeners
         * @brief Collection of all functions which are executed when a certain event is triggered.
         */
        std::vector<std::function<void(const Bot::ptr &, const InlineQuery::ptr &)>> m_inline_query_listeners;
        /**
         * @var m_message_listeners
         * @brief Collection of all functions which are executed when a certain event is triggered.
         */
        std::vector<std::function<void(const Bot::ptr &, const Message::ptr &)>> m_message_listeners;
        /**
         * @var m_my_chat_member_listeners
         * @brief Collection of all functions which are executed when a certain event is triggered.
         */
        std::vector<std::function<void(const Bot::ptr &, const ChatMemberUpdated::ptr &)>> m_my_chat_member_listeners;
        /**
         * @var m_poll_answer_listeners
         * @brief Collection of all functions which are executed when a certain event is triggered.
         */
        std::vector<std::function<void(const Bot::ptr &, const PollAnswer::ptr &)>> m_poll_answer_listeners;
        /**
         * @var m_poll_listeners
         * @brief Collection of all functions which are executed when a certain event is triggered.
         */
        std::vector<std::function<void(const Bot::ptr &, const Poll::ptr &)>> m_poll_listeners;
        /**
         * @var m_pre_checkout_query_listeners
         * @brief Collection of all functions which are executed when a certain event is triggered.
         */
        std::vector<std::function<void(const Bot::ptr &, const PreCheckoutQuery::ptr &)>> m_pre_checkout_query_listeners;
        /**
         * @var m_shipping_query_listeners
         * @brief Collection of all functions which are executed when a certain event is triggered.
         */
        std::vector<std::function<void(const Bot::ptr &, const ShippingQuery::ptr &)>> m_shipping_query_listeners;
        //Listeners which handle serveral update attributes at once for convenience.
        /**
         * @var m_any_message_listeners
         * @brief Collection of all functions which are executed when a certain event is triggered.
         */
        std::vector<std::function<void(const Bot::ptr &, const Message::ptr &)>> m_any_message_listeners;
        /**
         * @var m_command_listeners
         * @brief Collection of all functions which are executed when a certain event is triggered.
         */
        std::unordered_map<std::string, std::function<void(const Bot::ptr &, const Message::ptr &)>> m_command_listeners;
        /**
         * @var m_non_command_message_listeners
         * @brief Collection of all functions which are executed when a certain event is triggered.
         */
        std::vector<std::function<void(const Bot::ptr &, const Message::ptr &)>> m_non_command_message_listeners;
        /**
         * @var m_unknown_command_listeners
         * @brief Collection of all functions which are executed when a certain event is triggered.
         */
        std::vector<std::function<void(const Bot::ptr &, const Message::ptr &)>> m_unknown_command_listeners;

    public:
        //Constructors
        /**
         * @param[in] host Host on which the Telegram bot API runs.
         * @param[in] port Port on which the Telegram bot API runs.
         * @param[in] token Telegram bot token.
         * @param[in] test_value Value for the key "test" which is sent with an HTTP request. This is needed for testing purposes.
         * @param[in] certificate_verification States whether the server certificate is verified.
         */
        EventHandler(const std::string &host, const std::int32_t &port, const std::string &token,
                     const std::optional<std::string> &test_value, const bool &certificate_verification = true);

        //Member functions
        /**
         * @brief Checks the kind of incoming update and then executes the suitable listeners.
         * @param[in] bot Telegram bot.
         * @param[in] update Update object which is handled.
         */
        void handle_update(const Bot::ptr &bot, const Update::ptr &update) const;

        /**
         * @brief Gets the updates for the bot and handles them.
         * @note Needs to be run in an endless loop to react to newly incoming updates constantly.
         * @throw std::exception An exception was thrown during the handling of the update object.
         * @param[in] bot Telegram bot.
         */
        void long_poll(const Bot::ptr &bot);

        /**
         * @brief Adds the listener to proper lister list.
         * @param[in] listener_element Listener to add.
         */
        void on_any_message(const std::function<void(const Bot::ptr &, const Message::ptr &)> &listener_element);

        /**
         * @brief Adds the listener to proper lister list.
         * @param[in] listener_element Listener to add.
         */
        void
          on_callback_query(const std::function<void(const Bot::ptr &, const CallbackQuery::ptr &)> &listener_element);

        /**
         * @brief Adds the listener to proper lister list.
         * @param[in] listener_element Listener to add.
         */
        void on_channel_post(const std::function<void(const Bot::ptr &, const Message::ptr &)> &listener_element);

        /**
         * @brief Adds the listener to proper lister list.
         * @param[in] listener_element Listener to add.
         */
        void on_chat_join_request(
          const std::function<void(const Bot::ptr &, const ChatJoinRequest::ptr &)> &listener_element);

        /**
         * @brief Adds the listener to proper lister list.
         * @param[in] listener_element Listener to add.
         */
        void
          on_chat_member(const std::function<void(const Bot::ptr &, const ChatMemberUpdated::ptr &)> &listener_element);

        /**
         * @brief Adds the listener to proper lister list.
         * @param[in] listener_element Listener to add.
         */
        void on_chosen_inline_result(
          const std::function<void(const Bot::ptr &, const ChosenInlineResult::ptr &)> &listener_element);

        /**
         * @brief Adds the listener to proper lister list.
         * @param[in] cmd The command with which the passed function is called.
         * @param[in] listener_element Listener to add.
         */
        void on_command(
          const std::string &cmd, const std::function<void(const Bot::ptr &, const Message::ptr &)> &listener_element);

        /**
         * @brief Adds the listener to proper lister list.
         * @param[in] listener_element Listener to add.
         */
        void on_edited_channel_post(const std::function<void(const Bot::ptr &, const Message::ptr &)> &listener_element);

        /**
         * @brief Adds the listener to proper lister list.
         * @param[in] listener_element Listener to add.
         */
        void on_edited_message(const std::function<void(const Bot::ptr &, const Message::ptr &)> &listener_element);

        /**
         * @brief Adds the listener to proper lister list.
         * @param[in] listener_element Listener to add.
         */
        void on_inline_query(const std::function<void(const Bot::ptr &, const InlineQuery::ptr &)> &listener_element);

        /**
         * @brief Adds the listener to proper lister list.
         * @param[in] listener_element Listener to add.
         */
        void on_message(const std::function<void(const Bot::ptr &, const Message::ptr &)> &listener_element);

        /**
         * @brief Adds the listener to proper lister list.
         * @param[in] listener_element Listener to add.
         */
        void on_my_chat_member(
          const std::function<void(const Bot::ptr &, const ChatMemberUpdated::ptr &)> &listener_element);

        /**
         * @brief Adds the listener to proper lister list.
         * @param[in] listener_element Listener to add.
         */
        void on_non_command_message(const std::function<void(const Bot::ptr &, const Message::ptr &)> &listener_element);

        /**
         * @brief Adds the listener to proper lister list.
         * @param[in] listener_element Listener to add.
         */
        void on_poll(const std::function<void(const Bot::ptr &, const Poll::ptr &)> &listener_element);

        /**
         * @brief Adds the listener to proper lister list.
         * @param[in] listener_element Listener to add.
         */
        void on_poll_answer(const std::function<void(const Bot::ptr &, const PollAnswer::ptr &)> &listener_element);

        /**
         * @brief Adds the listener to proper lister list.
         * @param[in] listener_element Listener to add.
         */
        void on_pre_checkout_query(
          const std::function<void(const Bot::ptr &, const PreCheckoutQuery::ptr &)> &listener_element);

        /**
         * @brief Adds the listener to proper lister list.
         * @param[in] listener_element Listener to add.
         */
        void
          on_shipping_query(const std::function<void(const Bot::ptr &, const ShippingQuery::ptr &)> &listener_element);

        /**
         * @brief Adds the listener to proper lister list.
         * @param[in] listener_element Listener to add.
         */
        void on_unknown_command(const std::function<void(const Bot::ptr &, const Message::ptr &)> &listener_element);

        //Getter
        /**
         * @brief Getter.
         * @return m_any_message_listeners
         */
        std::vector<std::function<void(const Bot::ptr &, const Message::ptr &)>> get_any_message_listeners();

        /**
         * @brief Getter.
         * @return m_callback_query_listeners
         */
        std::vector<std::function<void(const Bot::ptr &, const CallbackQuery::ptr &)>> get_callback_query_listeners();

        /**
         * @brief Getter.
         * @return m_channel_post_listeners
         */
        std::vector<std::function<void(const Bot::ptr &, const Message::ptr &)>> get_channel_post_listeners();

        /**
         * @brief Getter.
         * @return m_chat_member_listeners
         */
        std::vector<std::function<void(const Bot::ptr &, const ChatJoinRequest::ptr &)>>
          get_chat_join_request_listeners();

        /**
         * @brief Getter.
         * @return m_chat_member_listeners
         */
        std::vector<std::function<void(const Bot::ptr &, const ChatMemberUpdated::ptr &)>> get_chat_member_listeners();

        /**
         * @brief Getter.
         * @return m_chosen_inline_result_listeners
         */
        std::vector<std::function<void(const Bot::ptr &, const ChosenInlineResult::ptr &)>>
          get_chosen_inline_result_listeners();

        /**
         * @brief Getter.
         * @return m_command_listeners
         */
        std::unordered_map<std::string, std::function<void(const Bot::ptr &, const Message::ptr &)>>
          get_command_listeners();

        /**
         * @brief Getter.
         * @return m_edited_channel_post_listeners
         */
        std::vector<std::function<void(const Bot::ptr &, const Message::ptr &)>> get_edited_channel_post_listeners();

        /**
         * @brief Getter.
         * @return m_edited_message_listeners
         */
        std::vector<std::function<void(const Bot::ptr &, const Message::ptr &)>> get_edited_message_listeners();

        /**
         * @brief Getter.
         * @return m_endpoints
         */
        Endpoints::ptr get_endpoints() const;

        /**
         * @brief Getter.
         * @return m_inline_query_listeners
         */
        std::vector<std::function<void(const Bot::ptr &, const InlineQuery::ptr &)>> get_inline_query_listeners();

        /**
         * @brief Getter.
         * @return m_message_listeners
         */
        std::vector<std::function<void(const Bot::ptr &, const Message::ptr &)>> get_message_listeners();

        /**
         * @brief Getter.
         * @return m_my_chat_member_listeners
         */
        std::vector<std::function<void(const Bot::ptr &, const ChatMemberUpdated::ptr &)>>
          get_my_chat_member_listeners();

        /**
         * @brief Getter.
         * @return m_non_command_message_listeners
         */
        std::vector<std::function<void(const Bot::ptr &, const Message::ptr &)>> get_non_command_message_listeners();

        /**
         * @brief Getter.
         * @return m_poll_answer_listeners
         */
        std::vector<std::function<void(const Bot::ptr &, const PollAnswer::ptr &)>> get_poll_answer_listeners();

        /**
         * @brief Getter.
         * @return m_poll_listeners
         */
        std::vector<std::function<void(const Bot::ptr &, const Poll::ptr &)>> get_poll_listeners();

        /**
         * @brief Getter.
         * @return m_pre_checkout_query_listeners
         */
        std::vector<std::function<void(const Bot::ptr &, const PreCheckoutQuery::ptr &)>>
          get_pre_checkout_query_listeners();

        /**
         * @brief Getter.
         * @return m_shipping_query_listeners
         */
        std::vector<std::function<void(const Bot::ptr &, const ShippingQuery::ptr &)>> get_shipping_query_listeners();

        /**
         * @brief Getter.
         * @return m_unknown_command_listeners
         */
        std::vector<std::function<void(const Bot::ptr &, const Message::ptr &)>> get_unknown_command_listeners();
    };
} //namespace tgbot

#endif
