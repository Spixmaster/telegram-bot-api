#ifndef TGBOT_CONSTANTS_CONSTANT_H
#define TGBOT_CONSTANTS_CONSTANT_H

#include <string>

namespace tgbot::constant
{
    //Variables
    /**
     * @var release
     * @brief Release date of the version.
     */
    extern const std::string release;
    /**
     * @var version
     * @brief Software version.
     */
    extern const std::string version;
} //namespace tgbot::constant

#endif
