#ifndef TGBOT_CONSTANTS_MESSAGE_H
#define TGBOT_CONSTANTS_MESSAGE_H

#include <optional>
#include <string>

namespace tgbot::message::json
{
    //Functions
    /**
     * @brief JSON key does not exist.
     * @param[in] key Key.
     * @return Message.
     */
    std::string key_non_existent(const std::string &key);

    namespace value
    {
        //Functions
        /**
         * @brief JSON value is not an array.
         * @param[in] key Key.
         * @return Message.
         */
        std::string not_array(const std::optional<std::string> &key);

        /**
         * @brief JSON value is not a boolean.
         * @param[in] key Key.
         * @return Message.
         */
        std::string not_bool(const std::string &key);

        /**
         * @brief JSON value is not a floating point number.
         * @param[in] key Key.
         * @return Message.
         */
        std::string not_float(const std::string &key);

        /**
         * @brief JSON value is not an integer.
         * @param[in] key Key.
         * @return Message.
         */
        std::string not_int(const std::string &key);

        /**
         * @brief JSON value is not an object.
         * @param[in] key Key.
         * @return Message.
         */
        std::string not_object(const std::optional<std::string> &key);

        /**
         * @brief JSON value is not a string.
         * @param[in] key Key.
         * @return Message.
         */
        std::string not_string(const std::string &key);

        /**
         * @brief JSON value is unexpected.
         * @param[in] key Key.
         * @return Message.
         */
        std::string unexpected(const std::string &key);

        namespace nested
        {
            //Functions
            /**
             * @brief Nested JSON value is not an array.
             * @param[in] key Key.
             * @return Message.
             */
            std::string not_array(const std::string &key);

            /**
             * @brief Nested JSON value is not an integer.
             * @param[in] key Key.
             * @return Message.
             */
            std::string not_int(const std::string &key);

            /**
             * @brief Nested JSON value is not an object.
             * @param[in] key Key.
             * @return Message.
             */
            std::string not_object(const std::string &key);

            /**
             * @brief Nested JSON value is not a string.
             * @param[in] key Key.
             * @return Message.
             */
            std::string not_string(const std::string &key);
        } //namespace nested
    } //namespace value
} //namespace tgbot::message::json

#endif
