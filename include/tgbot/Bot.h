#ifndef TGBOT_BOT_H
#define TGBOT_BOT_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>

#include "tgbot/Endpoints.h"

namespace tgbot
{
    class EventHandler;
} //namespace tgbot

/**
 * @mainpage telegram-bot-api
 * @author Matheus
 * @copyright Apache-2.0 License
 * @version
 * 3.2.1 (23.03.2023)
 * - Readme update.
 * @version
 * 3.2.0 (23.03.2023)
 * - Updated to Telegram bot API version 6.6.
 * - Shortened std::vector initialisation.
 * @version
 * 3.1.5 (16.03.2023)
 * - Updated data types.
 * @version
 * 3.1.4 (10.03.2023)
 * - Use library fmt.
 * - Substituted std::size_t with std::uint64_t.
 * @version
 * 3.1.3 (02.03.2023)
 * - Variable update.
 * @version
 * 3.1.2 (02.03.2023)
 * - Fixed tests.
 * @version
 * 3.1.1 (01.03.2023)
 * - Use std::uint64_t for times.
 * @version
 * 3.1.0 (01.03.2023)
 * - Update to Telegram bot API version 6.5.
 * - Splitted the tests for the types into one file for each type.
 * - Bug fixes.
 * @version
 * 3.0.0 (24.01.2023)
 * - Submodule removed.
 * - Updated to Telegram bot API version 6.4.
 * - Replaced rapidjson with nlohmann-json.
 * @version
 * 2.7.1 (11.01.2022)
 * - Documentation update.
 * @version
 * 2.7.0 (10.01.2022)
 * - Submodule update.
 * - Updated to Telegram bot API version 5.6.
 * @version
 * 2.6.2 (27.12.2021)
 * - Submodule update.
 * - Bug fix.
 * @version
 * 2.6.1 (19.12.2021)
 * - Made an argument in EventHandler::long_poll optional which fits the semantics better.
 * @version
 * 2.6.0 (12.12.2021)
 * - Updated to Telegram bot API version 5.5.
 * @version
 * 2.5.10 (07.12.2021)
 * - Added information to exceptions.
 * @version
 * 2.5.9 (03.12.2021)
 * - Submodule update.
 * - Code revision.
 * @version
 * 2.5.8 (03.12.2021)
 * - Performance improvement.
 * @version
 * 2.5.7 (20.11.2021)
 * - Usage of a new function.
 * @version
 * 2.5.6 (15.11.2021)
 * - Submodule update.
 * @version
 * 2.5.5 (15.11.2021)
 * - Updated some identifiers.
 * @version
 * 2.5.4 (14.11.2021)
 * - Submodule update.
 * - clang-tidy update.
 * @version
 * 2.5.3 (14.11.2021)
 * - Submodule update.
 * - clang-format and clang-tidy update.
 * @version
 * 2.5.2 (12.11.2021)
 * - Submodule update.
 * @version
 * 2.5.1 (07.11.2021)
 * - Documentation update.
 * @version
 * 2.5.0 (06.11.2021)
 * - Updated to Telegram bot API version 5.4.
 * @version
 * 2.4.15 (05.11.2021)
 * - Identifiers updated.
 * @version
 * 2.4.14 (04.11.2021)
 * - Submodule update.
 * - Usage of British English.
 * - Fixed an inheritance issue.
 * @version
 * 2.4.13 (21.10.2021)
 * - Submodule update.
 * @version
 * 2.4.12 (18.10.2021)
 * - Submodule update.
 * - Sorted the classes "Constants" and "Messages".
 * @version
 * 2.4.11 (16.10.2021)
 * - Submodule update.
 * - Comment update.
 * - Renamed some identifiers.
 * @version
 * 2.4.10 (14.10.2021)
 * - Improved one condition.
 * @version
 * 2.4.9 (10.10.2021)
 * - Documentation update.
 * @version
 * 2.4.8 (10.10.2021)
 * - Submodule update.
 * @version
 * 2.4.7 (08.10.2021)
 * - Submodule update.
 * - Fixed a typo.
 * @version
 * 2.4.6 (07.10.2021)
 * - Submodule update.
 * - Fixed a typo.
 * @version
 * 2.4.5 (05.10.2021)
 * - Submodule update.
 * - CMake update.
 * @version
 * 2.4.4 (04.10.2021)
 * - Documentation update.
 * @version
 * 2.4.3 (02.10.2021)
 * - Readme update.
 * @version
 * 2.4.2 (02.10.2021)
 * - Submodule update.
 * - Readme update.
 * @version
 * 2.4.1 (02.10.2021)
 * - Submodule update.
 * - Noted optional dependency.
 * @version
 * 2.4.0 (02.10.2021)
 * - Submodule update.
 * - Noted some optional dependencies.
 * - Updated to C++20.
 * - Renamed some functions.
 * - Renamed test names.
 * @version
 * 2.3.4 (11.09.2021)
 * - Submodule update.
 * - Updated the Doxyfile.
 * @version
 * 2.3.3 (04.07.2021)
 * - Readme update.
 * @version
 * 2.3.2 (03.07.2021)
 * - Submodule update.
 * - Made variables const where possible.
 * @version
 * 2.3.1 (30.06.2021)
 * - Fixed the return type of getChatMember.
 * - Adjusted the listener elements of the event handler.
 * @version
 * 2.3.0 (27.06.2021)
 * - Updated to Telegram bot API version 5.3.
 * - Added some std::variants instead of leaving one type out.
 * @version
 * 2.2.0 (25.06.2021)
 * - Throw exceptions instead of continuing with default values or writing an error log.
 * - Exceptions are thrown from now on when the JSON object for a type is not correct or an HTTP request fails.
 * - Fixed a few missing std::optional for member variables.
 * - Submodule update.
 * @version
 * 2.1.6 (20.06.2021)
 * - Usage of std::optional instead of a default value.
 * @version
 * 2.1.5 (31.05.2021)
 * - Submodule update.
 * - Added documentation.
 * @version
 * 2.1.4 (22.05.2021)
 * - Expanded code coverage.
 * @version
 * 2.1.3 (22.05.2021)
 * - Submodule update.
 * @version
 * 2.1.2 (22.05.2021)
 * - Constant updated.
 * @version
 * 2.1.1 (21.05.2021)
 * - Submodule update.
 * @version
 * 2.1.0 (12.05.2021)
 * - Removed poco as a dependency.
 * - Rewrote HTTP client and server.
 * - Submodule update.
 * - Updated the source code to bot API 5.2.
 * @version
 * 2.0.0 (18.04.2021)
 * - Update to the new Telegram bot API version 5.1.
 * - Optional parameters of the endpoints are now able to have a null value instead of a default one and thus are not
 * sent if not necessary.
 * - Null values are also available for all types.
 * - Included tests.
 * - Submodule update.
 * @version
 * 1.4.23 (08.03.2021)
 * - Cmake update.
 * - Submodule update.
 * @version
 * 1.4.22 (07.03.2021)
 * - Submodule update.
 * - Cmake update.
 * @version
 * 1.4.21 (06.03.2021)
 * - Submodule update.
 * @version
 * 1.4.20 (10.02.2021)
 * - Submodule update.
 * - Script update.
 * @version
 * 1.4.19 (09.02.2021)
 * - Submodule update.
 * @version
 * 1.4.18 (01.02.2021)
 * - Usage of correct HTTP methods.
 * - Fixed types.
 * @version
 * 1.4.17 (31.01.2021)
 * - Fixed types.
 * @version
 * 1.4.16 (31.01.2021)
 * - Submodule update.
 * @version
 * 1.4.15 (31.01.2021)
 * - Submodule update.
 * @version
 * 1.4.14 (31.01.2021)
 * - Submodule update.
 * @version
 * 1.4.13 (31.01.2021)
 * - clang-format update.
 * - Submodule update.
 * @version
 * 1.4.12 (30.01.2021)
 * - Submodule update.
 * - Adjusted types.
 * @version
 * 1.4.11 (29.01.2021)
 * - Added missing access specifier.
 * - Updated comments.
 * @version
 * 1.4.10 (29.01.2021)
 * - Submodule update.
 * @version
 * 1.4.9 (29.01.2021)
 * - Submodule update.
 * - Revision of the whole code.
 * - Applied clang help tools.
 * @version
 * 1.4.8 (16.01.2021)
 * - Submodule update.
 * - Documentation update.
 * @version
 * 1.4.7 (16.01.2021)
 * - Submodule update.
 * @version
 * 1.4.6 (15.01.2021)
 * - Submodule update.
 * - Code check.
 * @version
 * 1.4.5 (15.01.2021)
 * - Cmake update.
 * @version
 * 1.4.4 (15.01.2021)
 * - Cmake update.
 * - Submodule update.
 * - Error fixes.
 * - Readme update.
 * @version
 * 1.4.3 (16.11.2020)
 * - Build error fix.
 * @version
 * 1.4.2 (16.11.2020)
 * - Updated CMakeLists.txt.
 * @version
 * 1.4.1 (16.11.2020)
 * - Readme update.
 * - Example updates.
 * @version
 * 1.4.0 (15.11.2020)
 * - Updated this library to the Telegram bot API version 5.0.
 * @version
 * 1.3.28 (27.10.2020)
 * - Submodule update.
 * @version
 * 1.3.27 (27.10.2020)
 * - Submodule update.
 * @version
 * 1.3.26 (14.10.2020)
 * - Submodule update.
 * @version
 * 1.3.25 (24.08.2020)
 * - Git configuration updated.
 * @version
 * 1.3.24 (22.08.2020)
 * - Submodule update.
 * - License update.
 * @version
 * 1.3.23 (15.08.2020)
 * - Submodule update.
 * - Files update.
 * @version
 * 1.3.22 (12.08.2020)
 * - Cmake update.
 * - Submodule update.
 * @version
 * 1.3.21 (12.08.2020)
 * - Cmake update.
 * - Submodule update.
 * @version
 * 1.3.20 (12.08.2020)
 * - Cmake update.
 * - Submodule update.
 * @version
 * 1.3.19 (11.08.2020)
 * - Used unused variables.
 * @version
 * 1.3.18 (10.08.2020)
 * - Cmake update.
 * @version
 * 1.3.17 (09.08.2020)
 * - Cmake update.
 * - Readme update.
 * @version
 * 1.3.16 (09.08.2020)
 * - Cmake added.
 * - Typo fixed.
 * - Submodule update.
 * @version
 * 1.3.15 (06.07.2020)
 * - Submodule update.
 * @version
 * 1.3.14 (03.07.2020)
 * - Submodule update.
 * @version
 * 1.3.13 (25.06.2020)
 * - License changed.
 * @version
 * 1.3.12 (22.06.2020)
 * - Submodule update.
 * @version
 * 1.3.11 (22.06.2020)
 * - Submodule update.
 * @version
 * 1.3.10 (21.06.2020)
 * - Submodule update.
 * - Removed contributing.
 * @version
 * 1.3.9 (20.06.2020)
 * - Submodule update.
 * - Code revision.
 * @version
 * 1.3.8 (20.06.2020)
 * - Submodule update.
 * @version
 * 1.3.7 (19.06.2020)
 * - HTTP arguments corrected.
 * @version
 * 1.3.6 (19.06.2020)
 * - Submodule updated.
 * @version
 * 1.3.5 (18.06.2020)
 * - Comment errors were fixed.
 * @version
 * 1.3.4 (16.06.2020)
 * - Readme update.
 * @version
 * 1.3.3 (16.06.2020)
 * - Script updated.
 * - Submodule updated.
 * @version
 * 1.3.2 (15.06.2020)
 * - Submodule updated.
 * - Adjusted error log.
 * @version
 * 1.3.1 (07.06.2020)
 * - Readme updated.
 * @version
 * 1.3.0 (06.06.2020)
 * - Updated to Telegram bot API version 4.9.
 * - Code revision.
 * @version
 * 1.2.15 (04.06.2020)
 * - Submodule update.
 * - Contributing update.
 * - Code revision.
 * @version
 * 1.2.14 (31.05.2020)
 * - Submodule update.
 * @version
 * 1.2.13 (29.05.2020)
 * - Submodule updated.
 * - Fixed a wrong text message.
 * @version
 * 1.2.12 (28.05.2020)
 * - Submodule updated.
 * @version
 * 1.2.11 (25.05.2020)
 * - Changed some error output.
 * - Improved some messages.
 * @version
 * 1.2.10 (24.05.2020)
 * - Submodule updated.
 * @version
 * 1.2.9 (23.05.2020)
 * - Contributing.md update.
 * @version
 * 1.2.8 (22.05.2020)
 * - Submodule updated.
 * @version
 * 1.2.7 (15.05.2020)
 * - Bug fixes.
 * @version
 * 1.2.6 (15.05.2020)
 * - Submodule update.
 * @version
 * 1.2.5 (15.05.2020)
 * - Submodule updated.
 * - Contributing updated.
 * - Readme updated.
 * - Examples updated.
 * @version
 * 1.2.4 (11.05.2020)
 * - Submodule and readme update.
 * @version
 * 1.2.3 (01.05.2020)
 * - Regenerated the documentation.
 * @version
 * 1.2.2 (01.05.2020)
 * - Improved the endpoints.
 * @version
 * 1.2.1 (29.04.2020)
 * - Updated a Doxygen page.
 * @version
 * 1.2.0 (29.04.2020)
 * - Used formatting strings in the messages file to ease translations.
 * - Applied an error log instead of output to stderr.
 * - Modified all comments to fit the Doxygen syntax.
 * - Revision of the whole code.
 */

/**
 * @page developer-log Developer log
 * @section developer-log-notes Notes
 * - Integers:
 * - 32 bits by default
 * - 64 bits when stated or the member variable is called "chat_id" or "user_id"
 *
 * @section developer-log-updating Updating
 * - Update the software and documentation with each release by comparing the differences via https://web.archive.org.
 * - Documentation status: v6.6 (17.03.2023)
 */

namespace tgbot
{
    /**
     * @class Bot
     * @brief Telegram bot.
     * @details Contains all endpoints and knows how to process incoming information.
     */
    class Bot
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<Bot>;

    private:
        //Member variables
        /**
         * @var m_endpoints
         * @brief Endpoints.
         */
        Endpoints::ptr m_endpoints;
        /**
         * @var m_event_handler
         * @brief Handles the incoming events.
         */
        std::shared_ptr<EventHandler> m_event_handler;

    public:
        //Constructors
        /**
         * @param[in] host Host on which the Telegram bot API runs.
         * @param[in] port Port on which the Telegram bot API runs.
         * @param[in] token Telegram bot token.
         * @param[in] test_value Value for the key "test" which is sent with an HTTP request. This is needed for testing purposes.
         * @param[in] certificate_verification States whether the server certificate is verified.
         */
        Bot(const std::string &host, const std::int32_t &port, const std::string &token,
            const std::optional<std::string> &test_value, const bool &certificate_verification = true);

        //Member functions
        //Getter
        /**
         * @brief Getter.
         * @return m_endpoints
         */
        Endpoints::ptr get_endpoints() const;

        /**
         * @brief Getter.
         * @return m_event_handler
         */
        std::shared_ptr<EventHandler> get_event_handler() const;
    };
} //namespace tgbot

#endif
