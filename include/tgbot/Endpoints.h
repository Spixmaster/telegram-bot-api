#ifndef TGBOT_ENDPOINTS_H
#define TGBOT_ENDPOINTS_H

#include <cstdint>
#include <memory>
#include <optional>
#include <string>
#include <variant>
#include <vector>

#include "tgbot/types/BotCommand.h"
#include "tgbot/types/BotCommandScope.h"
#include "tgbot/types/BotDescription.h"
#include "tgbot/types/BotShortDescription.h"
#include "tgbot/types/Chat.h"
#include "tgbot/types/ChatAdministratorRights.h"
#include "tgbot/types/ChatInviteLink.h"
#include "tgbot/types/ChatMember.h"
#include "tgbot/types/ChatPermissions.h"
#include "tgbot/types/File.h"
#include "tgbot/types/ForumTopic.h"
#include "tgbot/types/GameHighScore.h"
#include "tgbot/types/InlineKeyboardMarkup.h"
#include "tgbot/types/InlineQueryResult.h"
#include "tgbot/types/InputMediaAnimation.h"
#include "tgbot/types/InputMediaAudio.h"
#include "tgbot/types/InputMediaDocument.h"
#include "tgbot/types/InputMediaPhoto.h"
#include "tgbot/types/InputMediaVideo.h"
#include "tgbot/types/InputSticker.h"
#include "tgbot/types/LabeledPrice.h"
#include "tgbot/types/MaskPosition.h"
#include "tgbot/types/MenuButton.h"
#include "tgbot/types/Message.h"
#include "tgbot/types/MessageEntity.h"
#include "tgbot/types/MessageId.h"
#include "tgbot/types/PassportElementError.h"
#include "tgbot/types/Poll.h"
#include "tgbot/types/Reply.h"
#include "tgbot/types/SentWebAppMessage.h"
#include "tgbot/types/ShippingOption.h"
#include "tgbot/types/Sticker.h"
#include "tgbot/types/StickerSet.h"
#include "tgbot/types/Update.h"
#include "tgbot/types/User.h"
#include "tgbot/types/UserProfilePhotos.h"
#include "tgbot/types/WebhookInfo.h"

namespace tgbot
{
    /**
     * @class Endpoints
     * @brief Communicates with the Telegram Bot API via HTTP requests.
     */
    class Endpoints
    {
    public:
        //Pointer of itself
        /**
         * @var ptr
         * @brief Pointer of itself.
         */
        using ptr = std::shared_ptr<Endpoints>;

    private:
        //Member variables
        /**
         * @var m_certificate_verification
         * @brief States whether the server certificate is verified.
         */
        bool m_certificate_verification;
        /**
         * @var m_host
         * @brief Host on which the Telegram bot API runs.
         */
        std::string m_host;
        /**
         * @var m_port
         * @brief Port on which the Telegram bot API runs.
         */
        std::int32_t m_port;
        /**
         * @var m_test_value
         * @brief Value for the key "test" which is sent with an HTTP request.
         * @details Needed for testing purposes.
         */
        std::optional<std::string> m_test_value;
        /**
         * @var m_token
         * @brief Token from the bot which is used for identification.
         */
        std::string m_token;

    public:
        //Constructors
        /**
         * @param[in] host See the member variable.
         * @param[in] port See the member variable.
         * @param[in] token See the member variable.
         * @param[in] test_value See the member variable.
         * @param[in] certificate_verification See the member variable.
         */
        Endpoints(std::string host, const std::int32_t &port, std::string token, std::optional<std::string> test_value,
                  const bool &certificate_verification = true);

        //Member functions
        /**
         * @brief Use this method to add a new sticker to a set created by the bot.
         * @details The format of the added sticker must match the format of the other stickers in the set.
         * @details Emoji sticker sets can have up to 200 stickers.
         * @details Animated and video sticker sets can have up to 50 stickers.
         * @details Static sticker sets can have up to 120 stickers.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] user_id User identifier of sticker set owner.
         * @param[in] name Sticker set name.
         * @param[in] sticker A JSON-serialized object with information about the added sticker. If exactly the same sticker had already been added to the set, then the set isn't changed.
         * @return Returns True on success.
         */
        bool
          addStickerToSet(const std::int64_t &user_id, const std::string &name, const InputSticker::ptr &sticker) const;

        /**
         * @brief Use this method to send answers to callback queries sent from inline keyboards.
         * @details The answer will be displayed to the user as a notification at the top of the chat screen or as an alert.
         * @details Alternatively, the user can be redirected to the specified Game URL. For this option to work, you must first create a game for your bot via \@BotFather and accept the terms. Otherwise, you may use links like t.me/your_bot?start=XXXX that open your bot with a parameter.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] callback_query_id Unique identifier for the query to be answered.
         * @param[in] text Text of the notification. If not specified, nothing will be shown to the user, 0-200 characters.
         * @param[in] show_alert If True, an alert will be shown by the client instead of a notification at the top of the chat screen. Defaults to false.
         * @param[in] url URL that will be opened by the user's client. If you have created a Game and accepted the conditions via \@BotFather, specify the URL that opens your game - note that this will only work if the query comes from a callback_game button. Otherwise, you may use links like t.me/your_bot?start=XXXX that open your bot with a parameter.
         * @param[in] cache_time The maximum amount of time in seconds that the result of the callback query may be cached client-side. Telegram apps will support caching starting in version 3.14. Defaults to 0.
         * @return On success, True is returned.
         */
        bool answerCallbackQuery(
          const std::string &callback_query_id, const std::optional<std::string> &text,
          const std::optional<bool> &show_alert, const std::optional<std::string> &url,
          const std::optional<std::int32_t> &cache_time) const;

        /**
         * @brief Use this method to send answers to an inline query.
         * @details No more than 50 results per query are allowed.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] inline_query_id Unique identifier for the answered query.
         * @param[in] results A JSON-serialized array of results for the inline query.
         * @param[in] cache_time The maximum amount of time in seconds that the result of the inline query may be cached on the server. Defaults to 300.
         * @param[in] is_personal Pass True if results may be cached on the server side only for the user that sent the query. By default, results may be returned to any user who sends the same query.
         * @param[in] next_offset Pass the offset that a client should send in the next query with the same text to receive more results. Pass an empty string if there are no more results or if you don't support pagination. Offset length can't exceed 64 bytes.
         * @param[in] switch_pm_text If passed, clients will display a button with specified text that switches the user to a private chat with the bot and sends the bot a start message with the parameter switch_pm_parameter.
         * @param[in] switch_pm_parameter Deep-linking parameter for the /start message sent to the bot when user presses the switch button. 1-64 characters, only A-Z, a-z, 0-9, _ and - are allowed. Example: An inline bot that sends YouTube videos can ask the user to connect the bot to their YouTube account to adapt search results accordingly. To do this, it displays a 'Connect your YouTube account' button above the results, or even before showing any. The user presses the button, switches to a private chat with the bot and, in doing so, passes a start parameter that instructs the bot to return an OAuth link. Once done, the bot can offer a switch_inline button so that the user can easily return to the chat where they wanted to use the bot's inline capabilities.
         * @return On success, True is returned.
         */
        bool answerInlineQuery(
          const std::string &inline_query_id, const std::vector<InlineQueryResult::ptr> &results,
          const std::optional<std::int32_t> &cache_time, const std::optional<bool> &is_personal,
          const std::optional<std::string> &next_offset, const std::optional<std::string> &switch_pm_text,
          const std::optional<std::string> &switch_pm_parameter) const;

        /**
         * @brief Once the user has confirmed their payment and shipping details, the Bot API sends the final confirmation in the form of an Update with the field pre_checkout_query.
         * @details Use this method to respond to such pre-checkout queries.
         * @note The Bot API must receive an answer within 10 seconds after the pre-checkout query was sent.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] pre_checkout_query_id Unique identifier for the query to be answered.
         * @param[in] ok Specify True if everything is alright (goods are available, etc.) and the bot is ready to proceed with the order. Use False if there are any problems.
         * @param[in] error_message Required if ok is False. Error message in human readable form that explains the reason for failure to proceed with the checkout (e.g. "Sorry, somebody just bought the last of our amazing black T-shirts while you were busy filling out your payment details. Please choose a different color or garment!"). Telegram will display this message to the user.
         * @return On success, True is returned.
         */
        bool answerPreCheckoutQuery(
          const std::string &pre_checkout_query_id, const bool &ok,
          const std::optional<std::string> &error_message) const;

        /**
         * @brief If you sent an invoice requesting a shipping address and the parameter is_flexible was specified, the Bot API will send an Update with a shipping_query field to the bot.
         * @details Use this method to reply to shipping queries.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] shipping_query_id Unique identifier for the query to be answered.
         * @param[in] ok Pass True if delivery to the specified address is possible and False if there are any problems (for example, if delivery to the specified address is not possible).
         * @param[in] shipping_options Required if ok is True. A JSON-serialized array of available shipping options.
         * @param[in] error_message Required if ok is False. Error message in human readable form that explains why it is impossible to complete the order (e.g. "Sorry, delivery to your desired address is unavailable'). Telegram will display this message to the user.
         * @return On success, True is returned.
         */
        bool answerShippingQuery(
          const std::string &shipping_query_id, const bool &ok,
          const std::optional<std::vector<ShippingOption::ptr>> &shipping_options,
          const std::optional<std::string> &error_message) const;

        /**
         * @brief Use this method to set the result of an interaction with a Web App and send a corresponding message on behalf of the user to the chat from which the query originated.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] web_app_query_id Unique identifier for the query to be answered.
         * @param[in] result A JSON-serialized object describing the message to be sent.
         * @return On success, a SentWebAppMessage object is returned.
         */
        SentWebAppMessage::ptr
          answerWebAppQuery(const std::string &web_app_query_id, const InlineQueryResult::ptr &result) const;

        /**
         * @brief Use this method to approve a chat join request.
         * @details The bot must be an administrator in the chat for this to work and must have the can_invite_users administrator right.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] user_id Unique identifier of the target user.
         * @return Returns True on success.
         */
        bool approveChatJoinRequest(
          const std::variant<std::int64_t, std::string> &chat_id, const std::int64_t &user_id) const;

        /**
         * @brief Use this method to ban a user in a group, a supergroup or a channel.
         * @details In the case of supergroups and channels, the user will not be able to return to the chat on their own using invite links, etc., unless unbanned first.
         * @details The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target group or username of the target supergroup or channel (in the format \@channelusername).
         * @param[in] user_id Unique identifier of the target user.
         * @param[in] until_date Date when the user will be unbanned, unix time. If user is banned for more than 366 days or less than 30 seconds from the current time they are considered to be banned forever. Applied for supergroups and channels only.
         * @param[in] revoke_messages Pass True to delete all messages from the chat for the user that is being removed. If False, the user will be able to see messages in the group that were sent before the user was removed. Always True for supergroups and channels.
         * @return Returns True on success.
         */
        bool banChatMember(
          const std::variant<std::int64_t, std::string> &chat_id, const std::int64_t &user_id,
          const std::optional<std::uint64_t> &until_date, const std::optional<bool> &revoke_messages) const;

        /**
         * @brief Use this method to ban a channel chat in a supergroup or a channel.
         * @details Until the chat is unbanned, the owner of the banned chat won't be able to send messages on behalf of any of their channels.
         * @details The bot must be an administrator in the supergroup or channel for this to work and must have the appropriate administrator rights.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] sender_chat_id Unique identifier of the target sender chat.
         * @return Returns True on success.
         */
        bool banChatSenderChat(
          const std::variant<std::int64_t, std::string> &chat_id, const std::int64_t &sender_chat_id) const;

        /**
         * @brief Use this method to close the bot instance before moving it from one local server to another.
         * @details You need to delete the webhook before calling this method to ensure that the bot isn't launched again after server restart.
         * @details The method will return error 429 in the first 10 minutes after the bot is launched.
         * @details Requires no parameters.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @return Returns True on success.
         */
        bool close() const;

        /**
         * @brief Use this method to close an open topic in a forum supergroup chat.
         * @details The bot must be an administrator in the chat for this to work and must have the can_manage_topics administrator rights, unless it is the creator of the topic.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target supergroup (in the format \@supergroupusername).
         * @param[in] message_thread_id Unique identifier for the target message thread of the forum topic.
         * @return Returns True on success.
         */
        bool closeForumTopic(
          const std::variant<std::int64_t, std::string> &chat_id, const std::int32_t &message_thread_id) const;

        /**
         * @brief Use this method to close an open 'General' topic in a forum supergroup chat.
         * @details The bot must be an administrator in the chat for this to work and must have the can_manage_topics administrator rights.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target supergroup (in the format \@supergroupusername).
         * @return Returns True on success.
         */
        bool closeGeneralForumTopic(const std::variant<std::int64_t, std::string> &chat_id) const;

        /**
         * @brief Use this method to copy messages of any kind.
         * @details Service messages and invoice messages can't be copied.
         * @details A quiz poll can be copied only if the value of the field correct_option_id is known to the bot.
         * @details The method is analogous to the method forwardMessage, but the copied message doesn't have a link to the original message.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_thread_id Unique identifier for the target message thread (topic) of the forum; for forum supergroups only.
         * @param[in] from_chat_id Unique identifier for the chat where the original message was sent (or channel username in the format \@channelusername).
         * @param[in] message_id Message identifier in the chat specified in from_chat_id.
         * @param[in] caption New caption for media, 0-1024 characters after entities parsing. If not specified, the original caption is kept.
         * @param[in] parse_mode Mode for parsing entities in the new caption. See formatting options for more details.
         * @param[in] caption_entities A JSON-serialized list of special entities that appear in the new caption, which can be specified instead of parse_mode.
         * @param[in] disable_notification Sends the message silently. Users will receive a notification with no sound.
         * @param[in] protect_content Protects the contents of the sent message from forwarding and saving.
         * @param[in] reply_to_message_id If the message is a reply, ID of the original message.
         * @param[in] allow_sending_without_reply Pass True if the message should be sent even if the specified replied-to message is not found.
         * @param[in] reply_markup Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
         * @return Returns the MessageId of the sent message on success.
         */
        MessageId::ptr copyMessage(
          const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
          const std::variant<std::int64_t, std::string> &from_chat_id, const std::int32_t &message_id,
          const std::optional<std::string> &caption, const std::optional<std::string> &parse_mode,
          const std::optional<std::vector<MessageEntity::ptr>> &caption_entities,
          const std::optional<bool> &disable_notification, const std::optional<bool> &protect_content,
          const std::optional<std::int32_t> &reply_to_message_id,
          const std::optional<bool> &allow_sending_without_reply, const std::optional<Reply::ptr> &reply_markup) const;

        /**
         * @brief Use this method to create an additional invite link for a chat.
         * @details The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights.
         * @details The link can be revoked using the method revokeChatInviteLink.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] name Invite link name; 0-32 characters.
         * @param[in] expire_date Point in time (Unix timestamp) when the link will expire.
         * @param[in] member_limit The maximum number of users that can be members of the chat simultaneously after joining the chat via this invite link; 1-99999.
         * @param[in] creates_join_request 	True, if users joining the chat via the link need to be approved by chat administrators. If True, member_limit can't be specified.
         * @return Returns the new invite link as ChatInviteLink object.
         */
        ChatInviteLink::ptr createChatInviteLink(
          const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::string> &name,
          const std::optional<std::uint64_t> &expire_date, const std::optional<std::int32_t> &member_limit,
          const std::optional<bool> &creates_join_request) const;

        /**
         * @brief Use this method to create a topic in a forum supergroup chat.
         * @details The bot must be an administrator in the chat for this to work and must have the can_manage_topics administrator rights.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target supergroup (in the format \@supergroupusername).
         * @param[in] name Topic name, 1-128 characters.
         * @param[in] icon_color Color of the topic icon in RGB format. Currently, must be one of 7322096 (0x6FB9F0), 16766590 (0xFFD67E), 13338331 (0xCB86DB), 9367192 (0x8EEE98), 16749490 (0xFF93B2), or 16478047 (0xFB6F5F).
         * @param[in] icon_custom_emoji_id Unique identifier of the custom emoji shown as the topic icon. Use getForumTopicIconStickers to get all allowed custom emoji identifiers.
         * @return Returns information about the created topic as a ForumTopic object.
         */
        ForumTopic::ptr createForumTopic(
          const std::variant<std::int64_t, std::string> &chat_id, const std::string &name,
          const std::optional<std::int32_t> &icon_color, const std::optional<std::string> &icon_custom_emoji_id) const;

        /**
         * @brief Use this method to create a link for an invoice.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] title Product name, 1-32 characters.
         * @param[in] description Product description, 1-255 characters.
         * @param[in] payload Bot-defined invoice payload, 1-128 bytes. This will not be displayed to the user, use for your internal processes.
         * @param[in] provider_token Payment provider token, obtained via BotFather.
         * @param[in] currency Three-letter ISO 4217 currency code, see more on currencies.
         * @param[in] prices Price breakdown, a JSON-serialized list of components (e.g. product price, tax, discount, delivery cost, delivery tax, bonus, etc.).
         * @param[in] max_tip_amount The maximum accepted amount for tips in the smallest units of the currency (integer, not float/double). For example, for a maximum tip of US$ 1.45 pass max_tip_amount = 145. See the exp parameter in currencies.json, it shows the number of digits past the decimal point for each currency (2 for the majority of currencies). Defaults to 0.
         * @param[in] suggested_tip_amounts A JSON-serialized array of suggested amounts of tips in the smallest units of the currency (integer, not float/double). At most 4 suggested tip amounts can be specified. The suggested tip amounts must be positive, passed in a strictly increased order and must not exceed max_tip_amount.
         * @param[in] provider_data JSON-serialized data about the invoice, which will be shared with the payment provider. A detailed description of required fields should be provided by the payment provider.
         * @param[in] photo_url URL of the product photo for the invoice. Can be a photo of the goods or a marketing image for a service.
         * @param[in] photo_size Photo size in bytes.
         * @param[in] photo_width Photo width.
         * @param[in] photo_height Photo height.
         * @param[in] need_name Pass True if you require the user's full name to complete the order.
         * @param[in] need_phone_number Pass True if you require the user's phone number to complete the order.
         * @param[in] need_email Pass True if you require the user's email address to complete the order.
         * @param[in] need_shipping_address Pass True if you require the user's shipping address to complete the order.
         * @param[in] send_phone_number_to_provider Pass True if the user's phone number should be sent to the provider.
         * @param[in] send_email_to_provider Pass True if the user's email address should be sent to the provider.
         * @param[in] is_flexible Pass True if the final price depends on the shipping method.
         * @return Returns the created invoice link as String on success.
         */
        std::string createInvoiceLink(
          const std::string &title, const std::string &description, const std::string &payload,
          const std::string &provider_token, const std::string &currency, const std::vector<LabeledPrice::ptr> &prices,
          const std::optional<std::int32_t> &max_tip_amount,
          const std::optional<std::vector<std::int32_t>> &suggested_tip_amounts,
          const std::optional<std::string> &provider_data, const std::optional<std::string> &photo_url,
          const std::optional<std::int32_t> &photo_size, const std::optional<std::int32_t> &photo_width,
          const std::optional<std::int32_t> &photo_height, const std::optional<bool> &need_name,
          const std::optional<bool> &need_phone_number, const std::optional<bool> &need_email,
          const std::optional<bool> &need_shipping_address, const std::optional<bool> &send_phone_number_to_provider,
          const std::optional<bool> &send_email_to_provider, const std::optional<bool> &is_flexible) const;

        /**
         * @brief Use this method to create a new sticker set owned by a user.
         * @details The bot will be able to edit the sticker set thus created.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] user_id User identifier of created sticker set owner.
         * @param[in] name Short name of sticker set, to be used in t.me/addstickers/ URLs (e.g., animals). Can contain only English letters, digits and underscores. Must begin with a letter, can't contain consecutive underscores and must end in "_by_<bot_username>". \<bot_username> is case insensitive. 1-64 characters.
         * @param[in] title Sticker set title, 1-64 characters.
         * @param[in] stickers A JSON-serialized list of 1-50 initial stickers to be added to the sticker set.
         * @param[in] sticker_format Format of stickers in the set, must be one of “static”, “animated”, “video”.
         * @param[in] sticker_type Type of stickers in the set, pass “regular”, “mask”, or “custom_emoji”. By default, a regular sticker set is created.
         * @param[in] needs_repainting Pass True if stickers in the sticker set must be repainted to the color of text when used in messages, the accent color if used as emoji status, white on chat photos, or another appropriate color based on context; for custom emoji sticker sets only.
         * @return Returns True on success.
         */
        bool createNewStickerSet(
          const std::int64_t &user_id, const std::string &name, const std::string &title,
          const std::vector<InputSticker::ptr> &stickers, const std::string &sticker_format,
          const std::optional<std::string> &sticker_type, const std::optional<bool> &needs_repainting) const;

        /**
         * @brief Use this method to decline a chat join request.
         * @details The bot must be an administrator in the chat for this to work and must have the can_invite_users administrator right.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] user_id Unique identifier of the target user.
         * @return Returns True on success.
         */
        bool declineChatJoinRequest(
          const std::variant<std::int64_t, std::string> &chat_id, const std::int64_t &user_id) const;

        /**
         * @brief Use this method to delete a chat photo.
         * @details Photos can't be changed for private chats.
         * @details The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @return Returns True on success.
         */
        bool deleteChatPhoto(const std::variant<std::int64_t, std::string> &chat_id) const;

        /**
         * @brief Use this method to delete a group sticker set from a supergroup.
         * @details The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights.
         * @details Use the field can_set_sticker_set optionally returned in getChat requests to check if the bot can use this method.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target supergroup (in the format \@supergroupusername).
         * @return Returns True on success.
         */
        bool deleteChatStickerSet(const std::variant<std::int64_t, std::string> &chat_id) const;

        /**
         * @brief Use this method to delete a forum topic along with all its messages in a forum supergroup chat.
         * @details The bot must be an administrator in the chat for this to work and must have the can_delete_messages administrator rights.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target supergroup (in the format \@supergroupusername).
         * @param[in] message_thread_id Unique identifier for the target message thread of the forum topic.
         * @return Returns True on success.
         */
        bool deleteForumTopic(
          const std::variant<std::int64_t, std::string> &chat_id, const std::int32_t &message_thread_id) const;

        /**
         * @brief Use this method to delete a message, including service messages, with the following limitations:
         * A message can only be deleted if it was sent less than 48 hours ago.
         * Service messages about a supergroup, channel, or forum topic creation can't be deleted.
         * A dice message in a private chat can only be deleted if it was sent more than 24 hours ago.
         * Bots can delete outgoing messages in private chats, groups, and supergroups.
         * Bots can delete incoming messages in private chats.
         * Bots granted can_post_messages permissions can delete outgoing messages in channels.
         * If the bot is an administrator of a group, it can delete any message there.
         * If the bot has can_delete_messages permission in a supergroup or a channel, it can delete any message there.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_id Identifier of the message to delete.
         * @return Returns True on success.
         */
        bool deleteMessage(const std::variant<std::int64_t, std::string> &chat_id, const std::int32_t &message_id) const;

        /**
         * @brief Use this method to delete the list of the bot's commands for the given scope and user language.
         * @details After deletion, higher level commands will be shown to affected users.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] scope A JSON-serialized object, describing scope of users for which the commands are relevant. Defaults to BotCommandScopeDefault.
         * @param[in] language_code A two-letter ISO 639-1 language code. If empty, commands will be applied to all users from the given scope, for whose language there are no dedicated commands.
         * @return Returns True on success.
         */
        bool deleteMyCommands(
          const std::optional<BotCommandScope::ptr> &scope, const std::optional<std::string> &language_code) const;

        /**
         * @brief Use this method to delete a sticker from a set created by the bot.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] sticker File identifier of the sticker.
         * @return Returns True on success.
         */
        bool deleteStickerFromSet(const std::string &sticker) const;

        /**
         * @brief Use this method to delete a sticker set that was created by the bot.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] name Sticker set name.
         * @return Returns True on success.
         */
        bool deleteStickerSet(const std::string &name) const;

        /**
         * @brief Use this method to remove webhook integration if you decide to switch back to getUpdates.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] drop_pending_updates Pass True to drop all pending updates.
         * @return Returns True on success.
         */
        bool deleteWebhook(const std::optional<bool> &drop_pending_updates) const;

        /**
         * @brief Use this method to edit a non-primary invite link created by the bot.
         * @details The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] invite_link The invite link to edit.
         * @param[in] name Invite link name; 0-32 characters.
         * @param[in] expire_date Point in time (Unix timestamp) when the link will expire.
         * @param[in] member_limit The maximum number of users that can be members of the chat simultaneously after joining the chat via this invite link; 1-99999.
         * @param[in] creates_join_request True, if users joining the chat via the link need to be approved by chat administrators. If True, member_limit can't be specified.
         * @return Returns the edited invite link as a ChatInviteLink object.
         */
        ChatInviteLink::ptr editChatInviteLink(
          const std::variant<std::int64_t, std::string> &chat_id, const std::string &invite_link,
          const std::optional<std::string> &name, const std::optional<std::uint64_t> &expire_date,
          const std::optional<std::int32_t> &member_limit, const std::optional<bool> &creates_join_request) const;

        /**
         * @brief Use this method to edit name and icon of a topic in a forum supergroup chat.
         * @details The bot must be an administrator in the chat for this to work and must have can_manage_topics administrator rights, unless it is the creator of the topic.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target supergroup (in the format \@supergroupusername).
         * @param[in] message_thread_id Unique identifier for the target message thread of the forum topic.
         * @param[in] name New topic name, 0-128 characters. If not specified or empty, the current name of the topic will be kept.
         * @param[in] icon_custom_emoji_id New unique identifier of the custom emoji shown as the topic icon. Use getForumTopicIconStickers to get all allowed custom emoji identifiers. Pass an empty string to remove the icon. If not specified, the current icon will be kept.
         * @return Returns True on success.
         */
        bool editForumTopic(
          const std::variant<std::int64_t, std::string> &chat_id, const std::int32_t &message_thread_id,
          const std::optional<std::string> &name, const std::optional<std::string> &icon_custom_emoji_id) const;

        /**
         * @brief Use this method to edit the name of the 'General' topic in a forum supergroup chat.
         * @details The bot must be an administrator in the chat for this to work and must have can_manage_topics administrator rights.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target supergroup (in the format \@supergroupusername).
         * @param[in] name New topic name, 1-128 characters.
         * @return Returns True on success.
         */
        bool
          editGeneralForumTopic(const std::variant<std::int64_t, std::string> &chat_id, const std::string &name) const;

        /**
         * @brief Use this method to edit captions of messages.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Required if inline_message_id is not specified. Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_id Required if inline_message_id is not specified. Identifier of the message to edit.
         * @param[in] inline_message_id Required if chat_id and message_id are not specified. Identifier of the inline message.
         * @param[in] caption New caption of the message, 0-1024 characters after entities parsing.
         * @param[in] parse_mode Mode for parsing entities in the message caption. See formatting options for more details.
         * @param[in] caption_entities A JSON-serialized list of special entities that appear in the caption, which can be specified instead of parse_mode.
         * @param[in] reply_markup A JSON-serialized object for an inline keyboard.
         * @return On success, if the edited message is not an inline message, the edited Message is returned, otherwise True is returned.
         */
        std::variant<bool, Message::ptr> editMessageCaption(
          const std::optional<std::variant<std::int64_t, std::string>> &chat_id,
          const std::optional<std::int32_t> &message_id, const std::optional<std::string> &inline_message_id,
          const std::optional<std::string> &caption, const std::optional<std::string> &parse_mode,
          const std::optional<std::vector<MessageEntity::ptr>> &caption_entities,
          const std::optional<InlineKeyboardMarkup::ptr> &reply_markup) const;

        /**
         * @brief Use this method to edit live location messages.
         * @details A location can be edited until its live_period expires or editing is explicitly disabled by a call to stopMessageLiveLocation.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Required if inline_message_id is not specified. Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_id Required if inline_message_id is not specified. Identifier of the message to edit.
         * @param[in] inline_message_id Required if chat_id and message_id are not specified. Identifier of the inline message.
         * @param[in] latitude Latitude of new location.
         * @param[in] longitude Longitude of new location.
         * @param[in] horizontal_accuracy The radius of uncertainty for the location, measured in meters; 0-1500.
         * @param[in] heading Direction in which the user is moving, in degrees. Must be between 1 and 360 if specified.
         * @param[in] proximity_alert_radius The maximum distance for proximity alerts about approaching another chat member, in meters. Must be between 1 and 100000 if specified.
         * @param[in] reply_markup A JSON-serialized object for a new inline keyboard.
         * @return On success, if the edited message is not an inline message, the edited Message is returned, otherwise True is returned.
         */
        std::variant<bool, Message::ptr> editMessageLiveLocation(
          const std::optional<std::variant<std::int64_t, std::string>> &chat_id,
          const std::optional<std::int32_t> &message_id, const std::optional<std::string> &inline_message_id,
          const float &latitude, const float &longitude, const std::optional<float> &horizontal_accuracy,
          const std::optional<std::int32_t> &heading, const std::optional<std::int32_t> &proximity_alert_radius,
          const std::optional<InlineKeyboardMarkup::ptr> &reply_markup) const;

        /**
         * @brief Use this method to edit animation, audio, document, photo, or video messages.
         * @details If a message is part of a message album, then it can be edited only to an audio for audio albums, only to a document for document albums and to a photo or a video otherwise.
         * @details When an inline message is edited, a new file can't be uploaded; use a previously uploaded file via its file_id or specify a URL.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Required if inline_message_id is not specified. Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_id Required if inline_message_id is not specified. Identifier of the message to edit.
         * @param[in] inline_message_id Required if chat_id and message_id are not specified. Identifier of the inline message.
         * @param[in] media A JSON-serialized object for a new media content of the message.
         * @param[in] reply_markup A JSON-serialized object for a new inline keyboard.
         * @return On success, if the edited message is not an inline message, the edited Message is returned, otherwise True is returned.
         */
        std::variant<bool, Message::ptr> editMessageMedia(
          const std::optional<std::variant<std::int64_t, std::string>> &chat_id,
          const std::optional<std::int32_t> &message_id, const std::optional<std::string> &inline_message_id,
          const std::variant<InputMediaAnimation::ptr, InputMediaAudio::ptr, InputMediaDocument::ptr,
                             InputMediaPhoto::ptr, InputMediaVideo::ptr> &media,
          const std::optional<InlineKeyboardMarkup::ptr> &reply_markup) const;

        /**
         * @brief Use this method to edit only the reply markup of messages.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Required if inline_message_id is not specified. Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_id Required if inline_message_id is not specified. Identifier of the message to edit.
         * @param[in] inline_message_id Required if chat_id and message_id are not specified. Identifier of the inline message.
         * @param[in] reply_markup A JSON-serialized object for an inline keyboard.
         * @return On success, if the edited message is not an inline message, the edited Message is returned, otherwise True is returned.
         */
        std::variant<bool, Message::ptr> editMessageReplyMarkup(
          const std::optional<std::variant<std::int64_t, std::string>> &chat_id,
          const std::optional<std::int32_t> &message_id, const std::optional<std::string> &inline_message_id,
          const std::optional<InlineKeyboardMarkup::ptr> &reply_markup) const;

        /**
         * @brief Use this method to edit text and game messages.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Required if inline_message_id is not specified. Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_id Required if inline_message_id is not specified. Identifier of the message to edit.
         * @param[in] inline_message_id Required if chat_id and message_id are not specified. Identifier of the inline message.
         * @param[in] text New text of the message, 1-4096 characters after entities parsing.
         * @param[in] parse_mode Mode for parsing entities in the message text. See formatting options for more details.
         * @param[in] entities A JSON-serialized list of special entities that appear in message text, which can be specified instead of parse_mode.
         * @param[in] disable_web_page_preview Disables link previews for links in this message.
         * @param[in] reply_markup A JSON-serialized object for an inline keyboard.
         * @return On success, if the edited message is not an inline message, the edited Message is returned, otherwise True is returned.
         */
        std::variant<bool, Message::ptr> editMessageText(
          const std::optional<std::variant<std::int64_t, std::string>> &chat_id,
          const std::optional<std::int32_t> &message_id, const std::optional<std::string> &inline_message_id,
          const std::string &text, const std::optional<std::string> &parse_mode,
          const std::optional<std::vector<MessageEntity::ptr>> &entities,
          const std::optional<bool> &disable_web_page_preview,
          const std::optional<InlineKeyboardMarkup::ptr> &reply_markup) const;

        /**
         * @brief Use this method to generate a new primary invite link for a chat; any previously generated primary link is revoked.
         * @details The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights.
         * @note Each administrator in a chat generates their own invite links. Bots can't use invite links generated by other administrators. If you want your bot to work with invite links, it will need to generate its own link using exportChatInviteLink or by calling the getChat method. If your bot needs to generate a new primary invite link replacing its previous one, use exportChatInviteLink again.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @return Returns the new invite link as String on success.
         */
        std::string exportChatInviteLink(const std::variant<std::int64_t, std::string> &chat_id) const;

        /**
         * @brief Use this method to forward messages of any kind.
         * @details Service messages can't be forwarded.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_thread_id Unique identifier for the target message thread (topic) of the forum; for forum supergroups only.
         * @param[in] from_chat_id Unique identifier for the chat where the original message was sent (or channel username in the format \@channelusername).
         * @param[in] disable_notification Sends the message silently. Users will receive a notification with no sound.
         * @param[in] protect_content Protects the contents of the forwarded message from forwarding and saving.
         * @param[in] message_id Message identifier in the chat specified in from_chat_id.
         * @return On success, the sent Message is returned.
         */
        Message::ptr forwardMessage(
          const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
          const std::variant<std::int64_t, std::string> &from_chat_id, const std::optional<bool> &disable_notification,
          const std::optional<bool> &protect_content, const std::int32_t &message_id) const;

        /**
         * @brief Use this method to get up to date information about the chat (current name of the user for one-on-one conversations, current username of a user, group or channel, etc.).
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target supergroup or channel (in the format \@channelusername).
         * @return Returns a Chat object on success.
         */
        Chat::ptr getChat(const std::variant<std::int64_t, std::string> &chat_id) const;

        /**
         * @brief Use this method to get a list of administrators in a chat, which aren't bots.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target supergroup or channel (in the format \@channelusername).
         * @return Returns an Array of ChatMember objects.
         */
        std::vector<ChatMember::ptr> getChatAdministrators(const std::variant<std::int64_t, std::string> &chat_id) const;

        /**
         * @brief Use this method to get information about a member of a chat.
         * @details The method is only guaranteed to work for other users if the bot is an administrator in the chat.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target supergroup or channel (in the format \@channelusername).
         * @param[in] user_id Unique identifier of the target user.
         * @return Returns a ChatMember object on success.
         */
        ChatMember::ptr
          getChatMember(const std::variant<std::int64_t, std::string> &chat_id, const std::int64_t &user_id) const;

        /**
         * @brief Use this method to get the number of members in a chat.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target supergroup or channel (in the format \@channelusername).
         * @return Returns Int on success.
         */
        std::int32_t getChatMemberCount(const std::variant<std::int64_t, std::string> &chat_id) const;

        /**
         * @brief Use this method to get the current value of the bot's menu button in a private chat, or the default menu button.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target private chat. If not specified, default bot's menu button will be returned.
         * @return Returns MenuButton on success.
         */
        MenuButton::ptr getChatMenuButton(const std::optional<std::int64_t> &chat_id) const;

        /**
         * @brief Use this method to get information about custom emoji stickers by their identifiers.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] custom_emoji_ids List of custom emoji identifiers. At most 200 custom emoji identifiers can be specified.
         * @return Returns an Array of Sticker objects.
         */
        std::vector<Sticker::ptr> getCustomEmojiStickers(const std::vector<std::string> &custom_emoji_ids) const;

        /**
         * @brief Use this method to get basic information about a file and prepare it for downloading.
         * @details For the moment, bots can download files of up to 20MB in size.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] file_id File identifier to get information about.
         * @return On success, a File object is returned. The file can then be downloaded via the link https://api.telegram.org/file/bot\<token>/\<file_path>, where \<file_path> is taken from the response. It is guaranteed that the link will be valid for at least 1 hour. When the link expires, a new one can be requested by calling getFile again.
         */
        File::ptr getFile(const std::string &file_id) const;

        /**
         * @brief Use this method to get custom emoji stickers, which can be used as a forum topic icon by any user.
         * @details Requires no parameters.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @return Returns an Array of Sticker objects.
         */
        std::vector<Sticker::ptr> getForumTopicIconStickers() const;

        /**
         * @brief Use this method to get data for high score tables.
         * @details Will return the score of the specified user and several of their neighbors in a game.
         * @note This method will currently return scores for the target user, plus two of their closest neighbors on each side. Will also return the top three users if the user and their neighbors are not among them. Please note that this behavior is subject to change.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] user_id Target user id.
         * @param[in] chat_id Required if inline_message_id is not specified. Unique identifier for the target chat.
         * @param[in] message_id Required if inline_message_id is not specified. Identifier of the sent message.
         * @param[in] inline_message_id Required if chat_id and message_id are not specified. Identifier of the inline message.
         * @return Returns an Array of GameHighScore objects.
         */
        std::vector<GameHighScore::ptr> getGameHighScores(
          const std::int64_t &user_id, const std::optional<std::int64_t> &chat_id,
          const std::optional<std::int32_t> &message_id, const std::optional<std::string> &inline_message_id) const;

        /**
         * @brief A simple method for testing your bot's authentication token.
         * @details Requires no parameters.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @return Returns basic information about the bot in form of a User object.
         */
        User::ptr getMe() const;

        /**
         * @brief Use this method to get the current list of the bot's commands for the given scope and user language.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] scope A JSON-serialized object, describing scope of users. Defaults to BotCommandScopeDefault.
         * @param[in] language_code A two-letter ISO 639-1 language code or an empty string.
         * @return Returns an Array of BotCommand objects. If commands aren't set, an empty list is returned.
         */
        std::vector<BotCommand::ptr> getMyCommands(
          const std::optional<BotCommandScope::ptr> &scope, const std::optional<std::string> &language_code) const;

        /**
         * @brief Use this method to get the current default administrator rights of the bot.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] for_channels Pass True to get default administrator rights of the bot in channels. Otherwise, default administrator rights of the bot for groups and supergroups will be returned.
         * @return Returns ChatAdministratorRights on success.
         */
        ChatAdministratorRights::ptr getMyDefaultAdministratorRights(const std::optional<bool> &for_channels) const;

        /**
         * @brief Use this method to get the current bot description for the given user language.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] language_code A two-letter ISO 639-1 language code or an empty string.
         * @return Returns BotDescription on success.
         */
        BotDescription::ptr getMyDescription(const std::optional<std::string> &language_code) const;

        /**
         * @brief Use this method to get the current bot short description for the given user language.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] language_code A two-letter ISO 639-1 language code or an empty string.
         * @return Returns BotShortDescription on success.
         */
        BotShortDescription::ptr getMyShortDescription(const std::optional<std::string> &language_code) const;

        /**
         * @brief Use this method to get a sticker set.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] name Name of the sticker set.
         * @return On success, a StickerSet object is returned.
         */
        StickerSet::ptr getStickerSet(const std::string &name) const;

        /**
         * @brief Use this method to receive incoming updates using long polling (wiki).
         * @note This method will not work if an outgoing webhook is set up.
         * @note In order to avoid getting duplicate updates, recalculate offset after each server response.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] offset Identifier of the first update to be returned. Must be greater by one than the highest among the identifiers of previously received updates. By default, updates starting with the earliest unconfirmed update are returned. An update is considered confirmed as soon as getUpdates is called with an offset higher than its update_id. The negative offset can be specified to retrieve updates starting from -offset update from the end of the updates queue. All previous updates will forgotten.
         * @param[in] limit Limits the number of updates to be retrieved. Values between 1-100 are accepted. Defaults to 100.
         * @param[in] timeout Timeout in seconds for long polling. Defaults to 0, i.e. usual short polling. Should be positive, short polling should be used for testing purposes only.
         * @param[in] allowed_updates A JSON-serialized list of the update types you want your bot to receive. For example, specify [“message”, “edited_channel_post”, “callback_query”] to only receive updates of these types. See Update for a complete list of available update types. Specify an empty list to receive all update types except chat_member (default). If not specified, the previous setting will be used. Please note that this parameter doesn't affect updates created before the call to the getUpdates, so unwanted updates may be received for a short period of time.
         * @return Returns an Array of Update objects.
         */
        std::vector<Update::ptr> getUpdates(
          const std::optional<std::int32_t> &offset, const std::optional<std::int32_t> &limit,
          const std::optional<std::int32_t> &timeout,
          const std::optional<std::vector<std::string>> &allowed_updates) const;

        /**
         * @brief Use this method to get a list of profile pictures for a user.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] user_id Unique identifier of the target user.
         * @param[in] offset Sequential number of the first photo to be returned. By default, all photos are returned.
         * @param[in] limit Limits the number of photos to be retrieved. Values between 1-100 are accepted. Defaults to 100.
         * @return Returns a UserProfilePhotos object.
         */
        UserProfilePhotos::ptr getUserProfilePhotos(
          const std::int64_t &user_id, const std::optional<std::int32_t> &offset,
          const std::optional<std::int32_t> &limit) const;

        /**
         * @brief Use this method to get current webhook status.
         * @details Requires no parameters.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @return On success, returns a WebhookInfo object. If the bot is using getUpdates, will return an object with the url field empty.
         */
        WebhookInfo::ptr getWebhookInfo() const;

        /**
         * @brief Use this method to hide the 'General' topic in a forum supergroup chat.
         * @details The bot must be an administrator in the chat for this to work and must have the can_manage_topics administrator rights.
         * @details The topic will be automatically closed if it was open.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target supergroup (in the format \@supergroupusername).
         * @return Returns True on success.
         */
        bool hideGeneralForumTopic(const std::variant<std::int64_t, std::string> &chat_id) const;

        /**
         * @brief Use this method for your bot to leave a group, supergroup or channel.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target supergroup or channel (in the format \@channelusername).
         * @return Returns True on success.
         */
        bool leaveChat(const std::variant<std::int64_t, std::string> &chat_id) const;

        /**
         * @brief Use this method to log out from the cloud Bot API server before launching the bot locally.
         * @details You must log out the bot before running it locally, otherwise there is no guarantee that the bot will receive updates.
         * @details After a successful call, you can immediately log in on a local server, but will not be able to log in back to the cloud Bot API server for 10 minutes.
         * @details Requires no parameters.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @return Returns True on success.
         */
        bool logOut() const;

        /**
         * @brief Use this method to add a message to the list of pinned messages in a chat.
         * @details If the chat is not a private chat, the bot must be an administrator in the chat for this to work and must have the 'can_pin_messages' administrator right in a supergroup or 'can_edit_messages' administrator right in a channel.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_id Identifier of a message to pin.
         * @param[in] disable_notification Pass True if it is not necessary to send a notification to all chat members about the new pinned message. Notifications are always disabled in channels and private chats.
         * @return Returns True on success.
         */
        bool pinChatMessage(
          const std::variant<std::int64_t, std::string> &chat_id, const std::int32_t &message_id,
          const std::optional<bool> &disable_notification) const;

        /**
         * @brief Use this method to promote or demote a user in a supergroup or a channel.
         * @details The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights.
         * @details Pass False for all boolean parameters to demote a user.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] user_id Unique identifier of the target user.
         * @param[in] is_anonymous Pass True if the administrator's presence in the chat is hidden.
         * @param[in] can_manage_chat Pass True if the administrator can access the chat event log, chat statistics, message statistics in channels, see channel members, see anonymous administrators in supergroups and ignore slow mode. Implied by any other administrator privilege.
         * @param[in] can_post_messages Pass True if the administrator can create channel posts, channels only.
         * @param[in] can_edit_messages Pass True if the administrator can edit messages of other users and can pin messages, channels only.
         * @param[in] can_delete_messages Pass True if the administrator can delete messages of other users.
         * @param[in] can_manage_video_chats Pass True if the administrator can manage voice chats.
         * @param[in] can_restrict_members Pass True if the administrator can restrict, ban or unban chat members.
         * @param[in] can_promote_members Pass True if the administrator can add new administrators with a subset of their own privileges or demote administrators that they have promoted, directly or indirectly (promoted by administrators that were appointed by him).
         * @param[in] can_change_info Pass True if the administrator can change chat title, photo and other settings.
         * @param[in] can_invite_users Pass True if the administrator can invite new users to the chat.
         * @param[in] can_pin_messages Pass True if the administrator can pin messages, supergroups only.
         * @param[in] can_manage_topics Pass True if the user is allowed to create, rename, close, and reopen forum topics, supergroups only.
         * @return Returns True on success.
         */
        bool promoteChatMember(
          const std::variant<std::int64_t, std::string> &chat_id, const std::int64_t &user_id,
          const std::optional<bool> &is_anonymous, const std::optional<bool> &can_manage_chat,
          const std::optional<bool> &can_post_messages, const std::optional<bool> &can_edit_messages,
          const std::optional<bool> &can_delete_messages, const std::optional<bool> &can_manage_video_chats,
          const std::optional<bool> &can_restrict_members, const std::optional<bool> &can_promote_members,
          const std::optional<bool> &can_change_info, const std::optional<bool> &can_invite_users,
          const std::optional<bool> &can_pin_messages, const std::optional<bool> &can_manage_topics) const;

        /**
         * @brief Use this method to reopen a closed topic in a forum supergroup chat.
         * @details The bot must be an administrator in the chat for this to work and must have the can_manage_topics administrator rights, unless it is the creator of the topic.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target supergroup (in the format \@supergroupusername).
         * @param[in] message_thread_id Unique identifier for the target message thread of the forum topic.
         * @return Returns True on success.
         */
        bool reopenForumTopic(
          const std::variant<std::int64_t, std::string> &chat_id, const std::int32_t &message_thread_id) const;

        /**
         * @brief Use this method to reopen a closed 'General' topic in a forum supergroup chat.
         * @details The bot must be an administrator in the chat for this to work and must have the can_manage_topics administrator rights.
         * @details The topic will be automatically unhidden if it was hidden.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target supergroup (in the format \@supergroupusername).
         * @return Returns True on success.
         */
        bool reopenGeneralForumTopic(const std::variant<std::int64_t, std::string> &chat_id) const;

        /**
         * @brief Use this method to restrict a user in a supergroup.
         * @details The bot must be an administrator in the supergroup for this to work and must have the appropriate administrator rights.
         * @details Pass True for all permissions to lift restrictions from a user.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target supergroup (in the format \@supergroupusername).
         * @param[in] user_id Unique identifier of the target user.
         * @param[in] permissions A JSON-serialized object for new user permissions.
         * @param[in] use_independent_chat_permissions Pass True if chat permissions are set independently. Otherwise, the can_send_other_messages and can_add_web_page_previews permissions will imply the can_send_messages, can_send_audios, can_send_documents, can_send_photos, can_send_videos, can_send_video_notes, and can_send_voice_notes permissions; the can_send_polls permission will imply the can_send_messages permission.
         * @param[in] until_date Date when restrictions will be lifted for the user, unix time. If user is restricted for more than 366 days or less than 30 seconds from the current time, they are considered to be restricted forever.
         * @return Returns True on success.
         */
        bool restrictChatMember(
          const std::variant<std::int64_t, std::string> &chat_id, const std::int64_t &user_id,
          const ChatPermissions::ptr &permissions, const std::optional<bool> &use_independent_chat_permissions,
          const std::optional<std::uint64_t> &until_date) const;

        /**
         * @brief Use this method to revoke an invite link created by the bot.
         * @details If the primary link is revoked, a new link is automatically generated.
         * @details The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier of the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] invite_link The invite link to revoke.
         * @return Returns the revoked invite link as ChatInviteLink object.
         */
        ChatInviteLink::ptr revokeChatInviteLink(
          const std::variant<std::int64_t, std::string> &chat_id, const std::string &invite_link) const;

        /**
         * @brief Use this method to send animation files (GIF or H.264/MPEG-4 AVC video without sound).
         * @details Bots can currently send animation files of up to 50 MB in size, this limit may be changed in the future.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_thread_id Unique identifier for the target message thread (topic) of the forum; for forum supergroups only.
         * @param[in] animation Animation to send. Pass a file_id as String to send an animation that exists on the Telegram servers (recommended), pass an HTTP URL as a String for Telegram to get an animation from the Internet, or upload a new animation using multipart/form-data. More information on Sending Files ».
         * @param[in] duration Duration of sent animation in seconds.
         * @param[in] width Animation width.
         * @param[in] height Animation height.
         * @param[in] thumbnail Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail's width and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can't be reused and can be only uploaded as a new file, so you can pass “attach://\<file_attach_name>” if the thumbnail was uploaded using multipart/form-data under \<file_attach_name>. More information on Sending Files ».
         * @param[in] caption Animation caption (may also be used when resending animation by file_id), 0-1024 characters after entities parsing.
         * @param[in] parse_mode Mode for parsing entities in the animation caption. See formatting options for more details.
         * @param[in] caption_entities A JSON-serialized list of special entities that appear in the caption, which can be specified instead of parse_mode.
         * @param[in] has_spoiler Pass True if the animation needs to be covered with a spoiler animation.
         * @param[in] disable_notification Sends the message silently. Users will receive a notification with no sound.
         * @param[in] protect_content Protects the contents of the sent message from forwarding and saving.
         * @param[in] reply_to_message_id If the message is a reply, ID of the original message.
         * @param[in] allow_sending_without_reply Pass True if the message should be sent even if the specified replied-to message is not found.
         * @param[in] reply_markup Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
         * @return On success, the sent Message is returned.
         */
        Message::ptr sendAnimation(
          const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
          const std::string &animation, const std::optional<std::int32_t> &duration,
          const std::optional<std::int32_t> &width, const std::optional<std::int32_t> &height,
          const std::optional<std::string> &thumbnail, const std::optional<std::string> &caption,
          const std::optional<std::string> &parse_mode,
          const std::optional<std::vector<MessageEntity::ptr>> &caption_entities,
          const std::optional<bool> &has_spoiler, const std::optional<bool> &disable_notification,
          const std::optional<bool> &protect_content, const std::optional<std::int32_t> &reply_to_message_id,
          const std::optional<bool> &allow_sending_without_reply, const std::optional<Reply::ptr> &reply_markup) const;

        /**
         * @brief Use this method to send audio files, if you want Telegram clients to display them in the music player.
         * @details Your audio must be in the .MP3 or .M4A format.
         * @details Bots can currently send audio files of up to 50 MB in size, this limit may be changed in the future.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_thread_id Unique identifier for the target message thread (topic) of the forum; for forum supergroups only.
         * @param[in] audio Audio file to send. Pass a file_id as String to send an audio file that exists on the Telegram servers (recommended), pass an HTTP URL as a String for Telegram to get an audio file from the Internet, or upload a new one using multipart/form-data. More information on Sending Files ».
         * @param[in] caption Audio caption, 0-1024 characters after entities parsing.
         * @param[in] parse_mode Mode for parsing entities in the audio caption. See formatting options for more details.
         * @param[in] caption_entities A JSON-serialized list of special entities that appear in the caption, which can be specified instead of parse_mode.
         * @param[in] duration Duration of the audio in seconds.
         * @param[in] performer Performer.
         * @param[in] title Track name.
         * @param[in] thumbnail Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail's width and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can't be reused and can be only uploaded as a new file, so you can pass “attach://\<file_attach_name>” if the thumbnail was uploaded using multipart/form-data under \<file_attach_name>. More information on Sending Files ».
         * @param[in] disable_notification Sends the message silently. Users will receive a notification with no sound.
         * @param[in] protect_content Protects the contents of the sent message from forwarding and saving.
         * @param[in] reply_to_message_id If the message is a reply, ID of the original message.
         * @param[in] allow_sending_without_reply Pass True if the message should be sent even if the specified replied-to message is not found.
         * @param[in] reply_markup Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
         * @return On success, the sent Message is returned.
         */
        Message::ptr sendAudio(
          const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
          const std::string &audio, const std::optional<std::string> &caption,
          const std::optional<std::string> &parse_mode,
          const std::optional<std::vector<MessageEntity::ptr>> &caption_entities,
          const std::optional<std::int32_t> &duration, const std::optional<std::string> &performer,
          const std::optional<std::string> &title, const std::optional<std::string> &thumbnail,
          const std::optional<bool> &disable_notification, const std::optional<bool> &protect_content,
          const std::optional<std::int32_t> &reply_to_message_id,
          const std::optional<bool> &allow_sending_without_reply, const std::optional<Reply::ptr> &reply_markup) const;

        /**
         * @brief Use this method when you need to tell the user that something is happening on the bot's side.
         * @details The status is set for 5 seconds or less (when a message arrives from your bot, Telegram clients clear its typing status).
         * @details Example: The ImageBot needs some time to process a request and upload the image. Instead of sending a text message along the lines of “Retrieving image, please wait…”, the bot may use sendChatAction with action = upload_photo. The user will see a “sending photo” status for the bot.
         * @note We only recommend using this method when a response from the bot will take a noticeable amount of time to arrive.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_thread_id Unique identifier for the target message thread; supergroups only.
         * @param[in] action Type of action to broadcast. Choose one, depending on what the user is about to receive: typing for text messages, upload_photo for photos, record_video or upload_video for videos, record_voice or upload_voice for voice notes, upload_document for general files, choose_sticker for stickers, find_location for location data, record_video_note or upload_video_note for video notes.
         * @return Returns True on success.
         */
        bool sendChatAction(
          const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
          const std::string &action) const;

        /**
         * @brief Use this method to send phone contacts.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_thread_id Unique identifier for the target message thread (topic) of the forum; for forum supergroups only.
         * @param[in] phone_number Contact's phone number.
         * @param[in] first_name Contact's first name.
         * @param[in] last_name Contact's last name.
         * @param[in] vcard Additional data about the contact in the form of a vCard, 0-2048 bytes.
         * @param[in] disable_notification Sends the message silently. Users will receive a notification with no sound.
         * @param[in] protect_content Protects the contents of the sent message from forwarding and saving.
         * @param[in] reply_to_message_id If the message is a reply, ID of the original message.
         * @param[in] allow_sending_without_reply Pass True if the message should be sent even if the specified replied-to message is not found.
         * @param[in] reply_markup Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
         * @return On success, the sent Message is returned.
         */
        Message::ptr sendContact(
          const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
          const std::string &phone_number, const std::string &first_name, const std::optional<std::string> &last_name,
          const std::optional<std::string> &vcard, const std::optional<bool> &disable_notification,
          const std::optional<bool> &protect_content, const std::optional<std::int32_t> &reply_to_message_id,
          const std::optional<bool> &allow_sending_without_reply, const std::optional<Reply::ptr> &reply_markup) const;

        /**
         * @brief Use this method to send an animated emoji that will display a random value.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_thread_id Unique identifier for the target message thread (topic) of the forum; for forum supergroups only.
         * @param[in] emoji Emoji on which the dice throw animation is based. Currently, must be one of “🎲”, “🎯”, “🏀”, “⚽”, “🎳”, or “🎰”. Dice can have values 1-6 for “🎲”, “🎯” and “🎳”, values 1-5 for “🏀” and “⚽”, and values 1-64 for “🎰”. Defaults to “🎲”.
         * @param[in] disable_notification Sends the message silently. Users will receive a notification with no sound.
         * @param[in] protect_content Protects the contents of the sent message from forwarding.
         * @param[in] reply_to_message_id If the message is a reply, ID of the original message.
         * @param[in] allow_sending_without_reply Pass True if the message should be sent even if the specified replied-to message is not found.
         * @param[in] reply_markup Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
         * @return On success, the sent Message is returned.
         */
        Message::ptr sendDice(
          const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
          const std::optional<std::string> &emoji, const std::optional<bool> &disable_notification,
          const std::optional<bool> &protect_content, const std::optional<std::int32_t> &reply_to_message_id,
          const std::optional<bool> &allow_sending_without_reply, const std::optional<Reply::ptr> &reply_markup) const;

        /**
         * @brief Use this method to send general files.
         * @details Bots can currently send files of any type of up to 50 MB in size, this limit may be changed in the future.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_thread_id Unique identifier for the target message thread (topic) of the forum; for forum supergroups only.
         * @param[in] document File to send. Pass a file_id as String to send a file that exists on the Telegram servers (recommended), pass an HTTP URL as a String for Telegram to get a file from the Internet, or upload a new one using multipart/form-data. More information on Sending Files ».
         * @param[in] thumbnail Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail's width and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can't be reused and can be only uploaded as a new file, so you can pass “attach://\<file_attach_name>” if the thumbnail was uploaded using multipart/form-data under \<file_attach_name>. More information on Sending Files ».
         * @param[in] caption Document caption (may also be used when resending documents by file_id), 0-1024 characters after entities parsing.
         * @param[in] parse_mode Mode for parsing entities in the document caption. See formatting options for more details.
         * @param[in] caption_entities A JSON-serialized list of special entities that appear in the caption, which can be specified instead of parse_mode.
         * @param[in] disable_content_type_detection Disables automatic server-side content type detection for files uploaded using multipart/form-data.
         * @param[in] disable_notification Sends the message silently. Users will receive a notification with no sound.
         * @param[in] protect_content Protects the contents of the sent message from forwarding and saving.
         * @param[in] reply_to_message_id If the message is a reply, ID of the original message.
         * @param[in] allow_sending_without_reply Pass True if the message should be sent even if the specified replied-to message is not found.
         * @param[in] reply_markup Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
         * @return On success, the sent Message is returned.
         */
        Message::ptr sendDocument(
          const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
          const std::string &document, const std::optional<std::string> &thumbnail,
          const std::optional<std::string> &caption, const std::optional<std::string> &parse_mode,
          const std::optional<std::vector<MessageEntity::ptr>> &caption_entities,
          const std::optional<bool> &disable_content_type_detection, const std::optional<bool> &disable_notification,
          const std::optional<bool> &protect_content, const std::optional<std::int32_t> &reply_to_message_id,
          const std::optional<bool> &allow_sending_without_reply, const std::optional<Reply::ptr> &reply_markup) const;

        /**
         * @brief Use this method to send a game.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat.
         * @param[in] message_thread_id Unique identifier for the target message thread (topic) of the forum; for forum supergroups only.
         * @param[in] game_short_name Short name of the game, serves as the unique identifier for the game. Set up your games via \@BotFather.
         * @param[in] disable_notification Sends the message silently. Users will receive a notification with no sound.
         * @param[in] protect_content Protects the contents of the sent message from forwarding and saving.
         * @param[in] reply_to_message_id If the message is a reply, ID of the original message.
         * @param[in] allow_sending_without_reply Pass True if the message should be sent even if the specified replied-to message is not found.
         * @param[in] reply_markup A JSON-serialized object for an inline keyboard. If empty, one 'Play game_title' button will be shown. If not empty, the first button must launch the game.
         * @return On success, the sent Message is returned.
         */
        Message::ptr sendGame(
          const std::int64_t &chat_id, const std::optional<std::int32_t> &message_thread_id,
          const std::string &game_short_name, const std::optional<bool> &disable_notification,
          const std::optional<bool> &protect_content, const std::optional<std::int32_t> &reply_to_message_id,
          const std::optional<bool> &allow_sending_without_reply,
          const std::optional<InlineKeyboardMarkup::ptr> &reply_markup) const;

        /**
         * @brief Use this method to send invoices.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_thread_id Unique identifier for the target message thread (topic) of the forum; for forum supergroups only.
         * @param[in] title Product name, 1-32 characters.
         * @param[in] description Product description, 1-255 characters.
         * @param[in] payload Bot-defined invoice payload, 1-128 bytes. This will not be displayed to the user, use for your internal processes.
         * @param[in] provider_token Payment provider token, obtained via \@BotFather.
         * @param[in] currency Three-letter ISO 4217 currency code, see more on currencies.
         * @param[in] prices Price breakdown, a JSON-serialized list of components (e.g. product price, tax, discount, delivery cost, delivery tax, bonus, etc.).
         * @param[in] max_tip_amount The maximum accepted amount for tips in the smallest units of the currency (integer, not float/double). For example, for a maximum tip of US$ 1.45 pass max_tip_amount = 145. See the exp parameter in currencies.json, it shows the number of digits past the decimal point for each currency (2 for the majority of currencies). Defaults to 0.
         * @param[in] suggested_tip_amounts A JSON-serialized array of suggested amounts of tips in the smallest units of the currency (integer, not float/double). At most 4 suggested tip amounts can be specified. The suggested tip amounts must be positive, passed in a strictly increased order and must not exceed max_tip_amount.
         * @param[in] start_parameter Unique deep-linking parameter. If left empty, forwarded copies of the sent message will have a Pay button, allowing multiple users to pay directly from the forwarded message, using the same invoice. If non-empty, forwarded copies of the sent message will have a URL button with a deep link to the bot (instead of a Pay button), with the value used as the start parameter.
         * @param[in] provider_data JSON-serialized data about the invoice, which will be shared with the payment provider. A detailed description of required fields should be provided by the payment provider.
         * @param[in] photo_url URL of the product photo for the invoice. Can be a photo of the goods or a marketing image for a service. People like it better when they see what they are paying for.
         * @param[in] photo_size Photo size in bytes.
         * @param[in] photo_width Photo width.
         * @param[in] photo_height Photo height.
         * @param[in] need_name Pass True if you require the user's full name to complete the order.
         * @param[in] need_phone_number Pass True if you require the user's phone number to complete the order.
         * @param[in] need_email Pass True if you require the user's email address to complete the order.
         * @param[in] need_shipping_address Pass True if you require the user's shipping address to complete the order.
         * @param[in] send_phone_number_to_provider Pass True if the user's phone number should be sent to provider.
         * @param[in] send_email_to_provider Pass True if the user's email address should be sent to provider.
         * @param[in] is_flexible Pass True if the final price depends on the shipping method.
         * @param[in] disable_notification Sends the message silently. Users will receive a notification with no sound.
         * @param[in] protect_content Protects the contents of the sent message from forwarding and saving.
         * @param[in] reply_to_message_id If the message is a reply, ID of the original message.
         * @param[in] allow_sending_without_reply Pass True if the message should be sent even if the specified replied-to message is not found.
         * @param[in] reply_markup A JSON-serialized object for an inline keyboard. If empty, one 'Pay total price' button will be shown. If not empty, the first button must be a Pay button.
         * @return On success, the sent Message is returned.
         */
        Message::ptr sendInvoice(
          const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
          const std::string &title, const std::string &description, const std::string &payload,
          const std::string &provider_token, const std::string &currency, const std::vector<LabeledPrice::ptr> &prices,
          const std::optional<std::int32_t> &max_tip_amount,
          const std::optional<std::vector<std::int32_t>> &suggested_tip_amounts,
          const std::optional<std::string> &start_parameter, const std::optional<std::string> &provider_data,
          const std::optional<std::string> &photo_url, const std::optional<std::int32_t> &photo_size,
          const std::optional<std::int32_t> &photo_width, const std::optional<std::int32_t> &photo_height,
          const std::optional<bool> &need_name, const std::optional<bool> &need_phone_number,
          const std::optional<bool> &need_email, const std::optional<bool> &need_shipping_address,
          const std::optional<bool> &send_phone_number_to_provider, const std::optional<bool> &send_email_to_provider,
          const std::optional<bool> &is_flexible, const std::optional<bool> &disable_notification,
          const std::optional<bool> &protect_content, const std::optional<std::int32_t> &reply_to_message_id,
          const std::optional<bool> &allow_sending_without_reply,
          const std::optional<InlineKeyboardMarkup::ptr> &reply_markup) const;

        /**
         * @brief Use this method to send point on the map.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_thread_id Unique identifier for the target message thread (topic) of the forum; for forum supergroups only.
         * @param[in] latitude Latitude of the location.
         * @param[in] longitude Longitude of the location.
         * @param[in] horizontal_accuracy The radius of uncertainty for the location, measured in meters; 0-1500.
         * @param[in] live_period Period in seconds for which the location will be updated (see Live Locations, should be between 60 and 86400.
         * @param[in] heading For live locations, a direction in which the user is moving, in degrees. Must be between 1 and 360 if specified.
         * @param[in] proximity_alert_radius For live locations, a maximum distance for proximity alerts about approaching another chat member, in meters. Must be between 1 and 100000 if specified.
         * @param[in] disable_notification Sends the message silently. Users will receive a notification with no sound.
         * @param[in] protect_content Protects the contents of the sent message from forwarding and saving.
         * @param[in] reply_to_message_id If the message is a reply, ID of the original message.
         * @param[in] allow_sending_without_reply Pass True if the message should be sent even if the specified replied-to message is not found.
         * @param[in] reply_markup Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
         * @return On success, the sent Message is returned.
         */
        Message::ptr sendLocation(
          const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
          const float &latitude, const float &longitude, const std::optional<float> &horizontal_accuracy,
          const std::optional<std::int32_t> &live_period, const std::optional<std::int32_t> &heading,
          const std::optional<std::int32_t> &proximity_alert_radius, const std::optional<bool> &disable_notification,
          const std::optional<bool> &protect_content, const std::optional<std::int32_t> &reply_to_message_id,
          const std::optional<bool> &allow_sending_without_reply, const std::optional<Reply::ptr> &reply_markup) const;

        /**
         * @brief Use this method to send a group of photos, videos, documents or audios as an album.
         * @details Documents and audio files can be only grouped in an album with messages of the same type.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_thread_id Unique identifier for the target message thread (topic) of the forum; for forum supergroups only.
         * @param[in] media A JSON-serialized array describing messages to be sent, must include 2-10 items.
         * @param[in] disable_notification Sends messages silently. Users will receive a notification with no sound.
         * @param[in] protect_content Protects the contents of the sent messages from forwarding and saving.
         * @param[in] reply_to_message_id If the messages are a reply, ID of the original message.
         * @param[in] allow_sending_without_reply Pass True if the message should be sent even if the specified replied-to message is not found.
         * @return On success, an array of Messages that were sent is returned.
         */
        std::vector<Message::ptr> sendMediaGroup(
          const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
          const std::vector<std::variant<
            InputMediaAudio::ptr, InputMediaDocument::ptr, InputMediaPhoto::ptr, InputMediaVideo::ptr>> &media,
          const std::optional<bool> &disable_notification, const std::optional<bool> &protect_content,
          const std::optional<std::int32_t> &reply_to_message_id,
          const std::optional<bool> &allow_sending_without_reply) const;

        /**
         * @brief Use this method to send text messages.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_thread_id Unique identifier for the target message thread (topic) of the forum; for forum supergroups only.
         * @param[in] text Text of the message to be sent, 1-4096 characters after entities parsing.
         * @param[in] parse_mode Mode for parsing entities in the message text. See formatting options for more details.
         * @param[in] entities A JSON-serialized list of special entities that appear in message text, which can be specified instead of parse_mode.
         * @param[in] disable_web_page_preview Disables link previews for links in this message.
         * @param[in] disable_notification Sends the message silently. Users will receive a notification with no sound.
         * @param[in] protect_content Protects the contents of the sent message from forwarding and saving.
         * @param[in] reply_to_message_id If the message is a reply, ID of the original message.
         * @param[in] allow_sending_without_reply Pass True if the message should be sent even if the specified replied-to message is not found.
         * @param[in] reply_markup Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
         * @return On success, the sent Message is returned.
         */
        Message::ptr sendMessage(
          const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
          const std::string &text, const std::optional<std::string> &parse_mode,
          const std::optional<std::vector<MessageEntity::ptr>> &entities,
          const std::optional<bool> &disable_web_page_preview, const std::optional<bool> &disable_notification,
          const std::optional<bool> &protect_content, const std::optional<std::int32_t> &reply_to_message_id,
          const std::optional<bool> &allow_sending_without_reply, const std::optional<Reply::ptr> &reply_markup) const;

        /**
         * @brief Use this method to send photos.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_thread_id Unique identifier for the target message thread (topic) of the forum; for forum supergroups only.
         * @param[in] photo Photo to send. Pass a file_id as String to send a photo that exists on the Telegram servers (recommended), pass an HTTP URL as a String for Telegram to get a photo from the Internet, or upload a new photo using multipart/form-data. The photo must be at most 10 MB in size. The photo's width and height must not exceed 10000 in total. Width and height ratio must be at most 20. More information on Sending Files ».
         * @param[in] caption Photo caption (may also be used when resending photos by file_id), 0-1024 characters after entities parsing.
         * @param[in] parse_mode Mode for parsing entities in the photo caption. See formatting options for more details.
         * @param[in] caption_entities A JSON-serialized list of special entities that appear in the caption, which can be specified instead of parse_mode.
         * @param[in] has_spoiler Pass True if the photo needs to be covered with a spoiler animation.
         * @param[in] disable_notification Sends the message silently. Users will receive a notification with no sound.
         * @param[in] protect_content Protects the contents of the sent message from forwarding and saving.
         * @param[in] reply_to_message_id If the message is a reply, ID of the original message.
         * @param[in] allow_sending_without_reply Pass True if the message should be sent even if the specified replied-to message is not found.
         * @param[in] reply_markup Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
         * @return On success, the sent Message is returned.
         */
        Message::ptr sendPhoto(
          const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
          const std::string &photo, const std::optional<std::string> &caption,
          const std::optional<std::string> &parse_mode,
          const std::optional<std::vector<MessageEntity::ptr>> &caption_entities,
          const std::optional<bool> &has_spoiler, const std::optional<bool> &disable_notification,
          const std::optional<bool> &protect_content, const std::optional<std::int32_t> &reply_to_message_id,
          const std::optional<bool> &allow_sending_without_reply, const std::optional<Reply::ptr> &reply_markup) const;

        /**
         * @brief Use this method to send a native poll.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_thread_id Unique identifier for the target message thread (topic) of the forum; for forum supergroups only.
         * @param[in] question Poll question, 1-300 characters.
         * @param[in] options A JSON-serialized list of answer options, 2-10 strings 1-100 characters each.
         * @param[in] is_anonymous True, if the poll needs to be anonymous, defaults to True.
         * @param[in] type Poll type, “quiz” or “regular”, defaults to “regular”.
         * @param[in] allows_multiple_answers True, if the poll allows multiple answers, ignored for polls in quiz mode, defaults to False.
         * @param[in] correct_option_id 0-based identifier of the correct answer option, required for polls in quiz mode.
         * @param[in] explanation Text that is shown when a user chooses an incorrect answer or taps on the lamp icon in a quiz-style poll, 0-200 characters with at most 2 line feeds after entities parsing.
         * @param[in] explanation_parse_mode Mode for parsing entities in the explanation. See formatting options for more details.
         * @param[in] explanation_entities A JSON-serialized list of special entities that appear in the poll explanation, which can be specified instead of parse_mode.
         * @param[in] open_period Amount of time in seconds the poll will be active after creation, 5-600. Can't be used together with close_date.
         * @param[in] close_date Point in time (Unix timestamp) when the poll will be automatically closed. Must be at least 5 and no more than 600 seconds in the future. Can't be used together with open_period.
         * @param[in] is_closed Pass True if the poll needs to be immediately closed. This can be useful for poll preview.
         * @param[in] disable_notification Sends the message silently. Users will receive a notification with no sound.
         * @param[in] protect_content Protects the contents of the sent message from forwarding and saving.
         * @param[in] reply_to_message_id If the message is a reply, ID of the original message.
         * @param[in] allow_sending_without_reply Pass True if the message should be sent even if the specified replied-to message is not found.
         * @param[in] reply_markup Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
         * @return On success, the sent Message is returned.
         */
        Message::ptr sendPoll(
          const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
          const std::string &question, const std::vector<std::string> &options, const std::optional<bool> &is_anonymous,
          const std::optional<std::string> &type, const std::optional<bool> &allows_multiple_answers,
          const std::optional<std::int32_t> &correct_option_id, const std::optional<std::string> &explanation,
          const std::optional<std::string> &explanation_parse_mode,
          const std::optional<std::vector<MessageEntity::ptr>> &explanation_entities,
          const std::optional<std::int32_t> &open_period, const std::optional<std::uint64_t> &close_date,
          const std::optional<bool> &is_closed, const std::optional<bool> &disable_notification,
          const std::optional<bool> &protect_content, const std::optional<std::int32_t> &reply_to_message_id,
          const std::optional<bool> &allow_sending_without_reply, const std::optional<Reply::ptr> &reply_markup) const;

        /**
         * @brief Use this method to send static .WEBP, animated .TGS, or video .WEBM stickers.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_thread_id Unique identifier for the target message thread (topic) of the forum; for forum supergroups only.
         * @param[in] sticker Sticker to send. Pass a file_id as String to send a file that exists on the Telegram servers (recommended), pass an HTTP URL as a String for Telegram to get a .WEBP sticker from the Internet, or upload a new .WEBP or .TGS sticker using multipart/form-data. More information on Sending Files ». Video stickers can only be sent by a file_id. Animated stickers can't be sent via an HTTP URL.
         * @param[in] emoji Emoji associated with the sticker; only for just uploaded stickers.
         * @param[in] disable_notification Sends the message silently. Users will receive a notification with no sound.
         * @param[in] protect_content Protects the contents of the sent message from forwarding and saving.
         * @param[in] reply_to_message_id If the message is a reply, ID of the original message.
         * @param[in] allow_sending_without_reply Pass True if the message should be sent even if the specified replied-to message is not found.
         * @param[in] reply_markup Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
         * @return On success, the sent Message is returned.
         */
        Message::ptr sendSticker(
          const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
          const std::string &sticker, const std::optional<std::string> &emoji,
          const std::optional<bool> &disable_notification, const std::optional<bool> &protect_content,
          const std::optional<std::int32_t> &reply_to_message_id,
          const std::optional<bool> &allow_sending_without_reply, const std::optional<Reply::ptr> &reply_markup) const;

        /**
         * @brief Use this method to send information about a venue.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_thread_id Unique identifier for the target message thread (topic) of the forum; for forum supergroups only.
         * @param[in] latitude Latitude of the venue.
         * @param[in] longitude Longitude of the venue.
         * @param[in] title Name of the venue.
         * @param[in] address Address of the venue.
         * @param[in] foursquare_id Foursquare identifier of the venue.
         * @param[in] foursquare_type Foursquare type of the venue, if known. (For example, “arts_entertainment/default”, “arts_entertainment/aquarium” or “food/icecream”.)
         * @param[in] google_place_id Google Places identifier of the venue.
         * @param[in] google_place_type Google Places type of the venue. (See supported types.)
         * @param[in] disable_notification Sends the message silently. Users will receive a notification with no sound.
         * @param[in] protect_content Protects the contents of the sent message from forwarding and saving.
         * @param[in] reply_to_message_id If the message is a reply, ID of the original message.
         * @param[in] allow_sending_without_reply Pass True, if the message should be sent even if the specified replied-to message is not found.
         * @param[in] reply_markup Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
         * @return On success, the sent Message is returned.
         */
        Message::ptr sendVenue(
          const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
          const float &latitude, const float &longitude, const std::string &title, const std::string &address,
          const std::optional<std::string> &foursquare_id, const std::optional<std::string> &foursquare_type,
          const std::optional<std::string> &google_place_id, const std::optional<std::string> &google_place_type,
          const std::optional<bool> &disable_notification, const std::optional<bool> &protect_content,
          const std::optional<std::int32_t> &reply_to_message_id,
          const std::optional<bool> &allow_sending_without_reply, const std::optional<Reply::ptr> &reply_markup) const;

        /**
         * @brief Use this method to send video files, Telegram clients support MPEG4 videos (other formats may be sent as Document).
         * @details Bots can currently send video files of up to 50 MB in size, this limit may be changed in the future.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_thread_id Unique identifier for the target message thread (topic) of the forum; for forum supergroups only.
         * @param[in] video Video to send. Pass a file_id as String to send a video that exists on the Telegram servers (recommended), pass an HTTP URL as a String for Telegram to get a video from the Internet, or upload a new video using multipart/form-data. More information on Sending Files ».
         * @param[in] duration Duration of sent video in seconds.
         * @param[in] width Video width.
         * @param[in] height Video height.
         * @param[in] thumbnail Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail's width and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can't be reused and can be only uploaded as a new file, so you can pass “attach://\<file_attach_name>” if the thumbnail was uploaded using multipart/form-data under \<file_attach_name>. More information on Sending Files ».
         * @param[in] caption Video caption (may also be used when resending videos by file_id), 0-1024 characters after entities parsing.
         * @param[in] parse_mode Mode for parsing entities in the video caption. See formatting options for more details.
         * @param[in] caption_entities A JSON-serialized list of special entities that appear in the caption, which can be specified instead of parse_mode.
         * @param[in] has_spoiler Pass True if the video needs to be covered with a spoiler animation.
         * @param[in] supports_streaming Pass True if the uploaded video is suitable for streaming.
         * @param[in] disable_notification Sends the message silently. Users will receive a notification with no sound.
         * @param[in] protect_content Protects the contents of the sent message from forwarding and saving.
         * @param[in] reply_to_message_id If the message is a reply, ID of the original message.
         * @param[in] allow_sending_without_reply Pass True if the message should be sent even if the specified replied-to message is not found.
         * @param[in] reply_markup Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
         * @return On success, the sent Message is returned.
         */
        Message::ptr sendVideo(
          const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
          const std::string &video, const std::optional<std::int32_t> &duration,
          const std::optional<std::int32_t> &width, const std::optional<std::int32_t> &height,
          const std::optional<std::string> &thumbnail, const std::optional<std::string> &caption,
          const std::optional<std::string> &parse_mode,
          const std::optional<std::vector<MessageEntity::ptr>> &caption_entities,
          const std::optional<bool> &has_spoiler, const std::optional<bool> &supports_streaming,
          const std::optional<bool> &disable_notification, const std::optional<bool> &protect_content,
          const std::optional<std::int32_t> &reply_to_message_id,
          const std::optional<bool> &allow_sending_without_reply, const std::optional<Reply::ptr> &reply_markup) const;

        /**
         * @brief As of v.4.0, Telegram clients support rounded square MPEG4 videos of up to 1 minute long. Use this method to send video messages.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_thread_id Unique identifier for the target message thread (topic) of the forum; for forum supergroups only.
         * @param[in] video_note Video note to send. Pass a file_id as String to send a video note that exists on the Telegram servers (recommended) or upload a new video using multipart/form-data. More information on Sending Files ». Sending video notes by a URL is currently unsupported.
         * @param[in] duration Duration of sent video in seconds.
         * @param[in] length Video width and height, i.e. diameter of the video message.
         * @param[in] thumbnail Thumbnail of the file sent; can be ignored if thumbnail generation for the file is supported server-side. The thumbnail should be in JPEG format and less than 200 kB in size. A thumbnail's width and height should not exceed 320. Ignored if the file is not uploaded using multipart/form-data. Thumbnails can't be reused and can be only uploaded as a new file, so you can pass “attach://\<file_attach_name>” if the thumbnail was uploaded using multipart/form-data under \<file_attach_name>. More information on Sending Files ».
         * @param[in] disable_notification Sends the message silently. Users will receive a notification with no sound.
         * @param[in] protect_content Protects the contents of the sent message from forwarding and saving.
         * @param[in] reply_to_message_id If the message is a reply, ID of the original message.
         * @param[in] allow_sending_without_reply Pass True if the message should be sent even if the specified replied-to message is not found.
         * @param[in] reply_markup Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
         * @return On success, the sent Message is returned.
         */
        Message::ptr sendVideoNote(
          const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
          const std::string &video_note, const std::optional<std::int32_t> &duration,
          const std::optional<std::int32_t> &length, const std::optional<std::string> &thumbnail,
          const std::optional<bool> &disable_notification, const std::optional<bool> &protect_content,
          const std::optional<std::int32_t> &reply_to_message_id,
          const std::optional<bool> &allow_sending_without_reply, const std::optional<Reply::ptr> &reply_markup) const;

        /**
         * @brief Use this method to send audio files, if you want Telegram clients to display the file as a playable voice message.
         * @details For this to work, your audio must be in an .OGG file encoded with OPUS (other formats may be sent as Audio or Document).
         * @details Bots can currently send voice messages of up to 50 MB in size, this limit may be changed in the future.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_thread_id message_thread_id Unique identifier for the target message thread (topic) of the forum; for forum supergroups only.
         * @param[in] voice Audio file to send. Pass a file_id as String to send a file that exists on the Telegram servers (recommended), pass an HTTP URL as a String for Telegram to get a file from the Internet, or upload a new one using multipart/form-data. More information on Sending Files ».
         * @param[in] caption Voice message caption, 0-1024 characters after entities parsing.
         * @param[in] parse_mode Mode for parsing entities in the voice message caption. See formatting options for more details.
         * @param[in] caption_entities A JSON-serialized list of special entities that appear in the caption, which can be specified instead of parse_mode.
         * @param[in] duration Duration of the voice message in seconds.
         * @param[in] disable_notification Sends the message silently. Users will receive a notification with no sound.
         * @param[in] protect_content Protects the contents of the sent message from forwarding and saving.
         * @param[in] reply_to_message_id If the message is a reply, ID of the original message.
         * @param[in] allow_sending_without_reply Pass True if the message should be sent even if the specified replied-to message is not found.
         * @param[in] reply_markup Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
         * @return On success, the sent Message is returned.
         */
        Message::ptr sendVoice(
          const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
          const std::string &voice, const std::optional<std::string> &caption,
          const std::optional<std::string> &parse_mode,
          const std::optional<std::vector<MessageEntity::ptr>> &caption_entities,
          const std::optional<std::int32_t> &duration, const std::optional<bool> &disable_notification,
          const std::optional<bool> &protect_content, const std::optional<std::int32_t> &reply_to_message_id,
          const std::optional<bool> &allow_sending_without_reply, const std::optional<Reply::ptr> &reply_markup) const;

        /**
         * @brief Use this method to set a custom title for an administrator in a supergroup promoted by the bot.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target supergroup (in the format \@supergroupusername.
         * @param[in] user_id Unique identifier of the target user.
         * @param[in] custom_title New custom title for the administrator; 0-16 characters, emoji are not allowed.
         * @return Returns True on success.
         */
        bool setChatAdministratorCustomTitle(
          const std::variant<std::int64_t, std::string> &chat_id, const std::int64_t &user_id,
          const std::string &custom_title) const;

        /**
         * @brief Use this method to change the description of a group, a supergroup or a channel.
         * @details The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] description New chat description, 0-255 characters.
         * @return Returns True on success.
         */
        bool setChatDescription(
          const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::string> &description) const;

        /**
         * @brief Use this method to change the bot's menu button in a private chat, or the default menu button.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target private chat. If not specified, default bot's menu button will be changed.
         * @param[in] menu_button A JSON-serialized object for the bot's new menu button. Defaults to MenuButtonDefault.
         * @return Returns True on success.
         */
        bool setChatMenuButton(
          const std::optional<std::int64_t> &chat_id, const std::optional<MenuButton::ptr> &menu_button) const;

        /**
         * @brief Use this method to set default chat permissions for all members.
         * @details The bot must be an administrator in the group or a supergroup for this to work and must have the can_restrict_members administrator rights.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target supergroup (in the format \@supergroupusername).
         * @param[in] permissions A JSON-serialized object for new default chat permissions.
         * @param[in] use_independent_chat_permissions Pass True if chat permissions are set independently. Otherwise, the can_send_other_messages and can_add_web_page_previews permissions will imply the can_send_messages, can_send_audios, can_send_documents, can_send_photos, can_send_videos, can_send_video_notes, and can_send_voice_notes permissions; the can_send_polls permission will imply the can_send_messages permission.
         * @return Returns True on success.
         */
        bool setChatPermissions(
          const std::variant<std::int64_t, std::string> &chat_id, const ChatPermissions::ptr &permissions,
          const std::optional<bool> &use_independent_chat_permissions) const;

        /**
         * @brief Use this method to set a new profile photo for the chat.
         * @details Photos can't be changed for private chats.
         * @details The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] photo New chat photo, uploaded using multipart/form-data.
         * @return Returns True on success.
         */
        bool setChatPhoto(const std::variant<std::int64_t, std::string> &chat_id, const std::string &photo) const;

        /**
         * @brief Use this method to set a new group sticker set for a supergroup.
         * @details The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights.
         * @details Use the field can_set_sticker_set optionally returned in getChat requests to check if the bot can use this method.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target supergroup (in the format \@supergroupusername).
         * @param[in] sticker_set_name Name of the sticker set to be set as the group sticker set.
         * @return Returns True on success.
         */
        bool setChatStickerSet(
          const std::variant<std::int64_t, std::string> &chat_id, const std::string &sticker_set_name) const;

        /**
         * @brief Use this method to change the title of a chat.
         * @details Titles can't be changed for private chats.
         * @details The bot must be an administrator in the chat for this to work and must have the appropriate administrator rights.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] title New chat title, 1-128 characters.
         * @return Returns True on success.
         */
        bool setChatTitle(const std::variant<std::int64_t, std::string> &chat_id, const std::string &title) const;

        /**
         * @brief Use this method to set the thumbnail of a custom emoji sticker set.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] name Sticker set name.
         * @param[in] custom_emoji_id Custom emoji identifier of a sticker from the sticker set; pass an empty string to drop the thumbnail and use the first sticker as the thumbnail.
         * @return Returns True on success.
         */
        bool setCustomEmojiStickerSetThumbnail(
          const std::string &name, const std::optional<std::string> &custom_emoji_id) const;

        /**
         * @brief Use this method to set the score of the specified user in a game message.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] user_id User identifier.
         * @param[in] score New score, must be non-negative.
         * @param[in] force Pass True if the high score is allowed to decrease. This can be useful when fixing mistakes or banning cheaters.
         * @param[in] disable_edit_message Pass True if the game message should not be automatically edited to include the current scoreboard.
         * @param[in] chat_id Required if inline_message_id is not specified. Unique identifier for the target chat.
         * @param[in] message_id Required if inline_message_id is not specified. Identifier of the sent message.
         * @param[in] inline_message_id Required if chat_id and message_id are not specified. Identifier of the inline message.
         * @return On success, if the message is not an inline message, the Message is returned, otherwise True is returned. Returns an error, if the new score is not greater than the user's current score in the chat and force is False.
         */
        std::variant<bool, Message::ptr> setGameScore(
          const std::int64_t &user_id, const std::int32_t &score, const std::optional<bool> &force,
          const std::optional<bool> &disable_edit_message, const std::optional<std::int64_t> &chat_id,
          const std::optional<std::int32_t> &message_id, const std::optional<std::string> &inline_message_id) const;

        /**
         * @brief Use this method to change the list of the bot's commands.
         * @details See this manual for more details about bot commands.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] commands A JSON-serialized list of bot commands to be set as the list of the bot's commands. At most 100 commands can be specified.
         * @param[in] scope A JSON-serialized object, describing scope of users for which the commands are relevant. Defaults to BotCommandScopeDefault.
         * @param[in] language_code A two-letter ISO 639-1 language code. If empty, commands will be applied to all users from the given scope, for whose language there are no dedicated commands.
         * @return Returns True on success.
         */
        bool setMyCommands(
          const std::vector<BotCommand::ptr> &commands, const std::optional<BotCommandScope::ptr> &scope,
          const std::optional<std::string> &language_code) const;

        /**
         * @brief Use this method to change the default administrator rights requested by the bot when it's added as an administrator to groups or channels.
         * @details These rights will be suggested to users, but they are free to modify the list before adding the bot.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] rights A JSON-serialized object describing new default administrator rights. If not specified, the default administrator rights will be cleared.
         * @param[in] for_channels Pass True to change the default administrator rights of the bot in channels. Otherwise, the default administrator rights of the bot for groups and supergroups will be changed.
         * @return Returns True on success.
         */
        bool setMyDefaultAdministratorRights(
          const std::optional<ChatAdministratorRights::ptr> &rights, const std::optional<bool> &for_channels) const;

        /**
         * @brief Use this method to change the bot's description, which is shown in the chat with the bot if the chat is empty.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] description New bot description; 0-512 characters. Pass an empty string to remove the dedicated description for the given language.
         * @param[in] language_code A two-letter ISO 639-1 language code. If empty, the description will be applied to all users for whose language there is no dedicated description.
         * @return Returns True on success.
         */
        bool setMyDescription(
          const std::optional<std::string> &description, const std::optional<std::string> &language_code) const;

        /**
         * @brief Use this method to change the bot's short description, which is shown on the bot's profile page and is sent together with the link when users share the bot.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] short_description New short description for the bot; 0-120 characters. Pass an empty string to remove the dedicated short description for the given language.
         * @param[in] language_code A two-letter ISO 639-1 language code. If empty, the short description will be applied to all users for whose language there is no dedicated short description.
         * @return Returns True on success.
         */
        bool setMyShortDescription(
          const std::optional<std::string> &short_description, const std::optional<std::string> &language_code) const;

        /**
         * @brief Informs a user that some of the Telegram Passport elements they provided contains errors.
         * @details The user will not be able to re-submit their Passport to you until the errors are fixed (the contents of the field for which you returned the error must change).
         * @details Use this if the data submitted by the user doesn't satisfy the standards your service requires for any reason.
         * @details For example, if a birthday date seems invalid, a submitted document is blurry, a scan shows evidence of tampering, etc.
         * @details Supply some details in the error message to make sure the user knows how to correct the issues.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] user_id User identifier.
         * @param[in] errors A JSON-serialized array describing the errors.
         * @return Returns True on success.
         */
        bool setPassportDataErrors(
          const std::int64_t &user_id, const std::vector<PassportElementError::ptr> &errors) const;

        /**
         * @brief Use this method to change the list of emoji assigned to a regular or custom emoji sticker.
         * @details The sticker must belong to a sticker set created by the bot.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] sticker File identifier of the sticker.
         * @param[in] emoji_list A JSON-serialized list of 1-20 emoji associated with the sticker.
         * @return Returns True on success.
         */
        bool setStickerEmojiList(const std::string &sticker, const std::vector<std::string> &emoji_list) const;

        /**
         * @brief Use this method to change search keywords assigned to a regular or custom emoji sticker.
         * @details The sticker must belong to a sticker set created by the bot.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] sticker File identifier of the sticker.
         * @param[in] keywords A JSON-serialized list of 0-20 search keywords for the sticker with total length of up to 64 characters.
         * @return Returns True on success.
         */
        bool
          setStickerKeywords(const std::string &sticker, const std::optional<std::vector<std::string>> &keywords) const;

        /**
         * @brief Use this method to change the mask position of a mask sticker.
         * @details The sticker must belong to a sticker set that was created by the bot.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] sticker File identifier of the sticker.
         * @param[in] mask_position A JSON-serialized object with the position where the mask should be placed on faces. Omit the parameter to remove the mask position.
         * @return Returns True on success.
         */
        bool setStickerMaskPosition(
          const std::string &sticker, const std::optional<MaskPosition::ptr> &mask_position) const;

        /**
         * @brief Use this method to move a sticker in a set created by the bot to a specific position.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] sticker File identifier of the sticker.
         * @param[in] position New sticker position in the set, zero-based.
         * @return Returns True on success.
         */
        bool setStickerPositionInSet(const std::string &sticker, const std::int32_t &position) const;

        /**
         * @brief Use this method to set the thumbnail of a regular or mask sticker set.
         * @details The format of the thumbnail file must match the format of the stickers in the set.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] name Sticker set name.
         * @param[in] user_id User identifier of the sticker set owner.
         * @param[in] thumbnail A .WEBP or .PNG image with the thumbnail, must be up to 128 kilobytes in size and have a width and height of exactly 100px, or a .TGS animation with a thumbnail up to 32 kilobytes in size (see https://core.telegram.org/stickers#animated-sticker-requirements for animated sticker technical requirements), or a WEBM video with the thumbnail up to 32 kilobytes in size; see https://core.telegram.org/stickers#video-sticker-requirements for video sticker technical requirements. Pass a file_id as a String to send a file that already exists on the Telegram servers, pass an HTTP URL as a String for Telegram to get a file from the Internet, or upload a new one using multipart/form-data. More information on Sending Files ». Animated and video sticker set thumbnails can't be uploaded via HTTP URL. If omitted, then the thumbnail is dropped and the first sticker is used as the thumbnail.
         * @return Returns True on success.
         */
        bool setStickerSetThumbnail(
          const std::string &name, const std::int64_t &user_id, const std::optional<std::string> &thumbnail) const;

        /**
         * @brief Use this method to set the title of a created sticker set.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] name Sticker set name.
         * @param[in] title Sticker set title, 1-64 characters.
         * @return Returns True on success.
         */
        bool setStickerSetTitle(const std::string &name, const std::string &title) const;

        /**
         * @brief Use this method to specify a URL and receive incoming updates via an outgoing webhook.
         * @details Whenever there is an update for the bot, we will send an HTTPS POST request to the specified URL, containing a JSON-serialized Update.
         * @details In case of an unsuccessful request, we will give up after a reasonable amount of attempts.
         * @details If you'd like to make sure that the webhook was set by you, you can specify secret data in the parameter secret_token. If specified, the request will contain a header “X-Telegram-Bot-Api-Secret-Token” with the secret token as content.
         * @note You will not be able to receive updates using getUpdates for as long as an outgoing webhook is set up.
         * @note To use a self-signed certificate, you need to upload your public key certificate using certificate parameter. Please upload as InputFile, sending a String will not work.
         * @note Ports currently supported for webhooks: 443, 80, 88, 8443.
         * @note If you're having any trouble setting up webhooks, please check out this amazing guide to webhooks.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] url HTTPS URL to send updates to. Use an empty string to remove webhook integration.
         * @param[in] certificate Upload your public key certificate so that the root certificate in use can be checked. See our self-signed guide for details.
         * @param[in] ip_address The fixed IP address which will be used to send webhook requests instead of the IP address resolved through DNS.
         * @param[in] max_connections The maximum allowed number of simultaneous HTTPS connections to the webhook for update delivery, 1-100. Defaults to 40. Use lower values to limit the load on your bot's server, and higher values to increase your bot's throughput.
         * @param[in] allowed_updates A JSON-serialized list of the update types you want your bot to receive. For example, specify [“message”, “edited_channel_post”, “callback_query”] to only receive updates of these types. See Update for a complete list of available update types. Specify an empty list to receive all update types except chat_member (default). If not specified, the previous setting will be used. Please note that this parameter doesn't affect updates created before the call to the setWebhook, so unwanted updates may be received for a short period of time. Pass True to drop all pending updates.
         * @param[in] drop_pending_updates Pass True to drop all pending updates.
         * @param[in] secret_token A secret token to be sent in a header “X-Telegram-Bot-Api-Secret-Token” in every webhook request, 1-256 characters. Only characters A-Z, a-z, 0-9, _ and - are allowed. The header is useful to ensure that the request comes from a webhook set by you.
         * @return Returns True on success.
         */
        bool setWebhook(
          const std::string &url, const std::optional<std::string> &certificate,
          const std::optional<std::string> &ip_address, const std::optional<std::int32_t> &max_connections,
          const std::optional<std::vector<std::string>> &allowed_updates,
          const std::optional<bool> &drop_pending_updates, const std::optional<std::string> &secret_token) const;

        /**
         * @brief Use this method to stop updating a live location message before live_period expires.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Required if inline_message_id is not specified. Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_id Required if inline_message_id is not specified. Identifier of the message with live location to stop.
         * @param[in] inline_message_id Required if chat_id and message_id are not specified. Identifier of the inline message.
         * @param[in] reply_markup A JSON-serialized object for a new inline keyboard.
         * @return On success, if the message is not an inline message, the edited Message is returned, otherwise True is returned.
         */
        std::variant<bool, Message::ptr> stopMessageLiveLocation(
          const std::optional<std::variant<std::int64_t, std::string>> &chat_id,
          const std::optional<std::int32_t> &message_id, const std::optional<std::string> &inline_message_id,
          const std::optional<InlineKeyboardMarkup::ptr> &reply_markup) const;

        /**
         * @brief Use this method to stop a poll which was sent by the bot.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_id Identifier of the original message with the poll.
         * @param[in] reply_markup A JSON-serialized object for a new message inline keyboard.
         * @return On success, the stopped Poll is returned.
         */
        Poll::ptr stopPoll(
          const std::variant<std::int64_t, std::string> &chat_id, const std::int32_t &message_id,
          const std::optional<InlineKeyboardMarkup::ptr> &reply_markup) const;

        /**
         * @brief Use this method to unban a previously banned user in a supergroup or channel.
         * @details The user will not return to the group or channel automatically, but will be able to join via link, etc.
         * @details The bot must be an administrator for this to work.
         * @details By default, this method guarantees that after the call the user is not a member of the chat, but will be able to join it. So if the user is a member of the chat they will also be removed from the chat. If you don't want this, use the parameter only_if_banned.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target group or username of the target supergroup or channel (in the format \@channelusername).
         * @param[in] user_id Unique identifier of the target user.
         * @param[in] only_if_banned Do nothing if the user is not banned.
         * @return Returns True on success.
         */
        bool unbanChatMember(
          const std::variant<std::int64_t, std::string> &chat_id, const std::int64_t &user_id,
          const std::optional<bool> &only_if_banned) const;

        /**
         * @brief Use this method to unban a previously banned channel chat in a supergroup or channel.
         * @details The bot must be an administrator for this to work and must have the appropriate administrator rights.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] sender_chat_id Unique identifier of the target sender chat.
         * @return Returns True on success.
         */
        bool unbanChatSenderChat(
          const std::variant<std::int64_t, std::string> &chat_id, const std::int64_t &sender_chat_id) const;

        /**
         * @brief Use this method to unhide the 'General' topic in a forum supergroup chat.
         * @details The bot must be an administrator in the chat for this to work and must have the can_manage_topics administrator rights.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target supergroup (in the format \@supergroupusername).
         * @return Returns True on success.
         */
        bool unhideGeneralForumTopic(const std::variant<std::int64_t, std::string> &chat_id) const;

        /**
         * @brief Use this method to clear the list of pinned messages in a chat.
         * @details If the chat is not a private chat, the bot must be an administrator in the chat for this to work and must have the 'can_pin_messages' administrator right in a supergroup or 'can_edit_messages' administrator right in a channel.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @return Returns True on success.
         */
        bool unpinAllChatMessages(const std::variant<std::int64_t, std::string> &chat_id) const;

        /**
         * @brief Use this method to clear the list of pinned messages in a forum topic.
         * @details The bot must be an administrator in the chat for this to work and must have the can_pin_messages administrator right in the supergroup.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target supergroup (in the format \@supergroupusername).
         * @param[in] message_thread_id Unique identifier for the target message thread of the forum topic.
         * @return Returns True on success.
         */
        bool unpinAllForumTopicMessages(
          const std::variant<std::int64_t, std::string> &chat_id, const std::int32_t &message_thread_id) const;

        /**
         * @brief Use this method to remove a message from the list of pinned messages in a chat.
         * @details If the chat is not a private chat, the bot must be an administrator in the chat for this to work and must have the 'can_pin_messages' administrator right in a supergroup or 'can_edit_messages' administrator right in a channel.
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] chat_id Unique identifier for the target chat or username of the target channel (in the format \@channelusername).
         * @param[in] message_id Identifier of a message to unpin. If not specified, the most recent pinned message (by sending date) will be unpinned.
         * @return Returns True on success.
         */
        bool unpinChatMessage(
          const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_id) const;

        /**
         * @brief Use this method to upload a file with a sticker for later use in the createNewStickerSet and addStickerToSet methods (the file can be used multiple times).
         * @throw std::runtime_error The HTTP request failed or the response is unexpected.
         * @param[in] user_id User identifier of sticker file owner.
         * @param[in] sticker A file with the sticker in .WEBP, .PNG, .TGS, or .WEBM format. See https://core.telegram.org/stickers for technical requirements. More information on Sending Files ».
         * @param[in] sticker_format Format of the sticker, must be one of “static”, “animated”, “video”
         * @return Returns the uploaded File on success.
         */
        File::ptr uploadStickerFile(
          const std::int64_t &user_id, const std::string &sticker, const std::string &sticker_format) const;

        //Getter
        /**
         * @brief Getter.
         * @return m_certificate_verification
         */
        bool get_certificate_verification() const;

        /**
         * @brief Getter.
         * @return m_host.
         */
        std::string get_host() const;

        /**
         * @brief Getter.
         * @return m_port.
         */
        std::int32_t get_port() const;

        /**
         * @brief Getter.
         * @return m_test_value
         */
        std::optional<std::string> get_test_value() const;

        /**
         * @brief Getter.
         * @return m_token
         */
        std::string get_token() const;

        //Setter
        /**
         * @brief Setter for m_certificate_verification.
         * @param[in] certificate_verification See the member variable.
         */
        void set_certificate_verification(const bool &certificate_verification);

        /**
         * @brief Setter for m_host.
         * @param[in] host See the member variable.
         */
        void set_host(const std::string &host);

        /**
         * @brief Setter for m_port.
         * @param[in] port See the member variable.
         */
        void set_port(const std::int32_t &port);

        /**
         * @brief Setter for m_test_value.
         * @param[in] test_value See the member variable.
         */
        void set_test_value(const std::optional<std::string> &test_value);

        /**
         * @brief Setter for m_token.
         * @param[in] token See the member variable.
         */
        void set_token(const std::string &token);
    };
} //namespace tgbot

#endif
