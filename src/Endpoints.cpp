#define CPPHTTPLIB_OPENSSL_SUPPORT
#define CPPHTTPLIB_ZLIB_SUPPORT

#include "tgbot/Endpoints.h"

#include <algorithm>
#include <filesystem>
#include <httplib.h>
#include <initializer_list>
#include <istream>
#include <iterator>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <utility>

#include "tgbot/constants/message.h"
#include "tgbot/types/BotShortDescription.h"
#include "tgbot/types/ChatMemberAdministrator.h"
#include "tgbot/types/ChatMemberBanned.h"
#include "tgbot/types/ChatMemberLeft.h"
#include "tgbot/types/ChatMemberMember.h"
#include "tgbot/types/ChatMemberOwner.h"
#include "tgbot/types/ChatMemberRestricted.h"
#include "tgbot/types/User.h"

namespace tgbot
{
    Endpoints::Endpoints(
      std::string host, const std::int32_t &port, std::string token, std::optional<std::string> test_value,
      const bool &certificate_verification) :
        m_certificate_verification(certificate_verification),
        m_host(std::move(host)), m_port(port), m_test_value(std::move(test_value)), m_token(std::move(token))
    {}

    bool Endpoints::addStickerToSet(
      const std::int64_t &user_id, const std::string &name, const InputSticker::ptr &sticker) const
    {
        std::optional<std::string> sticker_to_upload;

        if(std::filesystem::is_regular_file(sticker->get_sticker()))
        {
            sticker_to_upload = sticker->get_sticker();
            sticker->set_sticker("attach://" + std::filesystem::path(sticker->get_sticker()).string());
        }

        const nlohmann::json sticker_json = nlohmann::json::parse(sticker->serialise());

        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(sticker_to_upload.has_value())
        {
            std::ifstream inf(std::filesystem::path(sticker_to_upload.value()).string(), std::ios::binary);
            multipart.emplace_back(httplib::MultipartFormData(
              {std::filesystem::path(sticker_to_upload.value()).string(),
               std::string(std::istreambuf_iterator<char>(inf), {}),
               std::filesystem::path(sticker_to_upload.value()).string(), ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"user_id", std::to_string(user_id), "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"name", name, "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"sticker", sticker_json.dump(), "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/addStickerToSet", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::answerCallbackQuery(
      const std::string &callback_query_id, const std::optional<std::string> &text,
      const std::optional<bool> &show_alert, const std::optional<std::string> &url,
      const std::optional<std::int32_t> &cache_time) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;
        multipart.emplace_back(httplib::MultipartFormData({"callback_query_id", callback_query_id, "", ""}));

        if(text.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"text", text.value(), "", ""}));
        }

        if(show_alert.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"show_alert", std::to_string(static_cast<std::int8_t>(show_alert.value())), "", ""}));
        }

        if(url.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"url", url.value(), "", ""}));
        }

        if(cache_time.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"cache_time", std::to_string(cache_time.value()), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/answerCallbackQuery", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::answerInlineQuery(
      const std::string &inline_query_id, const std::vector<InlineQueryResult::ptr> &results,
      const std::optional<std::int32_t> &cache_time, const std::optional<bool> &is_personal,
      const std::optional<std::string> &next_offset, const std::optional<std::string> &switch_pm_text,
      const std::optional<std::string> &switch_pm_parameter) const
    {
        nlohmann::json results_json = nlohmann::json::array();

        for(std::uint64_t i = 0; i < results.size(); ++i)
        {
            results_json.emplace_back(nlohmann::json::parse(results.at(i)->serialise()));
        }

        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;
        multipart.emplace_back(httplib::MultipartFormData({"inline_query_id", inline_query_id, "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"results", results_json.dump(), "", ""}));

        if(cache_time.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"cache_time", std::to_string(cache_time.value()), "", ""}));
        }

        if(is_personal.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"is_personal", std::to_string(static_cast<std::int8_t>(is_personal.value())), "", ""}));
        }

        if(next_offset.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"next_offset", next_offset.value(), "", ""}));
        }

        if(switch_pm_text.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"switch_pm_text", switch_pm_text.value(), "", ""}));
        }

        if(switch_pm_parameter.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"switch_pm_parameter", switch_pm_parameter.value(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/answerInlineQuery", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::answerPreCheckoutQuery(
      const std::string &pre_checkout_query_id, const bool &ok, const std::optional<std::string> &error_message) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;
        multipart.emplace_back(httplib::MultipartFormData({"pre_checkout_query_id", pre_checkout_query_id, "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"ok", std::to_string(static_cast<std::int8_t>(ok)), "", ""}));

        if(error_message.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"error_message", error_message.value(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/answerPreCheckoutQuery", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::answerShippingQuery(
      const std::string &shipping_query_id, const bool &ok,
      const std::optional<std::vector<ShippingOption::ptr>> &shipping_options,
      const std::optional<std::string> &error_message) const
    {
        nlohmann::json shipping_options_json = nlohmann::json::array();

        if(shipping_options.has_value())
        {
            for(std::uint64_t i = 0; i < shipping_options.value().size(); ++i)
            {
                shipping_options_json.emplace_back(nlohmann::json::parse(shipping_options.value().at(i)->serialise()));
            }
        }

        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;
        multipart.emplace_back(httplib::MultipartFormData({"shipping_query_id", shipping_query_id, "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"ok", std::to_string(static_cast<std::int8_t>(ok)), "", ""}));

        if(shipping_options.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"shipping_options", shipping_options_json.dump(), "", ""}));
        }

        if(error_message.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"error_message", error_message.value(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/answerShippingQuery", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    SentWebAppMessage::ptr
      Endpoints::answerWebAppQuery(const std::string &web_app_query_id, const InlineQueryResult::ptr &result) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;
        multipart.emplace_back(httplib::MultipartFormData({"web_app_query_id", web_app_query_id, "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"result", result->serialise(), "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result http_result = client.Post("/bot" + m_token + "/answerWebAppQuery", multipart);

        if(http_result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(http_result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(http_result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<SentWebAppMessage>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(http_result->status) + "\n" +
                      http_result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(http_result->status) + "\n" +
                  http_result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(http_result->status) + "\n" +
              http_result->body);
        }
    }

    bool Endpoints::approveChatJoinRequest(
      const std::variant<std::int64_t, std::string> &chat_id, const std::int64_t &user_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"user_id", std::to_string(user_id), "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/approveChatJoinRequest", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::banChatMember(
      const std::variant<std::int64_t, std::string> &chat_id, const std::int64_t &user_id,
      const std::optional<std::uint64_t> &until_date, const std::optional<bool> &revoke_messages) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"user_id", std::to_string(user_id), "", ""}));

        if(until_date.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"until_date", std::to_string(until_date.value()), "", ""}));
        }

        if(revoke_messages.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"revoke_messages", std::to_string(static_cast<std::int8_t>(revoke_messages.value())), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/banChatMember", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::banChatSenderChat(
      const std::variant<std::int64_t, std::string> &chat_id, const std::int64_t &sender_chat_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"sender_chat_id", std::to_string(sender_chat_id), "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/banChatSenderChat", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::close() const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        std::int32_t result_status = -1;
        std::string result_body;

        if(m_test_value.has_value())
        {
            const httplib::Result result = client.Post("/bot" + m_token + "/close", multipart);

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }
        else
        {
            const httplib::Result result = client.Get("/bot" + m_token + "/close");

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }

        const nlohmann::json doc = nlohmann::json::parse(result_body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result_status) + "\n" +
                      result_body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result_status) + "\n" +
                  result_body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result_status) + "\n" +
              result_body);
        }
    }

    bool Endpoints::closeForumTopic(
      const std::variant<std::int64_t, std::string> &chat_id, const std::int32_t &message_thread_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        multipart.emplace_back(
          httplib::MultipartFormData({"message_thread_id", std::to_string(message_thread_id), "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/closeForumTopic", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::closeGeneralForumTopic(const std::variant<std::int64_t, std::string> &chat_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/closeGeneralForumTopic", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    MessageId::ptr Endpoints::copyMessage(
      const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
      const std::variant<std::int64_t, std::string> &from_chat_id, const std::int32_t &message_id,
      const std::optional<std::string> &caption, const std::optional<std::string> &parse_mode,
      const std::optional<std::vector<MessageEntity::ptr>> &caption_entities,
      const std::optional<bool> &disable_notification, const std::optional<bool> &protect_content,
      const std::optional<std::int32_t> &reply_to_message_id, const std::optional<bool> &allow_sending_without_reply,
      const std::optional<Reply::ptr> &reply_markup) const
    {
        nlohmann::json caption_entities_json = nlohmann::json::array();

        if(caption_entities.has_value())
        {
            for(std::uint64_t i = 0; i < caption_entities.value().size(); ++i)
            {
                caption_entities_json.emplace_back(nlohmann::json::parse(caption_entities.value().at(i)->serialise()));
            }
        }

        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(message_thread_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_thread_id", std::to_string(message_thread_id.value()), "", ""}));
        }

        if(std::holds_alternative<std::int64_t>(from_chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"from_chat_id", std::to_string(std::get<std::int64_t>(from_chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(from_chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"from_chat_id", std::get<std::string>(from_chat_id), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"message_id", std::to_string(message_id), "", ""}));

        if(caption.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"caption", caption.value(), "", ""}));
        }

        if(parse_mode.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"parse_mode", parse_mode.value(), "", ""}));
        }

        if(caption_entities.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"caption_entities", caption_entities_json.dump(), "", ""}));
        }

        if(disable_notification.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"disable_notification", std::to_string(static_cast<std::int8_t>(disable_notification.value())), "", ""}));
        }

        if(protect_content.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"protect_content", std::to_string(static_cast<std::int8_t>(protect_content.value())), "", ""}));
        }

        if(reply_to_message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_to_message_id", std::to_string(reply_to_message_id.value()), "", ""}));
        }

        if(allow_sending_without_reply.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"allow_sending_without_reply",
               std::to_string(static_cast<std::int8_t>(allow_sending_without_reply.value())), "", ""}));
        }

        if(reply_markup.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_markup", reply_markup.value()->serialise(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/copyMessage", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<MessageId>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    ChatInviteLink::ptr Endpoints::createChatInviteLink(
      const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::string> &name,
      const std::optional<std::uint64_t> &expire_date, const std::optional<std::int32_t> &member_limit,
      const std::optional<bool> &creates_join_request) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(name.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"name", name.value(), "", ""}));
        }

        if(expire_date.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"expire_date", std::to_string(expire_date.value()), "", ""}));
        }

        if(member_limit.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"member_limit", std::to_string(member_limit.value()), "", ""}));
        }

        if(creates_join_request.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"creates_join_request", std::to_string(static_cast<std::int8_t>(creates_join_request.value())), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/createChatInviteLink", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<ChatInviteLink>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    ForumTopic::ptr Endpoints::createForumTopic(
      const std::variant<std::int64_t, std::string> &chat_id, const std::string &name,
      const std::optional<std::int32_t> &icon_color, const std::optional<std::string> &icon_custom_emoji_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"name", name, "", ""}));

        if(icon_color.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"icon_color", std::to_string(icon_color.value()), "", ""}));
        }

        if(icon_custom_emoji_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"icon_custom_emoji_id", icon_custom_emoji_id.value(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/createForumTopic", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<ForumTopic>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    std::string Endpoints::createInvoiceLink(
      const std::string &title, const std::string &description, const std::string &payload,
      const std::string &provider_token, const std::string &currency, const std::vector<LabeledPrice::ptr> &prices,
      const std::optional<std::int32_t> &max_tip_amount,
      const std::optional<std::vector<std::int32_t>> &suggested_tip_amounts,
      const std::optional<std::string> &provider_data, const std::optional<std::string> &photo_url,
      const std::optional<std::int32_t> &photo_size, const std::optional<std::int32_t> &photo_width,
      const std::optional<std::int32_t> &photo_height, const std::optional<bool> &need_name,
      const std::optional<bool> &need_phone_number, const std::optional<bool> &need_email,
      const std::optional<bool> &need_shipping_address, const std::optional<bool> &send_phone_number_to_provider,
      const std::optional<bool> &send_email_to_provider, const std::optional<bool> &is_flexible) const
    {
        nlohmann::json prices_json = nlohmann::json::array();

        for(std::uint64_t i = 0; i < prices.size(); ++i)
        {
            prices_json.emplace_back(nlohmann::json::parse(prices.at(i)->serialise()));
        }

        nlohmann::json suggested_tip_amounts_json = nlohmann::json::array();

        if(suggested_tip_amounts.has_value())
        {
            for(std::uint64_t i = 0; i < suggested_tip_amounts.value().size(); ++i)
            {
                suggested_tip_amounts_json.emplace_back(suggested_tip_amounts.value().at(i));
            }
        }

        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;
        multipart.emplace_back(httplib::MultipartFormData({"title", title, "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"description", description, "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"payload", payload, "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"provider_token", provider_token, "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"currency", currency, "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"prices", prices_json.dump(), "", ""}));

        if(max_tip_amount.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"max_tip_amount", std::to_string(max_tip_amount.value()), "", ""}));
        }

        if(suggested_tip_amounts.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"suggested_tip_amounts", suggested_tip_amounts_json.dump(), "", ""}));
        }

        if(provider_data.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"provider_data", provider_data.value(), "", ""}));
        }

        if(photo_url.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"photo_url", photo_url.value(), "", ""}));
        }

        if(photo_size.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"photo_size", std::to_string(photo_size.value()), "", ""}));
        }

        if(photo_width.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"photo_width", std::to_string(photo_width.value()), "", ""}));
        }

        if(photo_height.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"photo_height", std::to_string(photo_height.value()), "", ""}));
        }

        if(need_name.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"need_name", std::to_string(static_cast<std::int8_t>(need_name.value())), "", ""}));
        }

        if(need_phone_number.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"need_phone_number", std::to_string(static_cast<std::int8_t>(need_phone_number.value())), "", ""}));
        }

        if(need_email.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"need_email", std::to_string(static_cast<std::int8_t>(need_email.value())), "", ""}));
        }

        if(need_shipping_address.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"need_shipping_address", std::to_string(static_cast<std::int8_t>(need_shipping_address.value())), "",
               ""}));
        }

        if(send_phone_number_to_provider.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"send_phone_number_to_provider",
               std::to_string(static_cast<std::int8_t>(send_phone_number_to_provider.value())), "", ""}));
        }

        if(send_email_to_provider.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"send_email_to_provider", std::to_string(static_cast<std::int8_t>(send_email_to_provider.value())), "",
               ""}));
        }

        if(is_flexible.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"is_flexible", std::to_string(static_cast<std::int8_t>(is_flexible.value())), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/createInvoiceLink", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_string())
                {
                    return doc["result"].get<std::string>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_string("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::createNewStickerSet(
      const std::int64_t &user_id, const std::string &name, const std::string &title,
      const std::vector<InputSticker::ptr> &stickers, const std::string &sticker_format,
      const std::optional<std::string> &sticker_type, const std::optional<bool> &needs_repainting) const
    {
        std::vector<std::string> stickers_to_upload;
        stickers_to_upload.reserve(stickers.size());
        nlohmann::json stickers_json = nlohmann::json::array();

        for(std::uint64_t i = 0; i < stickers.size(); ++i)
        {
            if(std::filesystem::is_regular_file(stickers.at(i)->get_sticker()))
            {
                stickers_to_upload.emplace_back(stickers.at(i)->get_sticker());
                stickers.at(i)->set_sticker("attach://" + std::filesystem::path(stickers.at(i)->get_sticker()).string());
            }

            stickers_json.emplace_back(nlohmann::json::parse(stickers.at(i)->serialise()));
        }

        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        for(std::uint64_t i = 0; i < stickers_to_upload.size(); ++i)
        {
            std::ifstream inf(std::filesystem::path(stickers_to_upload.at(i)).string(), std::ios::binary);
            multipart.emplace_back(httplib::MultipartFormData(
              {std::filesystem::path(stickers_to_upload.at(i)).string(),
               std::string(std::istreambuf_iterator<char>(inf), {}),
               std::filesystem::path(stickers_to_upload.at(i)).string(), ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"user_id", std::to_string(user_id), "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"name", name, "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"title", title, "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"stickers", stickers_json.dump(), "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"sticker_format", sticker_format, "", ""}));

        if(sticker_type.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"sticker_type", sticker_type.value(), "", ""}));
        }

        if(needs_repainting.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"needs_repainting", std::to_string(static_cast<std::int8_t>(needs_repainting.value())), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/createNewStickerSet", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::declineChatJoinRequest(
      const std::variant<std::int64_t, std::string> &chat_id, const std::int64_t &user_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"user_id", std::to_string(user_id), "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/declineChatJoinRequest", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::deleteChatPhoto(const std::variant<std::int64_t, std::string> &chat_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/deleteChatPhoto", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::deleteChatStickerSet(const std::variant<std::int64_t, std::string> &chat_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/deleteChatStickerSet", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::deleteForumTopic(
      const std::variant<std::int64_t, std::string> &chat_id, const std::int32_t &message_thread_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        multipart.emplace_back(
          httplib::MultipartFormData({"message_thread_id", std::to_string(message_thread_id), "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/deleteForumTopic", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::deleteMessage(
      const std::variant<std::int64_t, std::string> &chat_id, const std::int32_t &message_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"message_id", std::to_string(message_id), "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/deleteMessage", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::deleteMyCommands(
      const std::optional<BotCommandScope::ptr> &scope, const std::optional<std::string> &language_code) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(scope.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"scope", scope.value()->serialise(), "", ""}));
        }

        if(language_code.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"language_code", language_code.value(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/deleteMyCommands", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::deleteStickerFromSet(const std::string &sticker) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;
        multipart.emplace_back(httplib::MultipartFormData({"sticker", sticker, "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/deleteStickerFromSet", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::deleteStickerSet(const std::string &name) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;
        multipart.emplace_back(httplib::MultipartFormData({"name", name, "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/deleteStickerSet", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::deleteWebhook(const std::optional<bool> &drop_pending_updates) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(drop_pending_updates.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"drop_pending_updates", std::to_string(static_cast<std::int8_t>(drop_pending_updates.value())), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        std::int32_t result_status = -1;
        std::string result_body;

        if(drop_pending_updates.has_value() || m_test_value.has_value())
        {
            const httplib::Result result = client.Post("/bot" + m_token + "/deleteWebhook", multipart);

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }
        else
        {
            const httplib::Result result = client.Get("/bot" + m_token + "/deleteWebhook");

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }

        const nlohmann::json doc = nlohmann::json::parse(result_body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result_status) + "\n" +
                      result_body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result_status) + "\n" +
                  result_body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result_status) + "\n" +
              result_body);
        }
    }

    ChatInviteLink::ptr Endpoints::editChatInviteLink(
      const std::variant<std::int64_t, std::string> &chat_id, const std::string &invite_link,
      const std::optional<std::string> &name, const std::optional<std::uint64_t> &expire_date,
      const std::optional<std::int32_t> &member_limit, const std::optional<bool> &creates_join_request) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"invite_link", invite_link, "", ""}));

        if(name.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"name", name.value(), "", ""}));
        }

        if(expire_date.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"expire_date", std::to_string(expire_date.value()), "", ""}));
        }

        if(member_limit.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"member_limit", std::to_string(member_limit.value()), "", ""}));
        }

        if(creates_join_request.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"creates_join_request", std::to_string(static_cast<std::int8_t>(creates_join_request.value())), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/editChatInviteLink", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<ChatInviteLink>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::editForumTopic(
      const std::variant<std::int64_t, std::string> &chat_id, const std::int32_t &message_thread_id,
      const std::optional<std::string> &name, const std::optional<std::string> &icon_custom_emoji_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        multipart.emplace_back(
          httplib::MultipartFormData({"message_thread_id", std::to_string(message_thread_id), "", ""}));

        if(name.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"name", name.value(), "", ""}));
        }

        if(icon_custom_emoji_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"icon_custom_emoji_id", icon_custom_emoji_id.value(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/editForumTopic", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::editGeneralForumTopic(
      const std::variant<std::int64_t, std::string> &chat_id, const std::string &name) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"name", name, "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/editGeneralForumTopic", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    std::variant<bool, Message::ptr> Endpoints::editMessageCaption(
      const std::optional<std::variant<std::int64_t, std::string>> &chat_id,
      const std::optional<std::int32_t> &message_id, const std::optional<std::string> &inline_message_id,
      const std::optional<std::string> &caption, const std::optional<std::string> &parse_mode,
      const std::optional<std::vector<MessageEntity::ptr>> &caption_entities,
      const std::optional<InlineKeyboardMarkup::ptr> &reply_markup) const
    {
        nlohmann::json caption_entities_json = nlohmann::json::array();

        if(caption_entities.has_value())
        {
            for(std::uint64_t i = 0; i < caption_entities.value().size(); ++i)
            {
                caption_entities_json.emplace_back(nlohmann::json::parse(caption_entities.value().at(i)->serialise()));
            }
        }

        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(chat_id.has_value())
        {
            if(std::holds_alternative<std::int64_t>(chat_id.value()))
            {
                multipart.emplace_back(httplib::MultipartFormData(
                  {"chat_id", std::to_string(std::get<std::int64_t>(chat_id.value())), "", ""}));
            }
            else if(std::holds_alternative<std::string>(chat_id.value()))
            {
                multipart.emplace_back(
                  httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id.value()), "", ""}));
            }
        }

        if(message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_id", std::to_string(message_id.value()), "", ""}));
        }

        if(inline_message_id.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"inline_message_id", inline_message_id.value(), "", ""}));
        }

        if(caption.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"caption", caption.value(), "", ""}));
        }

        if(parse_mode.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"parse_mode", parse_mode.value(), "", ""}));
        }

        if(caption_entities.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"caption_entities", caption_entities_json.dump(), "", ""}));
        }

        if(reply_markup.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_markup", reply_markup.value()->serialise(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/editMessageCaption", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else if(doc["result"].is_object())
                {
                    return std::make_shared<Message>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + ", " + message::json::value::not_object("result") +
                      "\n" + std::to_string(result->status) + "\n" + result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    std::variant<bool, Message::ptr> Endpoints::editMessageLiveLocation(
      const std::optional<std::variant<std::int64_t, std::string>> &chat_id,
      const std::optional<std::int32_t> &message_id, const std::optional<std::string> &inline_message_id,
      const float &latitude, const float &longitude, const std::optional<float> &horizontal_accuracy,
      const std::optional<std::int32_t> &heading, const std::optional<std::int32_t> &proximity_alert_radius,
      const std::optional<InlineKeyboardMarkup::ptr> &reply_markup) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(chat_id.has_value())
        {
            if(std::holds_alternative<std::int64_t>(chat_id.value()))
            {
                multipart.emplace_back(httplib::MultipartFormData(
                  {"chat_id", std::to_string(std::get<std::int64_t>(chat_id.value())), "", ""}));
            }
            else if(std::holds_alternative<std::string>(chat_id.value()))
            {
                multipart.emplace_back(
                  httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id.value()), "", ""}));
            }
        }

        if(message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_id", std::to_string(message_id.value()), "", ""}));
        }

        if(inline_message_id.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"inline_message_id", inline_message_id.value(), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"latitude", std::to_string(latitude), "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"longitude", std::to_string(longitude), "", ""}));

        if(horizontal_accuracy.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"horizontal_accuracy", std::to_string(horizontal_accuracy.value()), "", ""}));
        }

        if(heading.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"heading", std::to_string(heading.value()), "", ""}));
        }

        if(proximity_alert_radius.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"proximity_alert_radius", std::to_string(proximity_alert_radius.value()), "", ""}));
        }

        if(reply_markup.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_markup", reply_markup.value()->serialise(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/editMessageLiveLocation", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else if(doc["result"].is_object())
                {
                    return std::make_shared<Message>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + ", " + message::json::value::not_object("result") +
                      "\n" + std::to_string(result->status) + "\n" + result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    std::variant<bool, Message::ptr> Endpoints::editMessageMedia(
      const std::optional<std::variant<std::int64_t, std::string>> &chat_id,
      const std::optional<std::int32_t> &message_id, const std::optional<std::string> &inline_message_id,
      const std::variant<InputMediaAnimation::ptr, InputMediaAudio::ptr, InputMediaDocument::ptr, InputMediaPhoto::ptr,
                         InputMediaVideo::ptr> &media,
      const std::optional<InlineKeyboardMarkup::ptr> &reply_markup) const
    {
        std::optional<std::string> media_to_upload;
        std::optional<std::string> thumbnail_to_upload;
        nlohmann::json media_json = nlohmann::json::object();

        if(std::holds_alternative<InputMediaAnimation::ptr>(media))
        {
            const InputMediaAnimation::ptr &input_media_animation = std::get<InputMediaAnimation::ptr>(media);

            if(std::filesystem::is_regular_file(input_media_animation->get_media()))
            {
                media_to_upload = input_media_animation->get_media();
                input_media_animation->set_media(
                  "attach://" + std::filesystem::path(input_media_animation->get_media()).string());
            }

            if(input_media_animation->get_thumbnail().has_value() &&
               std::filesystem::is_regular_file(input_media_animation->get_thumbnail().value()))
            {
                thumbnail_to_upload = input_media_animation->get_thumbnail().value();
                input_media_animation->set_thumbnail(
                  "attach://" + std::filesystem::path(input_media_animation->get_thumbnail().value()).string());
            }

            media_json = nlohmann::json::parse(input_media_animation->serialise());
        }
        else if(std::holds_alternative<InputMediaAudio::ptr>(media))
        {
            const InputMediaAudio::ptr &input_media_audio = std::get<InputMediaAudio::ptr>(media);

            if(std::filesystem::is_regular_file(input_media_audio->get_media()))
            {
                media_to_upload = input_media_audio->get_media();
                input_media_audio->set_media(
                  "attach://" + std::filesystem::path(input_media_audio->get_media()).string());
            }

            if(input_media_audio->get_thumbnail().has_value() &&
               std::filesystem::is_regular_file(input_media_audio->get_thumbnail().value()))
            {
                thumbnail_to_upload = input_media_audio->get_thumbnail().value();
                input_media_audio->set_thumbnail(
                  "attach://" + std::filesystem::path(input_media_audio->get_thumbnail().value()).string());
            }

            media_json = nlohmann::json::parse(input_media_audio->serialise());
        }
        else if(std::holds_alternative<InputMediaDocument::ptr>(media))
        {
            const InputMediaDocument::ptr &input_media_document = std::get<InputMediaDocument::ptr>(media);

            if(std::filesystem::is_regular_file(input_media_document->get_media()))
            {
                media_to_upload = input_media_document->get_media();
                input_media_document->set_media(
                  "attach://" + std::filesystem::path(input_media_document->get_media()).string());
            }

            if(input_media_document->get_thumbnail().has_value() &&
               std::filesystem::is_regular_file(input_media_document->get_thumbnail().value()))
            {
                thumbnail_to_upload = input_media_document->get_thumbnail().value();
                input_media_document->set_thumbnail(
                  "attach://" + std::filesystem::path(input_media_document->get_thumbnail().value()).string());
            }

            media_json = nlohmann::json::parse(input_media_document->serialise());
        }
        else if(std::holds_alternative<InputMediaPhoto::ptr>(media))
        {
            const InputMediaPhoto::ptr &input_media_photo = std::get<InputMediaPhoto::ptr>(media);

            if(std::filesystem::is_regular_file(input_media_photo->get_media()))
            {
                media_to_upload = input_media_photo->get_media();
                input_media_photo->set_media(
                  "attach://" + std::filesystem::path(input_media_photo->get_media()).string());
            }

            media_json = nlohmann::json::parse(input_media_photo->serialise());
        }
        else if(std::holds_alternative<InputMediaVideo::ptr>(media))
        {
            const InputMediaVideo::ptr &input_media_video = std::get<InputMediaVideo::ptr>(media);

            if(std::filesystem::is_regular_file(input_media_video->get_media()))
            {
                media_to_upload = input_media_video->get_media();
                input_media_video->set_media(
                  "attach://" + std::filesystem::path(input_media_video->get_media()).string());
            }

            if(input_media_video->get_thumbnail().has_value() &&
               std::filesystem::is_regular_file(input_media_video->get_thumbnail().value()))
            {
                thumbnail_to_upload = input_media_video->get_thumbnail().value();
                input_media_video->set_thumbnail(
                  "attach://" + std::filesystem::path(input_media_video->get_thumbnail().value()).string());
            }

            media_json = nlohmann::json::parse(input_media_video->serialise());
        }

        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(media_to_upload.has_value())
        {
            std::ifstream inf(std::filesystem::path(media_to_upload.value()).string(), std::ios::binary);
            multipart.emplace_back(httplib::MultipartFormData(
              {std::filesystem::path(media_to_upload.value()).string(),
               std::string(std::istreambuf_iterator<char>(inf), {}),
               std::filesystem::path(media_to_upload.value()).string(), ""}));
        }

        if(thumbnail_to_upload.has_value())
        {
            std::ifstream inf(std::filesystem::path(thumbnail_to_upload.value()).string(), std::ios::binary);
            multipart.emplace_back(httplib::MultipartFormData(
              {std::filesystem::path(thumbnail_to_upload.value()).string(),
               std::string(std::istreambuf_iterator<char>(inf), {}),
               std::filesystem::path(thumbnail_to_upload.value()).string(), ""}));
        }

        if(chat_id.has_value())
        {
            if(std::holds_alternative<std::int64_t>(chat_id.value()))
            {
                multipart.emplace_back(httplib::MultipartFormData(
                  {"chat_id", std::to_string(std::get<std::int64_t>(chat_id.value())), "", ""}));
            }
            else if(std::holds_alternative<std::string>(chat_id.value()))
            {
                multipart.emplace_back(
                  httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id.value()), "", ""}));
            }
        }

        if(message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_id", std::to_string(message_id.value()), "", ""}));
        }

        if(inline_message_id.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"inline_message_id", inline_message_id.value(), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"media", media_json.dump(), "", ""}));

        if(reply_markup.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_markup", reply_markup.value()->serialise(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/editMessageMedia", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else if(doc["result"].is_object())
                {
                    return std::make_shared<Message>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + ", " + message::json::value::not_object("result") +
                      "\n" + std::to_string(result->status) + "\n" + result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    std::variant<bool, Message::ptr> Endpoints::editMessageReplyMarkup(
      const std::optional<std::variant<std::int64_t, std::string>> &chat_id,
      const std::optional<std::int32_t> &message_id, const std::optional<std::string> &inline_message_id,
      const std::optional<InlineKeyboardMarkup::ptr> &reply_markup) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(chat_id.has_value())
        {
            if(std::holds_alternative<std::int64_t>(chat_id.value()))
            {
                multipart.emplace_back(httplib::MultipartFormData(
                  {"chat_id", std::to_string(std::get<std::int64_t>(chat_id.value())), "", ""}));
            }
            else if(std::holds_alternative<std::string>(chat_id.value()))
            {
                multipart.emplace_back(
                  httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id.value()), "", ""}));
            }
        }

        if(message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_id", std::to_string(message_id.value()), "", ""}));
        }

        if(inline_message_id.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"inline_message_id", inline_message_id.value(), "", ""}));
        }

        if(reply_markup.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_markup", reply_markup.value()->serialise(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/editMessageReplyMarkup", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else if(doc["result"].is_object())
                {
                    return std::make_shared<Message>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + ", " + message::json::value::not_object("result") +
                      "\n" + std::to_string(result->status) + "\n" + result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    std::variant<bool, Message::ptr> Endpoints::editMessageText(
      const std::optional<std::variant<std::int64_t, std::string>> &chat_id,
      const std::optional<std::int32_t> &message_id, const std::optional<std::string> &inline_message_id,
      const std::string &text, const std::optional<std::string> &parse_mode,
      const std::optional<std::vector<MessageEntity::ptr>> &entities,
      const std::optional<bool> &disable_web_page_preview,
      const std::optional<InlineKeyboardMarkup::ptr> &reply_markup) const
    {
        nlohmann::json entities_json = nlohmann::json::array();

        if(entities.has_value())
        {
            for(std::uint64_t i = 0; i < entities.value().size(); ++i)
            {
                entities_json.emplace_back(nlohmann::json::parse(entities.value().at(i)->serialise()));
            }
        }

        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(chat_id.has_value())
        {
            if(std::holds_alternative<std::int64_t>(chat_id.value()))
            {
                multipart.emplace_back(httplib::MultipartFormData(
                  {"chat_id", std::to_string(std::get<std::int64_t>(chat_id.value())), "", ""}));
            }
            else if(std::holds_alternative<std::string>(chat_id.value()))
            {
                multipart.emplace_back(
                  httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id.value()), "", ""}));
            }
        }

        if(message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_id", std::to_string(message_id.value()), "", ""}));
        }

        if(inline_message_id.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"inline_message_id", inline_message_id.value(), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"text", text, "", ""}));

        if(parse_mode.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"parse_mode", parse_mode.value(), "", ""}));
        }

        if(entities.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"entities", entities_json.dump(), "", ""}));
        }

        if(disable_web_page_preview.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"disable_web_page_preview", std::to_string(static_cast<std::int8_t>(disable_web_page_preview.value())),
               "", ""}));
        }

        if(reply_markup.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_markup", reply_markup.value()->serialise(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/editMessageText", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else if(doc["result"].is_object())
                {
                    return std::make_shared<Message>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + ", " + message::json::value::not_object("result") +
                      "\n" + std::to_string(result->status) + "\n" + result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    std::string Endpoints::exportChatInviteLink(const std::variant<std::int64_t, std::string> &chat_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/exportChatInviteLink", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_string())
                {
                    return doc["result"].get<std::string>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_string("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    Message::ptr Endpoints::forwardMessage(
      const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
      const std::variant<std::int64_t, std::string> &from_chat_id, const std::optional<bool> &disable_notification,
      const std::optional<bool> &protect_content, const std::int32_t &message_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(message_thread_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_thread_id", std::to_string(message_thread_id.value()), "", ""}));
        }

        if(std::holds_alternative<std::int64_t>(from_chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"from_chat_id", std::to_string(std::get<std::int64_t>(from_chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(from_chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"from_chat_id", std::get<std::string>(from_chat_id), "", ""}));
        }

        if(disable_notification.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"disable_notification", std::to_string(static_cast<std::int8_t>(disable_notification.value())), "", ""}));
        }

        if(protect_content.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"protect_content", std::to_string(static_cast<std::int8_t>(protect_content.value())), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"message_id", std::to_string(message_id), "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/forwardMessage", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<Message>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    Chat::ptr Endpoints::getChat(const std::variant<std::int64_t, std::string> &chat_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/getChat", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<Chat>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    std::vector<ChatMember::ptr>
      Endpoints::getChatAdministrators(const std::variant<std::int64_t, std::string> &chat_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/getChatAdministrators", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_array())
                {
                    std::vector<ChatMember::ptr> chat_members;
                    chat_members.reserve(doc["result"].size());

                    for(std::uint64_t i = 0; i < doc["result"].size(); ++i)
                    {
                        if(doc["result"].at(i).is_object())
                        {
                            if(doc["result"].at(i).contains("status"))
                            {
                                if(doc["result"].at(i)["status"].is_string())
                                {
                                    if(doc["result"].at(i)["status"].get<std::string>() == "administrator")
                                    {
                                        chat_members.emplace_back(
                                          std::make_shared<ChatMemberAdministrator>(doc["result"].at(i).dump()));
                                    }
                                    else if(doc["result"].at(i)["status"].get<std::string>() == "kicked")
                                    {
                                        chat_members.emplace_back(
                                          std::make_shared<ChatMemberBanned>(doc["result"].at(i).dump()));
                                    }
                                    else if(doc["result"].at(i)["status"].get<std::string>() == "left")
                                    {
                                        chat_members.emplace_back(
                                          std::make_shared<ChatMemberLeft>(doc["result"].at(i).dump()));
                                    }
                                    else if(doc["result"].at(i)["status"].get<std::string>() == "member")
                                    {
                                        chat_members.emplace_back(
                                          std::make_shared<ChatMemberMember>(doc["result"].at(i).dump()));
                                    }
                                    else if(doc["result"].at(i)["status"].get<std::string>() == "creator")
                                    {
                                        chat_members.emplace_back(
                                          std::make_shared<ChatMemberOwner>(doc["result"].at(i).dump()));
                                    }
                                    else if(doc["result"].at(i)["status"].get<std::string>() == "restricted")
                                    {
                                        chat_members.emplace_back(
                                          std::make_shared<ChatMemberRestricted>(doc["result"].at(i).dump()));
                                    }
                                    else
                                    {
                                        throw std::runtime_error(
                                          message::json::value::unexpected("status") + "\n" +
                                          std::to_string(result->status) + "\n" + result->body);
                                    }
                                }
                                else
                                {
                                    throw std::runtime_error(
                                      message::json::value::not_string("status") + "\n" +
                                      std::to_string(result->status) + "\n" + result->body);
                                }
                            }
                            else
                            {
                                throw std::runtime_error(
                                  message::json::key_non_existent("status") + "\n" + std::to_string(result->status) +
                                  "\n" + result->body);
                            }
                        }
                        else
                        {
                            throw std::runtime_error(
                              message::json::value::nested::not_object("result") + "\n" +
                              std::to_string(result->status) + "\n" + result->body);
                        }
                    }

                    return chat_members;
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_array("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    ChatMember::ptr Endpoints::getChatMember(
      const std::variant<std::int64_t, std::string> &chat_id, const std::int64_t &user_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"user_id", std::to_string(user_id), "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/getChatMember", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    if(doc["result"].contains("status"))
                    {
                        if(doc["result"]["status"].is_string())
                        {
                            if(doc["result"]["status"].get<std::string>() == "administrator")
                            {
                                return std::make_shared<ChatMemberAdministrator>(doc["result"].dump());
                            }
                            else if(doc["result"]["status"].get<std::string>() == "kicked")
                            {
                                return std::make_shared<ChatMemberBanned>(doc["result"].dump());
                            }
                            else if(doc["result"]["status"].get<std::string>() == "left")
                            {
                                return std::make_shared<ChatMemberLeft>(doc["result"].dump());
                            }
                            else if(doc["result"]["status"].get<std::string>() == "member")
                            {
                                return std::make_shared<ChatMemberMember>(doc["result"].dump());
                            }
                            else if(doc["result"]["status"].get<std::string>() == "creator")
                            {
                                return std::make_shared<ChatMemberOwner>(doc["result"].dump());
                            }
                            else if(doc["result"]["status"].get<std::string>() == "restricted")
                            {
                                return std::make_shared<ChatMemberRestricted>(doc["result"].dump());
                            }
                            else
                            {
                                throw std::runtime_error(
                                  message::json::value::unexpected("status") + "\n" + std::to_string(result->status) +
                                  "\n" + result->body);
                            }
                        }
                        else
                        {
                            throw std::runtime_error(
                              message::json::value::not_string("status") + "\n" + std::to_string(result->status) +
                              "\n" + result->body);
                        }
                    }
                    else
                    {
                        throw std::runtime_error(
                          message::json::key_non_existent("status") + "\n" + std::to_string(result->status) + "\n" +
                          result->body);
                    }
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    std::int32_t Endpoints::getChatMemberCount(const std::variant<std::int64_t, std::string> &chat_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/getChatMemberCount", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_number_integer())
                {
                    return doc["result"].get<std::int32_t>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_int("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    MenuButton::ptr Endpoints::getChatMenuButton(const std::optional<std::int64_t> &chat_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(chat_id.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::to_string(chat_id.value()), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        std::int32_t result_status = -1;
        std::string result_body;

        if(chat_id.has_value() || m_test_value.has_value())
        {
            const httplib::Result result = client.Post("/bot" + m_token + "/getChatMenuButton", multipart);

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }
        else
        {
            const httplib::Result result = client.Get("/bot" + m_token + "/getChatMenuButton");

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }

        const nlohmann::json doc = nlohmann::json::parse(result_body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<MenuButton>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result_status) + "\n" +
                      result_body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result_status) + "\n" +
                  result_body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result_status) + "\n" +
              result_body);
        }
    }

    std::vector<Sticker::ptr> Endpoints::getCustomEmojiStickers(const std::vector<std::string> &custom_emoji_ids) const
    {
        nlohmann::json custom_emoji_ids_json = nlohmann::json::array();

        for(std::uint64_t i = 0; i < custom_emoji_ids.size(); ++i)
        {
            custom_emoji_ids_json.emplace_back(custom_emoji_ids.at(i));
        }

        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;
        multipart.emplace_back(httplib::MultipartFormData({"custom_emoji_ids", custom_emoji_ids_json.dump(), "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/getCustomEmojiStickers", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_array())
                {
                    std::vector<Sticker::ptr> stickers;
                    stickers.reserve(doc["result"].size());

                    for(std::uint64_t i = 0; i < doc["result"].size(); ++i)
                    {
                        if(doc["result"].at(i).is_object())
                        {
                            stickers.emplace_back(std::make_shared<Sticker>(doc["result"].at(i).dump()));
                        }
                        else
                        {
                            throw std::runtime_error(
                              message::json::value::nested::not_object("result") + "\n" +
                              std::to_string(result->status) + "\n" + result->body);
                        }
                    }

                    return stickers;
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_array("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    File::ptr Endpoints::getFile(const std::string &file_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;
        multipart.emplace_back(httplib::MultipartFormData({"file_id", file_id, "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/getFile", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<File>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    std::vector<Sticker::ptr> Endpoints::getForumTopicIconStickers() const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        std::int32_t result_status = -1;
        std::string result_body;

        if(m_test_value.has_value())
        {
            const httplib::Result result = client.Post("/bot" + m_token + "/getForumTopicIconStickers", multipart);

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }
        else
        {
            const httplib::Result result = client.Get("/bot" + m_token + "/getForumTopicIconStickers");

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }

        const nlohmann::json doc = nlohmann::json::parse(result_body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_array())
                {
                    std::vector<Sticker::ptr> stickers;
                    stickers.reserve(doc["result"].size());

                    for(std::uint64_t i = 0; i < doc["result"].size(); ++i)
                    {
                        if(doc["result"].at(i).is_object())
                        {
                            stickers.emplace_back(std::make_shared<Sticker>(doc["result"].at(i).dump()));
                        }
                        else
                        {
                            throw std::runtime_error(
                              message::json::value::nested::not_object("result") + "\n" +
                              std::to_string(result_status) + "\n" + result_body);
                        }
                    }

                    return stickers;
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_array("result") + "\n" + std::to_string(result_status) + "\n" +
                      result_body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result_status) + "\n" +
                  result_body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result_status) + "\n" +
              result_body);
        }
    }

    std::vector<GameHighScore::ptr> Endpoints::getGameHighScores(
      const std::int64_t &user_id, const std::optional<std::int64_t> &chat_id,
      const std::optional<std::int32_t> &message_id, const std::optional<std::string> &inline_message_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;
        multipart.emplace_back(httplib::MultipartFormData({"user_id", std::to_string(user_id), "", ""}));

        if(chat_id.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::to_string(chat_id.value()), "", ""}));
        }

        if(message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_id", std::to_string(message_id.value()), "", ""}));
        }

        if(inline_message_id.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"inline_message_id", inline_message_id.value(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/getGameHighScores", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_array())
                {
                    std::vector<GameHighScore::ptr> game_high_scores;
                    game_high_scores.reserve(doc["result"].size());

                    for(std::uint64_t i = 0; i < doc["result"].size(); ++i)
                    {
                        if(doc["result"].at(i).is_object())
                        {
                            game_high_scores.emplace_back(std::make_shared<GameHighScore>(doc["result"].at(i).dump()));
                        }
                        else
                        {
                            throw std::runtime_error(
                              message::json::value::nested::not_object("result") + "\n" +
                              std::to_string(result->status) + "\n" + result->body);
                        }
                    }

                    return game_high_scores;
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_array("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    User::ptr Endpoints::getMe() const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        std::int32_t result_status = -1;
        std::string result_body;

        if(m_test_value.has_value())
        {
            const httplib::Result result = client.Post("/bot" + m_token + "/getMe", multipart);

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }
        else
        {
            const httplib::Result result = client.Get("/bot" + m_token + "/getMe");

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }

        const nlohmann::json doc = nlohmann::json::parse(result_body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<User>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result_status) + "\n" +
                      result_body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result_status) + "\n" +
                  result_body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result_status) + "\n" +
              result_body);
        }
    }

    std::vector<BotCommand::ptr> Endpoints::getMyCommands(
      const std::optional<BotCommandScope::ptr> &scope, const std::optional<std::string> &language_code) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(scope.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"scope", scope.value()->serialise(), "", ""}));
        }

        if(language_code.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"language_code", language_code.value(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        std::int32_t result_status = -1;
        std::string result_body;

        if(scope.has_value() || language_code.has_value() || m_test_value.has_value())
        {
            const httplib::Result result = client.Post("/bot" + m_token + "/getMyCommands", multipart);

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }
        else
        {
            const httplib::Result result = client.Get("/bot" + m_token + "/getMyCommands");

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }

        const nlohmann::json doc = nlohmann::json::parse(result_body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_array())
                {
                    std::vector<BotCommand::ptr> bot_commands;
                    bot_commands.reserve(doc["result"].size());

                    for(std::uint64_t i = 0; i < doc["result"].size(); ++i)
                    {
                        if(doc["result"].at(i).is_object())
                        {
                            bot_commands.emplace_back(std::make_shared<BotCommand>(doc["result"].at(i).dump()));
                        }
                        else
                        {
                            throw std::runtime_error(
                              message::json::value::nested::not_object("result") + "\n" +
                              std::to_string(result_status) + "\n" + result_body);
                        }
                    }

                    return bot_commands;
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_array("result") + "\n" + std::to_string(result_status) + "\n" +
                      result_body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result_status) + "\n" +
                  result_body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result_status) + "\n" +
              result_body);
        }
    }

    ChatAdministratorRights::ptr
      Endpoints::getMyDefaultAdministratorRights(const std::optional<bool> &for_channels) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(for_channels.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"for_channels", std::to_string(static_cast<std::int8_t>(for_channels.value())), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        std::int32_t result_status = -1;
        std::string result_body;

        if(for_channels.has_value() || m_test_value.has_value())
        {
            const httplib::Result result = client.Post("/bot" + m_token + "/getMyDefaultAdministratorRights", multipart);

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }
        else
        {
            const httplib::Result result = client.Get("/bot" + m_token + "/getMyDefaultAdministratorRights");

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }

        const nlohmann::json doc = nlohmann::json::parse(result_body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<ChatAdministratorRights>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result_status) + "\n" +
                      result_body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result_status) + "\n" +
                  result_body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result_status) + "\n" +
              result_body);
        }
    }

    BotDescription::ptr Endpoints::getMyDescription(const std::optional<std::string> &language_code) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(language_code.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"language_code", language_code.value(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        std::int32_t result_status = -1;
        std::string result_body;

        if(language_code.has_value() || m_test_value.has_value())
        {
            const httplib::Result result = client.Post("/bot" + m_token + "/getMyDescription", multipart);

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }
        else
        {
            const httplib::Result result = client.Get("/bot" + m_token + "/getMyDescription");

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }

        const nlohmann::json doc = nlohmann::json::parse(result_body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<BotDescription>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result_status) + "\n" +
                      result_body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result_status) + "\n" +
                  result_body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result_status) + "\n" +
              result_body);
        }
    }

    BotShortDescription::ptr Endpoints::getMyShortDescription(const std::optional<std::string> &language_code) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(language_code.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"language_code", language_code.value(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        std::int32_t result_status = -1;
        std::string result_body;

        if(language_code.has_value() || m_test_value.has_value())
        {
            const httplib::Result result = client.Post("/bot" + m_token + "/getMyShortDescription", multipart);

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }
        else
        {
            const httplib::Result result = client.Get("/bot" + m_token + "/getMyShortDescription");

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }

        const nlohmann::json doc = nlohmann::json::parse(result_body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<BotShortDescription>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result_status) + "\n" +
                      result_body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result_status) + "\n" +
                  result_body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result_status) + "\n" +
              result_body);
        }
    }

    StickerSet::ptr Endpoints::getStickerSet(const std::string &name) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;
        multipart.emplace_back(httplib::MultipartFormData({"name", name, "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/getStickerSet", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<StickerSet>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    std::vector<Update::ptr> Endpoints::getUpdates(
      const std::optional<std::int32_t> &offset, const std::optional<std::int32_t> &limit,
      const std::optional<std::int32_t> &timeout, const std::optional<std::vector<std::string>> &allowed_updates) const
    {
        nlohmann::json allowed_updates_json = nlohmann::json::array();

        if(allowed_updates.has_value())
        {
            for(std::uint64_t i = 0; i < allowed_updates.value().size(); ++i)
            {
                allowed_updates_json.emplace_back(allowed_updates.value().at(i));
            }
        }

        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(offset.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"offset", std::to_string(offset.value()), "", ""}));
        }

        if(limit.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"limit", std::to_string(limit.value()), "", ""}));
        }

        if(timeout.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"timeout", std::to_string(timeout.value()), "", ""}));
        }

        if(allowed_updates.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"allowed_updates", allowed_updates_json.dump(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        std::int32_t result_status = -1;
        std::string result_body;

        if(offset.has_value() || limit.has_value() || timeout.has_value() || allowed_updates.has_value() ||
           m_test_value.has_value())
        {
            const httplib::Result result = client.Post("/bot" + m_token + "/getUpdates", multipart);

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }
        else
        {
            const httplib::Result result = client.Get("/bot" + m_token + "/getUpdates");

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }

        const nlohmann::json doc = nlohmann::json::parse(result_body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_array())
                {
                    std::vector<Update::ptr> updates;
                    updates.reserve(doc["result"].size());

                    for(std::uint64_t i = 0; i < doc["result"].size(); ++i)
                    {
                        if(doc["result"].at(i).is_object())
                        {
                            updates.emplace_back(std::make_shared<Update>(doc["result"].at(i).dump()));
                        }
                        else
                        {
                            throw std::runtime_error(
                              message::json::value::nested::not_object("result") + "\n" +
                              std::to_string(result_status) + "\n" + result_body);
                        }
                    }

                    return updates;
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_array("result") + "\n" + std::to_string(result_status) + "\n" +
                      result_body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result_status) + "\n" +
                  result_body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result_status) + "\n" +
              result_body);
        }
    }

    UserProfilePhotos::ptr Endpoints::getUserProfilePhotos(
      const std::int64_t &user_id, const std::optional<std::int32_t> &offset,
      const std::optional<std::int32_t> &limit) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;
        multipart.emplace_back(httplib::MultipartFormData({"user_id", std::to_string(user_id), "", ""}));

        if(offset.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"offset", std::to_string(offset.value()), "", ""}));
        }

        if(limit.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"limit", std::to_string(limit.value()), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/getUserProfilePhotos", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<UserProfilePhotos>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    WebhookInfo::ptr Endpoints::getWebhookInfo() const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        std::int32_t result_status = -1;
        std::string result_body;

        if(m_test_value.has_value())
        {
            const httplib::Result result = client.Post("/bot" + m_token + "/getWebhookInfo", multipart);

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }
        else
        {
            const httplib::Result result = client.Get("/bot" + m_token + "/getWebhookInfo");

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }

        const nlohmann::json doc = nlohmann::json::parse(result_body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<WebhookInfo>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result_status) + "\n" +
                      result_body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result_status) + "\n" +
                  result_body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result_status) + "\n" +
              result_body);
        }
    }

    bool Endpoints::hideGeneralForumTopic(const std::variant<std::int64_t, std::string> &chat_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/hideGeneralForumTopic", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::leaveChat(const std::variant<std::int64_t, std::string> &chat_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/leaveChat", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::logOut() const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        std::int32_t result_status = -1;
        std::string result_body;

        if(m_test_value.has_value())
        {
            const httplib::Result result = client.Post("/bot" + m_token + "/logOut", multipart);

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }
        else
        {
            const httplib::Result result = client.Get("/bot" + m_token + "/logOut");

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }

        const nlohmann::json doc = nlohmann::json::parse(result_body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result_status) + "\n" +
                      result_body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result_status) + "\n" +
                  result_body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result_status) + "\n" +
              result_body);
        }
    }

    bool Endpoints::pinChatMessage(
      const std::variant<std::int64_t, std::string> &chat_id, const std::int32_t &message_id,
      const std::optional<bool> &disable_notification) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"message_id", std::to_string(message_id), "", ""}));

        if(disable_notification.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"disable_notification", std::to_string(static_cast<std::int8_t>(disable_notification.value())), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/pinChatMessage", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::promoteChatMember(
      const std::variant<std::int64_t, std::string> &chat_id, const std::int64_t &user_id,
      const std::optional<bool> &is_anonymous, const std::optional<bool> &can_manage_chat,
      const std::optional<bool> &can_post_messages, const std::optional<bool> &can_edit_messages,
      const std::optional<bool> &can_delete_messages, const std::optional<bool> &can_manage_video_chats,
      const std::optional<bool> &can_restrict_members, const std::optional<bool> &can_promote_members,
      const std::optional<bool> &can_change_info, const std::optional<bool> &can_invite_users,
      const std::optional<bool> &can_pin_messages, const std::optional<bool> &can_manage_topics) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"user_id", std::to_string(user_id), "", ""}));

        if(is_anonymous.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"is_anonymous", std::to_string(static_cast<std::int8_t>(is_anonymous.value())), "", ""}));
        }

        if(can_manage_chat.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"can_manage_chat", std::to_string(static_cast<std::int8_t>(can_manage_chat.value())), "", ""}));
        }

        if(can_post_messages.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"can_post_messages", std::to_string(static_cast<std::int8_t>(can_post_messages.value())), "", ""}));
        }

        if(can_edit_messages.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"can_edit_messages", std::to_string(static_cast<std::int8_t>(can_edit_messages.value())), "", ""}));
        }

        if(can_delete_messages.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"can_delete_messages", std::to_string(static_cast<std::int8_t>(can_delete_messages.value())), "", ""}));
        }

        if(can_manage_video_chats.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"can_manage_video_chats", std::to_string(static_cast<std::int8_t>(can_manage_video_chats.value())), "",
               ""}));
        }

        if(can_restrict_members.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"can_restrict_members", std::to_string(static_cast<std::int8_t>(can_restrict_members.value())), "", ""}));
        }

        if(can_promote_members.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"can_promote_members", std::to_string(static_cast<std::int8_t>(can_promote_members.value())), "", ""}));
        }

        if(can_change_info.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"can_change_info", std::to_string(static_cast<std::int8_t>(can_change_info.value())), "", ""}));
        }

        if(can_invite_users.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"can_invite_users", std::to_string(static_cast<std::int8_t>(can_invite_users.value())), "", ""}));
        }

        if(can_pin_messages.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"can_pin_messages", std::to_string(static_cast<std::int8_t>(can_pin_messages.value())), "", ""}));
        }

        if(can_manage_topics.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"can_pin_messages", std::to_string(static_cast<std::int8_t>(can_manage_topics.value())), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/promoteChatMember", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::reopenForumTopic(
      const std::variant<std::int64_t, std::string> &chat_id, const std::int32_t &message_thread_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        multipart.emplace_back(
          httplib::MultipartFormData({"message_thread_id", std::to_string(message_thread_id), "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/reopenForumTopic", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::reopenGeneralForumTopic(const std::variant<std::int64_t, std::string> &chat_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/reopenGeneralForumTopic", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::restrictChatMember(
      const std::variant<std::int64_t, std::string> &chat_id, const std::int64_t &user_id,
      const ChatPermissions::ptr &permissions, const std::optional<bool> &use_independent_chat_permissions,
      const std::optional<std::uint64_t> &until_date) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"user_id", std::to_string(user_id), "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"permissions", permissions->serialise(), "", ""}));

        if(use_independent_chat_permissions.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"use_independent_chat_permissions",
               std::to_string(static_cast<std::int8_t>(use_independent_chat_permissions.value())), "", ""}));
        }

        if(until_date.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"until_date", std::to_string(until_date.value()), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/restrictChatMember", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    ChatInviteLink::ptr Endpoints::revokeChatInviteLink(
      const std::variant<std::int64_t, std::string> &chat_id, const std::string &invite_link) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"invite_link", invite_link, "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/revokeChatInviteLink", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<ChatInviteLink>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    Message::ptr Endpoints::sendAnimation(
      const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
      const std::string &animation, const std::optional<std::int32_t> &duration,
      const std::optional<std::int32_t> &width, const std::optional<std::int32_t> &height,
      const std::optional<std::string> &thumbnail, const std::optional<std::string> &caption,
      const std::optional<std::string> &parse_mode,
      const std::optional<std::vector<MessageEntity::ptr>> &caption_entities, const std::optional<bool> &has_spoiler,
      const std::optional<bool> &disable_notification, const std::optional<bool> &protect_content,
      const std::optional<std::int32_t> &reply_to_message_id, const std::optional<bool> &allow_sending_without_reply,
      const std::optional<Reply::ptr> &reply_markup) const
    {
        nlohmann::json caption_entities_json = nlohmann::json::array();

        if(caption_entities.has_value())
        {
            for(std::uint64_t i = 0; i < caption_entities.value().size(); ++i)
            {
                caption_entities_json.emplace_back(nlohmann::json::parse(caption_entities.value().at(i)->serialise()));
            }
        }

        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(message_thread_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_thread_id", std::to_string(message_thread_id.value()), "", ""}));
        }

        if(std::filesystem::is_regular_file(animation))
        {
            std::ifstream inf(std::filesystem::path(animation).string(), std::ios::binary);
            multipart.emplace_back(httplib::MultipartFormData(
              {"animation", std::string(std::istreambuf_iterator<char>(inf), {}),
               std::filesystem::path(animation).string(), ""}));
        }
        else
        {
            multipart.emplace_back(httplib::MultipartFormData({"animation", animation, "", ""}));
        }

        if(duration.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"duration", std::to_string(duration.value()), "", ""}));
        }

        if(width.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"width", std::to_string(width.value()), "", ""}));
        }

        if(height.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"height", std::to_string(height.value()), "", ""}));
        }

        if(thumbnail.has_value())
        {
            if(std::filesystem::is_regular_file(thumbnail.value()))
            {
                std::ifstream inf(std::filesystem::path(thumbnail.value()).string(), std::ios::binary);
                multipart.emplace_back(httplib::MultipartFormData(
                  {std::filesystem::path(thumbnail.value()).string(),
                   std::string(std::istreambuf_iterator<char>(inf), {}),
                   std::filesystem::path(thumbnail.value()).string(), ""}));
                multipart.emplace_back(httplib::MultipartFormData(
                  {"thumbnail", "attach://" + std::filesystem::path(thumbnail.value()).string(), "", ""}));
            }
            else
            {
                multipart.emplace_back(httplib::MultipartFormData({"thumbnail", thumbnail.value(), "", ""}));
            }
        }

        if(caption.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"caption", caption.value(), "", ""}));
        }

        if(parse_mode.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"parse_mode", parse_mode.value(), "", ""}));
        }

        if(caption_entities.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"caption_entities", caption_entities_json.dump(), "", ""}));
        }

        if(has_spoiler.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"has_spoiler", std::to_string(static_cast<std::int8_t>(has_spoiler.value())), "", ""}));
        }

        if(disable_notification.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"disable_notification", std::to_string(static_cast<std::int8_t>(disable_notification.value())), "", ""}));
        }

        if(protect_content.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"protect_content", std::to_string(static_cast<std::int8_t>(protect_content.value())), "", ""}));
        }

        if(reply_to_message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_to_message_id", std::to_string(reply_to_message_id.value()), "", ""}));
        }

        if(allow_sending_without_reply.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"allow_sending_without_reply",
               std::to_string(static_cast<std::int8_t>(allow_sending_without_reply.value())), "", ""}));
        }

        if(reply_markup.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_markup", reply_markup.value()->serialise(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/sendAnimation", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<Message>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    Message::ptr Endpoints::sendAudio(
      const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
      const std::string &audio, const std::optional<std::string> &caption, const std::optional<std::string> &parse_mode,
      const std::optional<std::vector<MessageEntity::ptr>> &caption_entities,
      const std::optional<std::int32_t> &duration, const std::optional<std::string> &performer,
      const std::optional<std::string> &title, const std::optional<std::string> &thumbnail,
      const std::optional<bool> &disable_notification, const std::optional<bool> &protect_content,
      const std::optional<std::int32_t> &reply_to_message_id, const std::optional<bool> &allow_sending_without_reply,
      const std::optional<Reply::ptr> &reply_markup) const
    {
        nlohmann::json caption_entities_json = nlohmann::json::array();

        if(caption_entities.has_value())
        {
            for(std::uint64_t i = 0; i < caption_entities.value().size(); ++i)
            {
                caption_entities_json.emplace_back(nlohmann::json::parse(caption_entities.value().at(i)->serialise()));
            }
        }

        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(message_thread_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_thread_id", std::to_string(message_thread_id.value()), "", ""}));
        }

        if(std::filesystem::is_regular_file(audio))
        {
            std::ifstream inf(std::filesystem::path(audio).string(), std::ios::binary);
            multipart.emplace_back(httplib::MultipartFormData(
              {"audio", std::string(std::istreambuf_iterator<char>(inf), {}), std::filesystem::path(audio).string(),
               ""}));
        }
        else
        {
            multipart.emplace_back(httplib::MultipartFormData({"audio", audio, "", ""}));
        }

        if(caption.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"caption", caption.value(), "", ""}));
        }

        if(parse_mode.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"parse_mode", parse_mode.value(), "", ""}));
        }

        if(caption_entities.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"caption_entities", caption_entities_json.dump(), "", ""}));
        }

        if(duration.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"duration", std::to_string(duration.value()), "", ""}));
        }

        if(performer.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"performer", performer.value(), "", ""}));
        }

        if(title.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"title", title.value(), "", ""}));
        }

        if(thumbnail.has_value())
        {
            if(std::filesystem::is_regular_file(thumbnail.value()))
            {
                std::ifstream inf(std::filesystem::path(thumbnail.value()).string(), std::ios::binary);
                multipart.emplace_back(httplib::MultipartFormData(
                  {std::filesystem::path(thumbnail.value()).string(),
                   std::string(std::istreambuf_iterator<char>(inf), {}),
                   std::filesystem::path(thumbnail.value()).string(), ""}));
                multipart.emplace_back(httplib::MultipartFormData(
                  {"thumbnail", "attach://" + std::filesystem::path(thumbnail.value()).string(), "", ""}));
            }
            else
            {
                multipart.emplace_back(httplib::MultipartFormData({"thumbnail", thumbnail.value(), "", ""}));
            }
        }

        if(disable_notification.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"disable_notification", std::to_string(static_cast<std::int8_t>(disable_notification.value())), "", ""}));
        }

        if(protect_content.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"protect_content", std::to_string(static_cast<std::int8_t>(protect_content.value())), "", ""}));
        }

        if(reply_to_message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_to_message_id", std::to_string(reply_to_message_id.value()), "", ""}));
        }

        if(allow_sending_without_reply.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"allow_sending_without_reply",
               std::to_string(static_cast<std::int8_t>(allow_sending_without_reply.value())), "", ""}));
        }

        if(reply_markup.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_markup", reply_markup.value()->serialise(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/sendAudio", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<Message>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::sendChatAction(
      const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
      const std::string &action) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(message_thread_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_thread_id", std::to_string(message_thread_id.value()), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"action", action, "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/sendChatAction", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    Message::ptr Endpoints::sendContact(
      const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
      const std::string &phone_number, const std::string &first_name, const std::optional<std::string> &last_name,
      const std::optional<std::string> &vcard, const std::optional<bool> &disable_notification,
      const std::optional<bool> &protect_content, const std::optional<std::int32_t> &reply_to_message_id,
      const std::optional<bool> &allow_sending_without_reply, const std::optional<Reply::ptr> &reply_markup) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(message_thread_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_thread_id", std::to_string(message_thread_id.value()), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"phone_number", phone_number, "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"first_name", first_name, "", ""}));

        if(last_name.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"last_name", last_name.value(), "", ""}));
        }

        if(vcard.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"vcard", vcard.value(), "", ""}));
        }

        if(disable_notification.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"disable_notification", std::to_string(static_cast<std::int8_t>(disable_notification.value())), "", ""}));
        }

        if(protect_content.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"protect_content", std::to_string(static_cast<std::int8_t>(protect_content.value())), "", ""}));
        }

        if(reply_to_message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_to_message_id", std::to_string(reply_to_message_id.value()), "", ""}));
        }

        if(allow_sending_without_reply.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"allow_sending_without_reply",
               std::to_string(static_cast<std::int8_t>(allow_sending_without_reply.value())), "", ""}));
        }

        if(reply_markup.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_markup", reply_markup.value()->serialise(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/sendContact", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<Message>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    Message::ptr Endpoints::sendDice(
      const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
      const std::optional<std::string> &emoji, const std::optional<bool> &disable_notification,
      const std::optional<bool> &protect_content, const std::optional<std::int32_t> &reply_to_message_id,
      const std::optional<bool> &allow_sending_without_reply, const std::optional<Reply::ptr> &reply_markup) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(message_thread_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_thread_id", std::to_string(message_thread_id.value()), "", ""}));
        }

        if(emoji.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"emoji", emoji.value(), "", ""}));
        }

        if(disable_notification.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"disable_notification", std::to_string(static_cast<std::int8_t>(disable_notification.value())), "", ""}));
        }

        if(protect_content.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"protect_content", std::to_string(static_cast<std::int8_t>(protect_content.value())), "", ""}));
        }

        if(reply_to_message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_to_message_id", std::to_string(reply_to_message_id.value()), "", ""}));
        }

        if(allow_sending_without_reply.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"allow_sending_without_reply",
               std::to_string(static_cast<std::int8_t>(allow_sending_without_reply.value())), "", ""}));
        }

        if(reply_markup.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_markup", reply_markup.value()->serialise(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/sendDice", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<Message>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    Message::ptr Endpoints::sendDocument(
      const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
      const std::string &document, const std::optional<std::string> &thumbnail,
      const std::optional<std::string> &caption, const std::optional<std::string> &parse_mode,
      const std::optional<std::vector<MessageEntity::ptr>> &caption_entities,
      const std::optional<bool> &disable_content_type_detection, const std::optional<bool> &disable_notification,
      const std::optional<bool> &protect_content, const std::optional<std::int32_t> &reply_to_message_id,
      const std::optional<bool> &allow_sending_without_reply, const std::optional<Reply::ptr> &reply_markup) const
    {
        nlohmann::json caption_entities_json = nlohmann::json::array();

        if(caption_entities.has_value())
        {
            for(std::uint64_t i = 0; i < caption_entities.value().size(); ++i)
            {
                caption_entities_json.emplace_back(nlohmann::json::parse(caption_entities.value().at(i)->serialise()));
            }
        }

        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(message_thread_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_thread_id", std::to_string(message_thread_id.value()), "", ""}));
        }

        if(std::filesystem::is_regular_file(document))
        {
            std::ifstream inf(std::filesystem::path(document).string(), std::ios::binary);
            multipart.emplace_back(httplib::MultipartFormData(
              {"document", std::string(std::istreambuf_iterator<char>(inf), {}),
               std::filesystem::path(document).string(), ""}));
        }
        else
        {
            multipart.emplace_back(httplib::MultipartFormData({"document", document, "", ""}));
        }

        if(thumbnail.has_value())
        {
            if(std::filesystem::is_regular_file(thumbnail.value()))
            {
                std::ifstream inf(std::filesystem::path(thumbnail.value()).string(), std::ios::binary);
                multipart.emplace_back(httplib::MultipartFormData(
                  {std::filesystem::path(thumbnail.value()).string(),
                   std::string(std::istreambuf_iterator<char>(inf), {}),
                   std::filesystem::path(thumbnail.value()).string(), ""}));
                multipart.emplace_back(httplib::MultipartFormData(
                  {"thumbnail", "attach://" + std::filesystem::path(thumbnail.value()).string(), "", ""}));
            }
            else
            {
                multipart.emplace_back(httplib::MultipartFormData({"thumbnail", thumbnail.value(), "", ""}));
            }
        }

        if(caption.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"caption", caption.value(), "", ""}));
        }

        if(parse_mode.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"parse_mode", parse_mode.value(), "", ""}));
        }

        if(caption_entities.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"caption_entities", caption_entities_json.dump(), "", ""}));
        }

        if(disable_content_type_detection.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"disable_content_type_detection",
               std::to_string(static_cast<std::int8_t>(disable_content_type_detection.value())), "", ""}));
        }

        if(disable_notification.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"disable_notification", std::to_string(static_cast<std::int8_t>(disable_notification.value())), "", ""}));
        }

        if(protect_content.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"protect_content", std::to_string(static_cast<std::int8_t>(protect_content.value())), "", ""}));
        }

        if(reply_to_message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_to_message_id", std::to_string(reply_to_message_id.value()), "", ""}));
        }

        if(allow_sending_without_reply.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"allow_sending_without_reply",
               std::to_string(static_cast<std::int8_t>(allow_sending_without_reply.value())), "", ""}));
        }

        if(reply_markup.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_markup", reply_markup.value()->serialise(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/sendDocument", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<Message>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    Message::ptr Endpoints::sendGame(
      const std::int64_t &chat_id, const std::optional<std::int32_t> &message_thread_id,
      const std::string &game_short_name, const std::optional<bool> &disable_notification,
      const std::optional<bool> &protect_content, const std::optional<std::int32_t> &reply_to_message_id,
      const std::optional<bool> &allow_sending_without_reply,
      const std::optional<InlineKeyboardMarkup::ptr> &reply_markup) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;
        multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::to_string(chat_id), "", ""}));

        if(message_thread_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_thread_id", std::to_string(message_thread_id.value()), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"game_short_name", game_short_name, "", ""}));

        if(disable_notification.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"disable_notification", std::to_string(static_cast<std::int8_t>(disable_notification.value())), "", ""}));
        }

        if(protect_content.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"protect_content", std::to_string(static_cast<std::int8_t>(protect_content.value())), "", ""}));
        }

        if(reply_to_message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_to_message_id", std::to_string(reply_to_message_id.value()), "", ""}));
        }

        if(allow_sending_without_reply.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"allow_sending_without_reply",
               std::to_string(static_cast<std::int8_t>(allow_sending_without_reply.value())), "", ""}));
        }

        if(reply_markup.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_markup", reply_markup.value()->serialise(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/sendGame", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<Message>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    Message::ptr Endpoints::sendInvoice(
      const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
      const std::string &title, const std::string &description, const std::string &payload,
      const std::string &provider_token, const std::string &currency, const std::vector<LabeledPrice::ptr> &prices,
      const std::optional<std::int32_t> &max_tip_amount,
      const std::optional<std::vector<std::int32_t>> &suggested_tip_amounts,
      const std::optional<std::string> &start_parameter, const std::optional<std::string> &provider_data,
      const std::optional<std::string> &photo_url, const std::optional<std::int32_t> &photo_size,
      const std::optional<std::int32_t> &photo_width, const std::optional<std::int32_t> &photo_height,
      const std::optional<bool> &need_name, const std::optional<bool> &need_phone_number,
      const std::optional<bool> &need_email, const std::optional<bool> &need_shipping_address,
      const std::optional<bool> &send_phone_number_to_provider, const std::optional<bool> &send_email_to_provider,
      const std::optional<bool> &is_flexible, const std::optional<bool> &disable_notification,
      const std::optional<bool> &protect_content, const std::optional<std::int32_t> &reply_to_message_id,
      const std::optional<bool> &allow_sending_without_reply,
      const std::optional<InlineKeyboardMarkup::ptr> &reply_markup) const
    {
        nlohmann::json prices_json = nlohmann::json::array();

        for(std::uint64_t i = 0; i < prices.size(); ++i)
        {
            prices_json.emplace_back(nlohmann::json::parse(prices.at(i)->serialise()));
        }

        nlohmann::json suggested_tip_amounts_json = nlohmann::json::array();

        if(suggested_tip_amounts.has_value())
        {
            for(std::uint64_t i = 0; i < suggested_tip_amounts.value().size(); ++i)
            {
                suggested_tip_amounts_json.emplace_back(suggested_tip_amounts.value().at(i));
            }
        }

        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(message_thread_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_thread_id", std::to_string(message_thread_id.value()), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"title", title, "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"description", description, "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"payload", payload, "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"provider_token", provider_token, "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"currency", currency, "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"prices", prices_json.dump(), "", ""}));

        if(max_tip_amount.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"max_tip_amount", std::to_string(max_tip_amount.value()), "", ""}));
        }

        if(suggested_tip_amounts.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"suggested_tip_amounts", suggested_tip_amounts_json.dump(), "", ""}));
        }

        if(start_parameter.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"start_parameter", start_parameter.value(), "", ""}));
        }

        if(provider_data.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"provider_data", provider_data.value(), "", ""}));
        }

        if(photo_url.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"photo_url", photo_url.value(), "", ""}));
        }

        if(photo_size.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"photo_size", std::to_string(photo_size.value()), "", ""}));
        }

        if(photo_width.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"photo_width", std::to_string(photo_width.value()), "", ""}));
        }

        if(photo_height.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"photo_height", std::to_string(photo_height.value()), "", ""}));
        }

        if(need_name.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"need_name", std::to_string(static_cast<std::int8_t>(need_name.value())), "", ""}));
        }

        if(need_phone_number.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"need_phone_number", std::to_string(static_cast<std::int8_t>(need_phone_number.value())), "", ""}));
        }

        if(need_email.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"need_email", std::to_string(static_cast<std::int8_t>(need_email.value())), "", ""}));
        }

        if(need_shipping_address.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"need_shipping_address", std::to_string(static_cast<std::int8_t>(need_shipping_address.value())), "",
               ""}));
        }

        if(send_phone_number_to_provider.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"send_phone_number_to_provider",
               std::to_string(static_cast<std::int8_t>(send_phone_number_to_provider.value())), "", ""}));
        }

        if(send_email_to_provider.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"send_email_to_provider", std::to_string(static_cast<std::int8_t>(send_email_to_provider.value())), "",
               ""}));
        }

        if(is_flexible.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"is_flexible", std::to_string(static_cast<std::int8_t>(is_flexible.value())), "", ""}));
        }

        if(disable_notification.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"disable_notification", std::to_string(static_cast<std::int8_t>(disable_notification.value())), "", ""}));
        }

        if(protect_content.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"protect_content", std::to_string(static_cast<std::int8_t>(protect_content.value())), "", ""}));
        }

        if(reply_to_message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_to_message_id", std::to_string(reply_to_message_id.value()), "", ""}));
        }

        if(allow_sending_without_reply.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"allow_sending_without_reply",
               std::to_string(static_cast<std::int8_t>(allow_sending_without_reply.value())), "", ""}));
        }

        if(reply_markup.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_markup", reply_markup.value()->serialise(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/sendInvoice", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<Message>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    Message::ptr Endpoints::sendLocation(
      const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
      const float &latitude, const float &longitude, const std::optional<float> &horizontal_accuracy,
      const std::optional<std::int32_t> &live_period, const std::optional<std::int32_t> &heading,
      const std::optional<std::int32_t> &proximity_alert_radius, const std::optional<bool> &disable_notification,
      const std::optional<bool> &protect_content, const std::optional<std::int32_t> &reply_to_message_id,
      const std::optional<bool> &allow_sending_without_reply, const std::optional<Reply::ptr> &reply_markup) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(message_thread_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_thread_id", std::to_string(message_thread_id.value()), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"latitude", std::to_string(latitude), "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"longitude", std::to_string(longitude), "", ""}));

        if(horizontal_accuracy.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"horizontal_accuracy", std::to_string(horizontal_accuracy.value()), "", ""}));
        }

        if(live_period.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"live_period", std::to_string(live_period.value()), "", ""}));
        }

        if(heading.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"heading", std::to_string(heading.value()), "", ""}));
        }

        if(proximity_alert_radius.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"proximity_alert_radius", std::to_string(proximity_alert_radius.value()), "", ""}));
        }

        if(disable_notification.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"disable_notification", std::to_string(static_cast<std::int8_t>(disable_notification.value())), "", ""}));
        }

        if(protect_content.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"protect_content", std::to_string(static_cast<std::int8_t>(protect_content.value())), "", ""}));
        }

        if(reply_to_message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_to_message_id", std::to_string(reply_to_message_id.value()), "", ""}));
        }

        if(allow_sending_without_reply.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"allow_sending_without_reply",
               std::to_string(static_cast<std::int8_t>(allow_sending_without_reply.value())), "", ""}));
        }

        if(reply_markup.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_markup", reply_markup.value()->serialise(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/sendLocation", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<Message>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    std::vector<Message::ptr> Endpoints::sendMediaGroup(
      const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
      const std::vector<
        std::variant<InputMediaAudio::ptr, InputMediaDocument::ptr, InputMediaPhoto::ptr, InputMediaVideo::ptr>> &media,
      const std::optional<bool> &disable_notification, const std::optional<bool> &protect_content,
      const std::optional<std::int32_t> &reply_to_message_id,
      const std::optional<bool> &allow_sending_without_reply) const
    {
        std::vector<std::string> media_to_upload;
        media_to_upload.reserve(media.size());
        std::vector<std::string> thumbnails_to_upload;
        nlohmann::json media_json = nlohmann::json::array();

        for(std::uint64_t i = 0; i < media.size(); ++i)
        {
            if(std::holds_alternative<InputMediaAudio::ptr>(media.at(i)))
            {
                const InputMediaAudio::ptr &input_media_audio = std::get<InputMediaAudio::ptr>(media.at(i));

                if(std::filesystem::is_regular_file(input_media_audio->get_media()))
                {
                    media_to_upload.emplace_back(input_media_audio->get_media());
                    input_media_audio->set_media(
                      "attach://" + std::filesystem::path(input_media_audio->get_media()).string());
                }

                if(input_media_audio->get_thumbnail().has_value() &&
                   std::filesystem::is_regular_file(input_media_audio->get_thumbnail().value()))
                {
                    thumbnails_to_upload.emplace_back(input_media_audio->get_thumbnail().value());
                    input_media_audio->set_thumbnail(
                      "attach://" + std::filesystem::path(input_media_audio->get_thumbnail().value()).string());
                }

                media_json.emplace_back(nlohmann::json::parse(input_media_audio->serialise()));
            }
            else if(std::holds_alternative<InputMediaDocument::ptr>(media.at(i)))
            {
                const InputMediaDocument::ptr &input_media_document = std::get<InputMediaDocument::ptr>(media.at(i));

                if(std::filesystem::is_regular_file(input_media_document->get_media()))
                {
                    media_to_upload.emplace_back(input_media_document->get_media());
                    input_media_document->set_media(
                      "attach://" + std::filesystem::path(input_media_document->get_media()).string());
                }

                if(input_media_document->get_thumbnail().has_value() &&
                   std::filesystem::is_regular_file(input_media_document->get_thumbnail().value()))
                {
                    thumbnails_to_upload.emplace_back(input_media_document->get_thumbnail().value());
                    input_media_document->set_thumbnail(
                      "attach://" + std::filesystem::path(input_media_document->get_thumbnail().value()).string());
                }

                media_json.emplace_back(nlohmann::json::parse(input_media_document->serialise()));
            }
            else if(std::holds_alternative<InputMediaPhoto::ptr>(media.at(i)))
            {
                const InputMediaPhoto::ptr &input_media_photo = std::get<InputMediaPhoto::ptr>(media.at(i));

                if(std::filesystem::is_regular_file(input_media_photo->get_media()))
                {
                    media_to_upload.emplace_back(input_media_photo->get_media());
                    input_media_photo->set_media(
                      "attach://" + std::filesystem::path(input_media_photo->get_media()).string());
                }

                media_json.emplace_back(nlohmann::json::parse(input_media_photo->serialise()));
            }
            else if(std::holds_alternative<InputMediaVideo::ptr>(media.at(i)))
            {
                const InputMediaVideo::ptr &input_media_video = std::get<InputMediaVideo::ptr>(media.at(i));

                if(std::filesystem::is_regular_file(input_media_video->get_media()))
                {
                    media_to_upload.emplace_back(input_media_video->get_media());
                    input_media_video->set_media(
                      "attach://" + std::filesystem::path(input_media_video->get_media()).string());
                }

                if(input_media_video->get_thumbnail().has_value() &&
                   std::filesystem::is_regular_file(input_media_video->get_thumbnail().value()))
                {
                    thumbnails_to_upload.emplace_back(input_media_video->get_thumbnail().value());
                    input_media_video->set_thumbnail(
                      "attach://" + std::filesystem::path(input_media_video->get_thumbnail().value()).string());
                }

                media_json.emplace_back(nlohmann::json::parse(input_media_video->serialise()));
            }
        }

        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        for(std::uint64_t i = 0; i < media_to_upload.size(); ++i)
        {
            std::ifstream inf(std::filesystem::path(media_to_upload.at(i)).string(), std::ios::binary);
            multipart.emplace_back(httplib::MultipartFormData(
              {std::filesystem::path(media_to_upload.at(i)).string(),
               std::string(std::istreambuf_iterator<char>(inf), {}),
               std::filesystem::path(media_to_upload.at(i)).string(), ""}));
        }

        for(std::uint64_t i = 0; i < thumbnails_to_upload.size(); ++i)
        {
            std::ifstream inf(std::filesystem::path(thumbnails_to_upload.at(i)).string(), std::ios::binary);
            multipart.emplace_back(httplib::MultipartFormData(
              {std::filesystem::path(thumbnails_to_upload.at(i)).string(),
               std::string(std::istreambuf_iterator<char>(inf), {}),
               std::filesystem::path(thumbnails_to_upload.at(i)).string(), ""}));
        }

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(message_thread_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_thread_id", std::to_string(message_thread_id.value()), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"media", media_json.dump(), "", ""}));

        if(disable_notification.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"disable_notification", std::to_string(static_cast<std::int8_t>(disable_notification.value())), "", ""}));
        }

        if(protect_content.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"protect_content", std::to_string(static_cast<std::int8_t>(protect_content.value())), "", ""}));
        }

        if(reply_to_message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_to_message_id", std::to_string(reply_to_message_id.value()), "", ""}));
        }

        if(allow_sending_without_reply.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"allow_sending_without_reply",
               std::to_string(static_cast<std::int8_t>(allow_sending_without_reply.value())), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/sendMediaGroup", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_array())
                {
                    std::vector<Message::ptr> messages;
                    messages.reserve(doc["result"].size());

                    for(std::uint64_t i = 0; i < doc["result"].size(); ++i)
                    {
                        if(doc["result"].at(i).is_object())
                        {
                            messages.emplace_back(std::make_shared<Message>(doc["result"].at(i).dump()));
                        }
                        else
                        {
                            throw std::runtime_error(
                              message::json::value::nested::not_object("result") + "\n" +
                              std::to_string(result->status) + "\n" + result->body);
                        }
                    }

                    return messages;
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_array("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    Message::ptr Endpoints::sendMessage(
      const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
      const std::string &text, const std::optional<std::string> &parse_mode,
      const std::optional<std::vector<MessageEntity::ptr>> &entities,
      const std::optional<bool> &disable_web_page_preview, const std::optional<bool> &disable_notification,
      const std::optional<bool> &protect_content, const std::optional<std::int32_t> &reply_to_message_id,
      const std::optional<bool> &allow_sending_without_reply, const std::optional<Reply::ptr> &reply_markup) const
    {
        nlohmann::json entities_json = nlohmann::json::array();

        if(entities.has_value())
        {
            for(std::uint64_t i = 0; i < entities.value().size(); ++i)
            {
                entities_json.emplace_back(nlohmann::json::parse(entities.value().at(i)->serialise()));
            }
        }

        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(message_thread_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_thread_id", std::to_string(message_thread_id.value()), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"text", text, "", ""}));

        if(parse_mode.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"parse_mode", parse_mode.value(), "", ""}));
        }

        if(entities.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"entities", entities_json.dump(), "", ""}));
        }

        if(disable_web_page_preview.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"disable_web_page_preview", std::to_string(static_cast<std::int8_t>(disable_web_page_preview.value())),
               "", ""}));
        }

        if(disable_notification.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"disable_notification", std::to_string(static_cast<std::int8_t>(disable_notification.value())), "", ""}));
        }

        if(protect_content.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"protect_content", std::to_string(static_cast<std::int8_t>(protect_content.value())), "", ""}));
        }

        if(reply_to_message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_to_message_id", std::to_string(reply_to_message_id.value()), "", ""}));
        }

        if(allow_sending_without_reply.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"allow_sending_without_reply",
               std::to_string(static_cast<std::int8_t>(allow_sending_without_reply.value())), "", ""}));
        }

        if(reply_markup.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_markup", reply_markup.value()->serialise(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/sendMessage", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<Message>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    Message::ptr Endpoints::sendPhoto(
      const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
      const std::string &photo, const std::optional<std::string> &caption, const std::optional<std::string> &parse_mode,
      const std::optional<std::vector<MessageEntity::ptr>> &caption_entities, const std::optional<bool> &has_spoiler,
      const std::optional<bool> &disable_notification, const std::optional<bool> &protect_content,
      const std::optional<std::int32_t> &reply_to_message_id, const std::optional<bool> &allow_sending_without_reply,
      const std::optional<Reply::ptr> &reply_markup) const
    {
        nlohmann::json caption_entities_json = nlohmann::json::array();

        if(caption_entities.has_value())
        {
            for(std::uint64_t i = 0; i < caption_entities.value().size(); ++i)
            {
                caption_entities_json.emplace_back(nlohmann::json::parse(caption_entities.value().at(i)->serialise()));
            }
        }

        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(message_thread_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_thread_id", std::to_string(message_thread_id.value()), "", ""}));
        }

        if(std::filesystem::is_regular_file(photo))
        {
            std::ifstream inf(std::filesystem::path(photo).string(), std::ios::binary);
            multipart.emplace_back(httplib::MultipartFormData(
              {"photo", std::string(std::istreambuf_iterator<char>(inf), {}), std::filesystem::path(photo).string(),
               ""}));
        }
        else
        {
            multipart.emplace_back(httplib::MultipartFormData({"photo", photo, "", ""}));
        }

        if(caption.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"caption", caption.value(), "", ""}));
        }

        if(parse_mode.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"parse_mode", parse_mode.value(), "", ""}));
        }

        if(caption_entities.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"caption_entities", caption_entities_json.dump(), "", ""}));
        }

        if(has_spoiler.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"has_spoiler", std::to_string(static_cast<std::int8_t>(has_spoiler.value())), "", ""}));
        }

        if(disable_notification.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"disable_notification", std::to_string(static_cast<std::int8_t>(disable_notification.value())), "", ""}));
        }

        if(protect_content.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"protect_content", std::to_string(static_cast<std::int8_t>(protect_content.value())), "", ""}));
        }

        if(reply_to_message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_to_message_id", std::to_string(reply_to_message_id.value()), "", ""}));
        }

        if(allow_sending_without_reply.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"allow_sending_without_reply",
               std::to_string(static_cast<std::int8_t>(allow_sending_without_reply.value())), "", ""}));
        }

        if(reply_markup.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_markup", reply_markup.value()->serialise(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/sendPhoto", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<Message>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    Message::ptr Endpoints::sendPoll(
      const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
      const std::string &question, const std::vector<std::string> &options, const std::optional<bool> &is_anonymous,
      const std::optional<std::string> &type, const std::optional<bool> &allows_multiple_answers,
      const std::optional<std::int32_t> &correct_option_id, const std::optional<std::string> &explanation,
      const std::optional<std::string> &explanation_parse_mode,
      const std::optional<std::vector<MessageEntity::ptr>> &explanation_entities,
      const std::optional<std::int32_t> &open_period, const std::optional<std::uint64_t> &close_date,
      const std::optional<bool> &is_closed, const std::optional<bool> &disable_notification,
      const std::optional<bool> &protect_content, const std::optional<std::int32_t> &reply_to_message_id,
      const std::optional<bool> &allow_sending_without_reply, const std::optional<Reply::ptr> &reply_markup) const
    {
        nlohmann::json options_json = nlohmann::json::array();

        for(std::uint64_t i = 0; i < options.size(); ++i)
        {
            options_json.emplace_back(options.at(i));
        }

        nlohmann::json explanation_entities_json = nlohmann::json::array();

        if(explanation_entities.has_value())
        {
            for(std::uint64_t i = 0; i < explanation_entities.value().size(); ++i)
            {
                explanation_entities_json.emplace_back(
                  nlohmann::json::parse(explanation_entities.value().at(i)->serialise()));
            }
        }

        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(message_thread_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_thread_id", std::to_string(message_thread_id.value()), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"question", question, "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"options", options_json.dump(), "", ""}));

        if(is_anonymous.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"is_anonymous", std::to_string(static_cast<std::int8_t>(is_anonymous.value())), "", ""}));
        }

        if(type.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"type", type.value(), "", ""}));
        }

        if(allows_multiple_answers.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"allows_multiple_answers", std::to_string(static_cast<std::int8_t>(allows_multiple_answers.value())), "",
               ""}));
        }

        if(correct_option_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"correct_option_id", std::to_string(correct_option_id.value()), "", ""}));
        }

        if(explanation.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"explanation", explanation.value(), "", ""}));
        }

        if(explanation_parse_mode.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"explanation_parse_mode", explanation_parse_mode.value(), "", ""}));
        }

        if(explanation_entities.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"explanation_entities", explanation_entities_json.dump(), "", ""}));
        }

        if(open_period.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"open_period", std::to_string(open_period.value()), "", ""}));
        }

        if(close_date.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"close_date", std::to_string(close_date.value()), "", ""}));
        }

        if(is_closed.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"is_closed", std::to_string(static_cast<std::int8_t>(is_closed.value())), "", ""}));
        }

        if(disable_notification.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"disable_notification", std::to_string(static_cast<std::int8_t>(disable_notification.value())), "", ""}));
        }

        if(protect_content.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"protect_content", std::to_string(static_cast<std::int8_t>(protect_content.value())), "", ""}));
        }

        if(reply_to_message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_to_message_id", std::to_string(reply_to_message_id.value()), "", ""}));
        }

        if(allow_sending_without_reply.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"allow_sending_without_reply",
               std::to_string(static_cast<std::int8_t>(allow_sending_without_reply.value())), "", ""}));
        }

        if(reply_markup.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_markup", reply_markup.value()->serialise(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/sendPoll", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<Message>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    Message::ptr Endpoints::sendSticker(
      const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
      const std::string &sticker, const std::optional<std::string> &emoji,
      const std::optional<bool> &disable_notification, const std::optional<bool> &protect_content,
      const std::optional<std::int32_t> &reply_to_message_id, const std::optional<bool> &allow_sending_without_reply,
      const std::optional<Reply::ptr> &reply_markup) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(message_thread_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_thread_id", std::to_string(message_thread_id.value()), "", ""}));
        }

        if(std::filesystem::is_regular_file(sticker))
        {
            std::ifstream inf(std::filesystem::path(sticker).string(), std::ios::binary);
            multipart.emplace_back(httplib::MultipartFormData(
              {"sticker", std::string(std::istreambuf_iterator<char>(inf), {}), std::filesystem::path(sticker).string(),
               ""}));
        }
        else
        {
            multipart.emplace_back(httplib::MultipartFormData({"sticker", sticker, "", ""}));
        }

        if(emoji.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"emoji", emoji.value(), "", ""}));
        }

        if(disable_notification.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"disable_notification", std::to_string(static_cast<std::int8_t>(disable_notification.value())), "", ""}));
        }

        if(protect_content.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"protect_content", std::to_string(static_cast<std::int8_t>(protect_content.value())), "", ""}));
        }

        if(reply_to_message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_to_message_id", std::to_string(reply_to_message_id.value()), "", ""}));
        }

        if(allow_sending_without_reply.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"allow_sending_without_reply",
               std::to_string(static_cast<std::int8_t>(allow_sending_without_reply.value())), "", ""}));
        }

        if(reply_markup.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_markup", reply_markup.value()->serialise(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/sendSticker", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<Message>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    Message::ptr Endpoints::sendVenue(
      const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
      const float &latitude, const float &longitude, const std::string &title, const std::string &address,
      const std::optional<std::string> &foursquare_id, const std::optional<std::string> &foursquare_type,
      const std::optional<std::string> &google_place_id, const std::optional<std::string> &google_place_type,
      const std::optional<bool> &disable_notification, const std::optional<bool> &protect_content,
      const std::optional<std::int32_t> &reply_to_message_id, const std::optional<bool> &allow_sending_without_reply,
      const std::optional<Reply::ptr> &reply_markup) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(message_thread_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_thread_id", std::to_string(message_thread_id.value()), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"latitude", std::to_string(latitude), "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"longitude", std::to_string(longitude), "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"title", title, "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"address", address, "", ""}));

        if(foursquare_id.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"foursquare_id", foursquare_id.value(), "", ""}));
        }

        if(foursquare_type.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"foursquare_type", foursquare_type.value(), "", ""}));
        }

        if(google_place_id.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"google_place_id", google_place_id.value(), "", ""}));
        }

        if(google_place_type.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"google_place_type", google_place_type.value(), "", ""}));
        }

        if(disable_notification.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"disable_notification", std::to_string(static_cast<std::int8_t>(disable_notification.value())), "", ""}));
        }

        if(protect_content.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"protect_content", std::to_string(static_cast<std::int8_t>(protect_content.value())), "", ""}));
        }

        if(reply_to_message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_to_message_id", std::to_string(reply_to_message_id.value()), "", ""}));
        }

        if(allow_sending_without_reply.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"allow_sending_without_reply",
               std::to_string(static_cast<std::int8_t>(allow_sending_without_reply.value())), "", ""}));
        }

        if(reply_markup.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_markup", reply_markup.value()->serialise(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/sendVenue", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<Message>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    Message::ptr Endpoints::sendVideo(
      const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
      const std::string &video, const std::optional<std::int32_t> &duration, const std::optional<std::int32_t> &width,
      const std::optional<std::int32_t> &height, const std::optional<std::string> &thumbnail,
      const std::optional<std::string> &caption, const std::optional<std::string> &parse_mode,
      const std::optional<std::vector<MessageEntity::ptr>> &caption_entities, const std::optional<bool> &has_spoiler,
      const std::optional<bool> &supports_streaming, const std::optional<bool> &disable_notification,
      const std::optional<bool> &protect_content, const std::optional<std::int32_t> &reply_to_message_id,
      const std::optional<bool> &allow_sending_without_reply, const std::optional<Reply::ptr> &reply_markup) const
    {
        nlohmann::json caption_entities_json = nlohmann::json::array();

        if(caption_entities.has_value())
        {
            for(std::uint64_t i = 0; i < caption_entities.value().size(); ++i)
            {
                caption_entities_json.emplace_back(nlohmann::json::parse(caption_entities.value().at(i)->serialise()));
            }
        }

        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(message_thread_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_thread_id", std::to_string(message_thread_id.value()), "", ""}));
        }

        if(std::filesystem::is_regular_file(video))
        {
            std::ifstream inf(std::filesystem::path(video).string(), std::ios::binary);
            multipart.emplace_back(httplib::MultipartFormData(
              {"video", std::string(std::istreambuf_iterator<char>(inf), {}), std::filesystem::path(video).string(),
               ""}));
        }
        else
        {
            multipart.emplace_back(httplib::MultipartFormData({"video", video, "", ""}));
        }

        if(duration.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"duration", std::to_string(duration.value()), "", ""}));
        }

        if(width.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"width", std::to_string(width.value()), "", ""}));
        }

        if(height.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"height", std::to_string(height.value()), "", ""}));
        }

        if(thumbnail.has_value())
        {
            if(std::filesystem::is_regular_file(thumbnail.value()))
            {
                std::ifstream inf(std::filesystem::path(thumbnail.value()).string(), std::ios::binary);
                multipart.emplace_back(httplib::MultipartFormData(
                  {std::filesystem::path(thumbnail.value()).string(),
                   std::string(std::istreambuf_iterator<char>(inf), {}),
                   std::filesystem::path(thumbnail.value()).string(), ""}));
                multipart.emplace_back(httplib::MultipartFormData(
                  {"thumbnail", "attach://" + std::filesystem::path(thumbnail.value()).string(), "", ""}));
            }
            else
            {
                multipart.emplace_back(httplib::MultipartFormData({"thumbnail", thumbnail.value(), "", ""}));
            }
        }

        if(caption.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"caption", caption.value(), "", ""}));
        }

        if(parse_mode.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"parse_mode", parse_mode.value(), "", ""}));
        }

        if(caption_entities.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"caption_entities", caption_entities_json.dump(), "", ""}));
        }

        if(has_spoiler.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"has_spoiler", std::to_string(static_cast<std::int8_t>(has_spoiler.value())), "", ""}));
        }

        if(supports_streaming.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"supports_streaming", std::to_string(static_cast<std::int8_t>(supports_streaming.value())), "", ""}));
        }

        if(disable_notification.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"disable_notification", std::to_string(static_cast<std::int8_t>(disable_notification.value())), "", ""}));
        }

        if(protect_content.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"protect_content", std::to_string(static_cast<std::int8_t>(protect_content.value())), "", ""}));
        }

        if(reply_to_message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_to_message_id", std::to_string(reply_to_message_id.value()), "", ""}));
        }

        if(allow_sending_without_reply.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"allow_sending_without_reply",
               std::to_string(static_cast<std::int8_t>(allow_sending_without_reply.value())), "", ""}));
        }

        if(reply_markup.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_markup", reply_markup.value()->serialise(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/sendVideo", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<Message>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    Message::ptr Endpoints::sendVideoNote(
      const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
      const std::string &video_note, const std::optional<std::int32_t> &duration,
      const std::optional<std::int32_t> &length, const std::optional<std::string> &thumbnail,
      const std::optional<bool> &disable_notification, const std::optional<bool> &protect_content,
      const std::optional<std::int32_t> &reply_to_message_id, const std::optional<bool> &allow_sending_without_reply,
      const std::optional<Reply::ptr> &reply_markup) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(message_thread_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_thread_id", std::to_string(message_thread_id.value()), "", ""}));
        }

        if(std::filesystem::is_regular_file(video_note))
        {
            std::ifstream inf(std::filesystem::path(video_note).string(), std::ios::binary);
            multipart.emplace_back(httplib::MultipartFormData(
              {"video_note", std::string(std::istreambuf_iterator<char>(inf), {}),
               std::filesystem::path(video_note).string(), ""}));
        }
        else
        {
            multipart.emplace_back(httplib::MultipartFormData({"video_note", video_note, "", ""}));
        }

        if(duration.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"duration", std::to_string(duration.value()), "", ""}));
        }

        if(length.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"length", std::to_string(length.value()), "", ""}));
        }

        if(thumbnail.has_value())
        {
            if(std::filesystem::is_regular_file(thumbnail.value()))
            {
                std::ifstream inf(std::filesystem::path(thumbnail.value()).string(), std::ios::binary);
                multipart.emplace_back(httplib::MultipartFormData(
                  {std::filesystem::path(thumbnail.value()).string(),
                   std::string(std::istreambuf_iterator<char>(inf), {}),
                   std::filesystem::path(thumbnail.value()).string(), ""}));
                multipart.emplace_back(httplib::MultipartFormData(
                  {"thumbnail", "attach://" + std::filesystem::path(thumbnail.value()).string(), "", ""}));
            }
            else
            {
                multipart.emplace_back(httplib::MultipartFormData({"thumbnail", thumbnail.value(), "", ""}));
            }
        }

        if(disable_notification.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"disable_notification", std::to_string(static_cast<std::int8_t>(disable_notification.value())), "", ""}));
        }

        if(protect_content.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"protect_content", std::to_string(static_cast<std::int8_t>(protect_content.value())), "", ""}));
        }

        if(reply_to_message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_to_message_id", std::to_string(reply_to_message_id.value()), "", ""}));
        }

        if(allow_sending_without_reply.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"allow_sending_without_reply",
               std::to_string(static_cast<std::int8_t>(allow_sending_without_reply.value())), "", ""}));
        }

        if(reply_markup.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_markup", reply_markup.value()->serialise(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/sendVideoNote", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<Message>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    Message::ptr Endpoints::sendVoice(
      const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_thread_id,
      const std::string &voice, const std::optional<std::string> &caption, const std::optional<std::string> &parse_mode,
      const std::optional<std::vector<MessageEntity::ptr>> &caption_entities,
      const std::optional<std::int32_t> &duration, const std::optional<bool> &disable_notification,
      const std::optional<bool> &protect_content, const std::optional<std::int32_t> &reply_to_message_id,
      const std::optional<bool> &allow_sending_without_reply, const std::optional<Reply::ptr> &reply_markup) const
    {
        nlohmann::json caption_entities_json = nlohmann::json::array();

        if(caption_entities.has_value())
        {
            for(std::uint64_t i = 0; i < caption_entities.value().size(); ++i)
            {
                caption_entities_json.emplace_back(nlohmann::json::parse(caption_entities.value().at(i)->serialise()));
            }
        }

        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(message_thread_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_thread_id", std::to_string(message_thread_id.value()), "", ""}));
        }

        if(std::filesystem::is_regular_file(voice))
        {
            std::ifstream inf(std::filesystem::path(voice).string(), std::ios::binary);
            multipart.emplace_back(httplib::MultipartFormData(
              {"voice", std::string(std::istreambuf_iterator<char>(inf), {}), std::filesystem::path(voice).string(),
               ""}));
        }
        else
        {
            multipart.emplace_back(httplib::MultipartFormData({"voice", voice, "", ""}));
        }

        if(caption.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"caption", caption.value(), "", ""}));
        }

        if(parse_mode.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"parse_mode", parse_mode.value(), "", ""}));
        }

        if(caption_entities.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"caption_entities", caption_entities_json.dump(), "", ""}));
        }

        if(duration.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"duration", std::to_string(duration.value()), "", ""}));
        }

        if(disable_notification.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"disable_notification", std::to_string(static_cast<std::int8_t>(disable_notification.value())), "", ""}));
        }

        if(protect_content.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"protect_content", std::to_string(static_cast<std::int8_t>(protect_content.value())), "", ""}));
        }

        if(reply_to_message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_to_message_id", std::to_string(reply_to_message_id.value()), "", ""}));
        }

        if(allow_sending_without_reply.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"allow_sending_without_reply",
               std::to_string(static_cast<std::int8_t>(allow_sending_without_reply.value())), "", ""}));
        }

        if(reply_markup.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_markup", reply_markup.value()->serialise(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/sendVoice", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<Message>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::setChatAdministratorCustomTitle(
      const std::variant<std::int64_t, std::string> &chat_id, const std::int64_t &user_id,
      const std::string &custom_title) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"user_id", std::to_string(user_id), "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"custom_title", custom_title, "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/setChatAdministratorCustomTitle", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::setChatDescription(
      const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::string> &description) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(description.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"description", description.value(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/setChatDescription", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::setChatMenuButton(
      const std::optional<std::int64_t> &chat_id, const std::optional<MenuButton::ptr> &menu_button) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(chat_id.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::to_string(chat_id.value()), "", ""}));
        }

        if(menu_button.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"menu_button", menu_button.value()->serialise(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        std::int32_t result_status = -1;
        std::string result_body;

        if(chat_id.has_value() || menu_button.has_value() || m_test_value.has_value())
        {
            const httplib::Result result = client.Post("/bot" + m_token + "/setChatMenuButton", multipart);

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }
        else
        {
            const httplib::Result result = client.Get("/bot" + m_token + "/setChatMenuButton");

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }

        const nlohmann::json doc = nlohmann::json::parse(result_body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result_status) + "\n" +
                      result_body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result_status) + "\n" +
                  result_body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result_status) + "\n" +
              result_body);
        }
    }

    bool Endpoints::setChatPermissions(
      const std::variant<std::int64_t, std::string> &chat_id, const ChatPermissions::ptr &permissions,
      const std::optional<bool> &use_independent_chat_permissions) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"permissions", permissions->serialise(), "", ""}));

        if(use_independent_chat_permissions.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"use_independent_chat_permissions",
               std::to_string(static_cast<std::int8_t>(use_independent_chat_permissions.value())), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/setChatPermissions", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::setChatPhoto(const std::variant<std::int64_t, std::string> &chat_id, const std::string &photo) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        std::ifstream inf(std::filesystem::path(photo).string(), std::ios::binary);
        multipart.emplace_back(httplib::MultipartFormData(
          {"photo", std::string(std::istreambuf_iterator<char>(inf), {}), std::filesystem::path(photo).string(), ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/setChatPhoto", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::setChatStickerSet(
      const std::variant<std::int64_t, std::string> &chat_id, const std::string &sticker_set_name) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"sticker_set_name", sticker_set_name, "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/setChatStickerSet", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::setChatTitle(const std::variant<std::int64_t, std::string> &chat_id, const std::string &title) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"title", title, "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/setChatTitle", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::setCustomEmojiStickerSetThumbnail(
      const std::string &name, const std::optional<std::string> &custom_emoji_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;
        multipart.emplace_back(httplib::MultipartFormData({"name", name, "", ""}));

        if(custom_emoji_id.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"custom_emoji_id", custom_emoji_id.value(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/setCustomEmojiStickerSetThumbnail", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    std::variant<bool, Message::ptr> Endpoints::setGameScore(
      const std::int64_t &user_id, const std::int32_t &score, const std::optional<bool> &force,
      const std::optional<bool> &disable_edit_message, const std::optional<std::int64_t> &chat_id,
      const std::optional<std::int32_t> &message_id, const std::optional<std::string> &inline_message_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;
        multipart.emplace_back(httplib::MultipartFormData({"user_id", std::to_string(user_id), "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"score", std::to_string(score), "", ""}));

        if(force.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"force", std::to_string(static_cast<std::int8_t>(force.value())), "", ""}));
        }

        if(disable_edit_message.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"disable_edit_message", std::to_string(static_cast<std::int8_t>(disable_edit_message.value())), "", ""}));
        }

        if(chat_id.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::to_string(chat_id.value()), "", ""}));
        }

        if(message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_id", std::to_string(message_id.value()), "", ""}));
        }

        if(inline_message_id.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"inline_message_id", inline_message_id.value(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/setGameScore", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else if(doc["result"].is_object())
                {
                    return std::make_shared<Message>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + ", " + message::json::value::not_object("result") +
                      "\n" + std::to_string(result->status) + "\n" + result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::setMyCommands(
      const std::vector<BotCommand::ptr> &commands, const std::optional<BotCommandScope::ptr> &scope,
      const std::optional<std::string> &language_code) const
    {
        nlohmann::json commands_json = nlohmann::json::array();

        for(std::uint64_t i = 0; i < commands.size(); ++i)
        {
            commands_json.emplace_back(nlohmann::json::parse(commands.at(i)->serialise()));
        }

        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;
        multipart.emplace_back(httplib::MultipartFormData({"commands", commands_json.dump(), "", ""}));

        if(scope.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"scope", scope.value()->serialise(), "", ""}));
        }

        if(language_code.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"language_code", language_code.value(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/setMyCommands", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::setMyDefaultAdministratorRights(
      const std::optional<ChatAdministratorRights::ptr> &rights, const std::optional<bool> &for_channels) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(rights.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"rights", rights.value()->serialise(), "", ""}));
        }

        if(for_channels.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"for_channels", std::to_string(static_cast<std::int8_t>(for_channels.value())), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        std::int32_t result_status = -1;
        std::string result_body;

        if(rights.has_value() || for_channels.has_value() || m_test_value.has_value())
        {
            const httplib::Result result = client.Post("/bot" + m_token + "/setMyDefaultAdministratorRights", multipart);

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }
        else
        {
            const httplib::Result result = client.Get("/bot" + m_token + "/setMyDefaultAdministratorRights");

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }

        const nlohmann::json doc = nlohmann::json::parse(result_body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result_status) + "\n" +
                      result_body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result_status) + "\n" +
                  result_body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result_status) + "\n" +
              result_body);
        }
    }

    bool Endpoints::setMyDescription(
      const std::optional<std::string> &description, const std::optional<std::string> &language_code) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(description.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"description", description.value(), "", ""}));
        }

        if(language_code.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"language_code", language_code.value(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        std::int32_t result_status = -1;
        std::string result_body;

        if(description.has_value() || language_code.has_value() || m_test_value.has_value())
        {
            const httplib::Result result = client.Post("/bot" + m_token + "/setMyDescription", multipart);

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }
        else
        {
            const httplib::Result result = client.Get("/bot" + m_token + "/setMyDescription");

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }

        const nlohmann::json doc = nlohmann::json::parse(result_body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result_status) + "\n" +
                      result_body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result_status) + "\n" +
                  result_body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result_status) + "\n" +
              result_body);
        }
    }

    bool Endpoints::setMyShortDescription(
      const std::optional<std::string> &short_description, const std::optional<std::string> &language_code) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(short_description.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"short_description", short_description.value(), "", ""}));
        }

        if(language_code.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"language_code", language_code.value(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        std::int32_t result_status = -1;
        std::string result_body;

        if(short_description.has_value() || language_code.has_value() || m_test_value.has_value())
        {
            const httplib::Result result = client.Post("/bot" + m_token + "/setMyShortDescription", multipart);

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }
        else
        {
            const httplib::Result result = client.Get("/bot" + m_token + "/setMyShortDescription");

            if(result.error() != httplib::Error::Success)
            {
                throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
            }

            result_status = result->status;
            result_body = result->body;
        }

        const nlohmann::json doc = nlohmann::json::parse(result_body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result_status) + "\n" +
                      result_body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result_status) + "\n" +
                  result_body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result_status) + "\n" +
              result_body);
        }
    }

    bool Endpoints::setPassportDataErrors(
      const std::int64_t &user_id, const std::vector<PassportElementError::ptr> &errors) const
    {
        nlohmann::json errors_json = nlohmann::json::array();

        for(std::uint64_t i = 0; i < errors.size(); ++i)
        {
            errors_json.emplace_back(nlohmann::json::parse(errors.at(i)->serialise()));
        }

        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;
        multipart.emplace_back(httplib::MultipartFormData({"user_id", std::to_string(user_id), "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"errors", errors_json.dump(), "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/setPassportDataErrors", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::setStickerEmojiList(const std::string &sticker, const std::vector<std::string> &emoji_list) const
    {
        nlohmann::json emoji_list_json = nlohmann::json::array();

        for(std::uint64_t i = 0; i < emoji_list.size(); ++i)
        {
            emoji_list_json.emplace_back(emoji_list.at(i));
        }

        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;
        multipart.emplace_back(httplib::MultipartFormData({"sticker", sticker, "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"emoji_list", emoji_list_json.dump(), "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/setStickerEmojiList", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::setStickerKeywords(
      const std::string &sticker, const std::optional<std::vector<std::string>> &keywords) const
    {
        nlohmann::json keywords_json = nlohmann::json::array();

        if(keywords.has_value())
        {
            for(std::uint64_t i = 0; i < keywords.value().size(); ++i)
            {
                keywords_json.emplace_back(keywords.value().at(i));
            }
        }

        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;
        multipart.emplace_back(httplib::MultipartFormData({"sticker", sticker, "", ""}));

        if(keywords.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"keywords", keywords_json.dump(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/setStickerKeywords", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::setStickerMaskPosition(
      const std::string &sticker, const std::optional<MaskPosition::ptr> &mask_position) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;
        multipart.emplace_back(httplib::MultipartFormData({"sticker", sticker, "", ""}));

        if(mask_position.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"mask_position", mask_position.value()->serialise(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/setStickerMaskPosition", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::setStickerPositionInSet(const std::string &sticker, const std::int32_t &position) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;
        multipart.emplace_back(httplib::MultipartFormData({"sticker", sticker, "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"position", std::to_string(position), "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/setStickerPositionInSet", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::setStickerSetThumbnail(
      const std::string &name, const std::int64_t &user_id, const std::optional<std::string> &thumbnail) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;
        multipart.emplace_back(httplib::MultipartFormData({"name", name, "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"user_id", std::to_string(user_id), "", ""}));

        if(thumbnail.has_value())
        {
            if(std::filesystem::is_regular_file(thumbnail.value()))
            {
                std::ifstream inf(std::filesystem::path(thumbnail.value()).string(), std::ios::binary);
                multipart.emplace_back(httplib::MultipartFormData(
                  {"thumbnail", std::string(std::istreambuf_iterator<char>(inf), {}),
                   std::filesystem::path(thumbnail.value()).string(), ""}));
            }
            else
            {
                multipart.emplace_back(httplib::MultipartFormData({"thumbnail", thumbnail.value(), "", ""}));
            }
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/setStickerSetThumbnail", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::setStickerSetTitle(const std::string &name, const std::string &title) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;
        multipart.emplace_back(httplib::MultipartFormData({"name", name, "", ""}));
        multipart.emplace_back(httplib::MultipartFormData({"title", title, "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/setStickerSetTitle", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::setWebhook(
      const std::string &url, const std::optional<std::string> &certificate,
      const std::optional<std::string> &ip_address, const std::optional<std::int32_t> &max_connections,
      const std::optional<std::vector<std::string>> &allowed_updates, const std::optional<bool> &drop_pending_updates,
      const std::optional<std::string> &secret_token) const
    {
        nlohmann::json allowed_updates_json = nlohmann::json::array();

        if(allowed_updates.has_value())
        {
            for(std::uint64_t i = 0; i < allowed_updates.value().size(); ++i)
            {
                allowed_updates_json.emplace_back(allowed_updates.value().at(i));
            }
        }

        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;
        multipart.emplace_back(httplib::MultipartFormData({"url", url, "", ""}));

        if(certificate.has_value())
        {
            std::ifstream inf(std::filesystem::path(certificate.value()).string(), std::ios::binary);
            multipart.emplace_back(httplib::MultipartFormData(
              {"certificate", std::string(std::istreambuf_iterator<char>(inf), {}),
               std::filesystem::path(certificate.value()).string(), ""}));
        }

        if(ip_address.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"ip_address", ip_address.value(), "", ""}));
        }

        if(max_connections.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"max_connections", std::to_string(max_connections.value()), "", ""}));
        }

        if(allowed_updates.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"allowed_updates", allowed_updates_json.dump(), "", ""}));
        }

        if(drop_pending_updates.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"drop_pending_updates", std::to_string(static_cast<std::int8_t>(drop_pending_updates.value())), "", ""}));
        }

        if(secret_token.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"secret_token", secret_token.value(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/setWebhook", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    std::variant<bool, Message::ptr> Endpoints::stopMessageLiveLocation(
      const std::optional<std::variant<std::int64_t, std::string>> &chat_id,
      const std::optional<std::int32_t> &message_id, const std::optional<std::string> &inline_message_id,
      const std::optional<InlineKeyboardMarkup::ptr> &reply_markup) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(chat_id.has_value())
        {
            if(std::holds_alternative<std::int64_t>(chat_id.value()))
            {
                multipart.emplace_back(httplib::MultipartFormData(
                  {"chat_id", std::to_string(std::get<std::int64_t>(chat_id.value())), "", ""}));
            }
            else if(std::holds_alternative<std::string>(chat_id.value()))
            {
                multipart.emplace_back(
                  httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id.value()), "", ""}));
            }
        }

        if(message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_id", std::to_string(message_id.value()), "", ""}));
        }

        if(inline_message_id.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"inline_message_id", inline_message_id.value(), "", ""}));
        }

        if(reply_markup.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_markup", reply_markup.value()->serialise(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/stopMessageLiveLocation", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else if(doc["result"].is_object())
                {
                    return std::make_shared<Message>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + ", " + message::json::value::not_object("result") +
                      "\n" + std::to_string(result->status) + "\n" + result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    Poll::ptr Endpoints::stopPoll(
      const std::variant<std::int64_t, std::string> &chat_id, const std::int32_t &message_id,
      const std::optional<InlineKeyboardMarkup::ptr> &reply_markup) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"message_id", std::to_string(message_id), "", ""}));

        if(reply_markup.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"reply_markup", reply_markup.value()->serialise(), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/stopPoll", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<Poll>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::unbanChatMember(
      const std::variant<std::int64_t, std::string> &chat_id, const std::int64_t &user_id,
      const std::optional<bool> &only_if_banned) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"user_id", std::to_string(user_id), "", ""}));

        if(only_if_banned.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData(
              {"only_if_banned", std::to_string(static_cast<std::int8_t>(only_if_banned.value())), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/unbanChatMember", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::unbanChatSenderChat(
      const std::variant<std::int64_t, std::string> &chat_id, const std::int64_t &sender_chat_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        multipart.emplace_back(httplib::MultipartFormData({"sender_chat_id", std::to_string(sender_chat_id), "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/unbanChatSenderChat", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::unhideGeneralForumTopic(const std::variant<std::int64_t, std::string> &chat_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/unhideGeneralForumTopic", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::unpinAllChatMessages(const std::variant<std::int64_t, std::string> &chat_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/unpinAllChatMessages", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::unpinAllForumTopicMessages(
      const std::variant<std::int64_t, std::string> &chat_id, const std::int32_t &message_thread_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        multipart.emplace_back(
          httplib::MultipartFormData({"message_thread_id", std::to_string(message_thread_id), "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/unpinAllForumTopicMessages", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::unpinChatMessage(
      const std::variant<std::int64_t, std::string> &chat_id, const std::optional<std::int32_t> &message_id) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;

        if(std::holds_alternative<std::int64_t>(chat_id))
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"chat_id", std::to_string(std::get<std::int64_t>(chat_id)), "", ""}));
        }
        else if(std::holds_alternative<std::string>(chat_id))
        {
            multipart.emplace_back(httplib::MultipartFormData({"chat_id", std::get<std::string>(chat_id), "", ""}));
        }

        if(message_id.has_value())
        {
            multipart.emplace_back(
              httplib::MultipartFormData({"message_id", std::to_string(message_id.value()), "", ""}));
        }

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/unpinChatMessage", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_boolean())
                {
                    return doc["result"].get<bool>();
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_bool("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    File::ptr Endpoints::uploadStickerFile(
      const std::int64_t &user_id, const std::string &sticker, const std::string &sticker_format) const
    {
        httplib::SSLClient client(m_host, m_port);
        client.enable_server_certificate_verification(m_certificate_verification);
        httplib::MultipartFormDataItems multipart;
        multipart.emplace_back(httplib::MultipartFormData({"user_id", std::to_string(user_id), "", ""}));

        std::ifstream inf(std::filesystem::path(sticker).string(), std::ios::binary);
        multipart.emplace_back(httplib::MultipartFormData(
          {"sticker", std::string(std::istreambuf_iterator<char>(inf), {}), std::filesystem::path(sticker).string(),
           ""}));

        multipart.emplace_back(httplib::MultipartFormData({"sticker_format", sticker_format, "", ""}));

        if(m_test_value.has_value())
        {
            multipart.emplace_back(httplib::MultipartFormData({"test", m_test_value.value(), "", ""}));
        }

        const httplib::Result result = client.Post("/bot" + m_token + "/uploadStickerFile", multipart);

        if(result.error() != httplib::Error::Success)
        {
            throw std::runtime_error(std::to_string(static_cast<std::int8_t>(result.error())));
        }

        const nlohmann::json doc = nlohmann::json::parse(result->body);

        if(doc.is_object())
        {
            if(doc.contains("result"))
            {
                if(doc["result"].is_object())
                {
                    return std::make_shared<File>(doc["result"].dump());
                }
                else
                {
                    throw std::runtime_error(
                      message::json::value::not_object("result") + "\n" + std::to_string(result->status) + "\n" +
                      result->body);
                }
            }
            else
            {
                throw std::runtime_error(
                  message::json::key_non_existent("result") + "\n" + std::to_string(result->status) + "\n" +
                  result->body);
            }
        }
        else
        {
            throw std::runtime_error(
              message::json::value::not_object(std::nullopt) + "\n" + std::to_string(result->status) + "\n" +
              result->body);
        }
    }

    bool Endpoints::get_certificate_verification() const
    {
        return m_certificate_verification;
    }

    std::string Endpoints::get_host() const
    {
        return m_host;
    }

    std::int32_t Endpoints::get_port() const
    {
        return m_port;
    }

    std::optional<std::string> Endpoints::get_test_value() const
    {
        return m_test_value;
    }

    std::string Endpoints::get_token() const
    {
        return m_token;
    }

    void Endpoints::set_certificate_verification(const bool &certificate_verification)
    {
        m_certificate_verification = certificate_verification;
    }

    void Endpoints::set_host(const std::string &host)
    {
        m_host = host;
    }

    void Endpoints::set_port(const std::int32_t &port)
    {
        m_port = port;
    }

    void Endpoints::set_test_value(const std::optional<std::string> &test_value)
    {
        m_test_value = test_value;
    }

    void Endpoints::set_token(const std::string &token)
    {
        m_token = token;
    }
} //namespace tgbot
