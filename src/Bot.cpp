#include "tgbot/Bot.h"

#include "tgbot/EventHandler.h"

namespace tgbot
{
    Bot::Bot(const std::string &host, const std::int32_t &port, const std::string &token,
             const std::optional<std::string> &test_value, const bool &certificate_verification) :
        m_endpoints(std::make_shared<Endpoints>(host, port, token, test_value, certificate_verification)),
        m_event_handler(std::make_shared<EventHandler>(host, port, token, test_value, certificate_verification))
    {}

    Endpoints::ptr Bot::get_endpoints() const
    {
        return m_endpoints;
    }

    EventHandler::ptr Bot::get_event_handler() const
    {
        return m_event_handler;
    }
} //namespace tgbot
