#include "tgbot/EventHandler.h"

#include <exception>
#include <utility>

#include "tgbot/Bot.h"

namespace tgbot
{
    EventHandler::EventHandler(
      const std::string &host, const std::int32_t &port, const std::string &token,
      const std::optional<std::string> &test_value, const bool &certificate_verification) :
        m_endpoints(std::make_shared<Endpoints>(host, port, token, test_value, certificate_verification))
    {}

    void EventHandler::handle_update(const Bot::ptr &bot, const Update::ptr &update) const
    {
        //Listeners which handle only one update attribute at once.
        if(update->get_callback_query().has_value())
        {
            for(std::uint64_t i = 0; i < m_callback_query_listeners.size(); ++i)
            {
                m_callback_query_listeners.at(i)(bot, update->get_callback_query().value());
            }
        }
        else if(update->get_channel_post().has_value())
        {
            for(std::uint64_t i = 0; i < m_channel_post_listeners.size(); ++i)
            {
                m_channel_post_listeners.at(i)(bot, update->get_channel_post().value());
            }
        }
        else if(update->get_chat_join_request().has_value())
        {
            for(std::uint64_t i = 0; i < m_chat_join_request_listeners.size(); ++i)
            {
                m_chat_join_request_listeners.at(i)(bot, update->get_chat_join_request().value());
            }
        }
        else if(update->get_chat_member().has_value())
        {
            for(std::uint64_t i = 0; i < m_chat_member_listeners.size(); ++i)
            {
                m_my_chat_member_listeners.at(i)(bot, update->get_chat_member().value());
            }
        }
        else if(update->get_chosen_inline_result().has_value())
        {
            for(std::uint64_t i = 0; i < m_chosen_inline_result_listeners.size(); ++i)
            {
                m_chosen_inline_result_listeners.at(i)(bot, update->get_chosen_inline_result().value());
            }
        }
        else if(update->get_edited_channel_post().has_value())
        {
            for(std::uint64_t i = 0; i < m_edited_channel_post_listeners.size(); ++i)
            {
                m_edited_channel_post_listeners.at(i)(bot, update->get_edited_channel_post().value());
            }
        }
        else if(update->get_edited_message().has_value())
        {
            for(std::uint64_t i = 0; i < m_edited_message_listeners.size(); ++i)
            {
                m_edited_message_listeners.at(i)(bot, update->get_edited_message().value());
            }
        }
        else if(update->get_inline_query().has_value())
        {
            for(std::uint64_t i = 0; i < m_inline_query_listeners.size(); ++i)
            {
                m_inline_query_listeners.at(i)(bot, update->get_inline_query().value());
            }
        }
        else if(update->get_message().has_value())
        {
            for(std::uint64_t i = 0; i < m_message_listeners.size(); ++i)
            {
                m_message_listeners.at(i)(bot, update->get_message().value());
            }
        }
        else if(update->get_my_chat_member().has_value())
        {
            for(std::uint64_t i = 0; i < m_my_chat_member_listeners.size(); ++i)
            {
                m_my_chat_member_listeners.at(i)(bot, update->get_my_chat_member().value());
            }
        }
        else if(update->get_poll().has_value())
        {
            for(std::uint64_t i = 0; i < m_poll_listeners.size(); ++i)
            {
                m_poll_listeners.at(i)(bot, update->get_poll().value());
            }
        }
        else if(update->get_poll_answer().has_value())
        {
            for(std::uint64_t i = 0; i < m_poll_answer_listeners.size(); ++i)
            {
                m_poll_answer_listeners.at(i)(bot, update->get_poll_answer().value());
            }
        }
        else if(update->get_pre_checkout_query().has_value())
        {
            for(std::uint64_t i = 0; i < m_pre_checkout_query_listeners.size(); ++i)
            {
                m_pre_checkout_query_listeners.at(i)(bot, update->get_pre_checkout_query().value());
            }
        }
        else if(update->get_shipping_query().has_value())
        {
            for(std::uint64_t i = 0; i < m_shipping_query_listeners.size(); ++i)
            {
                m_shipping_query_listeners.at(i)(bot, update->get_shipping_query().value());
            }
        }

        //Listeners which handle serveral update attributes at once for convenience.
        if(update->get_message().has_value() || update->get_edited_message().has_value() ||
           update->get_channel_post().has_value() || update->get_edited_channel_post().has_value())
        {
            //Get the proper message.
            Message::ptr message;

            if(update->get_channel_post().has_value())
            {
                message = update->get_channel_post().value();
            }
            else if(update->get_edited_channel_post().has_value())
            {
                message = update->get_edited_channel_post().value();
            }
            else if(update->get_edited_message().has_value())
            {
                message = update->get_edited_message().value();
            }
            else if(update->get_message().has_value())
            {
                message = update->get_message().value();
            }

            //Any message
            for(std::uint64_t i = 0; i < m_any_message_listeners.size(); ++i)
            {
                m_any_message_listeners.at(i)(bot, message);
            }

            //Command
            if(message->get_text().has_value() && message->get_text().value().starts_with("/"))
            {
                const std::string command =
                  message->get_text().value().substr(0, message->get_text().value().find(" ")).substr(1);

                //Known command
                if(m_command_listeners.contains(command))
                {
                    m_command_listeners.find(command)->second(bot, message);
                }
                //Unknown command
                else
                {
                    for(std::uint64_t i = 0; i < m_unknown_command_listeners.size(); ++i)
                    {
                        m_unknown_command_listeners.at(i)(bot, message);
                    }
                }
            }
            //Non-command
            else
            {
                for(std::uint64_t i = 0; i < m_non_command_message_listeners.size(); ++i)
                {
                    m_non_command_message_listeners.at(i)(bot, message);
                }
            }
        }
    }

    void EventHandler::long_poll(const Bot::ptr &bot)
    {
        bool throw_again = false;

        try
        {
            const std::vector<Update::ptr> updates =
              m_endpoints->getUpdates(m_offset, std::nullopt, std::nullopt, std::nullopt);
            throw_again = true;

            for(std::uint64_t i = 0; i < updates.size(); ++i)
            {
                handle_update(bot, updates.at(i));
                m_offset = updates.at(i)->get_update_id() + 1;
            }
        }
        catch(const std::exception &exception)
        {
            if(throw_again)
            {
                throw exception;
            }
        }
    }

    void EventHandler::on_any_message(
      const std::function<void(const std::shared_ptr<Bot> &, const Message::ptr &)> &listener_element)
    {
        m_any_message_listeners.emplace_back(listener_element);
    }

    void EventHandler::on_callback_query(
      const std::function<void(const std::shared_ptr<Bot> &, const CallbackQuery::ptr &)> &listener_element)
    {
        m_callback_query_listeners.emplace_back(listener_element);
    }

    void EventHandler::on_channel_post(
      const std::function<void(const std::shared_ptr<Bot> &, const Message::ptr &)> &listener_element)
    {
        m_channel_post_listeners.emplace_back(listener_element);
    }

    void EventHandler::on_chat_join_request(
      const std::function<void(const std::shared_ptr<Bot> &, const ChatJoinRequest::ptr &)> &listener_element)
    {
        m_chat_join_request_listeners.emplace_back(listener_element);
    }

    void EventHandler::on_chat_member(
      const std::function<void(const std::shared_ptr<Bot> &, const ChatMemberUpdated::ptr &)> &listener_element)
    {
        m_chat_member_listeners.emplace_back(listener_element);
    }

    void EventHandler::on_chosen_inline_result(
      const std::function<void(const std::shared_ptr<Bot> &, const ChosenInlineResult::ptr &)> &listener_element)
    {
        m_chosen_inline_result_listeners.emplace_back(listener_element);
    }

    void EventHandler::on_command(
      const std::string &cmd,
      const std::function<void(const std::shared_ptr<Bot> &, const Message::ptr &)> &listener_element)
    {
        m_command_listeners.emplace(cmd, listener_element);
    }

    void EventHandler::on_edited_channel_post(
      const std::function<void(const std::shared_ptr<Bot> &, const Message::ptr &)> &listener_element)
    {
        m_edited_channel_post_listeners.emplace_back(listener_element);
    }

    void EventHandler::on_edited_message(
      const std::function<void(const std::shared_ptr<Bot> &, const Message::ptr &)> &listener_element)
    {
        m_edited_message_listeners.emplace_back(listener_element);
    }

    void EventHandler::on_inline_query(
      const std::function<void(const std::shared_ptr<Bot> &, const InlineQuery::ptr &)> &listener_element)
    {
        m_inline_query_listeners.emplace_back(listener_element);
    }

    void EventHandler::on_message(
      const std::function<void(const std::shared_ptr<Bot> &, const Message::ptr &)> &listener_element)
    {
        m_message_listeners.emplace_back(listener_element);
    }

    void EventHandler::on_my_chat_member(
      const std::function<void(const std::shared_ptr<Bot> &, const ChatMemberUpdated::ptr &)> &listener_element)
    {
        m_my_chat_member_listeners.emplace_back(listener_element);
    }

    void EventHandler::on_non_command_message(
      const std::function<void(const std::shared_ptr<Bot> &, const Message::ptr &)> &listener_element)
    {
        m_non_command_message_listeners.emplace_back(listener_element);
    }

    void EventHandler::on_poll(
      const std::function<void(const std::shared_ptr<Bot> &, const Poll::ptr &)> &listener_element)
    {
        m_poll_listeners.emplace_back(listener_element);
    }

    void EventHandler::on_poll_answer(
      const std::function<void(const std::shared_ptr<Bot> &, const PollAnswer::ptr &)> &listener_element)
    {
        m_poll_answer_listeners.emplace_back(listener_element);
    }

    void EventHandler::on_pre_checkout_query(
      const std::function<void(const std::shared_ptr<Bot> &, const PreCheckoutQuery::ptr &)> &listener_element)
    {
        m_pre_checkout_query_listeners.emplace_back(listener_element);
    }

    void EventHandler::on_shipping_query(
      const std::function<void(const std::shared_ptr<Bot> &, const ShippingQuery::ptr &)> &listener_element)
    {
        m_shipping_query_listeners.emplace_back(listener_element);
    }

    void EventHandler::on_unknown_command(
      const std::function<void(const std::shared_ptr<Bot> &, const Message::ptr &)> &listener_element)
    {
        m_unknown_command_listeners.emplace_back(listener_element);
    }

    std::vector<std::function<void(const Bot::ptr &, const Message::ptr &)>> EventHandler::get_any_message_listeners()
    {
        return m_any_message_listeners;
    }

    std::vector<std::function<void(const Bot::ptr &, const CallbackQuery::ptr &)>>
      EventHandler::get_callback_query_listeners()
    {
        return m_callback_query_listeners;
    }

    std::vector<std::function<void(const std::shared_ptr<Bot> &, const Message::ptr &)>>
      EventHandler::get_channel_post_listeners()
    {
        return m_channel_post_listeners;
    }

    std::vector<std::function<void(const Bot::ptr &, const ChatJoinRequest::ptr &)>>
      EventHandler::get_chat_join_request_listeners()
    {
        return m_chat_join_request_listeners;
    }

    std::vector<std::function<void(const Bot::ptr &, const ChatMemberUpdated::ptr &)>>
      EventHandler::get_chat_member_listeners()
    {
        return m_chat_member_listeners;
    }

    std::vector<std::function<void(const Bot::ptr &, const ChosenInlineResult::ptr &)>>
      EventHandler::get_chosen_inline_result_listeners()
    {
        return m_chosen_inline_result_listeners;
    }

    std::unordered_map<std::string, std::function<void(const Bot::ptr &, const Message::ptr &)>>
      EventHandler::get_command_listeners()
    {
        return m_command_listeners;
    }

    std::vector<std::function<void(const std::shared_ptr<Bot> &, const Message::ptr &)>>
      EventHandler::get_edited_channel_post_listeners()
    {
        return m_edited_channel_post_listeners;
    }

    std::vector<std::function<void(const std::shared_ptr<Bot> &, const Message::ptr &)>>
      EventHandler::get_edited_message_listeners()
    {
        return m_edited_message_listeners;
    }

    Endpoints::ptr EventHandler::get_endpoints() const
    {
        return m_endpoints;
    }

    std::vector<std::function<void(const Bot::ptr &, const InlineQuery::ptr &)>>
      EventHandler::get_inline_query_listeners()
    {
        return m_inline_query_listeners;
    }

    std::vector<std::function<void(const std::shared_ptr<Bot> &, const Message::ptr &)>>
      EventHandler::get_message_listeners()
    {
        return m_message_listeners;
    }

    std::vector<std::function<void(const Bot::ptr &, const ChatMemberUpdated::ptr &)>>
      EventHandler::get_my_chat_member_listeners()
    {
        return m_my_chat_member_listeners;
    }

    std::vector<std::function<void(const Bot::ptr &, const Message::ptr &)>>
      EventHandler::get_non_command_message_listeners()
    {
        return m_non_command_message_listeners;
    }

    std::vector<std::function<void(const Bot::ptr &, const PollAnswer::ptr &)>> EventHandler::get_poll_answer_listeners()
    {
        return m_poll_answer_listeners;
    }

    std::vector<std::function<void(const Bot::ptr &, const Poll::ptr &)>> EventHandler::get_poll_listeners()
    {
        return m_poll_listeners;
    }

    std::vector<std::function<void(const Bot::ptr &, const PreCheckoutQuery::ptr &)>>
      EventHandler::get_pre_checkout_query_listeners()
    {
        return m_pre_checkout_query_listeners;
    }

    std::vector<std::function<void(const Bot::ptr &, const ShippingQuery::ptr &)>>
      EventHandler::get_shipping_query_listeners()
    {
        return m_shipping_query_listeners;
    }

    std::vector<std::function<void(const Bot::ptr &, const Message::ptr &)>>
      EventHandler::get_unknown_command_listeners()
    {
        return m_unknown_command_listeners;
    }
} //namespace tgbot
