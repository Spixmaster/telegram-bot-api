#include "tgbot/constants/message.h"

#include <boost/locale/message.hpp>
#include <fmt/core.h>

namespace tgbot::message::json
{
    std::string key_non_existent(const std::string &key)
    {
        return fmt::vformat(
          boost::locale::translate("The key \"{}\" does not exist.").str(), fmt::make_format_args(key));
    }

    namespace value
    {
        std::string not_array(const std::optional<std::string> &key)
        {
            return key.has_value() ?
                     fmt::vformat(boost::locale::translate("The value of key \"{}\" is not an array.").str(),
                                  fmt::make_format_args(key.value())) :
                     boost::locale::translate("The value is not an array.");
        }

        std::string not_bool(const std::string &key)
        {
            return fmt::vformat(
              boost::locale::translate("The value of key \"{}\" is not a boolean.").str(), fmt::make_format_args(key));
        }

        std::string not_float(const std::string &key)
        {
            return fmt::vformat(
              boost::locale::translate("The value of key \"{}\" is not a floating point number.").str(),
              fmt::make_format_args(key));
        }

        std::string not_int(const std::string &key)
        {
            return fmt::vformat(
              boost::locale::translate("The value of key \"{}\" is not an integer.").str(), fmt::make_format_args(key));
        }

        std::string not_object(const std::optional<std::string> &key)
        {
            return key.has_value() ?
                     fmt::vformat(boost::locale::translate("The value of key \"{}\" is not an object.").str(),
                                  fmt::make_format_args(key.value())) :
                     boost::locale::translate("The value is not an object.");
        }

        std::string not_string(const std::string &key)
        {
            return fmt::vformat(
              boost::locale::translate("The value of key \"{}\" is not a string.").str(), fmt::make_format_args(key));
        }

        std::string unexpected(const std::string &key)
        {
            return fmt::vformat(
              boost::locale::translate("The value of key \"{}\" is unexpected.").str(), fmt::make_format_args(key));
        }

        namespace nested
        {
            std::string not_array(const std::string &key)
            {
                return fmt::vformat(
                  boost::locale::translate("The nested value of key \"{}\" is not an array.").str(),
                  fmt::make_format_args(key));
            }

            std::string not_int(const std::string &key)
            {
                return fmt::vformat(
                  boost::locale::translate("The nested value of key \"{}\" is not an integer.").str(),
                  fmt::make_format_args(key));
            }

            std::string not_object(const std::string &key)
            {
                return fmt::vformat(
                  boost::locale::translate("The nested value of key \"{}\" is not an object.").str(),
                  fmt::make_format_args(key));
            }

            std::string not_string(const std::string &key)
            {
                return fmt::vformat(
                  boost::locale::translate("The nested value of key \"{}\" is not a string.").str(),
                  fmt::make_format_args(key));
            }
        } //namespace nested
    } //namespace value
} //namespace tgbot::message::json
