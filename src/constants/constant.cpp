#include "tgbot/constants/constant.h"

#include <string>

namespace tgbot::constant
{
    const std::string release = "23.Mar.2023";
    const std::string version = "3.2.1";
} //namespace tgbot::constant
