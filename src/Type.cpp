#include "tgbot/Type.h"

#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>

namespace tgbot
{
    Type::Type() = default;

    Type::Type(Type &) = default;

    Type::Type(Type &&) noexcept = default;

    Type &Type::operator=(const Type &) = default;

    Type &Type::operator=(Type &&) noexcept = default;

    Type::~Type() = default;

    std::string Type::serialise() const
    {
        const nlohmann::json doc = nlohmann::json::object();
        return doc.dump();
    }
} //namespace tgbot
