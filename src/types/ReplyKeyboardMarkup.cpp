#include "tgbot/types/ReplyKeyboardMarkup.h"

#include <algorithm>
#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <utility>

#include "tgbot/constants/message.h"

namespace tgbot
{
    ReplyKeyboardMarkup::ReplyKeyboardMarkup() = default;

    ReplyKeyboardMarkup::ReplyKeyboardMarkup(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("input_field_placeholder"))
            {
                if(doc["input_field_placeholder"].is_string())
                {
                    m_input_field_placeholder = doc["input_field_placeholder"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("input_field_placeholder"));
                }
            }

            if(doc.contains("is_persistent"))
            {
                if(doc["is_persistent"].is_boolean())
                {
                    m_is_persistent = doc["is_persistent"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("is_persistent"));
                }
            }

            if(doc.contains("keyboard"))
            {
                if(doc["keyboard"].is_array())
                {
                    std::vector<std::vector<KeyboardButton::ptr>> keyboard;
                    keyboard.resize(doc["keyboard"].size());

                    for(std::uint64_t i = 0; i < doc["keyboard"].size(); ++i)
                    {
                        if(doc["keyboard"].at(i).is_array())
                        {
                            keyboard.at(i).reserve(doc["keyboard"].at(i).size());

                            for(std::uint64_t j = 0; j < doc["keyboard"].at(i).size(); ++j)
                            {
                                if(doc["keyboard"].at(i).at(j).is_object())
                                {
                                    keyboard.at(i).emplace_back(
                                      std::make_shared<KeyboardButton>(doc["keyboard"].at(i).at(j).dump()));
                                }
                                else
                                {
                                    throw std::invalid_argument(message::json::value::nested::not_object("keyboard"));
                                }
                            }
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_array("keyboard"));
                        }
                    }

                    m_keyboard = keyboard;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("keyboard"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("keyboard"));
            }

            if(doc.contains("one_time_keyboard"))
            {
                if(doc["one_time_keyboard"].is_boolean())
                {
                    m_one_time_keyboard = doc["one_time_keyboard"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("one_time_keyboard"));
                }
            }

            if(doc.contains("resize_keyboard"))
            {
                if(doc["resize_keyboard"].is_boolean())
                {
                    m_resize_keyboard = doc["resize_keyboard"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("resize_keyboard"));
                }
            }

            if(doc.contains("selective"))
            {
                if(doc["selective"].is_boolean())
                {
                    m_selective = doc["selective"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("selective"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    ReplyKeyboardMarkup::ReplyKeyboardMarkup(std::vector<std::vector<KeyboardButton::ptr>> keyboard) :
        m_keyboard(std::move(keyboard))
    {}

    std::string ReplyKeyboardMarkup::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_input_field_placeholder.has_value())
        {
            doc["input_field_placeholder"] = m_input_field_placeholder.value();
        }

        if(m_is_persistent.has_value())
        {
            doc["is_persistent"] = m_is_persistent.value();
        }

        {
            nlohmann::json keyboard = nlohmann::json::array();

            if(m_keyboard.empty())
            {
                keyboard.emplace_back(nlohmann::json::array());
            }
            else
            {
                for(std::uint64_t i = 0; i < m_keyboard.size(); ++i)
                {
                    nlohmann::json keyboard_row = nlohmann::json::array();

                    for(std::uint64_t j = 0; j < m_keyboard.at(i).size(); ++j)
                    {
                        keyboard_row.emplace_back(nlohmann::json::parse(m_keyboard.at(i).at(j)->serialise()));
                    }

                    keyboard.emplace_back(keyboard_row);
                }
            }

            doc["keyboard"] = keyboard;
        }

        if(m_one_time_keyboard.has_value())
        {
            doc["one_time_keyboard"] = m_one_time_keyboard.value();
        }

        if(m_resize_keyboard.has_value())
        {
            doc["resize_keyboard"] = m_resize_keyboard.value();
        }

        if(m_selective.has_value())
        {
            doc["selective"] = m_selective.value();
        }

        return doc.dump();
    }

    std::optional<std::string> ReplyKeyboardMarkup::get_input_field_placeholder() const
    {
        return m_input_field_placeholder;
    }

    std::optional<bool> ReplyKeyboardMarkup::get_is_persistent() const
    {
        return m_is_persistent;
    }

    std::vector<std::vector<KeyboardButton::ptr>> ReplyKeyboardMarkup::get_keyboard() const
    {
        return m_keyboard;
    }

    std::optional<bool> ReplyKeyboardMarkup::get_one_time_keyboard() const
    {
        return m_one_time_keyboard;
    }

    std::optional<bool> ReplyKeyboardMarkup::get_resize_keyboard() const
    {
        return m_resize_keyboard;
    }

    std::optional<bool> ReplyKeyboardMarkup::get_selective() const
    {
        return m_selective;
    }

    void ReplyKeyboardMarkup::set_input_field_placeholder(const std::optional<std::string> &input_field_placeholder)
    {
        m_input_field_placeholder = input_field_placeholder;
    }

    void ReplyKeyboardMarkup::set_is_persistent(const std::optional<bool> &is_persistent)
    {
        m_is_persistent = is_persistent;
    }

    void ReplyKeyboardMarkup::set_keyboard(const std::vector<std::vector<KeyboardButton::ptr>> &keyboard)
    {
        m_keyboard = keyboard;
    }

    void ReplyKeyboardMarkup::set_one_time_keyboard(const std::optional<bool> &one_time_keyboard)
    {
        m_one_time_keyboard = one_time_keyboard;
    }

    void ReplyKeyboardMarkup::set_resize_keyboard(const std::optional<bool> &resize_keyboard)
    {
        m_resize_keyboard = resize_keyboard;
    }

    void ReplyKeyboardMarkup::set_selective(const std::optional<bool> &selective)
    {
        m_selective = selective;
    }
} //namespace tgbot
