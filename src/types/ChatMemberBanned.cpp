#include "tgbot/types/ChatMemberBanned.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    ChatMemberBanned::ChatMemberBanned() : m_until_date(1), m_user(std::make_shared<User>())
    {}

    ChatMemberBanned::ChatMemberBanned(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("status"))
            {
                if(doc["status"].is_string())
                {
                    m_status = doc["status"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("status"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("status"));
            }

            if(doc.contains("until_date"))
            {
                if(doc["until_date"].is_number_integer())
                {
                    m_until_date = doc["until_date"].get<std::uint64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("until_date"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("until_date"));
            }

            if(doc.contains("user"))
            {
                if(doc["user"].is_object())
                {
                    m_user = std::make_shared<User>(doc["user"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("user"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("user"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string ChatMemberBanned::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["status"] = m_status;
        doc["until_date"] = m_until_date;
        doc["user"] = nlohmann::json::parse(m_user->serialise());
        return doc.dump();
    }

    std::string ChatMemberBanned::get_status() const
    {
        return m_status;
    }

    std::uint64_t ChatMemberBanned::get_until_date() const
    {
        return m_until_date;
    }

    User::ptr ChatMemberBanned::get_user() const
    {
        return m_user;
    }

    void ChatMemberBanned::set_status(const std::string &status)
    {
        m_status = status;
    }

    void ChatMemberBanned::set_until_date(const std::uint64_t &until_date)
    {
        m_until_date = until_date;
    }

    void ChatMemberBanned::set_user(const User::ptr &user)
    {
        m_user = user;
    }
} //namespace tgbot
