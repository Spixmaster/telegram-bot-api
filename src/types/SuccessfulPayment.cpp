#include "tgbot/types/SuccessfulPayment.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    SuccessfulPayment::SuccessfulPayment() : m_total_amount(-1)
    {}

    SuccessfulPayment::SuccessfulPayment(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("currency"))
            {
                if(doc["currency"].is_string())
                {
                    m_currency = doc["currency"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("currency"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("currency"));
            }

            if(doc.contains("invoice_payload"))
            {
                if(doc["invoice_payload"].is_string())
                {
                    m_invoice_payload = doc["invoice_payload"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("invoice_payload"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("invoice_payload"));
            }

            if(doc.contains("order_info"))
            {
                if(doc["order_info"].is_object())
                {
                    m_order_info = std::make_shared<OrderInfo>(doc["order_info"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("order_info"));
                }
            }

            if(doc.contains("provider_payment_charge_id"))
            {
                if(doc["provider_payment_charge_id"].is_string())
                {
                    m_provider_payment_charge_id = doc["provider_payment_charge_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("provider_payment_charge_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("provider_payment_charge_id"));
            }

            if(doc.contains("shipping_option_id"))
            {
                if(doc["shipping_option_id"].is_string())
                {
                    m_shipping_option_id = doc["shipping_option_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("shipping_option_id"));
                }
            }

            if(doc.contains("telegram_payment_charge_id"))
            {
                if(doc["telegram_payment_charge_id"].is_string())
                {
                    m_telegram_payment_charge_id = doc["telegram_payment_charge_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("telegram_payment_charge_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("telegram_payment_charge_id"));
            }

            if(doc.contains("total_amount"))
            {
                if(doc["total_amount"].is_number_integer())
                {
                    m_total_amount = doc["total_amount"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("total_amount"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("total_amount"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string SuccessfulPayment::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["currency"] = m_currency;
        doc["invoice_payload"] = m_invoice_payload;

        if(m_order_info.has_value())
        {
            doc["order_info"] = nlohmann::json::parse(m_order_info.value()->serialise());
        }

        doc["provider_payment_charge_id"] = m_provider_payment_charge_id;

        if(m_shipping_option_id.has_value())
        {
            doc["shipping_option_id"] = m_shipping_option_id.value();
        }

        doc["telegram_payment_charge_id"] = m_telegram_payment_charge_id;
        doc["total_amount"] = m_total_amount;
        return doc.dump();
    }

    std::string SuccessfulPayment::get_currency() const
    {
        return m_currency;
    }

    std::string SuccessfulPayment::get_invoice_payload() const
    {
        return m_invoice_payload;
    }

    std::optional<OrderInfo::ptr> SuccessfulPayment::get_order_info() const
    {
        return m_order_info;
    }

    std::string SuccessfulPayment::get_provider_payment_charge_id() const
    {
        return m_provider_payment_charge_id;
    }

    std::optional<std::string> SuccessfulPayment::get_shipping_option_id() const
    {
        return m_shipping_option_id;
    }

    std::string SuccessfulPayment::get_telegram_payment_charge_id() const
    {
        return m_telegram_payment_charge_id;
    }

    std::int32_t SuccessfulPayment::get_total_amount() const
    {
        return m_total_amount;
    }

    void SuccessfulPayment::set_currency(const std::string &currency)
    {
        m_currency = currency;
    }

    void SuccessfulPayment::set_invoice_payload(const std::string &invoice_payload)
    {
        m_invoice_payload = invoice_payload;
    }

    void SuccessfulPayment::set_order_info(const std::optional<OrderInfo::ptr> &order_info)
    {
        m_order_info = order_info;
    }

    void SuccessfulPayment::set_provider_payment_charge_id(const std::string &provider_payment_charge_id)
    {
        m_provider_payment_charge_id = provider_payment_charge_id;
    }

    void SuccessfulPayment::set_shipping_option_id(const std::optional<std::string> &shipping_option_id)
    {
        m_shipping_option_id = shipping_option_id;
    }

    void SuccessfulPayment::set_telegram_payment_charge_id(const std::string &telegram_payment_charge_id)
    {
        m_telegram_payment_charge_id = telegram_payment_charge_id;
    }

    void SuccessfulPayment::set_total_amount(const std::int32_t &total_amount)
    {
        m_total_amount = total_amount;
    }
} //namespace tgbot
