#include "tgbot/types/SentWebAppMessage.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    SentWebAppMessage::SentWebAppMessage() = default;

    SentWebAppMessage::SentWebAppMessage(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("inline_message_id"))
            {
                if(doc["inline_message_id"].is_string())
                {
                    m_inline_message_id = doc["inline_message_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("inline_message_id"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string SentWebAppMessage::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_inline_message_id.has_value())
        {
            doc["inline_message_id"] = m_inline_message_id.value();
        }

        return doc.dump();
    }

    std::optional<std::string> SentWebAppMessage::get_inline_message_id() const
    {
        return m_inline_message_id;
    }

    void SentWebAppMessage::set_inline_message_id(const std::optional<std::string> &inline_message_id)
    {
        m_inline_message_id = inline_message_id;
    }
} //namespace tgbot
