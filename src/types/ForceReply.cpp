#include "tgbot/types/ForceReply.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    ForceReply::ForceReply() : m_force_reply(false)
    {}

    ForceReply::ForceReply(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("force_reply"))
            {
                if(doc["force_reply"].is_boolean())
                {
                    m_force_reply = doc["force_reply"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("force_reply"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("force_reply"));
            }

            if(doc.contains("input_field_placeholder"))
            {
                if(doc["input_field_placeholder"].is_string())
                {
                    m_input_field_placeholder = doc["input_field_placeholder"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("input_field_placeholder"));
                }
            }

            if(doc.contains("selective"))
            {
                if(doc["selective"].is_boolean())
                {
                    m_selective = doc["selective"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("selective"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string ForceReply::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["force_reply"] = m_force_reply;

        if(m_input_field_placeholder.has_value())
        {
            doc["input_field_placeholder"] = m_input_field_placeholder.value();
        }

        if(m_selective.has_value())
        {
            doc["selective"] = m_selective.value();
        }

        return doc.dump();
    }

    bool ForceReply::get_force_reply() const
    {
        return m_force_reply;
    }

    std::optional<std::string> ForceReply::get_input_field_placeholder() const
    {
        return m_input_field_placeholder;
    }

    std::optional<bool> ForceReply::get_selective() const
    {
        return m_selective;
    }

    void ForceReply::set_force_reply(const bool &force_reply)
    {
        m_force_reply = force_reply;
    }

    void ForceReply::set_input_field_placeholder(const std::optional<std::string> &input_field_placeholder)
    {
        m_input_field_placeholder = input_field_placeholder;
    }

    void ForceReply::set_selective(const std::optional<bool> &selective)
    {
        m_selective = selective;
    }
} //namespace tgbot
