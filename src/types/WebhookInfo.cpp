#include "tgbot/types/WebhookInfo.h"

#include <algorithm>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "tgbot/constants/message.h"

namespace tgbot
{
    WebhookInfo::WebhookInfo() : m_has_custom_certificate(false), m_pending_update_count(-1)
    {}

    WebhookInfo::WebhookInfo(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("allowed_updates"))
            {
                if(doc["allowed_updates"].is_array())
                {
                    std::vector<std::string> allowed_updates;
                    allowed_updates.reserve(doc["allowed_updates"].size());

                    for(std::uint64_t i = 0; i < doc["allowed_updates"].size(); ++i)
                    {
                        if(doc["allowed_updates"].at(i).is_string())
                        {
                            allowed_updates.emplace_back(doc["allowed_updates"].at(i).get<std::string>());
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_string(
                              "allowed_"
                              "updates"));
                        }
                    }

                    m_allowed_updates = allowed_updates;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("allowed_updates"));
                }
            }

            if(doc.contains("has_custom_certificate"))
            {
                if(doc["has_custom_certificate"].is_boolean())
                {
                    m_has_custom_certificate = doc["has_custom_certificate"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("has_custom_certificate"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("has_custom_certificate"));
            }

            if(doc.contains("ip_address"))
            {
                if(doc["ip_address"].is_string())
                {
                    m_ip_address = doc["ip_address"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("ip_address"));
                }
            }

            if(doc.contains("last_error_date"))
            {
                if(doc["last_error_date"].is_number_integer())
                {
                    m_last_error_date = doc["last_error_date"].get<std::uint64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("last_error_date"));
                }
            }

            if(doc.contains("last_error_message"))
            {
                if(doc["last_error_message"].is_string())
                {
                    m_last_error_message = doc["last_error_message"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("last_error_message"));
                }
            }

            if(doc.contains("last_synchronization_error_date"))
            {
                if(doc["last_synchronization_error_date"].is_number_integer())
                {
                    m_last_synchronization_error_date = doc["last_synchronization_error_date"].get<std::uint64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int(
                      "last_synchronization_error_"
                      "date"));
                }
            }

            if(doc.contains("max_connections"))
            {
                if(doc["max_connections"].is_number_integer())
                {
                    m_max_connections = doc["max_connections"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("max_connections"));
                }
            }

            if(doc.contains("pending_update_count"))
            {
                if(doc["pending_update_count"].is_number_integer())
                {
                    m_pending_update_count = doc["pending_update_count"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("pending_update_count"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("pending_update_count"));
            }

            if(doc.contains("url"))
            {
                if(doc["url"].is_string())
                {
                    m_url = doc["url"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("url"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("url"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string WebhookInfo::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_allowed_updates.has_value())
        {
            nlohmann::json allowed_updates = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_allowed_updates.value().size(); ++i)
            {
                allowed_updates.emplace_back(m_allowed_updates.value().at(i));
            }

            doc["allowed_updates"] = allowed_updates;
        }

        doc["has_custom_certificate"] = m_has_custom_certificate;

        if(m_ip_address.has_value())
        {
            doc["ip_address"] = m_ip_address.value();
        }

        if(m_last_error_date.has_value())
        {
            doc["last_error_date"] = m_last_error_date.value();
        }

        if(m_last_error_message.has_value())
        {
            doc["last_error_message"] = m_last_error_message.value();
        }

        if(m_last_synchronization_error_date.has_value())
        {
            doc["last_synchronization_error_date"] = m_last_synchronization_error_date.value();
        }

        if(m_max_connections.has_value())
        {
            doc["max_connections"] = m_max_connections.value();
        }

        doc["pending_update_count"] = m_pending_update_count;
        doc["url"] = m_url;
        return doc.dump();
    }

    std::optional<std::vector<std::string>> WebhookInfo::get_allowed_updates() const
    {
        return m_allowed_updates;
    }

    bool WebhookInfo::get_has_custom_certificate() const
    {
        return m_has_custom_certificate;
    }

    std::optional<std::string> WebhookInfo::get_ip_address() const
    {
        return m_ip_address;
    }

    std::optional<std::uint64_t> WebhookInfo::get_last_error_date() const
    {
        return m_last_error_date;
    }

    std::optional<std::string> WebhookInfo::get_last_error_message() const
    {
        return m_last_error_message;
    }

    std::optional<std::uint64_t> WebhookInfo::get_last_synchronization_error_date() const
    {
        return m_last_synchronization_error_date;
    }

    std::optional<std::int32_t> WebhookInfo::get_max_connections() const
    {
        return m_max_connections;
    }

    std::int32_t WebhookInfo::get_pending_update_count() const
    {
        return m_pending_update_count;
    }

    std::string WebhookInfo::get_url() const
    {
        return m_url;
    }

    void WebhookInfo::set_allowed_updates(const std::optional<std::vector<std::string>> &allowed_updates)
    {
        m_allowed_updates = allowed_updates;
    }

    void WebhookInfo::set_has_custom_certificate(const bool &has_custom_certificate)
    {
        m_has_custom_certificate = has_custom_certificate;
    }

    void WebhookInfo::set_ip_address(const std::optional<std::string> &ip_address)
    {
        m_ip_address = ip_address;
    }

    void WebhookInfo::set_last_error_date(const std::optional<std::uint64_t> &last_error_date)
    {
        m_last_error_date = last_error_date;
    }

    void WebhookInfo::set_last_error_message(const std::optional<std::string> &last_error_message)
    {
        m_last_error_message = last_error_message;
    }

    void WebhookInfo::set_last_synchronization_error_date(
      const std::optional<std::uint64_t> &last_synchronization_error_date)
    {
        m_last_synchronization_error_date = last_synchronization_error_date;
    }

    void WebhookInfo::set_max_connections(const std::optional<std::int32_t> &max_connections)
    {
        m_max_connections = max_connections;
    }

    void WebhookInfo::set_pending_update_count(const std::int32_t &pending_update_count)
    {
        m_pending_update_count = pending_update_count;
    }

    void WebhookInfo::set_url(const std::string &url)
    {
        m_url = url;
    }
} //namespace tgbot
