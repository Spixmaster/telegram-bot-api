#include "tgbot/types/ChatJoinRequest.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    ChatJoinRequest::ChatJoinRequest() :
        m_chat(std::make_shared<Chat>()), m_date(1), m_from(std::make_shared<User>()), m_user_chat_id(-1)
    {}

    ChatJoinRequest::ChatJoinRequest(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("bio"))
            {
                if(doc["bio"].is_string())
                {
                    m_bio = doc["bio"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("bio"));
                }
            }

            if(doc.contains("chat"))
            {
                if(doc["chat"].is_object())
                {
                    m_chat = std::make_shared<Chat>(doc["chat"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("chat"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("chat"));
            }

            if(doc.contains("date"))
            {
                if(doc["date"].is_number_integer())
                {
                    m_date = doc["date"].get<std::uint64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("date"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("date"));
            }

            if(doc.contains("from"))
            {
                if(doc["from"].is_object())
                {
                    m_from = std::make_shared<User>(doc["from"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("from"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("from"));
            }

            if(doc.contains("invite_link"))
            {
                if(doc["invite_link"].is_object())
                {
                    m_invite_link = std::make_shared<ChatInviteLink>(doc["invite_link"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("invite_link"));
                }
            }

            if(doc.contains("user_chat_id"))
            {
                if(doc["user_chat_id"].is_number_integer())
                {
                    m_user_chat_id = doc["user_chat_id"].get<std::int64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("user_chat_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("user_chat_id"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string ChatJoinRequest::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_bio.has_value())
        {
            doc["bio"] = m_bio.value();
        }

        doc["chat"] = nlohmann::json::parse(m_chat->serialise());
        doc["date"] = m_date;
        doc["from"] = nlohmann::json::parse(m_from->serialise());

        if(m_invite_link.has_value())
        {
            doc["invite_link"] = nlohmann::json::parse(m_invite_link.value()->serialise());
        }

        doc["user_chat_id"] = m_user_chat_id;
        return doc.dump();
    }

    std::optional<std::string> ChatJoinRequest::get_bio() const
    {
        return m_bio;
    }

    Chat::ptr ChatJoinRequest::get_chat() const
    {
        return m_chat;
    }

    std::uint64_t ChatJoinRequest::get_date() const
    {
        return m_date;
    }

    User::ptr ChatJoinRequest::get_from() const
    {
        return m_from;
    }

    std::optional<ChatInviteLink::ptr> ChatJoinRequest::get_invite_link() const
    {
        return m_invite_link;
    }

    std::int64_t ChatJoinRequest::get_user_chat_id() const
    {
        return m_user_chat_id;
    }

    void ChatJoinRequest::set_bio(const std::optional<std::string> &bio)
    {
        m_bio = bio;
    }

    void ChatJoinRequest::set_chat(const Chat::ptr &chat)
    {
        m_chat = chat;
    }

    void ChatJoinRequest::set_date(const std::uint64_t &date)
    {
        m_date = date;
    }

    void ChatJoinRequest::set_from(const User::ptr &from)
    {
        m_from = from;
    }

    void ChatJoinRequest::set_invite_link(const std::optional<ChatInviteLink::ptr> &invite_link)
    {
        m_invite_link = invite_link;
    }

    void ChatJoinRequest::set_user_chat_id(const std::int64_t &user_chat_id)
    {
        m_user_chat_id = user_chat_id;
    }
} //namespace tgbot
