#include "tgbot/types/LabeledPrice.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    LabeledPrice::LabeledPrice() : m_amount(-1)
    {}

    LabeledPrice::LabeledPrice(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("amount"))
            {
                if(doc["amount"].is_number_integer())
                {
                    m_amount = doc["amount"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("amount"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("amount"));
            }

            if(doc.contains("label"))
            {
                if(doc["label"].is_string())
                {
                    m_label = doc["label"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("label"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("label"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string LabeledPrice::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["amount"] = m_amount;
        doc["label"] = m_label;
        return doc.dump();
    }

    std::int32_t LabeledPrice::get_amount() const
    {
        return m_amount;
    }

    std::string LabeledPrice::get_label() const
    {
        return m_label;
    }

    void LabeledPrice::set_amount(const std::int32_t &amount)
    {
        m_amount = amount;
    }

    void LabeledPrice::set_label(const std::string &label)
    {
        m_label = label;
    }
} //namespace tgbot
