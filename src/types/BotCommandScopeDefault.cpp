#include "tgbot/types/BotCommandScopeDefault.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    BotCommandScopeDefault::BotCommandScopeDefault() = default;

    BotCommandScopeDefault::BotCommandScopeDefault(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string BotCommandScopeDefault::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["type"] = m_type;
        return doc.dump();
    }

    std::string BotCommandScopeDefault::get_type() const
    {
        return m_type;
    }

    void BotCommandScopeDefault::set_type(const std::string &type)
    {
        m_type = type;
    }
} //namespace tgbot
