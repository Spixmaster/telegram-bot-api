#include "tgbot/types/ChatPermissions.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    ChatPermissions::ChatPermissions() = default;

    ChatPermissions::ChatPermissions(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("can_add_web_page_previews"))
            {
                if(doc["can_add_web_page_previews"].is_boolean())
                {
                    m_can_add_web_page_previews = doc["can_add_web_page_previews"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_add_web_page_previews"));
                }
            }

            if(doc.contains("can_change_info"))
            {
                if(doc["can_change_info"].is_boolean())
                {
                    m_can_change_info = doc["can_change_info"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_change_info"));
                }
            }

            if(doc.contains("can_invite_users"))
            {
                if(doc["can_invite_users"].is_boolean())
                {
                    m_can_invite_users = doc["can_invite_users"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_invite_users"));
                }
            }

            if(doc.contains("can_manage_topics"))
            {
                if(doc["can_manage_topics"].is_boolean())
                {
                    m_can_manage_topics = doc["can_manage_topics"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_manage_topics"));
                }
            }

            if(doc.contains("can_pin_messages"))
            {
                if(doc["can_pin_messages"].is_boolean())
                {
                    m_can_pin_messages = doc["can_pin_messages"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_pin_messages"));
                }
            }

            if(doc.contains("can_send_audios"))
            {
                if(doc["can_send_audios"].is_boolean())
                {
                    m_can_send_audios = doc["can_send_audios"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_send_audios"));
                }
            }

            if(doc.contains("can_send_documents"))
            {
                if(doc["can_send_documents"].is_boolean())
                {
                    m_can_send_documents = doc["can_send_documents"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_send_documents"));
                }
            }

            if(doc.contains("can_send_messages"))
            {
                if(doc["can_send_messages"].is_boolean())
                {
                    m_can_send_messages = doc["can_send_messages"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_send_messages"));
                }
            }

            if(doc.contains("can_send_other_messages"))
            {
                if(doc["can_send_other_messages"].is_boolean())
                {
                    m_can_send_other_messages = doc["can_send_other_messages"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_send_other_messages"));
                }
            }

            if(doc.contains("can_send_photos"))
            {
                if(doc["can_send_photos"].is_boolean())
                {
                    m_can_send_photos = doc["can_send_photos"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_send_photos"));
                }
            }

            if(doc.contains("can_send_polls"))
            {
                if(doc["can_send_polls"].is_boolean())
                {
                    m_can_send_polls = doc["can_send_polls"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_send_polls"));
                }
            }

            if(doc.contains("can_send_video_notes"))
            {
                if(doc["can_send_video_notes"].is_boolean())
                {
                    m_can_send_video_notes = doc["can_send_video_notes"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_send_video_notes"));
                }
            }

            if(doc.contains("can_send_videos"))
            {
                if(doc["can_send_videos"].is_boolean())
                {
                    m_can_send_videos = doc["can_send_videos"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_send_videos"));
                }
            }

            if(doc.contains("can_send_voice_notes"))
            {
                if(doc["can_send_voice_notes"].is_boolean())
                {
                    m_can_send_voice_notes = doc["can_send_voice_notes"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_send_voice_notes"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string ChatPermissions::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_can_add_web_page_previews.has_value())
        {
            doc["can_add_web_page_previews"] = m_can_add_web_page_previews.value();
        }

        if(m_can_change_info.has_value())
        {
            doc["can_change_info"] = m_can_change_info.value();
        }

        if(m_can_invite_users.has_value())
        {
            doc["can_invite_users"] = m_can_invite_users.value();
        }

        if(m_can_manage_topics.has_value())
        {
            doc["can_manage_topics"] = m_can_manage_topics.value();
        }

        if(m_can_pin_messages.has_value())
        {
            doc["can_pin_messages"] = m_can_pin_messages.value();
        }

        if(m_can_send_audios.has_value())
        {
            doc["can_send_audios"] = m_can_send_audios.value();
        }

        if(m_can_send_documents.has_value())
        {
            doc["can_send_documents"] = m_can_send_documents.value();
        }

        if(m_can_send_messages.has_value())
        {
            doc["can_send_messages"] = m_can_send_messages.value();
        }

        if(m_can_send_other_messages.has_value())
        {
            doc["can_send_other_messages"] = m_can_send_other_messages.value();
        }

        if(m_can_send_photos.has_value())
        {
            doc["can_send_photos"] = m_can_send_photos.value();
        }

        if(m_can_send_polls.has_value())
        {
            doc["can_send_polls"] = m_can_send_polls.value();
        }

        if(m_can_send_video_notes.has_value())
        {
            doc["can_send_video_notes"] = m_can_send_video_notes.value();
        }

        if(m_can_send_videos.has_value())
        {
            doc["can_send_videos"] = m_can_send_videos.value();
        }

        if(m_can_send_voice_notes.has_value())
        {
            doc["can_send_voice_notes"] = m_can_send_voice_notes.value();
        }

        return doc.dump();
    }

    std::optional<bool> ChatPermissions::get_can_add_web_page_previews() const
    {
        return m_can_add_web_page_previews;
    }

    std::optional<bool> ChatPermissions::get_can_change_info() const
    {
        return m_can_change_info;
    }

    std::optional<bool> ChatPermissions::get_can_invite_users() const
    {
        return m_can_invite_users;
    }

    std::optional<bool> ChatPermissions::get_can_manage_topics() const
    {
        return m_can_manage_topics;
    }

    std::optional<bool> ChatPermissions::get_can_pin_messages() const
    {
        return m_can_pin_messages;
    }

    std::optional<bool> ChatPermissions::get_can_send_audios() const
    {
        return m_can_send_audios;
    }

    std::optional<bool> ChatPermissions::get_can_send_documents() const
    {
        return m_can_send_documents;
    }

    std::optional<bool> ChatPermissions::get_can_send_messages() const
    {
        return m_can_send_messages;
    }

    std::optional<bool> ChatPermissions::get_can_send_other_messages() const
    {
        return m_can_send_other_messages;
    }

    std::optional<bool> ChatPermissions::get_can_send_photos() const
    {
        return m_can_send_photos;
    }

    std::optional<bool> ChatPermissions::get_can_send_polls() const
    {
        return m_can_send_polls;
    }

    std::optional<bool> ChatPermissions::get_can_send_video_notes() const
    {
        return m_can_send_video_notes;
    }

    std::optional<bool> ChatPermissions::get_can_send_videos() const
    {
        return m_can_send_videos;
    }

    std::optional<bool> ChatPermissions::get_can_send_voice_notes() const
    {
        return m_can_send_voice_notes;
    }

    void ChatPermissions::set_can_add_web_page_previews(const std::optional<bool> &can_add_web_page_previews)
    {
        m_can_add_web_page_previews = can_add_web_page_previews;
    }

    void ChatPermissions::set_can_change_info(const std::optional<bool> &can_change_info)
    {
        m_can_change_info = can_change_info;
    }

    void ChatPermissions::set_can_invite_users(const std::optional<bool> &can_invite_users)
    {
        m_can_invite_users = can_invite_users;
    }

    void ChatPermissions::set_can_manage_topics(const std::optional<bool> &can_manage_topics)
    {
        m_can_manage_topics = can_manage_topics;
    }

    void ChatPermissions::set_can_pin_messages(const std::optional<bool> &can_pin_messages)
    {
        m_can_pin_messages = can_pin_messages;
    }

    void ChatPermissions::set_can_send_audios(const std::optional<bool> &can_send_audios)
    {
        m_can_send_audios = can_send_audios;
    }

    void ChatPermissions::set_can_send_documents(const std::optional<bool> &can_send_documents)
    {
        m_can_send_documents = can_send_documents;
    }

    void ChatPermissions::set_can_send_messages(const std::optional<bool> &can_send_messages)
    {
        m_can_send_messages = can_send_messages;
    }

    void ChatPermissions::set_can_send_other_messages(const std::optional<bool> &can_send_other_messages)
    {
        m_can_send_other_messages = can_send_other_messages;
    }

    void ChatPermissions::set_can_send_photos(const std::optional<bool> &can_send_photos)
    {
        m_can_send_photos = can_send_photos;
    }

    void ChatPermissions::set_can_send_polls(const std::optional<bool> &can_send_polls)
    {
        m_can_send_polls = can_send_polls;
    }

    void ChatPermissions::set_can_send_video_notes(const std::optional<bool> &can_send_video_notes)
    {
        m_can_send_video_notes = can_send_video_notes;
    }

    void ChatPermissions::set_can_send_videos(const std::optional<bool> &can_send_videos)
    {
        m_can_send_videos = can_send_videos;
    }

    void ChatPermissions::set_can_send_voice_notes(const std::optional<bool> &can_send_voice_notes)
    {
        m_can_send_voice_notes = can_send_voice_notes;
    }
} //namespace tgbot
