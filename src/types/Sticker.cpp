#include "tgbot/types/Sticker.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    Sticker::Sticker() : m_height(-1), m_is_animated(false), m_is_video(false), m_width(-1)
    {}

    Sticker::Sticker(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("custom_emoji_id"))
            {
                if(doc["custom_emoji_id"].is_string())
                {
                    m_custom_emoji_id = doc["custom_emoji_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("custom_emoji_id"));
                }
            }

            if(doc.contains("emoji"))
            {
                if(doc["emoji"].is_string())
                {
                    m_emoji = doc["emoji"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("emoji"));
                }
            }

            if(doc.contains("file_id"))
            {
                if(doc["file_id"].is_string())
                {
                    m_file_id = doc["file_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("file_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("file_id"));
            }

            if(doc.contains("file_size"))
            {
                if(doc["file_size"].is_number_integer())
                {
                    m_file_size = doc["file_size"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("file_size"));
                }
            }

            if(doc.contains("file_unique_id"))
            {
                if(doc["file_unique_id"].is_string())
                {
                    m_file_unique_id = doc["file_unique_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("file_unique_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("file_unique_id"));
            }

            if(doc.contains("height"))
            {
                if(doc["height"].is_number_integer())
                {
                    m_height = doc["height"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("height"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("height"));
            }

            if(doc.contains("is_animated"))
            {
                if(doc["is_animated"].is_boolean())
                {
                    m_is_animated = doc["is_animated"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("is_animated"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("is_animated"));
            }

            if(doc.contains("is_video"))
            {
                if(doc["is_video"].is_boolean())
                {
                    m_is_video = doc["is_video"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("is_video"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("is_video"));
            }

            if(doc.contains("mask_position"))
            {
                if(doc["mask_position"].is_object())
                {
                    m_mask_position = std::make_shared<MaskPosition>(
                      doc["mask_"
                          "position"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("mask_position"));
                }
            }

            if(doc.contains("needs_repainting"))
            {
                if(doc["needs_repainting"].is_boolean())
                {
                    m_needs_repainting = doc["needs_repainting"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("needs_repainting"));
                }
            }

            if(doc.contains("premium_animation"))
            {
                if(doc["premium_animation"].is_object())
                {
                    m_premium_animation = std::make_shared<File>(doc["premium_animation"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("premium_animation"));
                }
            }

            if(doc.contains("set_name"))
            {
                if(doc["set_name"].is_string())
                {
                    m_set_name = doc["set_name"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("set_name"));
                }
            }

            if(doc.contains("thumbnail"))
            {
                if(doc["thumbnail"].is_object())
                {
                    m_thumbnail = std::make_shared<PhotoSize>(doc["thumbnail"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("thumbnail"));
                }
            }

            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }

            if(doc.contains("width"))
            {
                if(doc["width"].is_number_integer())
                {
                    m_width = doc["width"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("width"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("width"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string Sticker::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_custom_emoji_id.has_value())
        {
            doc["custom_emoji_id"] = m_custom_emoji_id.value();
        }

        if(m_emoji.has_value())
        {
            doc["emoji"] = m_emoji.value();
        }

        doc["file_id"] = m_file_id;

        if(m_file_size.has_value())
        {
            doc["file_size"] = m_file_size.value();
        }

        doc["file_unique_id"] = m_file_unique_id;
        doc["height"] = m_height;
        doc["is_animated"] = m_is_animated;
        doc["is_video"] = m_is_video;

        if(m_mask_position.has_value())
        {
            doc["mask_position"] = nlohmann::json::parse(m_mask_position.value()->serialise());
        }

        if(m_needs_repainting.has_value())
        {
            doc["needs_repainting"] = m_needs_repainting.value();
        }

        if(m_premium_animation.has_value())
        {
            doc["premium_animation"] = nlohmann::json::parse(m_premium_animation.value()->serialise());
        }

        if(m_set_name.has_value())
        {
            doc["set_name"] = m_set_name.value();
        }

        if(m_thumbnail.has_value())
        {
            doc["thumbnail"] = nlohmann::json::parse(m_thumbnail.value()->serialise());
        }

        doc["type"] = m_type;
        doc["width"] = m_width;
        return doc.dump();
    }

    std::optional<std::string> Sticker::get_custom_emoji_id() const
    {
        return m_custom_emoji_id;
    }

    std::optional<std::string> Sticker::get_emoji() const
    {
        return m_emoji;
    }

    std::string Sticker::get_file_id() const
    {
        return m_file_id;
    }

    std::optional<std::int32_t> Sticker::get_file_size() const
    {
        return m_file_size;
    }

    std::string Sticker::get_file_unique_id() const
    {
        return m_file_unique_id;
    }

    std::int32_t Sticker::get_height() const
    {
        return m_height;
    }

    bool Sticker::get_is_animated() const
    {
        return m_is_animated;
    }

    bool Sticker::get_is_video() const
    {
        return m_is_video;
    }

    std::optional<MaskPosition::ptr> Sticker::get_mask_position() const
    {
        return m_mask_position;
    }

    std::optional<bool> Sticker::get_needs_repainting() const
    {
        return m_needs_repainting;
    }

    std::optional<File::ptr> Sticker::get_premium_animation() const
    {
        return m_premium_animation;
    }

    std::optional<std::string> Sticker::get_set_name() const
    {
        return m_set_name;
    }

    std::optional<PhotoSize::ptr> Sticker::get_thumbnail() const
    {
        return m_thumbnail;
    }

    std::string Sticker::get_type() const
    {
        return m_type;
    }

    std::int32_t Sticker::get_width() const
    {
        return m_width;
    }

    void Sticker::set_custom_emoji_id(const std::optional<std::string> &custom_emoji_id)
    {
        m_custom_emoji_id = custom_emoji_id;
    }

    void Sticker::set_emoji(const std::optional<std::string> &emoji)
    {
        m_emoji = emoji;
    }

    void Sticker::set_file_id(const std::string &file_id)
    {
        m_file_id = file_id;
    }

    void Sticker::set_file_size(const std::optional<std::int32_t> &file_size)
    {
        m_file_size = file_size;
    }

    void Sticker::set_file_unique_id(const std::string &file_unique_id)
    {
        m_file_unique_id = file_unique_id;
    }

    void Sticker::set_height(const std::int32_t &height)
    {
        m_height = height;
    }

    void Sticker::set_is_animated(const bool &is_animated)
    {
        m_is_animated = is_animated;
    }

    void Sticker::set_is_video(const bool &is_video)
    {
        m_is_video = is_video;
    }

    void Sticker::set_mask_position(const std::optional<MaskPosition::ptr> &mask_position)
    {
        m_mask_position = mask_position;
    }

    void Sticker::set_needs_repainting(const std::optional<bool> &needs_repainting)
    {
        m_needs_repainting = needs_repainting;
    }

    void Sticker::set_premium_animation(const std::optional<File::ptr> &premium_animation)
    {
        m_premium_animation = premium_animation;
    }

    void Sticker::set_set_name(const std::optional<std::string> &set_name)
    {
        m_set_name = set_name;
    }

    void Sticker::set_thumbnail(const std::optional<PhotoSize::ptr> &thumbnail)
    {
        m_thumbnail = thumbnail;
    }

    void Sticker::set_type(const std::string &type)
    {
        m_type = type;
    }

    void Sticker::set_width(const std::int32_t &width)
    {
        m_width = width;
    }
} //namespace tgbot
