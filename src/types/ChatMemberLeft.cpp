#include "tgbot/types/ChatMemberLeft.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    ChatMemberLeft::ChatMemberLeft() : m_user(std::make_shared<User>())
    {}

    ChatMemberLeft::ChatMemberLeft(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("status"))
            {
                if(doc["status"].is_string())
                {
                    m_status = doc["status"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("status"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("status"));
            }

            if(doc.contains("user"))
            {
                if(doc["user"].is_object())
                {
                    m_user = std::make_shared<User>(doc["user"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("user"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("user"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string ChatMemberLeft::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["status"] = m_status;
        doc["user"] = nlohmann::json::parse(m_user->serialise());
        return doc.dump();
    }

    std::string ChatMemberLeft::get_status() const
    {
        return m_status;
    }

    User::ptr ChatMemberLeft::get_user() const
    {
        return m_user;
    }

    void ChatMemberLeft::set_status(const std::string &status)
    {
        m_status = status;
    }

    void ChatMemberLeft::set_user(const User::ptr &user)
    {
        m_user = user;
    }
} //namespace tgbot
