#include "tgbot/types/InputMediaAudio.h"

#include <algorithm>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "tgbot/constants/message.h"

namespace tgbot
{
    InputMediaAudio::InputMediaAudio() = default;

    InputMediaAudio::InputMediaAudio(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("caption"))
            {
                if(doc["caption"].is_string())
                {
                    m_caption = doc["caption"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("caption"));
                }
            }

            if(doc.contains("caption_entities"))
            {
                if(doc["caption_entities"].is_array())
                {
                    std::vector<MessageEntity::ptr> caption_entities;
                    caption_entities.reserve(doc["caption_entities"].size());

                    for(std::uint64_t i = 0; i < doc["caption_entities"].size(); ++i)
                    {
                        if(doc["caption_entities"].at(i).is_object())
                        {
                            caption_entities.emplace_back(
                              std::make_shared<MessageEntity>(doc["caption_entities"].at(i).dump()));
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_object(
                              "caption_"
                              "entitie"
                              "s"));
                        }
                    }

                    m_caption_entities = caption_entities;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("caption_entities"));
                }
            }

            if(doc.contains("duration"))
            {
                if(doc["duration"].is_number_integer())
                {
                    m_duration = doc["duration"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("duration"));
                }
            }

            if(doc.contains("media"))
            {
                if(doc["media"].is_string())
                {
                    m_media = doc["media"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("media"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("media"));
            }

            if(doc.contains("parse_mode"))
            {
                if(doc["parse_mode"].is_string())
                {
                    m_parse_mode = doc["parse_mode"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("parse_mode"));
                }
            }

            if(doc.contains("performer"))
            {
                if(doc["performer"].is_string())
                {
                    m_performer = doc["performer"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("performer"));
                }
            }

            if(doc.contains("thumbnail"))
            {
                if(doc["thumbnail"].is_string())
                {
                    m_thumbnail = doc["thumbnail"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("thumbnail"));
                }
            }

            if(doc.contains("title"))
            {
                if(doc["title"].is_string())
                {
                    m_title = doc["title"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("title"));
                }
            }

            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string InputMediaAudio::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_caption.has_value())
        {
            doc["caption"] = m_caption.value();
        }

        if(m_caption_entities.has_value())
        {
            nlohmann::json caption_entities = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_caption_entities.value().size(); ++i)
            {
                caption_entities.emplace_back(nlohmann::json::parse(m_caption_entities.value().at(i)->serialise()));
            }

            doc["caption_entities"] = caption_entities;
        }

        if(m_duration.has_value())
        {
            doc["duration"] = m_duration.value();
        }

        doc["media"] = m_media;

        if(m_parse_mode.has_value())
        {
            doc["parse_mode"] = m_parse_mode.value();
        }

        if(m_performer.has_value())
        {
            doc["performer"] = m_performer.value();
        }

        if(m_thumbnail.has_value())
        {
            doc["thumbnail"] = m_thumbnail.value();
        }

        if(m_title.has_value())
        {
            doc["title"] = m_title.value();
        }

        doc["type"] = m_type;
        return doc.dump();
    }

    std::optional<std::string> InputMediaAudio::get_caption() const
    {
        return m_caption;
    }

    std::optional<std::vector<MessageEntity::ptr>> InputMediaAudio::get_caption_entities() const
    {
        return m_caption_entities;
    }

    std::optional<std::int32_t> InputMediaAudio::get_duration() const
    {
        return m_duration;
    }

    std::string InputMediaAudio::get_media() const
    {
        return m_media;
    }

    std::optional<std::string> InputMediaAudio::get_parse_mode() const
    {
        return m_parse_mode;
    }

    std::optional<std::string> InputMediaAudio::get_performer() const
    {
        return m_performer;
    }

    std::optional<std::string> InputMediaAudio::get_thumbnail() const
    {
        return m_thumbnail;
    }

    std::optional<std::string> InputMediaAudio::get_title() const
    {
        return m_title;
    }

    std::string InputMediaAudio::get_type() const
    {
        return m_type;
    }

    void InputMediaAudio::set_caption(const std::optional<std::string> &caption)
    {
        m_caption = caption;
    }

    void InputMediaAudio::set_caption_entities(const std::optional<std::vector<MessageEntity::ptr>> &caption_entities)
    {
        m_caption_entities = caption_entities;
    }

    void InputMediaAudio::set_duration(const std::optional<std::int32_t> &duration)
    {
        m_duration = duration;
    }

    void InputMediaAudio::set_media(const std::string &media)
    {
        m_media = media;
    }

    void InputMediaAudio::set_parse_mode(const std::optional<std::string> &parse_mode)
    {
        m_parse_mode = parse_mode;
    }

    void InputMediaAudio::set_performer(const std::optional<std::string> &performer)
    {
        m_performer = performer;
    }

    void InputMediaAudio::set_thumbnail(const std::optional<std::string> &thumbnail)
    {
        m_thumbnail = thumbnail;
    }

    void InputMediaAudio::set_title(const std::optional<std::string> &title)
    {
        m_title = title;
    }

    void InputMediaAudio::set_type(const std::string &type)
    {
        m_type = type;
    }
} //namespace tgbot
