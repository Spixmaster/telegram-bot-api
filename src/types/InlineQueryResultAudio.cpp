#include "tgbot/types/InlineQueryResultAudio.h"

#include <algorithm>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "tgbot/constants/message.h"

namespace tgbot
{
    InlineQueryResultAudio::InlineQueryResultAudio() = default;

    InlineQueryResultAudio::InlineQueryResultAudio(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("audio_duration"))
            {
                if(doc["audio_duration"].is_number_integer())
                {
                    m_audio_duration = doc["audio_duration"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("audio_duration"));
                }
            }

            if(doc.contains("audio_url"))
            {
                if(doc["audio_url"].is_string())
                {
                    m_audio_url = doc["audio_url"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("audio_url"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("audio_url"));
            }

            if(doc.contains("caption"))
            {
                if(doc["caption"].is_string())
                {
                    m_caption = doc["caption"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("caption"));
                }
            }

            if(doc.contains("caption_entities"))
            {
                if(doc["caption_entities"].is_array())
                {
                    std::vector<MessageEntity::ptr> caption_entities;
                    caption_entities.reserve(doc["caption_entities"].size());

                    for(std::uint64_t i = 0; i < doc["caption_entities"].size(); ++i)
                    {
                        if(doc["caption_entities"].at(i).is_object())
                        {
                            caption_entities.emplace_back(
                              std::make_shared<MessageEntity>(doc["caption_entities"].at(i).dump()));
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_object(
                              "caption_"
                              "entitie"
                              "s"));
                        }
                    }

                    m_caption_entities = caption_entities;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("caption_entities"));
                }
            }

            if(doc.contains("id"))
            {
                if(doc["id"].is_string())
                {
                    m_id = doc["id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("id"));
            }

            if(doc.contains("input_message_content"))
            {
                if(doc["input_message_content"].is_object())
                {
                    m_input_message_content = std::make_shared<InputMessageContent>(doc["input_message_content"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("input_message_content"));
                }
            }

            if(doc.contains("parse_mode"))
            {
                if(doc["parse_mode"].is_string())
                {
                    m_parse_mode = doc["parse_mode"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("parse_mode"));
                }
            }

            if(doc.contains("performer"))
            {
                if(doc["performer"].is_string())
                {
                    m_performer = doc["performer"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("performer"));
                }
            }

            if(doc.contains("reply_markup"))
            {
                if(doc["reply_markup"].is_object())
                {
                    m_reply_markup = std::make_shared<InlineKeyboardMarkup>(
                      doc["reply_"
                          "marku"
                          "p"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("reply_markup"));
                }
            }

            if(doc.contains("title"))
            {
                if(doc["title"].is_string())
                {
                    m_title = doc["title"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("title"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("title"));
            }

            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string InlineQueryResultAudio::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_audio_duration.has_value())
        {
            doc["audio_duration"] = m_audio_duration.value();
        }

        doc["audio_url"] = m_audio_url;

        if(m_caption.has_value())
        {
            doc["caption"] = m_caption.value();
        }

        if(m_caption_entities.has_value())
        {
            nlohmann::json caption_entities = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_caption_entities.value().size(); ++i)
            {
                caption_entities.emplace_back(nlohmann::json::parse(m_caption_entities.value().at(i)->serialise()));
            }

            doc["caption_entities"] = caption_entities;
        }

        doc["id"] = m_id;

        if(m_input_message_content.has_value())
        {
            doc["input_message_content"] = nlohmann::json::parse(m_input_message_content.value()->serialise());
        }

        if(m_parse_mode.has_value())
        {
            doc["parse_mode"] = m_parse_mode.value();
        }

        if(m_performer.has_value())
        {
            doc["performer"] = m_performer.value();
        }

        if(m_reply_markup.has_value())
        {
            doc["reply_markup"] = nlohmann::json::parse(m_reply_markup.value()->serialise());
        }

        doc["title"] = m_title;
        doc["type"] = m_type;
        return doc.dump();
    }

    std::optional<std::int32_t> InlineQueryResultAudio::get_audio_duration() const
    {
        return m_audio_duration;
    }

    std::string InlineQueryResultAudio::get_audio_url() const
    {
        return m_audio_url;
    }

    std::optional<std::string> InlineQueryResultAudio::get_caption() const
    {
        return m_caption;
    }

    std::optional<std::vector<MessageEntity::ptr>> InlineQueryResultAudio::get_caption_entities() const
    {
        return m_caption_entities;
    }

    std::string InlineQueryResultAudio::get_id() const
    {
        return m_id;
    }

    std::optional<InputMessageContent::ptr> InlineQueryResultAudio::get_input_message_content() const
    {
        return m_input_message_content;
    }

    std::optional<std::string> InlineQueryResultAudio::get_parse_mode() const
    {
        return m_parse_mode;
    }

    std::optional<std::string> InlineQueryResultAudio::get_performer() const
    {
        return m_performer;
    }

    std::optional<InlineKeyboardMarkup::ptr> InlineQueryResultAudio::get_reply_markup() const
    {
        return m_reply_markup;
    }

    std::string InlineQueryResultAudio::get_title() const
    {
        return m_title;
    }

    std::string InlineQueryResultAudio::get_type() const
    {
        return m_type;
    }

    void InlineQueryResultAudio::set_audio_duration(const std::optional<std::int32_t> &audio_duration)
    {
        m_audio_duration = audio_duration;
    }

    void InlineQueryResultAudio::set_audio_url(const std::string &audio_url)
    {
        m_audio_url = audio_url;
    }

    void InlineQueryResultAudio::set_caption(const std::optional<std::string> &caption)
    {
        m_caption = caption;
    }

    void InlineQueryResultAudio::set_caption_entities(
      const std::optional<std::vector<MessageEntity::ptr>> &caption_entities)
    {
        m_caption_entities = caption_entities;
    }

    void InlineQueryResultAudio::set_id(const std::string &id)
    {
        m_id = id;
    }

    void InlineQueryResultAudio::set_input_message_content(
      const std::optional<InputMessageContent::ptr> &input_message_content)
    {
        m_input_message_content = input_message_content;
    }

    void InlineQueryResultAudio::set_parse_mode(const std::optional<std::string> &parse_mode)
    {
        m_parse_mode = parse_mode;
    }

    void InlineQueryResultAudio::set_performer(const std::optional<std::string> &performer)
    {
        m_performer = performer;
    }

    void InlineQueryResultAudio::set_reply_markup(const std::optional<InlineKeyboardMarkup::ptr> &reply_markup)
    {
        m_reply_markup = reply_markup;
    }

    void InlineQueryResultAudio::set_title(const std::string &title)
    {
        m_title = title;
    }

    void InlineQueryResultAudio::set_type(const std::string &type)
    {
        m_type = type;
    }
} //namespace tgbot
