#include "tgbot/types/ChatMemberUpdated.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    ChatMemberUpdated::ChatMemberUpdated() :
        m_chat(std::make_shared<Chat>()), m_date(1), m_from(std::make_shared<User>()),
        m_new_chat_member(std::make_shared<ChatMember>()), m_old_chat_member(std::make_shared<ChatMember>())
    {}

    ChatMemberUpdated::ChatMemberUpdated(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("chat"))
            {
                if(doc["chat"].is_object())
                {
                    m_chat = std::make_shared<Chat>(doc["chat"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("chat"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("chat"));
            }

            if(doc.contains("date"))
            {
                if(doc["date"].is_number_integer())
                {
                    m_date = doc["date"].get<std::uint64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("date"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("date"));
            }

            if(doc.contains("from"))
            {
                if(doc["from"].is_object())
                {
                    m_from = std::make_shared<User>(doc["from"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("from"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("from"));
            }

            if(doc.contains("invite_link"))
            {
                if(doc["invite_link"].is_object())
                {
                    m_invite_link = std::make_shared<ChatInviteLink>(doc["invite_link"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("invite_link"));
                }
            }

            if(doc.contains("new_chat_member"))
            {
                if(doc["new_chat_member"].is_object())
                {
                    m_new_chat_member = std::make_shared<ChatMember>(doc["new_chat_member"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("new_chat_member"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("new_chat_member"));
            }

            if(doc.contains("old_chat_member"))
            {
                if(doc["old_chat_member"].is_object())
                {
                    m_old_chat_member = std::make_shared<ChatMember>(doc["old_chat_member"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("old_chat_member"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("old_chat_member"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string ChatMemberUpdated::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["chat"] = nlohmann::json::parse(m_chat->serialise());
        doc["date"] = m_date;
        doc["from"] = nlohmann::json::parse(m_from->serialise());

        if(m_invite_link.has_value())
        {
            doc["invite_link"] = nlohmann::json::parse(m_invite_link.value()->serialise());
        }

        doc["new_chat_member"] = nlohmann::json::parse(m_new_chat_member->serialise());
        doc["old_chat_member"] = nlohmann::json::parse(m_old_chat_member->serialise());
        return doc.dump();
    }

    Chat::ptr ChatMemberUpdated::get_chat() const
    {
        return m_chat;
    }

    std::uint64_t ChatMemberUpdated::get_date() const
    {
        return m_date;
    }

    User::ptr ChatMemberUpdated::get_from() const
    {
        return m_from;
    }

    std::optional<ChatInviteLink::ptr> ChatMemberUpdated::get_invite_link() const
    {
        return m_invite_link;
    }

    ChatMember::ptr ChatMemberUpdated::get_new_chat_member() const
    {
        return m_new_chat_member;
    }

    ChatMember::ptr ChatMemberUpdated::get_old_chat_member() const
    {
        return m_old_chat_member;
    }

    void ChatMemberUpdated::set_chat(const Chat::ptr &chat)
    {
        m_chat = chat;
    }

    void ChatMemberUpdated::set_date(const std::uint64_t &date)
    {
        m_date = date;
    }

    void ChatMemberUpdated::set_from(const User::ptr &from)
    {
        m_from = from;
    }

    void ChatMemberUpdated::set_invite_link(const std::optional<ChatInviteLink::ptr> &invite_link)
    {
        m_invite_link = invite_link;
    }

    void ChatMemberUpdated::set_new_chat_member(const ChatMember::ptr &new_chat_member)
    {
        m_new_chat_member = new_chat_member;
    }

    void ChatMemberUpdated::set_old_chat_member(const ChatMember::ptr &old_chat_member)
    {
        m_old_chat_member = old_chat_member;
    }
} //namespace tgbot
