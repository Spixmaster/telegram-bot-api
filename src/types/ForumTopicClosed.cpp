#include "tgbot/types/ForumTopicClosed.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot

{
    ForumTopicClosed::ForumTopicClosed() = default;

    ForumTopicClosed::ForumTopicClosed(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {}
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string ForumTopicClosed::serialise() const
    {
        const nlohmann::json doc = nlohmann::json::object();
        return doc.dump();
    }
} //namespace tgbot
