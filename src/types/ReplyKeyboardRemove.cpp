#include "tgbot/types/ReplyKeyboardRemove.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    ReplyKeyboardRemove::ReplyKeyboardRemove() : m_remove_keyboard(false)
    {}

    ReplyKeyboardRemove::ReplyKeyboardRemove(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("remove_keyboard"))
            {
                if(doc["remove_keyboard"].is_boolean())
                {
                    m_remove_keyboard = doc["remove_keyboard"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("remove_keyboard"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("remove_keyboard"));
            }

            if(doc.contains("selective"))
            {
                if(doc["selective"].is_boolean())
                {
                    m_selective = doc["selective"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("selective"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string ReplyKeyboardRemove::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["remove_keyboard"] = m_remove_keyboard;

        if(m_selective.has_value())
        {
            doc["selective"] = m_selective.value();
        }

        return doc.dump();
    }

    bool ReplyKeyboardRemove::get_remove_keyboard() const
    {
        return m_remove_keyboard;
    }

    std::optional<bool> ReplyKeyboardRemove::get_selective() const
    {
        return m_selective;
    }

    void ReplyKeyboardRemove::set_remove_keyboard(const bool &remove_keyboard)
    {
        m_remove_keyboard = remove_keyboard;
    }

    void ReplyKeyboardRemove::set_selective(const std::optional<bool> &selective)
    {
        m_selective = selective;
    }
} //namespace tgbot
