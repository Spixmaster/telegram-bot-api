#include "tgbot/types/Dice.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    Dice::Dice() : m_value(-1)
    {}

    Dice::Dice(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("emoji"))
            {
                if(doc["emoji"].is_string())
                {
                    m_emoji = doc["emoji"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("emoji"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("emoji"));
            }

            if(doc.contains("value"))
            {
                if(doc["value"].is_number_integer())
                {
                    m_value = doc["value"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("value"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("value"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string Dice::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["emoji"] = m_emoji;
        doc["value"] = m_value;
        return doc.dump();
    }

    std::string Dice::get_emoji() const
    {
        return m_emoji;
    }

    std::int32_t Dice::get_value() const
    {
        return m_value;
    }

    void Dice::set_emoji(const std::string &emoji)
    {
        m_emoji = emoji;
    }

    void Dice::set_value(const std::int32_t &value)
    {
        m_value = value;
    }
} //namespace tgbot
