#include "tgbot/types/PassportElementErrorUnspecified.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    PassportElementErrorUnspecified::PassportElementErrorUnspecified() = default;

    PassportElementErrorUnspecified::PassportElementErrorUnspecified(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("element_hash"))
            {
                if(doc["element_hash"].is_string())
                {
                    m_element_hash = doc["element_hash"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("element_hash"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("element_hash"));
            }

            if(doc.contains("message"))
            {
                if(doc["message"].is_string())
                {
                    m_message = doc["message"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("message"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("message"));
            }

            if(doc.contains("source"))
            {
                if(doc["source"].is_string())
                {
                    m_source = doc["source"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("source"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("source"));
            }

            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string PassportElementErrorUnspecified::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["element_hash"] = m_element_hash;
        doc["message"] = m_message;
        doc["source"] = m_source;
        doc["type"] = m_type;
        return doc.dump();
    }

    std::string PassportElementErrorUnspecified::get_element_hash() const
    {
        return m_element_hash;
    }

    std::string PassportElementErrorUnspecified::get_message() const
    {
        return m_message;
    }

    std::string PassportElementErrorUnspecified::get_source() const
    {
        return m_source;
    }

    std::string PassportElementErrorUnspecified::get_type() const
    {
        return m_type;
    }

    void PassportElementErrorUnspecified::set_element_hash(const std::string &element_hash)
    {
        m_element_hash = element_hash;
    }

    void PassportElementErrorUnspecified::set_message(const std::string &message)
    {
        m_message = message;
    }

    void PassportElementErrorUnspecified::set_source(const std::string &source)
    {
        m_source = source;
    }

    void PassportElementErrorUnspecified::set_type(const std::string &type)
    {
        m_type = type;
    }
} //namespace tgbot
