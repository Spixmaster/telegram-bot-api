#include "tgbot/types/LoginUrl.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    LoginUrl::LoginUrl() = default;

    LoginUrl::LoginUrl(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("bot_username"))
            {
                if(doc["bot_username"].is_string())
                {
                    m_bot_username = doc["bot_username"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("bot_username"));
                }
            }

            if(doc.contains("forward_text"))
            {
                if(doc["forward_text"].is_string())
                {
                    m_forward_text = doc["forward_text"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("forward_text"));
                }
            }

            if(doc.contains("request_write_access"))
            {
                if(doc["request_write_access"].is_boolean())
                {
                    m_request_write_access = doc["request_write_access"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("request_write_access"));
                }
            }

            if(doc.contains("url"))
            {
                if(doc["url"].is_string())
                {
                    m_url = doc["url"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("url"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("url"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string LoginUrl::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_bot_username.has_value())
        {
            doc["bot_username"] = m_bot_username.value();
        }

        if(m_forward_text.has_value())
        {
            doc["forward_text"] = m_forward_text.value();
        }

        if(m_request_write_access.has_value())
        {
            doc["request_write_access"] = m_request_write_access.value();
        }

        doc["url"] = m_url;
        return doc.dump();
    }

    std::optional<std::string> LoginUrl::get_bot_username() const
    {
        return m_bot_username;
    }

    std::optional<std::string> LoginUrl::get_forward_text() const
    {
        return m_forward_text;
    }

    std::optional<bool> LoginUrl::get_request_write_access() const
    {
        return m_request_write_access;
    }

    std::string LoginUrl::get_url() const
    {
        return m_url;
    }

    void LoginUrl::set_bot_username(const std::optional<std::string> &bot_username)
    {
        m_bot_username = bot_username;
    }

    void LoginUrl::set_forward_text(const std::optional<std::string> &forward_text)
    {
        m_forward_text = forward_text;
    }

    void LoginUrl::set_request_write_access(const std::optional<bool> &request_write_access)
    {
        m_request_write_access = request_write_access;
    }

    void LoginUrl::set_url(const std::string &url)
    {
        m_url = url;
    }
} //namespace tgbot
