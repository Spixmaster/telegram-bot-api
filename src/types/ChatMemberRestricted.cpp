#include "tgbot/types/ChatMemberRestricted.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    ChatMemberRestricted::ChatMemberRestricted() :
        m_can_add_web_page_previews(false), m_can_change_info(false), m_can_invite_users(false),
        m_can_manage_topics(false), m_can_pin_messages(false), m_can_send_audios(false), m_can_send_documents(false),
        m_can_send_messages(false), m_can_send_other_messages(false), m_can_send_photos(false), m_can_send_polls(false),
        m_can_send_video_notes(false), m_can_send_videos(false), m_can_send_voice_notes(false), m_is_member(false),
        m_until_date(1), m_user(std::make_shared<User>())
    {}

    ChatMemberRestricted::ChatMemberRestricted(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("can_add_web_page_previews"))
            {
                if(doc["can_add_web_page_previews"].is_boolean())
                {
                    m_can_add_web_page_previews = doc["can_add_web_page_previews"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_add_web_page_previews"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_add_web_page_previews"));
            }

            if(doc.contains("can_change_info"))
            {
                if(doc["can_change_info"].is_boolean())
                {
                    m_can_change_info = doc["can_change_info"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_change_info"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_change_info"));
            }

            if(doc.contains("can_invite_users"))
            {
                if(doc["can_invite_users"].is_boolean())
                {
                    m_can_invite_users = doc["can_invite_users"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_invite_users"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_invite_users"));
            }

            if(doc.contains("can_manage_topics"))
            {
                if(doc["can_manage_topics"].is_boolean())
                {
                    m_can_manage_topics = doc["can_manage_topics"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_manage_topics"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_manage_topics"));
            }

            if(doc.contains("can_pin_messages"))
            {
                if(doc["can_pin_messages"].is_boolean())
                {
                    m_can_pin_messages = doc["can_pin_messages"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_pin_messages"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_pin_messages"));
            }

            if(doc.contains("can_send_audios"))
            {
                if(doc["can_send_audios"].is_boolean())
                {
                    m_can_send_audios = doc["can_send_audios"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_send_audios"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_send_audios"));
            }

            if(doc.contains("can_send_documents"))
            {
                if(doc["can_send_documents"].is_boolean())
                {
                    m_can_send_documents = doc["can_send_documents"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_send_documents"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_send_documents"));
            }

            if(doc.contains("can_send_messages"))
            {
                if(doc["can_send_messages"].is_boolean())
                {
                    m_can_send_messages = doc["can_send_messages"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_send_messages"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_send_messages"));
            }

            if(doc.contains("can_send_other_messages"))
            {
                if(doc["can_send_other_messages"].is_boolean())
                {
                    m_can_send_other_messages = doc["can_send_other_messages"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_send_other_messages"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_send_other_messages"));
            }

            if(doc.contains("can_send_photos"))
            {
                if(doc["can_send_photos"].is_boolean())
                {
                    m_can_send_photos = doc["can_send_photos"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_send_photos"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_send_photos"));
            }

            if(doc.contains("can_send_polls"))
            {
                if(doc["can_send_polls"].is_boolean())
                {
                    m_can_send_polls = doc["can_send_polls"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_send_polls"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_send_polls"));
            }

            if(doc.contains("can_send_video_notes"))
            {
                if(doc["can_send_video_notes"].is_boolean())
                {
                    m_can_send_video_notes = doc["can_send_video_notes"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_send_video_notes"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_send_video_notes"));
            }

            if(doc.contains("can_send_videos"))
            {
                if(doc["can_send_videos"].is_boolean())
                {
                    m_can_send_videos = doc["can_send_videos"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_send_videos"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_send_videos"));
            }

            if(doc.contains("can_send_voice_notes"))
            {
                if(doc["can_send_voice_notes"].is_boolean())
                {
                    m_can_send_voice_notes = doc["can_send_voice_notes"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_send_voice_notes"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_send_voice_notes"));
            }

            if(doc.contains("is_member"))
            {
                if(doc["is_member"].is_boolean())
                {
                    m_is_member = doc["is_member"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("is_member"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("is_member"));
            }

            if(doc.contains("status"))
            {
                if(doc["status"].is_string())
                {
                    m_status = doc["status"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("status"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("status"));
            }

            if(doc.contains("until_date"))
            {
                if(doc["until_date"].is_number_integer())
                {
                    m_until_date = doc["until_date"].get<std::uint64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("until_date"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("until_date"));
            }

            if(doc.contains("user"))
            {
                if(doc["user"].is_object())
                {
                    m_user = std::make_shared<User>(doc["user"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("user"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("user"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string ChatMemberRestricted::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["can_add_web_page_previews"] = m_can_add_web_page_previews;
        doc["can_change_info"] = m_can_change_info;
        doc["can_invite_users"] = m_can_invite_users;
        doc["can_manage_topics"] = m_can_manage_topics;
        doc["can_pin_messages"] = m_can_pin_messages;
        doc["can_send_audios"] = m_can_send_audios;
        doc["can_send_documents"] = m_can_send_documents;
        doc["can_send_messages"] = m_can_send_messages;
        doc["can_send_other_messages"] = m_can_send_other_messages;
        doc["can_send_photos"] = m_can_send_photos;
        doc["can_send_polls"] = m_can_send_polls;
        doc["can_send_video_notes"] = m_can_send_video_notes;
        doc["can_send_videos"] = m_can_send_videos;
        doc["can_send_voice_notes"] = m_can_send_voice_notes;
        doc["is_member"] = m_is_member;
        doc["status"] = m_status;
        doc["until_date"] = m_until_date;
        doc["user"] = nlohmann::json::parse(m_user->serialise());
        return doc.dump();
    }

    bool ChatMemberRestricted::get_can_add_web_page_previews() const
    {
        return m_can_add_web_page_previews;
    }

    bool ChatMemberRestricted::get_can_change_info() const
    {
        return m_can_change_info;
    }

    bool ChatMemberRestricted::get_can_invite_users() const
    {
        return m_can_invite_users;
    }

    bool ChatMemberRestricted::get_can_manage_topics() const
    {
        return m_can_manage_topics;
    }

    bool ChatMemberRestricted::get_can_pin_messages() const
    {
        return m_can_pin_messages;
    }

    bool ChatMemberRestricted::get_can_send_audios() const
    {
        return m_can_send_audios;
    }

    bool ChatMemberRestricted::get_can_send_documents() const
    {
        return m_can_send_documents;
    }

    bool ChatMemberRestricted::get_can_send_messages() const
    {
        return m_can_send_messages;
    }

    bool ChatMemberRestricted::get_can_send_other_messages() const
    {
        return m_can_send_other_messages;
    }

    bool ChatMemberRestricted::get_can_send_photos() const
    {
        return m_can_send_photos;
    }

    bool ChatMemberRestricted::get_can_send_polls() const
    {
        return m_can_send_polls;
    }

    bool ChatMemberRestricted::get_can_send_video_notes() const
    {
        return m_can_send_video_notes;
    }

    bool ChatMemberRestricted::get_can_send_videos() const
    {
        return m_can_send_videos;
    }

    bool ChatMemberRestricted::get_can_send_voice_notes() const
    {
        return m_can_send_voice_notes;
    }

    bool ChatMemberRestricted::get_is_member() const
    {
        return m_is_member;
    }

    std::string ChatMemberRestricted::get_status() const
    {
        return m_status;
    }

    std::uint64_t ChatMemberRestricted::get_until_date() const
    {
        return m_until_date;
    }

    User::ptr ChatMemberRestricted::get_user() const
    {
        return m_user;
    }

    void ChatMemberRestricted::set_can_add_web_page_previews(const bool &can_add_web_page_previews)
    {
        m_can_add_web_page_previews = can_add_web_page_previews;
    }

    void ChatMemberRestricted::set_can_change_info(const bool &can_change_info)
    {
        m_can_change_info = can_change_info;
    }

    void ChatMemberRestricted::set_can_invite_users(const bool &can_invite_users)
    {
        m_can_invite_users = can_invite_users;
    }

    void ChatMemberRestricted::set_can_manage_topics(const bool &can_manage_topics)
    {
        m_can_manage_topics = can_manage_topics;
    }

    void ChatMemberRestricted::set_can_pin_messages(const bool &can_pin_messages)
    {
        m_can_pin_messages = can_pin_messages;
    }

    void ChatMemberRestricted::set_can_send_audios(const bool &can_send_audios)
    {
        m_can_send_audios = can_send_audios;
    }

    void ChatMemberRestricted::set_can_send_documents(const bool &can_send_documents)
    {
        m_can_send_documents = can_send_documents;
    }

    void ChatMemberRestricted::set_can_send_messages(const bool &can_send_messages)
    {
        m_can_send_messages = can_send_messages;
    }

    void ChatMemberRestricted::set_can_send_other_messages(const bool &can_send_other_messages)
    {
        m_can_send_other_messages = can_send_other_messages;
    }

    void ChatMemberRestricted::set_can_send_photos(const bool &can_send_photos)
    {
        m_can_send_photos = can_send_photos;
    }

    void ChatMemberRestricted::set_can_send_polls(const bool &can_send_polls)
    {
        m_can_send_polls = can_send_polls;
    }

    void ChatMemberRestricted::set_can_send_video_notes(const bool &can_send_video_notes)
    {
        m_can_send_video_notes = can_send_video_notes;
    }

    void ChatMemberRestricted::set_can_send_videos(const bool &can_send_videos)
    {
        m_can_send_videos = can_send_videos;
    }

    void ChatMemberRestricted::set_can_send_voice_notes(const bool &can_send_voice_notes)
    {
        m_can_send_voice_notes = can_send_voice_notes;
    }

    void ChatMemberRestricted::set_is_member(const bool &is_member)
    {
        m_is_member = is_member;
    }

    void ChatMemberRestricted::set_status(const std::string &status)
    {
        m_status = status;
    }

    void ChatMemberRestricted::set_until_date(const std::uint64_t &until_date)
    {
        m_until_date = until_date;
    }

    void ChatMemberRestricted::set_user(const User::ptr &user)
    {
        m_user = user;
    }
} //namespace tgbot
