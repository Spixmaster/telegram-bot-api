#include "tgbot/types/VideoChatEnded.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <string>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    VideoChatEnded::VideoChatEnded() : m_duration(-1)
    {}

    VideoChatEnded::VideoChatEnded(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("duration"))
            {
                if(doc["duration"].is_number_integer())
                {
                    m_duration = doc["duration"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("duration"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("duration"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string VideoChatEnded::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["duration"] = m_duration;
        return doc.dump();
    }

    std::int32_t VideoChatEnded::get_duration() const
    {
        return m_duration;
    }

    void VideoChatEnded::set_duration(const std::int32_t &duration)
    {
        m_duration = duration;
    }
} //namespace tgbot
