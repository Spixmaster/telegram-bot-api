#include "tgbot/types/Game.h"

#include <algorithm>
#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "tgbot/constants/message.h"

namespace tgbot
{
    Game::Game() = default;

    Game::Game(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("animation"))
            {
                if(doc["animation"].is_object())
                {
                    m_animation = std::make_shared<Animation>(doc["animation"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("animation"));
                }
            }

            if(doc.contains("description"))
            {
                if(doc["description"].is_string())
                {
                    m_description = doc["description"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("description"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("description"));
            }

            if(doc.contains("photo"))
            {
                if(doc["photo"].is_array())
                {
                    std::vector<PhotoSize::ptr> photo;
                    photo.reserve(doc["photo"].size());

                    for(std::uint64_t i = 0; i < doc["photo"].size(); ++i)
                    {
                        if(doc["photo"].at(i).is_object())
                        {
                            photo.emplace_back(std::make_shared<PhotoSize>(doc["photo"].at(i).dump()));
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_object("photo"));
                        }
                    }

                    m_photo = photo;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("photo"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("photo"));
            }

            if(doc.contains("text"))
            {
                if(doc["text"].is_string())
                {
                    m_text = doc["text"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("text"));
                }
            }

            if(doc.contains("text_entities"))
            {
                if(doc["text_entities"].is_array())
                {
                    std::vector<MessageEntity::ptr> text_entities;
                    text_entities.reserve(doc["text_entities"].size());

                    for(std::uint64_t i = 0; i < doc["text_entities"].size(); ++i)
                    {
                        if(doc["text_entities"].at(i).is_object())
                        {
                            text_entities.emplace_back(
                              std::make_shared<MessageEntity>(doc["text_entities"].at(i).dump()));
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_object(
                              "text_"
                              "entitie"
                              "s"));
                        }
                    }

                    m_text_entities = text_entities;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("text_entities"));
                }
            }

            if(doc.contains("title"))
            {
                if(doc["title"].is_string())
                {
                    m_title = doc["title"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("title"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("title"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string Game::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_animation.has_value())
        {
            doc["animation"] = nlohmann::json::parse(m_animation.value()->serialise());
        }

        doc["description"] = m_description;

        {
            nlohmann::json photo = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_photo.size(); ++i)
            {
                photo.emplace_back(nlohmann::json::parse(m_photo.at(i)->serialise()));
            }

            doc["photo"] = photo;
        }

        if(m_text.has_value())
        {
            doc["text"] = m_text.value();
        }

        if(m_text_entities.has_value())
        {
            nlohmann::json text_entities = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_text_entities.value().size(); ++i)
            {
                text_entities.emplace_back(nlohmann::json::parse(m_text_entities.value().at(i)->serialise()));
            }

            doc["text_entities"] = text_entities;
        }

        doc["title"] = m_title;
        return doc.dump();
    }

    std::optional<Animation::ptr> Game::get_animation() const
    {
        return m_animation;
    }

    std::string Game::get_description() const
    {
        return m_description;
    }

    std::vector<PhotoSize::ptr> Game::get_photo() const
    {
        return m_photo;
    }

    std::optional<std::string> Game::get_text() const
    {
        return m_text;
    }

    std::optional<std::vector<MessageEntity::ptr>> Game::get_text_entities() const
    {
        return m_text_entities;
    }

    std::string Game::get_title() const
    {
        return m_title;
    }

    void Game::set_animation(const std::optional<Animation::ptr> &animation)
    {
        m_animation = animation;
    }

    void Game::set_description(const std::string &description)
    {
        m_description = description;
    }

    void Game::set_photo(const std::vector<PhotoSize::ptr> &photo)
    {
        m_photo = photo;
    }

    void Game::set_text(const std::optional<std::string> &text)
    {
        m_text = text;
    }

    void Game::set_text_entities(const std::optional<std::vector<MessageEntity::ptr>> &text_entities)
    {
        m_text_entities = text_entities;
    }

    void Game::set_title(const std::string &title)
    {
        m_title = title;
    }
} //namespace tgbot
