#include "tgbot/types/Invoice.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    Invoice::Invoice() : m_total_amount(-1)
    {}

    Invoice::Invoice(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("currency"))
            {
                if(doc["currency"].is_string())
                {
                    m_currency = doc["currency"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("currency"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("currency"));
            }

            if(doc.contains("description"))
            {
                if(doc["description"].is_string())
                {
                    m_description = doc["description"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("description"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("description"));
            }

            if(doc.contains("start_parameter"))
            {
                if(doc["start_parameter"].is_string())
                {
                    m_start_parameter = doc["start_parameter"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("start_parameter"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("start_parameter"));
            }

            if(doc.contains("title"))
            {
                if(doc["title"].is_string())
                {
                    m_title = doc["title"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("title"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("title"));
            }

            if(doc.contains("total_amount"))
            {
                if(doc["total_amount"].is_number_integer())
                {
                    m_total_amount = doc["total_amount"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("total_amount"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("total_amount"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string Invoice::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["currency"] = m_currency;
        doc["description"] = m_description;
        doc["start_parameter"] = m_start_parameter;
        doc["title"] = m_title;
        doc["total_amount"] = m_total_amount;
        return doc.dump();
    }

    std::string Invoice::get_currency() const
    {
        return m_currency;
    }

    std::string Invoice::get_description() const
    {
        return m_description;
    }

    std::string Invoice::get_start_parameter() const
    {
        return m_start_parameter;
    }

    std::string Invoice::get_title() const
    {
        return m_title;
    }

    std::int32_t Invoice::get_total_amount() const
    {
        return m_total_amount;
    }

    void Invoice::set_currency(const std::string &currency)
    {
        m_currency = currency;
    }

    void Invoice::set_description(const std::string &description)
    {
        m_description = description;
    }

    void Invoice::set_start_parameter(const std::string &start_parameter)
    {
        m_start_parameter = start_parameter;
    }

    void Invoice::set_title(const std::string &title)
    {
        m_title = title;
    }

    void Invoice::set_total_amount(const std::int32_t &total_amount)
    {
        m_total_amount = total_amount;
    }
} //namespace tgbot
