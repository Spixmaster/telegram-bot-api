#include "tgbot/types/ChatMemberAdministrator.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    ChatMemberAdministrator::ChatMemberAdministrator() :
        m_can_be_edited(false), m_can_change_info(false), m_can_delete_messages(false), m_can_invite_users(false),
        m_can_manage_chat(false), m_can_manage_video_chats(false), m_can_promote_members(false),
        m_can_restrict_members(false), m_is_anonymous(false), m_user(std::make_shared<User>())
    {}

    ChatMemberAdministrator::ChatMemberAdministrator(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("can_be_edited"))
            {
                if(doc["can_be_edited"].is_boolean())
                {
                    m_can_be_edited = doc["can_be_edited"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_be_edited"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_be_edited"));
            }

            if(doc.contains("can_change_info"))
            {
                if(doc["can_change_info"].is_boolean())
                {
                    m_can_change_info = doc["can_change_info"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_change_info"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_change_info"));
            }

            if(doc.contains("can_delete_messages"))
            {
                if(doc["can_delete_messages"].is_boolean())
                {
                    m_can_delete_messages = doc["can_delete_messages"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_delete_messages"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_delete_messages"));
            }

            if(doc.contains("can_edit_messages"))
            {
                if(doc["can_edit_messages"].is_boolean())
                {
                    m_can_edit_messages = doc["can_edit_messages"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_edit_messages"));
                }
            }

            if(doc.contains("can_invite_users"))
            {
                if(doc["can_invite_users"].is_boolean())
                {
                    m_can_invite_users = doc["can_invite_users"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_invite_users"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_invite_users"));
            }

            if(doc.contains("can_manage_chat"))
            {
                if(doc["can_manage_chat"].is_boolean())
                {
                    m_can_manage_chat = doc["can_manage_chat"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_manage_chat"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_manage_chat"));
            }

            if(doc.contains("can_manage_topics"))
            {
                if(doc["can_manage_topics"].is_boolean())
                {
                    m_can_manage_topics = doc["can_manage_topics"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_manage_topics"));
                }
            }

            if(doc.contains("can_manage_video_chats"))
            {
                if(doc["can_manage_video_chats"].is_boolean())
                {
                    m_can_manage_video_chats = doc["can_manage_video_chats"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_manage_video_chats"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_manage_video_chats"));
            }

            if(doc.contains("can_pin_messages"))
            {
                if(doc["can_pin_messages"].is_boolean())
                {
                    m_can_pin_messages = doc["can_pin_messages"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_pin_messages"));
                }
            }

            if(doc.contains("can_post_messages"))
            {
                if(doc["can_post_messages"].is_boolean())
                {
                    m_can_post_messages = doc["can_post_messages"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_post_messages"));
                }
            }

            if(doc.contains("can_promote_members"))
            {
                if(doc["can_promote_members"].is_boolean())
                {
                    m_can_promote_members = doc["can_promote_members"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_promote_members"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_promote_members"));
            }

            if(doc.contains("can_restrict_members"))
            {
                if(doc["can_restrict_members"].is_boolean())
                {
                    m_can_restrict_members = doc["can_restrict_members"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_restrict_members"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_restrict_members"));
            }

            if(doc.contains("custom_title"))
            {
                if(doc["custom_title"].is_string())
                {
                    m_custom_title = doc["custom_title"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("custom_title"));
                }
            }

            if(doc.contains("is_anonymous"))
            {
                if(doc["is_anonymous"].is_boolean())
                {
                    m_is_anonymous = doc["is_anonymous"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("is_anonymous"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("is_anonymous"));
            }

            if(doc.contains("status"))
            {
                if(doc["status"].is_string())
                {
                    m_status = doc["status"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("status"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("status"));
            }

            if(doc.contains("user"))
            {
                if(doc["user"].is_object())
                {
                    m_user = std::make_shared<User>(doc["user"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("user"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("user"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string ChatMemberAdministrator::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["can_be_edited"] = m_can_be_edited;
        doc["can_change_info"] = m_can_change_info;
        doc["can_delete_messages"] = m_can_delete_messages;

        if(m_can_edit_messages.has_value())
        {
            doc["can_edit_messages"] = m_can_edit_messages.value();
        }

        doc["can_invite_users"] = m_can_invite_users;
        doc["can_manage_chat"] = m_can_manage_chat;

        if(m_can_manage_topics.has_value())
        {
            doc["can_manage_topics"] = m_can_manage_topics.value();
        }

        doc["can_manage_video_chats"] = m_can_manage_video_chats;

        if(m_can_pin_messages.has_value())
        {
            doc["can_pin_messages"] = m_can_pin_messages.value();
        }

        if(m_can_post_messages.has_value())
        {
            doc["can_post_messages"] = m_can_post_messages.value();
        }

        doc["can_promote_members"] = m_can_promote_members;
        doc["can_restrict_members"] = m_can_restrict_members;

        if(m_custom_title.has_value())
        {
            doc["custom_title"] = m_custom_title.value();
        }

        doc["is_anonymous"] = m_is_anonymous;
        doc["status"] = m_status;
        doc["user"] = nlohmann::json::parse(m_user->serialise());
        return doc.dump();
    }

    bool ChatMemberAdministrator::get_can_be_edited() const
    {
        return m_can_be_edited;
    }

    bool ChatMemberAdministrator::get_can_change_info() const
    {
        return m_can_change_info;
    }

    bool ChatMemberAdministrator::get_can_delete_messages() const
    {
        return m_can_delete_messages;
    }

    std::optional<bool> ChatMemberAdministrator::get_can_edit_messages() const
    {
        return m_can_edit_messages;
    }

    bool ChatMemberAdministrator::get_can_invite_users() const
    {
        return m_can_invite_users;
    }

    bool ChatMemberAdministrator::get_can_manage_chat() const
    {
        return m_can_manage_chat;
    }

    std::optional<bool> ChatMemberAdministrator::get_can_manage_topics() const
    {
        return m_can_manage_topics;
    }

    bool ChatMemberAdministrator::get_can_manage_video_chats() const
    {
        return m_can_manage_video_chats;
    }

    std::optional<bool> ChatMemberAdministrator::get_can_pin_messages() const
    {
        return m_can_pin_messages;
    }

    std::optional<bool> ChatMemberAdministrator::get_can_post_messages() const
    {
        return m_can_post_messages;
    }

    bool ChatMemberAdministrator::get_can_promote_members() const
    {
        return m_can_promote_members;
    }

    bool ChatMemberAdministrator::get_can_restrict_members() const
    {
        return m_can_restrict_members;
    }

    std::optional<std::string> ChatMemberAdministrator::get_custom_title() const
    {
        return m_custom_title;
    }

    bool ChatMemberAdministrator::get_is_anonymous() const
    {
        return m_is_anonymous;
    }

    std::string ChatMemberAdministrator::get_status() const
    {
        return m_status;
    }

    User::ptr ChatMemberAdministrator::get_user() const
    {
        return m_user;
    }

    void ChatMemberAdministrator::set_can_be_edited(const bool &can_be_edited)
    {
        m_can_be_edited = can_be_edited;
    }

    void ChatMemberAdministrator::set_can_change_info(const bool &can_change_info)
    {
        m_can_change_info = can_change_info;
    }

    void ChatMemberAdministrator::set_can_delete_messages(const bool &can_delete_messages)
    {
        m_can_delete_messages = can_delete_messages;
    }

    void ChatMemberAdministrator::set_can_edit_messages(const std::optional<bool> &can_edit_messages)
    {
        m_can_edit_messages = can_edit_messages;
    }

    void ChatMemberAdministrator::set_can_invite_users(const bool &can_invite_users)
    {
        m_can_invite_users = can_invite_users;
    }

    void ChatMemberAdministrator::set_can_manage_chat(const bool &can_manage_chat)
    {
        m_can_manage_chat = can_manage_chat;
    }

    void ChatMemberAdministrator::set_can_manage_topics(const std::optional<bool> &can_manage_topics)
    {
        m_can_manage_topics = can_manage_topics;
    }

    void ChatMemberAdministrator::set_can_manage_video_chats(const bool &can_manage_video_chats)
    {
        m_can_manage_video_chats = can_manage_video_chats;
    }

    void ChatMemberAdministrator::set_can_pin_messages(const std::optional<bool> &can_pin_messages)
    {
        m_can_pin_messages = can_pin_messages;
    }

    void ChatMemberAdministrator::set_can_post_messages(const std::optional<bool> &can_post_messages)
    {
        m_can_post_messages = can_post_messages;
    }

    void ChatMemberAdministrator::set_can_promote_members(const bool &can_promote_members)
    {
        m_can_promote_members = can_promote_members;
    }

    void ChatMemberAdministrator::set_can_restrict_members(const bool &can_restrict_members)
    {
        m_can_restrict_members = can_restrict_members;
    }

    void ChatMemberAdministrator::set_custom_title(const std::optional<std::string> &custom_title)
    {
        m_custom_title = custom_title;
    }

    void ChatMemberAdministrator::set_is_anonymous(const bool &is_anonymous)
    {
        m_is_anonymous = is_anonymous;
    }

    void ChatMemberAdministrator::set_status(const std::string &status)
    {
        m_status = status;
    }

    void ChatMemberAdministrator::set_user(const User::ptr &user)
    {
        m_user = user;
    }
} //namespace tgbot
