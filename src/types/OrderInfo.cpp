#include "tgbot/types/OrderInfo.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    OrderInfo::OrderInfo() = default;

    OrderInfo::OrderInfo(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("email"))
            {
                if(doc["email"].is_string())
                {
                    m_email = doc["email"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("email"));
                }
            }

            if(doc.contains("name"))
            {
                if(doc["name"].is_string())
                {
                    m_name = doc["name"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("name"));
                }
            }

            if(doc.contains("phone_number"))
            {
                if(doc["phone_number"].is_string())
                {
                    m_phone_number = doc["phone_number"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("phone_number"));
                }
            }

            if(doc.contains("shipping_address"))
            {
                if(doc["shipping_address"].is_object())
                {
                    m_shipping_address = std::make_shared<ShippingAddress>(
                      doc["shipping_"
                          "addres"
                          "s"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("shipping_address"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string OrderInfo::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_email.has_value())
        {
            doc["email"] = m_email.value();
        }

        if(m_name.has_value())
        {
            doc["name"] = m_name.value();
        }

        if(m_phone_number.has_value())
        {
            doc["phone_number"] = m_phone_number.value();
        }

        if(m_shipping_address.has_value())
        {
            doc["shipping_address"] = nlohmann::json::parse(m_shipping_address.value()->serialise());
        }

        return doc.dump();
    }

    std::optional<std::string> OrderInfo::get_email() const
    {
        return m_email;
    }

    std::optional<std::string> OrderInfo::get_name() const
    {
        return m_name;
    }

    std::optional<std::string> OrderInfo::get_phone_number() const
    {
        return m_phone_number;
    }

    std::optional<ShippingAddress::ptr> OrderInfo::get_shipping_address() const
    {
        return m_shipping_address;
    }

    void OrderInfo::set_email(const std::optional<std::string> &email)
    {
        m_email = email;
    }

    void OrderInfo::set_name(const std::optional<std::string> &name)
    {
        m_name = name;
    }

    void OrderInfo::set_phone_number(const std::optional<std::string> &phone_number)
    {
        m_phone_number = phone_number;
    }

    void OrderInfo::set_shipping_address(const std::optional<ShippingAddress::ptr> &shipping_address)
    {
        m_shipping_address = shipping_address;
    }
} //namespace tgbot
