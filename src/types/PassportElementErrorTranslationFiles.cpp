#include "tgbot/types/PassportElementErrorTranslationFiles.h"

#include <algorithm>
#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>

#include "tgbot/constants/message.h"

namespace tgbot
{
    PassportElementErrorTranslationFiles::PassportElementErrorTranslationFiles() = default;

    PassportElementErrorTranslationFiles::PassportElementErrorTranslationFiles(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("file_hashes"))
            {
                if(doc["file_hashes"].is_array())
                {
                    std::vector<std::string> file_hashes;
                    file_hashes.reserve(doc["file_hashes"].size());

                    for(std::uint64_t i = 0; i < doc["file_hashes"].size(); ++i)
                    {
                        if(doc["file_hashes"].at(i).is_string())
                        {
                            file_hashes.emplace_back(doc["file_hashes"].at(i).get<std::string>());
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_string("file_hashes"));
                        }
                    }

                    m_file_hashes = file_hashes;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("file_hashes"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("file_hashes"));
            }

            if(doc.contains("message"))
            {
                if(doc["message"].is_string())
                {
                    m_message = doc["message"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("message"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("message"));
            }

            if(doc.contains("source"))
            {
                if(doc["source"].is_string())
                {
                    m_source = doc["source"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("source"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("source"));
            }

            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string PassportElementErrorTranslationFiles::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        {
            nlohmann::json file_hashes = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_file_hashes.size(); ++i)
            {
                file_hashes.emplace_back(m_file_hashes.at(i));
            }

            doc["file_hashes"] = file_hashes;
        }

        doc["message"] = m_message;
        doc["source"] = m_source;
        doc["type"] = m_type;
        return doc.dump();
    }

    std::vector<std::string> PassportElementErrorTranslationFiles::get_file_hashes() const
    {
        return m_file_hashes;
    }

    std::string PassportElementErrorTranslationFiles::get_message() const
    {
        return m_message;
    }

    std::string PassportElementErrorTranslationFiles::get_source() const
    {
        return m_source;
    }

    std::string PassportElementErrorTranslationFiles::get_type() const
    {
        return m_type;
    }

    void PassportElementErrorTranslationFiles::set_file_hashes(const std::vector<std::string> &file_hashes)
    {
        m_file_hashes = file_hashes;
    }

    void PassportElementErrorTranslationFiles::set_message(const std::string &message)
    {
        m_message = message;
    }

    void PassportElementErrorTranslationFiles::set_source(const std::string &source)
    {
        m_source = source;
    }

    void PassportElementErrorTranslationFiles::set_type(const std::string &type)
    {
        m_type = type;
    }
} //namespace tgbot
