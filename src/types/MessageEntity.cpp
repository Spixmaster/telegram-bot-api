#include "tgbot/types/MessageEntity.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    MessageEntity::MessageEntity() : m_length(-1), m_offset(-1)
    {}

    MessageEntity::MessageEntity(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("custom_emoji"))
            {
                if(doc["custom_emoji"].is_string())
                {
                    m_custom_emoji = doc["custom_emoji"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("custom_emoji"));
                }
            }

            if(doc.contains("language"))
            {
                if(doc["language"].is_string())
                {
                    m_language = doc["language"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("language"));
                }
            }

            if(doc.contains("length"))
            {
                if(doc["length"].is_number_integer())
                {
                    m_length = doc["length"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("length"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("length"));
            }

            if(doc.contains("offset"))
            {
                if(doc["offset"].is_number_integer())
                {
                    m_offset = doc["offset"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("offset"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("offset"));
            }

            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }

            if(doc.contains("url"))
            {
                if(doc["url"].is_string())
                {
                    m_url = doc["url"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("url"));
                }
            }

            if(doc.contains("user"))
            {
                if(doc["user"].is_object())
                {
                    m_user = std::make_shared<User>(doc["user"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("user"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string MessageEntity::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_custom_emoji.has_value())
        {
            doc["custom_emoji"] = m_custom_emoji.value();
        }

        if(m_language.has_value())
        {
            doc["language"] = m_language.value();
        }

        doc["length"] = m_length;
        doc["offset"] = m_offset;
        doc["type"] = m_type;

        if(m_url.has_value())
        {
            doc["url"] = m_url.value();
        }

        if(m_user.has_value())
        {
            doc["user"] = nlohmann::json::parse(m_user.value()->serialise());
        }

        return doc.dump();
    }

    std::optional<std::string> MessageEntity::get_custom_emoji() const
    {
        return m_custom_emoji;
    }

    std::optional<std::string> MessageEntity::get_language() const
    {
        return m_language;
    }

    std::int32_t MessageEntity::get_length() const
    {
        return m_length;
    }

    std::int32_t MessageEntity::get_offset() const
    {
        return m_offset;
    }

    std::string MessageEntity::get_type() const
    {
        return m_type;
    }

    std::optional<std::string> MessageEntity::get_url() const
    {
        return m_url;
    }

    std::optional<User::ptr> MessageEntity::get_user() const
    {
        return m_user;
    }

    void MessageEntity::set_custom_emoji(const std::optional<std::string> &custom_emoji)
    {
        m_custom_emoji = custom_emoji;
    }

    void MessageEntity::set_language(const std::optional<std::string> &language)
    {
        m_language = language;
    }

    void MessageEntity::set_length(const std::int32_t &length)
    {
        m_length = length;
    }

    void MessageEntity::set_offset(const std::int32_t &offset)
    {
        m_offset = offset;
    }

    void MessageEntity::set_type(const std::string &type)
    {
        m_type = type;
    }

    void MessageEntity::set_url(const std::optional<std::string> &url)
    {
        m_url = url;
    }

    void MessageEntity::set_user(const std::optional<User::ptr> &user)
    {
        m_user = user;
    }
} //namespace tgbot
