#include "tgbot/types/ChosenInlineResult.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    ChosenInlineResult::ChosenInlineResult() : m_from(std::make_shared<User>())
    {}

    ChosenInlineResult::ChosenInlineResult(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("from"))
            {
                if(doc["from"].is_object())
                {
                    m_from = std::make_shared<User>(doc["from"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("from"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("from"));
            }

            if(doc.contains("inline_message_id"))
            {
                if(doc["inline_message_id"].is_string())
                {
                    m_inline_message_id = doc["inline_message_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("inline_message_id"));
                }
            }

            if(doc.contains("location"))
            {
                if(doc["location"].is_object())
                {
                    m_location = std::make_shared<Location>(doc["location"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("location"));
                }
            }

            if(doc.contains("query"))
            {
                if(doc["query"].is_string())
                {
                    m_query = doc["query"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("query"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("query"));
            }

            if(doc.contains("result_id"))
            {
                if(doc["result_id"].is_string())
                {
                    m_result_id = doc["result_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("result_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("result_id"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string ChosenInlineResult::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["from"] = nlohmann::json::parse(m_from->serialise());

        if(m_inline_message_id.has_value())
        {
            doc["inline_message_id"] = m_inline_message_id.value();
        }

        if(m_location.has_value())
        {
            doc["location"] = nlohmann::json::parse(m_location.value()->serialise());
        }

        doc["query"] = m_query;
        doc["result_id"] = m_result_id;
        return doc.dump();
    }

    User::ptr ChosenInlineResult::get_from() const
    {
        return m_from;
    }

    std::optional<std::string> ChosenInlineResult::get_inline_message_id() const
    {
        return m_inline_message_id;
    }

    std::optional<Location::ptr> ChosenInlineResult::get_location() const
    {
        return m_location;
    }

    std::string ChosenInlineResult::get_query() const
    {
        return m_query;
    }

    std::string ChosenInlineResult::get_result_id() const
    {
        return m_result_id;
    }

    void ChosenInlineResult::set_from(const User::ptr &from)
    {
        m_from = from;
    }

    void ChosenInlineResult::set_inline_message_id(const std::optional<std::string> &inline_message_id)
    {
        m_inline_message_id = inline_message_id;
    }

    void ChosenInlineResult::set_location(const std::optional<Location::ptr> &location)
    {
        m_location = location;
    }

    void ChosenInlineResult::set_query(const std::string &query)
    {
        m_query = query;
    }

    void ChosenInlineResult::set_result_id(const std::string &result_id)
    {
        m_result_id = result_id;
    }
} //namespace tgbot
