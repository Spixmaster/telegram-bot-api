#include "tgbot/types/ShippingQuery.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    ShippingQuery::ShippingQuery() :
        m_from(std::make_shared<User>()), m_shipping_address(std::make_shared<ShippingAddress>())
    {}

    ShippingQuery::ShippingQuery(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("from"))
            {
                if(doc["from"].is_object())
                {
                    m_from = std::make_shared<User>(doc["from"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("from"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("from"));
            }

            if(doc.contains("id"))
            {
                if(doc["id"].is_string())
                {
                    m_id = doc["id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("id"));
            }

            if(doc.contains("invoice_payload"))
            {
                if(doc["invoice_payload"].is_string())
                {
                    m_invoice_payload = doc["invoice_payload"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("invoice_payload"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("invoice_payload"));
            }

            if(doc.contains("shipping_address"))
            {
                if(doc["shipping_address"].is_object())
                {
                    m_shipping_address = std::make_shared<ShippingAddress>(
                      doc["shipping_"
                          "addres"
                          "s"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("shipping_address"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("shipping_address"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string ShippingQuery::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["from"] = nlohmann::json::parse(m_from->serialise());
        doc["id"] = m_id;
        doc["invoice_payload"] = m_invoice_payload;
        doc["shipping_address"] = nlohmann::json::parse(m_shipping_address->serialise());
        return doc.dump();
    }

    User::ptr ShippingQuery::get_from() const
    {
        return m_from;
    }

    std::string ShippingQuery::get_id() const
    {
        return m_id;
    }

    std::string ShippingQuery::get_invoice_payload() const
    {
        return m_invoice_payload;
    }

    ShippingAddress::ptr ShippingQuery::get_shipping_address() const
    {
        return m_shipping_address;
    }

    void ShippingQuery::set_from(const User::ptr &from)
    {
        m_from = from;
    }

    void ShippingQuery::set_id(const std::string &id)
    {
        m_id = id;
    }

    void ShippingQuery::set_invoice_payload(const std::string &invoice_payload)
    {
        m_invoice_payload = invoice_payload;
    }

    void ShippingQuery::set_shipping_address(const ShippingAddress::ptr &shipping_address)
    {
        m_shipping_address = shipping_address;
    }
} //namespace tgbot
