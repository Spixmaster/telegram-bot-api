#include "tgbot/types/InlineQueryResultPhoto.h"

#include <algorithm>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "tgbot/constants/message.h"

namespace tgbot
{
    InlineQueryResultPhoto::InlineQueryResultPhoto() = default;

    InlineQueryResultPhoto::InlineQueryResultPhoto(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("caption"))
            {
                if(doc["caption"].is_string())
                {
                    m_caption = doc["caption"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("caption"));
                }
            }

            if(doc.contains("caption_entities"))
            {
                if(doc["caption_entities"].is_array())
                {
                    std::vector<MessageEntity::ptr> caption_entities;
                    caption_entities.reserve(doc["caption_entities"].size());

                    for(std::uint64_t i = 0; i < doc["caption_entities"].size(); ++i)
                    {
                        if(doc["caption_entities"].at(i).is_object())
                        {
                            caption_entities.emplace_back(
                              std::make_shared<MessageEntity>(doc["caption_entities"].at(i).dump()));
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_object(
                              "caption_"
                              "entitie"
                              "s"));
                        }
                    }

                    m_caption_entities = caption_entities;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("caption_entities"));
                }
            }

            if(doc.contains("description"))
            {
                if(doc["description"].is_string())
                {
                    m_description = doc["description"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("description"));
                }
            }

            if(doc.contains("id"))
            {
                if(doc["id"].is_string())
                {
                    m_id = doc["id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("id"));
            }

            if(doc.contains("input_message_content"))
            {
                if(doc["input_message_content"].is_object())
                {
                    m_input_message_content = std::make_shared<InputMessageContent>(doc["input_message_content"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("input_message_content"));
                }
            }

            if(doc.contains("parse_mode"))
            {
                if(doc["parse_mode"].is_string())
                {
                    m_parse_mode = doc["parse_mode"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("parse_mode"));
                }
            }

            if(doc.contains("photo_height"))
            {
                if(doc["photo_height"].is_number_integer())
                {
                    m_photo_height = doc["photo_height"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("photo_height"));
                }
            }

            if(doc.contains("photo_url"))
            {
                if(doc["photo_url"].is_string())
                {
                    m_photo_url = doc["photo_url"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("photo_url"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("photo_url"));
            }

            if(doc.contains("photo_width"))
            {
                if(doc["photo_width"].is_number_integer())
                {
                    m_photo_width = doc["photo_width"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("photo_width"));
                }
            }

            if(doc.contains("reply_markup"))
            {
                if(doc["reply_markup"].is_object())
                {
                    m_reply_markup = std::make_shared<InlineKeyboardMarkup>(
                      doc["reply_"
                          "marku"
                          "p"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("reply_markup"));
                }
            }

            if(doc.contains("thumbnail_url"))
            {
                if(doc["thumbnail_url"].is_string())
                {
                    m_thumbnail_url = doc["thumbnail_url"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("thumbnail_url"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("thumbnail_url"));
            }

            if(doc.contains("title"))
            {
                if(doc["title"].is_string())
                {
                    m_title = doc["title"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("title"));
                }
            }

            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string InlineQueryResultPhoto::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_caption.has_value())
        {
            doc["caption"] = m_caption.value();
        }

        if(m_caption_entities.has_value())
        {
            nlohmann::json caption_entities = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_caption_entities.value().size(); ++i)
            {
                caption_entities.emplace_back(nlohmann::json::parse(m_caption_entities.value().at(i)->serialise()));
            }

            doc["caption_entities"] = caption_entities;
        }

        if(m_description.has_value())
        {
            doc["description"] = m_description.value();
        }

        doc["id"] = m_id;

        if(m_input_message_content.has_value())
        {
            doc["input_message_content"] = nlohmann::json::parse(m_input_message_content.value()->serialise());
        }

        if(m_parse_mode.has_value())
        {
            doc["parse_mode"] = m_parse_mode.value();
        }

        if(m_photo_height.has_value())
        {
            doc["photo_height"] = m_photo_height.value();
        }

        doc["photo_url"] = m_photo_url;

        if(m_photo_width.has_value())
        {
            doc["photo_width"] = m_photo_width.value();
        }

        if(m_reply_markup.has_value())
        {
            doc["reply_markup"] = nlohmann::json::parse(m_reply_markup.value()->serialise());
        }

        doc["thumbnail_url"] = m_thumbnail_url;

        if(m_title.has_value())
        {
            doc["title"] = m_title.value();
        }

        doc["type"] = m_type;
        return doc.dump();
    }

    std::optional<std::string> InlineQueryResultPhoto::get_caption() const
    {
        return m_caption;
    }

    std::optional<std::vector<MessageEntity::ptr>> InlineQueryResultPhoto::get_caption_entities() const
    {
        return m_caption_entities;
    }

    std::optional<std::string> InlineQueryResultPhoto::get_description() const
    {
        return m_description;
    }

    std::string InlineQueryResultPhoto::get_id() const
    {
        return m_id;
    }

    std::optional<InputMessageContent::ptr> InlineQueryResultPhoto::get_input_message_content() const
    {
        return m_input_message_content;
    }

    std::optional<std::string> InlineQueryResultPhoto::get_parse_mode() const
    {
        return m_parse_mode;
    }

    std::optional<std::int32_t> InlineQueryResultPhoto::get_photo_height() const
    {
        return m_photo_height;
    }

    std::string InlineQueryResultPhoto::get_photo_url() const
    {
        return m_photo_url;
    }

    std::optional<std::int32_t> InlineQueryResultPhoto::get_photo_width() const
    {
        return m_photo_width;
    }

    std::optional<InlineKeyboardMarkup::ptr> InlineQueryResultPhoto::get_reply_markup() const
    {
        return m_reply_markup;
    }

    std::string InlineQueryResultPhoto::get_thumbnail_url() const
    {
        return m_thumbnail_url;
    }

    std::optional<std::string> InlineQueryResultPhoto::get_title() const
    {
        return m_title;
    }

    std::string InlineQueryResultPhoto::get_type() const
    {
        return m_type;
    }

    void InlineQueryResultPhoto::set_caption(const std::optional<std::string> &caption)
    {
        m_caption = caption;
    }

    void InlineQueryResultPhoto::set_caption_entities(
      const std::optional<std::vector<MessageEntity::ptr>> &caption_entities)
    {
        m_caption_entities = caption_entities;
    }

    void InlineQueryResultPhoto::set_description(const std::optional<std::string> &description)
    {
        m_description = description;
    }

    void InlineQueryResultPhoto::set_id(const std::string &id)
    {
        m_id = id;
    }

    void InlineQueryResultPhoto::set_input_message_content(
      const std::optional<InputMessageContent::ptr> &input_message_content)
    {
        m_input_message_content = input_message_content;
    }

    void InlineQueryResultPhoto::set_parse_mode(const std::optional<std::string> &parse_mode)
    {
        m_parse_mode = parse_mode;
    }

    void InlineQueryResultPhoto::set_photo_height(const std::optional<std::int32_t> &photo_height)
    {
        m_photo_height = photo_height;
    }

    void InlineQueryResultPhoto::set_photo_url(const std::string &photo_url)
    {
        m_photo_url = photo_url;
    }

    void InlineQueryResultPhoto::set_photo_width(const std::optional<std::int32_t> &photo_width)
    {
        m_photo_width = photo_width;
    }

    void InlineQueryResultPhoto::set_reply_markup(const std::optional<InlineKeyboardMarkup::ptr> &reply_markup)
    {
        m_reply_markup = reply_markup;
    }

    void InlineQueryResultPhoto::set_thumbnail_url(const std::string &thumbnail_url)
    {
        m_thumbnail_url = thumbnail_url;
    }

    void InlineQueryResultPhoto::set_title(const std::optional<std::string> &title)
    {
        m_title = title;
    }

    void InlineQueryResultPhoto::set_type(const std::string &type)
    {
        m_type = type;
    }
} //namespace tgbot
