#include "tgbot/types/EncryptedPassportElement.h"

#include <algorithm>
#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "tgbot/constants/message.h"

namespace tgbot
{
    EncryptedPassportElement::EncryptedPassportElement() = default;

    EncryptedPassportElement::EncryptedPassportElement(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("data"))
            {
                if(doc["data"].is_string())
                {
                    m_data = doc["data"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("data"));
                }
            }

            if(doc.contains("email"))
            {
                if(doc["email"].is_string())
                {
                    m_email = doc["email"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("email"));
                }
            }

            if(doc.contains("files"))
            {
                if(doc["files"].is_array())
                {
                    std::vector<tgbot::PassportFile::ptr> files;
                    files.reserve(doc["files"].size());

                    for(std::uint64_t i = 0; i < doc["files"].size(); ++i)
                    {
                        if(doc["files"].at(i).is_object())
                        {
                            files.emplace_back(std::make_shared<PassportFile>(doc["files"].at(i).dump()));
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_object("files"));
                        }
                    }

                    m_files = files;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("files"));
                }
            }

            if(doc.contains("front_side"))
            {
                if(doc["front_side"].is_object())
                {
                    m_front_side = std::make_shared<PassportFile>(doc["front_side"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("front_side"));
                }
            }

            if(doc.contains("hash"))
            {
                if(doc["hash"].is_string())
                {
                    m_hash = doc["hash"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("hash"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("hash"));
            }

            if(doc.contains("phone_number"))
            {
                if(doc["phone_number"].is_string())
                {
                    m_phone_number = doc["phone_number"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("phone_number"));
                }
            }

            if(doc.contains("reverse_side"))
            {
                if(doc["reverse_side"].is_object())
                {
                    m_reverse_side = std::make_shared<PassportFile>(doc["reverse_side"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("reverse_side"));
                }
            }

            if(doc.contains("selfie"))
            {
                if(doc["selfie"].is_object())

                {
                    m_selfie = std::make_shared<PassportFile>(doc["selfie"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("selfie"));
                }
            }

            if(doc.contains("translation"))
            {
                if(doc["translation"].is_array())
                {
                    std::vector<tgbot::PassportFile::ptr> translation;
                    translation.reserve(doc["translation"].size());

                    for(std::uint64_t i = 0; i < doc["translation"].size(); ++i)
                    {
                        if(doc["translation"].at(i).is_object())
                        {
                            translation.emplace_back(std::make_shared<PassportFile>(doc["translation"].at(i).dump()));
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_object(
                              "translati"
                              "on"));
                        }
                    }

                    m_translation = translation;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("translation"));
                }
            }

            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string EncryptedPassportElement::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_data.has_value())
        {
            doc["data"] = m_data.value();
        }

        if(m_email.has_value())
        {
            doc["email"] = m_email.value();
        }

        if(m_files.has_value())
        {
            nlohmann::json files = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_files.value().size(); ++i)
            {
                files.emplace_back(nlohmann::json::parse(m_files.value().at(i)->serialise()));
            }

            doc["files"] = files;
        }

        if(m_front_side.has_value())
        {
            doc["front_side"] = nlohmann::json::parse(m_front_side.value()->serialise());
        }

        doc["hash"] = m_hash;

        if(m_phone_number.has_value())
        {
            doc["phone_number"] = m_phone_number.value();
        }

        if(m_reverse_side.has_value())
        {
            doc["reverse_side"] = nlohmann::json::parse(m_reverse_side.value()->serialise());
        }

        if(m_selfie.has_value())
        {
            doc["selfie"] = nlohmann::json::parse(m_selfie.value()->serialise());
        }

        if(m_translation.has_value())
        {
            nlohmann::json translation = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_translation.value().size(); ++i)
            {
                translation.emplace_back(nlohmann::json::parse(m_translation.value().at(i)->serialise()));
            }

            doc["translation"] = translation;
        }

        doc["type"] = m_type;
        return doc.dump();
    }

    std::optional<std::string> EncryptedPassportElement::get_data() const
    {
        return m_data;
    }

    std::optional<std::string> EncryptedPassportElement::get_email() const
    {
        return m_email;
    }

    std::optional<std::vector<PassportFile::ptr>> EncryptedPassportElement::get_files() const
    {
        return m_files;
    }

    std::optional<PassportFile::ptr> EncryptedPassportElement::get_front_side() const
    {
        return m_front_side;
    }

    std::string EncryptedPassportElement::get_hash() const
    {
        return m_hash;
    }

    std::optional<std::string> EncryptedPassportElement::get_phone_number() const
    {
        return m_phone_number;
    }

    std::optional<PassportFile::ptr> EncryptedPassportElement::get_reverse_side() const
    {
        return m_reverse_side;
    }

    std::optional<PassportFile::ptr> EncryptedPassportElement::get_selfie() const
    {
        return m_selfie;
    }

    std::optional<std::vector<PassportFile::ptr>> EncryptedPassportElement::get_translation() const
    {
        return m_translation;
    }

    std::string EncryptedPassportElement::get_type() const
    {
        return m_type;
    }

    void EncryptedPassportElement::set_data(const std::optional<std::string> &data)
    {
        m_data = data;
    }

    void EncryptedPassportElement::set_email(const std::optional<std::string> &email)
    {
        m_email = email;
    }

    void EncryptedPassportElement::set_files(const std::optional<std::vector<PassportFile::ptr>> &files)
    {
        m_files = files;
    }

    void EncryptedPassportElement::set_front_side(const std::optional<PassportFile::ptr> &front_side)
    {
        m_front_side = front_side;
    }

    void EncryptedPassportElement::set_hash(const std::string &hash)
    {
        m_hash = hash;
    }

    void EncryptedPassportElement::set_phone_number(const std::optional<std::string> &phone_number)
    {
        m_phone_number = phone_number;
    }

    void EncryptedPassportElement::set_reverse_side(const std::optional<PassportFile::ptr> &reverse_side)
    {
        m_reverse_side = reverse_side;
    }

    void EncryptedPassportElement::set_selfie(const std::optional<PassportFile::ptr> &selfie)
    {
        m_selfie = selfie;
    }

    void EncryptedPassportElement::set_translation(const std::optional<std::vector<PassportFile::ptr>> &translation)
    {
        m_translation = translation;
    }

    void EncryptedPassportElement::set_type(const std::string &type)
    {
        m_type = type;
    }
} //namespace tgbot
