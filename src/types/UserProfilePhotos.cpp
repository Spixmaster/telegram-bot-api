#include "tgbot/types/UserProfilePhotos.h"

#include <algorithm>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>

#include "tgbot/constants/message.h"

namespace tgbot
{
    UserProfilePhotos::UserProfilePhotos() : m_total_count(-1)
    {}

    UserProfilePhotos::UserProfilePhotos(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("photos"))
            {
                if(doc["photos"].is_array())
                {
                    std::vector<std::vector<PhotoSize::ptr>> photos;
                    photos.resize(doc["photos"].size());

                    for(std::uint64_t i = 0; i < doc["photos"].size(); ++i)
                    {
                        if(doc["photos"].at(i).is_array())
                        {
                            photos.at(i).reserve(doc["photos"].at(i).size());

                            for(std::uint64_t j = 0; j < doc["photos"].at(i).size(); ++j)
                            {
                                if(doc["photos"].at(i).at(j).is_object())
                                {
                                    photos.at(i).emplace_back(
                                      std::make_shared<PhotoSize>(doc["photos"].at(i).at(j).dump()));
                                }
                                else
                                {
                                    throw std::invalid_argument(message::json::value::nested::not_object("photos"));
                                }
                            }
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_array("photos"));
                        }
                    }

                    m_photos = photos;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("photos"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("photos"));
            }

            if(doc.contains("total_count"))
            {
                if(doc["total_count"].is_number_integer())
                {
                    m_total_count = doc["total_count"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("total_count"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("total_count"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string UserProfilePhotos::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        {
            nlohmann::json photos = nlohmann::json::array();

            if(m_photos.empty())
            {
                photos.emplace_back(nlohmann::json::array());
            }
            else
            {
                for(std::uint64_t i = 0; i < m_photos.size(); ++i)
                {
                    nlohmann::json photo = nlohmann::json::array();

                    for(std::uint64_t j = 0; j < m_photos.at(i).size(); ++j)
                    {
                        photo.emplace_back(nlohmann::json::parse(m_photos.at(i).at(j)->serialise()));
                    }

                    photos.emplace_back(photo);
                }
            }

            doc["photos"] = photos;
        }

        doc["total_count"] = m_total_count;
        return doc.dump();
    }

    std::vector<std::vector<PhotoSize::ptr>> UserProfilePhotos::get_photos() const
    {
        return m_photos;
    }

    std::int32_t UserProfilePhotos::get_total_count() const
    {
        return m_total_count;
    }

    void UserProfilePhotos::set_photos(const std::vector<std::vector<PhotoSize::ptr>> &photos)
    {
        m_photos = photos;
    }

    void UserProfilePhotos::set_total_count(const std::int32_t &total_count)
    {
        m_total_count = total_count;
    }
} //namespace tgbot
