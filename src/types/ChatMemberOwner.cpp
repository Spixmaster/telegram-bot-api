#include "tgbot/types/ChatMemberOwner.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    ChatMemberOwner::ChatMemberOwner() : m_is_anonymous(false), m_user(std::make_shared<User>())
    {}

    ChatMemberOwner::ChatMemberOwner(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("custom_title"))
            {
                if(doc["custom_title"].is_string())
                {
                    m_custom_title = doc["custom_title"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("custom_title"));
                }
            }

            if(doc.contains("is_anonymous"))
            {
                if(doc["is_anonymous"].is_boolean())
                {
                    m_is_anonymous = doc["is_anonymous"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("is_anonymous"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("is_anonymous"));
            }

            if(doc.contains("status"))
            {
                if(doc["status"].is_string())
                {
                    m_status = doc["status"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("status"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("status"));
            }

            if(doc.contains("user"))
            {
                if(doc["user"].is_object())
                {
                    m_user = std::make_shared<User>(doc["user"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("user"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("user"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string ChatMemberOwner::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_custom_title.has_value())
        {
            doc["custom_title"] = m_custom_title.value();
        }

        doc["is_anonymous"] = m_is_anonymous;
        doc["status"] = m_status;
        doc["user"] = nlohmann::json::parse(m_user->serialise());
        return doc.dump();
    }

    std::optional<std::string> ChatMemberOwner::get_custom_title() const
    {
        return m_custom_title;
    }

    bool ChatMemberOwner::get_is_anonymous() const
    {
        return m_is_anonymous;
    }

    std::string ChatMemberOwner::get_status() const
    {
        return m_status;
    }

    User::ptr ChatMemberOwner::get_user() const
    {
        return m_user;
    }

    void ChatMemberOwner::set_custom_title(const std::optional<std::string> &custom_title)
    {
        m_custom_title = custom_title;
    }

    void ChatMemberOwner::set_is_anonymous(const bool &is_anonymous)
    {
        m_is_anonymous = is_anonymous;
    }

    void ChatMemberOwner::set_status(const std::string &status)
    {
        m_status = status;
    }

    void ChatMemberOwner::set_user(const User::ptr &user)
    {
        m_user = user;
    }
} //namespace tgbot
