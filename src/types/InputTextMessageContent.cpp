#include "tgbot/types/InputTextMessageContent.h"

#include <algorithm>
#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "tgbot/constants/message.h"

namespace tgbot
{
    InputTextMessageContent::InputTextMessageContent() = default;

    InputTextMessageContent::InputTextMessageContent(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("disable_web_page_preview"))
            {
                if(doc["disable_web_page_preview"].is_boolean())
                {
                    m_disable_web_page_preview = doc["disable_web_page_preview"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("disable_web_page_preview"));
                }
            }

            if(doc.contains("entities"))
            {
                if(doc["entities"].is_array())
                {
                    std::vector<MessageEntity::ptr> caption_entities;
                    caption_entities.reserve(doc["entities"].size());

                    for(std::uint64_t i = 0; i < doc["entities"].size(); ++i)
                    {
                        if(doc["entities"].at(i).is_object())
                        {
                            caption_entities.emplace_back(std::make_shared<MessageEntity>(doc["entities"].at(i).dump()));
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_object(
                              "entitie"
                              "s"));
                        }
                    }

                    m_entities = caption_entities;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("entities"));
                }
            }

            if(doc.contains("message_text"))
            {
                if(doc["message_text"].is_string())
                {
                    m_message_text = doc["message_text"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("message_text"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("message_text"));
            }

            if(doc.contains("parse_mode"))
            {
                if(doc["parse_mode"].is_string())
                {
                    m_parse_mode = doc["parse_mode"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("parse_mode"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string InputTextMessageContent::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_disable_web_page_preview.has_value())
        {
            doc["disable_web_page_preview"] = m_disable_web_page_preview.value();
        }

        if(m_entities.has_value())
        {
            nlohmann::json entities = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_entities.value().size(); ++i)
            {
                entities.emplace_back(nlohmann::json::parse(m_entities.value().at(i)->serialise()));
            }

            doc["entities"] = entities;
        }

        doc["message_text"] = m_message_text;

        if(m_parse_mode.has_value())
        {
            doc["parse_mode"] = m_parse_mode.value();
        }

        return doc.dump();
    }

    std::optional<bool> InputTextMessageContent::get_disable_web_page_preview() const
    {
        return m_disable_web_page_preview;
    }

    std::optional<std::vector<MessageEntity::ptr>> InputTextMessageContent::get_entities() const
    {
        return m_entities;
    }

    std::string InputTextMessageContent::get_message_text() const
    {
        return m_message_text;
    }

    std::optional<std::string> InputTextMessageContent::get_parse_mode() const
    {
        return m_parse_mode;
    }

    void InputTextMessageContent::set_disable_web_page_preview(const std::optional<bool> &disable_web_page_preview)
    {
        m_disable_web_page_preview = disable_web_page_preview;
    }

    void InputTextMessageContent::set_entities(const std::optional<std::vector<MessageEntity::ptr>> &entities)
    {
        m_entities = entities;
    }

    void InputTextMessageContent::set_message_text(const std::string &message_text)
    {
        m_message_text = message_text;
    }

    void InputTextMessageContent::set_parse_mode(const std::optional<std::string> &parse_mode)
    {
        m_parse_mode = parse_mode;
    }
} //namespace tgbot
