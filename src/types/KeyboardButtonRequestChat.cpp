#include "tgbot/types/KeyboardButtonRequestChat.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    KeyboardButtonRequestChat::KeyboardButtonRequestChat() : m_chat_is_channel(false), m_request_id(-1)
    {}

    KeyboardButtonRequestChat::KeyboardButtonRequestChat(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("bot_administrator_rights"))
            {
                if(doc["bot_administrator_rights"].is_object())
                {
                    m_bot_administrator_rights =
                      std::make_shared<ChatAdministratorRights>(doc["bot_administrator_rights"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("bot_administrator_rights"));
                }
            }

            if(doc.contains("bot_is_member"))
            {
                if(doc["bot_is_member"].is_boolean())
                {
                    m_bot_is_member = doc["bot_is_member"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("bot_is_member"));
                }
            }

            if(doc.contains("chat_has_username"))
            {
                if(doc["chat_has_username"].is_boolean())
                {
                    m_chat_has_username = doc["chat_has_username"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("chat_has_username"));
                }
            }

            if(doc.contains("chat_is_channel"))
            {
                if(doc["chat_is_channel"].is_boolean())
                {
                    m_chat_is_channel = doc["chat_is_channel"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("chat_is_channel"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("chat_is_channel"));
            }

            if(doc.contains("chat_is_created"))
            {
                if(doc["chat_is_created"].is_boolean())
                {
                    m_chat_is_created = doc["chat_is_created"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("chat_is_created"));
                }
            }

            if(doc.contains("chat_is_forum"))
            {
                if(doc["chat_is_forum"].is_boolean())
                {
                    m_chat_is_forum = doc["chat_is_forum"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("chat_is_forum"));
                }
            }

            if(doc.contains("request_id"))
            {
                if(doc["request_id"].is_number_integer())
                {
                    m_request_id = doc["request_id"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("request_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("request_id"));
            }

            if(doc.contains("user_administrator_rights"))
            {
                if(doc["user_administrator_rights"].is_object())
                {
                    m_user_administrator_rights =
                      std::make_shared<ChatAdministratorRights>(doc["user_administrator_rights"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("user_administrator_rights"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string KeyboardButtonRequestChat::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_bot_administrator_rights.has_value())
        {
            doc["bot_administrator_rights"] = nlohmann::json::parse(m_bot_administrator_rights.value()->serialise());
        }

        if(m_bot_is_member.has_value())
        {
            doc["bot_is_member"] = m_bot_is_member.value();
        }

        if(m_chat_has_username.has_value())
        {
            doc["chat_has_username"] = m_chat_has_username.value();
        }

        doc["chat_is_channel"] = m_chat_is_channel;

        if(m_chat_is_created.has_value())
        {
            doc["chat_is_created"] = m_chat_is_created.value();
        }

        if(m_chat_is_forum.has_value())
        {
            doc["chat_is_forum"] = m_chat_is_forum.value();
        }

        doc["request_id"] = m_request_id;

        if(m_user_administrator_rights.has_value())
        {
            doc["user_administrator_rights"] = nlohmann::json::parse(m_user_administrator_rights.value()->serialise());
        }

        return doc.dump();
    }

    std::optional<ChatAdministratorRights::ptr> KeyboardButtonRequestChat::get_bot_administrator_rights() const
    {
        return m_bot_administrator_rights;
    }

    std::optional<bool> KeyboardButtonRequestChat::get_bot_is_member() const
    {
        return m_bot_is_member;
    }

    std::optional<bool> KeyboardButtonRequestChat::get_chat_has_username() const
    {
        return m_chat_has_username;
    }

    bool KeyboardButtonRequestChat::get_chat_is_channel() const
    {
        return m_chat_is_channel;
    }

    std::optional<bool> KeyboardButtonRequestChat::get_chat_is_created() const
    {
        return m_chat_is_created;
    }

    std::optional<bool> KeyboardButtonRequestChat::get_chat_is_forum() const
    {
        return m_chat_is_forum;
    }

    std::int32_t KeyboardButtonRequestChat::get_request_id() const
    {
        return m_request_id;
    }

    std::optional<ChatAdministratorRights::ptr> KeyboardButtonRequestChat::get_user_administrator_rights() const
    {
        return m_user_administrator_rights;
    }

    void KeyboardButtonRequestChat::set_bot_administrator_rights(
      const std::optional<ChatAdministratorRights::ptr> &bot_administrator_rights)
    {
        m_bot_administrator_rights = bot_administrator_rights;
    }

    void KeyboardButtonRequestChat::set_bot_is_member(const std::optional<bool> &bot_is_member)
    {
        m_bot_is_member = bot_is_member;
    }

    void KeyboardButtonRequestChat::set_chat_has_username(const std::optional<bool> &chat_has_username)
    {
        m_chat_has_username = chat_has_username;
    }

    void KeyboardButtonRequestChat::set_chat_is_channel(const bool &chat_is_channel)
    {
        m_chat_is_channel = chat_is_channel;
    }

    void KeyboardButtonRequestChat::set_chat_is_created(const std::optional<bool> &chat_is_created)
    {
        m_chat_is_created = chat_is_created;
    }

    void KeyboardButtonRequestChat::set_chat_is_forum(const std::optional<bool> &chat_is_forum)
    {
        m_chat_is_forum = chat_is_forum;
    }

    void KeyboardButtonRequestChat::set_request_id(const std::int32_t &request_id)
    {
        m_request_id = request_id;
    }

    void KeyboardButtonRequestChat::set_user_administrator_rights(
      const std::optional<ChatAdministratorRights::ptr> &user_administrator_rights)
    {
        m_user_administrator_rights = user_administrator_rights;
    }
} //namespace tgbot
