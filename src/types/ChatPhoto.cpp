#include "tgbot/types/ChatPhoto.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    ChatPhoto::ChatPhoto() = default;

    ChatPhoto::ChatPhoto(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("big_file_id"))
            {
                if(doc["big_file_id"].is_string())
                {
                    m_big_file_id = doc["big_file_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("big_file_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("big_file_id"));
            }

            if(doc.contains("big_file_unique_id"))
            {
                if(doc["big_file_unique_id"].is_string())
                {
                    m_big_file_unique_id = doc["big_file_unique_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("big_file_unique_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("big_file_unique_id"));
            }

            if(doc.contains("small_file_id"))
            {
                if(doc["small_file_id"].is_string())
                {
                    m_small_file_id = doc["small_file_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("small_file_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("small_file_id"));
            }

            if(doc.contains("small_file_unique_id"))
            {
                if(doc["small_file_unique_id"].is_string())
                {
                    m_small_file_unique_id = doc["small_file_unique_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("small_file_unique_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("small_file_unique_id"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string ChatPhoto::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["big_file_id"] = m_big_file_id;
        doc["big_file_unique_id"] = m_big_file_unique_id;
        doc["small_file_id"] = m_small_file_id;
        doc["small_file_unique_id"] = m_small_file_unique_id;
        return doc.dump();
    }

    std::string ChatPhoto::get_big_file_id() const
    {
        return m_big_file_id;
    }

    std::string ChatPhoto::get_big_file_unique_id() const
    {
        return m_big_file_unique_id;
    }

    std::string ChatPhoto::get_small_file_id() const
    {
        return m_small_file_id;
    }

    std::string ChatPhoto::get_small_file_unique_id() const
    {
        return m_small_file_unique_id;
    }

    void ChatPhoto::set_big_file_id(const std::string &big_file_id)
    {
        m_big_file_id = big_file_id;
    }

    void ChatPhoto::set_big_file_unique_id(const std::string &big_file_unique_id)
    {
        m_big_file_unique_id = big_file_unique_id;
    }

    void ChatPhoto::set_small_file_id(const std::string &small_file_id)
    {
        m_small_file_id = small_file_id;
    }

    void ChatPhoto::set_small_file_unique_id(const std::string &small_file_unique_id)
    {
        m_small_file_unique_id = small_file_unique_id;
    }
} //namespace tgbot
