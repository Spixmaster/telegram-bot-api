#include "tgbot/types/VideoChatParticipantsInvited.h"

#include <algorithm>
#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>

#include "tgbot/constants/message.h"

namespace tgbot
{
    VideoChatParticipantsInvited::VideoChatParticipantsInvited() = default;

    VideoChatParticipantsInvited::VideoChatParticipantsInvited(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("users"))
            {
                if(doc["users"].is_array())
                {
                    std::vector<User::ptr> users;
                    users.reserve(doc["users"].size());

                    for(std::uint64_t i = 0; i < doc["users"].size(); ++i)
                    {
                        if(doc["users"].at(i).is_object())
                        {
                            users.emplace_back(std::make_shared<User>(doc["users"].at(i).dump()));
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_object("users"));
                        }
                    }

                    m_users = users;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("users"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("users"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string VideoChatParticipantsInvited::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        {
            nlohmann::json users = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_users.size(); ++i)
            {
                users.emplace_back(nlohmann::json::parse(m_users.at(i)->serialise()));
            }

            doc["users"] = users;
        }

        return doc.dump();
    }

    std::vector<User::ptr> VideoChatParticipantsInvited::get_users() const
    {
        return m_users;
    }

    void VideoChatParticipantsInvited::set_users(const std::vector<User::ptr> &users)
    {
        m_users = users;
    }
} //namespace tgbot
