#include "tgbot/types/File.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    File::File() = default;

    File::File(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("file_id"))
            {
                if(doc["file_id"].is_string())
                {
                    m_file_id = doc["file_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("file_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("file_id"));
            }

            if(doc.contains("file_path"))
            {
                if(doc["file_path"].is_string())
                {
                    m_file_path = doc["file_path"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("file_path"));
                }
            }

            if(doc.contains("file_size"))
            {
                if(doc["file_size"].is_number_integer())
                {
                    m_file_size = doc["file_size"].get<std::int64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("file_size"));
                }
            }

            if(doc.contains("file_unique_id"))
            {
                if(doc["file_unique_id"].is_string())
                {
                    m_file_unique_id = doc["file_unique_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("file_unique_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("file_unique_id"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string File::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["file_id"] = m_file_id;

        if(m_file_path.has_value())
        {
            doc["file_path"] = m_file_path.value();
        }

        if(m_file_size.has_value())
        {
            doc["file_size"] = m_file_size.value();
        }

        doc["file_unique_id"] = m_file_unique_id;
        return doc.dump();
    }

    std::string File::get_file_id() const
    {
        return m_file_id;
    }

    std::optional<std::string> File::get_file_path() const
    {
        return m_file_path;
    }

    std::optional<std::int64_t> File::get_file_size() const
    {
        return m_file_size;
    }

    std::string File::get_file_unique_id() const
    {
        return m_file_unique_id;
    }

    void File::set_file_id(const std::string &file_id)
    {
        m_file_id = file_id;
    }

    void File::set_file_path(const std::optional<std::string> &file_path)
    {
        m_file_path = file_path;
    }

    void File::set_file_size(const std::optional<std::int64_t> &file_size)
    {
        m_file_size = file_size;
    }

    void File::set_file_unique_id(const std::string &file_unique_id)
    {
        m_file_unique_id = file_unique_id;
    }
} //namespace tgbot
