#include "tgbot/types/MessageId.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <string>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    MessageId::MessageId() : m_message_id(-1)
    {}

    MessageId::MessageId(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("message_id"))
            {
                if(doc["message_id"].is_number_integer())
                {
                    m_message_id = doc["message_id"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("message_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("message_id"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string MessageId::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["message_id"] = m_message_id;
        return doc.dump();
    }

    std::int32_t MessageId::get_message_id() const
    {
        return m_message_id;
    }

    void MessageId::set_message_id(const std::int32_t &message_id)
    {
        m_message_id = message_id;
    }
} //namespace tgbot
