#include "tgbot/types/Message.h"

#include <algorithm>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "tgbot/constants/message.h"
#include "tgbot/types/Chat.h"

namespace tgbot
{
    Message::Message() :
        m_chat(std::make_shared<Chat>()), m_date(1), m_message_id(-1), m_message_thread_id(-1),
        m_user_shared(std::make_shared<UserShared>())
    {}

    //NOLINTNEXTLINE (misc-no-recursion)
    Message::Message(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("animation"))
            {
                if(doc["animation"].is_object())
                {
                    m_animation = std::make_shared<Animation>(doc["animation"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("animation"));
                }
            }

            if(doc.contains("audio"))
            {
                if(doc["audio"].is_object())
                {
                    m_audio = std::make_shared<Audio>(doc["audio"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("audio"));
                }
            }

            if(doc.contains("author_signature"))
            {
                if(doc["author_signature"].is_string())
                {
                    m_author_signature = doc["author_signature"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("author_signature"));
                }
            }

            if(doc.contains("caption"))
            {
                if(doc["caption"].is_string())
                {
                    m_caption = doc["caption"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("caption"));
                }
            }

            if(doc.contains("caption_entities"))
            {
                if(doc["caption_entities"].is_array())
                {
                    std::vector<MessageEntity::ptr> caption_entities;
                    caption_entities.reserve(doc["caption_entities"].size());

                    for(std::uint64_t i = 0; i < doc["caption_entities"].size(); ++i)
                    {
                        if(doc["caption_entities"].at(i).is_object())
                        {
                            caption_entities.emplace_back(
                              std::make_shared<MessageEntity>(doc["caption_entities"].at(i).dump()));
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_object(
                              "caption_"
                              "entitie"
                              "s"));
                        }
                    }

                    m_caption_entities = caption_entities;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("caption_entities"));
                }
            }

            if(doc.contains("channel_chat_created"))
            {
                if(doc["channel_chat_created"].is_boolean())
                {
                    m_channel_chat_created = doc["channel_chat_created"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("channel_chat_created"));
                }
            }

            if(doc.contains("chat"))
            {
                if(doc["chat"].is_object())
                {
                    m_chat = std::make_shared<Chat>(doc["chat"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("chat"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("chat"));
            }

            if(doc.contains("chat_shared"))
            {
                if(doc["chat_shared"].is_object())
                {
                    m_chat_shared = std::make_shared<ChatShared>(doc["chat_shared"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("chat_shared"));
                }
            }

            if(doc.contains("connected_website"))
            {
                if(doc["connected_website"].is_string())
                {
                    m_connected_website = doc["connected_website"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("connected_website"));
                }
            }

            if(doc.contains("contact"))
            {
                if(doc["contact"].is_object())
                {
                    m_contact = std::make_shared<Contact>(doc["contact"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("contact"));
                }
            }

            if(doc.contains("date"))
            {
                if(doc["date"].is_number_integer())
                {
                    m_date = doc["date"].get<std::uint64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("date"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("date"));
            }

            if(doc.contains("delete_chat_photo"))
            {
                if(doc["delete_chat_photo"].is_boolean())
                {
                    m_delete_chat_photo = doc["delete_chat_photo"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("delete_chat_photo"));
                }
            }

            if(doc.contains("dice"))
            {
                if(doc["dice"].is_object())
                {
                    m_dice = std::make_shared<Dice>(doc["dice"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("dice"));
                }
            }

            if(doc.contains("document"))
            {
                if(doc["document"].is_object())
                {
                    m_document = std::make_shared<Document>(doc["document"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("document"));
                }
            }

            if(doc.contains("edit_date"))
            {
                if(doc["edit_date"].is_number_integer())
                {
                    m_edit_date = doc["edit_date"].get<std::uint64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("edit_date"));
                }
            }

            if(doc.contains("entities"))
            {
                if(doc["entities"].is_array())
                {
                    std::vector<MessageEntity::ptr> entities;
                    entities.reserve(doc["entities"].size());

                    for(std::uint64_t i = 0; i < doc["entities"].size(); ++i)
                    {
                        if(doc["entities"].at(i).is_object())
                        {
                            entities.emplace_back(std::make_shared<MessageEntity>(doc["entities"].at(i).dump()));
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_object(
                              "entitie"
                              "s"));
                        }
                    }

                    m_entities = entities;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("entities"));
                }
            }

            if(doc.contains("forum_topic_closed"))
            {
                if(doc["forum_topic_closed"].is_object())
                {
                    m_forum_topic_closed = std::make_shared<ForumTopicClosed>(doc["forum_topic_closed"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("forum_topic_closed"));
                }
            }

            if(doc.contains("forum_topic_created"))
            {
                if(doc["forum_topic_created"].is_object())
                {
                    m_forum_topic_created = std::make_shared<ForumTopicCreated>(doc["forum_topic_created"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("forum_topic_created"));
                }
            }

            if(doc.contains("forum_topic_edited"))
            {
                if(doc["forum_topic_edited"].is_object())
                {
                    m_forum_topic_edited = std::make_shared<ForumTopicEdited>(doc["forum_topic_edited"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("forum_topic_edited"));
                }
            }

            if(doc.contains("forum_topic_reopened"))
            {
                if(doc["forum_topic_reopened"].is_object())
                {
                    m_forum_topic_reopened = std::make_shared<ForumTopicReopened>(doc["forum_topic_reopened"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("forum_topic_reopened"));
                }
            }

            if(doc.contains("forward_date"))
            {
                if(doc["forward_date"].is_number_integer())
                {
                    m_forward_date = doc["forward_date"].get<std::uint64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("forward_date"));
                }
            }

            if(doc.contains("forward_from"))
            {
                if(doc["forward_from"].is_object())
                {
                    m_forward_from = std::make_shared<User>(doc["forward_from"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("forward_from"));
                }
            }

            if(doc.contains("forward_from_chat"))
            {
                if(doc["forward_from_chat"].is_object())
                {
                    m_forward_from_chat = std::make_shared<Chat>(
                      doc["forward_from_"
                          "chat"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("forward_from_chat"));
                }
            }

            if(doc.contains("forward_from_message_id"))
            {
                if(doc["forward_from_message_id"].is_number_integer())
                {
                    m_forward_from_message_id = doc["forward_from_message_id"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("forward_from_message_id"));
                }
            }

            if(doc.contains("forward_sender_name"))
            {
                if(doc["forward_sender_name"].is_string())
                {
                    m_forward_sender_name = doc["forward_sender_name"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("forward_sender_name"));
                }
            }

            if(doc.contains("forward_signature"))
            {
                if(doc["forward_signature"].is_string())
                {
                    m_forward_signature = doc["forward_signature"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("forward_signature"));
                }
            }

            if(doc.contains("from"))
            {
                if(doc["from"].is_object())
                {
                    m_from = std::make_shared<User>(doc["from"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("from"));
                }
            }

            if(doc.contains("game"))
            {
                if(doc["game"].is_object())
                {
                    m_game = std::make_shared<Game>(doc["game"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("game"));
                }
            }

            if(doc.contains("general_forum_topic_hidden"))
            {
                if(doc["general_forum_topic_hidden"].is_object())
                {
                    m_general_forum_topic_hidden =
                      std::make_shared<GeneralForumTopicHidden>(doc["general_forum_topic_hidden"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object(
                      "general_forum_topic_"
                      "hidden"));
                }
            }

            if(doc.contains("general_forum_topic_unhidden"))
            {
                if(doc["general_forum_topic_unhidden"].is_object())
                {
                    m_general_forum_topic_unhidden =
                      std::make_shared<GeneralForumTopicUnhidden>(doc["general_forum_topic_unhidden"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object(
                      "general_forum_topic_"
                      "unhidden"));
                }
            }

            if(doc.contains("group_chat_created"))
            {
                if(doc["group_chat_created"].is_boolean())
                {
                    m_group_chat_created = doc["group_chat_created"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("group_chat_created"));
                }
            }

            if(doc.contains("has_media_spoiler"))
            {
                if(doc["has_media_spoiler"].is_boolean())
                {
                    m_has_media_spoiler = doc["has_media_spoiler"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("has_media_spoiler"));
                }
            }

            if(doc.contains("has_protected_content"))
            {
                if(doc["has_protected_content"].is_boolean())
                {
                    m_has_protected_content = doc["has_protected_content"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("has_protected_content"));
                }
            }

            if(doc.contains("invoice"))
            {
                if(doc["invoice"].is_object())
                {
                    m_invoice = std::make_shared<Invoice>(doc["invoice"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("invoice"));
                }
            }

            if(doc.contains("is_automatic_forward"))
            {
                if(doc["is_automatic_forward"].is_boolean())
                {
                    m_is_automatic_forward = doc["is_automatic_forward"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("is_automatic_forward"));
                }
            }

            if(doc.contains("is_topic_message"))
            {
                if(doc["is_topic_message"].is_boolean())
                {
                    m_is_topic_message = doc["is_topic_message"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("is_topic_message"));
                }
            }

            if(doc.contains("left_chat_member"))
            {
                if(doc["left_chat_member"].is_object())
                {
                    m_left_chat_member = std::make_shared<User>(doc["left_chat_member"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("left_chat_member"));
                }
            }

            if(doc.contains("location"))
            {
                if(doc["location"].is_object())
                {
                    m_location = std::make_shared<Location>(doc["location"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("location"));
                }
            }

            if(doc.contains("media_group_id"))
            {
                if(doc["media_group_id"].is_string())
                {
                    m_media_group_id = doc["media_group_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("media_group_id"));
                }
            }

            if(doc.contains("message_auto_delete_timer_changed"))
            {
                if(doc["message_auto_delete_timer_changed"].is_object())
                {
                    m_message_auto_delete_timer_changed =
                      std::make_shared<MessageAutoDeleteTimerChanged>(doc["message_auto_delete_timer_changed"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object(
                      "message_auto_delete_"
                      "timer_changed"));
                }
            }

            if(doc.contains("message_id"))
            {
                if(doc["message_id"].is_number_integer())
                {
                    m_message_id = doc["message_id"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("message_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("message_id"));
            }

            if(doc.contains("message_thread_id"))
            {
                if(doc["message_thread_id"].is_number_integer())
                {
                    m_message_thread_id = doc["message_thread_id"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("message_thread_id"));
                }
            }

            if(doc.contains("migrate_from_chat_id"))
            {
                if(doc["migrate_from_chat_id"].is_number_integer())
                {
                    m_migrate_from_chat_id = doc["migrate_from_chat_id"].get<std::int64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("migrate_from_chat_id"));
                }
            }

            if(doc.contains("migrate_to_chat_id"))
            {
                if(doc["migrate_to_chat_id"].is_number_integer())
                {
                    m_migrate_to_chat_id = doc["migrate_to_chat_id"].get<std::int64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("migrate_to_chat_id"));
                }
            }

            if(doc.contains("new_chat_members"))
            {
                if(doc["new_chat_members"].is_array())
                {
                    std::vector<User::ptr> new_chat_members;
                    new_chat_members.reserve(doc["new_chat_members"].size());

                    for(std::uint64_t i = 0; i < doc["new_chat_members"].size(); ++i)
                    {
                        if(doc["new_chat_members"].at(i).is_object())
                        {
                            new_chat_members.emplace_back(std::make_shared<User>(
                              doc["new_chat_"
                                  "members"]
                                .at(i)
                                .dump()));
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_object(
                              "new_chat_"
                              "member"
                              "s"));
                        }
                    }

                    m_new_chat_members = new_chat_members;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("new_chat_members"));
                }
            }

            if(doc.contains("new_chat_photo"))
            {
                if(doc["new_chat_photo"].is_array())
                {
                    std::vector<PhotoSize::ptr> new_chat_photo;
                    new_chat_photo.reserve(doc["new_chat_photo"].size());

                    for(std::uint64_t i = 0; i < doc["new_chat_photo"].size(); ++i)
                    {
                        if(doc["new_chat_photo"].at(i).is_object())
                        {
                            new_chat_photo.emplace_back(std::make_shared<PhotoSize>(doc["new_chat_photo"].at(i).dump()));
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_object(
                              "new_chat_"
                              "photo"));
                        }
                    }

                    m_new_chat_photo = new_chat_photo;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("new_chat_photo"));
                }
            }

            if(doc.contains("new_chat_title"))
            {
                if(doc["new_chat_title"].is_string())
                {
                    m_new_chat_title = doc["new_chat_title"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("new_chat_title"));
                }
            }

            if(doc.contains("passport_data"))
            {
                if(doc["passport_data"].is_object())
                {
                    m_passport_data = std::make_shared<PassportData>(
                      doc["passport_"
                          "data"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("passport_data"));
                }
            }

            if(doc.contains("photo"))
            {
                if(doc["photo"].is_array())
                {
                    std::vector<PhotoSize::ptr> photo;
                    photo.reserve(doc["photo"].size());

                    for(std::uint64_t i = 0; i < doc["photo"].size(); ++i)
                    {
                        if(doc["photo"].at(i).is_object())
                        {
                            photo.emplace_back(std::make_shared<PhotoSize>(doc["photo"].at(i).dump()));
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_object("photo"));
                        }
                    }

                    m_photo = photo;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("photo"));
                }
            }

            if(doc.contains("pinned_message"))
            {
                if(doc["pinned_message"].is_object())
                {
                    m_pinned_message = std::make_shared<Message>(doc["pinned_message"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("pinned_message"));
                }
            }

            if(doc.contains("poll"))
            {
                if(doc["poll"].is_object())
                {
                    m_poll = std::make_shared<Poll>(doc["poll"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("poll"));
                }
            }

            if(doc.contains("proximity_alert_triggered"))
            {
                if(doc["proximity_alert_triggered"].is_object())
                {
                    m_proximity_alert_triggered =
                      std::make_shared<ProximityAlertTriggered>(doc["proximity_alert_triggered"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object(
                      "proximity_alert_"
                      "triggered"));
                }
            }

            if(doc.contains("reply_markup"))
            {
                if(doc["reply_markup"].is_object())
                {
                    m_reply_markup = std::make_shared<InlineKeyboardMarkup>(
                      doc["reply_"
                          "marku"
                          "p"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("reply_markup"));
                }
            }

            if(doc.contains("reply_to_message"))
            {
                if(doc["reply_to_message"].is_object())
                {
                    m_reply_to_message = std::make_shared<Message>(
                      doc["reply_to_"
                          "message"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("reply_to_message"));
                }
            }

            if(doc.contains("sender_chat"))
            {
                if(doc["sender_chat"].is_object())
                {
                    m_sender_chat = std::make_shared<Chat>(doc["sender_chat"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("sender_chat"));
                }
            }

            if(doc.contains("sticker"))
            {
                if(doc["sticker"].is_object())
                {
                    m_sticker = std::make_shared<Sticker>(doc["sticker"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("sticker"));
                }
            }

            if(doc.contains("successful_payment"))
            {
                if(doc["successful_payment"].is_object())
                {
                    m_successful_payment = std::make_shared<SuccessfulPayment>(doc["successful_payment"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("successful_payment"));
                }
            }

            if(doc.contains("supergroup_chat_created"))
            {
                if(doc["supergroup_chat_created"].is_boolean())
                {
                    m_supergroup_chat_created = doc["supergroup_chat_created"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("supergroup_chat_created"));
                }
            }

            if(doc.contains("text"))
            {
                if(doc["text"].is_string())
                {
                    m_text = doc["text"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("text"));
                }
            }

            if(doc.contains("user_shared"))
            {
                if(doc["user_shared"].is_object())
                {
                    m_user_shared = std::make_shared<UserShared>(doc["user_shared"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("user_shared"));
                }
            }

            if(doc.contains("venue"))
            {
                if(doc["venue"].is_object())
                {
                    m_venue = std::make_shared<Venue>(doc["venue"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("venue"));
                }
            }

            if(doc.contains("via_bot"))
            {
                if(doc["via_bot"].is_object())
                {
                    m_via_bot = std::make_shared<User>(doc["via_bot"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("via_bot"));
                }
            }

            if(doc.contains("video"))
            {
                if(doc["video"].is_object())
                {
                    m_video = std::make_shared<Video>(doc["video"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("video"));
                }
            }

            if(doc.contains("video_chat_ended"))
            {
                if(doc["video_chat_ended"].is_object())
                {
                    m_video_chat_ended = std::make_shared<VideoChatEnded>(doc["video_chat_ended"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("video_chat_ended"));
                }
            }

            if(doc.contains("video_chat_participants_invited"))
            {
                if(doc["video_chat_participants_invited"].is_object())
                {
                    m_video_chat_participants_invited =
                      std::make_shared<VideoChatParticipantsInvited>(doc["video_chat_participants_invited"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object(
                      "video_chat_participants_"
                      "invited"));
                }
            }

            if(doc.contains("video_chat_scheduled"))
            {
                if(doc["video_chat_scheduled"].is_object())
                {
                    m_video_chat_scheduled = std::make_shared<VideoChatScheduled>(doc["video_chat_scheduled"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("video_chat_scheduled"));
                }
            }

            if(doc.contains("video_chat_started"))
            {
                if(doc["video_chat_started"].is_object())
                {
                    m_video_chat_started = std::make_shared<VideoChatStarted>(
                      doc["video_"
                          "chat_"
                          "starte"
                          "d"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("video_chat_started"));
                }
            }

            if(doc.contains("video_note"))
            {
                if(doc["video_note"].is_object())
                {
                    m_video_note = std::make_shared<VideoNote>(doc["video_note"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("video_note"));
                }
            }

            if(doc.contains("voice"))
            {
                if(doc["voice"].is_object())
                {
                    m_voice = std::make_shared<Voice>(doc["voice"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("voice"));
                }
            }

            if(doc.contains("web_app_data"))
            {
                if(doc["web_app_data"].is_object())
                {
                    m_web_app_data = std::make_shared<WebAppData>(doc["web_app_data"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("web_app_data"));
                }
            }

            if(doc.contains("write_access_allowed"))
            {
                if(doc["write_access_allowed"].is_object())
                {
                    m_write_access_allowed = std::make_shared<WriteAccessAllowed>(doc["write_access_allowed"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("write_access_allowed"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    //NOLINTNEXTLINE (misc-no-recursion)
    std::string Message::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_animation.has_value())
        {
            doc["animation"] = nlohmann::json::parse(m_animation.value()->serialise());
        }

        if(m_audio.has_value())
        {
            doc["audio"] = nlohmann::json::parse(m_audio.value()->serialise());
        }

        if(m_author_signature.has_value())
        {
            doc["author_signature"] = m_author_signature.value();
        }

        if(m_caption.has_value())
        {
            doc["caption"] = m_caption.value();
        }

        if(m_caption_entities.has_value())
        {
            nlohmann::json caption_entities = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_caption_entities.value().size(); ++i)
            {
                caption_entities.emplace_back(nlohmann::json::parse(m_caption_entities.value().at(i)->serialise()));
            }

            doc["caption_entities"] = caption_entities;
        }

        if(m_channel_chat_created.has_value())
        {
            doc["channel_chat_created"] = m_channel_chat_created.value();
        }

        doc["chat"] = nlohmann::json::parse(m_chat->serialise());

        if(m_chat_shared.has_value())
        {
            doc["chat_shared"] = nlohmann::json::parse(m_chat_shared.value()->serialise());
        }

        if(m_connected_website.has_value())
        {
            doc["connected_website"] = m_connected_website.value();
        }

        if(m_contact.has_value())
        {
            doc["contact"] = nlohmann::json::parse(m_contact.value()->serialise());
        }

        doc["date"] = m_date;

        if(m_delete_chat_photo.has_value())
        {
            doc["delete_chat_photo"] = m_delete_chat_photo.value();
        }

        if(m_dice.has_value())
        {
            doc["dice"] = nlohmann::json::parse(m_dice.value()->serialise());
        }

        if(m_document.has_value())
        {
            doc["document"] = nlohmann::json::parse(m_document.value()->serialise());
        }

        if(m_edit_date.has_value())
        {
            doc["edit_date"] = m_edit_date.value();
        }

        if(m_entities.has_value())
        {
            nlohmann::json entities = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_entities.value().size(); ++i)
            {
                entities.emplace_back(nlohmann::json::parse(m_entities.value().at(i)->serialise()));
            }

            doc["entities"] = entities;
        }

        if(m_forum_topic_closed.has_value())
        {
            doc["forum_topic_closed"] = nlohmann::json::parse(m_forum_topic_closed.value()->serialise());
        }

        if(m_forum_topic_created.has_value())
        {
            doc["forum_topic_created"] = nlohmann::json::parse(m_forum_topic_created.value()->serialise());
        }

        if(m_forum_topic_edited.has_value())
        {
            doc["forum_topic_edited"] = nlohmann::json::parse(m_forum_topic_edited.value()->serialise());
        }

        if(m_forum_topic_reopened.has_value())
        {
            doc["forum_topic_reopened"] = nlohmann::json::parse(m_forum_topic_reopened.value()->serialise());
        }

        if(m_forward_date.has_value())
        {
            doc["forward_date"] = m_forward_date.value();
        }

        if(m_forward_from.has_value())
        {
            doc["forward_from"] = nlohmann::json::parse(m_forward_from.value()->serialise());
        }

        if(m_forward_from_chat.has_value())
        {
            doc["forward_from_chat"] = nlohmann::json::parse(m_forward_from_chat.value()->serialise());
        }

        if(m_forward_from_message_id.has_value())
        {
            doc["forward_from_message_id"] = m_forward_from_message_id.value();
        }

        if(m_forward_sender_name.has_value())
        {
            doc["forward_sender_name"] = m_forward_sender_name.value();
        }

        if(m_forward_signature.has_value())
        {
            doc["forward_signature"] = m_forward_signature.value();
        }

        if(m_from.has_value())
        {
            doc["from"] = nlohmann::json::parse(m_from.value()->serialise());
        }

        if(m_game.has_value())
        {
            doc["game"] = nlohmann::json::parse(m_game.value()->serialise());
        }

        if(m_general_forum_topic_hidden.has_value())
        {
            doc["general_forum_topic_hidden"] = nlohmann::json::parse(m_general_forum_topic_hidden.value()->serialise());
        }

        if(m_general_forum_topic_unhidden.has_value())
        {
            doc["general_forum_topic_unhidden"] =
              nlohmann::json::parse(m_general_forum_topic_unhidden.value()->serialise());
        }

        if(m_group_chat_created.has_value())
        {
            doc["group_chat_created"] = m_group_chat_created.value();
        }

        if(m_has_media_spoiler.has_value())
        {
            doc["has_media_spoiler"] = m_has_media_spoiler.value();
        }

        if(m_has_protected_content.has_value())
        {
            doc["has_protected_content"] = m_has_protected_content.value();
        }

        if(m_invoice.has_value())
        {
            doc["invoice"] = nlohmann::json::parse(m_invoice.value()->serialise());
        }

        if(m_is_automatic_forward.has_value())
        {
            doc["is_automatic_forward"] = m_is_automatic_forward.value();
        }

        if(m_is_topic_message.has_value())
        {
            doc["is_topic_message"] = m_is_topic_message.value();
        }

        if(m_left_chat_member.has_value())
        {
            doc["left_chat_member"] = nlohmann::json::parse(m_left_chat_member.value()->serialise());
        }

        if(m_location.has_value())
        {
            doc["location"] = nlohmann::json::parse(m_location.value()->serialise());
        }

        if(m_media_group_id.has_value())
        {
            doc["media_group_id"] = m_media_group_id.value();
        }

        if(m_message_auto_delete_timer_changed.has_value())
        {
            doc["message_auto_delete_timer_changed"] =
              nlohmann::json::parse(m_message_auto_delete_timer_changed.value()->serialise());
        }

        doc["message_id"] = m_message_id;
        doc["message_thread_id"] = m_message_thread_id;

        if(m_migrate_from_chat_id.has_value())
        {
            doc["migrate_from_chat_id"] = m_migrate_from_chat_id.value();
        }

        if(m_migrate_to_chat_id.has_value())
        {
            doc["migrate_to_chat_id"] = m_migrate_to_chat_id.value();
        }

        if(m_new_chat_members.has_value())
        {
            nlohmann::json new_chat_members = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_new_chat_members.value().size(); ++i)
            {
                new_chat_members.emplace_back(nlohmann::json::parse(m_new_chat_members.value().at(i)->serialise()));
            }

            doc["new_chat_members"] = new_chat_members;
        }

        if(m_new_chat_photo.has_value())
        {
            nlohmann::json new_chat_photo = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_new_chat_photo.value().size(); ++i)
            {
                new_chat_photo.emplace_back(nlohmann::json::parse(m_new_chat_photo.value().at(i)->serialise()));
            }

            doc["new_chat_photo"] = new_chat_photo;
        }

        if(m_new_chat_title.has_value())
        {
            doc["new_chat_title"] = m_new_chat_title.value();
        }

        if(m_passport_data.has_value())
        {
            doc["passport_data"] = nlohmann::json::parse(m_passport_data.value()->serialise());
        }

        if(m_photo.has_value())
        {
            nlohmann::json photo = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_photo.value().size(); ++i)
            {
                photo.emplace_back(nlohmann::json::parse(m_photo.value().at(i)->serialise()));
            }

            doc["photo"] = photo;
        }

        if(m_pinned_message.has_value())
        {
            doc["pinned_message"] = nlohmann::json::parse(m_pinned_message.value()->serialise());
        }

        if(m_poll.has_value())
        {
            doc["poll"] = nlohmann::json::parse(m_poll.value()->serialise());
        }

        if(m_proximity_alert_triggered.has_value())
        {
            doc["proximity_alert_triggered"] = nlohmann::json::parse(m_proximity_alert_triggered.value()->serialise());
        }

        if(m_reply_markup.has_value())
        {
            doc["reply_markup"] = nlohmann::json::parse(m_reply_markup.value()->serialise());
        }

        if(m_reply_to_message.has_value())
        {
            doc["reply_to_message"] = nlohmann::json::parse(m_reply_to_message.value()->serialise());
        }

        if(m_sender_chat.has_value())
        {
            doc["sender_chat"] = nlohmann::json::parse(m_sender_chat.value()->serialise());
        }

        if(m_sticker.has_value())
        {
            doc["sticker"] = nlohmann::json::parse(m_sticker.value()->serialise());
        }

        if(m_successful_payment.has_value())
        {
            doc["successful_payment"] = nlohmann::json::parse(m_successful_payment.value()->serialise());
        }

        if(m_supergroup_chat_created.has_value())
        {
            doc["supergroup_chat_created"] = m_supergroup_chat_created.value();
        }

        if(m_text.has_value())
        {
            doc["text"] = m_text.value();
        }

        if(m_user_shared.has_value())
        {
            doc["user_shared"] = nlohmann::json::parse(m_user_shared.value()->serialise());
        }

        if(m_venue.has_value())
        {
            doc["venue"] = nlohmann::json::parse(m_venue.value()->serialise());
        }

        if(m_via_bot.has_value())
        {
            doc["via_bot"] = nlohmann::json::parse(m_via_bot.value()->serialise());
        }

        if(m_video.has_value())
        {
            doc["video"] = nlohmann::json::parse(m_video.value()->serialise());
        }

        if(m_video_chat_ended.has_value())
        {
            doc["video_chat_ended"] = nlohmann::json::parse(m_video_chat_ended.value()->serialise());
        }

        if(m_video_chat_participants_invited.has_value())
        {
            doc["video_chat_participants_invited"] =
              nlohmann::json::parse(m_video_chat_participants_invited.value()->serialise());
        }

        if(m_video_chat_scheduled.has_value())
        {
            doc["video_chat_scheduled"] = nlohmann::json::parse(m_video_chat_scheduled.value()->serialise());
        }

        if(m_video_chat_started.has_value())
        {
            doc["video_chat_started"] = nlohmann::json::parse(m_video_chat_started.value()->serialise());
        }

        if(m_video_note.has_value())
        {
            doc["video_note"] = nlohmann::json::parse(m_video_note.value()->serialise());
        }

        if(m_voice.has_value())
        {
            doc["voice"] = nlohmann::json::parse(m_voice.value()->serialise());
        }

        if(m_web_app_data.has_value())
        {
            doc["web_app_data"] = nlohmann::json::parse(m_web_app_data.value()->serialise());
        }

        if(m_write_access_allowed.has_value())
        {
            doc["write_access_allowed"] = nlohmann::json::parse(m_write_access_allowed.value()->serialise());
        }

        return doc.dump();
    }

    std::optional<Animation::ptr> Message::get_animation() const
    {
        return m_animation;
    }

    std::optional<Audio::ptr> Message::get_audio() const
    {
        return m_audio;
    }

    std::optional<std::string> Message::get_author_signature() const
    {
        return m_author_signature;
    }

    std::optional<std::string> Message::get_caption() const
    {
        return m_caption;
    }

    std::optional<std::vector<MessageEntity::ptr>> Message::get_caption_entities() const
    {
        return m_caption_entities;
    }

    std::optional<bool> Message::get_channel_chat_created() const
    {
        return m_channel_chat_created;
    }

    Chat::ptr Message::get_chat() const
    {
        return m_chat;
    }

    std::optional<ChatShared::ptr> Message::get_chat_shared() const
    {
        return m_chat_shared;
    }

    std::optional<std::string> Message::get_connected_website() const
    {
        return m_connected_website;
    }

    std::optional<Contact::ptr> Message::get_contact() const
    {
        return m_contact;
    }

    std::uint64_t Message::get_date() const
    {
        return m_date;
    }

    std::optional<bool> Message::get_delete_chat_photo() const
    {
        return m_delete_chat_photo;
    }

    std::optional<Dice::ptr> Message::get_dice() const
    {
        return m_dice;
    }

    std::optional<Document::ptr> Message::get_document() const
    {
        return m_document;
    }

    std::optional<std::uint64_t> Message::get_edit_date() const
    {
        return m_edit_date;
    }

    std::optional<std::vector<MessageEntity::ptr>> Message::get_entities() const
    {
        return m_entities;
    }

    std::optional<ForumTopicClosed::ptr> Message::get_forum_topic_closed() const
    {
        return m_forum_topic_closed;
    }

    std::optional<ForumTopicCreated::ptr> Message::get_forum_topic_created() const
    {
        return m_forum_topic_created;
    }

    std::optional<ForumTopicEdited::ptr> Message::get_forum_topic_edited() const
    {
        return m_forum_topic_edited;
    }

    std::optional<ForumTopicReopened::ptr> Message::get_forum_topic_reopened() const
    {
        return m_forum_topic_reopened;
    }

    std::optional<std::uint64_t> Message::get_forward_date() const
    {
        return m_forward_date;
    }

    std::optional<User::ptr> Message::get_forward_from() const
    {
        return m_forward_from;
    }

    std::optional<Chat::ptr> Message::get_forward_from_chat() const
    {
        return m_forward_from_chat;
    }

    std::optional<std::int32_t> Message::get_forward_from_message_id() const
    {
        return m_forward_from_message_id;
    }

    std::optional<std::string> Message::get_forward_sender_name() const
    {
        return m_forward_sender_name;
    }

    std::optional<std::string> Message::get_forward_signature() const
    {
        return m_forward_signature;
    }

    std::optional<User::ptr> Message::get_from() const
    {
        return m_from;
    }

    std::optional<Game::ptr> Message::get_game() const
    {
        return m_game;
    }

    std::optional<GeneralForumTopicHidden::ptr> Message::get_general_forum_topic_hidden() const
    {
        return m_general_forum_topic_hidden;
    }

    std::optional<GeneralForumTopicUnhidden::ptr> Message::get_general_forum_topic_unhidden() const
    {
        return m_general_forum_topic_unhidden;
    }

    std::optional<bool> Message::get_group_chat_created() const
    {
        return m_group_chat_created;
    }

    std::optional<bool> Message::get_has_media_spoiler() const
    {
        return m_has_media_spoiler;
    }

    std::optional<bool> Message::get_has_protected_content() const
    {
        return m_has_protected_content;
    }

    std::optional<Invoice::ptr> Message::get_invoice() const
    {
        return m_invoice;
    }

    std::optional<bool> Message::get_is_automatic_forward() const
    {
        return m_is_automatic_forward;
    }

    std::optional<bool> Message::get_is_topic_message() const
    {
        return m_is_topic_message;
    }

    std::optional<User::ptr> Message::get_left_chat_member() const
    {
        return m_left_chat_member;
    }

    std::optional<Location::ptr> Message::get_location() const
    {
        return m_location;
    }

    std::optional<std::string> Message::get_media_group_id() const
    {
        return m_media_group_id;
    }

    std::optional<MessageAutoDeleteTimerChanged::ptr> Message::get_message_auto_delete_timer_changed() const
    {
        return m_message_auto_delete_timer_changed;
    }

    std::int32_t Message::get_message_id() const
    {
        return m_message_id;
    }

    std::int32_t Message::get_message_thread_id() const
    {
        return m_message_thread_id;
    }

    std::optional<std::int64_t> Message::get_migrate_from_chat_id() const
    {
        return m_migrate_from_chat_id;
    }

    std::optional<std::int64_t> Message::get_migrate_to_chat_id() const
    {
        return m_migrate_to_chat_id;
    }

    std::optional<std::vector<User::ptr>> Message::get_new_chat_members() const
    {
        return m_new_chat_members;
    }

    std::optional<std::vector<PhotoSize::ptr>> Message::get_new_chat_photo() const
    {
        return m_new_chat_photo;
    }

    std::optional<std::string> Message::get_new_chat_title() const
    {
        return m_new_chat_title;
    }

    std::optional<PassportData::ptr> Message::get_passport_data() const
    {
        return m_passport_data;
    }

    std::optional<std::vector<PhotoSize::ptr>> Message::get_photo() const
    {
        return m_photo;
    }

    std::optional<Message::ptr> Message::get_pinned_message() const
    {
        return m_pinned_message;
    }

    std::optional<Poll::ptr> Message::get_poll() const
    {
        return m_poll;
    }

    std::optional<ProximityAlertTriggered::ptr> Message::get_proximity_alert_triggered() const
    {
        return m_proximity_alert_triggered;
    }

    std::optional<InlineKeyboardMarkup::ptr> Message::get_reply_markup() const
    {
        return m_reply_markup;
    }

    std::optional<Message::ptr> Message::get_reply_to_message() const
    {
        return m_reply_to_message;
    }

    std::optional<Chat::ptr> Message::get_sender_chat() const
    {
        return m_sender_chat;
    }

    std::optional<Sticker::ptr> Message::get_sticker() const
    {
        return m_sticker;
    }

    std::optional<SuccessfulPayment::ptr> Message::get_successful_payment() const
    {
        return m_successful_payment;
    }

    std::optional<bool> Message::get_supergroup_chat_created() const
    {
        return m_supergroup_chat_created;
    }

    std::optional<std::string> Message::get_text() const
    {
        return m_text;
    }

    std::optional<UserShared::ptr> Message::get_user_shared() const
    {
        return m_user_shared;
    }

    std::optional<Venue::ptr> Message::get_venue() const
    {
        return m_venue;
    }

    std::optional<User::ptr> Message::get_via_bot() const
    {
        return m_via_bot;
    }

    std::optional<Video::ptr> Message::get_video() const
    {
        return m_video;
    }

    std::optional<VideoChatEnded::ptr> Message::get_video_chat_ended() const
    {
        return m_video_chat_ended;
    }

    std::optional<VideoChatParticipantsInvited::ptr> Message::get_video_chat_participants_invited() const
    {
        return m_video_chat_participants_invited;
    }

    std::optional<VideoChatScheduled::ptr> Message::get_video_chat_scheduled() const
    {
        return m_video_chat_scheduled;
    }

    std::optional<VideoChatStarted::ptr> Message::get_video_chat_started() const
    {
        return m_video_chat_started;
    }

    std::optional<VideoNote::ptr> Message::get_video_note() const
    {
        return m_video_note;
    }

    std::optional<Voice::ptr> Message::get_voice() const
    {
        return m_voice;
    }

    std::optional<WebAppData::ptr> Message::get_web_app_data() const
    {
        return m_web_app_data;
    }

    std::optional<WriteAccessAllowed::ptr> Message::get_write_access_allowed() const
    {
        return m_write_access_allowed;
    }

    void Message::set_animation(const std::optional<Animation::ptr> &animation)
    {
        m_animation = animation;
    }

    void Message::set_audio(const std::optional<Audio::ptr> &audio)
    {
        m_audio = audio;
    }

    void Message::set_author_signature(const std::optional<std::string> &author_signature)
    {
        m_author_signature = author_signature;
    }

    void Message::set_caption(const std::optional<std::string> &caption)
    {
        m_caption = caption;
    }

    void Message::set_caption_entities(const std::optional<std::vector<MessageEntity::ptr>> &caption_entities)
    {
        m_caption_entities = caption_entities;
    }

    void Message::set_channel_chat_created(const std::optional<bool> &channel_chat_created)
    {
        m_channel_chat_created = channel_chat_created;
    }

    void Message::set_chat(const Chat::ptr &chat)
    {
        m_chat = chat;
    }

    void Message::set_chat_shared(const std::optional<ChatShared::ptr> &chat_shared)
    {
        m_chat_shared = chat_shared;
    }

    void Message::set_connected_website(const std::optional<std::string> &connected_website)
    {
        m_connected_website = connected_website;
    }

    void Message::set_contact(const std::optional<Contact::ptr> &contact)
    {
        m_contact = contact;
    }

    void Message::set_date(const std::uint64_t &date)
    {
        m_date = date;
    }

    void Message::set_delete_chat_photo(const std::optional<bool> &delete_chat_photo)
    {
        m_delete_chat_photo = delete_chat_photo;
    }

    void Message::set_dice(const std::optional<Dice::ptr> &dice)
    {
        m_dice = dice;
    }

    void Message::set_document(const std::optional<Document::ptr> &document)
    {
        m_document = document;
    }

    void Message::set_edit_date(const std::optional<std::uint64_t> &edit_date)
    {
        m_edit_date = edit_date;
    }

    void Message::set_entities(const std::optional<std::vector<MessageEntity::ptr>> &entities)
    {
        m_entities = entities;
    }

    void Message::set_forum_topic_closed(const std::optional<ForumTopicClosed::ptr> &forum_topic_closed)
    {
        m_forum_topic_closed = forum_topic_closed;
    }

    void Message::set_forum_topic_created(const std::optional<ForumTopicCreated::ptr> &forum_topic_created)
    {
        m_forum_topic_created = forum_topic_created;
    }

    void Message::set_forum_topic_edited(const std::optional<ForumTopicEdited::ptr> &forum_topic_edited)
    {
        m_forum_topic_edited = forum_topic_edited;
    }

    void Message::set_forum_topic_reopened(const std::optional<ForumTopicReopened::ptr> &forum_topic_reopened)
    {
        m_forum_topic_reopened = forum_topic_reopened;
    }

    void Message::set_forward_date(const std::optional<std::uint64_t> &forward_date)
    {
        m_forward_date = forward_date;
    }

    void Message::set_forward_from(const std::optional<User::ptr> &forward_from)
    {
        m_forward_from = forward_from;
    }

    void Message::set_forward_from_chat(const std::optional<Chat::ptr> &forward_from_chat)
    {
        m_forward_from_chat = forward_from_chat;
    }

    void Message::set_forward_from_message_id(const std::optional<std::int32_t> &forward_from_message_id)
    {
        m_forward_from_message_id = forward_from_message_id;
    }

    void Message::set_forward_sender_name(const std::optional<std::string> &forward_sender_name)
    {
        m_forward_sender_name = forward_sender_name;
    }

    void Message::set_forward_signature(const std::optional<std::string> &forward_signature)
    {
        m_forward_signature = forward_signature;
    }

    void Message::set_from(const std::optional<User::ptr> &from)
    {
        m_from = from;
    }

    void Message::set_game(const std::optional<Game::ptr> &game)
    {
        m_game = game;
    }

    void Message::set_general_forum_topic_hidden(
      const std::optional<GeneralForumTopicHidden::ptr> &general_forum_topic_hidden)
    {
        m_general_forum_topic_hidden = general_forum_topic_hidden;
    }

    void Message::set_general_forum_topic_unhidden(
      const std::optional<GeneralForumTopicUnhidden::ptr> &general_forum_topic_unhidden)
    {
        m_general_forum_topic_unhidden = general_forum_topic_unhidden;
    }

    void Message::set_group_chat_created(const std::optional<bool> &group_chat_created)
    {
        m_group_chat_created = group_chat_created;
    }

    void Message::set_has_media_spoiler(const std::optional<bool> &has_media_spoiler)
    {
        m_has_media_spoiler = has_media_spoiler;
    }

    void Message::set_has_protected_content(const std::optional<bool> &has_protected_content)
    {
        m_has_protected_content = has_protected_content;
    }

    void Message::set_invoice(const std::optional<Invoice::ptr> &invoice)
    {
        m_invoice = invoice;
    }

    void Message::set_is_automatic_forward(const std::optional<bool> &is_automatic_forward)
    {
        m_is_automatic_forward = is_automatic_forward;
    }

    void Message::set_is_topic_message(const std::optional<bool> &is_topic_message)
    {
        m_is_topic_message = is_topic_message;
    }

    void Message::set_left_chat_member(const std::optional<User::ptr> &left_chat_member)
    {
        m_left_chat_member = left_chat_member;
    }

    void Message::set_location(const std::optional<Location::ptr> &location)
    {
        m_location = location;
    }

    void Message::set_media_group_id(const std::optional<std::string> &media_group_id)
    {
        m_media_group_id = media_group_id;
    }

    void Message::set_message_auto_delete_timer_changed(
      const std::optional<MessageAutoDeleteTimerChanged::ptr> &message_auto_delete_timer_changed)
    {
        m_message_auto_delete_timer_changed = message_auto_delete_timer_changed;
    }

    void Message::set_message_id(const std::int32_t &message_id)
    {
        m_message_id = message_id;
    }

    void Message::set_message_thread_id(const std::int32_t &message_thread_id)
    {
        m_message_thread_id = message_thread_id;
    }

    void Message::set_migrate_from_chat_id(const std::optional<std::int64_t> &migrate_from_chat_id)
    {
        m_migrate_from_chat_id = migrate_from_chat_id;
    }

    void Message::set_migrate_to_chat_id(const std::optional<std::int64_t> &migrate_to_chat_id)
    {
        m_migrate_to_chat_id = migrate_to_chat_id;
    }

    void Message::set_new_chat_members(const std::optional<std::vector<User::ptr>> &new_chat_members)
    {
        m_new_chat_members = new_chat_members;
    }

    void Message::set_new_chat_photo(const std::optional<std::vector<PhotoSize::ptr>> &new_chat_photo)
    {
        m_new_chat_photo = new_chat_photo;
    }

    void Message::set_new_chat_title(const std::optional<std::string> &new_chat_title)
    {
        m_new_chat_title = new_chat_title;
    }

    void Message::set_passport_data(const std::optional<PassportData::ptr> &passport_data)
    {
        m_passport_data = passport_data;
    }

    void Message::set_photo(const std::optional<std::vector<PhotoSize::ptr>> &photo)
    {
        m_photo = photo;
    }

    void Message::set_pinned_message(const std::optional<Message::ptr> &pinned_message)
    {
        m_pinned_message = pinned_message;
    }

    void Message::set_poll(const std::optional<Poll::ptr> &poll)
    {
        m_poll = poll;
    }

    void Message::set_proximity_alert_triggered(
      const std::optional<ProximityAlertTriggered::ptr> &proximity_alert_triggered)
    {
        m_proximity_alert_triggered = proximity_alert_triggered;
    }

    void Message::set_reply_markup(const std::optional<InlineKeyboardMarkup::ptr> &reply_markup)
    {
        m_reply_markup = reply_markup;
    }

    void Message::set_reply_to_message(const std::optional<Message::ptr> &reply_to_message)
    {
        m_reply_to_message = reply_to_message;
    }

    void Message::set_sender_chat(const std::optional<Chat::ptr> &sender_chat)
    {
        m_sender_chat = sender_chat;
    }

    void Message::set_sticker(const std::optional<Sticker::ptr> &sticker)
    {
        m_sticker = sticker;
    }

    void Message::set_successful_payment(const std::optional<SuccessfulPayment::ptr> &successful_payment)
    {
        m_successful_payment = successful_payment;
    }

    void Message::set_supergroup_chat_created(const std::optional<bool> &supergroup_chat_created)
    {
        m_supergroup_chat_created = supergroup_chat_created;
    }

    void Message::set_text(const std::optional<std::string> &text)
    {
        m_text = text;
    }

    void Message::set_user_shared(const std::optional<UserShared::ptr> &user_shared)
    {
        m_user_shared = user_shared;
    }

    void Message::set_venue(const std::optional<Venue::ptr> &venue)
    {
        m_venue = venue;
    }

    void Message::set_via_bot(const std::optional<User::ptr> &via_bot)
    {
        m_via_bot = via_bot;
    }

    void Message::set_video(const std::optional<Video::ptr> &video)
    {
        m_video = video;
    }

    void Message::set_video_chat_ended(const std::optional<VideoChatEnded::ptr> &video_chat_ended)
    {
        m_video_chat_ended = video_chat_ended;
    }

    void Message::set_video_chat_participants_invited(
      const std::optional<VideoChatParticipantsInvited::ptr> &video_chat_participants_invited)
    {
        m_video_chat_participants_invited = video_chat_participants_invited;
    }

    void Message::set_video_chat_scheduled(const std::optional<VideoChatScheduled::ptr> &video_chat_scheduled)
    {
        m_video_chat_scheduled = video_chat_scheduled;
    }

    void Message::set_video_chat_started(const std::optional<VideoChatStarted::ptr> &video_chat_started)
    {
        m_video_chat_started = video_chat_started;
    }

    void Message::set_video_note(const std::optional<VideoNote::ptr> &video_note)
    {
        m_video_note = video_note;
    }

    void Message::set_voice(const std::optional<Voice::ptr> &voice)
    {
        m_voice = voice;
    }

    void Message::set_web_app_data(const std::optional<WebAppData::ptr> &web_app_data)
    {
        m_web_app_data = web_app_data;
    }

    void Message::set_write_access_allowed(const std::optional<WriteAccessAllowed::ptr> &write_access_allowed)
    {
        m_write_access_allowed = write_access_allowed;
    }
} //namespace tgbot
