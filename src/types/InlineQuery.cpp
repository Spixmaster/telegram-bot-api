#include "tgbot/types/InlineQuery.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    InlineQuery::InlineQuery() : m_from(std::make_shared<User>())
    {}

    InlineQuery::InlineQuery(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("chat_type"))
            {
                if(doc["chat_type"].is_string())
                {
                    m_chat_type = doc["chat_type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("chat_type"));
                }
            }

            if(doc.contains("from"))
            {
                if(doc["from"].is_object())
                {
                    m_from = std::make_shared<User>(doc["from"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("from"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("from"));
            }

            if(doc.contains("id"))
            {
                if(doc["id"].is_string())
                {
                    m_id = doc["id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("id"));
            }

            if(doc.contains("location"))
            {
                if(doc["location"].is_object())
                {
                    m_location = std::make_shared<Location>(doc["location"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("location"));
                }
            }

            if(doc.contains("offset"))
            {
                if(doc["offset"].is_string())
                {
                    m_offset = doc["offset"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("offset"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("offset"));
            }

            if(doc.contains("query"))
            {
                if(doc["query"].is_string())
                {
                    m_query = doc["query"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("query"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("query"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string InlineQuery::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_chat_type.has_value())
        {
            doc["chat_type"] = m_chat_type.value();
        }

        doc["from"] = nlohmann::json::parse(m_from->serialise());
        doc["id"] = m_id;

        if(m_location.has_value())
        {
            doc["location"] = nlohmann::json::parse(m_location.value()->serialise());
        }

        doc["offset"] = m_offset;
        doc["query"] = m_query;
        return doc.dump();
    }

    std::optional<std::string> InlineQuery::get_chat_type() const
    {
        return m_chat_type;
    }

    User::ptr InlineQuery::get_from() const
    {
        return m_from;
    }

    std::string InlineQuery::get_id() const
    {
        return m_id;
    }

    std::optional<Location::ptr> InlineQuery::get_location() const
    {
        return m_location;
    }

    std::string InlineQuery::get_offset() const
    {
        return m_offset;
    }

    std::string InlineQuery::get_query() const
    {
        return m_query;
    }

    void InlineQuery::set_chat_type(const std::optional<std::string> &chat_type)
    {
        m_chat_type = chat_type;
    }

    void InlineQuery::set_from(const User::ptr &from)
    {
        m_from = from;
    }

    void InlineQuery::set_id(const std::string &id)
    {
        m_id = id;
    }

    void InlineQuery::set_location(const std::optional<Location::ptr> &location)
    {
        m_location = location;
    }

    void InlineQuery::set_offset(const std::string &offset)
    {
        m_offset = offset;
    }

    void InlineQuery::set_query(const std::string &query)
    {
        m_query = query;
    }
} //namespace tgbot
