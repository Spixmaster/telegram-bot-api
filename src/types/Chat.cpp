#include "tgbot/types/Chat.h"

#include <algorithm>
#include <initializer_list>
#include <map>
#include <memory>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "tgbot/constants/message.h"
#include "tgbot/types/ChatPhoto.h"
#include "tgbot/types/Message.h"

namespace tgbot
{
    Chat::Chat() : m_id(-1)
    {}

    Chat::Chat(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("active_usernames"))
            {
                if(doc["active_usernames"].is_array())
                {
                    std::vector<std::string> active_usernames;
                    active_usernames.reserve(doc["active_usernames"].size());

                    for(std::uint64_t i = 0; i < doc["active_usernames"].size(); ++i)
                    {
                        if(doc["active_usernames"].at(i).is_string())
                        {
                            active_usernames.emplace_back(doc["active_usernames"].at(i).get<std::string>());
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_string(
                              "active_"
                              "usernames"));
                        }
                    }

                    m_active_usernames = active_usernames;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("active_usernames"));
                }
            }

            if(doc.contains("bio"))
            {
                if(doc["bio"].is_string())
                {
                    m_bio = doc["bio"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("bio"));
                }
            }

            if(doc.contains("can_set_sticker_set"))
            {
                if(doc["can_set_sticker_set"].is_boolean())
                {
                    m_can_set_sticker_set = doc["can_set_sticker_set"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_set_sticker_set"));
                }
            }

            if(doc.contains("description"))
            {
                if(doc["description"].is_string())
                {
                    m_description = doc["description"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("description"));
                }
            }

            if(doc.contains("emoji_status_custom_emoji_id"))
            {
                if(doc["emoji_status_custom_emoji_id"].is_string())
                {
                    m_emoji_status_custom_emoji_id = doc["emoji_status_custom_emoji_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string(
                      "emoji_status_custom_emoji_"
                      "id"));
                }
            }

            if(doc.contains("first_name"))
            {
                if(doc["first_name"].is_string())
                {
                    m_first_name = doc["first_name"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("first_name"));
                }
            }

            if(doc.contains("has_aggressive_anti_spam_enabled"))
            {
                if(doc["has_aggressive_anti_spam_enabled"].is_boolean())
                {
                    m_has_aggressive_anti_spam_enabled = doc["has_aggressive_anti_spam_enabled"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool(
                      "has_aggressive_anti_spam_"
                      "enabled"));
                }
            }

            if(doc.contains("has_hidden_members"))
            {
                if(doc["has_hidden_members"].is_boolean())
                {
                    m_has_hidden_members = doc["has_hidden_members"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("has_hidden_members"));
                }
            }

            if(doc.contains("has_private_forwards"))
            {
                if(doc["has_private_forwards"].is_boolean())
                {
                    m_has_private_forwards = doc["has_private_forwards"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("has_private_forwards"));
                }
            }

            if(doc.contains("has_protected_content"))
            {
                if(doc["has_protected_content"].is_boolean())
                {
                    m_has_protected_content = doc["has_protected_content"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("has_protected_content"));
                }
            }

            if(doc.contains("has_restricted_voice_and_video_messages"))
            {
                if(doc["has_restricted_voice_and_video_messages"].is_boolean())
                {
                    m_has_restricted_voice_and_video_messages =
                      doc["has_restricted_voice_and_video_messages"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool(
                      "has_restricted_voice_and_video_"
                      "messages"));
                }
            }

            if(doc.contains("id"))
            {
                if(doc["id"].is_number_integer())
                {
                    m_id = doc["id"].get<std::int64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("id"));
            }

            if(doc.contains("invite_link"))
            {
                if(doc["invite_link"].is_string())
                {
                    m_invite_link = doc["invite_link"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("invite_link"));
                }
            }

            if(doc.contains("is_forum"))
            {
                if(doc["is_forum"].is_boolean())
                {
                    m_is_forum = doc["is_forum"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("is_forum"));
                }
            }

            if(doc.contains("join_by_request"))
            {
                if(doc["join_by_request"].is_boolean())
                {
                    m_join_by_request = doc["join_by_request"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("join_by_request"));
                }
            }

            if(doc.contains("join_to_send_messages"))
            {
                if(doc["join_to_send_messages"].is_boolean())
                {
                    m_join_to_send_messages = doc["join_to_send_messages"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("join_to_send_messages"));
                }
            }

            if(doc.contains("last_name"))
            {
                if(doc["last_name"].is_string())
                {
                    m_last_name = doc["last_name"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("last_name"));
                }
            }

            if(doc.contains("linked_chat_id"))
            {
                if(doc["linked_chat_id"].is_number_integer())
                {
                    m_linked_chat_id = doc["linked_chat_id"].get<std::int64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("linked_chat_id"));
                }
            }

            if(doc.contains("location"))
            {
                if(doc["location"].is_object())
                {
                    m_location = std::make_shared<ChatLocation>(doc["location"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("location"));
                }
            }

            if(doc.contains("message_auto_delete_time"))
            {
                if(doc["message_auto_delete_time"].is_number_integer())
                {
                    m_message_auto_delete_time = doc["message_auto_delete_time"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("message_auto_delete_time"));
                }
            }

            if(doc.contains("permissions"))
            {
                if(doc["permissions"].is_object())
                {
                    m_permissions = std::make_shared<ChatPermissions>(doc["permissions"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("permissions"));
                }
            }

            if(doc.contains("photo"))
            {
                if(doc["photo"].is_object())
                {
                    m_photo = std::make_shared<ChatPhoto>(doc["photo"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("photo"));
                }
            }

            if(doc.contains("pinned_message"))
            {
                if(doc["pinned_message"].is_object())
                {
                    m_pinned_message = std::make_shared<Message>(doc["pinned_message"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("pinned_message"));
                }
            }

            if(doc.contains("slow_mode_delay"))
            {
                if(doc["slow_mode_delay"].is_number_integer())
                {
                    m_slow_mode_delay = doc["slow_mode_delay"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("slow_mode_delay"));
                }
            }

            if(doc.contains("sticker_set_name"))
            {
                if(doc["sticker_set_name"].is_string())
                {
                    m_sticker_set_name = doc["sticker_set_name"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("sticker_set_name"));
                }
            }

            if(doc.contains("title"))
            {
                if(doc["title"].is_string())
                {
                    m_title = doc["title"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("title"));
                }
            }

            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }

            if(doc.contains("username"))
            {
                if(doc["username"].is_string())
                {
                    m_username = doc["username"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("username"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string Chat::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_active_usernames.has_value())
        {
            nlohmann::json active_usernames = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_active_usernames.value().size(); ++i)
            {
                active_usernames.emplace_back(m_active_usernames.value().at(i));
            }

            doc["active_usernames"] = active_usernames;
        }

        if(m_bio.has_value())
        {
            doc["bio"] = m_bio.value();
        }

        if(m_can_set_sticker_set.has_value())
        {
            doc["can_set_sticker_set"] = m_can_set_sticker_set.value();
        }

        if(m_description.has_value())
        {
            doc["description"] = m_description.value();
        }

        if(m_emoji_status_custom_emoji_id.has_value())
        {
            doc["emoji_status_custom_emoji_id"] = m_emoji_status_custom_emoji_id.value();
        }

        if(m_first_name.has_value())
        {
            doc["first_name"] = m_first_name.value();
        }

        if(m_has_aggressive_anti_spam_enabled.has_value())
        {
            doc["has_aggressive_anti_spam_enabled"] = m_has_aggressive_anti_spam_enabled.value();
        }

        if(m_has_hidden_members.has_value())
        {
            doc["has_hidden_members"] = m_has_hidden_members.value();
        }

        if(m_has_private_forwards.has_value())
        {
            doc["has_private_forwards"] = m_has_private_forwards.value();
        }

        if(m_has_protected_content.has_value())
        {
            doc["has_protected_content"] = m_has_protected_content.value();
        }

        if(m_has_restricted_voice_and_video_messages.has_value())
        {
            doc["has_restricted_voice_and_video_messages"] = m_has_restricted_voice_and_video_messages.value();
        }

        doc["id"] = m_id;

        if(m_invite_link.has_value())
        {
            doc["invite_link"] = m_invite_link.value();
        }

        if(m_is_forum.has_value())
        {
            doc["is_forum"] = m_is_forum.value();
        }

        if(m_join_by_request.has_value())
        {
            doc["join_by_request"] = m_join_by_request.value();
        }

        if(m_join_to_send_messages.has_value())
        {
            doc["join_to_send_messages"] = m_join_to_send_messages.value();
        }

        if(m_last_name.has_value())
        {
            doc["last_name"] = m_last_name.value();
        }

        if(m_linked_chat_id.has_value())
        {
            doc["linked_chat_id"] = m_linked_chat_id.value();
        }

        if(m_location.has_value())
        {
            doc["location"] = nlohmann::json::parse(m_location.value()->serialise());
        }

        if(m_message_auto_delete_time.has_value())
        {
            doc["message_auto_delete_time"] = m_message_auto_delete_time.value();
        }

        if(m_permissions.has_value())
        {
            doc["permissions"] = nlohmann::json::parse(m_permissions.value()->serialise());
        }

        if(m_photo.has_value())
        {
            doc["photo"] = nlohmann::json::parse(m_photo.value()->serialise());
        }

        if(m_pinned_message.has_value())
        {
            doc["pinned_message"] = nlohmann::json::parse(m_pinned_message.value()->serialise());
        }

        if(m_slow_mode_delay.has_value())
        {
            doc["slow_mode_delay"] = m_slow_mode_delay.value();
        }

        if(m_sticker_set_name.has_value())
        {
            doc["sticker_set_name"] = m_sticker_set_name.value();
        }

        if(m_title.has_value())
        {
            doc["title"] = m_title.value();
        }

        doc["type"] = m_type;

        if(m_username.has_value())
        {
            doc["username"] = m_username.value();
        }

        return doc.dump();
    }

    std::optional<std::vector<std::string>> Chat::get_active_usernames() const
    {
        return m_active_usernames;
    }

    std::optional<std::string> Chat::get_bio() const
    {
        return m_bio;
    }

    std::optional<bool> Chat::get_can_set_sticker_set() const
    {
        return m_can_set_sticker_set;
    }

    std::optional<std::string> Chat::get_description() const
    {
        return m_description;
    }

    std::optional<std::string> Chat::get_emoji_status_custom_emoji_id() const
    {
        return m_emoji_status_custom_emoji_id;
    }

    std::optional<std::string> Chat::get_first_name() const
    {
        return m_first_name;
    }

    std::optional<bool> Chat::get_has_aggressive_anti_spam_enabled() const
    {
        return m_has_aggressive_anti_spam_enabled;
    }

    std::optional<bool> Chat::get_has_hidden_members() const
    {
        return m_has_hidden_members;
    }

    std::optional<bool> Chat::get_has_private_forwards() const
    {
        return m_has_private_forwards;
    }

    std::optional<bool> Chat::get_has_protected_content() const
    {
        return m_has_protected_content;
    }

    std::optional<bool> Chat::get_has_restricted_voice_and_video_messages() const
    {
        return m_has_restricted_voice_and_video_messages;
    }

    std::int64_t Chat::get_id() const
    {
        return m_id;
    }

    std::optional<std::string> Chat::get_invite_link() const
    {
        return m_invite_link;
    }

    std::optional<bool> Chat::get_is_forum() const
    {
        return m_is_forum;
    }

    std::optional<bool> Chat::get_join_by_request() const
    {
        return m_join_by_request;
    }

    std::optional<bool> Chat::get_join_to_send_messages() const
    {
        return m_join_to_send_messages;
    }

    std::optional<std::string> Chat::get_last_name() const
    {
        return m_last_name;
    }

    std::optional<std::int64_t> Chat::get_linked_chat_id() const
    {
        return m_linked_chat_id;
    }

    std::optional<ChatLocation::ptr> Chat::get_location() const
    {
        return m_location;
    }

    std::optional<std::int32_t> Chat::get_message_auto_delete_time() const
    {
        return m_message_auto_delete_time;
    }

    std::optional<ChatPermissions::ptr> Chat::get_permissions() const
    {
        return m_permissions;
    }

    std::optional<ChatPhoto::ptr> Chat::get_photo() const
    {
        return m_photo;
    }

    std::optional<std::shared_ptr<Message>> Chat::get_pinned_message() const
    {
        return m_pinned_message;
    }

    std::optional<std::int32_t> Chat::get_slow_mode_delay() const
    {
        return m_slow_mode_delay;
    }

    std::optional<std::string> Chat::get_sticker_set_name() const
    {
        return m_sticker_set_name;
    }

    std::optional<std::string> Chat::get_title() const
    {
        return m_title;
    }

    std::string Chat::get_type() const
    {
        return m_type;
    }

    std::optional<std::string> Chat::get_username() const
    {
        return m_username;
    }

    void Chat::set_active_usernames(const std::optional<std::vector<std::string>> &active_usernames)
    {
        m_active_usernames = active_usernames;
    }

    void Chat::set_bio(const std::optional<std::string> &bio)
    {
        m_bio = bio;
    }

    void Chat::set_can_set_sticker_set(const std::optional<bool> &can_set_sticker_set)
    {
        m_can_set_sticker_set = can_set_sticker_set;
    }

    void Chat::set_description(const std::optional<std::string> &description)
    {
        m_description = description;
    }

    void Chat::set_emoji_status_custom_emoji_id(const std::optional<std::string> &emoji_status_custom_emoji_id)
    {
        m_emoji_status_custom_emoji_id = emoji_status_custom_emoji_id;
    }

    void Chat::set_first_name(const std::optional<std::string> &first_name)
    {
        m_first_name = first_name;
    }

    void Chat::set_has_aggressive_anti_spam_enabled(const std::optional<bool> &has_aggressive_anti_spam_enabled)
    {
        m_has_aggressive_anti_spam_enabled = has_aggressive_anti_spam_enabled;
    }

    void Chat::set_has_hidden_members(const std::optional<bool> &has_hidden_members)
    {
        m_has_hidden_members = has_hidden_members;
    }

    void Chat::set_has_private_forwards(const std::optional<bool> &has_private_forwards)
    {
        m_has_private_forwards = has_private_forwards;
    }

    void Chat::set_has_protected_content(const std::optional<bool> &has_protected_content)
    {
        m_has_protected_content = has_protected_content;
    }

    void Chat::set_has_restricted_voice_and_video_messages(
      const std::optional<bool> &has_restricted_voice_and_video_messages)
    {
        m_has_restricted_voice_and_video_messages = has_restricted_voice_and_video_messages;
    }

    void Chat::set_id(const std::int64_t &id)
    {
        m_id = id;
    }

    void Chat::set_invite_link(const std::optional<std::string> &invite_link)
    {
        m_invite_link = invite_link;
    }

    void Chat::set_is_forum(const std::optional<bool> &is_forum)
    {
        m_is_forum = is_forum;
    }

    void Chat::set_join_by_request(const std::optional<bool> &join_by_request)
    {
        m_join_by_request = join_by_request;
    }

    void Chat::set_join_to_send_messages(const std::optional<bool> &join_to_send_messages)
    {
        m_join_to_send_messages = join_to_send_messages;
    }

    void Chat::set_last_name(const std::optional<std::string> &last_name)
    {
        m_last_name = last_name;
    }

    void Chat::set_linked_chat_id(const std::optional<std::int64_t> &linked_chat_id)
    {
        m_linked_chat_id = linked_chat_id;
    }

    void Chat::set_location(const std::optional<ChatLocation::ptr> &location)
    {
        m_location = location;
    }

    void Chat::set_message_auto_delete_time(const std::optional<std::int32_t> &message_auto_delete_time)
    {
        m_message_auto_delete_time = message_auto_delete_time;
    }

    void Chat::set_permissions(const std::optional<ChatPermissions::ptr> &permissions)
    {
        m_permissions = permissions;
    }

    void Chat::set_photo(const std::optional<ChatPhoto::ptr> &photo)
    {
        m_photo = photo;
    }

    void Chat::set_pinned_message(const std::optional<std::shared_ptr<Message>> &pinned_message)
    {
        m_pinned_message = pinned_message;
    }

    void Chat::set_slow_mode_delay(const std::optional<std::int32_t> &slow_mode_delay)
    {
        m_slow_mode_delay = slow_mode_delay;
    }

    void Chat::set_sticker_set_name(const std::optional<std::string> &sticker_set_name)
    {
        m_sticker_set_name = sticker_set_name;
    }

    void Chat::set_title(const std::optional<std::string> &title)
    {
        m_title = title;
    }

    void Chat::set_type(const std::string &type)
    {
        m_type = type;
    }

    void Chat::set_username(const std::optional<std::string> &username)
    {
        m_username = username;
    }
} //namespace tgbot
