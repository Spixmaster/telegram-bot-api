#include "tgbot/types/KeyboardButtonPollType.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    KeyboardButtonPollType::KeyboardButtonPollType() = default;

    KeyboardButtonPollType::KeyboardButtonPollType(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string KeyboardButtonPollType::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_type.has_value())
        {
            doc["type"] = m_type.value();
        }

        return doc.dump();
    }

    std::optional<std::string> KeyboardButtonPollType::get_type() const
    {
        return m_type;
    }

    void KeyboardButtonPollType::set_type(const std::optional<std::string> &type)
    {
        m_type = type;
    }
} //namespace tgbot
