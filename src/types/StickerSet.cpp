#include "tgbot/types/StickerSet.h"

#include <algorithm>
#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "tgbot/constants/message.h"

namespace tgbot
{
    StickerSet::StickerSet() : m_is_animated(false), m_is_video(false)
    {}

    StickerSet::StickerSet(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("is_animated"))
            {
                if(doc["is_animated"].is_boolean())
                {
                    m_is_animated = doc["is_animated"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("is_animated"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("is_animated"));
            }

            if(doc.contains("is_video"))
            {
                if(doc["is_video"].is_boolean())
                {
                    m_is_video = doc["is_video"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("is_video"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("is_video"));
            }

            if(doc.contains("name"))
            {
                if(doc["name"].is_string())
                {
                    m_name = doc["name"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("name"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("name"));
            }

            if(doc.contains("sticker_type"))
            {
                if(doc["sticker_type"].is_string())
                {
                    m_sticker_type = doc["sticker_type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("sticker_type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("sticker_type"));
            }

            if(doc.contains("stickers"))
            {
                if(doc["stickers"].is_array())
                {
                    std::vector<Sticker::ptr> stickers;
                    stickers.reserve(doc["stickers"].size());

                    for(std::uint64_t i = 0; i < doc["stickers"].size(); ++i)
                    {
                        if(doc["stickers"].at(i).is_object())
                        {
                            stickers.emplace_back(std::make_shared<Sticker>(doc["stickers"].at(i).dump()));
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_object(
                              "sticker"
                              "s"));
                        }
                    }

                    m_stickers = stickers;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("stickers"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("stickers"));
            }

            if(doc.contains("thumbnail"))
            {
                if(doc["thumbnail"].is_object())
                {
                    m_thumbnail = std::make_shared<PhotoSize>(doc["thumbnail"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("thumbnail"));
                }
            }

            if(doc.contains("title"))
            {
                if(doc["title"].is_string())
                {
                    m_title = doc["title"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("title"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("title"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string StickerSet::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["is_animated"] = m_is_animated;
        doc["is_video"] = m_is_video;
        doc["name"] = m_name;
        doc["sticker_type"] = m_sticker_type;

        {
            nlohmann::json stickers = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_stickers.size(); ++i)
            {
                stickers.emplace_back(nlohmann::json::parse(m_stickers.at(i)->serialise()));
            }

            doc["stickers"] = stickers;
        }

        if(m_thumbnail.has_value())
        {
            doc["thumbnail"] = nlohmann::json::parse(m_thumbnail.value()->serialise());
        }

        doc["title"] = m_title;
        return doc.dump();
    }

    bool StickerSet::get_is_animated() const
    {
        return m_is_animated;
    }

    bool StickerSet::get_is_video() const
    {
        return m_is_video;
    }

    std::string StickerSet::get_name() const
    {
        return m_name;
    }

    std::string StickerSet::get_sticker_type() const
    {
        return m_sticker_type;
    }

    std::vector<Sticker::ptr> StickerSet::get_stickers() const
    {
        return m_stickers;
    }

    std::optional<PhotoSize::ptr> StickerSet::get_thumbnail() const
    {
        return m_thumbnail;
    }

    std::string StickerSet::get_title() const
    {
        return m_title;
    }

    void StickerSet::set_is_animated(const bool &is_animated)
    {
        m_is_animated = is_animated;
    }

    void StickerSet::set_is_video(const bool &is_video)
    {
        m_is_video = is_video;
    }

    void StickerSet::set_name(const std::string &name)
    {
        m_name = name;
    }

    void StickerSet::set_sticker_type(const std::string &sticker_type)
    {
        m_sticker_type = sticker_type;
    }

    void StickerSet::set_stickers(const std::vector<Sticker::ptr> &stickers)
    {
        m_stickers = stickers;
    }

    void StickerSet::set_thumbnail(const std::optional<PhotoSize::ptr> &thumbnail)
    {
        m_thumbnail = thumbnail;
    }

    void StickerSet::set_title(const std::string &title)
    {
        m_title = title;
    }
} //namespace tgbot
