#include "tgbot/types/InlineKeyboardMarkup.h"

#include <algorithm>
#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <utility>

#include "tgbot/constants/message.h"

namespace tgbot
{
    InlineKeyboardMarkup::InlineKeyboardMarkup() = default;

    InlineKeyboardMarkup::InlineKeyboardMarkup(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("inline_keyboard"))
            {
                if(doc["inline_keyboard"].is_array())
                {
                    std::vector<std::vector<InlineKeyboardButton::ptr>> inline_keyboard;
                    inline_keyboard.resize(doc["inline_keyboard"].size());

                    for(std::uint64_t i = 0; i < doc["inline_keyboard"].size(); ++i)
                    {
                        if(doc["inline_keyboard"].at(i).is_array())
                        {
                            inline_keyboard.at(i).reserve(doc["inline_keyboard"].at(i).size());

                            for(std::uint64_t j = 0; j < doc["inline_keyboard"].at(i).size(); ++j)
                            {
                                if(doc["inline_keyboard"].at(i).at(j).is_object())
                                {
                                    inline_keyboard.at(i).emplace_back(std::make_shared<InlineKeyboardButton>(
                                      doc["inline_keyboard"].at(i).at(j).dump()));
                                }
                                else
                                {
                                    throw std::invalid_argument(message::json::value::nested::not_object(
                                      "inline_"
                                      "keyboard"));
                                }
                            }
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_array(
                              "inline_"
                              "keyboar"
                              "d"));
                        }
                    }

                    m_inline_keyboard = inline_keyboard;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("inline_keyboard"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("inline_keyboard"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    InlineKeyboardMarkup::InlineKeyboardMarkup(std::vector<std::vector<InlineKeyboardButton::ptr>> inline_keyboard) :
        m_inline_keyboard(std::move(inline_keyboard))
    {}

    std::string InlineKeyboardMarkup::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        {
            nlohmann::json inline_keyboard = nlohmann::json::array();

            if(m_inline_keyboard.empty())
            {
                inline_keyboard.emplace_back(nlohmann::json::array());
            }
            else
            {
                for(std::uint64_t i = 0; i < m_inline_keyboard.size(); ++i)
                {
                    nlohmann::json inline_keyboard_row = nlohmann::json::array();

                    for(std::uint64_t j = 0; j < m_inline_keyboard.at(i).size(); ++j)
                    {
                        inline_keyboard_row.emplace_back(
                          nlohmann::json::parse(m_inline_keyboard.at(i).at(j)->serialise()));
                    }

                    inline_keyboard.emplace_back(inline_keyboard_row);
                }
            }

            doc["inline_keyboard"] = inline_keyboard;
        }

        return doc.dump();
    }

    std::vector<std::vector<InlineKeyboardButton::ptr>> InlineKeyboardMarkup::get_inline_keyboard() const
    {
        return m_inline_keyboard;
    }

    void InlineKeyboardMarkup::set_inline_keyboard(
      const std::vector<std::vector<InlineKeyboardButton::ptr>> &inline_keyboard)
    {
        m_inline_keyboard = inline_keyboard;
    }
} //namespace tgbot
