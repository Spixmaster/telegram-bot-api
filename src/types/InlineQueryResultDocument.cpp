#include "tgbot/types/InlineQueryResultDocument.h"

#include <algorithm>
#include <initializer_list>
#include <map>
#include <memory>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <string>
#include <vector>

#include "tgbot/constants/message.h"
#include "tgbot/types/InlineKeyboardMarkup.h"
#include "tgbot/types/InputMessageContent.h"
#include "tgbot/types/MessageEntity.h"

namespace tgbot
{
    InlineQueryResultDocument::InlineQueryResultDocument() = default;

    InlineQueryResultDocument::InlineQueryResultDocument(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("caption"))
            {
                if(doc["caption"].is_string())
                {
                    m_caption = doc["caption"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("caption"));
                }
            }

            if(doc.contains("caption_entities"))
            {
                if(doc["caption_entities"].is_array())
                {
                    std::vector<MessageEntity::ptr> caption_entities;
                    caption_entities.reserve(doc["caption_entities"].size());

                    for(std::uint64_t i = 0; i < doc["caption_entities"].size(); ++i)
                    {
                        if(doc["caption_entities"].at(i).is_object())
                        {
                            caption_entities.emplace_back(
                              std::make_shared<MessageEntity>(doc["caption_entities"].at(i).dump()));
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_object(
                              "caption_"
                              "entitie"
                              "s"));
                        }
                    }

                    m_caption_entities = caption_entities;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("caption_entities"));
                }
            }

            if(doc.contains("description"))
            {
                if(doc["description"].is_string())
                {
                    m_description = doc["description"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("description"));
                }
            }

            if(doc.contains("document_url"))
            {
                if(doc["document_url"].is_string())
                {
                    m_document_url = doc["document_url"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("document_url"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("document_url"));
            }

            if(doc.contains("id"))
            {
                if(doc["id"].is_string())
                {
                    m_id = doc["id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("id"));
            }

            if(doc.contains("input_message_content"))
            {
                if(doc["input_message_content"].is_object())
                {
                    m_input_message_content = std::make_shared<InputMessageContent>(doc["input_message_content"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("input_message_content"));
                }
            }

            if(doc.contains("mime_type"))
            {
                if(doc["mime_type"].is_string())
                {
                    m_mime_type = doc["mime_type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("mime_type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("mime_type"));
            }

            if(doc.contains("parse_mode"))
            {
                if(doc["parse_mode"].is_string())
                {
                    m_parse_mode = doc["parse_mode"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("parse_mode"));
                }
            }

            if(doc.contains("reply_markup"))
            {
                if(doc["reply_markup"].is_object())
                {
                    m_reply_markup = std::make_shared<InlineKeyboardMarkup>(
                      doc["reply_"
                          "marku"
                          "p"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("reply_markup"));
                }
            }

            if(doc.contains("thumbnail_height"))
            {
                if(doc["thumbnail_height"].is_number_integer())
                {
                    m_thumbnail_height = doc["thumbnail_height"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("thumbnail_height"));
                }
            }

            if(doc.contains("thumbnail_url"))
            {
                if(doc["thumbnail_url"].is_string())
                {
                    m_thumbnail_url = doc["thumbnail_url"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("thumbnail_url"));
                }
            }

            if(doc.contains("thumbnail_width"))
            {
                if(doc["thumbnail_width"].is_number_integer())
                {
                    m_thumbnail_width = doc["thumbnail_width"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("thumbnail_width"));
                }
            }

            if(doc.contains("title"))
            {
                if(doc["title"].is_string())
                {
                    m_title = doc["title"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("title"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("title"));
            }

            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string InlineQueryResultDocument::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_caption.has_value())
        {
            doc["caption"] = m_caption.value();
        }

        if(m_caption_entities.has_value())
        {
            nlohmann::json caption_entities = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_caption_entities.value().size(); ++i)
            {
                caption_entities.emplace_back(nlohmann::json::parse(m_caption_entities.value().at(i)->serialise()));
            }

            doc["caption_entities"] = caption_entities;
        }

        if(m_description.has_value())
        {
            doc["description"] = m_description.value();
        }

        doc["document_url"] = m_document_url;
        doc["id"] = m_id;

        if(m_input_message_content.has_value())
        {
            doc["input_message_content"] = nlohmann::json::parse(m_input_message_content.value()->serialise());
        }

        doc["mime_type"] = m_mime_type;

        if(m_parse_mode.has_value())
        {
            doc["parse_mode"] = m_parse_mode.value();
        }

        if(m_reply_markup.has_value())
        {
            doc["reply_markup"] = nlohmann::json::parse(m_reply_markup.value()->serialise());
        }

        if(m_thumbnail_height.has_value())
        {
            doc["thumbnail_height"] = m_thumbnail_height.value();
        }

        if(m_thumbnail_url.has_value())
        {
            doc["thumbnail_url"] = m_thumbnail_url.value();
        }

        if(m_thumbnail_width.has_value())
        {
            doc["thumbnail_width"] = m_thumbnail_width.value();
        }

        doc["title"] = m_title;
        doc["type"] = m_type;
        return doc.dump();
    }

    std::optional<std::string> InlineQueryResultDocument::get_caption() const
    {
        return m_caption;
    }

    std::optional<std::vector<MessageEntity::ptr>> InlineQueryResultDocument::get_caption_entities() const
    {
        return m_caption_entities;
    }

    std::optional<std::string> InlineQueryResultDocument::get_description() const
    {
        return m_description;
    }

    std::string InlineQueryResultDocument::get_document_url() const
    {
        return m_document_url;
    }

    std::string InlineQueryResultDocument::get_id() const
    {
        return m_id;
    }

    std::optional<InputMessageContent::ptr> InlineQueryResultDocument::get_input_message_content() const
    {
        return m_input_message_content;
    }

    std::string InlineQueryResultDocument::get_mime_type() const
    {
        return m_mime_type;
    }

    std::optional<std::string> InlineQueryResultDocument::get_parse_mode() const
    {
        return m_parse_mode;
    }

    std::optional<InlineKeyboardMarkup::ptr> InlineQueryResultDocument::get_reply_markup() const
    {
        return m_reply_markup;
    }

    std::optional<std::int32_t> InlineQueryResultDocument::get_thumbnail_height() const
    {
        return m_thumbnail_height;
    }

    std::optional<std::string> InlineQueryResultDocument::get_thumbnail_url() const
    {
        return m_thumbnail_url;
    }

    std::optional<std::int32_t> InlineQueryResultDocument::get_thumbnail_width() const
    {
        return m_thumbnail_width;
    }

    std::string InlineQueryResultDocument::get_title() const
    {
        return m_title;
    }

    std::string InlineQueryResultDocument::get_type() const
    {
        return m_type;
    }

    void InlineQueryResultDocument::set_caption(const std::optional<std::string> &caption)
    {
        m_caption = caption;
    }

    void InlineQueryResultDocument::set_caption_entities(
      const std::optional<std::vector<MessageEntity::ptr>> &caption_entities)
    {
        m_caption_entities = caption_entities;
    }

    void InlineQueryResultDocument::set_description(const std::optional<std::string> &description)
    {
        m_description = description;
    }

    void InlineQueryResultDocument::set_document_url(const std::string &document_url)
    {
        m_document_url = document_url;
    }

    void InlineQueryResultDocument::set_id(const std::string &id)
    {
        m_id = id;
    }

    void InlineQueryResultDocument::set_input_message_content(
      const std::optional<InputMessageContent::ptr> &input_message_content)
    {
        m_input_message_content = input_message_content;
    }

    void InlineQueryResultDocument::set_mime_type(const std::string &mime_type)
    {
        m_mime_type = mime_type;
    }

    void InlineQueryResultDocument::set_parse_mode(const std::optional<std::string> &parse_mode)
    {
        m_parse_mode = parse_mode;
    }

    void InlineQueryResultDocument::set_reply_markup(const std::optional<InlineKeyboardMarkup::ptr> &reply_markup)
    {
        m_reply_markup = reply_markup;
    }

    void InlineQueryResultDocument::set_thumbnail_height(const std::optional<std::int32_t> &thumbnail_height)
    {
        m_thumbnail_height = thumbnail_height;
    }

    void InlineQueryResultDocument::set_thumbnail_url(const std::optional<std::string> &thumbnail_url)
    {
        m_thumbnail_url = thumbnail_url;
    }

    void InlineQueryResultDocument::set_thumbnail_width(const std::optional<std::int32_t> &thumbnail_width)
    {
        m_thumbnail_width = thumbnail_width;
    }

    void InlineQueryResultDocument::set_title(const std::string &title)
    {
        m_title = title;
    }

    void InlineQueryResultDocument::set_type(const std::string &type)
    {
        m_type = type;
    }
} //namespace tgbot
