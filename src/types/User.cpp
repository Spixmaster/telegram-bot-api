#include "tgbot/types/User.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    User::User() : m_id(-1), m_is_bot(false)
    {}

    User::User(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("added_to_attachment_menu"))
            {
                if(doc["added_to_attachment_menu"].is_boolean())
                {
                    m_added_to_attachment_menu = doc["added_to_attachment_menu"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("added_to_attachment_menu"));
                }
            }

            if(doc.contains("can_join_groups"))
            {
                if(doc["can_join_groups"].is_boolean())
                {
                    m_can_join_groups = doc["can_join_groups"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_join_groups"));
                }
            }

            if(doc.contains("can_read_all_group_messages"))
            {
                if(doc["can_read_all_group_messages"].is_boolean())
                {
                    m_can_read_all_group_messages = doc["can_read_all_group_messages"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_read_all_group_messages"));
                }
            }

            if(doc.contains("first_name"))
            {
                if(doc["first_name"].is_string())
                {
                    m_first_name = doc["first_name"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("first_name"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("first_name"));
            }

            if(doc.contains("id"))
            {
                if(doc["id"].is_number_integer())
                {
                    m_id = doc["id"].get<std::int64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("id"));
            }

            if(doc.contains("is_bot"))
            {
                if(doc["is_bot"].is_boolean())
                {
                    m_is_bot = doc["is_bot"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("is_bot"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("is_bot"));
            }

            if(doc.contains("is_premium"))
            {
                if(doc["is_premium"].is_boolean())
                {
                    m_is_premium = doc["is_premium"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("is_premium"));
                }
            }

            if(doc.contains("language_code"))
            {
                if(doc["language_code"].is_string())
                {
                    m_language_code = doc["language_code"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("language_code"));
                }
            }

            if(doc.contains("last_name"))
            {
                if(doc["last_name"].is_string())
                {
                    m_last_name = doc["last_name"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("last_name"));
                }
            }

            if(doc.contains("supports_inline_queries"))
            {
                if(doc["supports_inline_queries"].is_boolean())
                {
                    m_supports_inline_queries = doc["supports_inline_queries"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("supports_inline_queries"));
                }
            }

            if(doc.contains("username"))
            {
                if(doc["username"].is_string())
                {
                    m_username = doc["username"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("username"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string User::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_added_to_attachment_menu.has_value())
        {
            doc["added_to_attachment_menu"] = m_added_to_attachment_menu.value();
        }

        if(m_can_join_groups.has_value())
        {
            doc["can_join_groups"] = m_can_join_groups.value();
        }

        if(m_can_read_all_group_messages.has_value())
        {
            doc["can_read_all_group_messages"] = m_can_read_all_group_messages.value();
        }

        doc["first_name"] = m_first_name;
        doc["id"] = m_id;
        doc["is_bot"] = m_is_bot;

        if(m_is_premium.has_value())
        {
            doc["is_premium"] = m_is_premium.value();
        }

        if(m_language_code.has_value())
        {
            doc["language_code"] = m_language_code.value();
        }

        if(m_last_name.has_value())
        {
            doc["last_name"] = m_last_name.value();
        }

        if(m_supports_inline_queries.has_value())
        {
            doc["supports_inline_queries"] = m_supports_inline_queries.value();
        }

        if(m_username.has_value())
        {
            doc["username"] = m_username.value();
        }

        return doc.dump();
    }

    std::optional<bool> User::get_added_to_attachment_menu() const
    {
        return m_added_to_attachment_menu;
    }

    std::optional<bool> User::get_can_join_groups() const
    {
        return m_can_join_groups;
    }

    std::optional<bool> User::get_can_read_all_group_messages() const
    {
        return m_can_read_all_group_messages;
    }

    std::string User::get_first_name() const
    {
        return m_first_name;
    }

    std::int64_t User::get_id() const
    {
        return m_id;
    }

    bool User::get_is_bot() const
    {
        return m_is_bot;
    }

    std::optional<bool> User::get_is_premium() const
    {
        return m_is_premium;
    }

    std::optional<std::string> User::get_language_code() const
    {
        return m_language_code;
    }

    std::optional<std::string> User::get_last_name() const
    {
        return m_last_name;
    }

    std::optional<bool> User::get_supports_inline_queries() const
    {
        return m_supports_inline_queries;
    }

    std::optional<std::string> User::get_username() const
    {
        return m_username;
    }

    void User::set_added_to_attachment_menu(const std::optional<bool> &added_to_attachment_menu)
    {
        m_added_to_attachment_menu = added_to_attachment_menu;
    }

    void User::set_can_join_groups(const std::optional<bool> &can_join_groups)
    {
        m_can_join_groups = can_join_groups;
    }

    void User::set_can_read_all_group_messages(const std::optional<bool> &can_read_all_group_messages)
    {
        m_can_read_all_group_messages = can_read_all_group_messages;
    }

    void User::set_first_name(const std::string &first_name)
    {
        m_first_name = first_name;
    }

    void User::set_id(const std::int64_t &id)
    {
        m_id = id;
    }

    void User::set_is_bot(const bool &is_bot)
    {
        m_is_bot = is_bot;
    }

    void User::set_is_premium(const std::optional<bool> &is_premium)
    {
        m_is_premium = is_premium;
    }

    void User::set_language_code(const std::optional<std::string> &language_code)
    {
        m_language_code = language_code;
    }

    void User::set_last_name(const std::optional<std::string> &last_name)
    {
        m_last_name = last_name;
    }

    void User::set_supports_inline_queries(const std::optional<bool> &supports_inline_queries)
    {
        m_supports_inline_queries = supports_inline_queries;
    }

    void User::set_username(const std::optional<std::string> &username)
    {
        m_username = username;
    }
} //namespace tgbot
