#include "tgbot/types/BotShortDescription.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    BotShortDescription::BotShortDescription() = default;

    BotShortDescription::BotShortDescription(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("short_description"))
            {
                if(doc["short_description"].is_string())
                {
                    m_short_description = doc["short_description"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("short_description"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("short_description"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string BotShortDescription::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["short_description"] = m_short_description;
        return doc.dump();
    }

    std::string BotShortDescription::get_short_description() const
    {
        return m_short_description;
    }

    void BotShortDescription::set_short_description(const std::string &short_description)
    {
        m_short_description = short_description;
    }
} //namespace tgbot
