#include "tgbot/types/ResponseParameters.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    ResponseParameters::ResponseParameters() = default;

    ResponseParameters::ResponseParameters(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("migrate_to_chat_id"))
            {
                if(doc["migrate_to_chat_id"].is_number_integer())
                {
                    m_migrate_to_chat_id = doc["migrate_to_chat_id"].get<std::int64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("migrate_to_chat_id"));
                }
            }

            if(doc.contains("retry_after"))
            {
                if(doc["retry_after"].is_number_integer())
                {
                    m_retry_after = doc["retry_after"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("retry_after"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string ResponseParameters::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_migrate_to_chat_id.has_value())
        {
            doc["migrate_to_chat_id"] = m_migrate_to_chat_id.value();
        }

        if(m_retry_after.has_value())
        {
            doc["retry_after"] = m_retry_after.value();
        }

        return doc.dump();
    }

    std::optional<std::int64_t> ResponseParameters::get_migrate_to_chat_id() const
    {
        return m_migrate_to_chat_id;
    }

    std::optional<std::int32_t> ResponseParameters::get_retry_after() const
    {
        return m_retry_after;
    }

    void ResponseParameters::set_migrate_to_chat_id(const std::optional<std::int64_t> &migrate_to_chat_id)
    {
        m_migrate_to_chat_id = migrate_to_chat_id;
    }

    void ResponseParameters::set_retry_after(const std::optional<std::int32_t> &retry_after)
    {
        m_retry_after = retry_after;
    }
} //namespace tgbot
