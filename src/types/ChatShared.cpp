#include "tgbot/types/ChatShared.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    ChatShared::ChatShared() : m_chat_id(-1), m_request_id(-1)
    {}

    ChatShared::ChatShared(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("chat_id"))
            {
                if(doc["chat_id"].is_number_integer())
                {
                    m_chat_id = doc["chat_id"].get<std::int64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("chat_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("chat_id"));
            }

            if(doc.contains("request_id"))
            {
                if(doc["request_id"].is_number_integer())
                {
                    m_request_id = doc["request_id"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("request_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("request_id"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string ChatShared::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["chat_id"] = m_chat_id;
        doc["request_id"] = m_request_id;
        return doc.dump();
    }

    std::int64_t ChatShared::get_chat_id() const
    {
        return m_chat_id;
    }

    std::int32_t ChatShared::get_request_id() const
    {
        return m_request_id;
    }

    void ChatShared::set_chat_id(const std::int64_t &chat_id)
    {
        m_chat_id = chat_id;
    }

    void ChatShared::set_request_id(const std::int32_t &request_id)
    {
        m_request_id = request_id;
    }
} //namespace tgbot
