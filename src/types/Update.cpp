#include "tgbot/types/Update.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    Update::Update() : m_update_id(-1)
    {}

    Update::Update(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("callback_query"))
            {
                if(doc["callback_query"].is_object())
                {
                    m_callback_query = std::make_shared<CallbackQuery>(
                      doc["callback_"
                          "query"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("callback_query"));
                }
            }

            if(doc.contains("channel_post"))
            {
                if(doc["channel_post"].is_object())
                {
                    m_channel_post = std::make_shared<Message>(doc["channel_post"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("channel_post"));
                }
            }

            if(doc.contains("chat_join_request"))
            {
                if(doc["chat_join_request"].is_object())
                {
                    m_chat_join_request = std::make_shared<ChatJoinRequest>(
                      doc["chat_"
                          "join_"
                          "reques"
                          "t"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("chat_join_request"));
                }
            }

            if(doc.contains("chat_member"))
            {
                if(doc["chat_member"].is_object())
                {
                    m_chat_member = std::make_shared<ChatMemberUpdated>(
                      doc["chat_"
                          "member"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("chat_member"));
                }
            }

            if(doc.contains("chosen_inline_result"))
            {
                if(doc["chosen_inline_result"].is_object())
                {
                    m_chosen_inline_result = std::make_shared<ChosenInlineResult>(doc["chosen_inline_result"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("chosen_inline_result"));
                }
            }

            if(doc.contains("edited_channel_post"))
            {
                if(doc["edited_channel_post"].is_object())
                {
                    m_edited_channel_post = std::make_shared<Message>(
                      doc["edited_"
                          "channel_"
                          "post"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("edited_channel_post"));
                }
            }

            if(doc.contains("edited_message"))
            {
                if(doc["edited_message"].is_object())
                {
                    m_edited_message = std::make_shared<Message>(doc["edited_message"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("edited_message"));
                }
            }

            if(doc.contains("inline_query"))
            {
                if(doc["inline_query"].is_object())
                {
                    m_inline_query = std::make_shared<InlineQuery>(doc["inline_query"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("inline_query"));
                }
            }

            if(doc.contains("message"))
            {
                if(doc["message"].is_object())
                {
                    m_message = std::make_shared<Message>(doc["message"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("message"));
                }
            }

            if(doc.contains("my_chat_member"))
            {
                if(doc["my_chat_member"].is_object())
                {
                    m_my_chat_member = std::make_shared<ChatMemberUpdated>(
                      doc["my_chat_"
                          "membe"
                          "r"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("my_chat_member"));
                }
            }

            if(doc.contains("poll"))
            {
                if(doc["poll"].is_object())
                {
                    m_poll = std::make_shared<Poll>(doc["poll"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("poll"));
                }
            }

            if(doc.contains("poll_answer"))
            {
                if(doc["poll_answer"].is_object())
                {
                    m_poll_answer = std::make_shared<PollAnswer>(doc["poll_answer"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("poll_answer"));
                }
            }

            if(doc.contains("pre_checkout_query"))
            {
                if(doc["pre_checkout_query"].is_object())
                {
                    m_pre_checkout_query = std::make_shared<PreCheckoutQuery>(doc["pre_checkout_query"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("pre_checkout_query"));
                }
            }

            if(doc.contains("shipping_query"))
            {
                if(doc["shipping_query"].is_object())
                {
                    m_shipping_query = std::make_shared<ShippingQuery>(
                      doc["shipping_"
                          "query"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("shipping_query"));
                }
            }

            if(doc.contains("update_id"))
            {
                if(doc["update_id"].is_number_integer())
                {
                    m_update_id = doc["update_id"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("update_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("update_id"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string Update::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_callback_query.has_value())
        {
            doc["callback_query"] = nlohmann::json::parse(m_callback_query.value()->serialise());
        }

        if(m_channel_post.has_value())
        {
            doc["channel_post"] = nlohmann::json::parse(m_channel_post.value()->serialise());
        }

        if(m_chat_join_request.has_value())
        {
            doc["chat_join_request"] = nlohmann::json::parse(m_chat_join_request.value()->serialise());
        }

        if(m_chat_member.has_value())
        {
            doc["chat_member"] = nlohmann::json::parse(m_chat_member.value()->serialise());
        }

        if(m_chosen_inline_result.has_value())
        {
            doc["chosen_inline_result"] = nlohmann::json::parse(m_chosen_inline_result.value()->serialise());
        }

        if(m_edited_channel_post.has_value())
        {
            doc["edited_channel_post"] = nlohmann::json::parse(m_edited_channel_post.value()->serialise());
        }

        if(m_edited_message.has_value())
        {
            doc["edited_message"] = nlohmann::json::parse(m_edited_message.value()->serialise());
        }

        if(m_inline_query.has_value())
        {
            doc["inline_query"] = nlohmann::json::parse(m_inline_query.value()->serialise());
        }

        if(m_message.has_value())
        {
            doc["message"] = nlohmann::json::parse(m_message.value()->serialise());
        }

        if(m_my_chat_member.has_value())
        {
            doc["my_chat_member"] = nlohmann::json::parse(m_my_chat_member.value()->serialise());
        }

        if(m_poll.has_value())
        {
            doc["poll"] = nlohmann::json::parse(m_poll.value()->serialise());
        }

        if(m_poll_answer.has_value())
        {
            doc["poll_answer"] = nlohmann::json::parse(m_poll_answer.value()->serialise());
        }

        if(m_pre_checkout_query.has_value())
        {
            doc["pre_checkout_query"] = nlohmann::json::parse(m_pre_checkout_query.value()->serialise());
        }

        if(m_shipping_query.has_value())
        {
            doc["shipping_query"] = nlohmann::json::parse(m_shipping_query.value()->serialise());
        }

        doc["update_id"] = m_update_id;
        return doc.dump();
    }

    std::optional<CallbackQuery::ptr> Update::get_callback_query() const
    {
        return m_callback_query;
    }

    std::optional<Message::ptr> Update::get_channel_post() const
    {
        return m_channel_post;
    }

    std::optional<ChatJoinRequest::ptr> Update::get_chat_join_request() const
    {
        return m_chat_join_request;
    }

    std::optional<ChatMemberUpdated::ptr> Update::get_chat_member() const
    {
        return m_chat_member;
    }

    std::optional<ChosenInlineResult::ptr> Update::get_chosen_inline_result() const
    {
        return m_chosen_inline_result;
    }

    std::optional<Message::ptr> Update::get_edited_channel_post() const
    {
        return m_edited_channel_post;
    }

    std::optional<Message::ptr> Update::get_edited_message() const
    {
        return m_edited_message;
    }

    std::optional<InlineQuery::ptr> Update::get_inline_query() const
    {
        return m_inline_query;
    }

    std::optional<Message::ptr> Update::get_message() const
    {
        return m_message;
    }

    std::optional<ChatMemberUpdated::ptr> Update::get_my_chat_member() const
    {
        return m_my_chat_member;
    }

    std::optional<Poll::ptr> Update::get_poll() const
    {
        return m_poll;
    }

    std::optional<PollAnswer::ptr> Update::get_poll_answer() const
    {
        return m_poll_answer;
    }

    std::optional<PreCheckoutQuery::ptr> Update::get_pre_checkout_query() const
    {
        return m_pre_checkout_query;
    }

    std::optional<ShippingQuery::ptr> Update::get_shipping_query() const
    {
        return m_shipping_query;
    }

    std::int32_t Update::get_update_id() const
    {
        return m_update_id;
    }

    void Update::set_callback_query(const std::optional<CallbackQuery::ptr> &callback_query)
    {
        m_callback_query = callback_query;
    }

    void Update::set_channel_post(const std::optional<Message::ptr> &channel_post)
    {
        m_channel_post = channel_post;
    }

    void Update::set_chat_join_request(const std::optional<ChatJoinRequest::ptr> &chat_join_request)
    {
        m_chat_join_request = chat_join_request;
    }

    void Update::set_chat_member(const std::optional<ChatMemberUpdated::ptr> &chat_member)
    {
        m_chat_member = chat_member;
    }

    void Update::set_chosen_inline_result(const std::optional<ChosenInlineResult::ptr> &chosen_inline_result)
    {
        m_chosen_inline_result = chosen_inline_result;
    }

    void Update::set_edited_channel_post(const std::optional<Message::ptr> &edited_channel_post)
    {
        m_edited_channel_post = edited_channel_post;
    }

    void Update::set_edited_message(const std::optional<Message::ptr> &edited_message)
    {
        m_edited_message = edited_message;
    }

    void Update::set_inline_query(const std::optional<InlineQuery::ptr> &inline_query)
    {
        m_inline_query = inline_query;
    }

    void Update::set_message(const std::optional<Message::ptr> &message)
    {
        m_message = message;
    }

    void Update::set_my_chat_member(const std::optional<ChatMemberUpdated::ptr> &my_chat_member)
    {
        m_my_chat_member = my_chat_member;
    }

    void Update::set_poll(const std::optional<Poll::ptr> &poll)
    {
        m_poll = poll;
    }

    void Update::set_poll_answer(const std::optional<PollAnswer::ptr> &poll_answer)
    {
        m_poll_answer = poll_answer;
    }

    void Update::set_pre_checkout_query(const std::optional<PreCheckoutQuery::ptr> &pre_checkout_query)
    {
        m_pre_checkout_query = pre_checkout_query;
    }

    void Update::set_shipping_query(const std::optional<ShippingQuery::ptr> &shipping_query)
    {
        m_shipping_query = shipping_query;
    }

    void Update::set_update_id(const std::int32_t &update_id)
    {
        m_update_id = update_id;
    }
} //namespace tgbot
