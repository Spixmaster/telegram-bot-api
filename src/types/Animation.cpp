#include "tgbot/types/Animation.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    Animation::Animation() : m_duration(-1), m_height(-1), m_width(-1)
    {}

    Animation::Animation(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("duration"))
            {
                if(doc["duration"].is_number_integer())
                {
                    m_duration = doc["duration"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("duration"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("duration"));
            }

            if(doc.contains("file_id"))
            {
                if(doc["file_id"].is_string())
                {
                    m_file_id = doc["file_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("file_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("file_id"));
            }

            if(doc.contains("file_name"))
            {
                if(doc["file_name"].is_string())
                {
                    m_file_name = doc["file_name"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("file_name"));
                }
            }

            if(doc.contains("file_size"))
            {
                if(doc["file_size"].is_number_integer())
                {
                    m_file_size = doc["file_size"].get<std::int64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("file_size"));
                }
            }

            if(doc.contains("file_unique_id"))
            {
                if(doc["file_unique_id"].is_string())
                {
                    m_file_unique_id = doc["file_unique_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("file_unique_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("file_unique_id"));
            }

            if(doc.contains("height"))
            {
                if(doc["height"].is_number_integer())
                {
                    m_height = doc["height"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("height"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("height"));
            }

            if(doc.contains("mime_type"))
            {
                if(doc["mime_type"].is_string())
                {
                    m_mime_type = doc["mime_type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("mime_type"));
                }
            }

            if(doc.contains("thumbnail"))
            {
                if(doc["thumbnail"].is_object())
                {
                    m_thumbnail = std::make_shared<PhotoSize>(doc["thumbnail"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("thumbnail"));
                }
            }

            if(doc.contains("width"))
            {
                if(doc["width"].is_number_integer())
                {
                    m_width = doc["width"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("width"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("width"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string Animation::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["duration"] = m_duration;
        doc["file_id"] = m_file_id;

        if(m_file_name.has_value())
        {
            doc["file_name"] = m_file_name.value();
        }

        if(m_file_size.has_value())
        {
            doc["file_size"] = m_file_size.value();
        }

        doc["file_unique_id"] = m_file_unique_id;
        doc["height"] = m_height;

        if(m_mime_type.has_value())
        {
            doc["mime_type"] = m_mime_type.value();
        }

        if(m_thumbnail.has_value())
        {
            doc["thumbnail"] = nlohmann::json::parse(m_thumbnail.value()->serialise());
        }

        doc["width"] = m_width;
        return doc.dump();
    }

    std::int32_t Animation::get_duration() const
    {
        return m_duration;
    }

    std::string Animation::get_file_id() const
    {
        return m_file_id;
    }

    std::optional<std::string> Animation::get_file_name() const
    {
        return m_file_name;
    }

    std::optional<std::int64_t> Animation::get_file_size() const
    {
        return m_file_size;
    }

    std::string Animation::get_file_unique_id() const
    {
        return m_file_unique_id;
    }

    std::int32_t Animation::get_height() const
    {
        return m_height;
    }

    std::optional<std::string> Animation::get_mime_type() const
    {
        return m_mime_type;
    }

    std::optional<PhotoSize::ptr> Animation::get_thumbnail() const
    {
        return m_thumbnail;
    }

    std::int32_t Animation::get_width() const
    {
        return m_width;
    }

    void Animation::set_duration(const std::int32_t &duration)
    {
        m_duration = duration;
    }

    void Animation::set_file_id(const std::string &file_id)
    {
        m_file_id = file_id;
    }

    void Animation::set_file_name(const std::optional<std::string> &file_name)
    {
        m_file_name = file_name;
    }

    void Animation::set_file_size(const std::optional<std::int64_t> &file_size)
    {
        m_file_size = file_size;
    }

    void Animation::set_file_unique_id(const std::string &file_unique_id)
    {
        m_file_unique_id = file_unique_id;
    }

    void Animation::set_height(const std::int32_t &height)
    {
        m_height = height;
    }

    void Animation::set_mime_type(const std::optional<std::string> &mime_type)
    {
        m_mime_type = mime_type;
    }

    void Animation::set_thumbnail(const std::optional<PhotoSize::ptr> &thumbnail)
    {
        m_thumbnail = thumbnail;
    }

    void Animation::set_width(const std::int32_t &width)
    {
        m_width = width;
    }
} //namespace tgbot
