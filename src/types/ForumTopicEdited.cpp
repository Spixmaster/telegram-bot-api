#include "tgbot/types/ForumTopicEdited.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    ForumTopicEdited::ForumTopicEdited() = default;

    ForumTopicEdited::ForumTopicEdited(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("icon_custom_emoji_id"))
            {
                if(doc["icon_custom_emoji_id"].is_string())
                {
                    m_icon_custom_emoji_id = doc["icon_custom_emoji_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("icon_custom_emoji_id"));
                }
            }

            if(doc.contains("name"))
            {
                if(doc["name"].is_string())
                {
                    m_name = doc["name"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("name"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string ForumTopicEdited::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_icon_custom_emoji_id.has_value())
        {
            doc["icon_custom_emoji_id"] = m_icon_custom_emoji_id.value();
        }

        if(m_name.has_value())
        {
            doc["name"] = m_name.value();
        }

        return doc.dump();
    }

    std::optional<std::string> ForumTopicEdited::get_icon_custom_emoji_id() const
    {
        return m_icon_custom_emoji_id;
    }

    std::optional<std::string> ForumTopicEdited::get_name() const
    {
        return m_name;
    }

    void ForumTopicEdited::set_icon_custom_emoji_id(const std::optional<std::string> &icon_custom_emoji_id)
    {
        m_icon_custom_emoji_id = icon_custom_emoji_id;
    }

    void ForumTopicEdited::set_name(const std::optional<std::string> &name)
    {
        m_name = name;
    }
} //namespace tgbot
