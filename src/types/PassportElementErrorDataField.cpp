#include "tgbot/types/PassportElementErrorDataField.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    PassportElementErrorDataField::PassportElementErrorDataField() = default;

    PassportElementErrorDataField::PassportElementErrorDataField(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("data_hash"))
            {
                if(doc["data_hash"].is_string())
                {
                    m_data_hash = doc["data_hash"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("data_hash"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("data_hash"));
            }

            if(doc.contains("field_name"))
            {
                if(doc["field_name"].is_string())
                {
                    m_field_name = doc["field_name"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("field_name"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("field_name"));
            }

            if(doc.contains("message"))
            {
                if(doc["message"].is_string())
                {
                    m_message = doc["message"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("message"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("message"));
            }

            if(doc.contains("source"))
            {
                if(doc["source"].is_string())
                {
                    m_source = doc["source"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("source"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("source"));
            }

            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string PassportElementErrorDataField::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["data_hash"] = m_data_hash;
        doc["field_name"] = m_field_name;
        doc["message"] = m_message;
        doc["source"] = m_source;
        doc["type"] = m_type;
        return doc.dump();
    }

    std::string PassportElementErrorDataField::get_data_hash() const
    {
        return m_data_hash;
    }

    std::string PassportElementErrorDataField::get_field_name() const
    {
        return m_field_name;
    }

    std::string PassportElementErrorDataField::get_message() const
    {
        return m_message;
    }

    std::string PassportElementErrorDataField::get_source() const
    {
        return m_source;
    }

    std::string PassportElementErrorDataField::get_type() const
    {
        return m_type;
    }

    void PassportElementErrorDataField::set_data_hash(const std::string &data_hash)
    {
        m_data_hash = data_hash;
    }

    void PassportElementErrorDataField::set_field_name(const std::string &field_name)
    {
        m_field_name = field_name;
    }

    void PassportElementErrorDataField::set_message(const std::string &message)
    {
        m_message = message;
    }

    void PassportElementErrorDataField::set_source(const std::string &source)
    {
        m_source = source;
    }

    void PassportElementErrorDataField::set_type(const std::string &type)
    {
        m_type = type;
    }
} //namespace tgbot
