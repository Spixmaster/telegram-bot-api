#include "tgbot/types/ShippingAddress.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    ShippingAddress::ShippingAddress() = default;

    ShippingAddress::ShippingAddress(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("city"))
            {
                if(doc["city"].is_string())
                {
                    m_city = doc["city"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("city"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("city"));
            }

            if(doc.contains("country_code"))
            {
                if(doc["country_code"].is_string())
                {
                    m_country_code = doc["country_code"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("country_code"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("country_code"));
            }

            if(doc.contains("post_code"))
            {
                if(doc["post_code"].is_string())
                {
                    m_post_code = doc["post_code"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("post_code"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("post_code"));
            }

            if(doc.contains("state"))
            {
                if(doc["state"].is_string())
                {
                    m_state = doc["state"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("state"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("state"));
            }

            if(doc.contains("street_line1"))
            {
                if(doc["street_line1"].is_string())
                {
                    m_street_line1 = doc["street_line1"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("street_line1"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("street_line1"));
            }

            if(doc.contains("street_line2"))
            {
                if(doc["street_line2"].is_string())
                {
                    m_street_line2 = doc["street_line2"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("street_line2"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("street_line2"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string ShippingAddress::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["city"] = m_city;
        doc["country_code"] = m_country_code;
        doc["post_code"] = m_post_code;
        doc["state"] = m_state;
        doc["street_line1"] = m_street_line1;
        doc["street_line2"] = m_street_line2;
        return doc.dump();
    }

    std::string ShippingAddress::get_city() const
    {
        return m_city;
    }

    std::string ShippingAddress::get_country_code() const
    {
        return m_country_code;
    }

    std::string ShippingAddress::get_post_code() const
    {
        return m_post_code;
    }

    std::string ShippingAddress::get_state() const
    {
        return m_state;
    }

    std::string ShippingAddress::get_street_line1() const
    {
        return m_street_line1;
    }

    std::string ShippingAddress::get_street_line2() const
    {
        return m_street_line2;
    }

    void ShippingAddress::set_city(const std::string &city)
    {
        m_city = city;
    }

    void ShippingAddress::set_country_code(const std::string &country_code)
    {
        m_country_code = country_code;
    }

    void ShippingAddress::set_post_code(const std::string &post_code)
    {
        m_post_code = post_code;
    }

    void ShippingAddress::set_state(const std::string &state)
    {
        m_state = state;
    }

    void ShippingAddress::set_street_line1(const std::string &street_line1)
    {
        m_street_line1 = street_line1;
    }

    void ShippingAddress::set_street_line2(const std::string &street_line2)
    {
        m_street_line2 = street_line2;
    }
} //namespace tgbot
