#include "tgbot/types/UserShared.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    UserShared::UserShared() : m_request_id(-1), m_user_id(-1)
    {}

    UserShared::UserShared(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("request_id"))
            {
                if(doc["request_id"].is_number_integer())
                {
                    m_request_id = doc["request_id"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("request_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("request_id"));
            }

            if(doc.contains("user_id"))
            {
                if(doc["user_id"].is_number_integer())
                {
                    m_user_id = doc["user_id"].get<std::int64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("user_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("user_id"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string UserShared::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["request_id"] = m_request_id;
        doc["user_id"] = m_user_id;
        return doc.dump();
    }

    std::int32_t UserShared::get_request_id() const
    {
        return m_request_id;
    }

    std::int64_t UserShared::get_user_id() const
    {
        return m_user_id;
    }

    void UserShared::set_request_id(const std::int32_t &request_id)
    {
        m_request_id = request_id;
    }

    void UserShared::set_user_id(const std::int64_t &user_id)
    {
        m_user_id = user_id;
    }
} //namespace tgbot
