#include "tgbot/types/KeyboardButton.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    KeyboardButton::KeyboardButton() = default;

    KeyboardButton::KeyboardButton(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("request_chat"))
            {
                if(doc["request_chat"].is_object())
                {
                    m_request_chat = std::make_shared<KeyboardButtonRequestChat>(doc["request_chat"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("request_chat"));
                }
            }

            if(doc.contains("request_contact"))
            {
                if(doc["request_contact"].is_boolean())
                {
                    m_request_contact = doc["request_contact"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("request_contact"));
                }
            }

            if(doc.contains("request_location"))
            {
                if(doc["request_location"].is_boolean())
                {
                    m_request_location = doc["request_location"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("request_location"));
                }
            }

            if(doc.contains("request_poll"))
            {
                if(doc["request_poll"].is_object())
                {
                    m_request_poll = std::make_shared<KeyboardButtonPollType>(
                      doc["reques"
                          "t_"
                          "pol"
                          "l"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("request_poll"));
                }
            }

            if(doc.contains("request_user"))
            {
                if(doc["request_user"].is_object())
                {
                    m_request_user = std::make_shared<KeyboardButtonRequestUser>(doc["request_user"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("request_user"));
                }
            }

            if(doc.contains("text"))
            {
                if(doc["text"].is_string())
                {
                    m_text = doc["text"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("text"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("text"));
            }

            if(doc.contains("web_app"))
            {
                if(doc["web_app"].is_object())
                {
                    m_web_app = std::make_shared<WebAppInfo>(doc["web_app"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("web_app"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string KeyboardButton::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_request_chat.has_value())
        {
            doc["request_chat"] = nlohmann::json::parse(m_request_chat.value()->serialise());
        }

        if(m_request_contact.has_value())
        {
            doc["request_contact"] = m_request_contact.value();
        }

        if(m_request_location.has_value())
        {
            doc["request_location"] = m_request_location.value();
        }

        if(m_request_poll.has_value())
        {
            doc["request_poll"] = nlohmann::json::parse(m_request_poll.value()->serialise());
        }

        if(m_request_user.has_value())
        {
            doc["request_user"] = nlohmann::json::parse(m_request_user.value()->serialise());
        }

        doc["text"] = m_text;

        if(m_web_app.has_value())
        {
            doc["web_app"] = nlohmann::json::parse(m_web_app.value()->serialise());
        }

        return doc.dump();
    }

    std::optional<KeyboardButtonRequestChat::ptr> KeyboardButton::get_request_chat() const
    {
        return m_request_chat;
    }

    std::optional<bool> KeyboardButton::get_request_contact() const
    {
        return m_request_contact;
    }

    std::optional<bool> KeyboardButton::get_request_location() const
    {
        return m_request_location;
    }

    std::optional<KeyboardButtonPollType::ptr> KeyboardButton::get_request_poll() const
    {
        return m_request_poll;
    }

    std::optional<KeyboardButtonRequestUser::ptr> KeyboardButton::get_request_user() const
    {
        return m_request_user;
    }

    std::string KeyboardButton::get_text() const
    {
        return m_text;
    }

    std::optional<WebAppInfo::ptr> KeyboardButton::get_web_app() const
    {
        return m_web_app;
    }

    void KeyboardButton::set_request_chat(const std::optional<KeyboardButtonRequestChat::ptr> &request_chat)
    {
        m_request_chat = request_chat;
    }

    void KeyboardButton::set_request_contact(const std::optional<bool> &request_contact)
    {
        m_request_contact = request_contact;
    }

    void KeyboardButton::set_request_location(const std::optional<bool> &request_location)
    {
        m_request_location = request_location;
    }

    void KeyboardButton::set_request_poll(const std::optional<KeyboardButtonPollType::ptr> &request_poll)
    {
        m_request_poll = request_poll;
    }

    void KeyboardButton::set_request_user(const std::optional<KeyboardButtonRequestUser::ptr> &request_user)
    {
        m_request_user = request_user;
    }

    void KeyboardButton::set_text(const std::string &text)
    {
        m_text = text;
    }

    void KeyboardButton::set_web_app(const std::optional<WebAppInfo::ptr> &web_app)
    {
        m_web_app = web_app;
    }
} //namespace tgbot
