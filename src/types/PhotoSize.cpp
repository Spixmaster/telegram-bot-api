#include "tgbot/types/PhotoSize.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    PhotoSize::PhotoSize() : m_height(-1), m_width(-1)
    {}

    PhotoSize::PhotoSize(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("file_id"))
            {
                if(doc["file_id"].is_string())
                {
                    m_file_id = doc["file_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("file_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("file_id"));
            }

            if(doc.contains("file_size"))
            {
                if(doc["file_size"].is_number_integer())
                {
                    m_file_size = doc["file_size"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("file_size"));
                }
            }

            if(doc.contains("file_unique_id"))
            {
                if(doc["file_unique_id"].is_string())
                {
                    m_file_unique_id = doc["file_unique_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("file_unique_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("file_unique_id"));
            }

            if(doc.contains("height"))
            {
                if(doc["height"].is_number_integer())
                {
                    m_height = doc["height"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("height"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("height"));
            }

            if(doc.contains("width"))
            {
                if(doc["width"].is_number_integer())
                {
                    m_width = doc["width"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("width"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("width"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string PhotoSize::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["file_id"] = m_file_id;

        if(m_file_size.has_value())
        {
            doc["file_size"] = m_file_size.value();
        }

        doc["file_unique_id"] = m_file_unique_id;
        doc["height"] = m_height;
        doc["width"] = m_width;
        return doc.dump();
    }

    std::string PhotoSize::get_file_id() const
    {
        return m_file_id;
    }

    std::optional<std::int32_t> PhotoSize::get_file_size() const
    {
        return m_file_size;
    }

    std::string PhotoSize::get_file_unique_id() const
    {
        return m_file_unique_id;
    }

    std::int32_t PhotoSize::get_height() const
    {
        return m_height;
    }

    std::int32_t PhotoSize::get_width() const
    {
        return m_width;
    }

    void PhotoSize::set_file_id(const std::string &file_id)
    {
        m_file_id = file_id;
    }

    void PhotoSize::set_file_size(const std::optional<std::int32_t> &file_size)
    {
        m_file_size = file_size;
    }

    void PhotoSize::set_file_unique_id(const std::string &file_unique_id)
    {
        m_file_unique_id = file_unique_id;
    }

    void PhotoSize::set_height(const std::int32_t &height)
    {
        m_height = height;
    }

    void PhotoSize::set_width(const std::int32_t &width)
    {
        m_width = width;
    }
} //namespace tgbot
