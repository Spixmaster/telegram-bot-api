#include "tgbot/types/KeyboardButtonRequestUser.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    KeyboardButtonRequestUser::KeyboardButtonRequestUser() : m_request_id(-1)
    {}

    KeyboardButtonRequestUser::KeyboardButtonRequestUser(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("request_id"))
            {
                if(doc["request_id"].is_number_integer())
                {
                    m_request_id = doc["request_id"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("request_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("request_id"));
            }

            if(doc.contains("user_is_bot"))
            {
                if(doc["user_is_bot"].is_boolean())
                {
                    m_user_is_bot = doc["user_is_bot"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("user_is_bot"));
                }
            }

            if(doc.contains("user_is_premium"))
            {
                if(doc["user_is_premium"].is_boolean())
                {
                    m_user_is_premium = doc["user_is_premium"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("user_is_premium"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string KeyboardButtonRequestUser::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["request_id"] = m_request_id;

        if(m_user_is_bot.has_value())
        {
            doc["user_is_bot"] = m_user_is_bot.value();
        }

        if(m_user_is_premium.has_value())
        {
            doc["user_is_premium"] = m_user_is_premium.value();
        }

        return doc.dump();
    }

    std::int32_t KeyboardButtonRequestUser::get_request_id() const
    {
        return m_request_id;
    }

    std::optional<bool> KeyboardButtonRequestUser::get_user_is_bot() const
    {
        return m_user_is_bot;
    }

    std::optional<bool> KeyboardButtonRequestUser::get_user_is_premium() const
    {
        return m_user_is_premium;
    }

    void KeyboardButtonRequestUser::set_request_id(const std::int32_t &request_id)
    {
        m_request_id = request_id;
    }

    void KeyboardButtonRequestUser::set_user_is_bot(const std::optional<bool> &user_is_bot)
    {
        m_user_is_bot = user_is_bot;
    }

    void KeyboardButtonRequestUser::set_user_is_premium(const std::optional<bool> &user_is_premium)
    {
        m_user_is_premium = user_is_premium;
    }
} //namespace tgbot
