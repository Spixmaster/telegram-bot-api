#include "tgbot/types/ShippingOption.h"

#include <algorithm>
#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    ShippingOption::ShippingOption() = default;

    ShippingOption::ShippingOption(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("id"))
            {
                if(doc["id"].is_string())
                {
                    m_id = doc["id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("id"));
            }

            if(doc.contains("prices"))
            {
                if(doc["prices"].is_array())
                {
                    std::vector<LabeledPrice::ptr> prices;
                    prices.reserve(doc["prices"].size());

                    for(std::uint64_t i = 0; i < doc["prices"].size(); ++i)
                    {
                        if(doc["prices"].at(i).is_object())
                        {
                            prices.emplace_back(std::make_shared<LabeledPrice>(doc["prices"].at(i).dump()));
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_object("prices"));
                        }
                    }

                    m_prices = prices;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("prices"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("prices"));
            }

            if(doc.contains("title"))
            {
                if(doc["title"].is_string())
                {
                    m_title = doc["title"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("title"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("title"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string ShippingOption::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["id"] = m_id;

        {
            nlohmann::json prices = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_prices.size(); ++i)
            {
                prices.emplace_back(nlohmann::json::parse(m_prices.at(i)->serialise()));
            }

            doc["prices"] = prices;
        }

        doc["title"] = m_title;
        return doc.dump();
    }

    std::string ShippingOption::get_id() const
    {
        return m_id;
    }

    std::vector<LabeledPrice::ptr> ShippingOption::get_prices() const
    {
        return m_prices;
    }

    std::string ShippingOption::get_title() const
    {
        return m_title;
    }

    void ShippingOption::set_id(const std::string &id)
    {
        m_id = id;
    }

    void ShippingOption::set_prices(const std::vector<LabeledPrice::ptr> &prices)
    {
        m_prices = prices;
    }

    void ShippingOption::set_title(const std::string &title)
    {
        m_title = title;
    }
} //namespace tgbot
