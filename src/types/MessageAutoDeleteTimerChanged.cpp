#include "tgbot/types/MessageAutoDeleteTimerChanged.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    MessageAutoDeleteTimerChanged::MessageAutoDeleteTimerChanged() : m_message_auto_delete_time(-1)
    {}

    MessageAutoDeleteTimerChanged::MessageAutoDeleteTimerChanged(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("message_auto_delete_time"))
            {
                if(doc["message_auto_delete_time"].is_number_integer())
                {
                    m_message_auto_delete_time = doc["message_auto_delete_time"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("message_auto_delete_time"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("message_auto_delete_time"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string MessageAutoDeleteTimerChanged::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["message_auto_delete_time"] = m_message_auto_delete_time;
        return doc.dump();
    }

    std::int32_t MessageAutoDeleteTimerChanged::get_message_auto_delete_time() const
    {
        return m_message_auto_delete_time;
    }

    void MessageAutoDeleteTimerChanged::set_message_auto_delete_time(const std::int32_t &message_auto_delete_time)
    {
        m_message_auto_delete_time = message_auto_delete_time;
    }
} //namespace tgbot
