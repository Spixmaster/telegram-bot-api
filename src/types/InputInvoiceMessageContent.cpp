#include "tgbot/types/InputInvoiceMessageContent.h"

#include <algorithm>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "tgbot/constants/message.h"

namespace tgbot
{
    InputInvoiceMessageContent::InputInvoiceMessageContent() = default;

    InputInvoiceMessageContent::InputInvoiceMessageContent(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("currency"))
            {
                if(doc["currency"].is_string())
                {
                    m_currency = doc["currency"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("currency"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("currency"));
            }

            if(doc.contains("description"))
            {
                if(doc["description"].is_string())
                {
                    m_description = doc["description"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("description"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("description"));
            }

            if(doc.contains("is_flexible"))
            {
                if(doc["is_flexible"].is_boolean())
                {
                    m_is_flexible = doc["is_flexible"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("is_flexible"));
                }
            }

            if(doc.contains("max_tip_amount"))
            {
                if(doc["max_tip_amount"].is_number_integer())
                {
                    m_max_tip_amount = doc["max_tip_amount"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("max_tip_amount"));
                }
            }

            if(doc.contains("need_email"))
            {
                if(doc["need_email"].is_boolean())
                {
                    m_need_email = doc["need_email"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("need_email"));
                }
            }

            if(doc.contains("need_name"))
            {
                if(doc["need_name"].is_boolean())
                {
                    m_need_name = doc["need_name"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("need_name"));
                }
            }

            if(doc.contains("need_phone_number"))
            {
                if(doc["need_phone_number"].is_boolean())
                {
                    m_need_phone_number = doc["need_phone_number"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("need_phone_number"));
                }
            }

            if(doc.contains("need_shipping_address"))
            {
                if(doc["need_shipping_address"].is_boolean())
                {
                    m_need_shipping_address = doc["need_shipping_address"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("need_shipping_address"));
                }
            }

            if(doc.contains("payload"))
            {
                if(doc["payload"].is_string())
                {
                    m_payload = doc["payload"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("payload"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("payload"));
            }

            if(doc.contains("photo_height"))
            {
                if(doc["photo_height"].is_number_integer())
                {
                    m_photo_height = doc["photo_height"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("photo_height"));
                }
            }

            if(doc.contains("photo_size"))
            {
                if(doc["photo_size"].is_number_integer())
                {
                    m_photo_size = doc["photo_size"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("photo_size"));
                }
            }

            if(doc.contains("photo_url"))
            {
                if(doc["photo_url"].is_string())
                {
                    m_photo_url = doc["photo_url"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("photo_url"));
                }
            }

            if(doc.contains("photo_width"))
            {
                if(doc["photo_width"].is_number_integer())
                {
                    m_photo_width = doc["photo_width"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("photo_width"));
                }
            }

            if(doc.contains("prices"))
            {
                if(doc["prices"].is_array())
                {
                    std::vector<LabeledPrice::ptr> prices;
                    prices.reserve(doc["prices"].size());

                    for(std::uint64_t i = 0; i < doc["prices"].size(); ++i)
                    {
                        if(doc["prices"].at(i).is_object())
                        {
                            prices.emplace_back(std::make_shared<LabeledPrice>(doc["prices"].at(i).dump()));
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_object("prices"));
                        }
                    }

                    m_prices = prices;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("prices"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("prices"));
            }

            if(doc.contains("provider_data"))
            {
                if(doc["provider_data"].is_string())
                {
                    m_provider_data = doc["provider_data"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("provider_data"));
                }
            }

            if(doc.contains("provider_token"))
            {
                if(doc["provider_token"].is_string())
                {
                    m_provider_token = doc["provider_token"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("provider_token"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("provider_token"));
            }

            if(doc.contains("send_email_to_provider"))
            {
                if(doc["send_email_to_provider"].is_boolean())
                {
                    m_send_email_to_provider = doc["send_email_to_provider"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("send_email_to_provider"));
                }
            }

            if(doc.contains("send_phone_number_to_provider"))
            {
                if(doc["send_phone_number_to_provider"].is_boolean())
                {
                    m_send_phone_number_to_provider = doc["send_phone_number_to_provider"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("send_phone_number_to_provider"));
                }
            }

            if(doc.contains("suggested_tip_amounts"))
            {
                if(doc["suggested_tip_amounts"].is_array())
                {
                    std::vector<std::int32_t> suggested_tip_amounts;
                    suggested_tip_amounts.reserve(doc["suggested_tip_amounts"].size());

                    for(std::uint64_t i = 0; i < doc["suggested_tip_amounts"].size(); ++i)
                    {
                        if(doc["suggested_tip_amounts"].at(i).is_number_integer())
                        {
                            suggested_tip_amounts.emplace_back(doc["suggested_tip_amounts"].at(i).get<std::int32_t>());
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_int(
                              "suggested_tip_"
                              "amounts"));
                        }
                    }

                    m_suggested_tip_amounts = suggested_tip_amounts;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("suggested_tip_amounts"));
                }
            }

            if(doc.contains("title"))
            {
                if(doc["title"].is_string())
                {
                    m_title = doc["title"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("title"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("title"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string InputInvoiceMessageContent::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["currency"] = m_currency;
        doc["description"] = m_description;

        if(m_is_flexible.has_value())
        {
            doc["is_flexible"] = m_is_flexible.value();
        }

        if(m_max_tip_amount.has_value())
        {
            doc["max_tip_amount"] = m_max_tip_amount.value();
        }

        if(m_need_email.has_value())
        {
            doc["need_email"] = m_need_email.value();
        }

        if(m_need_name.has_value())
        {
            doc["need_name"] = m_need_name.value();
        }

        if(m_need_phone_number.has_value())
        {
            doc["need_phone_number"] = m_need_phone_number.value();
        }

        if(m_need_shipping_address.has_value())
        {
            doc["need_shipping_address"] = m_need_shipping_address.value();
        }

        doc["payload"] = m_payload;

        if(m_photo_height.has_value())
        {
            doc["photo_height"] = m_photo_height.value();
        }

        if(m_photo_size.has_value())
        {
            doc["photo_size"] = m_photo_size.value();
        }

        if(m_photo_url.has_value())
        {
            doc["photo_url"] = m_photo_url.value();
        }

        if(m_photo_width.has_value())
        {
            doc["photo_width"] = m_photo_width.value();
        }

        {
            nlohmann::json prices = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_prices.size(); ++i)
            {
                prices.emplace_back(nlohmann::json::parse(m_prices.at(i)->serialise()));
            }

            doc["prices"] = prices;
        }

        if(m_provider_data.has_value())
        {
            doc["provider_data"] = m_provider_data.value();
        }

        doc["provider_token"] = m_provider_token;

        if(m_send_email_to_provider.has_value())
        {
            doc["send_email_to_provider"] = m_send_email_to_provider.value();
        }

        if(m_send_phone_number_to_provider.has_value())
        {
            doc["send_phone_number_to_provider"] = m_send_phone_number_to_provider.value();
        }

        if(m_suggested_tip_amounts.has_value())
        {
            nlohmann::json suggested_tip_amounts = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_suggested_tip_amounts.value().size(); ++i)
            {
                suggested_tip_amounts.emplace_back(m_suggested_tip_amounts.value().at(i));
            }

            doc["suggested_tip_amounts"] = suggested_tip_amounts;
        }

        doc["title"] = m_title;
        return doc.dump();
    }

    std::string InputInvoiceMessageContent::get_currency() const
    {
        return m_currency;
    }

    std::string InputInvoiceMessageContent::get_description() const
    {
        return m_description;
    }

    std::optional<bool> InputInvoiceMessageContent::get_is_flexible() const
    {
        return m_is_flexible;
    }

    std::optional<std::int32_t> InputInvoiceMessageContent::get_max_tip_amount() const
    {
        return m_max_tip_amount;
    }

    std::optional<bool> InputInvoiceMessageContent::get_need_email() const
    {
        return m_need_email;
    }

    std::optional<bool> InputInvoiceMessageContent::get_need_name() const
    {
        return m_need_name;
    }

    std::optional<bool> InputInvoiceMessageContent::get_need_phone_number() const
    {
        return m_need_phone_number;
    }

    std::optional<bool> InputInvoiceMessageContent::get_need_shipping_address() const
    {
        return m_need_shipping_address;
    }

    std::string InputInvoiceMessageContent::get_payload() const
    {
        return m_payload;
    }

    std::optional<std::int32_t> InputInvoiceMessageContent::get_photo_height() const
    {
        return m_photo_height;
    }

    std::optional<std::int32_t> InputInvoiceMessageContent::get_photo_size() const
    {
        return m_photo_size;
    }

    std::optional<std::string> InputInvoiceMessageContent::get_photo_url() const
    {
        return m_photo_url;
    }

    std::optional<std::int32_t> InputInvoiceMessageContent::get_photo_width() const
    {
        return m_photo_width;
    }

    std::vector<LabeledPrice::ptr> InputInvoiceMessageContent::get_prices() const
    {
        return m_prices;
    }

    std::optional<std::string> InputInvoiceMessageContent::get_provider_data() const
    {
        return m_provider_data;
    }

    std::string InputInvoiceMessageContent::get_provider_token() const
    {
        return m_provider_token;
    }

    std::optional<bool> InputInvoiceMessageContent::get_send_email_to_provider() const
    {
        return m_send_email_to_provider;
    }

    std::optional<bool> InputInvoiceMessageContent::get_send_phone_number_to_provider() const
    {
        return m_send_phone_number_to_provider;
    }

    std::optional<std::vector<std::int32_t>> InputInvoiceMessageContent::get_suggested_tip_amounts() const
    {
        return m_suggested_tip_amounts;
    }

    std::string InputInvoiceMessageContent::get_title() const
    {
        return m_title;
    }

    void InputInvoiceMessageContent::set_currency(const std::string &currency)
    {
        m_currency = currency;
    }

    void InputInvoiceMessageContent::set_description(const std::string &description)
    {
        m_description = description;
    }

    void InputInvoiceMessageContent::set_is_flexible(const std::optional<bool> &is_flexible)
    {
        m_is_flexible = is_flexible;
    }

    void InputInvoiceMessageContent::set_max_tip_amount(const std::optional<std::int32_t> &max_tip_amount)
    {
        m_max_tip_amount = max_tip_amount;
    }

    void InputInvoiceMessageContent::set_need_email(const std::optional<bool> &need_email)
    {
        m_need_email = need_email;
    }

    void InputInvoiceMessageContent::set_need_name(const std::optional<bool> &need_name)
    {
        m_need_name = need_name;
    }

    void InputInvoiceMessageContent::set_need_phone_number(const std::optional<bool> &need_phone_number)
    {
        m_need_phone_number = need_phone_number;
    }

    void InputInvoiceMessageContent::set_need_shipping_address(const std::optional<bool> &need_shipping_address)
    {
        m_need_shipping_address = need_shipping_address;
    }

    void InputInvoiceMessageContent::set_payload(const std::string &payload)
    {
        m_payload = payload;
    }

    void InputInvoiceMessageContent::set_photo_height(const std::optional<std::int32_t> &photo_height)
    {
        m_photo_height = photo_height;
    }

    void InputInvoiceMessageContent::set_photo_size(const std::optional<std::int32_t> &photo_size)
    {
        m_photo_size = photo_size;
    }

    void InputInvoiceMessageContent::set_photo_url(const std::optional<std::string> &photo_url)
    {
        m_photo_url = photo_url;
    }

    void InputInvoiceMessageContent::set_photo_width(const std::optional<std::int32_t> &photo_width)
    {
        m_photo_width = photo_width;
    }

    void InputInvoiceMessageContent::set_prices(const std::vector<LabeledPrice::ptr> &prices)
    {
        m_prices = prices;
    }

    void InputInvoiceMessageContent::set_provider_data(const std::optional<std::string> &provider_data)
    {
        m_provider_data = provider_data;
    }

    void InputInvoiceMessageContent::set_provider_token(const std::string &provider_token)
    {
        m_provider_token = provider_token;
    }

    void InputInvoiceMessageContent::set_send_email_to_provider(const std::optional<bool> &send_email_to_provider)
    {
        m_send_email_to_provider = send_email_to_provider;
    }

    void InputInvoiceMessageContent::set_send_phone_number_to_provider(
      const std::optional<bool> &send_phone_number_to_provider)
    {
        m_send_phone_number_to_provider = send_phone_number_to_provider;
    }

    void InputInvoiceMessageContent::set_suggested_tip_amounts(
      const std::optional<std::vector<std::int32_t>> &suggested_tip_amounts)
    {
        m_suggested_tip_amounts = suggested_tip_amounts;
    }

    void InputInvoiceMessageContent::set_title(const std::string &title)
    {
        m_title = title;
    }
} //namespace tgbot
