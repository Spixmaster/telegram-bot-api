#include "tgbot/types/BotCommandScopeChatMember.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    BotCommandScopeChatMember::BotCommandScopeChatMember() : m_user_id(-1)
    {}

    BotCommandScopeChatMember::BotCommandScopeChatMember(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("chat_id"))
            {
                if(doc["chat_id"].is_number_integer())
                {
                    m_chat_id = doc["chat_id"].get<std::int64_t>();
                }
                else if(doc["chat_id"].is_string())
                {
                    m_chat_id = doc["chat_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(
                      message::json::value::not_int("chat_id") + ", " + message::json::value::not_string("chat_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("chat_id"));
            }

            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }

            if(doc.contains("user_id"))
            {
                if(doc["user_id"].is_number_integer())
                {
                    m_user_id = doc["user_id"].get<std::int64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("user_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("user_id"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string BotCommandScopeChatMember::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(std::holds_alternative<std::int64_t>(m_chat_id))
        {
            doc["chat_id"] = std::get<std::int64_t>(m_chat_id);
        }
        else if(std::holds_alternative<std::string>(m_chat_id))
        {
            doc["chat_id"] = std::get<std::string>(m_chat_id);
        }

        doc["type"] = m_type;
        doc["user_id"] = m_user_id;
        return doc.dump();
    }

    std::variant<std::int64_t, std::string> BotCommandScopeChatMember::get_chat_id() const
    {
        return m_chat_id;
    }

    std::string BotCommandScopeChatMember::get_type() const
    {
        return m_type;
    }

    std::int64_t BotCommandScopeChatMember::get_user_id() const
    {
        return m_user_id;
    }

    void BotCommandScopeChatMember::set_chat_id(const std::variant<std::int64_t, std::string> &chat_id)
    {
        m_chat_id = chat_id;
    }

    void BotCommandScopeChatMember::set_type(const std::string &type)
    {
        m_type = type;
    }

    void BotCommandScopeChatMember::set_user_id(const std::int64_t &user_id)
    {
        m_user_id = user_id;
    }
} //namespace tgbot
