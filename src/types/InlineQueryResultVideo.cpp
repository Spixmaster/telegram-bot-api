#include "tgbot/types/InlineQueryResultVideo.h"

#include <algorithm>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "tgbot/constants/message.h"

namespace tgbot
{
    InlineQueryResultVideo::InlineQueryResultVideo() = default;

    InlineQueryResultVideo::InlineQueryResultVideo(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("caption"))
            {
                if(doc["caption"].is_string())
                {
                    m_caption = doc["caption"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("caption"));
                }
            }

            if(doc.contains("caption_entities"))
            {
                if(doc["caption_entities"].is_array())
                {
                    std::vector<MessageEntity::ptr> caption_entities;
                    caption_entities.reserve(doc["caption_entities"].size());

                    for(std::uint64_t i = 0; i < doc["caption_entities"].size(); ++i)
                    {
                        if(doc["caption_entities"].at(i).is_object())
                        {
                            caption_entities.emplace_back(
                              std::make_shared<MessageEntity>(doc["caption_entities"].at(i).dump()));
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_object(
                              "caption_"
                              "entitie"
                              "s"));
                        }
                    }

                    m_caption_entities = caption_entities;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("caption_entities"));
                }
            }

            if(doc.contains("description"))
            {
                if(doc["description"].is_string())
                {
                    m_description = doc["description"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("description"));
                }
            }

            if(doc.contains("id"))
            {
                if(doc["id"].is_string())
                {
                    m_id = doc["id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("id"));
            }

            if(doc.contains("input_message_content"))
            {
                if(doc["input_message_content"].is_object())
                {
                    m_input_message_content = std::make_shared<InputMessageContent>(doc["input_message_content"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("input_message_content"));
                }
            }

            if(doc.contains("mime_type"))
            {
                if(doc["mime_type"].is_string())
                {
                    m_mime_type = doc["mime_type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("mime_type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("mime_type"));
            }

            if(doc.contains("parse_mode"))
            {
                if(doc["parse_mode"].is_string())
                {
                    m_parse_mode = doc["parse_mode"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("parse_mode"));
                }
            }

            if(doc.contains("reply_markup"))
            {
                if(doc["reply_markup"].is_object())
                {
                    m_reply_markup = std::make_shared<InlineKeyboardMarkup>(
                      doc["reply_"
                          "marku"
                          "p"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("reply_markup"));
                }
            }

            if(doc.contains("thumbnail_url"))
            {
                if(doc["thumbnail_url"].is_string())
                {
                    m_thumbnail_url = doc["thumbnail_url"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("thumbnail_url"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("thumbnail_url"));
            }

            if(doc.contains("title"))
            {
                if(doc["title"].is_string())
                {
                    m_title = doc["title"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("title"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("title"));
            }

            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }

            if(doc.contains("video_duration"))
            {
                if(doc["video_duration"].is_number_integer())
                {
                    m_video_duration = doc["video_duration"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("video_duration"));
                }
            }

            if(doc.contains("video_height"))
            {
                if(doc["video_height"].is_number_integer())
                {
                    m_video_height = doc["video_height"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("video_height"));
                }
            }

            if(doc.contains("video_url"))
            {
                if(doc["video_url"].is_string())
                {
                    m_video_url = doc["video_url"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("video_url"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("video_url"));
            }

            if(doc.contains("video_width"))
            {
                if(doc["video_width"].is_number_integer())
                {
                    m_video_width = doc["video_width"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("video_width"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string InlineQueryResultVideo::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_caption.has_value())
        {
            doc["caption"] = m_caption.value();
        }

        if(m_caption_entities.has_value())
        {
            nlohmann::json caption_entities = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_caption_entities.value().size(); ++i)
            {
                caption_entities.emplace_back(nlohmann::json::parse(m_caption_entities.value().at(i)->serialise()));
            }

            doc["caption_entities"] = caption_entities;
        }

        if(m_description.has_value())
        {
            doc["description"] = m_description.value();
        }

        doc["id"] = m_id;

        if(m_input_message_content.has_value())
        {
            doc["input_message_content"] = nlohmann::json::parse(m_input_message_content.value()->serialise());
        }

        doc["mime_type"] = m_mime_type;

        if(m_parse_mode.has_value())
        {
            doc["parse_mode"] = m_parse_mode.value();
        }

        if(m_reply_markup.has_value())
        {
            doc["reply_markup"] = nlohmann::json::parse(m_reply_markup.value()->serialise());
        }

        doc["thumbnail_url"] = m_thumbnail_url;
        doc["title"] = m_title;
        doc["type"] = m_type;

        if(m_video_duration.has_value())
        {
            doc["video_duration"] = m_video_duration.value();
        }

        if(m_video_height.has_value())
        {
            doc["video_height"] = m_video_height.value();
        }

        doc["video_url"] = m_video_url;

        if(m_video_width.has_value())
        {
            doc["video_width"] = m_video_width.value();
        }

        return doc.dump();
    }

    std::optional<std::string> InlineQueryResultVideo::get_caption() const
    {
        return m_caption;
    }

    std::optional<std::vector<MessageEntity::ptr>> InlineQueryResultVideo::get_caption_entities() const
    {
        return m_caption_entities;
    }

    std::optional<std::string> InlineQueryResultVideo::get_description() const
    {
        return m_description;
    }

    std::string InlineQueryResultVideo::get_id() const
    {
        return m_id;
    }

    std::optional<InputMessageContent::ptr> InlineQueryResultVideo::get_input_message_content() const
    {
        return m_input_message_content;
    }

    std::string InlineQueryResultVideo::get_mime_type() const
    {
        return m_mime_type;
    }

    std::optional<std::string> InlineQueryResultVideo::get_parse_mode() const
    {
        return m_parse_mode;
    }

    std::optional<InlineKeyboardMarkup::ptr> InlineQueryResultVideo::get_reply_markup() const
    {
        return m_reply_markup;
    }

    std::string InlineQueryResultVideo::get_thumbnail_url() const
    {
        return m_thumbnail_url;
    }

    std::string InlineQueryResultVideo::get_title() const
    {
        return m_title;
    }

    std::string InlineQueryResultVideo::get_type() const
    {
        return m_type;
    }

    std::optional<std::int32_t> InlineQueryResultVideo::get_video_duration() const
    {
        return m_video_duration;
    }

    std::optional<std::int32_t> InlineQueryResultVideo::get_video_height() const
    {
        return m_video_height;
    }

    std::string InlineQueryResultVideo::get_video_url() const
    {
        return m_video_url;
    }

    std::optional<std::int32_t> InlineQueryResultVideo::get_video_width() const
    {
        return m_video_width;
    }

    void InlineQueryResultVideo::set_caption(const std::optional<std::string> &caption)
    {
        m_caption = caption;
    }

    void InlineQueryResultVideo::set_caption_entities(
      const std::optional<std::vector<MessageEntity::ptr>> &caption_entities)
    {
        m_caption_entities = caption_entities;
    }

    void InlineQueryResultVideo::set_description(const std::optional<std::string> &description)
    {
        m_description = description;
    }

    void InlineQueryResultVideo::set_id(const std::string &id)
    {
        m_id = id;
    }

    void InlineQueryResultVideo::set_input_message_content(
      const std::optional<InputMessageContent::ptr> &input_message_content)
    {
        m_input_message_content = input_message_content;
    }

    void InlineQueryResultVideo::set_mime_type(const std::string &mime_type)
    {
        m_mime_type = mime_type;
    }

    void InlineQueryResultVideo::set_parse_mode(const std::optional<std::string> &parse_mode)
    {
        m_parse_mode = parse_mode;
    }

    void InlineQueryResultVideo::set_reply_markup(const std::optional<InlineKeyboardMarkup::ptr> &reply_markup)
    {
        m_reply_markup = reply_markup;
    }

    void InlineQueryResultVideo::set_thumbnail_url(const std::string &thumbnail_url)
    {
        m_thumbnail_url = thumbnail_url;
    }

    void InlineQueryResultVideo::set_title(const std::string &title)
    {
        m_title = title;
    }

    void InlineQueryResultVideo::set_type(const std::string &type)
    {
        m_type = type;
    }

    void InlineQueryResultVideo::set_video_duration(const std::optional<std::int32_t> &video_duration)
    {
        m_video_duration = video_duration;
    }

    void InlineQueryResultVideo::set_video_height(const std::optional<std::int32_t> &video_height)
    {
        m_video_height = video_height;
    }

    void InlineQueryResultVideo::set_video_url(const std::string &video_url)
    {
        m_video_url = video_url;
    }

    void InlineQueryResultVideo::set_video_width(const std::optional<std::int32_t> &video_width)
    {
        m_video_width = video_width;
    }
} //namespace tgbot
