#include "tgbot/types/BotCommandScopeChatAdministrators.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    BotCommandScopeChatAdministrators::BotCommandScopeChatAdministrators() = default;

    BotCommandScopeChatAdministrators::BotCommandScopeChatAdministrators(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("chat_id"))
            {
                if(doc["chat_id"].is_number_integer())
                {
                    m_chat_id = doc["chat_id"].get<std::int64_t>();
                }
                else if(doc["chat_id"].is_string())
                {
                    m_chat_id = doc["chat_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(
                      message::json::value::not_int("chat_id") + ", " + message::json::value::not_string("chat_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("chat_id"));
            }

            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string BotCommandScopeChatAdministrators::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(std::holds_alternative<std::int64_t>(m_chat_id))
        {
            doc["chat_id"] = std::get<std::int64_t>(m_chat_id);
        }
        else if(std::holds_alternative<std::string>(m_chat_id))
        {
            doc["chat_id"] = std::get<std::string>(m_chat_id);
        }

        doc["type"] = m_type;
        return doc.dump();
    }

    std::variant<std::int64_t, std::string> BotCommandScopeChatAdministrators::get_chat_id() const
    {
        return m_chat_id;
    }

    std::string BotCommandScopeChatAdministrators::get_type() const
    {
        return m_type;
    }

    void BotCommandScopeChatAdministrators::set_chat_id(const std::variant<std::int64_t, std::string> &chat_id)
    {
        m_chat_id = chat_id;
    }

    void BotCommandScopeChatAdministrators::set_type(const std::string &type)
    {
        m_type = type;
    }
} //namespace tgbot
