#include "tgbot/types/ChatAdministratorRights.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    ChatAdministratorRights::ChatAdministratorRights() :
        m_can_change_info(false), m_can_delete_messages(false), m_can_invite_users(false), m_can_manage_chat(false),
        m_can_manage_video_chats(false), m_can_promote_members(false), m_can_restrict_members(false),
        m_is_anonymous(false)
    {}

    ChatAdministratorRights::ChatAdministratorRights(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("can_change_info"))
            {
                if(doc["can_change_info"].is_boolean())
                {
                    m_can_change_info = doc["can_change_info"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_change_info"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_change_info"));
            }

            if(doc.contains("can_delete_messages"))
            {
                if(doc["can_delete_messages"].is_boolean())
                {
                    m_can_delete_messages = doc["can_delete_messages"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_delete_messages"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_delete_messages"));
            }

            if(doc.contains("can_edit_messages"))
            {
                if(doc["can_edit_messages"].is_boolean())
                {
                    m_can_edit_messages = doc["can_edit_messages"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_edit_messages"));
                }
            }

            if(doc.contains("can_invite_users"))
            {
                if(doc["can_invite_users"].is_boolean())
                {
                    m_can_invite_users = doc["can_invite_users"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_invite_users"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_invite_users"));
            }

            if(doc.contains("can_manage_chat"))
            {
                if(doc["can_manage_chat"].is_boolean())
                {
                    m_can_manage_chat = doc["can_manage_chat"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_manage_chat"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_manage_chat"));
            }

            if(doc.contains("can_manage_topics"))
            {
                if(doc["can_manage_topics"].is_boolean())
                {
                    m_can_manage_topics = doc["can_manage_topics"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_manage_topics"));
                }
            }

            if(doc.contains("can_manage_video_chats"))
            {
                if(doc["can_manage_video_chats"].is_boolean())
                {
                    m_can_manage_video_chats = doc["can_manage_video_chats"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_manage_video_chats"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_manage_video_chats"));
            }

            if(doc.contains("can_pin_messages"))
            {
                if(doc["can_pin_messages"].is_boolean())
                {
                    m_can_pin_messages = doc["can_pin_messages"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_pin_messages"));
                }
            }

            if(doc.contains("can_post_messages"))
            {
                if(doc["can_post_messages"].is_boolean())
                {
                    m_can_post_messages = doc["can_post_messages"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_post_messages"));
                }
            }

            if(doc.contains("can_promote_members"))
            {
                if(doc["can_promote_members"].is_boolean())
                {
                    m_can_promote_members = doc["can_promote_members"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_promote_members"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_promote_members"));
            }

            if(doc.contains("can_restrict_members"))
            {
                if(doc["can_restrict_members"].is_boolean())
                {
                    m_can_restrict_members = doc["can_restrict_members"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("can_restrict_members"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("can_restrict_members"));
            }

            if(doc.contains("is_anonymous"))
            {
                if(doc["is_anonymous"].is_boolean())
                {
                    m_is_anonymous = doc["is_anonymous"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("is_anonymous"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("is_anonymous"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string ChatAdministratorRights::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["can_change_info"] = m_can_change_info;
        doc["can_delete_messages"] = m_can_delete_messages;

        if(m_can_edit_messages.has_value())
        {
            doc["can_edit_messages"] = m_can_edit_messages.value();
        }

        doc["can_invite_users"] = m_can_invite_users;
        doc["can_manage_chat"] = m_can_manage_chat;

        if(m_can_manage_topics.has_value())
        {
            doc["can_manage_topics"] = m_can_manage_topics.value();
        }

        doc["can_manage_video_chats"] = m_can_manage_video_chats;

        if(m_can_pin_messages.has_value())
        {
            doc["can_pin_messages"] = m_can_pin_messages.value();
        }

        if(m_can_post_messages.has_value())
        {
            doc["can_post_messages"] = m_can_post_messages.value();
        }

        doc["can_promote_members"] = m_can_promote_members;
        doc["can_restrict_members"] = m_can_restrict_members;
        doc["is_anonymous"] = m_is_anonymous;
        return doc.dump();
    }

    bool ChatAdministratorRights::get_can_change_info() const
    {
        return m_can_change_info;
    }

    bool ChatAdministratorRights::get_can_delete_messages() const
    {
        return m_can_delete_messages;
    }

    std::optional<bool> ChatAdministratorRights::get_can_edit_messages() const
    {
        return m_can_edit_messages;
    }

    bool ChatAdministratorRights::get_can_invite_users() const
    {
        return m_can_invite_users;
    }

    bool ChatAdministratorRights::get_can_manage_chat() const
    {
        return m_can_manage_chat;
    }

    std::optional<bool> ChatAdministratorRights::get_can_manage_topics() const
    {
        return m_can_manage_topics;
    }

    bool ChatAdministratorRights::get_can_manage_video_chats() const
    {
        return m_can_manage_video_chats;
    }

    std::optional<bool> ChatAdministratorRights::get_can_pin_messages() const
    {
        return m_can_pin_messages;
    }

    std::optional<bool> ChatAdministratorRights::get_can_post_messages() const
    {
        return m_can_post_messages;
    }

    bool ChatAdministratorRights::get_can_promote_members() const
    {
        return m_can_promote_members;
    }

    bool ChatAdministratorRights::get_can_restrict_members() const
    {
        return m_can_restrict_members;
    }

    bool ChatAdministratorRights::get_is_anonymous() const
    {
        return m_is_anonymous;
    }

    void ChatAdministratorRights::set_can_change_info(const bool &can_change_info)
    {
        m_can_change_info = can_change_info;
    }

    void ChatAdministratorRights::set_can_delete_messages(const bool &can_delete_messages)
    {
        m_can_delete_messages = can_delete_messages;
    }

    void ChatAdministratorRights::set_can_edit_messages(const std::optional<bool> &can_edit_messages)
    {
        m_can_edit_messages = can_edit_messages;
    }

    void ChatAdministratorRights::set_can_invite_users(const bool &can_invite_users)
    {
        m_can_invite_users = can_invite_users;
    }

    void ChatAdministratorRights::set_can_manage_chat(const bool &can_manage_chat)
    {
        m_can_manage_chat = can_manage_chat;
    }

    void ChatAdministratorRights::set_can_manage_topics(const std::optional<bool> &can_manage_topics)
    {
        m_can_manage_topics = can_manage_topics;
    }

    void ChatAdministratorRights::set_can_manage_video_chats(const bool &can_manage_video_chats)
    {
        m_can_manage_video_chats = can_manage_video_chats;
    }

    void ChatAdministratorRights::set_can_pin_messages(const std::optional<bool> &can_pin_messages)
    {
        m_can_pin_messages = can_pin_messages;
    }

    void ChatAdministratorRights::set_can_post_messages(const std::optional<bool> &can_post_messages)
    {
        m_can_post_messages = can_post_messages;
    }

    void ChatAdministratorRights::set_can_promote_members(const bool &can_promote_members)
    {
        m_can_promote_members = can_promote_members;
    }

    void ChatAdministratorRights::set_can_restrict_members(const bool &can_restrict_members)
    {
        m_can_restrict_members = can_restrict_members;
    }

    void ChatAdministratorRights::set_is_anonymous(const bool &is_anonymous)
    {
        m_is_anonymous = is_anonymous;
    }
} //namespace tgbot
