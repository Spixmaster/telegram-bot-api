#include "tgbot/types/InlineQueryResultLocation.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    InlineQueryResultLocation::InlineQueryResultLocation() : m_latitude(-1.0), m_longitude(-1.0)
    {}

    InlineQueryResultLocation::InlineQueryResultLocation(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("heading"))
            {
                if(doc["heading"].is_number_integer())
                {
                    m_heading = doc["heading"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("heading"));
                }
            }

            if(doc.contains("horizontal_accuracy"))
            {
                if(doc["horizontal_accuracy"].is_number_float())
                {
                    m_horizontal_accuracy = doc["horizontal_accuracy"].get<float>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_float("horizontal_accuracy"));
                }
            }

            if(doc.contains("id"))
            {
                if(doc["id"].is_string())
                {
                    m_id = doc["id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("id"));
            }

            if(doc.contains("input_message_content"))
            {
                if(doc["input_message_content"].is_object())
                {
                    m_input_message_content = std::make_shared<InputMessageContent>(doc["input_message_content"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("input_message_content"));
                }
            }

            if(doc.contains("latitude"))
            {
                if(doc["latitude"].is_number_float())
                {
                    m_latitude = doc["latitude"].get<float>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_float("latitude"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("latitude"));
            }

            if(doc.contains("live_period"))
            {
                if(doc["live_period"].is_number_integer())
                {
                    m_live_period = doc["live_period"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("live_period"));
                }
            }

            if(doc.contains("longitude"))
            {
                if(doc["longitude"].is_number_float())
                {
                    m_longitude = doc["longitude"].get<float>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_float("longitude"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("longitude"));
            }

            if(doc.contains("proximity_alert_radius"))
            {
                if(doc["proximity_alert_radius"].is_number_integer())
                {
                    m_proximity_alert_radius = doc["proximity_alert_radius"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("proximity_alert_radius"));
                }
            }

            if(doc.contains("reply_markup"))
            {
                if(doc["reply_markup"].is_object())
                {
                    m_reply_markup = std::make_shared<InlineKeyboardMarkup>(
                      doc["reply_"
                          "marku"
                          "p"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("reply_markup"));
                }
            }

            if(doc.contains("thumbnail_height"))
            {
                if(doc["thumbnail_height"].is_number_integer())
                {
                    m_thumbnail_height = doc["thumbnail_height"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("thumbnail_height"));
                }
            }

            if(doc.contains("thumbnail_url"))
            {
                if(doc["thumbnail_url"].is_string())
                {
                    m_thumbnail_url = doc["thumbnail_url"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("thumbnail_url"));
                }
            }

            if(doc.contains("thumbnail_width"))
            {
                if(doc["thumbnail_width"].is_number_integer())
                {
                    m_thumbnail_width = doc["thumbnail_width"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("thumbnail_width"));
                }
            }

            if(doc.contains("title"))
            {
                if(doc["title"].is_string())
                {
                    m_title = doc["title"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("title"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("title"));
            }

            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string InlineQueryResultLocation::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_heading.has_value())
        {
            doc["heading"] = m_heading.value();
        }

        if(m_horizontal_accuracy.has_value())
        {
            doc["horizontal_accuracy"] = m_horizontal_accuracy.value();
        }

        doc["id"] = m_id;

        if(m_input_message_content.has_value())
        {
            doc["input_message_content"] = nlohmann::json::parse(m_input_message_content.value()->serialise());
        }

        doc["latitude"] = m_latitude;

        if(m_live_period.has_value())
        {
            doc["live_period"] = m_live_period.value();
        }

        doc["longitude"] = m_longitude;

        if(m_proximity_alert_radius.has_value())
        {
            doc["proximity_alert_radius"] = m_proximity_alert_radius.value();
        }

        if(m_reply_markup.has_value())
        {
            doc["reply_markup"] = nlohmann::json::parse(m_reply_markup.value()->serialise());
        }

        if(m_thumbnail_height.has_value())
        {
            doc["thumbnail_height"] = m_thumbnail_height.value();
        }

        if(m_thumbnail_url.has_value())
        {
            doc["thumbnail_url"] = m_thumbnail_url.value();
        }

        if(m_thumbnail_width.has_value())
        {
            doc["thumbnail_width"] = m_thumbnail_width.value();
        }

        doc["title"] = m_title;
        doc["type"] = m_type;
        return doc.dump();
    }

    std::optional<std::int32_t> InlineQueryResultLocation::get_heading() const
    {
        return m_heading;
    }

    std::optional<float> InlineQueryResultLocation::get_horizontal_accuracy() const
    {
        return m_horizontal_accuracy;
    }

    std::string InlineQueryResultLocation::get_id() const
    {
        return m_id;
    }

    std::optional<InputMessageContent::ptr> InlineQueryResultLocation::get_input_message_content() const
    {
        return m_input_message_content;
    }

    float InlineQueryResultLocation::get_latitude() const
    {
        return m_latitude;
    }

    std::optional<std::int32_t> InlineQueryResultLocation::get_live_period() const
    {
        return m_live_period;
    }

    float InlineQueryResultLocation::get_longitude() const
    {
        return m_longitude;
    }

    std::optional<std::int32_t> InlineQueryResultLocation::get_proximity_alert_radius() const
    {
        return m_proximity_alert_radius;
    }

    std::optional<InlineKeyboardMarkup::ptr> InlineQueryResultLocation::get_reply_markup() const
    {
        return m_reply_markup;
    }

    std::optional<std::int32_t> InlineQueryResultLocation::get_thumbnail_height() const
    {
        return m_thumbnail_height;
    }

    std::optional<std::string> InlineQueryResultLocation::get_thumbnail_url() const
    {
        return m_thumbnail_url;
    }

    std::optional<std::int32_t> InlineQueryResultLocation::get_thumbnail_width() const
    {
        return m_thumbnail_width;
    }

    std::string InlineQueryResultLocation::get_title() const
    {
        return m_title;
    }

    std::string InlineQueryResultLocation::get_type() const
    {
        return m_type;
    }

    void InlineQueryResultLocation::set_heading(const std::optional<std::int32_t> &heading)
    {
        m_heading = heading;
    }

    void InlineQueryResultLocation::set_horizontal_accuracy(const std::optional<float> &horizontal_accuracy)
    {
        m_horizontal_accuracy = horizontal_accuracy;
    }

    void InlineQueryResultLocation::set_id(const std::string &id)
    {
        m_id = id;
    }

    void InlineQueryResultLocation::set_input_message_content(
      const std::optional<InputMessageContent::ptr> &input_message_content)
    {
        m_input_message_content = input_message_content;
    }

    void InlineQueryResultLocation::set_latitude(const float &latitude)
    {
        m_latitude = latitude;
    }

    void InlineQueryResultLocation::set_live_period(const std::optional<std::int32_t> &live_period)
    {
        m_live_period = live_period;
    }

    void InlineQueryResultLocation::set_longitude(const float &longitude)
    {
        m_longitude = longitude;
    }

    void InlineQueryResultLocation::set_proximity_alert_radius(const std::optional<std::int32_t> &proximity_alert_radius)
    {
        m_proximity_alert_radius = proximity_alert_radius;
    }

    void InlineQueryResultLocation::set_reply_markup(const std::optional<InlineKeyboardMarkup::ptr> &reply_markup)
    {
        m_reply_markup = reply_markup;
    }

    void InlineQueryResultLocation::set_thumbnail_height(const std::optional<std::int32_t> &thumbnail_height)
    {
        m_thumbnail_height = thumbnail_height;
    }

    void InlineQueryResultLocation::set_thumbnail_url(const std::optional<std::string> &thumbnail_url)
    {
        m_thumbnail_url = thumbnail_url;
    }

    void InlineQueryResultLocation::set_thumbnail_width(const std::optional<std::int32_t> &thumbnail_width)
    {
        m_thumbnail_width = thumbnail_width;
    }

    void InlineQueryResultLocation::set_title(const std::string &title)
    {
        m_title = title;
    }

    void InlineQueryResultLocation::set_type(const std::string &type)
    {
        m_type = type;
    }
} //namespace tgbot
