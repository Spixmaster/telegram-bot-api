#include "tgbot/types/Location.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    Location::Location() : m_latitude(-1.0), m_longitude(-1.0)
    {}

    Location::Location(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("heading"))
            {
                if(doc["heading"].is_number_integer())
                {
                    m_heading = doc["heading"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("heading"));
                }
            }

            if(doc.contains("horizontal_accuracy"))
            {
                if(doc["horizontal_accuracy"].is_number_float())
                {
                    m_horizontal_accuracy = doc["horizontal_accuracy"].get<float>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_float("horizontal_accuracy"));
                }
            }

            if(doc.contains("latitude"))
            {
                if(doc["latitude"].is_number_float())
                {
                    m_latitude = doc["latitude"].get<float>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_float("latitude"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("latitude"));
            }

            if(doc.contains("live_period"))
            {
                if(doc["live_period"].is_number_integer())
                {
                    m_live_period = doc["live_period"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("live_period"));
                }
            }

            if(doc.contains("longitude"))
            {
                if(doc["longitude"].is_number_float())
                {
                    m_longitude = doc["longitude"].get<float>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_float("longitude"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("longitude"));
            }

            if(doc.contains("proximity_alert_radius"))
            {
                if(doc["proximity_alert_radius"].is_number_integer())
                {
                    m_proximity_alert_radius = doc["proximity_alert_radius"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("proximity_alert_radius"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string Location::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_heading.has_value())
        {
            doc["heading"] = m_heading.value();
        }

        if(m_horizontal_accuracy.has_value())
        {
            doc["horizontal_accuracy"] = m_horizontal_accuracy.value();
        }

        doc["latitude"] = m_latitude;

        if(m_live_period.has_value())
        {
            doc["live_period"] = m_live_period.value();
        }

        doc["longitude"] = m_longitude;

        if(m_proximity_alert_radius.has_value())
        {
            doc["proximity_alert_radius"] = m_proximity_alert_radius.value();
        }

        return doc.dump();
    }

    std::optional<std::int32_t> Location::get_heading() const
    {
        return m_heading;
    }

    std::optional<float> Location::get_horizontal_accuracy() const
    {
        return m_horizontal_accuracy;
    }

    float Location::get_latitude() const
    {
        return m_latitude;
    }

    std::optional<std::int32_t> Location::get_live_period() const
    {
        return m_live_period;
    }

    float Location::get_longitude() const
    {
        return m_longitude;
    }

    std::optional<std::int32_t> Location::get_proximity_alert_radius() const
    {
        return m_proximity_alert_radius;
    }

    void Location::set_heading(const std::optional<std::int32_t> &heading)
    {
        m_heading = heading;
    }

    void Location::set_horizontal_accuracy(const std::optional<float> &horizontal_accuracy)
    {
        m_horizontal_accuracy = horizontal_accuracy;
    }

    void Location::set_latitude(const float &latitude)
    {
        m_latitude = latitude;
    }

    void Location::set_live_period(const std::optional<std::int32_t> &live_period)
    {
        m_live_period = live_period;
    }

    void Location::set_longitude(const float &longitude)
    {
        m_longitude = longitude;
    }

    void Location::set_proximity_alert_radius(const std::optional<std::int32_t> &proximity_alert_radius)
    {
        m_proximity_alert_radius = proximity_alert_radius;
    }
} //namespace tgbot
