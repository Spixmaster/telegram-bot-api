#include "tgbot/types/WebAppInfo.h"

#include <initializer_list>
#include <map>
#include <memory>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <string>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    WebAppInfo::WebAppInfo() = default;

    WebAppInfo::WebAppInfo(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("url"))
            {
                if(doc["url"].is_string())
                {
                    m_url = doc["url"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("url"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("url"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string WebAppInfo::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["url"] = m_url;
        return doc.dump();
    }

    std::string WebAppInfo::get_url() const
    {
        return m_url;
    }

    void WebAppInfo::set_url(const std::string &url)
    {
        m_url = url;
    }
} //namespace tgbot
