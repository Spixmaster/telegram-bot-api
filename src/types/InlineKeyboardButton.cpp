#include "tgbot/types/InlineKeyboardButton.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    InlineKeyboardButton::InlineKeyboardButton() = default;

    InlineKeyboardButton::InlineKeyboardButton(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("callback_data"))
            {
                if(doc["callback_data"].is_string())
                {
                    m_callback_data = doc["callback_data"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("callback_data"));
                }
            }

            if(doc.contains("callback_game"))
            {
                if(doc["callback_game"].is_object())
                {
                    m_callback_game = std::make_shared<CallbackGame>(
                      doc["callback_"
                          "game"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("callback_game"));
                }
            }

            if(doc.contains("login_url"))
            {
                if(doc["login_url"].is_object())
                {
                    m_login_url = std::make_shared<LoginUrl>(doc["login_url"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("login_url"));
                }
            }

            if(doc.contains("pay"))
            {
                if(doc["pay"].is_boolean())
                {
                    m_pay = doc["pay"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("pay"));
                }
            }

            if(doc.contains("switch_inline_query"))
            {
                if(doc["switch_inline_query"].is_string())
                {
                    m_switch_inline_query = doc["switch_inline_query"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("switch_inline_query"));
                }
            }

            if(doc.contains("switch_inline_query_current_chat"))
            {
                if(doc["switch_inline_query_current_chat"].is_string())
                {
                    m_switch_inline_query_current_chat = doc["switch_inline_query_current_chat"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string(
                      "switch_inline_query_current_"
                      "chat"));
                }
            }

            if(doc.contains("text"))
            {
                if(doc["text"].is_string())
                {
                    m_text = doc["text"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("text"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("text"));
            }

            if(doc.contains("url"))
            {
                if(doc["url"].is_string())
                {
                    m_url = doc["url"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("url"));
                }
            }

            if(doc.contains("web_app"))
            {
                if(doc["web_app"].is_object())
                {
                    m_web_app = std::make_shared<WebAppInfo>(doc["web_app"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("web_app"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string InlineKeyboardButton::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_callback_data.has_value())
        {
            doc["callback_data"] = m_callback_data.value();
        }

        if(m_callback_game.has_value())
        {
            doc["callback_game"] = nlohmann::json::parse(m_callback_game.value()->serialise());
        }

        if(m_login_url.has_value())
        {
            doc["login_url"] = nlohmann::json::parse(m_login_url.value()->serialise());
        }

        if(m_pay.has_value())
        {
            doc["pay"] = m_pay.value();
        }

        if(m_switch_inline_query.has_value())
        {
            doc["switch_inline_query"] = m_switch_inline_query.value();
        }

        if(m_switch_inline_query_current_chat.has_value())
        {
            doc["switch_inline_query_current_chat"] = m_switch_inline_query_current_chat.value();
        }

        doc["text"] = m_text;

        if(m_url.has_value())
        {
            doc["url"] = m_url.value();
        }

        if(m_web_app.has_value())
        {
            doc["web_app"] = nlohmann::json::parse(m_web_app.value()->serialise());
        }

        return doc.dump();
    }

    std::optional<std::string> InlineKeyboardButton::get_callback_data() const
    {
        return m_callback_data;
    }

    std::optional<CallbackGame::ptr> InlineKeyboardButton::get_callback_game() const
    {
        return m_callback_game;
    }

    std::optional<LoginUrl::ptr> InlineKeyboardButton::get_login_url() const
    {
        return m_login_url;
    }

    std::optional<bool> InlineKeyboardButton::get_pay() const
    {
        return m_pay;
    }

    std::optional<std::string> InlineKeyboardButton::get_switch_inline_query() const
    {
        return m_switch_inline_query;
    }

    std::optional<std::string> InlineKeyboardButton::get_switch_inline_query_current_chat() const
    {
        return m_switch_inline_query_current_chat;
    }

    std::string InlineKeyboardButton::get_text() const
    {
        return m_text;
    }

    std::optional<std::string> InlineKeyboardButton::get_url() const
    {
        return m_url;
    }

    std::optional<WebAppInfo::ptr> InlineKeyboardButton::get_web_app() const
    {
        return m_web_app;
    }

    void InlineKeyboardButton::set_callback_data(const std::optional<std::string> &callback_data)
    {
        m_callback_data = callback_data;
    }

    void InlineKeyboardButton::set_callback_game(const std::optional<CallbackGame::ptr> &callback_game)
    {
        m_callback_game = callback_game;
    }

    void InlineKeyboardButton::set_login_url(const std::optional<LoginUrl::ptr> &login_url)
    {
        m_login_url = login_url;
    }

    void InlineKeyboardButton::set_pay(const std::optional<bool> &pay)
    {
        m_pay = pay;
    }

    void InlineKeyboardButton::set_switch_inline_query(const std::optional<std::string> &switch_inline_query)
    {
        m_switch_inline_query = switch_inline_query;
    }

    void InlineKeyboardButton::set_switch_inline_query_current_chat(
      const std::optional<std::string> &switch_inline_query_current_chat)
    {
        m_switch_inline_query_current_chat = switch_inline_query_current_chat;
    }

    void InlineKeyboardButton::set_text(const std::string &text)
    {
        m_text = text;
    }

    void InlineKeyboardButton::set_url(const std::optional<std::string> &url)
    {
        m_url = url;
    }

    void InlineKeyboardButton::set_web_app(const std::optional<WebAppInfo::ptr> &web_app)
    {
        m_web_app = web_app;
    }
} //namespace tgbot
