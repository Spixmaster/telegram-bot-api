#include "tgbot/types/ForumTopic.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    ForumTopic::ForumTopic() : m_icon_color(-1), m_message_thread_id(-1)
    {}

    ForumTopic::ForumTopic(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("icon_color"))
            {
                if(doc["icon_color"].is_number_integer())
                {
                    m_icon_color = doc["icon_color"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("icon_color"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("icon_color"));
            }

            if(doc.contains("icon_custom_emoji_id"))
            {
                if(doc["icon_custom_emoji_id"].is_string())
                {
                    m_icon_custom_emoji_id = doc["icon_custom_emoji_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("icon_custom_emoji_id"));
                }
            }

            if(doc.contains("message_thread_id"))
            {
                if(doc["message_thread_id"].is_number_integer())
                {
                    m_message_thread_id = doc["message_thread_id"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("message_thread_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("message_thread_id"));
            }

            if(doc.contains("name"))
            {
                if(doc["name"].is_string())
                {
                    m_name = doc["name"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("name"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("name"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string ForumTopic::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["icon_color"] = m_icon_color;

        if(m_icon_custom_emoji_id.has_value())
        {
            doc["icon_custom_emoji_id"] = m_icon_custom_emoji_id.value();
        }

        doc["message_thread_id"] = m_message_thread_id;
        doc["name"] = m_name;
        return doc.dump();
    }

    std::int32_t ForumTopic::get_icon_color() const
    {
        return m_icon_color;
    }

    std::optional<std::string> ForumTopic::get_icon_custom_emoji_id() const
    {
        return m_icon_custom_emoji_id;
    }

    std::int32_t ForumTopic::get_message_thread_id() const
    {
        return m_message_thread_id;
    }

    std::string ForumTopic::get_name() const
    {
        return m_name;
    }

    void ForumTopic::set_icon_color(const std::int32_t &icon_color)
    {
        m_icon_color = icon_color;
    }

    void ForumTopic::set_icon_custom_emoji_id(const std::optional<std::string> &icon_custom_emoji_id)
    {
        m_icon_custom_emoji_id = icon_custom_emoji_id;
    }

    void ForumTopic::set_message_thread_id(const std::int32_t &message_thread_id)
    {
        m_message_thread_id = message_thread_id;
    }

    void ForumTopic::set_name(const std::string &name)
    {
        m_name = name;
    }
} //namespace tgbot
