#include "tgbot/types/Poll.h"

#include <algorithm>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "tgbot/constants/message.h"

namespace tgbot
{
    Poll::Poll() : m_allows_multiple_answers(false), m_is_anonymous(false), m_is_closed(false), m_total_voter_count(-1)
    {}

    Poll::Poll(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("allows_multiple_answers"))
            {
                if(doc["allows_multiple_answers"].is_boolean())
                {
                    m_allows_multiple_answers = doc["allows_multiple_answers"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("allows_multiple_answers"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("allows_multiple_answers"));
            }

            if(doc.contains("close_date"))
            {
                if(doc["close_date"].is_number_integer())
                {
                    m_close_date = doc["close_date"].get<std::uint64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("close_date"));
                }
            }

            if(doc.contains("correct_option_id"))
            {
                if(doc["correct_option_id"].is_number_integer())
                {
                    m_correct_option_id = doc["correct_option_id"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("correct_option_id"));
                }
            }

            if(doc.contains("explanation"))
            {
                if(doc["explanation"].is_string())
                {
                    m_explanation = doc["explanation"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("explanation"));
                }
            }

            if(doc.contains("explanation_entities"))
            {
                if(doc["explanation_entities"].is_array())
                {
                    std::vector<MessageEntity::ptr> explanation_entities;
                    explanation_entities.reserve(doc["explanation_entities"].size());

                    for(std::uint64_t i = 0; i < doc["explanation_entities"].size(); ++i)
                    {
                        if(doc["explanation_entities"].at(i).is_object())
                        {
                            explanation_entities.emplace_back(
                              std::make_shared<MessageEntity>(doc["explanation_entities"].at(i).dump()));
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_object(
                              "explanati"
                              "on_"
                              "entitie"
                              "s"));
                        }
                    }

                    m_explanation_entities = explanation_entities;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("explanation_entities"));
                }
            }

            if(doc.contains("id"))
            {
                if(doc["id"].is_string())
                {
                    m_id = doc["id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("id"));
            }

            if(doc.contains("is_anonymous"))
            {
                if(doc["is_anonymous"].is_boolean())
                {
                    m_is_anonymous = doc["is_anonymous"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("is_anonymous"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("is_anonymous"));
            }

            if(doc.contains("is_closed"))
            {
                if(doc["is_closed"].is_boolean())
                {
                    m_is_closed = doc["is_closed"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("is_closed"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("is_closed"));
            }

            if(doc.contains("open_period"))
            {
                if(doc["open_period"].is_number_integer())
                {
                    m_open_period = doc["open_period"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("open_period"));
                }
            }

            if(doc.contains("options"))
            {
                if(doc["options"].is_array())
                {
                    std::vector<PollOption::ptr> options;
                    options.reserve(doc["options"].size());

                    for(std::uint64_t i = 0; i < doc["options"].size(); ++i)
                    {
                        if(doc["options"].at(i).is_object())
                        {
                            options.emplace_back(std::make_shared<PollOption>(doc["options"].at(i).dump()));
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_object(
                              "option"
                              "s"));
                        }
                    }

                    m_options = options;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("options"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("options"));
            }

            if(doc.contains("question"))
            {
                if(doc["question"].is_string())
                {
                    m_question = doc["question"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("question"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("question"));
            }

            if(doc.contains("total_voter_count"))
            {
                if(doc["total_voter_count"].is_number_integer())
                {
                    m_total_voter_count = doc["total_voter_count"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("total_voter_count"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("total_voter_count"));
            }

            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string Poll::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["allows_multiple_answers"] = m_allows_multiple_answers;

        if(m_close_date.has_value())
        {
            doc["close_date"] = m_close_date.value();
        }

        if(m_correct_option_id.has_value())
        {
            doc["correct_option_id"] = m_correct_option_id.value();
        }

        if(m_explanation.has_value())
        {
            doc["explanation"] = m_explanation.value();
        }

        if(m_explanation_entities.has_value())
        {
            nlohmann::json explanation_entities = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_explanation_entities.value().size(); ++i)
            {
                explanation_entities.emplace_back(
                  nlohmann::json::parse(m_explanation_entities.value().at(i)->serialise()));
            }

            doc["explanation_entities"] = explanation_entities;
        }

        doc["id"] = m_id;
        doc["is_anonymous"] = m_is_anonymous;
        doc["is_closed"] = m_is_closed;

        if(m_open_period.has_value())
        {
            doc["open_period"] = m_open_period.value();
        }

        {
            nlohmann::json options = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_options.size(); ++i)
            {
                options.emplace_back(nlohmann::json::parse(m_options.at(i)->serialise()));
            }

            doc["options"] = options;
        }

        doc["question"] = m_question;
        doc["total_voter_count"] = m_total_voter_count;
        doc["type"] = m_type;
        return doc.dump();
    }

    bool Poll::get_allows_multiple_answers() const
    {
        return m_allows_multiple_answers;
    }

    std::optional<std::uint64_t> Poll::get_close_date() const
    {
        return m_close_date;
    }

    std::optional<std::int32_t> Poll::get_correct_option_id() const
    {
        return m_correct_option_id;
    }

    std::optional<std::string> Poll::get_explanation() const
    {
        return m_explanation;
    }

    std::optional<std::vector<MessageEntity::ptr>> Poll::get_explanation_entities() const
    {
        return m_explanation_entities;
    }

    std::string Poll::get_id() const
    {
        return m_id;
    }

    bool Poll::get_is_anonymous() const
    {
        return m_is_anonymous;
    }

    bool Poll::get_is_closed() const
    {
        return m_is_closed;
    }

    std::optional<std::int32_t> Poll::get_open_period() const
    {
        return m_open_period;
    }

    std::vector<PollOption::ptr> Poll::get_options() const
    {
        return m_options;
    }

    std::string Poll::get_question() const
    {
        return m_question;
    }

    std::int32_t Poll::get_total_voter_count() const
    {
        return m_total_voter_count;
    }

    std::string Poll::get_type() const
    {
        return m_type;
    }

    void Poll::set_allows_multiple_answers(const bool &allows_multiple_answers)
    {
        m_allows_multiple_answers = allows_multiple_answers;
    }

    void Poll::set_close_date(const std::optional<std::uint64_t> &close_date)
    {
        m_close_date = close_date;
    }

    void Poll::set_correct_option_id(const std::optional<std::int32_t> &correct_option_id)
    {
        m_correct_option_id = correct_option_id;
    }

    void Poll::set_explanation(const std::optional<std::string> &explanation)
    {
        m_explanation = explanation;
    }

    void Poll::set_explanation_entities(const std::optional<std::vector<MessageEntity::ptr>> &explanation_entities)
    {
        m_explanation_entities = explanation_entities;
    }

    void Poll::set_id(const std::string &id)
    {
        m_id = id;
    }

    void Poll::set_is_anonymous(const bool &is_anonymous)
    {
        m_is_anonymous = is_anonymous;
    }

    void Poll::set_is_closed(const bool &is_closed)
    {
        m_is_closed = is_closed;
    }

    void Poll::set_open_period(const std::optional<std::int32_t> &open_period)
    {
        m_open_period = open_period;
    }

    void Poll::set_options(const std::vector<PollOption::ptr> &options)
    {
        m_options = options;
    }

    void Poll::set_question(const std::string &question)
    {
        m_question = question;
    }

    void Poll::set_total_voter_count(const std::int32_t &total_voter_count)
    {
        m_total_voter_count = total_voter_count;
    }

    void Poll::set_type(const std::string &type)
    {
        m_type = type;
    }
} //namespace tgbot
