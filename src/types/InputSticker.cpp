#include "tgbot/types/InputSticker.h"

#include <algorithm>
#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    InputSticker::InputSticker() = default;

    InputSticker::InputSticker(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("emoji_list"))
            {
                if(doc["emoji_list"].is_array())
                {
                    std::vector<std::string> emoji_list;
                    emoji_list.reserve(doc["emoji_list"].size());

                    for(std::uint64_t i = 0; i < doc["emoji_list"].size(); ++i)
                    {
                        if(doc["emoji_list"].at(i).is_string())
                        {
                            emoji_list.emplace_back(doc["emoji_list"].at(i).get<std::string>());
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_string("emoji_list"));
                        }
                    }

                    m_emoji_list = emoji_list;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("emoji_list"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("emoji_list"));
            }

            if(doc.contains("keywords"))
            {
                if(doc["keywords"].is_array())
                {
                    std::vector<std::string> keywords;
                    keywords.reserve(doc["keywords"].size());

                    for(std::uint64_t i = 0; i < doc["keywords"].size(); ++i)
                    {
                        if(doc["keywords"].at(i).is_string())
                        {
                            keywords.emplace_back(doc["keywords"].at(i).get<std::string>());
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_string("keywords"));
                        }
                    }

                    m_keywords = keywords;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("keywords"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("keywords"));
            }

            if(doc.contains("mask_position"))
            {
                if(doc["mask_position"].is_object())
                {
                    m_mask_position = std::make_shared<MaskPosition>(doc["mask_position"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("mask_position"));
                }
            }

            if(doc.contains("sticker"))
            {
                if(doc["sticker"].is_string())
                {
                    m_sticker = doc["sticker"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("sticker"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("sticker"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string InputSticker::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        {
            nlohmann::json emoji_list = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_emoji_list.size(); ++i)
            {
                emoji_list.emplace_back(m_emoji_list.at(i));
            }

            doc["emoji_list"] = emoji_list;
        }

        {
            nlohmann::json keywords = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_keywords.size(); ++i)
            {
                keywords.emplace_back(m_keywords.at(i));
            }

            doc["keywords"] = keywords;
        }

        if(m_mask_position.has_value())
        {
            doc["mask_position"] = nlohmann::json::parse(m_mask_position.value()->serialise());
        }

        doc["sticker"] = m_sticker;
        return doc.dump();
    }

    std::vector<std::string> InputSticker::get_emoji_list() const
    {
        return m_emoji_list;
    }

    std::vector<std::string> InputSticker::get_keywords() const
    {
        return m_keywords;
    }

    std::optional<MaskPosition::ptr> InputSticker::get_mask_position() const
    {
        return m_mask_position;
    }

    std::string InputSticker::get_sticker() const
    {
        return m_sticker;
    }

    void InputSticker::set_emoji_list(const std::vector<std::string> &emoji_list)
    {
        m_emoji_list = emoji_list;
    }

    void InputSticker::set_keywords(const std::vector<std::string> &keywords)
    {
        m_keywords = keywords;
    }

    void InputSticker::set_mask_position(const std::optional<MaskPosition::ptr> &mask_position)
    {
        m_mask_position = mask_position;
    }

    void InputSticker::set_sticker(const std::string &sticker)
    {
        m_sticker = sticker;
    }
} //namespace tgbot
