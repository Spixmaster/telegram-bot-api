#include "tgbot/types/InlineQueryResultCachedGif.h"

#include <algorithm>
#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "tgbot/constants/message.h"

namespace tgbot
{
    InlineQueryResultCachedGif::InlineQueryResultCachedGif() = default;

    InlineQueryResultCachedGif::InlineQueryResultCachedGif(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("caption"))
            {
                if(doc["caption"].is_string())
                {
                    m_caption = doc["caption"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("caption"));
                }
            }

            if(doc.contains("caption_entities"))
            {
                if(doc["caption_entities"].is_array())
                {
                    std::vector<MessageEntity::ptr> caption_entities;
                    caption_entities.reserve(doc["caption_entities"].size());

                    for(std::uint64_t i = 0; i < doc["caption_entities"].size(); ++i)
                    {
                        if(doc["caption_entities"].at(i).is_object())
                        {
                            caption_entities.emplace_back(
                              std::make_shared<MessageEntity>(doc["caption_entities"].at(i).dump()));
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_object(
                              "caption_"
                              "entitie"
                              "s"));
                        }
                    }

                    m_caption_entities = caption_entities;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("caption_entities"));
                }
            }

            if(doc.contains("gif_file_id"))
            {
                if(doc["gif_file_id"].is_string())
                {
                    m_gif_file_id = doc["gif_file_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("gif_file_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("gif_file_id"));
            }

            if(doc.contains("id"))
            {
                if(doc["id"].is_string())
                {
                    m_id = doc["id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("id"));
            }

            if(doc.contains("input_message_content"))
            {
                if(doc["input_message_content"].is_object())
                {
                    m_input_message_content = std::make_shared<InputMessageContent>(doc["input_message_content"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("input_message_content"));
                }
            }

            if(doc.contains("parse_mode"))
            {
                if(doc["parse_mode"].is_string())
                {
                    m_parse_mode = doc["parse_mode"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("parse_mode"));
                }
            }

            if(doc.contains("reply_markup"))
            {
                if(doc["reply_markup"].is_object())
                {
                    m_reply_markup = std::make_shared<InlineKeyboardMarkup>(
                      doc["reply_"
                          "marku"
                          "p"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("reply_markup"));
                }
            }

            if(doc.contains("title"))
            {
                if(doc["title"].is_string())
                {
                    m_title = doc["title"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("title"));
                }
            }

            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string InlineQueryResultCachedGif::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_caption.has_value())
        {
            doc["caption"] = m_caption.value();
        }

        if(m_caption_entities.has_value())
        {
            nlohmann::json caption_entities = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_caption_entities.value().size(); ++i)
            {
                caption_entities.emplace_back(nlohmann::json::parse(m_caption_entities.value().at(i)->serialise()));
            }

            doc["caption_entities"] = caption_entities;
        }

        doc["gif_file_id"] = m_gif_file_id;
        doc["id"] = m_id;

        if(m_input_message_content.has_value())
        {
            doc["input_message_content"] = nlohmann::json::parse(m_input_message_content.value()->serialise());
        }

        if(m_parse_mode.has_value())
        {
            doc["parse_mode"] = m_parse_mode.value();
        }

        if(m_reply_markup.has_value())
        {
            doc["reply_markup"] = nlohmann::json::parse(m_reply_markup.value()->serialise());
        }

        if(m_title.has_value())
        {
            doc["title"] = m_title.value();
        }

        doc["type"] = m_type;
        return doc.dump();
    }

    std::optional<std::string> InlineQueryResultCachedGif::get_caption() const
    {
        return m_caption;
    }

    std::optional<std::vector<MessageEntity::ptr>> InlineQueryResultCachedGif::get_caption_entities() const
    {
        return m_caption_entities;
    }

    std::string InlineQueryResultCachedGif::get_gif_file_id() const
    {
        return m_gif_file_id;
    }

    std::string InlineQueryResultCachedGif::get_id() const
    {
        return m_id;
    }

    std::optional<InputMessageContent::ptr> InlineQueryResultCachedGif::get_input_message_content() const
    {
        return m_input_message_content;
    }

    std::optional<std::string> InlineQueryResultCachedGif::get_parse_mode() const
    {
        return m_parse_mode;
    }

    std::optional<InlineKeyboardMarkup::ptr> InlineQueryResultCachedGif::get_reply_markup() const
    {
        return m_reply_markup;
    }

    std::optional<std::string> InlineQueryResultCachedGif::get_title() const
    {
        return m_title;
    }

    std::string InlineQueryResultCachedGif::get_type() const
    {
        return m_type;
    }

    void InlineQueryResultCachedGif::set_caption(const std::optional<std::string> &caption)
    {
        m_caption = caption;
    }

    void InlineQueryResultCachedGif::set_caption_entities(
      const std::optional<std::vector<MessageEntity::ptr>> &caption_entities)
    {
        m_caption_entities = caption_entities;
    }

    void InlineQueryResultCachedGif::set_gif_file_id(const std::string &gif_file_id)
    {
        m_gif_file_id = gif_file_id;
    }

    void InlineQueryResultCachedGif::set_id(const std::string &id)
    {
        m_id = id;
    }

    void InlineQueryResultCachedGif::set_input_message_content(
      const std::optional<InputMessageContent::ptr> &input_message_content)
    {
        m_input_message_content = input_message_content;
    }

    void InlineQueryResultCachedGif::set_parse_mode(const std::optional<std::string> &parse_mode)
    {
        m_parse_mode = parse_mode;
    }

    void InlineQueryResultCachedGif::set_reply_markup(const std::optional<InlineKeyboardMarkup::ptr> &reply_markup)
    {
        m_reply_markup = reply_markup;
    }

    void InlineQueryResultCachedGif::set_title(const std::optional<std::string> &title)
    {
        m_title = title;
    }

    void InlineQueryResultCachedGif::set_type(const std::string &type)
    {
        m_type = type;
    }
} //namespace tgbot
