#include "tgbot/types/Venue.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    Venue::Venue() : m_location(std::make_shared<Location>())
    {}

    Venue::Venue(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("address"))
            {
                if(doc["address"].is_string())
                {
                    m_address = doc["address"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("address"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("address"));
            }

            if(doc.contains("foursquare_id"))
            {
                if(doc["foursquare_id"].is_string())
                {
                    m_foursquare_id = doc["foursquare_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("foursquare_id"));
                }
            }

            if(doc.contains("foursquare_type"))
            {
                if(doc["foursquare_type"].is_string())
                {
                    m_foursquare_type = doc["foursquare_type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("foursquare_type"));
                }
            }

            if(doc.contains("google_place_id"))
            {
                if(doc["google_place_id"].is_string())
                {
                    m_google_place_id = doc["google_place_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("google_place_id"));
                }
            }

            if(doc.contains("google_place_type"))
            {
                if(doc["google_place_type"].is_string())
                {
                    m_google_place_type = doc["google_place_type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("google_place_type"));
                }
            }

            if(doc.contains("location"))
            {
                if(doc["location"].is_object())
                {
                    m_location = std::make_shared<Location>(doc["location"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("location"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("location"));
            }

            if(doc.contains("title"))
            {
                if(doc["title"].is_string())
                {
                    m_title = doc["title"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("title"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("title"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string Venue::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["address"] = m_address;

        if(m_foursquare_id.has_value())
        {
            doc["foursquare_id"] = m_foursquare_id.value();
        }

        if(m_foursquare_type.has_value())
        {
            doc["foursquare_type"] = m_foursquare_type.value();
        }

        if(m_google_place_id.has_value())
        {
            doc["google_place_id"] = m_google_place_id.value();
        }

        if(m_google_place_type.has_value())
        {
            doc["google_place_type"] = m_google_place_type.value();
        }

        doc["location"] = nlohmann::json::parse(m_location->serialise());
        doc["title"] = m_title;
        return doc.dump();
    }

    std::string Venue::get_address() const
    {
        return m_address;
    }

    std::optional<std::string> Venue::get_foursquare_id() const
    {
        return m_foursquare_id;
    }

    std::optional<std::string> Venue::get_foursquare_type() const
    {
        return m_foursquare_type;
    }

    std::optional<std::string> Venue::get_google_place_id() const
    {
        return m_google_place_id;
    }

    std::optional<std::string> Venue::get_google_place_type() const
    {
        return m_google_place_type;
    }

    Location::ptr Venue::get_location() const
    {
        return m_location;
    }

    std::string Venue::get_title() const
    {
        return m_title;
    }

    void Venue::set_address(const std::string &address)
    {
        m_address = address;
    }

    void Venue::set_foursquare_id(const std::optional<std::string> &foursquare_id)
    {
        m_foursquare_id = foursquare_id;
    }

    void Venue::set_foursquare_type(const std::optional<std::string> &foursquare_type)
    {
        m_foursquare_type = foursquare_type;
    }

    void Venue::set_google_place_id(const std::optional<std::string> &google_place_id)
    {
        m_google_place_id = google_place_id;
    }

    void Venue::set_google_place_type(const std::optional<std::string> &google_place_type)
    {
        m_google_place_type = google_place_type;
    }

    void Venue::set_location(const Location::ptr &location)
    {
        m_location = location;
    }

    void Venue::set_title(const std::string &title)
    {
        m_title = title;
    }
} //namespace tgbot
