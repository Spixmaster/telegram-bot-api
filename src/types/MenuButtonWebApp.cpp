#include "tgbot/types/MenuButtonWebApp.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"
#include "tgbot/types/WebAppInfo.h"

namespace tgbot
{
    MenuButtonWebApp::MenuButtonWebApp() : m_web_app(std::make_shared<WebAppInfo>())
    {}

    MenuButtonWebApp::MenuButtonWebApp(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("text"))
            {
                if(doc["text"].is_string())
                {
                    m_text = doc["text"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("text"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("text"));
            }

            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }

            if(doc.contains("web_app"))
            {
                if(doc["web_app"].is_object())
                {
                    m_web_app = std::make_shared<WebAppInfo>(doc["web_app"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("web_app"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("web_app"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string MenuButtonWebApp::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["text"] = m_text;
        doc["type"] = m_type;
        doc["web_app"] = nlohmann::json::parse(m_web_app->serialise());
        return doc.dump();
    }

    std::string MenuButtonWebApp::get_text() const
    {
        return m_text;
    }

    std::string MenuButtonWebApp::get_type() const
    {
        return m_type;
    }

    WebAppInfo::ptr MenuButtonWebApp::get_web_app() const
    {
        return m_web_app;
    }

    void MenuButtonWebApp::set_text(const std::string &text)
    {
        m_text = text;
    }

    void MenuButtonWebApp::set_type(const std::string &type)
    {
        m_type = type;
    }

    void MenuButtonWebApp::set_web_app(const WebAppInfo::ptr &web_app)
    {
        m_web_app = web_app;
    }
} //namespace tgbot
