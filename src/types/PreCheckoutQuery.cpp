#include "tgbot/types/PreCheckoutQuery.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    PreCheckoutQuery::PreCheckoutQuery() : m_from(std::make_shared<User>()), m_total_amount(-1)
    {}

    PreCheckoutQuery::PreCheckoutQuery(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("currency"))
            {
                if(doc["currency"].is_string())
                {
                    m_currency = doc["currency"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("currency"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("currency"));
            }

            if(doc.contains("from"))
            {
                if(doc["from"].is_object())
                {
                    m_from = std::make_shared<User>(doc["from"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("from"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("from"));
            }

            if(doc.contains("id"))
            {
                if(doc["id"].is_string())
                {
                    m_id = doc["id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("id"));
            }

            if(doc.contains("invoice_payload"))
            {
                if(doc["invoice_payload"].is_string())
                {
                    m_invoice_payload = doc["invoice_payload"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("invoice_payload"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("invoice_payload"));
            }

            if(doc.contains("order_info"))
            {
                if(doc["order_info"].is_object())
                {
                    m_order_info = std::make_shared<OrderInfo>(doc["order_info"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("order_info"));
                }
            }

            if(doc.contains("shipping_option_id"))
            {
                if(doc["shipping_option_id"].is_string())
                {
                    m_shipping_option_id = doc["shipping_option_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("shipping_option_id"));
                }
            }

            if(doc.contains("total_amount"))
            {
                if(doc["total_amount"].is_number_integer())
                {
                    m_total_amount = doc["total_amount"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("total_amount"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("total_amount"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string PreCheckoutQuery::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["currency"] = m_currency;
        doc["from"] = nlohmann::json::parse(m_from->serialise());
        doc["id"] = m_id;
        doc["invoice_payload"] = m_invoice_payload;

        if(m_order_info.has_value())
        {
            doc["order_info"] = nlohmann::json::parse(m_order_info.value()->serialise());
        }

        if(m_shipping_option_id.has_value())
        {
            doc["shipping_option_id"] = m_shipping_option_id.value();
        }

        doc["total_amount"] = m_total_amount;
        return doc.dump();
    }

    std::string PreCheckoutQuery::get_currency() const
    {
        return m_currency;
    }

    User::ptr PreCheckoutQuery::get_from() const
    {
        return m_from;
    }

    std::string PreCheckoutQuery::get_id() const
    {
        return m_id;
    }

    std::string PreCheckoutQuery::get_invoice_payload() const
    {
        return m_invoice_payload;
    }

    std::optional<OrderInfo::ptr> PreCheckoutQuery::get_order_info() const
    {
        return m_order_info;
    }

    std::optional<std::string> PreCheckoutQuery::get_shipping_option_id() const
    {
        return m_shipping_option_id;
    }

    std::int32_t PreCheckoutQuery::get_total_amount() const
    {
        return m_total_amount;
    }

    void PreCheckoutQuery::set_currency(const std::string &currency)
    {
        m_currency = currency;
    }

    void PreCheckoutQuery::set_from(const User::ptr &from)
    {
        m_from = from;
    }

    void PreCheckoutQuery::set_id(const std::string &id)
    {
        m_id = id;
    }

    void PreCheckoutQuery::set_invoice_payload(const std::string &invoice_payload)
    {
        m_invoice_payload = invoice_payload;
    }

    void PreCheckoutQuery::set_order_info(const std::optional<OrderInfo::ptr> &order_info)
    {
        m_order_info = order_info;
    }

    void PreCheckoutQuery::set_shipping_option_id(const std::optional<std::string> &shipping_option_id)
    {
        m_shipping_option_id = shipping_option_id;
    }

    void PreCheckoutQuery::set_total_amount(const std::int32_t &total_amount)
    {
        m_total_amount = total_amount;
    }
} //namespace tgbot
