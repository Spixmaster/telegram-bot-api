#include "tgbot/types/PassportData.h"

#include <algorithm>
#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>

#include "tgbot/constants/message.h"

namespace tgbot
{
    PassportData::PassportData() : m_credentials(std::make_shared<EncryptedCredentials>())
    {}

    PassportData::PassportData(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("credentials"))
            {
                if(doc["credentials"].is_object())
                {
                    m_credentials = std::make_shared<EncryptedCredentials>(
                      doc["credentia"
                          "ls"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("credentials"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("credentials"));
            }

            if(doc.contains("data"))
            {
                if(doc["data"].is_array())
                {
                    std::vector<EncryptedPassportElement::ptr> data;
                    data.reserve(doc["data"].size());

                    for(std::uint64_t i = 0; i < doc["data"].size(); ++i)
                    {
                        if(doc["data"].at(i).is_object())
                        {
                            data.emplace_back(std::make_shared<EncryptedPassportElement>(doc["data"].at(i).dump()));
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_object("data"));
                        }
                    }

                    m_data = data;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("data"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("data"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string PassportData::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["credentials"] = nlohmann::json::parse(m_credentials->serialise());

        {
            nlohmann::json data = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_data.size(); ++i)
            {
                data.emplace_back(nlohmann::json::parse(m_data.at(i)->serialise()));
            }

            doc["data"] = data;
        }

        return doc.dump();
    }

    EncryptedCredentials::ptr PassportData::get_credentials() const
    {
        return m_credentials;
    }

    std::vector<EncryptedPassportElement::ptr> PassportData::get_data() const
    {
        return m_data;
    }

    void PassportData::set_credentials(const EncryptedCredentials::ptr &credentials)
    {
        m_credentials = credentials;
    }

    void PassportData::set_data(const std::vector<EncryptedPassportElement::ptr> &data)
    {
        m_data = data;
    }
} //namespace tgbot
