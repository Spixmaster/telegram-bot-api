#include "tgbot/types/PollOption.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    PollOption::PollOption() : m_voter_count(-1)
    {}

    PollOption::PollOption(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("text"))
            {
                if(doc["text"].is_string())
                {
                    m_text = doc["text"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("text"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("text"));
            }

            if(doc.contains("voter_count"))
            {
                if(doc["voter_count"].is_number_integer())
                {
                    m_voter_count = doc["voter_count"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("voter_count"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("voter_count"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string PollOption::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["text"] = m_text;
        doc["voter_count"] = m_voter_count;
        return doc.dump();
    }

    std::string PollOption::get_text() const
    {
        return m_text;
    }

    std::int32_t PollOption::get_voter_count() const
    {
        return m_voter_count;
    }

    void PollOption::set_text(const std::string &text)
    {
        m_text = text;
    }

    void PollOption::set_voter_count(const std::int32_t &voter_count)
    {
        m_voter_count = voter_count;
    }
} //namespace tgbot
