#include "tgbot/types/InputMediaAnimation.h"

#include <algorithm>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "tgbot/constants/message.h"

namespace tgbot
{
    InputMediaAnimation::InputMediaAnimation() = default;

    InputMediaAnimation::InputMediaAnimation(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("caption"))
            {
                if(doc["caption"].is_string())
                {
                    m_caption = doc["caption"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("caption"));
                }
            }

            if(doc.contains("caption_entities"))
            {
                if(doc["caption_entities"].is_array())
                {
                    std::vector<MessageEntity::ptr> caption_entities;
                    caption_entities.reserve(doc["caption_entities"].size());

                    for(std::uint64_t i = 0; i < doc["caption_entities"].size(); ++i)
                    {
                        if(doc["caption_entities"].at(i).is_object())
                        {
                            caption_entities.emplace_back(
                              std::make_shared<MessageEntity>(doc["caption_entities"].at(i).dump()));
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_object(
                              "caption_"
                              "entitie"
                              "s"));
                        }
                    }

                    m_caption_entities = caption_entities;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("caption_entities"));
                }
            }

            if(doc.contains("duration"))
            {
                if(doc["duration"].is_number_integer())
                {
                    m_duration = doc["duration"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("duration"));
                }
            }

            if(doc.contains("has_spoiler"))
            {
                if(doc["has_spoiler"].is_boolean())
                {
                    m_has_spoiler = doc["has_spoiler"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("has_spoiler"));
                }
            }

            if(doc.contains("height"))
            {
                if(doc["height"].is_number_integer())
                {
                    m_height = doc["height"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("height"));
                }
            }

            if(doc.contains("media"))
            {
                if(doc["media"].is_string())
                {
                    m_media = doc["media"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("media"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("media"));
            }

            if(doc.contains("parse_mode"))
            {
                if(doc["parse_mode"].is_string())
                {
                    m_parse_mode = doc["parse_mode"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("parse_mode"));
                }
            }

            if(doc.contains("thumbnail"))
            {
                if(doc["thumbnail"].is_string())
                {
                    m_thumbnail = doc["thumbnail"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("thumbnail"));
                }
            }

            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }

            if(doc.contains("width"))
            {
                if(doc["width"].is_number_integer())
                {
                    m_width = doc["width"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("width"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string InputMediaAnimation::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_caption.has_value())
        {
            doc["caption"] = m_caption.value();
        }

        if(m_caption_entities.has_value())
        {
            nlohmann::json caption_entities = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_caption_entities.value().size(); ++i)
            {
                caption_entities.emplace_back(nlohmann::json::parse(m_caption_entities.value().at(i)->serialise()));
            }

            doc["caption_entities"] = caption_entities;
        }

        if(m_duration.has_value())
        {
            doc["duration"] = m_duration.value();
        }

        if(m_has_spoiler.has_value())
        {
            doc["has_spoiler"] = m_has_spoiler.value();
        }

        if(m_height.has_value())
        {
            doc["height"] = m_height.value();
        }

        doc["media"] = m_media;

        if(m_parse_mode.has_value())
        {
            doc["parse_mode"] = m_parse_mode.value();
        }

        if(m_thumbnail.has_value())
        {
            doc["thumbnail"] = m_thumbnail.value();
        }

        doc["type"] = m_type;

        if(m_width.has_value())
        {
            doc["width"] = m_width.value();
        }

        return doc.dump();
    }

    std::optional<std::string> InputMediaAnimation::get_caption() const
    {
        return m_caption;
    }

    std::optional<std::vector<MessageEntity::ptr>> InputMediaAnimation::get_caption_entities() const
    {
        return m_caption_entities;
    }

    std::optional<std::int32_t> InputMediaAnimation::get_duration() const
    {
        return m_duration;
    }

    std::optional<bool> InputMediaAnimation::get_has_spoiler() const
    {
        return m_has_spoiler;
    }

    std::optional<std::int32_t> InputMediaAnimation::get_height() const
    {
        return m_height;
    }

    std::string InputMediaAnimation::get_media() const
    {
        return m_media;
    }

    std::optional<std::string> InputMediaAnimation::get_parse_mode() const
    {
        return m_parse_mode;
    }

    std::optional<std::string> InputMediaAnimation::get_thumbnail() const
    {
        return m_thumbnail;
    }

    std::string InputMediaAnimation::get_type() const
    {
        return m_type;
    }

    std::optional<std::int32_t> InputMediaAnimation::get_width() const
    {
        return m_width;
    }

    void InputMediaAnimation::set_caption(const std::optional<std::string> &caption)
    {
        m_caption = caption;
    }

    void
      InputMediaAnimation::set_caption_entities(const std::optional<std::vector<MessageEntity::ptr>> &caption_entities)
    {
        m_caption_entities = caption_entities;
    }

    void InputMediaAnimation::set_duration(const std::optional<std::int32_t> &duration)
    {
        m_duration = duration;
    }

    void InputMediaAnimation::set_has_spoiler(const std::optional<bool> &has_spoiler)
    {
        m_has_spoiler = has_spoiler;
    }

    void InputMediaAnimation::set_height(const std::optional<std::int32_t> &height)
    {
        m_height = height;
    }

    void InputMediaAnimation::set_media(const std::string &media)
    {
        m_media = media;
    }

    void InputMediaAnimation::set_parse_mode(const std::optional<std::string> &parse_mode)
    {
        m_parse_mode = parse_mode;
    }

    void InputMediaAnimation::set_thumbnail(const std::optional<std::string> &thumbnail)
    {
        m_thumbnail = thumbnail;
    }

    void InputMediaAnimation::set_type(const std::string &type)
    {
        m_type = type;
    }

    void InputMediaAnimation::set_width(const std::optional<std::int32_t> &width)
    {
        m_width = width;
    }
} //namespace tgbot
