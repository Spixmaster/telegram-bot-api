#include "tgbot/types/ChatLocation.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    ChatLocation::ChatLocation() : m_location(std::make_shared<Location>())
    {}

    ChatLocation::ChatLocation(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("address"))
            {
                if(doc["address"].is_string())
                {
                    m_address = doc["address"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("address"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("address"));
            }

            if(doc.contains("location"))
            {
                if(doc["location"].is_object())
                {
                    m_location = std::make_shared<Location>(doc["location"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("location"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("location"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string ChatLocation::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["address"] = m_address;
        doc["location"] = nlohmann::json::parse(m_location->serialise());
        return doc.dump();
    }

    std::string ChatLocation::get_address() const
    {
        return m_address;
    }

    Location::ptr ChatLocation::get_location() const
    {
        return m_location;
    }

    void ChatLocation::set_address(const std::string &address)
    {
        m_address = address;
    }

    void ChatLocation::set_location(const Location::ptr &location)
    {
        m_location = location;
    }
} //namespace tgbot
