#include "tgbot/types/GameHighScore.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    GameHighScore::GameHighScore() : m_position(-1), m_score(-1), m_user(std::make_shared<User>())
    {}

    GameHighScore::GameHighScore(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("position"))
            {
                if(doc["position"].is_number_integer())
                {
                    m_position = doc["position"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("position"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("position"));
            }

            if(doc.contains("score"))
            {
                if(doc["score"].is_number_integer())
                {
                    m_score = doc["score"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("score"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("score"));
            }

            if(doc.contains("user"))
            {
                if(doc["user"].is_object())
                {
                    m_user = std::make_shared<User>(doc["user"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("user"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("user"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string GameHighScore::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["position"] = m_position;
        doc["score"] = m_score;
        doc["user"] = nlohmann::json::parse(m_user->serialise());
        return doc.dump();
    }

    std::int32_t GameHighScore::get_position() const
    {
        return m_position;
    }

    std::int32_t GameHighScore::get_score() const
    {
        return m_score;
    }

    User::ptr GameHighScore::get_user() const
    {
        return m_user;
    }

    void GameHighScore::set_position(const std::int32_t &position)
    {
        m_position = position;
    }

    void GameHighScore::set_score(const std::int32_t &score)
    {
        m_score = score;
    }

    void GameHighScore::set_user(const User::ptr &user)
    {
        m_user = user;
    }
} //namespace tgbot
