#include "tgbot/types/CallbackQuery.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    CallbackQuery::CallbackQuery() : m_from(std::make_shared<User>())
    {}

    CallbackQuery::CallbackQuery(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("chat_instance"))
            {
                if(doc["chat_instance"].is_string())
                {
                    m_chat_instance = doc["chat_instance"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("chat_instance"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("chat_instance"));
            }

            if(doc.contains("data"))
            {
                if(doc["data"].is_string())
                {
                    m_data = doc["data"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("data"));
                }
            }

            if(doc.contains("from"))
            {
                if(doc["from"].is_object())
                {
                    m_from = std::make_shared<User>(doc["from"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("from"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("from"));
            }

            if(doc.contains("game_short_name"))
            {
                if(doc["game_short_name"].is_string())
                {
                    m_game_short_name = doc["game_short_name"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("game_short_name"));
                }
            }

            if(doc.contains("id"))
            {
                if(doc["id"].is_string())
                {
                    m_id = doc["id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("id"));
            }

            if(doc.contains("inline_message_id"))
            {
                if(doc["inline_message_id"].is_string())
                {
                    m_inline_message_id = doc["inline_message_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("inline_message_id"));
                }
            }

            if(doc.contains("message"))
            {
                if(doc["message"].is_object())
                {
                    m_message = std::make_shared<Message>(doc["message"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("message"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string CallbackQuery::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["chat_instance"] = m_chat_instance;

        if(m_data.has_value())
        {
            doc["data"] = m_data.value();
        }

        doc["from"] = nlohmann::json::parse(m_from->serialise());

        if(m_game_short_name.has_value())
        {
            doc["game_short_name"] = m_game_short_name.value();
        }

        doc["id"] = m_id;

        if(m_inline_message_id.has_value())
        {
            doc["inline_message_id"] = m_inline_message_id.value();
        }

        if(m_message.has_value())
        {
            doc["message"] = nlohmann::json::parse(m_message.value()->serialise());
        }

        return doc.dump();
    }

    std::string CallbackQuery::get_chat_instance() const
    {
        return m_chat_instance;
    }

    std::optional<std::string> CallbackQuery::get_data() const
    {
        return m_data;
    }

    User::ptr CallbackQuery::get_from() const
    {
        return m_from;
    }

    std::optional<std::string> CallbackQuery::get_game_short_name() const
    {
        return m_game_short_name;
    }

    std::string CallbackQuery::get_id() const
    {
        return m_id;
    }

    std::optional<std::string> CallbackQuery::get_inline_message_id() const
    {
        return m_inline_message_id;
    }

    std::optional<Message::ptr> CallbackQuery::get_message() const
    {
        return m_message;
    }

    void CallbackQuery::set_chat_instance(const std::string &chat_instance)
    {
        m_chat_instance = chat_instance;
    }

    void CallbackQuery::set_data(const std::optional<std::string> &data)
    {
        m_data = data;
    }

    void CallbackQuery::set_from(const User::ptr &from)
    {
        m_from = from;
    }

    void CallbackQuery::set_game_short_name(const std::optional<std::string> &game_short_name)
    {
        m_game_short_name = game_short_name;
    }

    void CallbackQuery::set_id(const std::string &id)
    {
        m_id = id;
    }

    void CallbackQuery::set_inline_message_id(const std::optional<std::string> &inline_message_id)
    {
        m_inline_message_id = inline_message_id;
    }

    void CallbackQuery::set_message(const std::optional<Message::ptr> &message)
    {
        m_message = message;
    }
} //namespace tgbot
