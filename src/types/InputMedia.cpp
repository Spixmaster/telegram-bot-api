#include "tgbot/types/InputMedia.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <string>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    InputMedia::InputMedia() = default;

    InputMedia::InputMedia(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {}
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string InputMedia::serialise() const
    {
        const nlohmann::json doc = nlohmann::json::object();
        return doc.dump();
    }
} //namespace tgbot
