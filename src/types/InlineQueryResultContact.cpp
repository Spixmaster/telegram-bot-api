#include "tgbot/types/InlineQueryResultContact.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"
#include "tgbot/types/InlineKeyboardMarkup.h"
#include "tgbot/types/InputMessageContent.h"

namespace tgbot
{
    InlineQueryResultContact::InlineQueryResultContact() = default;

    InlineQueryResultContact::InlineQueryResultContact(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("first_name"))
            {
                if(doc["first_name"].is_string())
                {
                    m_first_name = doc["first_name"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("first_name"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("first_name"));
            }

            if(doc.contains("id"))
            {
                if(doc["id"].is_string())
                {
                    m_id = doc["id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("id"));
            }

            if(doc.contains("input_message_content"))
            {
                if(doc["input_message_content"].is_object())
                {
                    m_input_message_content = std::make_shared<InputMessageContent>(doc["input_message_content"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("input_message_content"));
                }
            }

            if(doc.contains("last_name"))
            {
                if(doc["last_name"].is_string())
                {
                    m_last_name = doc["last_name"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("last_name"));
                }
            }

            if(doc.contains("phone_number"))
            {
                if(doc["phone_number"].is_string())
                {
                    m_phone_number = doc["phone_number"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("phone_number"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("phone_number"));
            }

            if(doc.contains("reply_markup"))
            {
                if(doc["reply_markup"].is_object())
                {
                    m_reply_markup = std::make_shared<InlineKeyboardMarkup>(
                      doc["reply_"
                          "marku"
                          "p"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("reply_markup"));
                }
            }

            if(doc.contains("thumbnail_height"))
            {
                if(doc["thumbnail_height"].is_number_integer())
                {
                    m_thumbnail_height = doc["thumbnail_height"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("thumbnail_height"));
                }
            }

            if(doc.contains("thumbnail_url"))
            {
                if(doc["thumbnail_url"].is_string())
                {
                    m_thumbnail_url = doc["thumbnail_url"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("thumbnail_url"));
                }
            }

            if(doc.contains("thumbnail_width"))
            {
                if(doc["thumbnail_width"].is_number_integer())
                {
                    m_thumbnail_width = doc["thumbnail_width"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("thumbnail_width"));
                }
            }

            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }

            if(doc.contains("vcard"))
            {
                if(doc["vcard"].is_string())
                {
                    m_vcard = doc["vcard"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("vcard"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string InlineQueryResultContact::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["first_name"] = m_first_name;
        doc["id"] = m_id;

        if(m_input_message_content.has_value())
        {
            doc["input_message_content"] = nlohmann::json::parse(m_input_message_content.value()->serialise());
        }

        if(m_last_name.has_value())
        {
            doc["last_name"] = m_last_name.value();
        }

        doc["phone_number"] = m_phone_number;

        if(m_reply_markup.has_value())
        {
            doc["reply_markup"] = nlohmann::json::parse(m_reply_markup.value()->serialise());
        }

        if(m_thumbnail_height.has_value())
        {
            doc["thumbnail_height"] = m_thumbnail_height.value();
        }

        if(m_thumbnail_url.has_value())
        {
            doc["thumbnail_url"] = m_thumbnail_url.value();
        }

        if(m_thumbnail_width.has_value())
        {
            doc["thumbnail_width"] = m_thumbnail_width.value();
        }

        doc["type"] = m_type;

        if(m_vcard.has_value())
        {
            doc["vcard"] = m_vcard.value();
        }

        return doc.dump();
    }

    std::string InlineQueryResultContact::get_first_name() const
    {
        return m_first_name;
    }

    std::string InlineQueryResultContact::get_id() const
    {
        return m_id;
    }

    std::optional<InputMessageContent::ptr> InlineQueryResultContact::get_input_message_content() const
    {
        return m_input_message_content;
    }

    std::optional<std::string> InlineQueryResultContact::get_last_name() const
    {
        return m_last_name;
    }

    std::string InlineQueryResultContact::get_phone_number() const
    {
        return m_phone_number;
    }

    std::optional<InlineKeyboardMarkup::ptr> InlineQueryResultContact::get_reply_markup() const
    {
        return m_reply_markup;
    }

    std::optional<std::int32_t> InlineQueryResultContact::get_thumbnail_height() const
    {
        return m_thumbnail_height;
    }

    std::optional<std::string> InlineQueryResultContact::get_thumbnail_url() const
    {
        return m_thumbnail_url;
    }

    std::optional<std::int32_t> InlineQueryResultContact::get_thumbnail_width() const
    {
        return m_thumbnail_width;
    }

    std::string InlineQueryResultContact::get_type() const
    {
        return m_type;
    }

    std::optional<std::string> InlineQueryResultContact::get_vcard() const
    {
        return m_vcard;
    }

    void InlineQueryResultContact::set_first_name(const std::string &first_name)
    {
        m_first_name = first_name;
    }

    void InlineQueryResultContact::set_id(const std::string &id)
    {
        m_id = id;
    }

    void InlineQueryResultContact::set_input_message_content(
      const std::optional<InputMessageContent::ptr> &input_message_content)
    {
        m_input_message_content = input_message_content;
    }

    void InlineQueryResultContact::set_last_name(const std::optional<std::string> &last_name)
    {
        m_last_name = last_name;
    }

    void InlineQueryResultContact::set_phone_number(const std::string &phone_number)
    {
        m_phone_number = phone_number;
    }

    void InlineQueryResultContact::set_reply_markup(const std::optional<InlineKeyboardMarkup::ptr> &reply_markup)
    {
        m_reply_markup = reply_markup;
    }

    void InlineQueryResultContact::set_thumbnail_height(const std::optional<std::int32_t> &thumbnail_height)
    {
        m_thumbnail_height = thumbnail_height;
    }

    void InlineQueryResultContact::set_thumbnail_url(const std::optional<std::string> &thumbnail_url)
    {
        m_thumbnail_url = thumbnail_url;
    }

    void InlineQueryResultContact::set_thumbnail_width(const std::optional<std::int32_t> &thumbnail_width)
    {
        m_thumbnail_width = thumbnail_width;
    }

    void InlineQueryResultContact::set_type(const std::string &type)
    {
        m_type = type;
    }

    void InlineQueryResultContact::set_vcard(const std::optional<std::string> &vcard)
    {
        m_vcard = vcard;
    }
} //namespace tgbot
