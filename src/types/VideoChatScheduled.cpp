#include "tgbot/types/VideoChatScheduled.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    VideoChatScheduled::VideoChatScheduled() : m_start_date(1)
    {}

    VideoChatScheduled::VideoChatScheduled(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("start_date"))
            {
                if(doc["start_date"].is_number_integer())
                {
                    m_start_date = doc["start_date"].get<std::uint64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("start_date"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("start_date"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string VideoChatScheduled::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["start_date"] = m_start_date;
        return doc.dump();
    }

    std::uint64_t VideoChatScheduled::get_start_date() const
    {
        return m_start_date;
    }

    void VideoChatScheduled::set_start_date(const std::uint64_t &start_date)
    {
        m_start_date = start_date;
    }
} //namespace tgbot
