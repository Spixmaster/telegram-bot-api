#include "tgbot/types/InlineQueryResultArticle.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    InlineQueryResultArticle::InlineQueryResultArticle() :
        m_input_message_content(std::make_shared<InputMessageContent>())
    {}

    InlineQueryResultArticle::InlineQueryResultArticle(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("description"))
            {
                if(doc["description"].is_string())
                {
                    m_description = doc["description"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("description"));
                }
            }

            if(doc.contains("hide_url"))
            {
                if(doc["hide_url"].is_boolean())
                {
                    m_hide_url = doc["hide_url"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("hide_url"));
                }
            }

            if(doc.contains("id"))
            {
                if(doc["id"].is_string())
                {
                    m_id = doc["id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("id"));
            }

            if(doc.contains("input_message_content"))
            {
                if(doc["input_message_content"].is_object())
                {
                    m_input_message_content = std::make_shared<InputMessageContent>(doc["input_message_content"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("input_message_content"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("input_message_content"));
            }

            if(doc.contains("reply_markup"))
            {
                if(doc["reply_markup"].is_object())
                {
                    m_reply_markup = std::make_shared<InlineKeyboardMarkup>(
                      doc["reply_"
                          "marku"
                          "p"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("reply_markup"));
                }
            }

            if(doc.contains("thumbnail_height"))
            {
                if(doc["thumbnail_height"].is_number_integer())
                {
                    m_thumbnail_height = doc["thumbnail_height"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("thumbnail_height"));
                }
            }

            if(doc.contains("thumbnail_url"))
            {
                if(doc["thumbnail_url"].is_string())
                {
                    m_thumbnail_url = doc["thumbnail_url"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("thumbnail_url"));
                }
            }

            if(doc.contains("thumbnail_width"))
            {
                if(doc["thumbnail_width"].is_number_integer())
                {
                    m_thumbnail_width = doc["thumbnail_width"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("thumbnail_width"));
                }
            }

            if(doc.contains("title"))
            {
                if(doc["title"].is_string())
                {
                    m_title = doc["title"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("title"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("title"));
            }

            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }

            if(doc.contains("url"))
            {
                if(doc["url"].is_string())
                {
                    m_url = doc["url"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("url"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string InlineQueryResultArticle::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_description.has_value())
        {
            doc["description"] = m_description.value();
        }

        if(m_hide_url.has_value())
        {
            doc["hide_url"] = m_hide_url.value();
        }

        doc["id"] = m_id;
        doc["input_message_content"] = nlohmann::json::parse(m_input_message_content->serialise());

        if(m_reply_markup.has_value())
        {
            doc["reply_markup"] = nlohmann::json::parse(m_reply_markup.value()->serialise());
        }

        if(m_thumbnail_height.has_value())
        {
            doc["thumbnail_height"] = m_thumbnail_height.value();
        }

        if(m_thumbnail_url.has_value())
        {
            doc["thumbnail_url"] = m_thumbnail_url.value();
        }

        if(m_thumbnail_width.has_value())
        {
            doc["thumbnail_width"] = m_thumbnail_width.value();
        }

        doc["title"] = m_title;
        doc["type"] = m_type;

        if(m_url.has_value())
        {
            doc["url"] = m_url.value();
        }

        return doc.dump();
    }

    std::optional<std::string> InlineQueryResultArticle::get_description() const
    {
        return m_description;
    }

    std::optional<bool> InlineQueryResultArticle::get_hide_url() const
    {
        return m_hide_url;
    }

    std::string InlineQueryResultArticle::get_id() const
    {
        return m_id;
    }

    InputMessageContent::ptr InlineQueryResultArticle::get_input_message_content() const
    {
        return m_input_message_content;
    }

    std::optional<InlineKeyboardMarkup::ptr> InlineQueryResultArticle::get_reply_markup() const
    {
        return m_reply_markup;
    }

    std::optional<std::int32_t> InlineQueryResultArticle::get_thumbnail_height() const
    {
        return m_thumbnail_height;
    }

    std::optional<std::string> InlineQueryResultArticle::get_thumbnail_url() const
    {
        return m_thumbnail_url;
    }

    std::optional<std::int32_t> InlineQueryResultArticle::get_thumbnail_width() const
    {
        return m_thumbnail_width;
    }

    std::string InlineQueryResultArticle::get_title() const
    {
        return m_title;
    }

    std::string InlineQueryResultArticle::get_type() const
    {
        return m_type;
    }

    std::optional<std::string> InlineQueryResultArticle::get_url() const
    {
        return m_url;
    }

    void InlineQueryResultArticle::set_description(const std::optional<std::string> &description)
    {
        m_description = description;
    }

    void InlineQueryResultArticle::set_hide_url(const std::optional<bool> &hide_url)
    {
        m_hide_url = hide_url;
    }

    void InlineQueryResultArticle::set_id(const std::string &id)
    {
        m_id = id;
    }

    void InlineQueryResultArticle::set_input_message_content(const InputMessageContent::ptr &input_message_content)
    {
        m_input_message_content = input_message_content;
    }

    void InlineQueryResultArticle::set_reply_markup(const std::optional<InlineKeyboardMarkup::ptr> &reply_markup)
    {
        m_reply_markup = reply_markup;
    }

    void InlineQueryResultArticle::set_thumbnail_height(const std::optional<std::int32_t> &thumbnail_height)
    {
        m_thumbnail_height = thumbnail_height;
    }

    void InlineQueryResultArticle::set_thumbnail_url(const std::optional<std::string> &thumbnail_url)
    {
        m_thumbnail_url = thumbnail_url;
    }

    void InlineQueryResultArticle::set_thumbnail_width(const std::optional<std::int32_t> &thumbnail_width)
    {
        m_thumbnail_width = thumbnail_width;
    }

    void InlineQueryResultArticle::set_title(const std::string &title)
    {
        m_title = title;
    }

    void InlineQueryResultArticle::set_type(const std::string &type)
    {
        m_type = type;
    }

    void InlineQueryResultArticle::set_url(const std::optional<std::string> &url)
    {
        m_url = url;
    }
} //namespace tgbot
