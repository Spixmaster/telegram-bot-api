#include "tgbot/types/ChatInviteLink.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    ChatInviteLink::ChatInviteLink() :
        m_creates_join_request(false), m_creator(std::make_shared<User>()), m_is_primary(false), m_is_revoked(false)
    {}

    ChatInviteLink::ChatInviteLink(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("creates_join_request"))
            {
                if(doc["creates_join_request"].is_boolean())
                {
                    m_creates_join_request = doc["creates_join_request"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("creates_join_request"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("creates_join_request"));
            }

            if(doc.contains("creator"))
            {
                if(doc["creator"].is_object())
                {
                    m_creator = std::make_shared<User>(doc["creator"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("creator"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("creator"));
            }

            if(doc.contains("expire_date"))
            {
                if(doc["expire_date"].is_number_integer())
                {
                    m_expire_date = doc["expire_date"].get<std::uint64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("expire_date"));
                }
            }

            if(doc.contains("invite_link"))
            {
                if(doc["invite_link"].is_string())
                {
                    m_invite_link = doc["invite_link"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("invite_link"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("invite_link"));
            }

            if(doc.contains("is_primary"))
            {
                if(doc["is_primary"].is_boolean())
                {
                    m_is_primary = doc["is_primary"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("is_primary"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("is_primary"));
            }

            if(doc.contains("is_revoked"))
            {
                if(doc["is_revoked"].is_boolean())
                {
                    m_is_revoked = doc["is_revoked"].get<bool>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_bool("is_revoked"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("is_revoked"));
            }

            if(doc.contains("member_limit"))
            {
                if(doc["member_limit"].is_number_integer())
                {
                    m_member_limit = doc["member_limit"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("member_limit"));
                }
            }

            if(doc.contains("name"))
            {
                if(doc["name"].is_string())
                {
                    m_name = doc["name"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("name"));
                }
            }

            if(doc.contains("pending_join_request_count"))
            {
                if(doc["pending_join_request_count"].is_number_integer())
                {
                    m_pending_join_request_count = doc["pending_join_request_count"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("pending_join_request_count"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string ChatInviteLink::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["creates_join_request"] = m_creates_join_request;
        doc["creator"] = nlohmann::json::parse(m_creator->serialise());

        if(m_expire_date.has_value())
        {
            doc["expire_date"] = m_expire_date.value();
        }

        doc["invite_link"] = m_invite_link;
        doc["is_primary"] = m_is_primary;
        doc["is_revoked"] = m_is_revoked;

        if(m_member_limit.has_value())
        {
            doc["member_limit"] = m_member_limit.value();
        }

        if(m_name.has_value())
        {
            doc["name"] = m_name.value();
        }

        if(m_pending_join_request_count.has_value())
        {
            doc["pending_join_request_count"] = m_pending_join_request_count.value();
        }

        return doc.dump();
    }

    bool ChatInviteLink::get_creates_join_request() const
    {
        return m_creates_join_request;
    }

    User::ptr ChatInviteLink::get_creator() const
    {
        return m_creator;
    }

    std::optional<std::uint64_t> ChatInviteLink::get_expire_date() const
    {
        return m_expire_date;
    }

    std::string ChatInviteLink::get_invite_link() const
    {
        return m_invite_link;
    }

    bool ChatInviteLink::get_is_primary() const
    {
        return m_is_primary;
    }

    bool ChatInviteLink::get_is_revoked() const
    {
        return m_is_revoked;
    }

    std::optional<std::int32_t> ChatInviteLink::get_member_limit() const
    {
        return m_member_limit;
    }

    std::optional<std::string> ChatInviteLink::get_name() const
    {
        return m_name;
    }

    std::optional<std::int32_t> ChatInviteLink::get_pending_join_request_count() const
    {
        return m_pending_join_request_count;
    }

    void ChatInviteLink::set_creates_join_request(const bool &creates_join_request)
    {
        m_creates_join_request = creates_join_request;
    }

    void ChatInviteLink::set_creator(const User::ptr &creator)
    {
        m_creator = creator;
    }

    void ChatInviteLink::set_expire_date(const std::optional<std::uint64_t> &expire_date)
    {
        m_expire_date = expire_date;
    }

    void ChatInviteLink::set_invite_link(const std::string &invite_link)
    {
        m_invite_link = invite_link;
    }

    void ChatInviteLink::set_is_primary(const bool &is_primary)
    {
        m_is_primary = is_primary;
    }

    void ChatInviteLink::set_is_revoked(const bool &is_revoked)
    {
        m_is_revoked = is_revoked;
    }

    void ChatInviteLink::set_member_limit(const std::optional<std::int32_t> &member_limit)
    {
        m_member_limit = member_limit;
    }

    void ChatInviteLink::set_name(const std::optional<std::string> &name)
    {
        m_name = name;
    }

    void ChatInviteLink::set_pending_join_request_count(const std::optional<std::int32_t> &pending_join_request_count)
    {
        m_pending_join_request_count = pending_join_request_count;
    }
} //namespace tgbot
