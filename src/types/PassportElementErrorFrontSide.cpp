#include "tgbot/types/PassportElementErrorFrontSide.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    PassportElementErrorFrontSide::PassportElementErrorFrontSide() = default;

    PassportElementErrorFrontSide::PassportElementErrorFrontSide(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("file_hash"))
            {
                if(doc["file_hash"].is_string())
                {
                    m_file_hash = doc["file_hash"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("file_hash"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("file_hash"));
            }

            if(doc.contains("message"))
            {
                if(doc["message"].is_string())
                {
                    m_message = doc["message"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("message"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("message"));
            }

            if(doc.contains("source"))
            {
                if(doc["source"].is_string())
                {
                    m_source = doc["source"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("source"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("source"));
            }

            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string PassportElementErrorFrontSide::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["file_hash"] = m_file_hash;
        doc["message"] = m_message;
        doc["source"] = m_source;
        doc["type"] = m_type;
        return doc.dump();
    }

    std::string PassportElementErrorFrontSide::get_file_hash() const
    {
        return m_file_hash;
    }

    std::string PassportElementErrorFrontSide::get_message() const
    {
        return m_message;
    }

    std::string PassportElementErrorFrontSide::get_source() const
    {
        return m_source;
    }

    std::string PassportElementErrorFrontSide::get_type() const
    {
        return m_type;
    }

    void PassportElementErrorFrontSide::set_file_hash(const std::string &file_hash)
    {
        m_file_hash = file_hash;
    }

    void PassportElementErrorFrontSide::set_message(const std::string &message)
    {
        m_message = message;
    }

    void PassportElementErrorFrontSide::set_source(const std::string &source)
    {
        m_source = source;
    }

    void PassportElementErrorFrontSide::set_type(const std::string &type)
    {
        m_type = type;
    }
} //namespace tgbot
