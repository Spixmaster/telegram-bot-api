#include "tgbot/types/EncryptedCredentials.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    EncryptedCredentials::EncryptedCredentials() = default;

    EncryptedCredentials::EncryptedCredentials(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("data"))
            {
                if(doc["data"].is_string())
                {
                    m_data = doc["data"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("data"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("data"));
            }

            if(doc.contains("hash"))
            {
                if(doc["hash"].is_string())
                {
                    m_hash = doc["hash"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("hash"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("hash"));
            }

            if(doc.contains("secret"))
            {
                if(doc["secret"].is_string())
                {
                    m_secret = doc["secret"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("secret"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("secret"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string EncryptedCredentials::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["data"] = m_data;
        doc["hash"] = m_hash;
        doc["secret"] = m_secret;
        return doc.dump();
    }

    std::string EncryptedCredentials::get_data() const
    {
        return m_data;
    }

    std::string EncryptedCredentials::get_hash() const
    {
        return m_hash;
    }

    std::string EncryptedCredentials::get_secret() const
    {
        return m_secret;
    }

    void EncryptedCredentials::set_data(const std::string &data)
    {
        m_data = data;
    }

    void EncryptedCredentials::set_hash(const std::string &hash)
    {
        m_hash = hash;
    }

    void EncryptedCredentials::set_secret(const std::string &secret)
    {
        m_secret = secret;
    }
} //namespace tgbot
