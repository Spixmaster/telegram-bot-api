#include "tgbot/types/BotCommand.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    BotCommand::BotCommand() = default;

    BotCommand::BotCommand(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("command"))
            {
                if(doc["command"].is_string())
                {
                    m_command = doc["command"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("command"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("command"));
            }

            if(doc.contains("description"))
            {
                if(doc["description"].is_string())
                {
                    m_description = doc["description"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("description"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("description"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string BotCommand::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["command"] = m_command;
        doc["description"] = m_description;
        return doc.dump();
    }

    std::string BotCommand::get_command() const
    {
        return m_command;
    }

    std::string BotCommand::get_description() const
    {
        return m_description;
    }

    void BotCommand::set_command(const std::string &command)
    {
        m_command = command;
    }

    void BotCommand::set_description(const std::string &description)
    {
        m_description = description;
    }
} //namespace tgbot
