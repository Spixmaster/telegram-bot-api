#include "tgbot/types/ProximityAlertTriggered.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    ProximityAlertTriggered::ProximityAlertTriggered() :
        m_distance(-1), m_traveler(std::make_shared<User>()), m_watcher(std::make_shared<User>())
    {}

    ProximityAlertTriggered::ProximityAlertTriggered(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("distance"))
            {
                if(doc["distance"].is_number_integer())
                {
                    m_distance = doc["distance"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("distance"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("distance"));
            }

            if(doc.contains("traveler"))
            {
                if(doc["traveler"].is_object())
                {
                    m_traveler = std::make_shared<User>(doc["traveler"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("traveler"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("traveler"));
            }

            if(doc.contains("watcher"))
            {
                if(doc["watcher"].is_object())
                {
                    m_watcher = std::make_shared<User>(doc["watcher"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("watcher"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("watcher"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string ProximityAlertTriggered::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["distance"] = m_distance;
        doc["traveler"] = nlohmann::json::parse(m_traveler->serialise());
        doc["watcher"] = nlohmann::json::parse(m_watcher->serialise());
        return doc.dump();
    }

    std::int32_t ProximityAlertTriggered::get_distance() const
    {
        return m_distance;
    }

    User::ptr ProximityAlertTriggered::get_traveler() const
    {
        return m_traveler;
    }

    User::ptr ProximityAlertTriggered::get_watcher() const
    {
        return m_watcher;
    }

    void ProximityAlertTriggered::set_distance(const std::int32_t &distance)
    {
        m_distance = distance;
    }

    void ProximityAlertTriggered::set_traveler(const User::ptr &traveler)
    {
        m_traveler = traveler;
    }

    void ProximityAlertTriggered::set_watcher(const User::ptr &watcher)
    {
        m_watcher = watcher;
    }
} //namespace tgbot
