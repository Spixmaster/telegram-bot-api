#include "tgbot/types/VideoNote.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    VideoNote::VideoNote() : m_duration(-1), m_length(-1)
    {}

    VideoNote::VideoNote(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("duration"))
            {
                if(doc["duration"].is_number_integer())
                {
                    m_duration = doc["duration"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("duration"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("duration"));
            }

            if(doc.contains("file_id"))
            {
                if(doc["file_id"].is_string())
                {
                    m_file_id = doc["file_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("file_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("file_id"));
            }

            if(doc.contains("file_size"))
            {
                if(doc["file_size"].is_number_integer())
                {
                    m_file_size = doc["file_size"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("file_size"));
                }
            }

            if(doc.contains("file_unique_id"))
            {
                if(doc["file_unique_id"].is_string())
                {
                    m_file_unique_id = doc["file_unique_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("file_unique_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("file_unique_id"));
            }

            if(doc.contains("length"))
            {
                if(doc["length"].is_number_integer())
                {
                    m_length = doc["length"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("length"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("length"));
            }

            if(doc.contains("thumbnail"))
            {
                if(doc["thumbnail"].is_object())
                {
                    m_thumbnail = std::make_shared<PhotoSize>(doc["thumbnail"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("thumbnail"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string VideoNote::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["duration"] = m_duration;
        doc["file_id"] = m_file_id;

        if(m_file_size.has_value())
        {
            doc["file_size"] = m_file_size.value();
        }

        doc["file_unique_id"] = m_file_unique_id;
        doc["length"] = m_length;

        if(m_thumbnail.has_value())
        {
            doc["thumbnail"] = nlohmann::json::parse(m_thumbnail.value()->serialise());
        }

        return doc.dump();
    }

    std::int32_t VideoNote::get_duration() const
    {
        return m_duration;
    }

    std::string VideoNote::get_file_id() const
    {
        return m_file_id;
    }

    std::optional<std::int32_t> VideoNote::get_file_size() const
    {
        return m_file_size;
    }

    std::string VideoNote::get_file_unique_id() const
    {
        return m_file_unique_id;
    }

    std::int32_t VideoNote::get_length() const
    {
        return m_length;
    }

    std::optional<PhotoSize::ptr> VideoNote::get_thumbnail() const
    {
        return m_thumbnail;
    }

    void VideoNote::set_duration(const std::int32_t &duration)
    {
        m_duration = duration;
    }

    void VideoNote::set_file_id(const std::string &file_id)
    {
        m_file_id = file_id;
    }

    void VideoNote::set_file_size(const std::optional<std::int32_t> &file_size)
    {
        m_file_size = file_size;
    }

    void VideoNote::set_file_unique_id(const std::string &file_unique_id)
    {
        m_file_unique_id = file_unique_id;
    }

    void VideoNote::set_length(const std::int32_t &length)
    {
        m_length = length;
    }

    void VideoNote::set_thumbnail(const std::optional<PhotoSize::ptr> &thumbnail)
    {
        m_thumbnail = thumbnail;
    }
} //namespace tgbot
