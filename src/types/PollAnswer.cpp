#include "tgbot/types/PollAnswer.h"

#include <algorithm>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>

#include "tgbot/constants/message.h"

namespace tgbot
{
    PollAnswer::PollAnswer() : m_user(std::make_shared<User>())
    {}

    PollAnswer::PollAnswer(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("option_ids"))
            {
                if(doc["option_ids"].is_array())
                {
                    std::vector<std::int32_t> option_ids;
                    option_ids.reserve(doc["option_ids"].size());

                    for(std::uint64_t i = 0; i < doc["option_ids"].size(); ++i)
                    {
                        if(doc["option_ids"].at(i).is_number_integer())
                        {
                            option_ids.emplace_back(doc["option_ids"].at(i).get<std::int32_t>());
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_int("option_ids"));
                        }
                    }

                    m_option_ids = option_ids;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("option_ids"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("option_ids"));
            }

            if(doc.contains("poll_id"))
            {
                if(doc["poll_id"].is_string())
                {
                    m_poll_id = doc["poll_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("poll_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("poll_id"));
            }

            if(doc.contains("user"))
            {
                if(doc["user"].is_object())
                {
                    m_user = std::make_shared<User>(doc["user"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("user"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("user"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string PollAnswer::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        {
            nlohmann::json option_ids = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_option_ids.size(); ++i)
            {
                option_ids.emplace_back(m_option_ids.at(i));
            }

            doc["option_ids"] = option_ids;
        }

        doc["poll_id"] = m_poll_id;
        doc["user"] = nlohmann::json::parse(m_user->serialise());
        return doc.dump();
    }

    std::vector<std::int32_t> PollAnswer::get_option_ids() const
    {
        return m_option_ids;
    }

    std::string PollAnswer::get_poll_id() const
    {
        return m_poll_id;
    }

    User::ptr PollAnswer::get_user() const
    {
        return m_user;
    }

    void PollAnswer::set_option_ids(const std::vector<std::int32_t> &option_ids)
    {
        m_option_ids = option_ids;
    }

    void PollAnswer::set_poll_id(const std::string &poll_id)
    {
        m_poll_id = poll_id;
    }

    void PollAnswer::set_user(const User::ptr &user)
    {
        m_user = user;
    }
} //namespace tgbot
