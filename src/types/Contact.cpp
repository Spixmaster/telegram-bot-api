#include "tgbot/types/Contact.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    Contact::Contact() = default;

    Contact::Contact(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("first_name"))
            {
                if(doc["first_name"].is_string())
                {
                    m_first_name = doc["first_name"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("first_name"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("first_name"));
            }

            if(doc.contains("last_name"))
            {
                if(doc["last_name"].is_string())
                {
                    m_last_name = doc["last_name"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("last_name"));
                }
            }

            if(doc.contains("phone_number"))
            {
                if(doc["phone_number"].is_string())
                {
                    m_phone_number = doc["phone_number"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("phone_number"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("phone_number"));
            }

            if(doc.contains("user_id"))
            {
                if(doc["user_id"].is_number_integer())
                {
                    m_user_id = doc["user_id"].get<std::int64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("user_id"));
                }
            }

            if(doc.contains("vcard"))
            {
                if(doc["vcard"].is_string())
                {
                    m_vcard = doc["vcard"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("vcard"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string Contact::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["first_name"] = m_first_name;

        if(m_last_name.has_value())
        {
            doc["last_name"] = m_last_name.value();
        }

        doc["phone_number"] = m_phone_number;

        if(m_user_id.has_value())
        {
            doc["user_id"] = m_user_id.value();
        }

        if(m_vcard.has_value())
        {
            doc["vcard"] = m_vcard.value();
        }

        return doc.dump();
    }

    std::string Contact::get_first_name() const
    {
        return m_first_name;
    }

    std::optional<std::string> Contact::get_last_name() const
    {
        return m_last_name;
    }

    std::string Contact::get_phone_number() const
    {
        return m_phone_number;
    }

    std::optional<std::int64_t> Contact::get_user_id() const
    {
        return m_user_id;
    }

    std::optional<std::string> Contact::get_vcard() const
    {
        return m_vcard;
    }

    void Contact::set_first_name(const std::string &first_name)
    {
        m_first_name = first_name;
    }

    void Contact::set_last_name(const std::optional<std::string> &last_name)
    {
        m_last_name = last_name;
    }

    void Contact::set_phone_number(const std::string &phone_number)
    {
        m_phone_number = phone_number;
    }

    void Contact::set_user_id(const std::optional<std::int64_t> &user_id)
    {
        m_user_id = user_id;
    }

    void Contact::set_vcard(const std::optional<std::string> &vcard)
    {
        m_vcard = vcard;
    }
} //namespace tgbot
