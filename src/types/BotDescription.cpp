#include "tgbot/types/BotDescription.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    BotDescription::BotDescription() = default;

    BotDescription::BotDescription(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("description"))
            {
                if(doc["description"].is_string())
                {
                    m_description = doc["description"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("description"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("description"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string BotDescription::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["description"] = m_description;
        return doc.dump();
    }

    std::string BotDescription::get_description() const
    {
        return m_description;
    }

    void BotDescription::set_description(const std::string &description)
    {
        m_description = description;
    }
} //namespace tgbot
