#include "tgbot/types/PassportFile.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    PassportFile::PassportFile() : m_file_date(1), m_file_size(-1)
    {}

    PassportFile::PassportFile(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("file_date"))
            {
                if(doc["file_date"].is_number_integer())
                {
                    m_file_date = doc["file_date"].get<std::uint64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("file_date"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("file_date"));
            }

            if(doc.contains("file_id"))
            {
                if(doc["file_id"].is_string())
                {
                    m_file_id = doc["file_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("file_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("file_id"));
            }

            if(doc.contains("file_size"))
            {
                if(doc["file_size"].is_number_integer())
                {
                    m_file_size = doc["file_size"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("file_size"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("file_size"));
            }

            if(doc.contains("file_unique_id"))
            {
                if(doc["file_unique_id"].is_string())
                {
                    m_file_unique_id = doc["file_unique_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("file_unique_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("file_unique_id"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string PassportFile::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["file_date"] = m_file_date;
        doc["file_id"] = m_file_id;
        doc["file_size"] = m_file_size;
        doc["file_unique_id"] = m_file_unique_id;
        return doc.dump();
    }

    std::uint64_t PassportFile::get_file_date() const
    {
        return m_file_date;
    }

    std::string PassportFile::get_file_id() const
    {
        return m_file_id;
    }

    std::int32_t PassportFile::get_file_size() const
    {
        return m_file_size;
    }

    std::string PassportFile::get_file_unique_id() const
    {
        return m_file_unique_id;
    }

    void PassportFile::set_file_date(const std::uint64_t &file_date)
    {
        m_file_date = file_date;
    }

    void PassportFile::set_file_id(const std::string &file_id)
    {
        m_file_id = file_id;
    }

    void PassportFile::set_file_size(const std::int32_t &file_size)
    {
        m_file_size = file_size;
    }

    void PassportFile::set_file_unique_id(const std::string &file_unique_id)
    {
        m_file_unique_id = file_unique_id;
    }
} //namespace tgbot
