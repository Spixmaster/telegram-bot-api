#include "tgbot/types/InlineQueryResultVenue.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    InlineQueryResultVenue::InlineQueryResultVenue() : m_latitude(-1.0), m_longitude(-1.0)
    {}

    InlineQueryResultVenue::InlineQueryResultVenue(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("address"))
            {
                if(doc["address"].is_string())
                {
                    m_address = doc["address"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("address"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("address"));
            }

            if(doc.contains("foursquare_id"))
            {
                if(doc["foursquare_id"].is_string())
                {
                    m_foursquare_id = doc["foursquare_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("foursquare_id"));
                }
            }

            if(doc.contains("foursquare_type"))
            {
                if(doc["foursquare_type"].is_string())
                {
                    m_foursquare_type = doc["foursquare_type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("foursquare_type"));
                }
            }

            if(doc.contains("google_place_id"))
            {
                if(doc["google_place_id"].is_string())
                {
                    m_google_place_id = doc["google_place_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("google_place_id"));
                }
            }

            if(doc.contains("google_place_type"))
            {
                if(doc["google_place_type"].is_string())
                {
                    m_google_place_type = doc["google_place_type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("google_place_type"));
                }
            }

            if(doc.contains("id"))
            {
                if(doc["id"].is_string())
                {
                    m_id = doc["id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("id"));
            }

            if(doc.contains("input_message_content"))
            {
                if(doc["input_message_content"].is_object())
                {
                    m_input_message_content = std::make_shared<InputMessageContent>(doc["input_message_content"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("input_message_content"));
                }
            }

            if(doc.contains("latitude"))
            {
                if(doc["latitude"].is_number_float())
                {
                    m_latitude = doc["latitude"].get<float>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_float("latitude"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("latitude"));
            }

            if(doc.contains("longitude"))
            {
                if(doc["longitude"].is_number_float())
                {
                    m_longitude = doc["longitude"].get<float>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_float("longitude"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("longitude"));
            }

            if(doc.contains("reply_markup"))
            {
                if(doc["reply_markup"].is_object())
                {
                    m_reply_markup = std::make_shared<InlineKeyboardMarkup>(
                      doc["reply_"
                          "marku"
                          "p"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("reply_markup"));
                }
            }

            if(doc.contains("thumbnail_height"))
            {
                if(doc["thumbnail_height"].is_number_integer())
                {
                    m_thumbnail_height = doc["thumbnail_height"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("thumbnail_height"));
                }
            }

            if(doc.contains("thumbnail_url"))
            {
                if(doc["thumbnail_url"].is_string())
                {
                    m_thumbnail_url = doc["thumbnail_url"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("thumbnail_url"));
                }
            }

            if(doc.contains("thumbnail_width"))
            {
                if(doc["thumbnail_width"].is_number_integer())
                {
                    m_thumbnail_width = doc["thumbnail_width"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("thumbnail_width"));
                }
            }

            if(doc.contains("title"))
            {
                if(doc["title"].is_string())
                {
                    m_title = doc["title"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("title"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("title"));
            }

            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string InlineQueryResultVenue::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["address"] = m_address;

        if(m_foursquare_id.has_value())
        {
            doc["foursquare_id"] = m_foursquare_id.value();
        }

        if(m_foursquare_type.has_value())
        {
            doc["foursquare_type"] = m_foursquare_type.value();
        }

        if(m_google_place_id.has_value())
        {
            doc["google_place_id"] = m_google_place_id.value();
        }

        if(m_google_place_type.has_value())
        {
            doc["google_place_type"] = m_google_place_type.value();
        }

        doc["id"] = m_id;

        if(m_input_message_content.has_value())
        {
            doc["input_message_content"] = nlohmann::json::parse(m_input_message_content.value()->serialise());
        }

        doc["latitude"] = m_latitude;
        doc["longitude"] = m_longitude;

        if(m_reply_markup.has_value())
        {
            doc["reply_markup"] = nlohmann::json::parse(m_reply_markup.value()->serialise());
        }

        if(m_thumbnail_height.has_value())
        {
            doc["thumbnail_height"] = m_thumbnail_height.value();
        }

        if(m_thumbnail_url.has_value())
        {
            doc["thumbnail_url"] = m_thumbnail_url.value();
        }

        if(m_thumbnail_width.has_value())
        {
            doc["thumbnail_width"] = m_thumbnail_width.value();
        }

        doc["title"] = m_title;
        doc["type"] = m_type;
        return doc.dump();
    }

    std::string InlineQueryResultVenue::get_address() const
    {
        return m_address;
    }

    std::optional<std::string> InlineQueryResultVenue::get_foursquare_id() const
    {
        return m_foursquare_id;
    }

    std::optional<std::string> InlineQueryResultVenue::get_foursquare_type() const
    {
        return m_foursquare_type;
    }

    std::optional<std::string> InlineQueryResultVenue::get_google_place_id() const
    {
        return m_google_place_id;
    }

    std::optional<std::string> InlineQueryResultVenue::get_google_place_type() const
    {
        return m_google_place_type;
    }

    std::string InlineQueryResultVenue::get_id() const
    {
        return m_id;
    }

    std::optional<InputMessageContent::ptr> InlineQueryResultVenue::get_input_message_content() const
    {
        return m_input_message_content;
    }

    float InlineQueryResultVenue::get_latitude() const
    {
        return m_latitude;
    }

    float InlineQueryResultVenue::get_longitude() const
    {
        return m_longitude;
    }

    std::optional<InlineKeyboardMarkup::ptr> InlineQueryResultVenue::get_reply_markup() const
    {
        return m_reply_markup;
    }

    std::optional<std::int32_t> InlineQueryResultVenue::get_thumbnail_height() const
    {
        return m_thumbnail_height;
    }

    std::optional<std::string> InlineQueryResultVenue::get_thumbnail_url() const
    {
        return m_thumbnail_url;
    }

    std::optional<std::int32_t> InlineQueryResultVenue::get_thumbnail_width() const
    {
        return m_thumbnail_width;
    }

    std::string InlineQueryResultVenue::get_title() const
    {
        return m_title;
    }

    std::string InlineQueryResultVenue::get_type() const
    {
        return m_type;
    }

    void InlineQueryResultVenue::set_address(const std::string &address)
    {
        m_address = address;
    }

    void InlineQueryResultVenue::set_foursquare_id(const std::optional<std::string> &foursquare_id)
    {
        m_foursquare_id = foursquare_id;
    }

    void InlineQueryResultVenue::set_foursquare_type(const std::optional<std::string> &foursquare_type)
    {
        m_foursquare_type = foursquare_type;
    }

    void InlineQueryResultVenue::set_google_place_id(const std::optional<std::string> &google_place_id)
    {
        m_google_place_id = google_place_id;
    }

    void InlineQueryResultVenue::set_google_place_type(const std::optional<std::string> &google_place_type)
    {
        m_google_place_type = google_place_type;
    }

    void InlineQueryResultVenue::set_id(const std::string &id)
    {
        m_id = id;
    }

    void InlineQueryResultVenue::set_input_message_content(
      const std::optional<InputMessageContent::ptr> &input_message_content)
    {
        m_input_message_content = input_message_content;
    }

    void InlineQueryResultVenue::set_latitude(const float &latitude)
    {
        m_latitude = latitude;
    }

    void InlineQueryResultVenue::set_longitude(const float &longitude)
    {
        m_longitude = longitude;
    }

    void InlineQueryResultVenue::set_reply_markup(const std::optional<InlineKeyboardMarkup::ptr> &reply_markup)
    {
        m_reply_markup = reply_markup;
    }

    void InlineQueryResultVenue::set_thumbnail_height(const std::optional<std::int32_t> &thumbnail_height)
    {
        m_thumbnail_height = thumbnail_height;
    }

    void InlineQueryResultVenue::set_thumbnail_url(const std::optional<std::string> &thumbnail_url)
    {
        m_thumbnail_url = thumbnail_url;
    }

    void InlineQueryResultVenue::set_thumbnail_width(const std::optional<std::int32_t> &thumbnail_width)
    {
        m_thumbnail_width = thumbnail_width;
    }

    void InlineQueryResultVenue::set_title(const std::string &title)
    {
        m_title = title;
    }

    void InlineQueryResultVenue::set_type(const std::string &type)
    {
        m_type = type;
    }
} //namespace tgbot
