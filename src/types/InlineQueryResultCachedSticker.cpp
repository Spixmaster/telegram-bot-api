#include "tgbot/types/InlineQueryResultCachedSticker.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"
#include "tgbot/types/InlineKeyboardMarkup.h"
#include "tgbot/types/InputMessageContent.h"

namespace tgbot
{
    InlineQueryResultCachedSticker::InlineQueryResultCachedSticker() = default;

    InlineQueryResultCachedSticker::InlineQueryResultCachedSticker(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("id"))
            {
                if(doc["id"].is_string())
                {
                    m_id = doc["id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("id"));
            }

            if(doc.contains("input_message_content"))
            {
                if(doc["input_message_content"].is_object())
                {
                    m_input_message_content = std::make_shared<InputMessageContent>(doc["input_message_content"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("input_message_content"));
                }
            }

            if(doc.contains("reply_markup"))
            {
                if(doc["reply_markup"].is_object())
                {
                    m_reply_markup = std::make_shared<InlineKeyboardMarkup>(
                      doc["reply_"
                          "marku"
                          "p"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("reply_markup"));
                }
            }

            if(doc.contains("sticker_file_id"))
            {
                if(doc["sticker_file_id"].is_string())
                {
                    m_sticker_file_id = doc["sticker_file_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("sticker_file_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("sticker_file_id"));
            }

            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string InlineQueryResultCachedSticker::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["id"] = m_id;

        if(m_input_message_content.has_value())
        {
            doc["input_message_content"] = nlohmann::json::parse(m_input_message_content.value()->serialise());
        }

        if(m_reply_markup.has_value())
        {
            doc["reply_markup"] = nlohmann::json::parse(m_reply_markup.value()->serialise());
        }

        doc["sticker_file_id"] = m_sticker_file_id;
        doc["type"] = m_type;
        return doc.dump();
    }

    std::string InlineQueryResultCachedSticker::get_id() const
    {
        return m_id;
    }

    std::optional<InputMessageContent::ptr> InlineQueryResultCachedSticker::get_input_message_content() const
    {
        return m_input_message_content;
    }

    std::optional<InlineKeyboardMarkup::ptr> InlineQueryResultCachedSticker::get_reply_markup() const
    {
        return m_reply_markup;
    }

    std::string InlineQueryResultCachedSticker::get_sticker_file_id() const
    {
        return m_sticker_file_id;
    }

    std::string InlineQueryResultCachedSticker::get_type() const
    {
        return m_type;
    }

    void InlineQueryResultCachedSticker::set_id(const std::string &id)
    {
        m_id = id;
    }

    void InlineQueryResultCachedSticker::set_input_message_content(
      const std::optional<InputMessageContent::ptr> &input_message_content)
    {
        m_input_message_content = input_message_content;
    }

    void InlineQueryResultCachedSticker::set_reply_markup(const std::optional<InlineKeyboardMarkup::ptr> &reply_markup)
    {
        m_reply_markup = reply_markup;
    }

    void InlineQueryResultCachedSticker::set_sticker_file_id(const std::string &sticker_file_id)
    {
        m_sticker_file_id = sticker_file_id;
    }

    void InlineQueryResultCachedSticker::set_type(const std::string &type)
    {
        m_type = type;
    }
} //namespace tgbot
