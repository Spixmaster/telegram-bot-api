#include "tgbot/types/WebAppData.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    WebAppData::WebAppData() = default;

    WebAppData::WebAppData(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("button_text"))
            {
                if(doc["button_text"].is_string())
                {
                    m_button_text = doc["button_text"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("button_text"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("button_text"));
            }

            if(doc.contains("data"))
            {
                if(doc["data"].is_string())
                {
                    m_data = doc["data"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("data"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("data"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string WebAppData::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["button_text"] = m_button_text;
        doc["data"] = m_data;
        return doc.dump();
    }

    std::string WebAppData::get_button_text() const
    {
        return m_button_text;
    }

    std::string WebAppData::get_data() const
    {
        return m_data;
    }

    void WebAppData::set_button_text(const std::string &button_text)
    {
        m_button_text = button_text;
    }

    void WebAppData::set_data(const std::string &data)
    {
        m_data = data;
    }
} //namespace tgbot
