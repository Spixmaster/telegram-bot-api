#include "tgbot/types/InlineQueryResultMpeg4Gif.h"

#include <algorithm>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "tgbot/constants/message.h"

namespace tgbot
{
    InlineQueryResultMpeg4Gif::InlineQueryResultMpeg4Gif() = default;

    InlineQueryResultMpeg4Gif::InlineQueryResultMpeg4Gif(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("caption"))
            {
                if(doc["caption"].is_string())
                {
                    m_caption = doc["caption"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("caption"));
                }
            }

            if(doc.contains("caption_entities"))
            {
                if(doc["caption_entities"].is_array())
                {
                    std::vector<MessageEntity::ptr> caption_entities;
                    caption_entities.reserve(doc["caption_entities"].size());

                    for(std::uint64_t i = 0; i < doc["caption_entities"].size(); ++i)
                    {
                        if(doc["caption_entities"].at(i).is_object())
                        {
                            caption_entities.emplace_back(
                              std::make_shared<MessageEntity>(doc["caption_entities"].at(i).dump()));
                        }
                        else
                        {
                            throw std::invalid_argument(message::json::value::nested::not_object(
                              "caption_"
                              "entitie"
                              "s"));
                        }
                    }

                    m_caption_entities = caption_entities;
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_array("caption_entities"));
                }
            }

            if(doc.contains("id"))
            {
                if(doc["id"].is_string())
                {
                    m_id = doc["id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("id"));
            }

            if(doc.contains("input_message_content"))
            {
                if(doc["input_message_content"].is_object())
                {
                    m_input_message_content = std::make_shared<InputMessageContent>(doc["input_message_content"].dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("input_message_content"));
                }
            }

            if(doc.contains("mpeg4_duration"))
            {
                if(doc["mpeg4_duration"].is_number_integer())
                {
                    m_mpeg4_duration = doc["mpeg4_duration"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("mpeg4_duration"));
                }
            }

            if(doc.contains("mpeg4_height"))
            {
                if(doc["mpeg4_height"].is_number_integer())
                {
                    m_mpeg4_height = doc["mpeg4_height"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("mpeg4_height"));
                }
            }

            if(doc.contains("mpeg4_url"))
            {
                if(doc["mpeg4_url"].is_string())
                {
                    m_mpeg4_url = doc["mpeg4_url"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("mpeg4_url"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("mpeg4_url"));
            }

            if(doc.contains("mpeg4_width"))
            {
                if(doc["mpeg4_width"].is_number_integer())
                {
                    m_mpeg4_width = doc["mpeg4_width"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("mpeg4_width"));
                }
            }

            if(doc.contains("parse_mode"))
            {
                if(doc["parse_mode"].is_string())
                {
                    m_parse_mode = doc["parse_mode"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("parse_mode"));
                }
            }

            if(doc.contains("reply_markup"))
            {
                if(doc["reply_markup"].is_object())
                {
                    m_reply_markup = std::make_shared<InlineKeyboardMarkup>(
                      doc["reply_"
                          "marku"
                          "p"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("reply_markup"));
                }
            }

            if(doc.contains("thumbnail_mime_type"))
            {
                if(doc["thumbnail_mime_type"].is_string())
                {
                    m_thumbnail_mime_type = doc["thumbnail_mime_type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("thumbnail_mime_type"));
                }
            }

            if(doc.contains("thumbnail_url"))
            {
                if(doc["thumbnail_url"].is_string())
                {
                    m_thumbnail_url = doc["thumbnail_url"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("thumbnail_url"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("thumbnail_url"));
            }

            if(doc.contains("title"))
            {
                if(doc["title"].is_string())
                {
                    m_title = doc["title"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("title"));
                }
            }

            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string InlineQueryResultMpeg4Gif::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();

        if(m_caption.has_value())
        {
            doc["caption"] = m_caption.value();
        }

        if(m_caption_entities.has_value())
        {
            nlohmann::json caption_entities = nlohmann::json::array();

            for(std::uint64_t i = 0; i < m_caption_entities.value().size(); ++i)
            {
                caption_entities.emplace_back(nlohmann::json::parse(m_caption_entities.value().at(i)->serialise()));
            }

            doc["caption_entities"] = caption_entities;
        }

        doc["id"] = m_id;

        if(m_input_message_content.has_value())
        {
            doc["input_message_content"] = nlohmann::json::parse(m_input_message_content.value()->serialise());
        }

        if(m_mpeg4_duration.has_value())
        {
            doc["mpeg4_duration"] = m_mpeg4_duration.value();
        }

        if(m_mpeg4_height.has_value())
        {
            doc["mpeg4_height"] = m_mpeg4_height.value();
        }

        doc["mpeg4_url"] = m_mpeg4_url;

        if(m_mpeg4_width.has_value())
        {
            doc["mpeg4_width"] = m_mpeg4_width.value();
        }

        if(m_parse_mode.has_value())
        {
            doc["parse_mode"] = m_parse_mode.value();
        }

        if(m_reply_markup.has_value())
        {
            doc["reply_markup"] = nlohmann::json::parse(m_reply_markup.value()->serialise());
        }

        if(m_thumbnail_mime_type.has_value())
        {
            doc["thumbnail_mime_type"] = m_thumbnail_mime_type.value();
        }

        doc["thumbnail_url"] = m_thumbnail_url;

        if(m_title.has_value())
        {
            doc["title"] = m_title.value();
        }

        doc["type"] = m_type;
        return doc.dump();
    }

    std::optional<std::string> InlineQueryResultMpeg4Gif::get_caption() const
    {
        return m_caption;
    }

    std::optional<std::vector<MessageEntity::ptr>> InlineQueryResultMpeg4Gif::get_caption_entities() const
    {
        return m_caption_entities;
    }

    std::string InlineQueryResultMpeg4Gif::get_id() const
    {
        return m_id;
    }

    std::optional<InputMessageContent::ptr> InlineQueryResultMpeg4Gif::get_input_message_content() const
    {
        return m_input_message_content;
    }

    std::optional<std::int32_t> InlineQueryResultMpeg4Gif::get_mpeg4_duration() const
    {
        return m_mpeg4_duration;
    }

    std::optional<std::int32_t> InlineQueryResultMpeg4Gif::get_mpeg4_height() const
    {
        return m_mpeg4_height;
    }

    std::string InlineQueryResultMpeg4Gif::get_mpeg4_url() const
    {
        return m_mpeg4_url;
    }

    std::optional<std::int32_t> InlineQueryResultMpeg4Gif::get_mpeg4_width() const
    {
        return m_mpeg4_width;
    }

    std::optional<std::string> InlineQueryResultMpeg4Gif::get_parse_mode() const
    {
        return m_parse_mode;
    }

    std::optional<InlineKeyboardMarkup::ptr> InlineQueryResultMpeg4Gif::get_reply_markup() const
    {
        return m_reply_markup;
    }

    std::optional<std::string> InlineQueryResultMpeg4Gif::get_thumbnail_mime_type() const
    {
        return m_thumbnail_mime_type;
    }

    std::string InlineQueryResultMpeg4Gif::get_thumbnail_url() const
    {
        return m_thumbnail_url;
    }

    std::optional<std::string> InlineQueryResultMpeg4Gif::get_title() const
    {
        return m_title;
    }

    std::string InlineQueryResultMpeg4Gif::get_type() const
    {
        return m_type;
    }

    void InlineQueryResultMpeg4Gif::set_caption(const std::optional<std::string> &caption)
    {
        m_caption = caption;
    }

    void InlineQueryResultMpeg4Gif::set_caption_entities(
      const std::optional<std::vector<MessageEntity::ptr>> &caption_entities)
    {
        m_caption_entities = caption_entities;
    }

    void InlineQueryResultMpeg4Gif::set_id(const std::string &id)
    {
        m_id = id;
    }

    void InlineQueryResultMpeg4Gif::set_input_message_content(
      const std::optional<InputMessageContent::ptr> &input_message_content)
    {
        m_input_message_content = input_message_content;
    }

    void InlineQueryResultMpeg4Gif::set_mpeg4_duration(const std::optional<std::int32_t> &mpeg4_duration)
    {
        m_mpeg4_duration = mpeg4_duration;
    }

    void InlineQueryResultMpeg4Gif::set_mpeg4_height(const std::optional<std::int32_t> &mpeg4_height)
    {
        m_mpeg4_height = mpeg4_height;
    }

    void InlineQueryResultMpeg4Gif::set_mpeg4_url(const std::string &mpeg4_url)
    {
        m_mpeg4_url = mpeg4_url;
    }

    void InlineQueryResultMpeg4Gif::set_mpeg4_width(const std::optional<std::int32_t> &mpeg4_width)
    {
        m_mpeg4_width = mpeg4_width;
    }

    void InlineQueryResultMpeg4Gif::set_parse_mode(const std::optional<std::string> &parse_mode)
    {
        m_parse_mode = parse_mode;
    }

    void InlineQueryResultMpeg4Gif::set_reply_markup(const std::optional<InlineKeyboardMarkup::ptr> &reply_markup)
    {
        m_reply_markup = reply_markup;
    }

    void InlineQueryResultMpeg4Gif::set_thumbnail_mime_type(const std::optional<std::string> &thumbnail_mime_type)
    {
        m_thumbnail_mime_type = thumbnail_mime_type;
    }

    void InlineQueryResultMpeg4Gif::set_thumbnail_url(const std::string &thumbnail_url)
    {
        m_thumbnail_url = thumbnail_url;
    }

    void InlineQueryResultMpeg4Gif::set_title(const std::optional<std::string> &title)
    {
        m_title = title;
    }

    void InlineQueryResultMpeg4Gif::set_type(const std::string &type)
    {
        m_type = type;
    }
} //namespace tgbot
