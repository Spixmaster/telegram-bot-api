#include "tgbot/types/MaskPosition.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <optional>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    MaskPosition::MaskPosition() : m_scale(-1.0), m_x_shift(-1.0), m_y_shift(-1.0)
    {}

    MaskPosition::MaskPosition(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("point"))
            {
                if(doc["point"].is_string())
                {
                    m_point = doc["point"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("point"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("point"));
            }

            if(doc.contains("scale"))
            {
                if(doc["scale"].is_number_float())
                {
                    m_scale = doc["scale"].get<float>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_float("scale"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("scale"));
            }

            if(doc.contains("x_shift"))
            {
                if(doc["x_shift"].is_number_float())
                {
                    m_x_shift = doc["x_shift"].get<float>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_float("x_shift"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("x_shift"));
            }

            if(doc.contains("y_shift"))
            {
                if(doc["y_shift"].is_number_float())
                {
                    m_y_shift = doc["y_shift"].get<float>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_float("y_shift"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("y_shift"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string MaskPosition::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["point"] = m_point;
        doc["scale"] = m_scale;
        doc["x_shift"] = m_x_shift;
        doc["y_shift"] = m_y_shift;
        return doc.dump();
    }

    std::string MaskPosition::get_point() const
    {
        return m_point;
    }

    float MaskPosition::get_scale() const
    {
        return m_scale;
    }

    float MaskPosition::get_x_shift() const
    {
        return m_x_shift;
    }

    float MaskPosition::get_y_shift() const
    {
        return m_y_shift;
    }

    void MaskPosition::set_point(const std::string &point)
    {
        m_point = point;
    }

    void MaskPosition::set_scale(const float &scale)
    {
        m_scale = scale;
    }

    void MaskPosition::set_x_shift(const float &x_shift)
    {
        m_x_shift = x_shift;
    }

    void MaskPosition::set_y_shift(const float &y_shift)
    {
        m_y_shift = y_shift;
    }
} //namespace tgbot
