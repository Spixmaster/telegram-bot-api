#include "tgbot/types/InlineQueryResultGame.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    InlineQueryResultGame::InlineQueryResultGame() = default;

    InlineQueryResultGame::InlineQueryResultGame(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("game_short_name"))
            {
                if(doc["game_short_name"].is_string())
                {
                    m_game_short_name = doc["game_short_name"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("game_short_name"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("game_short_name"));
            }

            if(doc.contains("id"))
            {
                if(doc["id"].is_string())
                {
                    m_id = doc["id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("id"));
            }

            if(doc.contains("reply_markup"))
            {
                if(doc["reply_markup"].is_object())
                {
                    m_reply_markup = std::make_shared<InlineKeyboardMarkup>(
                      doc["reply_"
                          "marku"
                          "p"]
                        .dump());
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_object("reply_markup"));
                }
            }

            if(doc.contains("type"))
            {
                if(doc["type"].is_string())
                {
                    m_type = doc["type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("type"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("type"));
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string InlineQueryResultGame::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["game_short_name"] = m_game_short_name;
        doc["id"] = m_id;

        if(m_reply_markup.has_value())
        {
            doc["reply_markup"] = nlohmann::json::parse(m_reply_markup.value()->serialise());
        }

        doc["type"] = m_type;
        return doc.dump();
    }

    std::string InlineQueryResultGame::get_game_short_name() const
    {
        return m_game_short_name;
    }

    std::string InlineQueryResultGame::get_id() const
    {
        return m_id;
    }

    std::optional<InlineKeyboardMarkup::ptr> InlineQueryResultGame::get_reply_markup() const
    {
        return m_reply_markup;
    }

    std::string InlineQueryResultGame::get_type() const
    {
        return m_type;
    }

    void InlineQueryResultGame::set_game_short_name(const std::string &game_short_name)
    {
        m_game_short_name = game_short_name;
    }

    void InlineQueryResultGame::set_id(const std::string &id)
    {
        m_id = id;
    }

    void InlineQueryResultGame::set_reply_markup(const std::optional<InlineKeyboardMarkup::ptr> &reply_markup)
    {
        m_reply_markup = reply_markup;
    }

    void InlineQueryResultGame::set_type(const std::string &type)
    {
        m_type = type;
    }
} //namespace tgbot
