#include "tgbot/types/Voice.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "tgbot/constants/message.h"

namespace tgbot
{
    Voice::Voice() : m_duration(-1)
    {}

    Voice::Voice(const std::string &json)
    {
        const nlohmann::json doc = nlohmann::json::parse(json);

        if(doc.is_object())
        {
            if(doc.contains("duration"))
            {
                if(doc["duration"].is_number_integer())
                {
                    m_duration = doc["duration"].get<std::int32_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("duration"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("duration"));
            }

            if(doc.contains("file_id"))
            {
                if(doc["file_id"].is_string())
                {
                    m_file_id = doc["file_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("file_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("file_id"));
            }

            if(doc.contains("file_size"))
            {
                if(doc["file_size"].is_number_integer())
                {
                    m_file_size = doc["file_size"].get<std::int64_t>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_int("file_size"));
                }
            }

            if(doc.contains("file_unique_id"))
            {
                if(doc["file_unique_id"].is_string())
                {
                    m_file_unique_id = doc["file_unique_id"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("file_unique_id"));
                }
            }
            else
            {
                throw std::invalid_argument(message::json::key_non_existent("file_unique_id"));
            }

            if(doc.contains("mime_type"))
            {
                if(doc["mime_type"].is_string())
                {
                    m_mime_type = doc["mime_type"].get<std::string>();
                }
                else
                {
                    throw std::invalid_argument(message::json::value::not_string("mime_type"));
                }
            }
        }
        else
        {
            throw std::invalid_argument(message::json::value::not_object(std::nullopt));
        }
    }

    std::string Voice::serialise() const
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["duration"] = m_duration;
        doc["file_id"] = m_file_id;

        if(m_file_size.has_value())
        {
            doc["file_size"] = m_file_size.value();
        }

        doc["file_unique_id"] = m_file_unique_id;

        if(m_mime_type.has_value())
        {
            doc["mime_type"] = m_mime_type.value();
        }

        return doc.dump();
    }

    std::int32_t Voice::get_duration() const
    {
        return m_duration;
    }

    std::string Voice::get_file_id() const
    {
        return m_file_id;
    }

    std::optional<std::int64_t> Voice::get_file_size() const
    {
        return m_file_size;
    }

    std::string Voice::get_file_unique_id() const
    {
        return m_file_unique_id;
    }

    std::optional<std::string> Voice::get_mime_type() const
    {
        return m_mime_type;
    }

    void Voice::set_duration(const std::int32_t &duration)
    {
        m_duration = duration;
    }

    void Voice::set_file_id(const std::string &file_id)
    {
        m_file_id = file_id;
    }

    void Voice::set_file_size(const std::optional<std::int64_t> &file_size)
    {
        m_file_size = file_size;
    }

    void Voice::set_file_unique_id(const std::string &file_unique_id)
    {
        m_file_unique_id = file_unique_id;
    }

    void Voice::set_mime_type(const std::optional<std::string> &mime_type)
    {
        m_mime_type = mime_type;
    }
} //namespace tgbot
