#include "tgbot/Bot.h"

#include "gtest/gtest.h"

TEST(Bot, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt);
    ASSERT_NE(bot.get_endpoints(), nullptr);
    ASSERT_NE(bot.get_event_handler(), nullptr);
}
