#include "tgbot/EventHandler.h"

#include <exception>
#include <stdexcept>

#include "gtest/gtest.h"
#include "tgbot/Bot.h"

TEST(EventHandlergetendpoints, normal)
{
    const tgbot::EventHandler event_handler("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NE(event_handler.get_endpoints(), nullptr);
}

TEST(EventHandlerhandleupdate, normal)
{
    tgbot::EventHandler event_handler("localhost", 5286, "token", std::nullopt, false);

    event_handler.on_any_message(
      [](const tgbot::Bot::ptr &, const tgbot::Message::ptr &) -> void
      {
      });
    ASSERT_EQ(event_handler.get_any_message_listeners().size(), 1);

    event_handler.on_callback_query(
      [](const tgbot::Bot::ptr &, const tgbot::CallbackQuery::ptr &) -> void
      {
      });
    ASSERT_EQ(event_handler.get_callback_query_listeners().size(), 1);

    event_handler.on_channel_post(
      [](const tgbot::Bot::ptr &, const tgbot::Message::ptr &) -> void
      {
      });
    ASSERT_EQ(event_handler.get_channel_post_listeners().size(), 1);

    event_handler.on_chat_join_request(
      [](const tgbot::Bot::ptr &, const tgbot::ChatJoinRequest::ptr &) -> void
      {
      });
    ASSERT_EQ(event_handler.get_chat_join_request_listeners().size(), 1);

    event_handler.on_chat_member(
      [](const tgbot::Bot::ptr &, const tgbot::ChatMemberUpdated::ptr &) -> void
      {
      });
    ASSERT_EQ(event_handler.get_chat_member_listeners().size(), 1);

    event_handler.on_chosen_inline_result(
      [](const tgbot::Bot::ptr &, const tgbot::ChosenInlineResult::ptr &) -> void
      {
      });
    ASSERT_EQ(event_handler.get_chosen_inline_result_listeners().size(), 1);

    event_handler.on_command(
      "known-command",
      [](const tgbot::Bot::ptr &, const tgbot::Message::ptr &) -> void
      {
      });
    ASSERT_EQ(event_handler.get_command_listeners().size(), 1);

    event_handler.on_edited_channel_post(
      [](const tgbot::Bot::ptr &, const tgbot::Message::ptr &) -> void
      {
      });
    ASSERT_EQ(event_handler.get_edited_channel_post_listeners().size(), 1);

    event_handler.on_edited_message(
      [](const tgbot::Bot::ptr &, const tgbot::Message::ptr &) -> void
      {
      });
    ASSERT_EQ(event_handler.get_edited_message_listeners().size(), 1);

    event_handler.on_inline_query(
      [](const tgbot::Bot::ptr &, const tgbot::InlineQuery::ptr &) -> void
      {
      });
    ASSERT_EQ(event_handler.get_inline_query_listeners().size(), 1);

    event_handler.on_message(
      [](const tgbot::Bot::ptr &, const tgbot::Message::ptr &) -> void
      {
      });
    ASSERT_EQ(event_handler.get_message_listeners().size(), 1);

    event_handler.on_my_chat_member(
      [](const tgbot::Bot::ptr &, const tgbot::ChatMemberUpdated::ptr &) -> void
      {
      });
    ASSERT_EQ(event_handler.get_my_chat_member_listeners().size(), 1);

    event_handler.on_non_command_message(
      [](const tgbot::Bot::ptr &, const tgbot::Message::ptr &) -> void
      {
      });
    ASSERT_EQ(event_handler.get_non_command_message_listeners().size(), 1);

    event_handler.on_poll(
      [](const tgbot::Bot::ptr &, const tgbot::Poll::ptr &) -> void
      {
      });
    ASSERT_EQ(event_handler.get_poll_listeners().size(), 1);

    event_handler.on_poll_answer(
      [](const tgbot::Bot::ptr &, const tgbot::PollAnswer::ptr &) -> void
      {
      });
    ASSERT_EQ(event_handler.get_poll_answer_listeners().size(), 1);

    event_handler.on_pre_checkout_query(
      [](const tgbot::Bot::ptr &, const tgbot::PreCheckoutQuery::ptr &) -> void
      {
      });
    ASSERT_EQ(event_handler.get_pre_checkout_query_listeners().size(), 1);

    event_handler.on_shipping_query(
      [](const tgbot::Bot::ptr &, const tgbot::ShippingQuery::ptr &) -> void
      {
      });
    ASSERT_EQ(event_handler.get_shipping_query_listeners().size(), 1);

    event_handler.on_unknown_command(
      [](const tgbot::Bot::ptr &, const tgbot::Message::ptr &) -> void
      {
      });
    ASSERT_EQ(event_handler.get_unknown_command_listeners().size(), 1);

    {
        const tgbot::Update::ptr update = std::make_shared<tgbot::Update>();
        update->set_callback_query(std::make_shared<tgbot::CallbackQuery>());
        ASSERT_NO_THROW(event_handler.handle_update(
          std::make_shared<tgbot::Bot>("localhost", 5286, "token", std::nullopt, false), update));
    }

    {
        const tgbot::Update::ptr update = std::make_shared<tgbot::Update>();
        update->set_channel_post(std::make_shared<tgbot::Message>());
        ASSERT_NO_THROW(event_handler.handle_update(
          std::make_shared<tgbot::Bot>("localhost", 5286, "token", std::nullopt, false), update));
    }

    {
        const tgbot::Update::ptr update = std::make_shared<tgbot::Update>();
        update->set_chat_join_request(std::make_shared<tgbot::ChatJoinRequest>());
        ASSERT_NO_THROW(event_handler.handle_update(
          std::make_shared<tgbot::Bot>("localhost", 5286, "token", std::nullopt, false), update));
    }

    {
        const tgbot::Update::ptr update = std::make_shared<tgbot::Update>();
        update->set_chat_member(std::make_shared<tgbot::ChatMemberUpdated>());
        ASSERT_NO_THROW(event_handler.handle_update(
          std::make_shared<tgbot::Bot>("localhost", 5286, "token", std::nullopt, false), update));
    }

    {
        const tgbot::Update::ptr update = std::make_shared<tgbot::Update>();
        update->set_chosen_inline_result(std::make_shared<tgbot::ChosenInlineResult>());
        ASSERT_NO_THROW(event_handler.handle_update(
          std::make_shared<tgbot::Bot>("localhost", 5286, "token", std::nullopt, false), update));
    }

    {
        const tgbot::Update::ptr update = std::make_shared<tgbot::Update>();
        update->set_edited_channel_post(std::make_shared<tgbot::Message>());
        ASSERT_NO_THROW(event_handler.handle_update(
          std::make_shared<tgbot::Bot>("localhost", 5286, "token", std::nullopt, false), update));
    }

    {
        const tgbot::Update::ptr update = std::make_shared<tgbot::Update>();
        update->set_edited_message(std::make_shared<tgbot::Message>());
        ASSERT_NO_THROW(event_handler.handle_update(
          std::make_shared<tgbot::Bot>("localhost", 5286, "token", std::nullopt, false), update));
    }

    {
        const tgbot::Update::ptr update = std::make_shared<tgbot::Update>();
        update->set_inline_query(std::make_shared<tgbot::InlineQuery>());
        ASSERT_NO_THROW(event_handler.handle_update(
          std::make_shared<tgbot::Bot>("localhost", 5286, "token", std::nullopt, false), update));
    }

    {
        const tgbot::Update::ptr update = std::make_shared<tgbot::Update>();
        update->set_message(std::make_shared<tgbot::Message>());
        ASSERT_NO_THROW(event_handler.handle_update(
          std::make_shared<tgbot::Bot>("localhost", 5286, "token", std::nullopt, false), update));
    }

    {
        const tgbot::Update::ptr update = std::make_shared<tgbot::Update>();
        update->set_my_chat_member(std::make_shared<tgbot::ChatMemberUpdated>());
        ASSERT_NO_THROW(event_handler.handle_update(
          std::make_shared<tgbot::Bot>("localhost", 5286, "token", std::nullopt, false), update));
    }

    {
        const tgbot::Update::ptr update = std::make_shared<tgbot::Update>();
        update->set_poll(std::make_shared<tgbot::Poll>());
        ASSERT_NO_THROW(event_handler.handle_update(
          std::make_shared<tgbot::Bot>("localhost", 5286, "token", std::nullopt, false), update));
    }

    {
        const tgbot::Update::ptr update = std::make_shared<tgbot::Update>();
        update->set_poll_answer(std::make_shared<tgbot::PollAnswer>());
        ASSERT_NO_THROW(event_handler.handle_update(
          std::make_shared<tgbot::Bot>("localhost", 5286, "token", std::nullopt, false), update));
    }

    {
        const tgbot::Update::ptr update = std::make_shared<tgbot::Update>();
        update->set_pre_checkout_query(std::make_shared<tgbot::PreCheckoutQuery>());
        ASSERT_NO_THROW(event_handler.handle_update(
          std::make_shared<tgbot::Bot>("localhost", 5286, "token", std::nullopt, false), update));
    }

    {
        const tgbot::Update::ptr update = std::make_shared<tgbot::Update>();
        update->set_shipping_query(std::make_shared<tgbot::ShippingQuery>());
        ASSERT_NO_THROW(event_handler.handle_update(
          std::make_shared<tgbot::Bot>("localhost", 5286, "token", std::nullopt, false), update));
    }

    {
        const tgbot::Update::ptr update = std::make_shared<tgbot::Update>();
        tgbot::Message::ptr message = std::make_shared<tgbot::Message>();
        message->set_text("/known-command");
        update->set_message(message);
        ASSERT_NO_THROW(event_handler.handle_update(
          std::make_shared<tgbot::Bot>("localhost", 5286, "token", std::nullopt, false), update));
    }

    {
        const tgbot::Update::ptr update = std::make_shared<tgbot::Update>();
        tgbot::Message::ptr message = std::make_shared<tgbot::Message>();
        message->set_text(std::string("/unknown-command"));
        update->set_message(message);
        ASSERT_NO_THROW(event_handler.handle_update(
          std::make_shared<tgbot::Bot>("localhost", 5286, "token", std::nullopt, false), update));
    }
}

TEST(EventHandlerlongpoll, normal)
{
    tgbot::EventHandler event_handler("error-domain", 5286, "token", std::nullopt);
    ASSERT_NO_THROW(
      event_handler.long_poll(std::make_shared<tgbot::Bot>("localhost", 5286, "token", std::nullopt, false)));
}

TEST(EventHandlerlongpoll, errors)
{
    tgbot::EventHandler event_handler("localhost", 5286, "token", std::nullopt, false);
    event_handler.on_message(
      [](const tgbot::Bot::ptr &, const tgbot::Message::ptr &) -> void
      {
          throw std::runtime_error("");
      });
    ASSERT_THROW(event_handler.long_poll(std::make_shared<tgbot::Bot>("localhost", 5286, "token", std::nullopt, false)),
                 std::exception);
}
