#include "tgbot/types/InputTextMessageContent.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InputTextMessageContent, normal)
{
    ASSERT_NO_THROW(tgbot::InputTextMessageContent(tgbot::InputTextMessageContent().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["disable_web_page_preview"] = true;
    doc["entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())};
    doc["message_text"] = "iojwdf";
    doc["parse_mode"] = "oinxa";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InputTextMessageContent(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InputTextMessageContent input_text_message_content;

        input_text_message_content.set_disable_web_page_preview(true);
        ASSERT_EQ(input_text_message_content.get_disable_web_page_preview(), true);

        input_text_message_content.set_entities(std::vector<tgbot::MessageEntity::ptr> {
          std::make_shared<tgbot::MessageEntity>()});

        if(input_text_message_content.get_entities().has_value())
        {
            ASSERT_EQ(
              input_text_message_content.get_entities().value().at(0)->serialise(), tgbot::MessageEntity().serialise());
        }

        input_text_message_content.set_message_text("iojwdf");
        ASSERT_EQ(input_text_message_content.get_message_text(), "iojwdf");

        input_text_message_content.set_parse_mode("oinxa");
        ASSERT_EQ(input_text_message_content.get_parse_mode(), "oinxa");

        ASSERT_EQ(input_text_message_content.serialise(), serialised_doc);
    }
}

TEST(InputTextMessageContent, errors)
{
    ASSERT_THROW(tgbot::InputTextMessageContent(nlohmann::json::array().dump()), std::invalid_argument);

    {
        const nlohmann::json doc = nlohmann::json::object();

        ASSERT_THROW(tgbot::InputTextMessageContent(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["disable_web_page_preview"] = false : doc["disable_web_page_preview"] = 7187;
        i != 1 ? doc["entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())} : doc["entities"] = 486;
        i != 2 ? doc["message_text"] = "aoisjgldfgs" : doc["message_text"] = 286;
        i != 3 ? doc["parse_mode"] = "owihf" : doc["parse_mode"] = 65;

        ASSERT_THROW(tgbot::InputTextMessageContent(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["entities"] = {146824};
        doc["message_text"] = "aoisjgldfgs";

        ASSERT_THROW(tgbot::InputTextMessageContent(doc.dump()), std::invalid_argument);
    }
}
