#include "tgbot/types/BotCommandScopeChatMember.h"

#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(BotCommandScopeChatMember, normal)
{
    ASSERT_NO_THROW(tgbot::BotCommandScopeChatMember(tgbot::BotCommandScopeChatMember().serialise()));

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i == 0 ? doc["chat_id"] = 984254 : doc["chat_id"] = "thofijpsiot";
        doc["type"] = "fcqopihfdg";
        doc["user_id"] = 819582;

        const std::string serialised_doc = doc.dump();
        ASSERT_EQ(tgbot::BotCommandScopeChatMember(serialised_doc).serialise(), serialised_doc);

        {
            tgbot::BotCommandScopeChatMember bot_command_scope_chat_member;

            if(i == 0)
            {
                bot_command_scope_chat_member.set_chat_id(984254);
                ASSERT_EQ(std::get<std::int64_t>(bot_command_scope_chat_member.get_chat_id()), 984254);
            }
            else
            {
                bot_command_scope_chat_member.set_chat_id("thofijpsiot");
                ASSERT_EQ(std::get<std::string>(bot_command_scope_chat_member.get_chat_id()), "thofijpsiot");
            }

            bot_command_scope_chat_member.set_type("fcqopihfdg");
            ASSERT_EQ(bot_command_scope_chat_member.get_type(), "fcqopihfdg");

            bot_command_scope_chat_member.set_user_id(819582);
            ASSERT_EQ(bot_command_scope_chat_member.get_user_id(), 819582);

            ASSERT_EQ(bot_command_scope_chat_member.serialise(), serialised_doc);
        }
    }
}

TEST(BotCommandScopeChatMember, errors)
{
    ASSERT_THROW(tgbot::BotCommandScopeChatMember(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["chat_id"] = 49151;
        }

        if(i != 1)
        {
            doc["type"] = "daawe";
        }

        if(i != 2)
        {
            doc["user_id"] = 81984;
        }

        ASSERT_THROW(tgbot::BotCommandScopeChatMember(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["chat_id"] = 68156 : doc["chat_id"] = false;
        i != 1 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;
        i != 2 ? doc["user_id"] = 49818 : doc["user_id"] = "föosihj";

        ASSERT_THROW(tgbot::BotCommandScopeChatMember(doc.dump()), std::invalid_argument);
    }
}
