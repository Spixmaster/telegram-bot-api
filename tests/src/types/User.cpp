#include "tgbot/types/User.h"

#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(User, normal)
{
    ASSERT_NO_THROW(tgbot::User(tgbot::User().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["added_to_attachment_menu"] = false;
    doc["can_join_groups"] = false;
    doc["can_read_all_group_messages"] = true;
    doc["first_name"] = "robnsdf";
    doc["id"] = 762465;
    doc["is_bot"] = true;
    doc["is_premium"] = true;
    doc["language_code"] = "oiehbks";
    doc["last_name"] = "yöifnhdf";
    doc["supports_inline_queries"] = false;
    doc["username"] = "oinsknndfb";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::User(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::User user;

        user.set_added_to_attachment_menu(false);
        ASSERT_EQ(user.get_added_to_attachment_menu(), false);

        user.set_can_join_groups(false);
        ASSERT_EQ(user.get_can_join_groups(), false);

        user.set_can_read_all_group_messages(true);
        ASSERT_EQ(user.get_can_read_all_group_messages(), true);

        user.set_first_name("robnsdf");
        ASSERT_EQ(user.get_first_name(), "robnsdf");

        user.set_id(762465);
        ASSERT_EQ(user.get_id(), 762465);

        user.set_is_bot(true);
        ASSERT_EQ(user.get_is_bot(), true);

        user.set_is_premium(true);
        ASSERT_EQ(user.get_is_premium(), true);

        user.set_language_code("oiehbks");
        ASSERT_EQ(user.get_language_code(), "oiehbks");

        user.set_last_name("yöifnhdf");
        ASSERT_EQ(user.get_last_name(), "yöifnhdf");

        user.set_supports_inline_queries(false);
        ASSERT_EQ(user.get_supports_inline_queries(), false);

        user.set_username("oinsknndfb");
        ASSERT_EQ(user.get_username(), "oinsknndfb");

        ASSERT_EQ(user.serialise(), serialised_doc);
    }
}

TEST(User, errors)
{
    ASSERT_THROW(tgbot::User(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["first_name"] = "rpüuc";
        }

        if(i != 1)
        {
            doc["id"] = 7161;
        }

        if(i != 2)
        {
            doc["is_bot"] = true;
        }

        ASSERT_THROW(tgbot::User(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 11; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["added_to_attachment_menu"] = true : doc["added_to_attachment_menu"] = 1354;
        i != 1 ? doc["can_join_groups"] = true : doc["can_join_groups"] = -56132;
        i != 2 ? doc["can_read_all_group_messages"] = true : doc["can_read_all_group_messages"] = 71684;
        i != 3 ? doc["first_name"] = "aoisjgldfgs" : doc["first_name"] = 286;
        i != 4 ? doc["id"] = 198454 : doc["id"] = "xnuhüksdhq";
        i != 5 ? doc["is_bot"] = false : doc["is_bot"] = 49812;
        i != 6 ? doc["is_premium"] = false : doc["is_premium"] = 6921354;
        i != 7 ? doc["language_code"] = "aoisjgldfgs" : doc["language_code"] = 83165;
        i != 8 ? doc["last_name"] = "rxoijrqh" : doc["last_name"] = 791312;
        i != 9 ? doc["supports_inline_queries"] = true : doc["supports_inline_queries"] = 68134;
        i != 10 ? doc["username"] = "aoisjgldfgs" : doc["username"] = 498135;

        ASSERT_THROW(tgbot::User(doc.dump()), std::invalid_argument);
    }
}
