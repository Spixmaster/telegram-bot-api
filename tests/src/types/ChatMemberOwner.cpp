#include "tgbot/types/ChatMemberOwner.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(ChatMemberOwner, normal)
{
    ASSERT_NO_THROW(tgbot::ChatMemberOwner(tgbot::ChatMemberOwner().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["custom_title"] = "okzvweg";
    doc["is_anonymous"] = true;
    doc["status"] = "rpüovkomng";
    doc["user"] = nlohmann::json::parse(tgbot::User().serialise());

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ChatMemberOwner(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::ChatMemberOwner chat_member_owner;

        chat_member_owner.set_custom_title("okzvweg");
        ASSERT_EQ(chat_member_owner.get_custom_title(), "okzvweg");

        chat_member_owner.set_is_anonymous(true);
        ASSERT_EQ(chat_member_owner.get_is_anonymous(), true);

        chat_member_owner.set_status("rpüovkomng");
        ASSERT_EQ(chat_member_owner.get_status(), "rpüovkomng");

        chat_member_owner.set_user(std::make_shared<tgbot::User>());
        ASSERT_EQ(chat_member_owner.get_user()->serialise(), tgbot::User().serialise());

        ASSERT_EQ(chat_member_owner.serialise(), serialised_doc);
    }
}

TEST(ChatMemberOwner, errors)
{
    ASSERT_THROW(tgbot::ChatMemberOwner(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["is_anonymous"] = true;
        }

        if(i != 1)
        {
            doc["status"] = "owihf";
        }

        if(i != 2)
        {
            doc["user"] = nlohmann::json::parse(tgbot::User().serialise());
        }

        ASSERT_THROW(tgbot::ChatMemberOwner(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["custom_title"] = "pnw" : doc["custom_title"] = 684;
        i != 1 ? doc["is_anonymous"] = false : doc["is_anonymous"] = 13876886;
        i != 2 ? doc["status"] = "qwrpovasd" : doc["status"] = 65;
        i != 3 ? doc["user"] = nlohmann::json::parse(tgbot::User().serialise()) : doc["user"] = 73546;

        ASSERT_THROW(tgbot::ChatMemberOwner(doc.dump()), std::invalid_argument);
    }
}
