#include "tgbot/types/ReplyKeyboardRemove.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(ReplyKeyboardRemove, normal)
{
    ASSERT_NO_THROW(tgbot::ReplyKeyboardRemove(tgbot::ReplyKeyboardRemove().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["remove_keyboard"] = false;
    doc["selective"] = true;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ReplyKeyboardRemove(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::ReplyKeyboardRemove reply_keyboard_remove;

        reply_keyboard_remove.set_remove_keyboard(false);
        ASSERT_EQ(reply_keyboard_remove.get_remove_keyboard(), false);

        reply_keyboard_remove.set_selective(true);
        ASSERT_EQ(reply_keyboard_remove.get_selective(), true);

        ASSERT_EQ(reply_keyboard_remove.serialise(), serialised_doc);
    }
}

TEST(ReplyKeyboardRemove, errors)
{
    ASSERT_THROW(tgbot::ReplyKeyboardRemove(nlohmann::json::array().dump()), std::invalid_argument);

    {
        const nlohmann::json doc = nlohmann::json::object();

        ASSERT_THROW(tgbot::ReplyKeyboardRemove(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["remove_keyboard"] = true : doc["remove_keyboard"] = 71357;
        i != 1 ? doc["selective"] = false : doc["selective"] = 789531341;

        ASSERT_THROW(tgbot::ReplyKeyboardRemove(doc.dump()), std::invalid_argument);
    }
}
