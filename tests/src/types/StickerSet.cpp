#include "tgbot/types/StickerSet.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(StickerSet, normal)
{
    ASSERT_NO_THROW(tgbot::StickerSet(tgbot::StickerSet().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["is_animated"] = false;
    doc["is_video"] = false;
    doc["name"] = "sdgerg";
    doc["sticker_type"] = "eoixnqg";
    doc["stickers"] = {nlohmann::json::parse(tgbot::Sticker().serialise())};
    doc["thumbnail"] = nlohmann::json::parse(tgbot::PhotoSize().serialise());
    doc["title"] = "pdijhsdf";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::StickerSet(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::StickerSet sticker_set;

        sticker_set.set_is_animated(false);
        ASSERT_EQ(sticker_set.get_is_animated(), false);

        sticker_set.set_is_video(false);
        ASSERT_EQ(sticker_set.get_is_video(), false);

        sticker_set.set_name("sdgerg");
        ASSERT_EQ(sticker_set.get_name(), "sdgerg");

        sticker_set.set_sticker_type("eoixnqg");
        ASSERT_EQ(sticker_set.get_sticker_type(), "eoixnqg");

        sticker_set.set_stickers({std::make_shared<tgbot::Sticker>()});
        ASSERT_EQ(sticker_set.get_stickers().at(0)->serialise(), tgbot::Sticker().serialise());

        sticker_set.set_thumbnail(std::make_shared<tgbot::PhotoSize>());

        if(sticker_set.get_thumbnail().has_value())
        {
            ASSERT_EQ(sticker_set.get_thumbnail().value()->serialise(), tgbot::PhotoSize().serialise());
        }

        sticker_set.set_title("pdijhsdf");
        ASSERT_EQ(sticker_set.get_title(), "pdijhsdf");

        ASSERT_EQ(sticker_set.serialise(), serialised_doc);
    }
}

TEST(StickerSet, errors)
{
    ASSERT_THROW(tgbot::StickerSet(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 6; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["is_animated"] = false;
        }

        if(i != 1)
        {
            doc["is_video"] = false;
        }

        if(i != 2)
        {
            doc["name"] = "aoisjgldfgs";
        }

        if(i != 3)
        {
            doc["sticker_type"] = "rpoijasg";
        }

        if(i != 4)
        {
            doc["stickers"] = {nlohmann::json::parse(tgbot::Sticker().serialise())};
        }

        if(i != 5)
        {
            doc["title"] = "owihf";
        }

        ASSERT_THROW(tgbot::StickerSet(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 7; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["is_animated"] = true : doc["is_animated"] = -17;
        i != 1 ? doc["is_video"] = true : doc["is_video"] = 14;
        i != 2 ? doc["name"] = "aoisjgldfgs" : doc["name"] = 286;
        i != 3 ? doc["sticker_type"] = "opinnt" : doc["sticker_type"] = 31254;
        i != 4 ? doc["stickers"] = {nlohmann::json::parse(tgbot::Sticker().serialise())} : doc["stickers"] = 55965;
        i != 5 ? doc["thumbnail"] = nlohmann::json::parse(tgbot::PhotoSize().serialise()) : doc["thumbnail"] = 468;
        i != 6 ? doc["title"] = "owihf" : doc["title"] = 65;

        ASSERT_THROW(tgbot::StickerSet(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["is_animated"] = false;
        doc["is_video"] = false;
        doc["name"] = "aoisjgldfgs";
        doc["sticker_type"] = "enasdgph";
        doc["stickers"] = {17215};
        doc["title"] = "owihf";

        ASSERT_THROW(tgbot::StickerSet(doc.dump()), std::invalid_argument);
    }
}
