#include "tgbot/types/PassportElementErrorSelfie.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(PassportElementErrorSelfie, normal)
{
    ASSERT_NO_THROW(tgbot::PassportElementErrorSelfie(tgbot::PassportElementErrorSelfie().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["file_hash"] = "rthpoisjdfg";
    doc["message"] = "hölkjsdgf";
    doc["source"] = "thpoksjdfg";
    doc["type"] = "byoij";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::PassportElementErrorSelfie(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::PassportElementErrorSelfie passport_element_error_selfie;

        passport_element_error_selfie.set_file_hash("rthpoisjdfg");
        ASSERT_EQ(passport_element_error_selfie.get_file_hash(), "rthpoisjdfg");

        passport_element_error_selfie.set_message("hölkjsdgf");
        ASSERT_EQ(passport_element_error_selfie.get_message(), "hölkjsdgf");

        passport_element_error_selfie.set_source("thpoksjdfg");
        ASSERT_EQ(passport_element_error_selfie.get_source(), "thpoksjdfg");

        passport_element_error_selfie.set_type("byoij");
        ASSERT_EQ(passport_element_error_selfie.get_type(), "byoij");

        ASSERT_EQ(passport_element_error_selfie.serialise(), serialised_doc);
    }
}

TEST(PassportElementErrorSelfie, errors)
{
    ASSERT_THROW(tgbot::PassportElementErrorSelfie(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["file_hash"] = "pobunfb";
        }

        if(i != 1)
        {
            doc["message"] = "yperoih";
        }

        if(i != 2)
        {
            doc["source"] = "aoisjgldfgs";
        }

        if(i != 3)
        {
            doc["type"] = "owihf";
        }

        ASSERT_THROW(tgbot::PassportElementErrorSelfie(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["file_hash"] = "egoijasdg" : doc["file_hash"] = -41766;
        i != 1 ? doc["message"] = "thoijsdv" : doc["message"] = 8687;
        i != 2 ? doc["source"] = "aoisjgldfgs" : doc["source"] = 286;
        i != 3 ? doc["type"] = "owihf" : doc["type"] = 65;

        ASSERT_THROW(tgbot::PassportElementErrorSelfie(doc.dump()), std::invalid_argument);
    }
}
