#include "tgbot/types/KeyboardButtonPollType.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(KeyboardButtonPollType, normal)
{
    ASSERT_NO_THROW(tgbot::KeyboardButtonPollType(tgbot::KeyboardButtonPollType().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["type"] = "hoskjdfg";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::KeyboardButtonPollType(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::KeyboardButtonPollType keyboard_button_poll_type;

        keyboard_button_poll_type.set_type("hoskjdfg");
        ASSERT_EQ(keyboard_button_poll_type.get_type(), "hoskjdfg");

        ASSERT_EQ(keyboard_button_poll_type.serialise(), serialised_doc);
    }
}

TEST(KeyboardButtonPollType, errors)
{
    ASSERT_THROW(tgbot::KeyboardButtonPollType(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 1; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;

        ASSERT_THROW(tgbot::KeyboardButtonPollType(doc.dump()), std::invalid_argument);
    }
}
