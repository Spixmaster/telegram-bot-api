#include "tgbot/types/ChosenInlineResult.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(ChosenInlineResult, normal)
{
    ASSERT_NO_THROW(tgbot::ChosenInlineResult(tgbot::ChosenInlineResult().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["from"] = nlohmann::json::parse(tgbot::User().serialise());
    doc["inline_message_id"] = "sdfbsmdfg";
    doc["location"] = nlohmann::json::parse(tgbot::Location().serialise());
    doc["query"] = "vnpismfdg";
    doc["result_id"] = "qdobpwet";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ChosenInlineResult(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::ChosenInlineResult chosen_inline_result;

        chosen_inline_result.set_from(std::make_shared<tgbot::User>());
        ASSERT_EQ(chosen_inline_result.get_from()->serialise(), tgbot::User().serialise());

        chosen_inline_result.set_inline_message_id("sdfbsmdfg");
        ASSERT_EQ(chosen_inline_result.get_inline_message_id(), "sdfbsmdfg");

        chosen_inline_result.set_location(std::make_shared<tgbot::Location>());

        if(chosen_inline_result.get_location().has_value())
        {
            ASSERT_EQ(chosen_inline_result.get_location().value()->serialise(), tgbot::Location().serialise());
        }

        chosen_inline_result.set_query("vnpismfdg");
        ASSERT_EQ(chosen_inline_result.get_query(), "vnpismfdg");

        chosen_inline_result.set_result_id("qdobpwet");
        ASSERT_EQ(chosen_inline_result.get_result_id(), "qdobpwet");

        ASSERT_EQ(chosen_inline_result.serialise(), serialised_doc);
    }
}

TEST(ChosenInlineResult, errors)
{
    ASSERT_THROW(tgbot::ChosenInlineResult(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["from"] = nlohmann::json::parse(tgbot::User().serialise());
        }

        if(i != 1)
        {
            doc["query"] = "rhpoisjdg";
        }

        if(i != 2)
        {
            doc["result_id"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::ChosenInlineResult(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 5; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["from"] = nlohmann::json::parse(tgbot::User().serialise()) : doc["from"] = 65;
        i != 1 ? doc["inline_message_id"] = "sfpjweh" : doc["inline_message_id"] = 468;
        i != 2 ? doc["location"] = nlohmann::json::parse(tgbot::Location().serialise()) : doc["location"] = 65;
        i != 3 ? doc["query"] = "cijwrh" : doc["query"] = 18764;
        i != 4 ? doc["result_id"] = "aoisjgldfgs" : doc["result_id"] = 286;

        ASSERT_THROW(tgbot::ChosenInlineResult(doc.dump()), std::invalid_argument);
    }
}
