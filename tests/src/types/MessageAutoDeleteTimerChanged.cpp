#include "tgbot/types/MessageAutoDeleteTimerChanged.h"

#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(MessageAutoDeleteTimerChanged, normal)
{
    ASSERT_NO_THROW(tgbot::MessageAutoDeleteTimerChanged(tgbot::MessageAutoDeleteTimerChanged().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["message_auto_delete_time"] = -6851685;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::MessageAutoDeleteTimerChanged(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::MessageAutoDeleteTimerChanged message_auto_delete_timer_changed;

        message_auto_delete_timer_changed.set_message_auto_delete_time(-6851685);
        ASSERT_EQ(message_auto_delete_timer_changed.get_message_auto_delete_time(), -6851685);

        ASSERT_EQ(message_auto_delete_timer_changed.serialise(), serialised_doc);
    }
}

TEST(MessageAutoDeleteTimerChanged, errors)
{
    ASSERT_THROW(tgbot::MessageAutoDeleteTimerChanged(nlohmann::json::array().dump()), std::invalid_argument);

    {
        const nlohmann::json doc = nlohmann::json::object();

        ASSERT_THROW(tgbot::MessageAutoDeleteTimerChanged(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 1; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["message_auto_delete_time"] = 4 : doc["message_auto_delete_time"] = "coih";

        ASSERT_THROW(tgbot::MessageAutoDeleteTimerChanged(doc.dump()), std::invalid_argument);
    }
}
