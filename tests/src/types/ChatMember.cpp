#include "tgbot/types/ChatMember.h"

#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(ChatMember, normal)
{
    ASSERT_NO_THROW(tgbot::ChatMember(tgbot::ChatMember().serialise()));

    const nlohmann::json doc = nlohmann::json::object();

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ChatMember().serialise(), serialised_doc);

    {
        const tgbot::ChatMember chat_member;

        ASSERT_EQ(chat_member.serialise(), serialised_doc);
    }
}

TEST(ChatMember, errors)
{
    ASSERT_THROW(tgbot::ChatMember(nlohmann::json::array().dump()), std::invalid_argument);
}
