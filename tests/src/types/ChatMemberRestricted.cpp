#include "tgbot/types/ChatMemberRestricted.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(ChatMemberRestricted, normal)
{
    ASSERT_NO_THROW(tgbot::ChatMemberRestricted(tgbot::ChatMemberRestricted().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["can_add_web_page_previews"] = false;
    doc["can_change_info"] = false;
    doc["can_invite_users"] = true;
    doc["can_manage_topics"] = false;
    doc["can_pin_messages"] = true;
    doc["can_send_audios"] = false;
    doc["can_send_documents"] = false;
    doc["can_send_messages"] = true;
    doc["can_send_other_messages"] = false;
    doc["can_send_photos"] = true;
    doc["can_send_polls"] = false;
    doc["can_send_video_notes"] = false;
    doc["can_send_videos"] = false;
    doc["can_send_voice_notes"] = true;
    doc["is_member"] = false;
    doc["status"] = "topwmic";
    doc["until_date"] = 78165;
    doc["user"] = nlohmann::json::parse(tgbot::User().serialise());

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ChatMemberRestricted(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::ChatMemberRestricted chat_member_restricted;

        chat_member_restricted.set_can_add_web_page_previews(false);
        ASSERT_EQ(chat_member_restricted.get_can_add_web_page_previews(), false);

        chat_member_restricted.set_can_change_info(false);
        ASSERT_EQ(chat_member_restricted.get_can_change_info(), false);

        chat_member_restricted.set_can_invite_users(true);
        ASSERT_EQ(chat_member_restricted.get_can_invite_users(), true);

        chat_member_restricted.set_can_manage_topics(false);
        ASSERT_EQ(chat_member_restricted.get_can_manage_topics(), false);

        chat_member_restricted.set_can_pin_messages(true);
        ASSERT_EQ(chat_member_restricted.get_can_pin_messages(), true);

        chat_member_restricted.set_can_send_audios(false);
        ASSERT_EQ(chat_member_restricted.get_can_send_audios(), false);

        chat_member_restricted.set_can_send_documents(false);
        ASSERT_EQ(chat_member_restricted.get_can_send_documents(), false);

        chat_member_restricted.set_can_send_messages(true);
        ASSERT_EQ(chat_member_restricted.get_can_send_messages(), true);

        chat_member_restricted.set_can_send_other_messages(false);
        ASSERT_EQ(chat_member_restricted.get_can_send_other_messages(), false);

        chat_member_restricted.set_can_send_photos(true);
        ASSERT_EQ(chat_member_restricted.get_can_send_photos(), true);

        chat_member_restricted.set_can_send_polls(false);
        ASSERT_EQ(chat_member_restricted.get_can_send_polls(), false);

        chat_member_restricted.set_can_send_video_notes(false);
        ASSERT_EQ(chat_member_restricted.get_can_send_video_notes(), false);

        chat_member_restricted.set_can_send_videos(false);
        ASSERT_EQ(chat_member_restricted.get_can_send_videos(), false);

        chat_member_restricted.set_can_send_voice_notes(true);
        ASSERT_EQ(chat_member_restricted.get_can_send_voice_notes(), true);

        chat_member_restricted.set_is_member(false);
        ASSERT_EQ(chat_member_restricted.get_is_member(), false);

        chat_member_restricted.set_status("topwmic");
        ASSERT_EQ(chat_member_restricted.get_status(), "topwmic");

        chat_member_restricted.set_until_date(78165);
        ASSERT_EQ(chat_member_restricted.get_until_date(), 78165);

        chat_member_restricted.set_user(std::make_shared<tgbot::User>());
        ASSERT_EQ(chat_member_restricted.get_user()->serialise(), tgbot::User().serialise());

        ASSERT_EQ(chat_member_restricted.serialise(), serialised_doc);
    }
}

TEST(ChatMemberRestricted, errors)
{
    ASSERT_THROW(tgbot::ChatMemberRestricted(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 18; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["can_add_web_page_previews"] = false;
        }

        if(i != 1)
        {
            doc["can_change_info"] = true;
        }

        if(i != 2)
        {
            doc["can_invite_users"] = false;
        }

        if(i != 3)
        {
            doc["can_manage_topics"] = true;
        }

        if(i != 4)
        {
            doc["can_pin_messages"] = false;
        }

        if(i != 5)
        {
            doc["can_send_audios"] = false;
        }

        if(i != 6)
        {
            doc["can_send_documents"] = true;
        }

        if(i != 7)
        {
            doc["can_send_messages"] = false;
        }

        if(i != 8)
        {
            doc["can_send_other_messages"] = true;
        }

        if(i != 9)
        {
            doc["can_send_photos"] = true;
        }

        if(i != 10)
        {
            doc["can_send_polls"] = false;
        }

        if(i != 11)
        {
            doc["can_send_video_notes"] = false;
        }

        if(i != 12)
        {
            doc["can_send_videos"] = false;
        }

        if(i != 13)
        {
            doc["can_send_voice_notes"] = false;
        }

        if(i != 14)
        {
            doc["is_member"] = false;
        }

        if(i != 15)
        {
            doc["status"] = "owihf";
        }

        if(i != 16)
        {
            doc["until_date"] = 984;
        }

        if(i != 17)
        {
            doc["user"] = nlohmann::json::parse(tgbot::User().serialise());
        }

        ASSERT_THROW(tgbot::ChatMemberRestricted(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 18; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["can_add_web_page_previews"] = true : doc["can_add_web_page_previews"] = 5138;
        i != 1 ? doc["can_change_info"] = false : doc["can_change_info"] = 6831654;
        i != 2 ? doc["can_invite_users"] = true : doc["can_invite_users"] = 35564;
        i != 3 ? doc["can_manage_topics"] = true : doc["can_manage_topics"] = 54915;
        i != 4 ? doc["can_pin_messages"] = false : doc["can_pin_messages"] = 7321954;
        i != 5 ? doc["can_send_audios"] = false : doc["can_send_audios"] = 298721;
        i != 6 ? doc["can_send_documents"] = false : doc["can_send_documents"] = 995153;
        i != 7 ? doc["can_send_messages"] = true : doc["can_send_messages"] = 79815;
        i != 8 ? doc["can_send_other_messages"] = true : doc["can_send_other_messages"] = 9415165;
        i != 9 ? doc["can_send_photos"] = true : doc["can_send_photos"] = 9135473;
        i != 10 ? doc["can_send_polls"] = true : doc["can_send_polls"] = 9865;
        i != 11 ? doc["can_send_video_notes"] = false : doc["can_send_video_notes"] = 62687;
        i != 12 ? doc["can_send_videos"] = false : doc["can_send_videos"] = 1687115;
        i != 13 ? doc["can_send_voice_notes"] = false : doc["can_send_voice_notes"] = 1897654135;
        i != 14 ? doc["is_member"] = true : doc["is_member"] = 321546;
        i != 15 ? doc["status"] = "owihf" : doc["status"] = 65;
        i != 16 ? doc["until_date"] = 861573 : doc["until_date"] = "rhapdosijh";
        i != 17 ? doc["user"] = nlohmann::json::parse(tgbot::User().serialise()) : doc["user"] = 9155;

        ASSERT_THROW(tgbot::ChatMemberRestricted(doc.dump()), std::invalid_argument);
    }
}
