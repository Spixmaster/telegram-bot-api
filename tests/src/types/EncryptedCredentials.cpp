#include "tgbot/types/EncryptedCredentials.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(EncryptedCredentials, normal)
{
    ASSERT_NO_THROW(tgbot::EncryptedCredentials(tgbot::EncryptedCredentials().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["data"] = "cphoiwfg";
    doc["hash"] = "fowiufg";
    doc["secret"] = "zijomusvb";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::EncryptedCredentials(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::EncryptedCredentials encrypted_credentials;

        encrypted_credentials.set_data("cphoiwfg");
        ASSERT_EQ(encrypted_credentials.get_data(), "cphoiwfg");

        encrypted_credentials.set_hash("fowiufg");
        ASSERT_EQ(encrypted_credentials.get_hash(), "fowiufg");

        encrypted_credentials.set_secret("zijomusvb");
        ASSERT_EQ(encrypted_credentials.get_secret(), "zijomusvb");

        ASSERT_EQ(encrypted_credentials.serialise(), serialised_doc);
    }
}

TEST(EncryptedCredentials, errors)
{
    ASSERT_THROW(tgbot::EncryptedCredentials(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["data"] = "aoisjgldfgs";
        }

        if(i != 1)
        {
            doc["hash"] = "owihf";
        }

        if(i != 2)
        {
            doc["secret"] = "itims";
        }

        ASSERT_THROW(tgbot::EncryptedCredentials(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["data"] = "aoisjgldfgs" : doc["data"] = 761648;
        i != 1 ? doc["hash"] = "owihf" : doc["hash"] = 7321654;
        i != 2 ? doc["secret"] = "idupwher" : doc["secret"] = 71651;

        ASSERT_THROW(tgbot::EncryptedCredentials(doc.dump()), std::invalid_argument);
    }
}
