#include "tgbot/types/Voice.h"

#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(Voice, normal)
{
    ASSERT_NO_THROW(tgbot::Voice(tgbot::Voice().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["duration"] = 468151;
    doc["file_id"] = "rihsdf";
    doc["file_size"] = -559412;
    doc["file_unique_id"] = "oejlkjsdfg";
    doc["mime_type"] = "pcknc";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::Voice(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::Voice voice;

        voice.set_duration(468151);
        ASSERT_EQ(voice.get_duration(), 468151);

        voice.set_file_id("rihsdf");
        ASSERT_EQ(voice.get_file_id(), "rihsdf");

        voice.set_file_size(-559412);
        ASSERT_EQ(voice.get_file_size(), -559412);

        voice.set_file_unique_id("oejlkjsdfg");
        ASSERT_EQ(voice.get_file_unique_id(), "oejlkjsdfg");

        voice.set_mime_type("pcknc");
        ASSERT_EQ(voice.get_mime_type(), "pcknc");

        ASSERT_EQ(voice.serialise(), serialised_doc);
    }
}

TEST(Voice, errors)
{
    ASSERT_THROW(tgbot::Voice(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["duration"] = 7981684;
        }

        if(i != 1)
        {
            doc["file_id"] = "aoisjgldfgs";
        }

        if(i != 2)
        {
            doc["file_unique_id"] = "owihf";
        }

        ASSERT_THROW(tgbot::Voice(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 5; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["duration"] = 7981684 : doc["duration"] = "bsefbsdfh";
        i != 1 ? doc["file_id"] = "aoisjgldfgs" : doc["file_id"] = 286;
        i != 2 ? doc["file_size"] = 4561 : doc["file_size"] = "sdfhsdrg";
        i != 3 ? doc["file_unique_id"] = "owihf" : doc["file_unique_id"] = 65;
        i != 4 ? doc["mime_type"] = "woöinefmoib" : doc["mime_type"] = 616842;

        ASSERT_THROW(tgbot::Voice(doc.dump()), std::invalid_argument);
    }
}
