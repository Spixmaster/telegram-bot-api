#include "tgbot/types/WriteAccessAllowed.h"

#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(WriteAccessAllowed, normal)
{
    ASSERT_NO_THROW(tgbot::WriteAccessAllowed(tgbot::WriteAccessAllowed().serialise()));

    const nlohmann::json doc = nlohmann::json::object();

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::WriteAccessAllowed(serialised_doc).serialise(), serialised_doc);

    {
        const tgbot::WriteAccessAllowed write_access_allowed;

        ASSERT_EQ(write_access_allowed.serialise(), serialised_doc);
    }
}

TEST(WriteAccessAllowed, errors)
{
    ASSERT_THROW(tgbot::WriteAccessAllowed(nlohmann::json::array().dump()), std::invalid_argument);
}
