#include "tgbot/types/VideoChatEnded.h"

#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(VideoChatEnded, normal)
{
    ASSERT_NO_THROW(tgbot::VideoChatEnded(tgbot::VideoChatEnded().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["duration"] = 79674;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::VideoChatEnded(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::VideoChatEnded voice_chat_ended;

        voice_chat_ended.set_duration(79674);
        ASSERT_EQ(voice_chat_ended.get_duration(), 79674);

        ASSERT_EQ(voice_chat_ended.serialise(), serialised_doc);
    }
}

TEST(VideoChatEnded, errors)
{
    ASSERT_THROW(tgbot::VideoChatEnded(nlohmann::json::array().dump()), std::invalid_argument);

    {
        const nlohmann::json doc = nlohmann::json::object();

        ASSERT_THROW(tgbot::VideoChatEnded(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 1; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["duration"] = 7981684 : doc["duration"] = "bsefbsdfh";

        ASSERT_THROW(tgbot::VideoChatEnded(doc.dump()), std::invalid_argument);
    }
}
