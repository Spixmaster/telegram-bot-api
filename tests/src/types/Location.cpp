#include "tgbot/types/Location.h"

#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(Location, normal)
{
    ASSERT_NO_THROW(tgbot::Location(tgbot::Location().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["heading"] = -68;
    doc["horizontal_accuracy"] = -6851.68F;
    doc["latitude"] = 4.574F;
    doc["live_period"] = 64841;
    doc["longitude"] = -684.175F;
    doc["proximity_alert_radius"] = -6841;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::Location(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::Location location;

        location.set_heading(-68);
        ASSERT_EQ(location.get_heading(), -68);

        location.set_horizontal_accuracy(-6851.68F);
        ASSERT_EQ(location.get_horizontal_accuracy(), -6851.68F);

        location.set_latitude(4.574F);
        ASSERT_EQ(location.get_latitude(), 4.574F);

        location.set_live_period(64841);
        ASSERT_EQ(location.get_live_period(), 64841);

        location.set_longitude(-684.175F);
        ASSERT_EQ(location.get_longitude(), -684.175F);

        location.set_proximity_alert_radius(-6841);
        ASSERT_EQ(location.get_proximity_alert_radius(), -6841);

        ASSERT_EQ(location.serialise(), serialised_doc);
    }
}

TEST(Location, errors)
{
    ASSERT_THROW(tgbot::Location(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["latitude"] = 984.6;
        }

        if(i != 1)
        {
            doc["longitude"] = 84.6;
        }

        ASSERT_THROW(tgbot::Location(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 6; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["heading"] = 87 : doc["heading"] = "herthsdf";
        i != 1 ? doc["horizontal_accuracy"] = 984.35 : doc["horizontal_accuracy"] = "cohah";
        i != 2 ? doc["latitude"] = 984.35 : doc["latitude"] = "cohah";
        i != 3 ? doc["live_period"] = 6844 : doc["live_period"] = "sdfg";
        i != 4 ? doc["longitude"] = 981.65 : doc["longitude"] = "coihqb";
        i != 5 ? doc["proximity_alert_radius"] = 7981684 : doc["proximity_alert_radius"] = "bsefbsdfh";

        ASSERT_THROW(tgbot::Location(doc.dump()), std::invalid_argument);
    }
}
