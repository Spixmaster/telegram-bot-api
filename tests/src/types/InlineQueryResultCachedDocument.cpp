#include "tgbot/types/InlineQueryResultCachedDocument.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InlineQueryResultCachedDocument, normal)
{
    ASSERT_NO_THROW(tgbot::InlineQueryResultCachedDocument(tgbot::InlineQueryResultCachedDocument().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["caption"] = "coimbnwoifb";
    doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())};
    doc["description"] = "cpumnwiodfb";
    doc["document_file_id"] = "vbsuidnf";
    doc["id"] = "cposmjdf";
    doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise());
    doc["parse_mode"] = "cmbmonsdfh";
    doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise());
    doc["title"] = "nmjoisjdfib";
    doc["type"] = "coimjdwfh";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InlineQueryResultCachedDocument(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InlineQueryResultCachedDocument inline_query_result_cached_document;

        inline_query_result_cached_document.set_caption("coimbnwoifb");
        ASSERT_EQ(inline_query_result_cached_document.get_caption(), "coimbnwoifb");

        inline_query_result_cached_document.set_caption_entities(std::vector<tgbot::MessageEntity::ptr> {
          std::make_shared<tgbot::MessageEntity>()});

        if(inline_query_result_cached_document.get_caption_entities().has_value())
        {
            ASSERT_EQ(inline_query_result_cached_document.get_caption_entities().value().at(0)->serialise(),
                      tgbot::MessageEntity().serialise());
        }

        inline_query_result_cached_document.set_description("cpumnwiodfb");
        ASSERT_EQ(inline_query_result_cached_document.get_description(), "cpumnwiodfb");

        inline_query_result_cached_document.set_document_file_id("vbsuidnf");
        ASSERT_EQ(inline_query_result_cached_document.get_document_file_id(), "vbsuidnf");

        inline_query_result_cached_document.set_id("cposmjdf");
        ASSERT_EQ(inline_query_result_cached_document.get_id(), "cposmjdf");

        inline_query_result_cached_document.set_input_message_content(std::make_shared<tgbot::InputMessageContent>());

        if(inline_query_result_cached_document.get_input_message_content().has_value())
        {
            ASSERT_EQ(inline_query_result_cached_document.get_input_message_content().value()->serialise(),
                      tgbot::InputMessageContent().serialise());
        }

        inline_query_result_cached_document.set_parse_mode("cmbmonsdfh");
        ASSERT_EQ(inline_query_result_cached_document.get_parse_mode(), "cmbmonsdfh");

        inline_query_result_cached_document.set_reply_markup(std::make_shared<tgbot::InlineKeyboardMarkup>());

        if(inline_query_result_cached_document.get_reply_markup().has_value())
        {
            ASSERT_EQ(inline_query_result_cached_document.get_reply_markup().value()->serialise(),
                      tgbot::InlineKeyboardMarkup().serialise());
        }

        inline_query_result_cached_document.set_title("nmjoisjdfib");
        ASSERT_EQ(inline_query_result_cached_document.get_title(), "nmjoisjdfib");

        inline_query_result_cached_document.set_type("coimjdwfh");
        ASSERT_EQ(inline_query_result_cached_document.get_type(), "coimjdwfh");

        ASSERT_EQ(inline_query_result_cached_document.serialise(), serialised_doc);
    }
}

TEST(InlineQueryResultCachedDocument, errors)
{
    ASSERT_THROW(tgbot::InlineQueryResultCachedDocument(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["document_file_id"] = "diopqb";
        }

        if(i != 1)
        {
            doc["id"] = "rpoehmc";
        }

        if(i != 2)
        {
            doc["title"] = "icqun";
        }

        if(i != 3)
        {
            doc["type"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::InlineQueryResultCachedDocument(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 10; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["caption"] = "mpoiashxj" : doc["caption"] = 73587;
        i != 1 ?
          doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())} :
          doc["caption_entities"] = 4168;
        i != 2 ? doc["description"] = "cxpjih" : doc["description"] = 16841;
        i != 3 ? doc["document_file_id"] = "cxpjih" : doc["document_file_id"] = 16841;
        i != 4 ? doc["id"] = "owihf" : doc["id"] = 65;
        i != 5 ?
          doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise()) :
          doc["input_message_content"] = 49;
        i != 6 ? doc["parse_mode"] = "djlhqg" : doc["parse_mode"] = 4168;
        i != 7 ?
          doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise()) :
          doc["reply_markup"] = 49;
        i != 8 ? doc["title"] = "opqgd" : doc["title"] = 486;
        i != 9 ? doc["type"] = "aoisjgldf" : doc["type"] = 286;

        ASSERT_THROW(tgbot::InlineQueryResultCachedDocument(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["caption_entities"] = {382684};
        doc["document_file_id"] = "diopqb";
        doc["id"] = "owihf";
        doc["title"] = "icqun";
        doc["type"] = "aoisjgldfgs";

        ASSERT_THROW(tgbot::InlineQueryResultCachedDocument(doc.dump()), std::invalid_argument);
    }
}
