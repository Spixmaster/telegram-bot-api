#include "tgbot/types/KeyboardButtonRequestChat.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(KeyboardButtonRequestChat, normal)
{
    ASSERT_NO_THROW(tgbot::KeyboardButtonRequestChat(tgbot::KeyboardButtonRequestChat().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["bot_administrator_rights"] = nlohmann::json::parse(tgbot::ChatAdministratorRights().serialise());
    doc["bot_is_member"] = false;
    doc["chat_has_username"] = false;
    doc["chat_is_channel"] = true;
    doc["chat_is_created"] = true;
    doc["chat_is_forum"] = false;
    doc["request_id"] = -71378656;
    doc["user_administrator_rights"] = nlohmann::json::parse(tgbot::ChatAdministratorRights().serialise());

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::KeyboardButtonRequestChat(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::KeyboardButtonRequestChat keyboard_button_request_chat;

        keyboard_button_request_chat.set_bot_administrator_rights(std::make_shared<tgbot::ChatAdministratorRights>());

        if(keyboard_button_request_chat.get_bot_administrator_rights().has_value())
        {
            ASSERT_EQ(keyboard_button_request_chat.get_bot_administrator_rights().value()->serialise(),
                      tgbot::ChatAdministratorRights().serialise());
        }

        keyboard_button_request_chat.set_bot_is_member(false);
        ASSERT_EQ(keyboard_button_request_chat.get_bot_is_member(), false);

        keyboard_button_request_chat.set_chat_has_username(false);
        ASSERT_EQ(keyboard_button_request_chat.get_chat_has_username(), false);

        keyboard_button_request_chat.set_chat_is_channel(true);
        ASSERT_EQ(keyboard_button_request_chat.get_chat_is_channel(), true);

        keyboard_button_request_chat.set_chat_is_created(true);
        ASSERT_EQ(keyboard_button_request_chat.get_chat_is_created(), true);

        keyboard_button_request_chat.set_chat_is_forum(false);
        ASSERT_EQ(keyboard_button_request_chat.get_chat_is_forum(), false);

        keyboard_button_request_chat.set_request_id(-71378656);
        ASSERT_EQ(keyboard_button_request_chat.get_request_id(), -71378656);

        keyboard_button_request_chat.set_user_administrator_rights(std::make_shared<tgbot::ChatAdministratorRights>());

        if(keyboard_button_request_chat.get_user_administrator_rights().has_value())
        {
            ASSERT_EQ(keyboard_button_request_chat.get_user_administrator_rights().value()->serialise(),
                      tgbot::ChatAdministratorRights().serialise());
        }

        ASSERT_EQ(keyboard_button_request_chat.serialise(), serialised_doc);
    }
}

TEST(KeyboardButtonRequestChat, errors)
{
    ASSERT_THROW(tgbot::KeyboardButtonRequestChat(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["chat_is_channel"] = false;
        }

        if(i != 1)
        {
            doc["request_id"] = 56531;
        }

        ASSERT_THROW(tgbot::KeyboardButtonRequestChat(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 8; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ?
          doc["bot_administrator_rights"] = nlohmann::json::parse(tgbot::ChatAdministratorRights().serialise()) :
          doc["bot_administrator_rights"] = 1685;
        i != 1 ? doc["bot_is_member"] = false : doc["bot_is_member"] = 4897136;
        i != 2 ? doc["chat_has_username"] = true : doc["chat_has_username"] = 19578;
        i != 3 ? doc["chat_is_channel"] = true : doc["chat_is_channel"] = 4987123;
        i != 4 ? doc["chat_is_created"] = false : doc["chat_is_created"] = 49123;
        i != 5 ? doc["chat_is_forum"] = true : doc["chat_is_forum"] = 1894;
        i != 6 ? doc["request_id"] = 358713 : doc["request_id"] = "pqoidwh";
        i != 7 ?
          doc["user_administrator_rights"] = nlohmann::json::parse(tgbot::ChatAdministratorRights().serialise()) :
          doc["user_administrator_rights"] = 1685;

        ASSERT_THROW(tgbot::KeyboardButtonRequestChat(doc.dump()), std::invalid_argument);
    }
}
