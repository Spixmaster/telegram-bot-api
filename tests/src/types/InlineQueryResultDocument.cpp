#include "tgbot/types/InlineQueryResultDocument.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InlineQueryResultDocument, normal)
{
    ASSERT_NO_THROW(tgbot::InlineQueryResultDocument(tgbot::InlineQueryResultDocument().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["caption"] = "tnpkmd";
    doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())};
    doc["description"] = "wpognoc";
    doc["document_url"] = "poinisbdcb";
    doc["id"] = "rgönkf";
    doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise());
    doc["mime_type"] = "whopijnioc";
    doc["parse_mode"] = "wlnscgdfgf";
    doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise());
    doc["thumbnail_height"] = 419498;
    doc["thumbnail_url"] = "wpoihcinj";
    doc["thumbnail_width"] = -4284;
    doc["title"] = "sdüpkbdfb";
    doc["type"] = "kakjdsg";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InlineQueryResultDocument(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InlineQueryResultDocument inline_query_result_document;

        inline_query_result_document.set_caption("tnpkmd");
        ASSERT_EQ(inline_query_result_document.get_caption(), "tnpkmd");

        inline_query_result_document.set_caption_entities(std::vector<tgbot::MessageEntity::ptr> {
          std::make_shared<tgbot::MessageEntity>()});

        if(inline_query_result_document.get_caption_entities().has_value())
        {
            ASSERT_EQ(inline_query_result_document.get_caption_entities().value().at(0)->serialise(),
                      tgbot::MessageEntity().serialise());
        }

        inline_query_result_document.set_description("wpognoc");
        ASSERT_EQ(inline_query_result_document.get_description(), "wpognoc");

        inline_query_result_document.set_document_url("poinisbdcb");
        ASSERT_EQ(inline_query_result_document.get_document_url(), "poinisbdcb");

        inline_query_result_document.set_id("rgönkf");
        ASSERT_EQ(inline_query_result_document.get_id(), "rgönkf");

        inline_query_result_document.set_input_message_content(std::make_shared<tgbot::InputMessageContent>());

        if(inline_query_result_document.get_input_message_content().has_value())
        {
            ASSERT_EQ(inline_query_result_document.get_input_message_content().value()->serialise(),
                      tgbot::InputMessageContent().serialise());
        }

        inline_query_result_document.set_mime_type("whopijnioc");
        ASSERT_EQ(inline_query_result_document.get_mime_type(), "whopijnioc");

        inline_query_result_document.set_parse_mode("wlnscgdfgf");
        ASSERT_EQ(inline_query_result_document.get_parse_mode(), "wlnscgdfgf");

        inline_query_result_document.set_reply_markup(std::make_shared<tgbot::InlineKeyboardMarkup>());

        if(inline_query_result_document.get_reply_markup().has_value())
        {
            ASSERT_EQ(inline_query_result_document.get_reply_markup().value()->serialise(),
                      tgbot::InlineKeyboardMarkup().serialise());
        }

        inline_query_result_document.set_thumbnail_height(419498);
        ASSERT_EQ(inline_query_result_document.get_thumbnail_height(), 419498);

        inline_query_result_document.set_thumbnail_url("wpoihcinj");
        ASSERT_EQ(inline_query_result_document.get_thumbnail_url(), "wpoihcinj");

        inline_query_result_document.set_thumbnail_width(-4284);
        ASSERT_EQ(inline_query_result_document.get_thumbnail_width(), -4284);

        inline_query_result_document.set_title("sdüpkbdfb");
        ASSERT_EQ(inline_query_result_document.get_title(), "sdüpkbdfb");

        inline_query_result_document.set_type("kakjdsg");
        ASSERT_EQ(inline_query_result_document.get_type(), "kakjdsg");

        ASSERT_EQ(inline_query_result_document.serialise(), serialised_doc);
    }
}

TEST(InlineQueryResultDocument, errors)
{
    ASSERT_THROW(tgbot::InlineQueryResultDocument(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 5; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["document_url"] = "diopqb";
        }

        if(i != 1)
        {
            doc["id"] = "owihf";
        }

        if(i != 2)
        {
            doc["mime_type"] = "wopfiwb";
        }

        if(i != 3)
        {
            doc["title"] = "rqoisjdg";
        }

        if(i != 4)
        {
            doc["type"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::InlineQueryResultDocument(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 14; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["caption"] = "djlhqg" : doc["caption"] = 4168;
        i != 1 ?
          doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())} :
          doc["caption_entities"] = 4168;
        i != 2 ? doc["description"] = "cqg" : doc["description"] = 6;
        i != 3 ? doc["document_url"] = "wuefhbnjdfb" : doc["document_url"] = 651;
        i != 4 ? doc["id"] = "owihf" : doc["id"] = 65;
        i != 5 ?
          doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise()) :
          doc["input_message_content"] = 49;
        i != 6 ? doc["mime_type"] = "iuh" : doc["mime_type"] = 17;
        i != 7 ? doc["parse_mode"] = "djlhqg" : doc["parse_mode"] = 4168;
        i != 8 ?
          doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise()) :
          doc["reply_markup"] = 49;
        i != 9 ? doc["thumbnail_height"] = 682 : doc["thumbnail_height"] = "qpuihc";
        i != 10 ? doc["thumbnail_url"] = "wuefhbnjdfb" : doc["thumbnail_url"] = 651;
        i != 11 ? doc["thumbnail_width"] = 1791 : doc["thumbnail_width"] = "cqpoij";
        i != 12 ? doc["title"] = "cxpjih" : doc["title"] = 739;
        i != 13 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;

        ASSERT_THROW(tgbot::InlineQueryResultDocument(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["caption_entities"] = {89216};
        doc["document_url"] = "eqpoijasdg";
        doc["id"] = "rhqpoisjd";
        doc["mime_type"] = "wegpoisg";
        doc["title"] = "diopqb";
        doc["type"] = "rcopaisrg";

        ASSERT_THROW(tgbot::InlineQueryResultDocument(doc.dump()), std::invalid_argument);
    }
}
