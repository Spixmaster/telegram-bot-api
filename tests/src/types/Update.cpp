#include "tgbot/types/Update.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(Update, normal)
{
    ASSERT_NO_THROW(tgbot::Update(tgbot::Update().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["callback_query"] = nlohmann::json::parse(tgbot::CallbackQuery().serialise());
    doc["channel_post"] = nlohmann::json::parse(tgbot::Message().serialise());
    doc["chat_join_request"] = nlohmann::json::parse(tgbot::ChatJoinRequest().serialise());
    doc["chat_member"] = nlohmann::json::parse(tgbot::ChatMemberUpdated().serialise());
    doc["chosen_inline_result"] = nlohmann::json::parse(tgbot::ChosenInlineResult().serialise());
    doc["edited_channel_post"] = nlohmann::json::parse(tgbot::Message().serialise());
    doc["edited_message"] = nlohmann::json::parse(tgbot::Message().serialise());
    doc["inline_query"] = nlohmann::json::parse(tgbot::InlineQuery().serialise());
    doc["message"] = nlohmann::json::parse(tgbot::Message().serialise());
    doc["my_chat_member"] = nlohmann::json::parse(tgbot::ChatMemberUpdated().serialise());
    doc["poll"] = nlohmann::json::parse(tgbot::Poll().serialise());
    doc["poll_answer"] = nlohmann::json::parse(tgbot::PollAnswer().serialise());
    doc["pre_checkout_query"] = nlohmann::json::parse(tgbot::PreCheckoutQuery().serialise());
    doc["shipping_query"] = nlohmann::json::parse(tgbot::ShippingQuery().serialise());
    doc["update_id"] = 645615;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::Update(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::Update update;

        update.set_callback_query(std::make_shared<tgbot::CallbackQuery>());

        if(update.get_callback_query().has_value())
        {
            ASSERT_EQ(update.get_callback_query().value()->serialise(), tgbot::CallbackQuery().serialise());
        }

        update.set_channel_post(std::make_shared<tgbot::Message>());

        if(update.get_channel_post().has_value())
        {
            ASSERT_EQ(update.get_channel_post().value()->serialise(), tgbot::Message().serialise());
        }

        update.set_chat_join_request(std::make_shared<tgbot::ChatJoinRequest>());

        if(update.get_chat_join_request().has_value())
        {
            ASSERT_EQ(update.get_chat_join_request().value()->serialise(), tgbot::ChatJoinRequest().serialise());
        }

        update.set_chat_member(std::make_shared<tgbot::ChatMemberUpdated>());

        if(update.get_chat_member().has_value())
        {
            ASSERT_EQ(update.get_chat_member().value()->serialise(), tgbot::ChatMemberUpdated().serialise());
        }

        update.set_chosen_inline_result(std::make_shared<tgbot::ChosenInlineResult>());

        if(update.get_chosen_inline_result().has_value())
        {
            ASSERT_EQ(update.get_chosen_inline_result().value()->serialise(), tgbot::ChosenInlineResult().serialise());
        }

        update.set_edited_channel_post(std::make_shared<tgbot::Message>());

        if(update.get_edited_channel_post().has_value())
        {
            ASSERT_EQ(update.get_edited_channel_post().value()->serialise(), tgbot::Message().serialise());
        }

        update.set_edited_message(std::make_shared<tgbot::Message>());

        if(update.get_edited_message().has_value())
        {
            ASSERT_EQ(update.get_edited_message().value()->serialise(), tgbot::Message().serialise());
        }

        update.set_inline_query(std::make_shared<tgbot::InlineQuery>());

        if(update.get_inline_query().has_value())
        {
            ASSERT_EQ(update.get_inline_query().value()->serialise(), tgbot::InlineQuery().serialise());
        }

        update.set_message(std::make_shared<tgbot::Message>());

        if(update.get_message().has_value())
        {
            ASSERT_EQ(update.get_message().value()->serialise(), tgbot::Message().serialise());
        }

        update.set_my_chat_member(std::make_shared<tgbot::ChatMemberUpdated>());

        if(update.get_my_chat_member().has_value())
        {
            ASSERT_EQ(update.get_my_chat_member().value()->serialise(), tgbot::ChatMemberUpdated().serialise());
        }

        update.set_poll(std::make_shared<tgbot::Poll>());

        if(update.get_poll().has_value())
        {
            ASSERT_EQ(update.get_poll().value()->serialise(), tgbot::Poll().serialise());
        }

        update.set_poll_answer(std::make_shared<tgbot::PollAnswer>());

        if(update.get_poll_answer().has_value())
        {
            ASSERT_EQ(update.get_poll_answer().value()->serialise(), tgbot::PollAnswer().serialise());
        }

        update.set_pre_checkout_query(std::make_shared<tgbot::PreCheckoutQuery>());

        if(update.get_pre_checkout_query().has_value())
        {
            ASSERT_EQ(update.get_pre_checkout_query().value()->serialise(), tgbot::PreCheckoutQuery().serialise());
        }

        update.set_shipping_query(std::make_shared<tgbot::ShippingQuery>());

        if(update.get_shipping_query().has_value())
        {
            ASSERT_EQ(update.get_shipping_query().value()->serialise(), tgbot::ShippingQuery().serialise());
        }

        update.set_update_id(645615);
        ASSERT_EQ(update.get_update_id(), 645615);

        ASSERT_EQ(update.serialise(), serialised_doc);
    }
}

TEST(Update, errors)
{
    ASSERT_THROW(tgbot::Update(nlohmann::json::array().dump()), std::invalid_argument);

    {
        const nlohmann::json doc = nlohmann::json::object();

        ASSERT_THROW(tgbot::Update(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 15; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ?
          doc["callback_query"] = nlohmann::json::parse(tgbot::CallbackQuery().serialise()) :
          doc["callback_query"] = 1687;
        i != 1 ? doc["channel_post"] = nlohmann::json::parse(tgbot::Message().serialise()) : doc["channel_post"] = 468;
        i != 2 ?
          doc["chat_join_request"] = nlohmann::json::parse(tgbot::ChatJoinRequest().serialise()) :
          doc["chat_join_request"] = -7651;
        i != 3 ?
          doc["chat_member"] = nlohmann::json::parse(tgbot::ChatMemberUpdated().serialise()) :
          doc["chat_member"] = 867231354;
        i != 4 ?
          doc["chosen_inline_result"] = nlohmann::json::parse(tgbot::ChosenInlineResult().serialise()) :
          doc["chosen_inline_result"] = -865132;
        i != 5 ?
          doc["edited_channel_post"] = nlohmann::json::parse(tgbot::Message().serialise()) :
          doc["edited_channel_post"] = 2151;
        i != 6 ?
          doc["edited_message"] = nlohmann::json::parse(tgbot::Message().serialise()) :
          doc["edited_message"] = 8132;
        i != 7 ?
          doc["inline_query"] = nlohmann::json::parse(tgbot::InlineQuery().serialise()) :
          doc["inline_query"] = -55;
        i != 8 ? doc["message"] = nlohmann::json::parse(tgbot::Message().serialise()) : doc["message"] = 468;
        i != 9 ?
          doc["my_chat_member"] = nlohmann::json::parse(tgbot::ChatMemberUpdated().serialise()) :
          doc["my_chat_member"] = 4187;
        i != 10 ? doc["poll"] = nlohmann::json::parse(tgbot::Poll().serialise()) : doc["poll"] = 468;
        i != 11 ? doc["poll_answer"] = nlohmann::json::parse(tgbot::PollAnswer().serialise()) : doc["poll_answer"] = 468;
        i != 12 ?
          doc["pre_checkout_query"] = nlohmann::json::parse(tgbot::PreCheckoutQuery().serialise()) :
          doc["pre_checkout_query"] = 59823;
        i != 13 ?
          doc["shipping_query"] = nlohmann::json::parse(tgbot::ShippingQuery().serialise()) :
          doc["shipping_query"] = 114611;
        i != 14 ? doc["update_id"] = 6844 : doc["update_id"] = "sdfg";

        ASSERT_THROW(tgbot::Update(doc.dump()), std::invalid_argument);
    }
}
