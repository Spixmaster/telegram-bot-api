#include "tgbot/types/InlineQueryResultArticle.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InlineQueryResultArticle, normal)
{
    ASSERT_NO_THROW(tgbot::InlineQueryResultArticle(tgbot::InlineQueryResultArticle().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["description"] = "bsoidfnh";
    doc["hide_url"] = true;
    doc["id"] = "cbowijfg";
    doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise());
    doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise());
    doc["thumbnail_height"] = -84168;
    doc["thumbnail_url"] = "cbpowndfg";
    doc["thumbnail_width"] = 64184;
    doc["title"] = "fbosiudjfboiu";
    doc["type"] = "dfboiwerg";
    doc["url"] = "sdfoibjnf";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InlineQueryResultArticle(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InlineQueryResultArticle inline_query_result_article;

        inline_query_result_article.set_description("bsoidfnh");
        ASSERT_EQ(inline_query_result_article.get_description(), "bsoidfnh");

        inline_query_result_article.set_hide_url(true);
        ASSERT_EQ(inline_query_result_article.get_hide_url(), true);

        inline_query_result_article.set_id("cbowijfg");
        ASSERT_EQ(inline_query_result_article.get_id(), "cbowijfg");

        inline_query_result_article.set_input_message_content(std::make_shared<tgbot::InputMessageContent>());
        ASSERT_EQ(inline_query_result_article.get_input_message_content()->serialise(),
                  tgbot::InputMessageContent().serialise());

        inline_query_result_article.set_reply_markup(std::make_shared<tgbot::InlineKeyboardMarkup>());

        if(inline_query_result_article.get_reply_markup().has_value())
        {
            ASSERT_EQ(inline_query_result_article.get_reply_markup().value()->serialise(),
                      tgbot::InlineKeyboardMarkup().serialise());
        }

        inline_query_result_article.set_thumbnail_height(-84168);
        ASSERT_EQ(inline_query_result_article.get_thumbnail_height(), -84168);

        inline_query_result_article.set_thumbnail_url("cbpowndfg");
        ASSERT_EQ(inline_query_result_article.get_thumbnail_url(), "cbpowndfg");

        inline_query_result_article.set_thumbnail_width(64184);
        ASSERT_EQ(inline_query_result_article.get_thumbnail_width(), 64184);

        inline_query_result_article.set_title("fbosiudjfboiu");
        ASSERT_EQ(inline_query_result_article.get_title(), "fbosiudjfboiu");

        inline_query_result_article.set_type("dfboiwerg");
        ASSERT_EQ(inline_query_result_article.get_type(), "dfboiwerg");

        inline_query_result_article.set_url("sdfoibjnf");
        ASSERT_EQ(inline_query_result_article.get_url(), "sdfoibjnf");

        ASSERT_EQ(inline_query_result_article.serialise(), serialised_doc);
    }
}

TEST(InlineQueryResultArticle, errors)
{
    ASSERT_THROW(tgbot::InlineQueryResultArticle(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["id"] = "owihf";
        }

        if(i != 1)
        {
            doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise());
        }

        if(i != 2)
        {
            doc["title"] = "odkipmq";
        }

        if(i != 3)
        {
            doc["type"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::InlineQueryResultArticle(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 11; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["description"] = "woöinefmoib" : doc["description"] = 616842;
        i != 1 ? doc["hide_url"] = true : doc["hide_url"] = 731687;
        i != 2 ? doc["id"] = "owihf" : doc["id"] = 65;
        i != 3 ?
          doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise()) :
          doc["input_message_content"] = 562;
        i != 4 ?
          doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise()) :
          doc["reply_markup"] = 562;
        i != 5 ? doc["thumbnail_height"] = 4561 : doc["thumbnail_height"] = "sdfhsdrg";
        i != 6 ? doc["thumbnail_url"] = "tiümnc" : doc["thumbnail_url"] = 51;
        i != 7 ? doc["thumbnail_width"] = 4561 : doc["thumbnail_width"] = "sdfhsdrg";
        i != 8 ? doc["title"] = "coöwh" : doc["title"] = 8717;
        i != 9 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;
        i != 10 ? doc["url"] = "wuefhbnjdfb" : doc["url"] = 651;

        ASSERT_THROW(tgbot::InlineQueryResultArticle(doc.dump()), std::invalid_argument);
    }
}
