#include "tgbot/types/OrderInfo.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(OrderInfo, normal)
{
    ASSERT_NO_THROW(tgbot::OrderInfo(tgbot::OrderInfo().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["email"] = "üpicihbq";
    doc["name"] = "qoifjkcv";
    doc["phone_number"] = "töpijie";
    doc["shipping_address"] = nlohmann::json::parse(tgbot::ShippingAddress().serialise());

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::OrderInfo(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::OrderInfo order_info;

        order_info.set_email("üpicihbq");
        ASSERT_EQ(order_info.get_email(), "üpicihbq");

        order_info.set_name("qoifjkcv");
        ASSERT_EQ(order_info.get_name(), "qoifjkcv");

        order_info.set_phone_number("töpijie");
        ASSERT_EQ(order_info.get_phone_number(), "töpijie");

        order_info.set_shipping_address(std::make_shared<tgbot::ShippingAddress>());

        if(order_info.get_shipping_address().has_value())
        {
            ASSERT_EQ(order_info.get_shipping_address().value()->serialise(), tgbot::ShippingAddress().serialise());
        }

        ASSERT_EQ(order_info.serialise(), serialised_doc);
    }
}

TEST(OrderInfo, errors)
{
    ASSERT_THROW(tgbot::OrderInfo(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["email"] = "owihf" : doc["email"] = 65;
        i != 1 ? doc["name"] = "aoisjgldfgs" : doc["name"] = 286;
        i != 2 ? doc["phone_number"] = "owihf" : doc["phone_number"] = 65;
        i != 3 ?
          doc["shipping_address"] = nlohmann::json::parse(tgbot::ShippingAddress().serialise()) :
          doc["shipping_address"] = 468;

        ASSERT_THROW(tgbot::OrderInfo(doc.dump()), std::invalid_argument);
    }
}
