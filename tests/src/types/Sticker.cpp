#include "tgbot/types/Sticker.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(Sticker, normal)
{
    ASSERT_NO_THROW(tgbot::Sticker(tgbot::Sticker().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["custom_emoji_id"] = "öyohsfg";
    doc["emoji"] = "öyohsfg";
    doc["file_id"] = "soidhfhd";
    doc["file_size"] = 6418;
    doc["file_unique_id"] = "ödhsdfg";
    doc["height"] = -7613254;
    doc["is_animated"] = false;
    doc["is_video"] = true;
    doc["mask_position"] = nlohmann::json::parse(tgbot::MaskPosition().serialise());
    doc["needs_repainting"] = false;
    doc["premium_animation"] = nlohmann::json::parse(tgbot::File().serialise());
    doc["set_name"] = "rgrt";
    doc["thumbnail"] = nlohmann::json::parse(tgbot::PhotoSize().serialise());
    doc["type"] = "roxampois";
    doc["width"] = 8716514;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::Sticker(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::Sticker sticker;

        sticker.set_custom_emoji_id("öyohsfg");
        ASSERT_EQ(sticker.get_custom_emoji_id(), "öyohsfg");

        sticker.set_emoji("öyohsfg");
        ASSERT_EQ(sticker.get_emoji(), "öyohsfg");

        sticker.set_file_id("soidhfhd");
        ASSERT_EQ(sticker.get_file_id(), "soidhfhd");

        sticker.set_file_size(6418);
        ASSERT_EQ(sticker.get_file_size(), 6418);

        sticker.set_file_unique_id("ödhsdfg");
        ASSERT_EQ(sticker.get_file_unique_id(), "ödhsdfg");

        sticker.set_height(-7613254);
        ASSERT_EQ(sticker.get_height(), -7613254);

        sticker.set_is_animated(false);
        ASSERT_EQ(sticker.get_is_animated(), false);

        sticker.set_is_video(true);
        ASSERT_EQ(sticker.get_is_video(), true);

        sticker.set_mask_position(std::make_shared<tgbot::MaskPosition>());

        if(sticker.get_mask_position().has_value())
        {
            ASSERT_EQ(sticker.get_mask_position().value()->serialise(), tgbot::MaskPosition().serialise());
        }

        sticker.set_needs_repainting(false);

        if(sticker.get_needs_repainting().has_value())
        {
            ASSERT_EQ(sticker.get_needs_repainting(), false);
        }

        sticker.set_premium_animation(std::make_shared<tgbot::File>());

        if(sticker.get_premium_animation().has_value())
        {
            ASSERT_EQ(sticker.get_premium_animation().value()->serialise(), tgbot::File().serialise());
        }

        sticker.set_set_name("rgrt");
        ASSERT_EQ(sticker.get_set_name(), "rgrt");

        sticker.set_thumbnail(std::make_shared<tgbot::PhotoSize>());

        if(sticker.get_thumbnail().has_value())
        {
            ASSERT_EQ(sticker.get_thumbnail().value()->serialise(), tgbot::PhotoSize().serialise());
        }

        sticker.set_type("roxampois");
        ASSERT_EQ(sticker.get_type(), "roxampois");

        sticker.set_width(8716514);
        ASSERT_EQ(sticker.get_width(), 8716514);

        ASSERT_EQ(sticker.serialise(), serialised_doc);
    }
}

TEST(Sticker, errors)
{
    ASSERT_THROW(tgbot::Sticker(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 7; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["file_id"] = "aoisjgldfgs";
        }

        if(i != 1)
        {
            doc["file_unique_id"] = "owihf";
        }

        if(i != 2)
        {
            doc["height"] = 87;
        }

        if(i != 3)
        {
            doc["is_animated"] = false;
        }

        if(i != 4)
        {
            doc["is_video"] = false;
        }

        if(i != 5)
        {
            doc["type"] = "rxaponbsrtb";
        }

        if(i != 6)
        {
            doc["width"] = 6844;
        }

        ASSERT_THROW(tgbot::Sticker(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 15; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["custom_emoji_id"] = "fxaohwf" : doc["custom_emoji_id"] = 19573;
        i != 1 ? doc["emoji"] = "wuefhbnjdfb" : doc["emoji"] = 651;
        i != 2 ? doc["file_id"] = "aoisjgldfgs" : doc["file_id"] = 286;
        i != 3 ? doc["file_size"] = 4561 : doc["file_size"] = "sdfhsdrg";
        i != 4 ? doc["file_unique_id"] = "owihf" : doc["file_unique_id"] = 65;
        i != 5 ? doc["height"] = 87 : doc["height"] = "herthsdf";
        i != 6 ? doc["is_animated"] = false : doc["is_animated"] = 73194;
        i != 7 ? doc["is_video"] = false : doc["is_video"] = 4513;
        i != 8 ?
          doc["mask_position"] = nlohmann::json::parse(tgbot::MaskPosition().serialise()) :
          doc["mask_position"] = 468;
        i != 9 ? doc["needs_repainting"] = true : doc["needs_repainting"] = 168765265;
        i != 10 ?
          doc["premium_animation"] = nlohmann::json::parse(tgbot::File().serialise()) :
          doc["premium_animation"] = 3791546;
        i != 11 ? doc["set_name"] = "woöinefmoib" : doc["set_name"] = 616842;
        i != 12 ? doc["thumbnail"] = nlohmann::json::parse(tgbot::PhotoSize().serialise()) : doc["thumbnail"] = 468;
        i != 13 ? doc["type"] = "posnaponjgjdf" : doc["type"] = 44621;
        i != 14 ? doc["width"] = 6844 : doc["width"] = "sdfg";

        ASSERT_THROW(tgbot::Sticker(doc.dump()), std::invalid_argument);
    }
}
