#include "tgbot/types/PassportElementErrorFiles.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(PassportElementErrorFiles, normal)
{
    ASSERT_NO_THROW(tgbot::PassportElementErrorFiles(tgbot::PassportElementErrorFiles().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["file_hashes"] = {"poibjndfb"};
    doc["message"] = "zhpüjsdf";
    doc["source"] = "xŕthpoidfg";
    doc["type"] = "yöijufeh";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::PassportElementErrorFiles(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::PassportElementErrorFiles passport_element_error_files;

        passport_element_error_files.set_file_hashes({"poibjndfb"});
        ASSERT_EQ(passport_element_error_files.get_file_hashes(), std::vector<std::string> {"poibjndfb"});

        passport_element_error_files.set_message("zhpüjsdf");
        ASSERT_EQ(passport_element_error_files.get_message(), "zhpüjsdf");

        passport_element_error_files.set_source("xŕthpoidfg");
        ASSERT_EQ(passport_element_error_files.get_source(), "xŕthpoidfg");

        passport_element_error_files.set_type("yöijufeh");
        ASSERT_EQ(passport_element_error_files.get_type(), "yöijufeh");

        ASSERT_EQ(passport_element_error_files.serialise(), serialised_doc);
    }
}

TEST(PassportElementErrorFiles, errors)
{
    ASSERT_THROW(tgbot::PassportElementErrorFiles(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["file_hashes"] = {"poibjndfb"};
        }

        if(i != 1)
        {
            doc["message"] = "yperoih";
        }

        if(i != 2)
        {
            doc["source"] = "aoisjgldfgs";
        }

        if(i != 3)
        {
            doc["type"] = "owihf";
        }

        ASSERT_THROW(tgbot::PassportElementErrorFiles(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["file_hashes"] = {"poibjndfb"} : doc["file_hashes"] = 65;
        i != 1 ? doc["message"] = "owihf" : doc["message"] = 73157;
        i != 2 ? doc["source"] = "aoisjgldfgs" : doc["source"] = 286;
        i != 3 ? doc["type"] = "owihf" : doc["type"] = 7981357;

        ASSERT_THROW(tgbot::PassportElementErrorFiles(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["file_hashes"] = {762682};
        doc["message"] = "yperoih";
        doc["source"] = "aoisjgldfgs";
        doc["type"] = "owihf";

        ASSERT_THROW(tgbot::PassportElementErrorFiles(doc.dump()), std::invalid_argument);
    }
}
