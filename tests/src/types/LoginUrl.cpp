#include "tgbot/types/LoginUrl.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(LoginUrl, normal)
{
    ASSERT_NO_THROW(tgbot::LoginUrl(tgbot::LoginUrl().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["bot_username"] = "poposijdfgdf";
    doc["forward_text"] = "rfkjbsdfg";
    doc["request_write_access"] = false;
    doc["url"] = "sejgoisdfg";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::LoginUrl(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::LoginUrl login_url;

        login_url.set_bot_username("poposijdfgdf");
        ASSERT_EQ(login_url.get_bot_username(), "poposijdfgdf");

        login_url.set_forward_text("rfkjbsdfg");
        ASSERT_EQ(login_url.get_forward_text(), "rfkjbsdfg");

        login_url.set_request_write_access(false);
        ASSERT_EQ(login_url.get_request_write_access(), false);

        login_url.set_url("sejgoisdfg");
        ASSERT_EQ(login_url.get_url(), "sejgoisdfg");

        ASSERT_EQ(login_url.serialise(), serialised_doc);
    }
}

TEST(LoginUrl, errors)
{
    ASSERT_THROW(tgbot::LoginUrl(nlohmann::json::array().dump()), std::invalid_argument);

    {
        const nlohmann::json doc = nlohmann::json::object();

        ASSERT_THROW(tgbot::LoginUrl(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["bot_username"] = "owihf" : doc["bot_username"] = 65;
        i != 1 ? doc["forward_text"] = "erapcsoij" : doc["forward_text"] = 716654;
        i != 2 ? doc["request_write_access"] = true : doc["request_write_access"] = 71;
        i != 3 ? doc["url"] = "aoisjgldfgs" : doc["url"] = 286;

        ASSERT_THROW(tgbot::LoginUrl(doc.dump()), std::invalid_argument);
    }
}
