#include "tgbot/types/BotCommandScopeAllGroupChats.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(BotCommandScopeAllGroupChats, normal)
{
    ASSERT_NO_THROW(tgbot::BotCommandScopeAllGroupChats(tgbot::BotCommandScopeAllGroupChats().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["type"] = "cwoirejhf";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::BotCommandScopeAllGroupChats(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::BotCommandScopeAllGroupChats bot_command_scope_all_private_chats;

        bot_command_scope_all_private_chats.set_type("cwoirejhf");
        ASSERT_EQ(bot_command_scope_all_private_chats.get_type(), "cwoirejhf");

        ASSERT_EQ(bot_command_scope_all_private_chats.serialise(), serialised_doc);
    }
}

TEST(BotCommandScopeAllGroupChats, errors)
{
    ASSERT_THROW(tgbot::BotCommandScopeAllGroupChats(nlohmann::json::array().dump()), std::invalid_argument);

    {
        const nlohmann::json doc = nlohmann::json::object();

        ASSERT_THROW(tgbot::BotCommandScopeAllGroupChats(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 1; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;

        ASSERT_THROW(tgbot::BotCommandScopeAllGroupChats(doc.dump()), std::invalid_argument);
    }
}
