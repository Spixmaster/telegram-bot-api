#include "tgbot/types/ForumTopic.h"

#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(ForumTopic, normal)
{
    ASSERT_NO_THROW(tgbot::ForumTopic(tgbot::ForumTopic().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["icon_color"] = 91654;
    doc["icon_custom_emoji_id"] = "xpoqrdh";
    doc["message_thread_id"] = 7154651;
    doc["name"] = "qsgwerh";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ForumTopic(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::ForumTopic forum_topic;

        forum_topic.set_icon_color(91654);
        ASSERT_EQ(forum_topic.get_icon_color(), 91654);

        forum_topic.set_icon_custom_emoji_id("xpoqrdh");
        ASSERT_EQ(forum_topic.get_icon_custom_emoji_id(), "xpoqrdh");

        forum_topic.set_message_thread_id(7154651);
        ASSERT_EQ(forum_topic.get_message_thread_id(), 7154651);

        forum_topic.set_name("qsgwerh");
        ASSERT_EQ(forum_topic.get_name(), "qsgwerh");

        ASSERT_EQ(forum_topic.serialise(), serialised_doc);
    }
}

TEST(ForumTopic, errors)
{
    ASSERT_THROW(tgbot::ForumTopic(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["icon_color"] = 49713;
        }

        if(i != 1)
        {
            doc["message_thread_id"] = 79134;
        }

        if(i != 2)
        {
            doc["name"] = "rpuxag";
        }

        ASSERT_THROW(tgbot::ForumTopic(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["icon_color"] = 3168 : doc["icon_color"] = "xoihax";
        i != 1 ? doc["icon_custom_emoji_id"] = "xgaxoiwerh" : doc["icon_custom_emoji_id"] = 7354867;
        i != 2 ? doc["message_thread_id"] = 71646 : doc["message_thread_id"] = "xahxiuh";
        i != 3 ? doc["name"] = "qsgwerh" : doc["name"] = 6546;

        ASSERT_THROW(tgbot::ForumTopic(doc.dump()), std::invalid_argument);
    }
}
