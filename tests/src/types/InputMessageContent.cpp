#include "tgbot/types/InputMessageContent.h"

#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(InputMessageContent, normal)
{
    ASSERT_NO_THROW(tgbot::InputMessageContent(tgbot::InputMessageContent().serialise()));

    const nlohmann::json doc = nlohmann::json::object();

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InputMessageContent(serialised_doc).serialise(), serialised_doc);

    {
        const tgbot::InputMessageContent input_message_content;

        ASSERT_EQ(input_message_content.serialise(), serialised_doc);
    }
}

TEST(InputMessageContent, errors)
{
    ASSERT_THROW(tgbot::InputMessageContent(nlohmann::json::array().dump()), std::invalid_argument);
}
