#include "tgbot/types/Dice.h"

#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(Dice, normal)
{
    ASSERT_NO_THROW(tgbot::Dice(tgbot::Dice().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["emoji"] = "envbsdc";
    doc["value"] = -684168;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::Dice(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::Dice dice;

        dice.set_emoji("envbsdc");
        ASSERT_EQ(dice.get_emoji(), "envbsdc");

        dice.set_value(-684168);
        ASSERT_EQ(dice.get_value(), -684168);

        ASSERT_EQ(dice.serialise(), serialised_doc);
    }
}

TEST(Dice, errors)
{
    ASSERT_THROW(tgbot::Dice(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["emoji"] = "aoisjgldfgs";
        }

        if(i != 1)
        {
            doc["value"] = 468;
        }

        ASSERT_THROW(tgbot::Dice(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["emoji"] = "aoisjgldfgs" : doc["emoji"] = 286;
        i != 1 ? doc["value"] = 31 : doc["value"] = "cwherh";

        ASSERT_THROW(tgbot::Dice(doc.dump()), std::invalid_argument);
    }
}
