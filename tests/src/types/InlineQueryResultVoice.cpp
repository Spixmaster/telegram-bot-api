#include "tgbot/types/InlineQueryResultVoice.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InlineQueryResultVoice, normal)
{
    ASSERT_NO_THROW(tgbot::InlineQueryResultVoice(tgbot::InlineQueryResultVoice().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["caption"] = "tkscvcv";
    doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())};
    doc["id"] = "kijc";
    doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise());
    doc["parse_mode"] = "üpkyxnfg";
    doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise());
    doc["title"] = "rtäpohjsc";
    doc["type"] = "thscsdf";
    doc["voice_duration"] = -85285;
    doc["voice_url"] = "thlaksdf";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InlineQueryResultVoice(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InlineQueryResultVoice inline_query_result_voice;

        inline_query_result_voice.set_caption("tkscvcv");
        ASSERT_EQ(inline_query_result_voice.get_caption(), "tkscvcv");

        inline_query_result_voice.set_caption_entities(std::vector<tgbot::MessageEntity::ptr> {
          std::make_shared<tgbot::MessageEntity>()});

        if(inline_query_result_voice.get_caption_entities().has_value())
        {
            ASSERT_EQ(inline_query_result_voice.get_caption_entities().value().at(0)->serialise(),
                      tgbot::MessageEntity().serialise());
        }

        inline_query_result_voice.set_id("kijc");
        ASSERT_EQ(inline_query_result_voice.get_id(), "kijc");

        inline_query_result_voice.set_input_message_content(std::make_shared<tgbot::InputMessageContent>());

        if(inline_query_result_voice.get_input_message_content().has_value())
        {
            ASSERT_EQ(inline_query_result_voice.get_input_message_content().value()->serialise(),
                      tgbot::InputMessageContent().serialise());
        }

        inline_query_result_voice.set_parse_mode("üpkyxnfg");
        ASSERT_EQ(inline_query_result_voice.get_parse_mode(), "üpkyxnfg");

        inline_query_result_voice.set_reply_markup(std::make_shared<tgbot::InlineKeyboardMarkup>());

        if(inline_query_result_voice.get_reply_markup().has_value())
        {
            ASSERT_EQ(inline_query_result_voice.get_reply_markup().value()->serialise(),
                      tgbot::InlineKeyboardMarkup().serialise());
        }

        inline_query_result_voice.set_title("rtäpohjsc");
        ASSERT_EQ(inline_query_result_voice.get_title(), "rtäpohjsc");

        inline_query_result_voice.set_type("thscsdf");
        ASSERT_EQ(inline_query_result_voice.get_type(), "thscsdf");

        inline_query_result_voice.set_voice_duration(-85285);
        ASSERT_EQ(inline_query_result_voice.get_voice_duration(), -85285);

        inline_query_result_voice.set_voice_url("thlaksdf");
        ASSERT_EQ(inline_query_result_voice.get_voice_url(), "thlaksdf");

        ASSERT_EQ(inline_query_result_voice.serialise(), serialised_doc);
    }
}

TEST(InlineQueryResultVoice, errors)
{
    ASSERT_THROW(tgbot::InlineQueryResultVoice(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["id"] = "owihf";
        }

        if(i != 1)
        {
            doc["title"] = "diopqb";
        }

        if(i != 2)
        {
            doc["type"] = "aoisjgldfgs";
        }

        if(i != 3)
        {
            doc["voice_url"] = "icqun";
        }

        ASSERT_THROW(tgbot::InlineQueryResultVoice(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 10; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["caption"] = "djlhqg" : doc["caption"] = 4168;
        i != 1 ?
          doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())} :
          doc["caption_entities"] = 4168;
        i != 2 ? doc["id"] = "owihf" : doc["id"] = 65;
        i != 3 ?
          doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise()) :
          doc["input_message_content"] = 49;
        i != 4 ? doc["parse_mode"] = "djlhqg" : doc["parse_mode"] = 4168;
        i != 5 ?
          doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise()) :
          doc["reply_markup"] = 49;
        i != 6 ? doc["title"] = "cxpjih" : doc["title"] = 739;
        i != 7 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;
        i != 8 ? doc["voice_duration"] = 915 : doc["voice_duration"] = "qpoifhh";
        i != 9 ? doc["voice_url"] = "opqgd" : doc["voice_url"] = 486;

        ASSERT_THROW(tgbot::InlineQueryResultVoice(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["caption_entities"] = {79154};
        doc["id"] = "owihf";
        doc["title"] = "diopqb";
        doc["type"] = "aoisjgldfgs";
        doc["voice_url"] = "icqun";

        ASSERT_THROW(tgbot::InlineQueryResultVoice(doc.dump()), std::invalid_argument);
    }
}
