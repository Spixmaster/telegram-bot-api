#include "tgbot/types/PassportElementErrorTranslationFile.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(PassportElementErrorTranslationFile, normal)
{
    ASSERT_NO_THROW(
      tgbot::PassportElementErrorTranslationFile(tgbot::PassportElementErrorTranslationFile().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["file_hash"] = "rhskdjfg";
    doc["message"] = "hosijdf";
    doc["source"] = "sdfhsdfg";
    doc["type"] = "hijsdfg";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::PassportElementErrorTranslationFile(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::PassportElementErrorTranslationFile passport_element_error_translation_file;

        passport_element_error_translation_file.set_file_hash("rhskdjfg");
        ASSERT_EQ(passport_element_error_translation_file.get_file_hash(), "rhskdjfg");

        passport_element_error_translation_file.set_message("hosijdf");
        ASSERT_EQ(passport_element_error_translation_file.get_message(), "hosijdf");

        passport_element_error_translation_file.set_source("sdfhsdfg");
        ASSERT_EQ(passport_element_error_translation_file.get_source(), "sdfhsdfg");

        passport_element_error_translation_file.set_type("hijsdfg");
        ASSERT_EQ(passport_element_error_translation_file.get_type(), "hijsdfg");

        ASSERT_EQ(passport_element_error_translation_file.serialise(), serialised_doc);
    }
}

TEST(PassportElementErrorTranslationFile, errors)
{
    ASSERT_THROW(tgbot::PassportElementErrorTranslationFile(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["file_hash"] = "pobunfb";
        }

        if(i != 1)
        {
            doc["message"] = "yperoih";
        }

        if(i != 2)
        {
            doc["source"] = "aoisjgldfgs";
        }

        if(i != 3)
        {
            doc["type"] = "owihf";
        }

        ASSERT_THROW(tgbot::PassportElementErrorTranslationFile(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["file_hash"] = "erhpashsd" : doc["file_hash"] = 671687;
        i != 1 ? doc["message"] = "oiyasd" : doc["message"] = 684357;
        i != 2 ? doc["source"] = "rhoijasv" : doc["source"] = -9815;
        i != 3 ? doc["type"] = "rhoijasv" : doc["type"] = 7335;

        ASSERT_THROW(tgbot::PassportElementErrorTranslationFile(doc.dump()), std::invalid_argument);
    }
}
