#include "tgbot/types/ProximityAlertTriggered.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(ProximityAlertTriggered, normal)
{
    ASSERT_NO_THROW(tgbot::ProximityAlertTriggered(tgbot::ProximityAlertTriggered().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["distance"] = 6828518;
    doc["traveler"] = nlohmann::json::parse(tgbot::User().serialise());
    doc["watcher"] = nlohmann::json::parse(tgbot::User().serialise());

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ProximityAlertTriggered(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::ProximityAlertTriggered proximity_alert_triggered;

        proximity_alert_triggered.set_distance(6828518);
        ASSERT_EQ(proximity_alert_triggered.get_distance(), 6828518);

        proximity_alert_triggered.set_traveler(std::make_shared<tgbot::User>());
        ASSERT_EQ(proximity_alert_triggered.get_traveler()->serialise(), tgbot::User().serialise());

        proximity_alert_triggered.set_watcher(std::make_shared<tgbot::User>());
        ASSERT_EQ(proximity_alert_triggered.get_watcher()->serialise(), tgbot::User().serialise());

        ASSERT_EQ(proximity_alert_triggered.serialise(), serialised_doc);
    }
}

TEST(ProximityAlertTriggered, errors)
{
    ASSERT_THROW(tgbot::ProximityAlertTriggered(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["distance"] = 6844;
        }

        if(i != 1)
        {
            doc["traveler"] = nlohmann::json::parse(tgbot::User().serialise());
        }

        if(i != 2)
        {
            doc["watcher"] = nlohmann::json::parse(tgbot::User().serialise());
        }

        ASSERT_THROW(tgbot::ProximityAlertTriggered(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["distance"] = 87 : doc["distance"] = "herthsdf";
        i != 1 ? doc["traveler"] = nlohmann::json::parse(tgbot::User().serialise()) : doc["traveler"] = 9943517;
        i != 2 ? doc["watcher"] = nlohmann::json::parse(tgbot::User().serialise()) : doc["watcher"] = 286;

        ASSERT_THROW(tgbot::ProximityAlertTriggered(doc.dump()), std::invalid_argument);
    }
}
