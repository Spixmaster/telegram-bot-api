#include "tgbot/types/ChatAdministratorRights.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(ChatAdministratorRights, normal)
{
    ASSERT_NO_THROW(tgbot::ChatAdministratorRights(tgbot::ChatAdministratorRights().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["can_change_info"] = false;
    doc["can_delete_messages"] = true;
    doc["can_edit_messages"] = false;
    doc["can_invite_users"] = true;
    doc["can_manage_chat"] = false;
    doc["can_manage_topics"] = false;
    doc["can_manage_video_chats"] = true;
    doc["can_pin_messages"] = false;
    doc["can_post_messages"] = true;
    doc["can_promote_members"] = true;
    doc["can_restrict_members"] = false;
    doc["is_anonymous"] = false;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ChatAdministratorRights(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::ChatAdministratorRights chat_administrator_rights;

        chat_administrator_rights.set_can_change_info(false);
        ASSERT_EQ(chat_administrator_rights.get_can_change_info(), false);

        chat_administrator_rights.set_can_delete_messages(true);
        ASSERT_EQ(chat_administrator_rights.get_can_delete_messages(), true);

        chat_administrator_rights.set_can_edit_messages(false);
        ASSERT_EQ(chat_administrator_rights.get_can_edit_messages(), false);

        chat_administrator_rights.set_can_invite_users(true);
        ASSERT_EQ(chat_administrator_rights.get_can_invite_users(), true);

        chat_administrator_rights.set_can_manage_chat(false);
        ASSERT_EQ(chat_administrator_rights.get_can_manage_chat(), false);

        chat_administrator_rights.set_can_manage_topics(false);
        ASSERT_EQ(chat_administrator_rights.get_can_manage_topics(), false);

        chat_administrator_rights.set_can_manage_video_chats(true);
        ASSERT_EQ(chat_administrator_rights.get_can_manage_video_chats(), true);

        chat_administrator_rights.set_can_pin_messages(false);
        ASSERT_EQ(chat_administrator_rights.get_can_pin_messages(), false);

        chat_administrator_rights.set_can_post_messages(true);
        ASSERT_EQ(chat_administrator_rights.get_can_post_messages(), true);

        chat_administrator_rights.set_can_promote_members(true);
        ASSERT_EQ(chat_administrator_rights.get_can_promote_members(), true);

        chat_administrator_rights.set_can_restrict_members(false);
        ASSERT_EQ(chat_administrator_rights.get_can_restrict_members(), false);

        chat_administrator_rights.set_is_anonymous(false);
        ASSERT_EQ(chat_administrator_rights.get_is_anonymous(), false);

        ASSERT_EQ(chat_administrator_rights.serialise(), serialised_doc);
    }
}

TEST(ChatAdministratorRights, errors)
{
    ASSERT_THROW(tgbot::ChatAdministratorRights(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 8; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["can_change_info"] = true;
        }

        if(i != 1)
        {
            doc["can_delete_messages"] = false;
        }

        if(i != 2)
        {
            doc["can_invite_users"] = true;
        }

        if(i != 3)
        {
            doc["can_manage_chat"] = false;
        }

        if(i != 4)
        {
            doc["can_manage_video_chats"] = true;
        }

        if(i != 5)
        {
            doc["can_promote_members"] = true;
        }

        if(i != 6)
        {
            doc["can_restrict_members"] = false;
        }

        if(i != 7)
        {
            doc["is_anonymous"] = true;
        }

        ASSERT_THROW(tgbot::ChatAdministratorRights(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 12; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["can_change_info"] = false : doc["can_change_info"] = 91645;
        i != 1 ? doc["can_delete_messages"] = false : doc["can_delete_messages"] = 4915;
        i != 2 ? doc["can_edit_messages"] = true : doc["can_edit_messages"] = 668151;
        i != 3 ? doc["can_invite_users"] = false : doc["can_invite_users"] = 716;
        i != 4 ? doc["can_manage_chat"] = true : doc["can_manage_chat"] = 3218;
        i != 5 ? doc["can_manage_topics"] = false : doc["can_manage_topics"] = 9715;
        i != 6 ? doc["can_manage_video_chats"] = true : doc["can_manage_video_chats"] = 79215;
        i != 7 ? doc["can_pin_messages"] = true : doc["can_pin_messages"] = 616;
        i != 8 ? doc["can_post_messages"] = false : doc["can_post_messages"] = 8168;
        i != 9 ? doc["can_promote_members"] = true : doc["can_promote_members"] = 934;
        i != 10 ? doc["can_restrict_members"] = true : doc["can_restrict_members"] = 92561;
        i != 11 ? doc["is_anonymous"] = false : doc["is_anonymous"] = 894561;

        ASSERT_THROW(tgbot::ChatAdministratorRights(doc.dump()), std::invalid_argument);
    }
}
