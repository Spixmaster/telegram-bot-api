#include "tgbot/types/InputMediaDocument.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InputMediaDocument, normal)
{
    ASSERT_NO_THROW(tgbot::InputMediaDocument(tgbot::InputMediaDocument().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["caption"] = "cskdfhc";
    doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())};
    doc["disable_content_type_detection"] = false;
    doc["media"] = "cölkdhfs";
    doc["parse_mode"] = "cöofcc";
    doc["thumbnail"] = "ofölbe";
    doc["type"] = "cxosjndfh";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InputMediaDocument(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InputMediaDocument input_media_document;

        input_media_document.set_caption("cskdfhc");
        ASSERT_EQ(input_media_document.get_caption(), "cskdfhc");

        input_media_document.set_caption_entities(std::vector<tgbot::MessageEntity::ptr> {
          std::make_shared<tgbot::MessageEntity>()});

        if(input_media_document.get_caption_entities().has_value())
        {
            ASSERT_EQ(input_media_document.get_caption_entities().value().at(0)->serialise(),
                      tgbot::MessageEntity().serialise());
        }

        input_media_document.set_disable_content_type_detection(false);
        ASSERT_EQ(input_media_document.get_disable_content_type_detection(), false);

        input_media_document.set_media("cölkdhfs");
        ASSERT_EQ(input_media_document.get_media(), "cölkdhfs");

        input_media_document.set_parse_mode("cöofcc");
        ASSERT_EQ(input_media_document.get_parse_mode(), "cöofcc");

        input_media_document.set_thumbnail("ofölbe");
        ASSERT_EQ(input_media_document.get_thumbnail(), "ofölbe");

        input_media_document.set_type("cxosjndfh");
        ASSERT_EQ(input_media_document.get_type(), "cxosjndfh");

        ASSERT_EQ(input_media_document.serialise(), serialised_doc);
    }
}

TEST(InputMediaDocument, errors)
{
    ASSERT_THROW(tgbot::InputMediaDocument(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["media"] = "owihf";
        }

        if(i != 1)
        {
            doc["type"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::InputMediaDocument(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 7; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["caption"] = "djlhqg" : doc["caption"] = 4168;
        i != 1 ?
          doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())} :
          doc["caption_entities"] = 4168;
        i != 2 ? doc["disable_content_type_detection"] = true : doc["disable_content_type_detection"] = "cih";
        i != 3 ? doc["media"] = "owihf" : doc["media"] = 65;
        i != 4 ? doc["parse_mode"] = "djlhqg" : doc["parse_mode"] = 4168;
        i != 5 ? doc["thumbnail"] = "opqgd" : doc["thumbnail"] = 486;
        i != 6 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;

        ASSERT_THROW(tgbot::InputMediaDocument(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["caption_entities"] = {7918249};
        doc["media"] = "owihf";
        doc["type"] = "aoisjgldfgs";

        ASSERT_THROW(tgbot::InputMediaDocument(doc.dump()), std::invalid_argument);
    }
}
