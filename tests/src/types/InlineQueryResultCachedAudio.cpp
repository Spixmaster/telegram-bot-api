#include "tgbot/types/InlineQueryResultCachedAudio.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"
#include "tgbot/types/LoginUrl.h"

TEST(InlineQueryResultCachedAudio, normal)
{
    ASSERT_NO_THROW(tgbot::InlineQueryResultCachedAudio(tgbot::InlineQueryResultCachedAudio().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["audio_file_id"] = "cbpmnudfio";
    doc["caption"] = "cbopiwufh";
    doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())};
    doc["id"] = "cbosunhdfiug";
    doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise());
    doc["parse_mode"] = "vbpomijnfeg";
    doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise());
    doc["type"] = "comnishdf";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InlineQueryResultCachedAudio(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InlineQueryResultCachedAudio inline_query_result_cached_audio;

        inline_query_result_cached_audio.set_audio_file_id("cbpmnudfio");
        ASSERT_EQ(inline_query_result_cached_audio.get_audio_file_id(), "cbpmnudfio");

        inline_query_result_cached_audio.set_caption("cbopiwufh");
        ASSERT_EQ(inline_query_result_cached_audio.get_caption(), "cbopiwufh");

        inline_query_result_cached_audio.set_caption_entities(std::vector<tgbot::MessageEntity::ptr> {
          std::make_shared<tgbot::MessageEntity>()});

        if(inline_query_result_cached_audio.get_caption_entities().has_value())
        {
            ASSERT_EQ(inline_query_result_cached_audio.get_caption_entities().value().at(0)->serialise(),
                      tgbot::MessageEntity().serialise());
        }

        inline_query_result_cached_audio.set_id("cbosunhdfiug");
        ASSERT_EQ(inline_query_result_cached_audio.get_id(), "cbosunhdfiug");

        inline_query_result_cached_audio.set_input_message_content(std::make_shared<tgbot::InputMessageContent>());

        if(inline_query_result_cached_audio.get_input_message_content().has_value())
        {
            ASSERT_EQ(inline_query_result_cached_audio.get_input_message_content().value()->serialise(),
                      tgbot::InputMessageContent().serialise());
        }

        inline_query_result_cached_audio.set_parse_mode("vbpomijnfeg");
        ASSERT_EQ(inline_query_result_cached_audio.get_parse_mode(), "vbpomijnfeg");

        inline_query_result_cached_audio.set_reply_markup(std::make_shared<tgbot::InlineKeyboardMarkup>());

        if(inline_query_result_cached_audio.get_reply_markup())
        {
            ASSERT_EQ(inline_query_result_cached_audio.get_reply_markup().value()->serialise(),
                      tgbot::InlineKeyboardMarkup().serialise());
        }

        inline_query_result_cached_audio.set_type("comnishdf");
        ASSERT_EQ(inline_query_result_cached_audio.get_type(), "comnishdf");

        ASSERT_EQ(inline_query_result_cached_audio.serialise(), serialised_doc);
    }
}

TEST(InlineQueryResultCachedAudio, errors)
{
    ASSERT_THROW(tgbot::InlineQueryResultCachedAudio(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["audio_file_id"] = "icqun";
        }

        if(i != 1)
        {
            doc["id"] = "owihf";
        }

        if(i != 2)
        {
            doc["type"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::InlineQueryResultCachedAudio(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 8; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["audio_file_id"] = "opqgd" : doc["audio_file_id"] = 486;
        i != 1 ? doc["caption"] = "djlhqg" : doc["caption"] = 4168;
        i != 2 ?
          doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())} :
          doc["caption_entities"] = 4168;
        i != 3 ? doc["id"] = "qwpoigjqer" : doc["id"] = 65;
        i != 4 ?
          doc["input_message_content"] = nlohmann::json::parse(tgbot::LoginUrl().serialise()) :
          doc["input_message_content"] = 49;
        i != 5 ? doc["parse_mode"] = "djlhqg" : doc["parse_mode"] = 4168;
        i != 6 ?
          doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise()) :
          doc["reply_markup"] = 49;
        i != 7 ? doc["type"] = "qwegopihoidf" : doc["type"] = 286;

        ASSERT_THROW(tgbot::InlineQueryResultCachedAudio(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["audio_file_id"] = "icqun";
        doc["caption_entities"] = {7426447};
        doc["id"] = "rezüijsdf";
        doc["type"] = "rpojkjwcdsn";

        ASSERT_THROW(tgbot::InlineQueryResultCachedAudio(doc.dump()), std::invalid_argument);
    }
}
