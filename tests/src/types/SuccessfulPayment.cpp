#include "tgbot/types/SuccessfulPayment.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(SuccessfulPayment, normal)
{
    ASSERT_NO_THROW(tgbot::SuccessfulPayment(tgbot::SuccessfulPayment().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["currency"] = "xlhpousdf";
    doc["invoice_payload"] = "öoisdf";
    doc["order_info"] = nlohmann::json::parse(tgbot::OrderInfo().serialise());
    doc["provider_payment_charge_id"] = "eofjbksdf";
    doc["shipping_option_id"] = "ribkjsdf";
    doc["telegram_payment_charge_id"] = "rokbjsodjfg";
    doc["total_amount"] = 7671651;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::SuccessfulPayment(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::SuccessfulPayment successful_payment;

        successful_payment.set_currency("xlhpousdf");
        ASSERT_EQ(successful_payment.get_currency(), "xlhpousdf");

        successful_payment.set_invoice_payload("öoisdf");
        ASSERT_EQ(successful_payment.get_invoice_payload(), "öoisdf");

        successful_payment.set_order_info(std::make_shared<tgbot::OrderInfo>());

        if(successful_payment.get_order_info().has_value())
        {
            ASSERT_EQ(successful_payment.get_order_info().value()->serialise(), tgbot::OrderInfo().serialise());
        }

        successful_payment.set_provider_payment_charge_id("eofjbksdf");
        ASSERT_EQ(successful_payment.get_provider_payment_charge_id(), "eofjbksdf");

        successful_payment.set_shipping_option_id("ribkjsdf");
        ASSERT_EQ(successful_payment.get_shipping_option_id(), "ribkjsdf");

        successful_payment.set_telegram_payment_charge_id("rokbjsodjfg");
        ASSERT_EQ(successful_payment.get_telegram_payment_charge_id(), "rokbjsodjfg");

        successful_payment.set_total_amount(7671651);
        ASSERT_EQ(successful_payment.get_total_amount(), 7671651);

        ASSERT_EQ(successful_payment.serialise(), serialised_doc);
    }
}

TEST(SuccessfulPayment, errors)
{
    ASSERT_THROW(tgbot::SuccessfulPayment(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 5; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["currency"] = "aoisjgldfgs";
        }

        if(i != 1)
        {
            doc["invoice_payload"] = "cpiuhhr";
        }

        if(i != 2)
        {
            doc["provider_payment_charge_id"] = "cmnsbcmbv";
        }

        if(i != 3)
        {
            doc["telegram_payment_charge_id"] = "wpeuhwpun";
        }

        if(i != 4)
        {
            doc["total_amount"] = 984;
        }

        ASSERT_THROW(tgbot::SuccessfulPayment(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 7; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["currency"] = "aoisjgldfgs" : doc["currency"] = 286;
        i != 1 ? doc["invoice_payload"] = "cpwuhbxsa" : doc["invoice_payload"] = 97841;
        i != 2 ? doc["order_info"] = nlohmann::json::parse(tgbot::OrderInfo().serialise()) : doc["order_info"] = 468;
        i != 3 ? doc["provider_payment_charge_id"] = "woöinefmoib" : doc["provider_payment_charge_id"] = 616842;
        i != 4 ? doc["shipping_option_id"] = "dpowiurf" : doc["shipping_option_id"] = 984;
        i != 5 ? doc["telegram_payment_charge_id"] = "wuefhbnjdfb" : doc["telegram_payment_charge_id"] = 651;
        i != 6 ? doc["total_amount"] = 981 : doc["total_amount"] = "cknpeut";

        ASSERT_THROW(tgbot::SuccessfulPayment(doc.dump()), std::invalid_argument);
    }
}
