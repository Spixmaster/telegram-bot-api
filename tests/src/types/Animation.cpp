#include "tgbot/types/Animation.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

/*
 * Test patterns:
 * 1. Test name "normal"
 * 1.1 Serialise an object with no passed arguments to the constructor and create another object of the same type from
 * it.
 * 1.2 Create a JSON object and pass it to the proper constructor. Compare the result of serialise() with the origin.
 * 1.3 Set values via setters. Check the getters and compare the result of serialise() with the origin.
 * 1.4 (optional) Cover all remaining cases.
 *
 * 2. Test name "errors"
 * 2.1 Pass a JSON array to the constructor.
 * 2.2 Pass the constructor several JSON objects where necessary fields are missing.
 * 2.3 Pass the constructor several JSON objects where the types are wrong.
 * 2.4 (optional) Cover all remaining cases.
 */

TEST(Animation, normal)
{
    ASSERT_NO_THROW(tgbot::Animation(tgbot::Animation().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["duration"] = 7981684;
    doc["file_id"] = "aoisjgldfgs";
    doc["file_name"] = "wuefhbnjdfb";
    doc["file_size"] = 73518;
    doc["file_unique_id"] = "owihf";
    doc["height"] = 87;
    doc["mime_type"] = "woöinefmoib";
    doc["thumbnail"] = nlohmann::json::parse(tgbot::PhotoSize().serialise());
    doc["width"] = 6844;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::Animation(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::Animation animation;

        animation.set_duration(7981684);
        ASSERT_EQ(animation.get_duration(), 7981684);

        animation.set_file_id("aoisjgldfgs");
        ASSERT_EQ(animation.get_file_id(), "aoisjgldfgs");

        animation.set_file_name("wuefhbnjdfb");
        ASSERT_EQ(animation.get_file_name(), "wuefhbnjdfb");

        animation.set_file_size(73518);
        ASSERT_EQ(animation.get_file_size(), 73518);

        animation.set_file_unique_id("owihf");
        ASSERT_EQ(animation.get_file_unique_id(), "owihf");

        animation.set_height(87);
        ASSERT_EQ(animation.get_height(), 87);

        animation.set_mime_type("woöinefmoib");
        ASSERT_EQ(animation.get_mime_type(), "woöinefmoib");

        animation.set_thumbnail(std::make_shared<tgbot::PhotoSize>());

        if(animation.get_thumbnail().has_value())
        {
            ASSERT_EQ(animation.get_thumbnail().value()->serialise(), tgbot::PhotoSize().serialise());
        }

        animation.set_width(6844);
        ASSERT_EQ(animation.get_width(), 6844);

        ASSERT_EQ(animation.serialise(), serialised_doc);
    }
}

TEST(Animation, errors)
{
    ASSERT_THROW(tgbot::Animation(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 5; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["duration"] = 7981684;
        }

        if(i != 1)
        {
            doc["file_id"] = "aoisjgldfgs";
        }

        if(i != 2)
        {
            doc["file_unique_id"] = "owihf";
        }

        if(i != 3)
        {
            doc["height"] = 87;
        }

        if(i != 4)
        {
            doc["width"] = 6844;
        }

        ASSERT_THROW(tgbot::Animation(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 9; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["duration"] = 7981684 : doc["duration"] = "bsefbsdfh";
        i != 1 ? doc["file_id"] = "aoisjgldfgs" : doc["file_id"] = 286;
        i != 2 ? doc["file_name"] = "wuefhbnjdfb" : doc["file_name"] = 651;
        i != 3 ? doc["file_size"] = 4561 : doc["file_size"] = "sdfhsdrg";
        i != 4 ? doc["file_unique_id"] = "owihf" : doc["file_unique_id"] = 65;
        i != 5 ? doc["height"] = 87 : doc["height"] = "herthsdf";
        i != 6 ? doc["mime_type"] = "woöinefmoib" : doc["mime_type"] = 616842;
        i != 7 ? doc["thumbnail"] = nlohmann::json::parse(tgbot::PhotoSize().serialise()) : doc["thumbnail"] = 468;
        i != 8 ? doc["width"] = 6844 : doc["width"] = "sdfg";

        ASSERT_THROW(tgbot::Animation(doc.dump()), std::invalid_argument);
    }
}
