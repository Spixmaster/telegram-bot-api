#include "tgbot/types/ForumTopicClosed.h"

#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(ForumTopicClosed, normal)
{
    ASSERT_NO_THROW(tgbot::ForumTopicClosed(tgbot::ForumTopicClosed().serialise()));

    const nlohmann::json doc = nlohmann::json::object();

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ForumTopicClosed(serialised_doc).serialise(), serialised_doc);

    {
        const tgbot::ForumTopicClosed forum_topic_closed;

        ASSERT_EQ(forum_topic_closed.serialise(), serialised_doc);
    }
}

TEST(ForumTopicClosed, errors)
{
    ASSERT_THROW(tgbot::ForumTopicClosed(nlohmann::json::array().dump()), std::invalid_argument);
}
