#include "tgbot/types/MenuButtonDefault.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(MenuButtonDefault, normal)
{
    ASSERT_NO_THROW(tgbot::MenuButtonDefault(tgbot::MenuButtonDefault().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["type"] = "prpoaeropi";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::MenuButtonDefault(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::MenuButtonDefault menu_button_default;

        menu_button_default.set_type("prpoaeropi");
        ASSERT_EQ(menu_button_default.get_type(), "prpoaeropi");

        ASSERT_EQ(menu_button_default.serialise(), serialised_doc);
    }
}

TEST(MenuButtonDefault, errors)
{
    ASSERT_THROW(tgbot::MenuButtonDefault(nlohmann::json::array().dump()), std::invalid_argument);

    {
        const nlohmann::json doc = nlohmann::json::object();

        ASSERT_THROW(tgbot::MenuButtonDefault(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 1; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["type"] = "üoinih" : doc["type"] = 79346;

        ASSERT_THROW(tgbot::MenuButtonDefault(doc.dump()), std::invalid_argument);
    }
}
