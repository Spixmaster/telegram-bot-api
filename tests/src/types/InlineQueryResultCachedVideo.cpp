#include "tgbot/types/InlineQueryResultCachedVideo.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InlineQueryResultCachedVideo, normal)
{
    ASSERT_NO_THROW(tgbot::InlineQueryResultCachedVideo(tgbot::InlineQueryResultCachedVideo().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["caption"] = "cüpüosfdg";
    doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())};
    doc["description"] = "tphoijmsfg";
    doc["id"] = "pomijsadfg";
    doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise());
    doc["parse_mode"] = "womposf";
    doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise());
    doc["title"] = "wfoimjbscg";
    doc["type"] = "xljmoiafdsg";
    doc["video_file_id"] = "oqimcgs";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InlineQueryResultCachedVideo(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InlineQueryResultCachedVideo inline_query_result_cached_video;

        inline_query_result_cached_video.set_caption("cüpüosfdg");
        ASSERT_EQ(inline_query_result_cached_video.get_caption(), "cüpüosfdg");

        inline_query_result_cached_video.set_caption_entities(std::vector<tgbot::MessageEntity::ptr> {
          std::make_shared<tgbot::MessageEntity>()});

        if(inline_query_result_cached_video.get_caption_entities().has_value())
        {
            ASSERT_EQ(inline_query_result_cached_video.get_caption_entities().value().at(0)->serialise(),
                      tgbot::MessageEntity().serialise());
        }

        inline_query_result_cached_video.set_description("tphoijmsfg");
        ASSERT_EQ(inline_query_result_cached_video.get_description(), "tphoijmsfg");

        inline_query_result_cached_video.set_id("pomijsadfg");
        ASSERT_EQ(inline_query_result_cached_video.get_id(), "pomijsadfg");

        inline_query_result_cached_video.set_input_message_content(std::make_shared<tgbot::InputMessageContent>());

        if(inline_query_result_cached_video.get_input_message_content().has_value())
        {
            ASSERT_EQ(inline_query_result_cached_video.get_input_message_content().value()->serialise(),
                      tgbot::InputMessageContent().serialise());
        }

        inline_query_result_cached_video.set_parse_mode("womposf");
        ASSERT_EQ(inline_query_result_cached_video.get_parse_mode(), "womposf");

        inline_query_result_cached_video.set_reply_markup(std::make_shared<tgbot::InlineKeyboardMarkup>());

        if(inline_query_result_cached_video.get_reply_markup().has_value())
        {
            ASSERT_EQ(inline_query_result_cached_video.get_reply_markup().value()->serialise(),
                      tgbot::InlineKeyboardMarkup().serialise());
        }

        inline_query_result_cached_video.set_title("wfoimjbscg");
        ASSERT_EQ(inline_query_result_cached_video.get_title(), "wfoimjbscg");

        inline_query_result_cached_video.set_type("xljmoiafdsg");
        ASSERT_EQ(inline_query_result_cached_video.get_type(), "xljmoiafdsg");

        inline_query_result_cached_video.set_video_file_id("oqimcgs");
        ASSERT_EQ(inline_query_result_cached_video.get_video_file_id(), "oqimcgs");

        ASSERT_EQ(inline_query_result_cached_video.serialise(), serialised_doc);
    }
}

TEST(InlineQueryResultCachedVideo, errors)
{
    ASSERT_THROW(tgbot::InlineQueryResultCachedVideo(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["id"] = "owihf";
        }

        if(i != 1)
        {
            doc["title"] = "diopqb";
        }

        if(i != 2)
        {
            doc["type"] = "aoisjgldfgs";
        }

        if(i != 3)
        {
            doc["video_file_id"] = "icqun";
        }

        ASSERT_THROW(tgbot::InlineQueryResultCachedVideo(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 10; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["caption"] = "djlhqg" : doc["caption"] = 4168;
        i != 1 ?
          doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())} :
          doc["caption_entities"] = 4168;
        i != 2 ? doc["description"] = "cxpjih" : doc["description"] = 739;
        i != 3 ? doc["id"] = "owihf" : doc["id"] = 65;
        i != 4 ?
          doc["input_message_content"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise()) :
          doc["input_message_content"] = 49;
        i != 5 ? doc["parse_mode"] = "djlhqg" : doc["parse_mode"] = 4168;
        i != 6 ?
          doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise()) :
          doc["reply_markup"] = 49;
        i != 7 ? doc["title"] = "cxpjih" : doc["title"] = 739;
        i != 8 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;
        i != 9 ? doc["video_file_id"] = "opqgd" : doc["video_file_id"] = 486;

        ASSERT_THROW(tgbot::InlineQueryResultCachedVideo(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["caption_entities"] = {58642648};
        doc["id"] = "owihf";
        doc["title"] = "diopqb";
        doc["type"] = "aoisjgldfgs";
        doc["video_file_id"] = "icqun";

        ASSERT_THROW(tgbot::InlineQueryResultCachedVideo(doc.dump()), std::invalid_argument);
    }
}
