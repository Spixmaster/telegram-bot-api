#include "tgbot/types/PassportElementErrorDataField.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(PassportElementErrorDataField, normal)
{
    ASSERT_NO_THROW(tgbot::PassportElementErrorDataField(tgbot::PassportElementErrorDataField().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["data_hash"] = "thiomx";
    doc["field_name"] = "okknmq";
    doc["message"] = "rthoijsdf";
    doc["source"] = "thsjdf";
    doc["type"] = "bmmyids";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::PassportElementErrorDataField(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::PassportElementErrorDataField passport_element_error_data_field;

        passport_element_error_data_field.set_data_hash("thiomx");
        ASSERT_EQ(passport_element_error_data_field.get_data_hash(), "thiomx");

        passport_element_error_data_field.set_field_name("okknmq");
        ASSERT_EQ(passport_element_error_data_field.get_field_name(), "okknmq");

        passport_element_error_data_field.set_message("rthoijsdf");
        ASSERT_EQ(passport_element_error_data_field.get_message(), "rthoijsdf");

        passport_element_error_data_field.set_source("thsjdf");
        ASSERT_EQ(passport_element_error_data_field.get_source(), "thsjdf");

        passport_element_error_data_field.set_type("bmmyids");
        ASSERT_EQ(passport_element_error_data_field.get_type(), "bmmyids");

        ASSERT_EQ(passport_element_error_data_field.serialise(), serialised_doc);
    }
}

TEST(PassportElementErrorDataField, errors)
{
    ASSERT_THROW(tgbot::PassportElementErrorDataField(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 5; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["data_hash"] = "mpihfbf";
        }

        if(i != 1)
        {
            doc["field_name"] = "pobunfb";
        }

        if(i != 2)
        {
            doc["message"] = "yperoih";
        }

        if(i != 3)
        {
            doc["source"] = "aoisjgldfgs";
        }

        if(i != 4)
        {
            doc["type"] = "owihf";
        }

        ASSERT_THROW(tgbot::PassportElementErrorDataField(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 5; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["data_hash"] = "qproeigjsdf" : doc["data_hash"] = 7387;
        i != 1 ? doc["field_name"] = "owihf" : doc["field_name"] = 91651354;
        i != 2 ? doc["message"] = "wergpoijs" : doc["message"] = 15974154;
        i != 3 ? doc["source"] = "aoisjgldfgs" : doc["source"] = 9517;
        i != 4 ? doc["type"] = "qoijdf" : doc["type"] = 731597;

        ASSERT_THROW(tgbot::PassportElementErrorDataField(doc.dump()), std::invalid_argument);
    }
}
