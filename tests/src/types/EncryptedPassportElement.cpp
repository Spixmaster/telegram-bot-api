#include "tgbot/types/EncryptedPassportElement.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(EncryptedPassportElement, normal)
{
    ASSERT_NO_THROW(tgbot::EncryptedPassportElement(tgbot::EncryptedPassportElement().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["data"] = "pomwsf";
    doc["email"] = "thwöoimj";
    doc["files"] = {nlohmann::json::parse(tgbot::PassportFile().serialise())};
    doc["front_side"] = nlohmann::json::parse(tgbot::PassportFile().serialise());
    doc["hash"] = "qpioujcb";
    doc["phone_number"] = "cbaliusndg";
    doc["reverse_side"] = nlohmann::json::parse(tgbot::PassportFile().serialise());
    doc["selfie"] = nlohmann::json::parse(tgbot::PassportFile().serialise());
    doc["translation"] = {nlohmann::json::parse(tgbot::PassportFile().serialise())};
    doc["type"] = "cpojusfdg";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::EncryptedPassportElement(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::EncryptedPassportElement encrypted_passport_element;

        encrypted_passport_element.set_data("pomwsf");
        ASSERT_EQ(encrypted_passport_element.get_data(), "pomwsf");

        encrypted_passport_element.set_email("thwöoimj");
        ASSERT_EQ(encrypted_passport_element.get_email(), "thwöoimj");

        encrypted_passport_element.set_files(std::vector<tgbot::PassportFile::ptr> {
          std::make_shared<tgbot::PassportFile>()});

        if(encrypted_passport_element.get_files().has_value())
        {
            ASSERT_EQ(
              encrypted_passport_element.get_files().value().at(0)->serialise(), tgbot::PassportFile().serialise());
        }

        encrypted_passport_element.set_front_side(std::make_shared<tgbot::PassportFile>());

        if(encrypted_passport_element.get_front_side().has_value())
        {
            ASSERT_EQ(
              encrypted_passport_element.get_front_side().value()->serialise(), tgbot::PassportFile().serialise());
        }

        encrypted_passport_element.set_hash("qpioujcb");
        ASSERT_EQ(encrypted_passport_element.get_hash(), "qpioujcb");

        encrypted_passport_element.set_phone_number("cbaliusndg");
        ASSERT_EQ(encrypted_passport_element.get_phone_number(), "cbaliusndg");

        encrypted_passport_element.set_reverse_side(std::make_shared<tgbot::PassportFile>());

        if(encrypted_passport_element.get_reverse_side().has_value())
        {
            ASSERT_EQ(
              encrypted_passport_element.get_reverse_side().value()->serialise(), tgbot::PassportFile().serialise());
        }

        encrypted_passport_element.set_selfie(std::make_shared<tgbot::PassportFile>());

        if(encrypted_passport_element.get_selfie().has_value())
        {
            ASSERT_EQ(encrypted_passport_element.get_selfie().value()->serialise(), tgbot::PassportFile().serialise());
        }

        encrypted_passport_element.set_translation(std::vector<tgbot::PassportFile::ptr> {
          std::make_shared<tgbot::PassportFile>()});

        if(encrypted_passport_element.get_translation().has_value())
        {
            ASSERT_EQ(encrypted_passport_element.get_translation().value().at(0)->serialise(),
                      tgbot::PassportFile().serialise());
        }

        encrypted_passport_element.set_type("cpojusfdg");
        ASSERT_EQ(encrypted_passport_element.get_type(), "cpojusfdg");

        ASSERT_EQ(encrypted_passport_element.serialise(), serialised_doc);
    }
}

TEST(EncryptedPassportElement, errors)
{
    ASSERT_THROW(tgbot::EncryptedPassportElement(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["hash"] = "aoisjgldfgs";
        }

        if(i != 1)
        {
            doc["type"] = "reqgpsovd";
        }

        ASSERT_THROW(tgbot::EncryptedPassportElement(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 10; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["data"] = "qwfgopij" : doc["data"] = 65;
        i != 1 ? doc["email"] = "copiw" : doc["email"] = 684;
        i != 2 ? doc["files"] = {nlohmann::json::parse(tgbot::PassportFile().serialise())} : doc["files"] = 4;
        i != 3 ? doc["front_side"] = nlohmann::json::parse(tgbot::PassportFile().serialise()) : doc["front_side"] = 468;
        i != 4 ? doc["hash"] = "wuefhbnjdfb" : doc["hash"] = 651;
        i != 5 ? doc["phone_number"] = "ofpwft" : doc["phone_number"] = 497;
        i != 6 ?
          doc["reverse_side"] = nlohmann::json::parse(tgbot::PassportFile().serialise()) :
          doc["reverse_side"] = 6841;
        i != 7 ? doc["selfie"] = nlohmann::json::parse(tgbot::PassportFile().serialise()) : doc["selfie"] = 468;
        i != 8 ?
          doc["translation"] = {nlohmann::json::parse(tgbot::PassportFile().serialise())} :
          doc["translation"] = 76465;
        i != 9 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;

        ASSERT_THROW(tgbot::EncryptedPassportElement(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i == 0)
        {
            doc["files"] = {413242};
        }

        doc["hash"] = "qwgopijsad";

        if(i == 1)
        {
            doc["translation"] = {684};
        }

        doc["type"] = "qpioujcb";

        ASSERT_THROW(tgbot::EncryptedPassportElement(doc.dump()), std::invalid_argument);
    }
}
