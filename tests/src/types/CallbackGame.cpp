#include "tgbot/types/CallbackGame.h"

#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(CallbackGame, normal)
{
    ASSERT_NO_THROW(tgbot::CallbackGame(tgbot::CallbackGame().serialise()));

    const nlohmann::json doc = nlohmann::json::object();

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::CallbackGame(serialised_doc).serialise(), serialised_doc);

    {
        const tgbot::CallbackGame callback_game;

        ASSERT_EQ(callback_game.serialise(), serialised_doc);
    }
}

TEST(CallbackGame, errors)
{
    ASSERT_THROW(tgbot::CallbackGame(nlohmann::json::array().dump()), std::invalid_argument);
}
