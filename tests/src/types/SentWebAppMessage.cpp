#include "tgbot/types/SentWebAppMessage.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(SentWebAppMessage, normal)
{
    ASSERT_NO_THROW(tgbot::SentWebAppMessage(tgbot::SentWebAppMessage().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["inline_message_id"] = "rhoiphsdfh";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::SentWebAppMessage(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::SentWebAppMessage sent_web_app_message;

        sent_web_app_message.set_inline_message_id("rhoiphsdfh");
        ASSERT_EQ(sent_web_app_message.get_inline_message_id(), "rhoiphsdfh");

        ASSERT_EQ(sent_web_app_message.serialise(), serialised_doc);
    }
}

TEST(SentWebAppMessage, errors)
{
    ASSERT_THROW(tgbot::SentWebAppMessage(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 1; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["inline_message_id"] = "rhaoispoht" : doc["inline_message_id"] = 681684;

        ASSERT_THROW(tgbot::SentWebAppMessage(doc.dump()), std::invalid_argument);
    }
}
