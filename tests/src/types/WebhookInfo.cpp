#include "tgbot/types/WebhookInfo.h"

#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(WebhookInfo, normal)
{
    ASSERT_NO_THROW(tgbot::WebhookInfo(tgbot::WebhookInfo().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["allowed_updates"] = {"eölfhjsdfgrg"};
    doc["has_custom_certificate"] = false;
    doc["ip_address"] = "kbiuhn";
    doc["last_error_date"] = 71645;
    doc["last_error_message"] = "wökfjhe";
    doc["last_synchronization_error_date"] = 8981;
    doc["max_connections"] = -34572;
    doc["pending_update_count"] = -618;
    doc["url"] = "oijegpowerg";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::WebhookInfo(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::WebhookInfo webhook_info;

        webhook_info.set_allowed_updates(std::vector<std::string> {"eölfhjsdfgrg"});
        ASSERT_EQ(webhook_info.get_allowed_updates(), std::vector<std::string> {"eölfhjsdfgrg"});

        webhook_info.set_has_custom_certificate(false);
        ASSERT_EQ(webhook_info.get_has_custom_certificate(), false);

        webhook_info.set_ip_address("kbiuhn");
        ASSERT_EQ(webhook_info.get_ip_address(), "kbiuhn");

        webhook_info.set_last_error_date(71645);
        ASSERT_EQ(webhook_info.get_last_error_date(), 71645);

        webhook_info.set_last_error_message("wökfjhe");
        ASSERT_EQ(webhook_info.get_last_error_message(), "wökfjhe");

        webhook_info.set_last_synchronization_error_date(8981);
        ASSERT_EQ(webhook_info.get_last_synchronization_error_date(), 8981);

        webhook_info.set_max_connections(-34572);
        ASSERT_EQ(webhook_info.get_max_connections(), -34572);

        webhook_info.set_pending_update_count(-618);
        ASSERT_EQ(webhook_info.get_pending_update_count(), -618);

        webhook_info.set_url("oijegpowerg");
        ASSERT_EQ(webhook_info.get_url(), "oijegpowerg");

        ASSERT_EQ(webhook_info.serialise(), serialised_doc);
    }
}

TEST(WebhookInfo, errors)
{
    ASSERT_THROW(tgbot::WebhookInfo(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["has_custom_certificate"] = false;
        }

        if(i != 1)
        {
            doc["pending_update_count"] = 6844;
        }

        if(i != 2)
        {
            doc["url"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::WebhookInfo(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 9; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["allowed_updates"] = {"eölfhjsdfgrg"} : doc["allowed_updates"] = -4652;
        i != 1 ? doc["has_custom_certificate"] = true : doc["has_custom_certificate"] = 53154;
        i != 2 ? doc["ip_address"] = "toiu" : doc["ip_address"] = 798;
        i != 3 ? doc["last_error_date"] = 7981684 : doc["last_error_date"] = "bsefbsdfh";
        i != 4 ? doc["last_error_message"] = "qpoirzu" : doc["last_error_message"] = 87;
        i != 5 ? doc["last_synchronization_error_date"] = 4668 : doc["last_synchronization_error_date"] = "rwoz";
        i != 6 ? doc["max_connections"] = 7981684 : doc["max_connections"] = "bsefbsdfh";
        i != 7 ? doc["pending_update_count"] = 6844 : doc["pending_update_count"] = "sdfg";
        i != 8 ? doc["url"] = "aoisjgldfgs" : doc["url"] = 286;

        ASSERT_THROW(tgbot::WebhookInfo(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["allowed_updates"] = {144251};
        doc["has_custom_certificate"] = false;
        doc["pending_update_count"] = 6844;
        doc["url"] = "aoisjgldfgs";

        ASSERT_THROW(tgbot::WebhookInfo(doc.dump()), std::invalid_argument);
    }
}
