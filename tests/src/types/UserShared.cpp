#include "tgbot/types/UserShared.h"

#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(UserShared, normal)
{
    ASSERT_NO_THROW(tgbot::UserShared(tgbot::UserShared().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["request_id"] = 267165;
    doc["user_id"] = 561266546;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::UserShared(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::UserShared user_shared;

        user_shared.set_request_id(267165);
        ASSERT_EQ(user_shared.get_request_id(), 267165);

        user_shared.set_user_id(561266546);
        ASSERT_EQ(user_shared.get_user_id(), 561266546);

        ASSERT_EQ(user_shared.serialise(), serialised_doc);
    }
}

TEST(UserShared, errors)
{
    ASSERT_THROW(tgbot::UserShared(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["request_id"] = 768137;
        }

        if(i != 1)
        {
            doc["user_id"] = 186;
        }

        ASSERT_THROW(tgbot::UserShared(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["request_id"] = 658 : doc["request_id"] = "cawrpjdb";
        i != 1 ? doc["user_id"] = 731267 : doc["user_id"] = "pymüaosg";

        ASSERT_THROW(tgbot::UserShared(doc.dump()), std::invalid_argument);
    }
}
