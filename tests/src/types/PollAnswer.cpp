#include "tgbot/types/PollAnswer.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(PollAnswer, normal)
{
    ASSERT_NO_THROW(tgbot::PollAnswer(tgbot::PollAnswer().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["option_ids"] = {-65105};
    doc["poll_id"] = "spdfokmaperijg";
    doc["user"] = nlohmann::json::parse(tgbot::User().serialise());

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::PollAnswer(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::PollAnswer poll_answer;

        poll_answer.set_option_ids({-65105});
        ASSERT_EQ(poll_answer.get_option_ids(), std::vector<std::int32_t> {-65105});

        poll_answer.set_poll_id("spdfokmaperijg");
        ASSERT_EQ(poll_answer.get_poll_id(), "spdfokmaperijg");

        poll_answer.set_user(std::make_shared<tgbot::User>());
        ASSERT_EQ(poll_answer.get_user()->serialise(), tgbot::User().serialise());

        ASSERT_EQ(poll_answer.serialise(), serialised_doc);
    }
}

TEST(PollAnswer, errors)
{
    ASSERT_THROW(tgbot::PollAnswer(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["option_ids"] = {-65105};
        }

        if(i != 1)
        {
            doc["poll_id"] = "rpoij";
        }

        if(i != 2)
        {
            doc["user"] = nlohmann::json::parse(tgbot::User().serialise());
        }

        ASSERT_THROW(tgbot::PollAnswer(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["option_ids"] = {-65105} : doc["option_ids"] = -634;
        i != 1 ? doc["poll_id"] = "rqoizq" : doc["poll_id"] = 94;
        i != 2 ? doc["user"] = nlohmann::json::parse(tgbot::User().serialise()) : doc["user"] = 2431;

        ASSERT_THROW(tgbot::PollAnswer(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["option_ids"] = {"vlamisjd"};
        doc["poll_id"] = "pou";
        doc["user"] = nlohmann::json::parse(tgbot::User().serialise());

        ASSERT_THROW(tgbot::PollAnswer(doc.dump()), std::invalid_argument);
    }
}
