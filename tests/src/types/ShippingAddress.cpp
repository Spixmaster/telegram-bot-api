#include "tgbot/types/ShippingAddress.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(ShippingAddress, normal)
{
    ASSERT_NO_THROW(tgbot::ShippingAddress(tgbot::ShippingAddress().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["city"] = "rtnsgsdfg";
    doc["country_code"] = "dfgbetg";
    doc["post_code"] = "yofhgpisd";
    doc["state"] = "ösjdfg";
    doc["street_line1"] = "ezkipfgnf";
    doc["street_line2"] = "ysawrzdfgfh";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ShippingAddress(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::ShippingAddress shipping_address;

        shipping_address.set_city("rtnsgsdfg");
        ASSERT_EQ(shipping_address.get_city(), "rtnsgsdfg");

        shipping_address.set_country_code("dfgbetg");
        ASSERT_EQ(shipping_address.get_country_code(), "dfgbetg");

        shipping_address.set_post_code("yofhgpisd");
        ASSERT_EQ(shipping_address.get_post_code(), "yofhgpisd");

        shipping_address.set_state("ösjdfg");
        ASSERT_EQ(shipping_address.get_state(), "ösjdfg");

        shipping_address.set_street_line1("ezkipfgnf");
        ASSERT_EQ(shipping_address.get_street_line1(), "ezkipfgnf");

        shipping_address.set_street_line2("ysawrzdfgfh");
        ASSERT_EQ(shipping_address.get_street_line2(), "ysawrzdfgfh");

        ASSERT_EQ(shipping_address.serialise(), serialised_doc);
    }
}

TEST(ShippingAddress, errors)
{
    ASSERT_THROW(tgbot::ShippingAddress(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 6; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["city"] = "cxjashwqwg";
        }

        if(i != 1)
        {
            doc["country_code"] = "aoisjgldfgs";
        }

        if(i != 2)
        {
            doc["post_code"] = "rhxalnvg";
        }

        if(i != 3)
        {
            doc["state"] = "qzopinaxb";
        }

        if(i != 4)
        {
            doc["street_line1"] = "lanösjpoeir";
        }

        if(i != 5)
        {
            doc["street_line2"] = "rewponasdg";
        }

        ASSERT_THROW(tgbot::ShippingAddress(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 6; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["city"] = "xoijeqrz" : doc["city"] = 71687;
        i != 1 ? doc["country_code"] = "xoijwrezx" : doc["country_code"] = 1546;
        i != 2 ? doc["post_code"] = "rxnpiwerh" : doc["post_code"] = -51264;
        i != 3 ? doc["state"] = "oerwhplkmav" : doc["state"] = -24;
        i != 4 ? doc["street_line1"] = "jfoisdb" : doc["street_line1"] = 85;
        i != 5 ? doc["street_line2"] = "öiuhca" : doc["street_line2"] = 88;

        ASSERT_THROW(tgbot::ShippingAddress(doc.dump()), std::invalid_argument);
    }
}
