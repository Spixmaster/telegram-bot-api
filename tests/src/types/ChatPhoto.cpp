#include "tgbot/types/ChatPhoto.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(ChatPhoto, normal)
{
    ASSERT_NO_THROW(tgbot::ChatPhoto(tgbot::ChatPhoto().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["big_file_id"] = "wehüoimjc";
    doc["big_file_unique_id"] = "xẃthomncv";
    doc["small_file_id"] = "egfbkjma";
    doc["small_file_unique_id"] = "cwsndb";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ChatPhoto(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::ChatPhoto chat_photo;

        chat_photo.set_big_file_id("wehüoimjc");
        ASSERT_EQ(chat_photo.get_big_file_id(), "wehüoimjc");

        chat_photo.set_big_file_unique_id("xẃthomncv");
        ASSERT_EQ(chat_photo.get_big_file_unique_id(), "xẃthomncv");

        chat_photo.set_small_file_id("egfbkjma");
        ASSERT_EQ(chat_photo.get_small_file_id(), "egfbkjma");

        chat_photo.set_small_file_unique_id("cwsndb");
        ASSERT_EQ(chat_photo.get_small_file_unique_id(), "cwsndb");

        ASSERT_EQ(chat_photo.serialise(), serialised_doc);
    }
}

TEST(ChatPhoto, errors)
{
    ASSERT_THROW(tgbot::ChatPhoto(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["big_file_id"] = "agdf";
        }

        if(i != 1)
        {
            doc["big_file_unique_id"] = "cwhr";
        }

        if(i != 2)
        {
            doc["small_file_id"] = "aoisjgldfgs";
        }

        if(i != 3)
        {
            doc["small_file_unique_id"] = "owihf";
        }

        ASSERT_THROW(tgbot::ChatPhoto(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["big_file_id"] = "weho" : doc["big_file_id"] = 81;
        i != 1 ? doc["big_file_unique_id"] = "cowerh" : doc["big_file_unique_id"] = 681;
        i != 2 ? doc["small_file_id"] = "aoisjgldfgs" : doc["small_file_id"] = 8135;
        i != 3 ? doc["small_file_unique_id"] = "owihf" : doc["small_file_unique_id"] = 65;

        ASSERT_THROW(tgbot::ChatPhoto(doc.dump()), std::invalid_argument);
    }
}
