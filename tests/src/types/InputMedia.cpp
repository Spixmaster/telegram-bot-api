#include "tgbot/types/InputMedia.h"

#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(InputMedia, normal)
{
    ASSERT_NO_THROW(tgbot::InputMedia(tgbot::InputMedia().serialise()));

    const nlohmann::json doc = nlohmann::json::object();

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InputMedia().serialise(), serialised_doc);

    {
        const tgbot::InputMedia input_media;

        ASSERT_EQ(input_media.serialise(), serialised_doc);
    }
}

TEST(InputMedia, errors)
{
    ASSERT_THROW(tgbot::InputMedia(nlohmann::json::array().dump()), std::invalid_argument);
}
