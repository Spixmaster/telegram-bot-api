#include "tgbot/types/InputMediaAudio.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InputMediaAudio, normal)
{
    ASSERT_NO_THROW(tgbot::InputMediaAudio(tgbot::InputMediaAudio().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["caption"] = "cmüsjdfhc";
    doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())};
    doc["duration"] = 6851;
    doc["media"] = "cölksmdf";
    doc["parse_mode"] = "thokmkomsc";
    doc["performer"] = "ciohjnpoitjpw";
    doc["thumbnail"] = "üomsiodfh";
    doc["title"] = "thomcjdf";
    doc["type"] = "hpoijcsdfh";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InputMediaAudio(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InputMediaAudio input_media_audio;

        input_media_audio.set_caption("cmüsjdfhc");
        ASSERT_EQ(input_media_audio.get_caption(), "cmüsjdfhc");

        input_media_audio.set_caption_entities(std::vector<tgbot::MessageEntity::ptr> {
          std::make_shared<tgbot::MessageEntity>()});

        if(input_media_audio.get_caption_entities().has_value())
        {
            ASSERT_EQ(
              input_media_audio.get_caption_entities().value().at(0)->serialise(), tgbot::MessageEntity().serialise());
        }

        input_media_audio.set_duration(6851);
        ASSERT_EQ(input_media_audio.get_duration(), 6851);

        input_media_audio.set_media("cölksmdf");
        ASSERT_EQ(input_media_audio.get_media(), "cölksmdf");

        input_media_audio.set_parse_mode("thokmkomsc");
        ASSERT_EQ(input_media_audio.get_parse_mode(), "thokmkomsc");

        input_media_audio.set_performer("ciohjnpoitjpw");
        ASSERT_EQ(input_media_audio.get_performer(), "ciohjnpoitjpw");

        input_media_audio.set_thumbnail("üomsiodfh");
        ASSERT_EQ(input_media_audio.get_thumbnail(), "üomsiodfh");

        input_media_audio.set_title("thomcjdf");
        ASSERT_EQ(input_media_audio.get_title(), "thomcjdf");

        input_media_audio.set_type("hpoijcsdfh");
        ASSERT_EQ(input_media_audio.get_type(), "hpoijcsdfh");

        ASSERT_EQ(input_media_audio.serialise(), serialised_doc);
    }
}

TEST(InputMediaAudio, errors)
{
    ASSERT_THROW(tgbot::InputMediaAudio(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["media"] = "owihf";
        }

        if(i != 1)
        {
            doc["type"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::InputMediaAudio(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 9; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["caption"] = "djlhqg" : doc["caption"] = 4168;
        i != 1 ?
          doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())} :
          doc["caption_entities"] = 4168;
        i != 2 ? doc["duration"] = 984 : doc["duration"] = "copij";
        i != 3 ? doc["media"] = "owihf" : doc["media"] = 65;
        i != 4 ? doc["parse_mode"] = "djlhqg" : doc["parse_mode"] = 4168;
        i != 5 ? doc["performer"] = "rpjasndinpeb" : doc["performer"] = 168751;
        i != 6 ? doc["thumbnail"] = "opqgd" : doc["thumbnail"] = 486;
        i != 7 ? doc["title"] = "hpojipkmsocv" : doc["title"] = 681345;
        i != 8 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;

        ASSERT_THROW(tgbot::InputMediaAudio(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["caption_entities"] = {92441};
        doc["media"] = "owihf";
        doc["type"] = "aoisjgldfgs";

        ASSERT_THROW(tgbot::InputMediaAudio(doc.dump()), std::invalid_argument);
    }
}
