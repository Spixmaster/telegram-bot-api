#include "tgbot/types/ShippingOption.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(ShippingOption, normal)
{
    ASSERT_NO_THROW(tgbot::ShippingOption(tgbot::ShippingOption().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["id"] = "sokgmfnfgb";
    doc["prices"] = {nlohmann::json::parse(tgbot::LabeledPrice().serialise())};
    doc["title"] = "spboidnfpobjdf";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ShippingOption(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::ShippingOption shipping_option;

        shipping_option.set_id("sokgmfnfgb");
        ASSERT_EQ(shipping_option.get_id(), "sokgmfnfgb");

        shipping_option.set_prices({std::make_shared<tgbot::LabeledPrice>()});
        ASSERT_EQ(shipping_option.get_prices().at(0)->serialise(), tgbot::LabeledPrice().serialise());

        shipping_option.set_title("spboidnfpobjdf");
        ASSERT_EQ(shipping_option.get_title(), "spboidnfpobjdf");

        ASSERT_EQ(shipping_option.serialise(), serialised_doc);
    }
}

TEST(ShippingOption, errors)
{
    ASSERT_THROW(tgbot::ShippingOption(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["id"] = "aoisjgldfgs";
        }

        if(i != 1)
        {
            doc["prices"] = {nlohmann::json::parse(tgbot::LabeledPrice().serialise())};
        }

        if(i != 2)
        {
            doc["title"] = "owihf";
        }

        ASSERT_THROW(tgbot::ShippingOption(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["id"] = "aoisjgldfgs" : doc["id"] = 286;
        i != 1 ? doc["prices"] = {nlohmann::json::parse(tgbot::LabeledPrice().serialise())} : doc["prices"] = 65;
        i != 2 ? doc["title"] = "owihf" : doc["title"] = 65;

        ASSERT_THROW(tgbot::ShippingOption(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["id"] = "aoisjgldfgs";
        doc["prices"] = {72113};
        doc["title"] = "owihf";

        ASSERT_THROW(tgbot::ShippingOption(doc.dump()), std::invalid_argument);
    }
}
