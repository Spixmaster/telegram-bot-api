#include "tgbot/types/PassportData.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(PassportData, normal)
{
    ASSERT_NO_THROW(tgbot::PassportData(tgbot::PassportData().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["credentials"] = nlohmann::json::parse(tgbot::EncryptedCredentials().serialise());
    doc["data"] = {nlohmann::json::parse(tgbot::EncryptedPassportElement().serialise())};

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::PassportData(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::PassportData passport_data;

        passport_data.set_credentials(std::make_shared<tgbot::EncryptedCredentials>());
        ASSERT_EQ(passport_data.get_credentials()->serialise(), tgbot::EncryptedCredentials().serialise());

        passport_data.set_data({std::make_shared<tgbot::EncryptedPassportElement>()});
        ASSERT_EQ(passport_data.get_data().at(0)->serialise(), tgbot::EncryptedPassportElement().serialise());

        ASSERT_EQ(passport_data.serialise(), serialised_doc);
    }
}

TEST(PassportData, errors)
{
    ASSERT_THROW(tgbot::PassportData(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["credentials"] = nlohmann::json::parse(tgbot::EncryptedCredentials().serialise());
        }

        if(i != 1)
        {
            doc["data"] = {nlohmann::json::parse(tgbot::EncryptedPassportElement().serialise())};
        }

        ASSERT_THROW(tgbot::PassportData(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ?
          doc["credentials"] = nlohmann::json::parse(tgbot::EncryptedCredentials().serialise()) :
          doc["credentials"] = 987354;
        i != 1 ?
          doc["data"] = {nlohmann::json::parse(tgbot::EncryptedPassportElement().serialise())} :
          doc["data"] = 468;

        ASSERT_THROW(tgbot::PassportData(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["credentials"] = nlohmann::json::parse(tgbot::EncryptedCredentials().serialise());
        doc["data"] = {68053};

        ASSERT_THROW(tgbot::PassportData(doc.dump()), std::invalid_argument);
    }
}
