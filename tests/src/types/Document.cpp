#include "tgbot/types/Document.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(Document, normal)
{
    ASSERT_NO_THROW(tgbot::Document(tgbot::Document().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["file_id"] = "ebnfsbdf";
    doc["file_name"] = "fühwpoinfg";
    doc["file_size"] = -68148;
    doc["file_unique_id"] = "tpoisfdg";
    doc["mime_type"] = "gpwujefb";
    doc["thumbnail"] = nlohmann::json::parse(tgbot::PhotoSize().serialise());

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::Document(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::Document document;

        document.set_file_id("ebnfsbdf");
        ASSERT_EQ(document.get_file_id(), "ebnfsbdf");

        document.set_file_name("fühwpoinfg");
        ASSERT_EQ(document.get_file_name(), "fühwpoinfg");

        document.set_file_size(-68148);
        ASSERT_EQ(document.get_file_size(), -68148);

        document.set_file_unique_id("tpoisfdg");
        ASSERT_EQ(document.get_file_unique_id(), "tpoisfdg");

        document.set_mime_type("gpwujefb");
        ASSERT_EQ(document.get_mime_type(), "gpwujefb");

        document.set_thumbnail(std::make_shared<tgbot::PhotoSize>());

        if(document.get_thumbnail().has_value())
        {
            ASSERT_EQ(document.get_thumbnail().value()->serialise(), tgbot::PhotoSize().serialise());
        }

        ASSERT_EQ(document.serialise(), serialised_doc);
    }
}

TEST(Document, errors)
{
    ASSERT_THROW(tgbot::Document(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["file_id"] = "aoisjgldfgs";
        }

        if(i != 1)
        {
            doc["file_unique_id"] = "owihf";
        }

        ASSERT_THROW(tgbot::Document(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 6; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["file_id"] = "aoisjgldfgs" : doc["file_id"] = 286;
        i != 1 ? doc["file_name"] = "wuefhbnjdfb" : doc["file_name"] = 651;
        i != 2 ? doc["file_size"] = 4561 : doc["file_size"] = "sdfhsdrg";
        i != 3 ? doc["file_unique_id"] = "owihf" : doc["file_unique_id"] = 65;
        i != 4 ? doc["mime_type"] = "woöinefmoib" : doc["mime_type"] = 616842;
        i != 5 ? doc["thumbnail"] = nlohmann::json::parse(tgbot::PhotoSize().serialise()) : doc["thumbnail"] = 468;

        ASSERT_THROW(tgbot::Document(doc.dump()), std::invalid_argument);
    }
}
