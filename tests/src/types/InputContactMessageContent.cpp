#include "tgbot/types/InputContactMessageContent.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(InputContactMessageContent, normal)
{
    ASSERT_NO_THROW(tgbot::InputContactMessageContent(tgbot::InputContactMessageContent().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["first_name"] = "pojqdvx";
    doc["last_name"] = "thoimax";
    doc["phone_number"] = "fhlkjdfg";
    doc["vcard"] = "xthojiwomc";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InputContactMessageContent(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InputContactMessageContent input_contact_message_content;

        input_contact_message_content.set_first_name("pojqdvx");
        ASSERT_EQ(input_contact_message_content.get_first_name(), "pojqdvx");

        input_contact_message_content.set_last_name("thoimax");
        ASSERT_EQ(input_contact_message_content.get_last_name(), "thoimax");

        input_contact_message_content.set_phone_number("fhlkjdfg");
        ASSERT_EQ(input_contact_message_content.get_phone_number(), "fhlkjdfg");

        input_contact_message_content.set_vcard("xthojiwomc");
        ASSERT_EQ(input_contact_message_content.get_vcard(), "xthojiwomc");

        ASSERT_EQ(input_contact_message_content.serialise(), serialised_doc);
    }
}

TEST(InputContactMessageContent, errors)
{
    ASSERT_THROW(tgbot::InputContactMessageContent(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["first_name"] = "owihf";
        }

        if(i != 1)
        {
            doc["phone_number"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::InputContactMessageContent(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["first_name"] = "owihf" : doc["first_name"] = 65;
        i != 1 ? doc["last_name"] = "popgone" : doc["last_name"] = 71359;
        i != 2 ? doc["phone_number"] = "aoisjgldfgs" : doc["phone_number"] = 286;
        i != 3 ? doc["vcard"] = "thpoihw" : doc["vcard"] = 768135;

        ASSERT_THROW(tgbot::InputContactMessageContent(doc.dump()), std::invalid_argument);
    }
}
