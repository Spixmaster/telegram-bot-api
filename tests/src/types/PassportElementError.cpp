#include "tgbot/types/PassportElementError.h"

#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(PassportElementError, normal)
{
    ASSERT_NO_THROW(tgbot::PassportElementError(tgbot::PassportElementError().serialise()));

    const nlohmann::json doc = nlohmann::json::object();

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::PassportElementError().serialise(), serialised_doc);

    {
        const tgbot::PassportElementError passport_element_error;

        ASSERT_EQ(passport_element_error.serialise(), serialised_doc);
    }
}

TEST(PassportElementError, errors)
{
    ASSERT_THROW(tgbot::PassportElementError(nlohmann::json::array().dump()), std::invalid_argument);
}
