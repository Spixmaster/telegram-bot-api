#include "tgbot/types/ChatInviteLink.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(ChatInviteLink, normal)
{
    ASSERT_NO_THROW(tgbot::ChatInviteLink(tgbot::ChatInviteLink().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["creates_join_request"] = true;
    doc["creator"] = nlohmann::json::parse(tgbot::User().serialise());
    doc["expire_date"] = 68146;
    doc["invite_link"] = "jmnoijscb";
    doc["is_primary"] = true;
    doc["is_revoked"] = false;
    doc["member_limit"] = -185;
    doc["name"] = "rxpounwre";
    doc["pending_join_request_count"] = -71354543;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ChatInviteLink(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::ChatInviteLink chat_invite_link;

        chat_invite_link.set_creates_join_request(true);
        ASSERT_EQ(chat_invite_link.get_creates_join_request(), true);

        chat_invite_link.set_creator(std::make_shared<tgbot::User>());
        ASSERT_EQ(chat_invite_link.get_creator()->serialise(), tgbot::User().serialise());

        chat_invite_link.set_expire_date(68146);
        ASSERT_EQ(chat_invite_link.get_expire_date(), 68146);

        chat_invite_link.set_invite_link("jmnoijscb");
        ASSERT_EQ(chat_invite_link.get_invite_link(), "jmnoijscb");

        chat_invite_link.set_is_primary(true);
        ASSERT_EQ(chat_invite_link.get_is_primary(), true);

        chat_invite_link.set_is_revoked(false);
        ASSERT_EQ(chat_invite_link.get_is_revoked(), false);

        chat_invite_link.set_member_limit(-185);
        ASSERT_EQ(chat_invite_link.get_member_limit(), -185);

        chat_invite_link.set_name("rxpounwre");
        ASSERT_EQ(chat_invite_link.get_name(), "rxpounwre");

        chat_invite_link.set_pending_join_request_count(-71354543);
        ASSERT_EQ(chat_invite_link.get_pending_join_request_count(), -71354543);

        ASSERT_EQ(chat_invite_link.serialise(), serialised_doc);
    }
}

TEST(ChatInviteLink, errors)
{
    ASSERT_THROW(tgbot::ChatInviteLink(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 5; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["creates_join_request"] = false;
        }

        if(i != 1)
        {
            doc["creator"] = nlohmann::json::parse(tgbot::User().serialise());
        }

        if(i != 2)
        {
            doc["invite_link"] = "aoisjgldfgs";
        }

        if(i != 3)
        {
            doc["is_primary"] = false;
        }

        if(i != 4)
        {
            doc["is_revoked"] = true;
        }

        ASSERT_THROW(tgbot::ChatInviteLink(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 9; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["creates_join_request"] = false : doc["creates_join_request"] = 684561;
        i != 1 ? doc["creator"] = nlohmann::json::parse(tgbot::User().serialise()) : doc["creator"] = 65;
        i != 2 ? doc["expire_date"] = 7981684 : doc["expire_date"] = "bsefbsdfh";
        i != 3 ? doc["invite_link"] = "aoisjgldfgs" : doc["invite_link"] = 286;
        i != 4 ? doc["is_primary"] = false : doc["is_primary"] = 76513;
        i != 5 ? doc["is_revoked"] = true : doc["is_revoked"] = 760879;
        i != 6 ? doc["member_limit"] = 18 : doc["member_limit"] = "csg";
        i != 7 ? doc["name"] = "rhiuocg" : doc["name"] = 618;
        i != 8 ? doc["pending_join_request_count"] = 17 : doc["pending_join_request_count"] = "dg";

        ASSERT_THROW(tgbot::ChatInviteLink(doc.dump()), std::invalid_argument);
    }
}
