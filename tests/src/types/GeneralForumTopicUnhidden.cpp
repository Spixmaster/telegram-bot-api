#include "tgbot/types/GeneralForumTopicUnhidden.h"

#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(GeneralForumTopicUnhidden, normal)
{
    ASSERT_NO_THROW(tgbot::GeneralForumTopicUnhidden(tgbot::GeneralForumTopicUnhidden().serialise()));

    const nlohmann::json doc = nlohmann::json::object();

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::GeneralForumTopicUnhidden(serialised_doc).serialise(), serialised_doc);

    {
        const tgbot::GeneralForumTopicUnhidden general_forum_topic_unhidden;

        ASSERT_EQ(general_forum_topic_unhidden.serialise(), serialised_doc);
    }
}

TEST(GeneralForumTopicUnhidden, errors)
{
    ASSERT_THROW(tgbot::GeneralForumTopicUnhidden(nlohmann::json::array().dump()), std::invalid_argument);
}
