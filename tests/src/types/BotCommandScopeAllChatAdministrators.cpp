#include "tgbot/types/BotCommandScopeAllChatAdministrators.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(BotCommandScopeAllChatAdministrators, normal)
{
    ASSERT_NO_THROW(
      tgbot::BotCommandScopeAllChatAdministrators(tgbot::BotCommandScopeAllChatAdministrators().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["type"] = "fojwoijth";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::BotCommandScopeAllChatAdministrators(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::BotCommandScopeAllChatAdministrators bot_command_scope_all_administrators;

        bot_command_scope_all_administrators.set_type("fojwoijth");
        ASSERT_EQ(bot_command_scope_all_administrators.get_type(), "fojwoijth");

        ASSERT_EQ(bot_command_scope_all_administrators.serialise(), serialised_doc);
    }
}

TEST(BotCommandScopeAllChatAdministrators, errors)
{
    ASSERT_THROW(tgbot::BotCommandScopeAllChatAdministrators(nlohmann::json::array().dump()), std::invalid_argument);

    {
        const nlohmann::json doc = nlohmann::json::object();

        ASSERT_THROW(tgbot::BotCommandScopeAllChatAdministrators(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 1; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;

        ASSERT_THROW(tgbot::BotCommandScopeAllChatAdministrators(doc.dump()), std::invalid_argument);
    }
}
