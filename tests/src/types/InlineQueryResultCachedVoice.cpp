#include "tgbot/types/InlineQueryResultCachedVoice.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InlineQueryResultCachedVoice, normal)
{
    ASSERT_NO_THROW(tgbot::InlineQueryResultCachedVoice(tgbot::InlineQueryResultCachedVoice().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["caption"] = "hkjmasfdg";
    doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())};
    doc["id"] = "slckmölksd";
    doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise());
    doc["parse_mode"] = "lsoxmjfg";
    doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise());
    doc["title"] = "xopajo";
    doc["type"] = "lknökac";
    doc["voice_file_id"] = "cmkspoidfh";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InlineQueryResultCachedVoice(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InlineQueryResultCachedVoice inline_query_result_cached_voice;

        inline_query_result_cached_voice.set_caption("hkjmasfdg");
        ASSERT_EQ(inline_query_result_cached_voice.get_caption(), "hkjmasfdg");

        inline_query_result_cached_voice.set_caption_entities(std::vector<tgbot::MessageEntity::ptr> {
          std::make_shared<tgbot::MessageEntity>()});

        if(inline_query_result_cached_voice.get_caption_entities().has_value())
        {
            ASSERT_EQ(inline_query_result_cached_voice.get_caption_entities().value().at(0)->serialise(),
                      tgbot::MessageEntity().serialise());
        }

        inline_query_result_cached_voice.set_id("slckmölksd");
        ASSERT_EQ(inline_query_result_cached_voice.get_id(), "slckmölksd");

        inline_query_result_cached_voice.set_input_message_content(std::make_shared<tgbot::InputMessageContent>());

        if(inline_query_result_cached_voice.get_input_message_content().has_value())
        {
            ASSERT_EQ(inline_query_result_cached_voice.get_input_message_content().value()->serialise(),
                      tgbot::InputMessageContent().serialise());
        }

        inline_query_result_cached_voice.set_parse_mode("lsoxmjfg");
        ASSERT_EQ(inline_query_result_cached_voice.get_parse_mode(), "lsoxmjfg");

        inline_query_result_cached_voice.set_reply_markup(std::make_shared<tgbot::InlineKeyboardMarkup>());

        if(inline_query_result_cached_voice.get_reply_markup().has_value())
        {
            ASSERT_EQ(inline_query_result_cached_voice.get_reply_markup().value()->serialise(),
                      tgbot::InlineKeyboardMarkup().serialise());
        }

        inline_query_result_cached_voice.set_title("xopajo");
        ASSERT_EQ(inline_query_result_cached_voice.get_title(), "xopajo");

        inline_query_result_cached_voice.set_type("lknökac");
        ASSERT_EQ(inline_query_result_cached_voice.get_type(), "lknökac");

        inline_query_result_cached_voice.set_voice_file_id("cmkspoidfh");
        ASSERT_EQ(inline_query_result_cached_voice.get_voice_file_id(), "cmkspoidfh");

        ASSERT_EQ(inline_query_result_cached_voice.serialise(), serialised_doc);
    }
}

TEST(InlineQueryResultCachedVoice, errors)
{
    ASSERT_THROW(tgbot::InlineQueryResultCachedVoice(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["id"] = "owihf";
        }

        if(i != 1)
        {
            doc["title"] = "diopqb";
        }

        if(i != 2)
        {
            doc["type"] = "aoisjgldfgs";
        }

        if(i != 3)
        {
            doc["voice_file_id"] = "icqun";
        }

        ASSERT_THROW(tgbot::InlineQueryResultCachedVoice(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 9; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["caption"] = "djlhqg" : doc["caption"] = 4168;
        i != 1 ?
          doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())} :
          doc["caption_entities"] = 4168;
        i != 2 ? doc["id"] = "owihf" : doc["id"] = 65;
        i != 3 ?
          doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise()) :
          doc["input_message_content"] = 49;
        i != 4 ? doc["parse_mode"] = "djlhqg" : doc["parse_mode"] = 4168;
        i != 5 ?
          doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise()) :
          doc["reply_markup"] = 49;
        i != 6 ? doc["title"] = "cxpjih" : doc["title"] = 739;
        i != 7 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;
        i != 8 ? doc["voice_file_id"] = "opqgd" : doc["voice_file_id"] = 486;

        ASSERT_THROW(tgbot::InlineQueryResultCachedVoice(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["caption_entities"] = {289941};
        doc["id"] = "owihf";
        doc["title"] = "diopqb";
        doc["type"] = "aoisjgldfgs";
        doc["voice_file_id"] = "icqun";

        ASSERT_THROW(tgbot::InlineQueryResultCachedVoice(doc.dump()), std::invalid_argument);
    }
}
