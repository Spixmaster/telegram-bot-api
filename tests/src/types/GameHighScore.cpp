#include "tgbot/types/GameHighScore.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(GameHighScore, normal)
{
    ASSERT_NO_THROW(tgbot::GameHighScore(tgbot::GameHighScore().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["position"] = -488;
    doc["score"] = 68418;
    doc["user"] = nlohmann::json::parse(tgbot::User().serialise());

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::GameHighScore(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::GameHighScore game_high_score;

        game_high_score.set_position(-488);
        ASSERT_EQ(game_high_score.get_position(), -488);

        game_high_score.set_score(68418);
        ASSERT_EQ(game_high_score.get_score(), 68418);

        game_high_score.set_user(std::make_shared<tgbot::User>());
        ASSERT_EQ(game_high_score.get_user()->serialise(), tgbot::User().serialise());

        ASSERT_EQ(game_high_score.serialise(), serialised_doc);
    }
}

TEST(GameHighScore, errors)
{
    ASSERT_THROW(tgbot::GameHighScore(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["position"] = 936;
        }

        if(i != 1)
        {
            doc["score"] = 6844;
        }

        if(i != 2)
        {
            doc["user"] = nlohmann::json::parse(tgbot::User().serialise());
        }

        ASSERT_THROW(tgbot::GameHighScore(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["position"] = 46 : doc["position"] = "zsfb";
        i != 1 ? doc["score"] = 6844 : doc["score"] = "sdfg";
        i != 2 ? doc["user"] = nlohmann::json::parse(tgbot::User().serialise()) : doc["user"] = 413;

        ASSERT_THROW(tgbot::GameHighScore(doc.dump()), std::invalid_argument);
    }
}
