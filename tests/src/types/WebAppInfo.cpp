#include "tgbot/types/WebAppInfo.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(WebAppInfo, normal)
{
    ASSERT_NO_THROW(tgbot::WebAppInfo(tgbot::WebAppInfo().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["url"] = "oasdölkewh";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::WebAppInfo(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::WebAppInfo web_app_info;

        web_app_info.set_url("oasdölkewh");
        ASSERT_EQ(web_app_info.get_url(), "oasdölkewh");

        ASSERT_EQ(web_app_info.serialise(), serialised_doc);
    }
}

TEST(WebAppInfo, errors)
{
    ASSERT_THROW(tgbot::WebAppInfo(nlohmann::json::array().dump()), std::invalid_argument);

    {
        const nlohmann::json doc = nlohmann::json::object();

        ASSERT_THROW(tgbot::WebAppInfo(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 1; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["url"] = "rwonac" : doc["url"] = 1687;

        ASSERT_THROW(tgbot::WebAppInfo(doc.dump()), std::invalid_argument);
    }
}
