#include "tgbot/types/MenuButton.h"

#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(MenuButton, normal)
{
    ASSERT_NO_THROW(tgbot::MenuButton(tgbot::MenuButton().serialise()));

    const nlohmann::json doc = nlohmann::json::object();

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::MenuButton(serialised_doc).serialise(), serialised_doc);

    {
        const tgbot::MenuButton menu_button;

        ASSERT_EQ(menu_button.serialise(), serialised_doc);
    }
}

TEST(MenuButton, errors)
{
    ASSERT_THROW(tgbot::MenuButton(nlohmann::json::array().dump()), std::invalid_argument);
}
