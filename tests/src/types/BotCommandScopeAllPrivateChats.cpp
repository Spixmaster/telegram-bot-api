#include "tgbot/types/BotCommandScopeAllPrivateChats.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(BotCommandScopeAllPrivateChats, normal)
{
    ASSERT_NO_THROW(tgbot::BotCommandScopeAllPrivateChats(tgbot::BotCommandScopeAllPrivateChats().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["type"] = "rhowijf";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::BotCommandScopeAllPrivateChats(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::BotCommandScopeAllPrivateChats bot_command_scope_all_private_chats;

        bot_command_scope_all_private_chats.set_type("rhowijf");
        ASSERT_EQ(bot_command_scope_all_private_chats.get_type(), "rhowijf");

        ASSERT_EQ(bot_command_scope_all_private_chats.serialise(), serialised_doc);
    }
}

TEST(BotCommandScopeAllPrivateChats, errors)
{
    ASSERT_THROW(tgbot::BotCommandScopeAllPrivateChats(nlohmann::json::array().dump()), std::invalid_argument);

    {
        const nlohmann::json doc = nlohmann::json::object();

        ASSERT_THROW(tgbot::BotCommandScopeAllPrivateChats(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 1; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;

        ASSERT_THROW(tgbot::BotCommandScopeAllPrivateChats(doc.dump()), std::invalid_argument);
    }
}
