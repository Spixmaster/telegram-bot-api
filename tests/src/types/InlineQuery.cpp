#include "tgbot/types/InlineQuery.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InlineQuery, normal)
{
    ASSERT_NO_THROW(tgbot::InlineQuery(tgbot::InlineQuery().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["chat_type"] = "ojeljmfd";
    doc["from"] = nlohmann::json::parse(tgbot::User().serialise());
    doc["id"] = "äkjmasdfg";
    doc["location"] = nlohmann::json::parse(tgbot::Location().serialise());
    doc["offset"] = "bosijdfh";
    doc["query"] = "sdföbjsdfh";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InlineQuery(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InlineQuery inline_query;

        inline_query.set_chat_type("ojeljmfd");
        ASSERT_EQ(inline_query.get_chat_type(), "ojeljmfd");

        inline_query.set_from(std::make_shared<tgbot::User>());
        ASSERT_EQ(inline_query.get_from()->serialise(), tgbot::User().serialise());

        inline_query.set_id("äkjmasdfg");
        ASSERT_EQ(inline_query.get_id(), "äkjmasdfg");

        inline_query.set_location(std::make_shared<tgbot::Location>());

        if(inline_query.get_location().has_value())
        {
            ASSERT_EQ(inline_query.get_location().value()->serialise(), tgbot::Location().serialise());
        }

        inline_query.set_offset("bosijdfh");
        ASSERT_EQ(inline_query.get_offset(), "bosijdfh");

        inline_query.set_query("sdföbjsdfh");
        ASSERT_EQ(inline_query.get_query(), "sdföbjsdfh");

        ASSERT_EQ(inline_query.serialise(), serialised_doc);
    }
}

TEST(InlineQuery, errors)
{
    ASSERT_THROW(tgbot::InlineQuery(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["from"] = nlohmann::json::parse(tgbot::User().serialise());
        }

        if(i != 1)
        {
            doc["id"] = "qpjtoion";
        }

        if(i != 2)
        {
            doc["offset"] = "oioijowezrtz";
        }

        if(i != 3)
        {
            doc["query"] = "woizpony";
        }

        ASSERT_THROW(tgbot::InlineQuery(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 6; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["chat_type"] = "woöinefmoib" : doc["chat_type"] = 616842;
        i != 1 ? doc["from"] = nlohmann::json::parse(tgbot::User().serialise()) : doc["from"] = 76641;
        i != 2 ? doc["id"] = "hynoizs" : doc["id"] = 7913654;
        i != 3 ? doc["location"] = nlohmann::json::parse(tgbot::Location().serialise()) : doc["location"] = 7235416;
        i != 4 ? doc["offset"] = "rponxah" : doc["offset"] = 713495;
        i != 5 ? doc["query"] = "üpmlkbepor" : doc["query"] = 149753;

        ASSERT_THROW(tgbot::InlineQuery(doc.dump()), std::invalid_argument);
    }
}
