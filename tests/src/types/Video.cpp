#include "tgbot/types/Video.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(Video, normal)
{
    ASSERT_NO_THROW(tgbot::Video(tgbot::Video().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["duration"] = 6815;
    doc["file_id"] = "qwijdnglcmv";
    doc["file_name"] = "wkefjsdf";
    doc["file_size"] = -78961;
    doc["file_unique_id"] = "eorijkm";
    doc["height"] = -656116;
    doc["mime_type"] = "sldfjgr";
    doc["thumbnail"] = nlohmann::json::parse(tgbot::PhotoSize().serialise());
    doc["width"] = 86413525;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::Video(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::Video video;

        video.set_duration(6815);
        ASSERT_EQ(video.get_duration(), 6815);

        video.set_file_id("qwijdnglcmv");
        ASSERT_EQ(video.get_file_id(), "qwijdnglcmv");

        video.set_file_name("wkefjsdf");
        ASSERT_EQ(video.get_file_name(), "wkefjsdf");

        video.set_file_size(-78961);
        ASSERT_EQ(video.get_file_size(), -78961);

        video.set_file_unique_id("eorijkm");
        ASSERT_EQ(video.get_file_unique_id(), "eorijkm");

        video.set_height(-656116);
        ASSERT_EQ(video.get_height(), -656116);

        video.set_mime_type("sldfjgr");
        ASSERT_EQ(video.get_mime_type(), "sldfjgr");

        video.set_thumbnail(std::make_shared<tgbot::PhotoSize>());

        if(video.get_thumbnail().has_value())
        {
            ASSERT_EQ(video.get_thumbnail().value()->serialise(), tgbot::PhotoSize().serialise());
        }

        video.set_width(86413525);
        ASSERT_EQ(video.get_width(), 86413525);

        ASSERT_EQ(video.serialise(), serialised_doc);
    }
}

TEST(Video, errors)
{
    ASSERT_THROW(tgbot::Video(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 5; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["duration"] = 7981684;
        }

        if(i != 1)
        {
            doc["file_id"] = "aoisjgldfgs";
        }

        if(i != 2)
        {
            doc["file_unique_id"] = "owihf";
        }

        if(i != 3)
        {
            doc["height"] = 87;
        }

        if(i != 4)
        {
            doc["width"] = 6844;
        }

        ASSERT_THROW(tgbot::Video(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 9; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["duration"] = 7981684 : doc["duration"] = "bsefbsdfh";
        i != 1 ? doc["file_id"] = "aoisjgldfgs" : doc["file_id"] = 286;
        i != 2 ? doc["file_name"] = "wuefhbnjdfb" : doc["file_name"] = 651;
        i != 3 ? doc["file_size"] = 4561 : doc["file_size"] = "sdfhsdrg";
        i != 4 ? doc["file_unique_id"] = "owihf" : doc["file_unique_id"] = 65;
        i != 5 ? doc["height"] = 87 : doc["height"] = "herthsdf";
        i != 6 ? doc["mime_type"] = "woöinefmoib" : doc["mime_type"] = 616842;
        i != 7 ? doc["thumbnail"] = nlohmann::json::parse(tgbot::PhotoSize().serialise()) : doc["thumbnail"] = 468;
        i != 8 ? doc["width"] = 6844 : doc["width"] = "sdfg";

        ASSERT_THROW(tgbot::Video(doc.dump()), std::invalid_argument);
    }
}
