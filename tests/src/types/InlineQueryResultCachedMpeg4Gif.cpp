#include "tgbot/types/InlineQueryResultCachedMpeg4Gif.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InlineQueryResultCachedMpeg4Gif, normal)
{
    ASSERT_NO_THROW(tgbot::InlineQueryResultCachedMpeg4Gif(tgbot::InlineQueryResultCachedMpeg4Gif().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["caption"] = "spdofimgfg";
    doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())};
    doc["id"] = "hoimdsfh";
    doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise());
    doc["mpeg4_file_id"] = "hpoisfmdg";
    doc["parse_mode"] = "xehokmdf";
    doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise());
    doc["title"] = "sfhojmfdsg";
    doc["type"] = "wioejfh";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InlineQueryResultCachedMpeg4Gif(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InlineQueryResultCachedMpeg4Gif inline_query_result_cached_mpeg4_gif;

        inline_query_result_cached_mpeg4_gif.set_caption("spdofimgfg");
        ASSERT_EQ(inline_query_result_cached_mpeg4_gif.get_caption(), "spdofimgfg");

        inline_query_result_cached_mpeg4_gif.set_caption_entities(std::vector<tgbot::MessageEntity::ptr> {
          std::make_shared<tgbot::MessageEntity>()});

        if(inline_query_result_cached_mpeg4_gif.get_caption_entities().has_value())
        {
            ASSERT_EQ(inline_query_result_cached_mpeg4_gif.get_caption_entities().value().at(0)->serialise(),
                      tgbot::MessageEntity().serialise());
        }

        inline_query_result_cached_mpeg4_gif.set_id("hoimdsfh");
        ASSERT_EQ(inline_query_result_cached_mpeg4_gif.get_id(), "hoimdsfh");

        inline_query_result_cached_mpeg4_gif.set_input_message_content(std::make_shared<tgbot::InputMessageContent>());

        if(inline_query_result_cached_mpeg4_gif.get_input_message_content().has_value())
        {
            ASSERT_EQ(inline_query_result_cached_mpeg4_gif.get_input_message_content().value()->serialise(),
                      tgbot::InputMessageContent().serialise());
        }

        inline_query_result_cached_mpeg4_gif.set_mpeg4_file_id("hpoisfmdg");
        ASSERT_EQ(inline_query_result_cached_mpeg4_gif.get_mpeg4_file_id(), "hpoisfmdg");

        inline_query_result_cached_mpeg4_gif.set_parse_mode("xehokmdf");
        ASSERT_EQ(inline_query_result_cached_mpeg4_gif.get_parse_mode(), "xehokmdf");

        inline_query_result_cached_mpeg4_gif.set_reply_markup(std::make_shared<tgbot::InlineKeyboardMarkup>());

        if(inline_query_result_cached_mpeg4_gif.get_reply_markup().has_value())
        {
            ASSERT_EQ(inline_query_result_cached_mpeg4_gif.get_reply_markup().value()->serialise(),
                      tgbot::InlineKeyboardMarkup().serialise());
        }

        inline_query_result_cached_mpeg4_gif.set_title("sfhojmfdsg");
        ASSERT_EQ(inline_query_result_cached_mpeg4_gif.get_title(), "sfhojmfdsg");

        inline_query_result_cached_mpeg4_gif.set_type("wioejfh");
        ASSERT_EQ(inline_query_result_cached_mpeg4_gif.get_type(), "wioejfh");

        ASSERT_EQ(inline_query_result_cached_mpeg4_gif.serialise(), serialised_doc);
    }
}

TEST(InlineQueryResultCachedMpeg4Gif, errors)
{
    ASSERT_THROW(tgbot::InlineQueryResultCachedMpeg4Gif(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["id"] = "owihf";
        }

        if(i != 1)
        {
            doc["mpeg4_file_id"] = "icqun";
        }

        if(i != 2)
        {
            doc["type"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::InlineQueryResultCachedMpeg4Gif(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 9; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["caption"] = "djlhqg" : doc["caption"] = 4168;
        i != 1 ?
          doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())} :
          doc["caption_entities"] = 4168;
        i != 2 ? doc["id"] = "owihf" : doc["id"] = 65;
        i != 3 ?
          doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise()) :
          doc["input_message_content"] = 49;
        i != 4 ? doc["mpeg4_file_id"] = "opqgd" : doc["mpeg4_file_id"] = 486;
        i != 5 ? doc["parse_mode"] = "djlhqg" : doc["parse_mode"] = 4168;
        i != 6 ?
          doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise()) :
          doc["reply_markup"] = 49;
        i != 7 ? doc["title"] = "cxpjih" : doc["title"] = 739;
        i != 8 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;

        ASSERT_THROW(tgbot::InlineQueryResultCachedMpeg4Gif(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["caption_entities"] = {894345};
        doc["id"] = "owihf";
        doc["mpeg4_file_id"] = "icqun";
        doc["type"] = "aoisjgldfgs";

        ASSERT_THROW(tgbot::InlineQueryResultCachedMpeg4Gif(doc.dump()), std::invalid_argument);
    }
}
