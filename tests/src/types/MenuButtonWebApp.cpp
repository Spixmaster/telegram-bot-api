#include "tgbot/types/MenuButtonWebApp.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(MenuButtonWebApp, normal)
{
    ASSERT_NO_THROW(tgbot::MenuButtonWebApp(tgbot::MenuButtonWebApp().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["text"] = "rophnx";
    doc["type"] = "aerhpoiun";
    doc["web_app"] = nlohmann::json::parse(tgbot::WebAppInfo().serialise());

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::MenuButtonWebApp(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::MenuButtonWebApp menu_button_web_app;

        menu_button_web_app.set_text("rophnx");
        ASSERT_EQ(menu_button_web_app.get_text(), "rophnx");

        menu_button_web_app.set_type("aerhpoiun");
        ASSERT_EQ(menu_button_web_app.get_type(), "aerhpoiun");

        menu_button_web_app.set_web_app(std::make_shared<tgbot::WebAppInfo>());
        ASSERT_EQ(menu_button_web_app.get_web_app()->serialise(), tgbot::WebAppInfo().serialise());

        ASSERT_EQ(menu_button_web_app.serialise(), serialised_doc);
    }
}

TEST(MenuButtonWebApp, errors)
{
    ASSERT_THROW(tgbot::MenuButtonWebApp(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["text"] = "tipmuha";
        }

        if(i != 1)
        {
            doc["type"] = "topiasuih";
        }

        if(i != 2)
        {
            doc["web_app"] = nlohmann::json::parse(tgbot::WebAppInfo().serialise());
        }

        ASSERT_THROW(tgbot::MenuButtonWebApp(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["text"] = "rpoimasg" : doc["text"] = 81657;
        i != 1 ? doc["type"] = "eqpoing" : doc["type"] = 931457;
        i != 2 ? doc["web_app"] = nlohmann::json::parse(tgbot::WebAppInfo().serialise()) : doc["web_app"] = 9713546;

        ASSERT_THROW(tgbot::MenuButtonWebApp(doc.dump()), std::invalid_argument);
    }
}
