#include "tgbot/types/PassportElementErrorFrontSide.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(PassportElementErrorFrontSide, normal)
{
    ASSERT_NO_THROW(tgbot::PassportElementErrorFrontSide(tgbot::PassportElementErrorFrontSide().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["file_hash"] = "thsoijdg";
    doc["message"] = "hfsh";
    doc["source"] = "thposijdfg";
    doc["type"] = "yokfjg";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::PassportElementErrorFrontSide(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::PassportElementErrorFrontSide passport_element_error_front_side;

        passport_element_error_front_side.set_file_hash("thsoijdg");
        ASSERT_EQ(passport_element_error_front_side.get_file_hash(), "thsoijdg");

        passport_element_error_front_side.set_message("hfsh");
        ASSERT_EQ(passport_element_error_front_side.get_message(), "hfsh");

        passport_element_error_front_side.set_source("thposijdfg");
        ASSERT_EQ(passport_element_error_front_side.get_source(), "thposijdfg");

        passport_element_error_front_side.set_type("yokfjg");
        ASSERT_EQ(passport_element_error_front_side.get_type(), "yokfjg");

        ASSERT_EQ(passport_element_error_front_side.serialise(), serialised_doc);
    }
}

TEST(PassportElementErrorFrontSide, errors)
{
    ASSERT_THROW(tgbot::PassportElementErrorFrontSide(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["file_hash"] = "pobunfb";
        }

        if(i != 1)
        {
            doc["message"] = "yperoih";
        }

        if(i != 2)
        {
            doc["source"] = "aoisjgldfgs";
        }

        if(i != 3)
        {
            doc["type"] = "owihf";
        }

        ASSERT_THROW(tgbot::PassportElementErrorFrontSide(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["file_hash"] = "rpijsadv" : doc["file_hash"] = 168751;
        i != 1 ? doc["message"] = "xpoijqewr" : doc["message"] = -616;
        i != 2 ? doc["source"] = "xüijwre" : doc["source"] = 76;
        i != 3 ? doc["type"] = "tpwoiu" : doc["type"] = 6484;

        ASSERT_THROW(tgbot::PassportElementErrorFrontSide(doc.dump()), std::invalid_argument);
    }
}
