#include "tgbot/types/PreCheckoutQuery.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(PreCheckoutQuery, normal)
{
    ASSERT_NO_THROW(tgbot::PreCheckoutQuery(tgbot::PreCheckoutQuery().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["currency"] = "öoijpyoijsg";
    doc["from"] = nlohmann::json::parse(tgbot::User().serialise());
    doc["id"] = "oyisjkmeh";
    doc["invoice_payload"] = "sdfbsdfh";
    doc["order_info"] = nlohmann::json::parse(tgbot::OrderInfo().serialise());
    doc["shipping_option_id"] = "ouhdsdfg";
    doc["total_amount"] = -985478;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::PreCheckoutQuery(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::PreCheckoutQuery pre_checkout_query;

        pre_checkout_query.set_currency("öoijpyoijsg");
        ASSERT_EQ(pre_checkout_query.get_currency(), "öoijpyoijsg");

        pre_checkout_query.set_from(std::make_shared<tgbot::User>());
        ASSERT_EQ(pre_checkout_query.get_from()->serialise(), tgbot::User().serialise());

        pre_checkout_query.set_id("oyisjkmeh");
        ASSERT_EQ(pre_checkout_query.get_id(), "oyisjkmeh");

        pre_checkout_query.set_invoice_payload("sdfbsdfh");
        ASSERT_EQ(pre_checkout_query.get_invoice_payload(), "sdfbsdfh");

        pre_checkout_query.set_order_info(std::make_shared<tgbot::OrderInfo>());

        if(pre_checkout_query.get_order_info().has_value())
        {
            ASSERT_EQ(pre_checkout_query.get_order_info().value()->serialise(), tgbot::OrderInfo().serialise());
        }

        pre_checkout_query.set_shipping_option_id("ouhdsdfg");
        ASSERT_EQ(pre_checkout_query.get_shipping_option_id(), "ouhdsdfg");

        pre_checkout_query.set_total_amount(-985478);
        ASSERT_EQ(pre_checkout_query.get_total_amount(), -985478);

        ASSERT_EQ(pre_checkout_query.serialise(), serialised_doc);
    }
}

TEST(PreCheckoutQuery, errors)
{
    ASSERT_THROW(tgbot::PreCheckoutQuery(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 5; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["currency"] = "aoisjgldfgs";
        }

        if(i != 1)
        {
            doc["from"] = nlohmann::json::parse(tgbot::User().serialise());
        }

        if(i != 2)
        {
            doc["id"] = "aoisjgldfgs";
        }

        if(i != 3)
        {
            doc["invoice_payload"] = "rüoijc";
        }

        if(i != 4)
        {
            doc["total_amount"] = 89;
        }

        ASSERT_THROW(tgbot::PreCheckoutQuery(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 7; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["currency"] = "owihf" : doc["currency"] = 65;
        i != 1 ? doc["from"] = nlohmann::json::parse(tgbot::User().serialise()) : doc["from"] = 286;
        i != 2 ? doc["id"] = "qxcoijhwer" : doc["id"] = 113541;
        i != 3 ? doc["invoice_payload"] = "owihf" : doc["invoice_payload"] = 65;
        i != 4 ? doc["order_info"] = nlohmann::json::parse(tgbot::OrderInfo().serialise()) : doc["order_info"] = 286;
        i != 5 ? doc["shipping_option_id"] = "owihf" : doc["shipping_option_id"] = 65;
        i != 6 ? doc["total_amount"] = 6844 : doc["total_amount"] = "sdfg";

        ASSERT_THROW(tgbot::PreCheckoutQuery(doc.dump()), std::invalid_argument);
    }
}
