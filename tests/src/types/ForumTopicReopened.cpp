#include "tgbot/types/ForumTopicReopened.h"

#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(ForumTopicReopened, normal)
{
    ASSERT_NO_THROW(tgbot::ForumTopicReopened(tgbot::ForumTopicReopened().serialise()));

    const nlohmann::json doc = nlohmann::json::object();

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ForumTopicReopened(serialised_doc).serialise(), serialised_doc);

    {
        const tgbot::ForumTopicReopened forum_topic_reopened;

        ASSERT_EQ(forum_topic_reopened.serialise(), serialised_doc);
    }
}

TEST(ForumTopicReopened, errors)
{
    ASSERT_THROW(tgbot::ForumTopicReopened(nlohmann::json::array().dump()), std::invalid_argument);
}
