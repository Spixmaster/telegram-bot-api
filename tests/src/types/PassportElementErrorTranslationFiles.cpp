#include "tgbot/types/PassportElementErrorTranslationFiles.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(PassportElementErrorTranslationFiles, normal)
{
    ASSERT_NO_THROW(
      tgbot::PassportElementErrorTranslationFiles(tgbot::PassportElementErrorTranslationFiles().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["file_hashes"] = {"eöoinasdg"};
    doc["message"] = "ehposdjfg";
    doc["source"] = "ojasdgfdg";
    doc["type"] = "hyoijsdfgdfg";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::PassportElementErrorTranslationFiles(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::PassportElementErrorTranslationFiles passport_element_error_translation_files;

        passport_element_error_translation_files.set_file_hashes({"eöoinasdg"});
        ASSERT_EQ(passport_element_error_translation_files.get_file_hashes(), std::vector<std::string> {"eöoinasdg"});

        passport_element_error_translation_files.set_message("ehposdjfg");
        ASSERT_EQ(passport_element_error_translation_files.get_message(), "ehposdjfg");

        passport_element_error_translation_files.set_source("ojasdgfdg");
        ASSERT_EQ(passport_element_error_translation_files.get_source(), "ojasdgfdg");

        passport_element_error_translation_files.set_type("hyoijsdfgdfg");
        ASSERT_EQ(passport_element_error_translation_files.get_type(), "hyoijsdfgdfg");

        ASSERT_EQ(passport_element_error_translation_files.serialise(), serialised_doc);
    }
}

TEST(PassportElementErrorTranslationFiles, errors)
{
    ASSERT_THROW(tgbot::PassportElementErrorTranslationFiles(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["file_hashes"] = {"eöoinasdg"};
        }

        if(i != 1)
        {
            doc["message"] = "yperoih";
        }

        if(i != 2)
        {
            doc["source"] = "aoisjgldfgs";
        }

        if(i != 3)
        {
            doc["type"] = "owihf";
        }

        ASSERT_THROW(tgbot::PassportElementErrorTranslationFiles(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["file_hashes"] = {"eöoinasdg"} : doc["file_hashes"] = 65;
        i != 1 ? doc["message"] = "owihf" : doc["message"] = 711687;
        i != 2 ? doc["source"] = "aoisjgldfgs" : doc["source"] = 286;
        i != 3 ? doc["type"] = "owihf" : doc["type"] = 65;

        ASSERT_THROW(tgbot::PassportElementErrorTranslationFiles(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["file_hashes"] = {841684};
        doc["message"] = "yperoih";
        doc["source"] = "aoisjgldfgs";
        doc["type"] = "owihf";

        ASSERT_THROW(tgbot::PassportElementErrorTranslationFiles(doc.dump()), std::invalid_argument);
    }
}
