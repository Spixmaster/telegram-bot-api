#include "tgbot/types/PassportElementErrorReverseSide.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(PassportElementErrorReverseSide, normal)
{
    ASSERT_NO_THROW(tgbot::PassportElementErrorReverseSide(tgbot::PassportElementErrorReverseSide().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["file_hash"] = "thosijdfb";
    doc["message"] = "tühoisjdf";
    doc["source"] = "hosjdfg";
    doc["type"] = "yxfjgdfg";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::PassportElementErrorReverseSide(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::PassportElementErrorReverseSide passport_element_error_reverse_side;

        passport_element_error_reverse_side.set_file_hash("thosijdfb");
        ASSERT_EQ(passport_element_error_reverse_side.get_file_hash(), "thosijdfb");

        passport_element_error_reverse_side.set_message("tühoisjdf");
        ASSERT_EQ(passport_element_error_reverse_side.get_message(), "tühoisjdf");

        passport_element_error_reverse_side.set_source("hosjdfg");
        ASSERT_EQ(passport_element_error_reverse_side.get_source(), "hosjdfg");

        passport_element_error_reverse_side.set_type("yxfjgdfg");
        ASSERT_EQ(passport_element_error_reverse_side.get_type(), "yxfjgdfg");

        ASSERT_EQ(passport_element_error_reverse_side.serialise(), serialised_doc);
    }
}

TEST(PassportElementErrorReverseSide, errors)
{
    ASSERT_THROW(tgbot::PassportElementErrorReverseSide(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["file_hash"] = "pobunfb";
        }

        if(i != 1)
        {
            doc["message"] = "yperoih";
        }

        if(i != 2)
        {
            doc["source"] = "aoisjgldfgs";
        }

        if(i != 3)
        {
            doc["type"] = "owihf";
        }

        ASSERT_THROW(tgbot::PassportElementErrorReverseSide(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["file_hash"] = "xoaierg" : doc["file_hash"] = 783547;
        i != 1 ? doc["message"] = "rogijqsv" : doc["message"] = 65;
        i != 2 ? doc["source"] = "aoisjgldfgs" : doc["source"] = 981687;
        i != 3 ? doc["type"] = "rpoijasdv" : doc["type"] = 768487;

        ASSERT_THROW(tgbot::PassportElementErrorReverseSide(doc.dump()), std::invalid_argument);
    }
}
