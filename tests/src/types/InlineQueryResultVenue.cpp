#include "tgbot/types/InlineQueryResultVenue.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InlineQueryResultVenue, normal)
{
    ASSERT_NO_THROW(tgbot::InlineQueryResultVenue(tgbot::InlineQueryResultVenue().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["address"] = "wrphijsxcv";
    doc["foursquare_id"] = "thkjadsg";
    doc["foursquare_type"] = "fhoijmscv";
    doc["google_place_id"] = "thöoijscb";
    doc["google_place_type"] = "ztjkjsxg";
    doc["id"] = "rtjkmasdg";
    doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise());
    doc["latitude"] = 682.7F;
    doc["longitude"] = -64.6841F;
    doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise());
    doc["thumbnail_height"] = 684184;
    doc["thumbnail_url"] = "grhpjasdg";
    doc["thumbnail_width"] = -57410;
    doc["title"] = "hpüoijdölb";
    doc["type"] = "wehca";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InlineQueryResultVenue(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InlineQueryResultVenue inline_query_result_venue;

        inline_query_result_venue.set_address("wrphijsxcv");
        ASSERT_EQ(inline_query_result_venue.get_address(), "wrphijsxcv");

        inline_query_result_venue.set_foursquare_id("thkjadsg");
        ASSERT_EQ(inline_query_result_venue.get_foursquare_id(), "thkjadsg");

        inline_query_result_venue.set_foursquare_type("fhoijmscv");
        ASSERT_EQ(inline_query_result_venue.get_foursquare_type(), "fhoijmscv");

        inline_query_result_venue.set_google_place_id("thöoijscb");
        ASSERT_EQ(inline_query_result_venue.get_google_place_id(), "thöoijscb");

        inline_query_result_venue.set_google_place_type("ztjkjsxg");
        ASSERT_EQ(inline_query_result_venue.get_google_place_type(), "ztjkjsxg");

        inline_query_result_venue.set_id("rtjkmasdg");
        ASSERT_EQ(inline_query_result_venue.get_id(), "rtjkmasdg");

        inline_query_result_venue.set_input_message_content(std::make_shared<tgbot::InputMessageContent>());

        if(inline_query_result_venue.get_input_message_content().has_value())
        {
            ASSERT_EQ(inline_query_result_venue.get_input_message_content().value()->serialise(),
                      tgbot::InputMessageContent().serialise());
        }

        inline_query_result_venue.set_latitude(682.7F);
        ASSERT_EQ(inline_query_result_venue.get_latitude(), 682.7F);

        inline_query_result_venue.set_longitude(-64.6841F);
        ASSERT_EQ(inline_query_result_venue.get_longitude(), -64.6841F);

        inline_query_result_venue.set_reply_markup(std::make_shared<tgbot::InlineKeyboardMarkup>());

        if(inline_query_result_venue.get_reply_markup().has_value())
        {
            ASSERT_EQ(inline_query_result_venue.get_reply_markup().value()->serialise(),
                      tgbot::InlineKeyboardMarkup().serialise());
        }

        inline_query_result_venue.set_thumbnail_height(684184);
        ASSERT_EQ(inline_query_result_venue.get_thumbnail_height(), 684184);

        inline_query_result_venue.set_thumbnail_url("grhpjasdg");
        ASSERT_EQ(inline_query_result_venue.get_thumbnail_url(), "grhpjasdg");

        inline_query_result_venue.set_thumbnail_width(-57410);
        ASSERT_EQ(inline_query_result_venue.get_thumbnail_width(), -57410);

        inline_query_result_venue.set_title("hpüoijdölb");
        ASSERT_EQ(inline_query_result_venue.get_title(), "hpüoijdölb");

        inline_query_result_venue.set_type("wehca");
        ASSERT_EQ(inline_query_result_venue.get_type(), "wehca");

        ASSERT_EQ(inline_query_result_venue.serialise(), serialised_doc);
    }
}

TEST(InlineQueryResultVenue, errors)
{
    ASSERT_THROW(tgbot::InlineQueryResultVenue(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 6; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["address"] = "diopqb";
        }

        if(i != 1)
        {
            doc["id"] = "owihf";
        }

        if(i != 2)
        {
            doc["latitude"] = 671.8;
        }

        if(i != 3)
        {
            doc["longitude"] = 671.8;
        }

        if(i != 4)
        {
            doc["title"] = "diopqb";
        }

        if(i != 5)
        {
            doc["type"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::InlineQueryResultVenue(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 15; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["address"] = "cxpjih" : doc["address"] = 739;
        i != 1 ? doc["foursquare_id"] = "egaspdkj" : doc["foursquare_id"] = 22;
        i != 2 ? doc["foursquare_type"] = "rqhre" : doc["foursquare_type"] = 154;
        i != 3 ? doc["google_place_id"] = "xhrjd" : doc["google_place_id"] = 5;
        i != 4 ? doc["google_place_type"] = "wjscb" : doc["google_place_type"] = 741;
        i != 5 ? doc["id"] = "owihf" : doc["id"] = 65;
        i != 6 ?
          doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise()) :
          doc["input_message_content"] = 49;
        i != 7 ? doc["latitude"] = 731.5 : doc["latitude"] = "pranpoh";
        i != 8 ? doc["longitude"] = 671.5 : doc["longitude"] = "cns";
        i != 9 ?
          doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise()) :
          doc["reply_markup"] = 49;
        i != 10 ? doc["thumbnail_height"] = 187 : doc["thumbnail_height"] = "cns";
        i != 11 ? doc["thumbnail_url"] = "owihf" : doc["thumbnail_url"] = 65;
        i != 12 ? doc["thumbnail_width"] = 7315 : doc["thumbnail_width"] = "easfboh";
        i != 13 ? doc["title"] = "cxpjih" : doc["title"] = 739;
        i != 14 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;

        ASSERT_THROW(tgbot::InlineQueryResultVenue(doc.dump()), std::invalid_argument);
    }
}
