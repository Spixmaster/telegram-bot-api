#include "tgbot/types/InlineQueryResultLocation.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InlineQueryResultLocation, normal)
{
    ASSERT_NO_THROW(tgbot::InlineQueryResultLocation(tgbot::InlineQueryResultLocation().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["heading"] = 46814;
    doc["horizontal_accuracy"] = -5848.478F;
    doc["id"] = "wthöojmadsg";
    doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise());
    doc["latitude"] = -6.681F;
    doc["live_period"] = -68148;
    doc["longitude"] = 618.4F;
    doc["proximity_alert_radius"] = -6841684;
    doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise());
    doc["thumbnail_height"] = -68718;
    doc["thumbnail_url"] = "wethpoij";
    doc["thumbnail_width"] = -6841894;
    doc["title"] = "thökljmsdfg";
    doc["type"] = "wodfjbkdfg";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InlineQueryResultLocation(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InlineQueryResultLocation inline_query_result_location;

        inline_query_result_location.set_heading(46814);
        ASSERT_EQ(inline_query_result_location.get_heading(), 46814);

        inline_query_result_location.set_horizontal_accuracy(-5848.478F);
        ASSERT_EQ(inline_query_result_location.get_horizontal_accuracy(), -5848.478F);

        inline_query_result_location.set_id("wthöojmadsg");
        ASSERT_EQ(inline_query_result_location.get_id(), "wthöojmadsg");

        inline_query_result_location.set_input_message_content(std::make_shared<tgbot::InputMessageContent>());

        if(inline_query_result_location.get_input_message_content().has_value())
        {
            ASSERT_EQ(inline_query_result_location.get_input_message_content().value()->serialise(),
                      tgbot::InputMessageContent().serialise());
        }

        inline_query_result_location.set_latitude(-6.681F);
        ASSERT_EQ(inline_query_result_location.get_latitude(), -6.681F);

        inline_query_result_location.set_live_period(-68148);
        ASSERT_EQ(inline_query_result_location.get_live_period(), -68148);

        inline_query_result_location.set_longitude(618.4F);
        ASSERT_EQ(inline_query_result_location.get_longitude(), 618.4F);

        inline_query_result_location.set_proximity_alert_radius(-6841684);
        ASSERT_EQ(inline_query_result_location.get_proximity_alert_radius(), -6841684);

        inline_query_result_location.set_reply_markup(std::make_shared<tgbot::InlineKeyboardMarkup>());

        if(inline_query_result_location.get_reply_markup().has_value())
        {
            ASSERT_EQ(inline_query_result_location.get_reply_markup().value()->serialise(),
                      tgbot::InlineKeyboardMarkup().serialise());
        }

        inline_query_result_location.set_thumbnail_height(-68718);
        ASSERT_EQ(inline_query_result_location.get_thumbnail_height(), -68718);

        inline_query_result_location.set_thumbnail_url("wethpoij");
        ASSERT_EQ(inline_query_result_location.get_thumbnail_url(), "wethpoij");

        inline_query_result_location.set_thumbnail_width(-6841894);
        ASSERT_EQ(inline_query_result_location.get_thumbnail_width(), -6841894);

        inline_query_result_location.set_title("thökljmsdfg");
        ASSERT_EQ(inline_query_result_location.get_title(), "thökljmsdfg");

        inline_query_result_location.set_type("wodfjbkdfg");
        ASSERT_EQ(inline_query_result_location.get_type(), "wodfjbkdfg");

        ASSERT_EQ(inline_query_result_location.serialise(), serialised_doc);
    }
}

TEST(InlineQueryResultLocation, errors)
{
    ASSERT_THROW(tgbot::InlineQueryResultLocation(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 5; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["id"] = "owihf";
        }

        if(i != 1)
        {
            doc["latitude"] = 61.6;
        }

        if(i != 2)
        {
            doc["longitude"] = 67.6;
        }

        if(i != 3)
        {
            doc["title"] = "diopqb";
        }

        if(i != 4)
        {
            doc["type"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::InlineQueryResultLocation(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 14; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["heading"] = 819 : doc["heading"] = "diu";
        i != 1 ? doc["horizontal_accuracy"] = 819.81 : doc["horizontal_accuracy"] = "egqpsodij";
        i != 2 ? doc["id"] = "owihf" : doc["id"] = 65;
        i != 3 ?
          doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise()) :
          doc["input_message_content"] = 49;
        i != 4 ? doc["latitude"] = 195.81 : doc["latitude"] = "rina";
        i != 5 ? doc["live_period"] = 819 : doc["live_period"] = "diu";
        i != 6 ? doc["longitude"] = 195.81 : doc["longitude"] = "rina";
        i != 7 ? doc["proximity_alert_radius"] = 819 : doc["proximity_alert_radius"] = "diu";
        i != 8 ?
          doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise()) :
          doc["reply_markup"] = 49;
        i != 9 ? doc["thumbnail_height"] = 819 : doc["thumbnail_height"] = "diu";
        i != 10 ? doc["thumbnail_url"] = "pohgd" : doc["thumbnail_url"] = 197;
        i != 11 ? doc["thumbnail_width"] = 7357 : doc["thumbnail_width"] = "wqregoij";
        i != 12 ? doc["title"] = "cxpjih" : doc["title"] = 739;
        i != 13 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;

        ASSERT_THROW(tgbot::InlineQueryResultLocation(doc.dump()), std::invalid_argument);
    }
}
