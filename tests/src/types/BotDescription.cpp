#include "tgbot/types/BotDescription.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(BotDescription, normal)
{
    ASSERT_NO_THROW(tgbot::BotDescription(tgbot::BotDescription().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["description"] = "hqwdiojwerh";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::BotDescription(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::BotDescription bot_description;

        bot_description.set_description("hqwdiojwerh");
        ASSERT_EQ(bot_description.get_description(), "hqwdiojwerh");

        ASSERT_EQ(bot_description.serialise(), serialised_doc);
    }
}

TEST(BotDescription, errors)
{
    ASSERT_THROW(tgbot::BotDescription(nlohmann::json::array().dump()), std::invalid_argument);

    {
        const nlohmann::json doc = nlohmann::json::object();

        ASSERT_THROW(tgbot::BotDescription(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 1; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["description"] = "rhqüosidjg" : doc["description"] = 167;

        ASSERT_THROW(tgbot::BotDescription(doc.dump()), std::invalid_argument);
    }
}
