#include "tgbot/types/InlineQueryResultVideo.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InlineQueryResultVideo, normal)
{
    ASSERT_NO_THROW(tgbot::InlineQueryResultVideo(tgbot::InlineQueryResultVideo().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["caption"] = "chslidfjhc";
    doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())};
    doc["description"] = "dhscvdfg";
    doc["id"] = "rhsdfogj";
    doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise());
    doc["mime_type"] = "csdhfc";
    doc["parse_mode"] = "hscbsdfhd";
    doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise());
    doc["thumbnail_url"] = "dbsdfhsc";
    doc["title"] = "fhokmyxcg";
    doc["type"] = "erhoiajd";
    doc["video_duration"] = -681486;
    doc["video_height"] = 6814864;
    doc["video_url"] = "thlksjg";
    doc["video_width"] = -85168;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InlineQueryResultVideo(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InlineQueryResultVideo inline_query_result_video;

        inline_query_result_video.set_caption("chslidfjhc");
        ASSERT_EQ(inline_query_result_video.get_caption(), "chslidfjhc");

        inline_query_result_video.set_caption_entities(std::vector<tgbot::MessageEntity::ptr> {
          std::make_shared<tgbot::MessageEntity>()});

        if(inline_query_result_video.get_caption_entities().has_value())
        {
            ASSERT_EQ(inline_query_result_video.get_caption_entities().value().at(0)->serialise(),
                      tgbot::MessageEntity().serialise());
        }

        inline_query_result_video.set_description("dhscvdfg");
        ASSERT_EQ(inline_query_result_video.get_description(), "dhscvdfg");

        inline_query_result_video.set_id("rhsdfogj");
        ASSERT_EQ(inline_query_result_video.get_id(), "rhsdfogj");

        inline_query_result_video.set_input_message_content(std::make_shared<tgbot::InputMessageContent>());

        if(inline_query_result_video.get_input_message_content().has_value())
        {
            ASSERT_EQ(inline_query_result_video.get_input_message_content().value()->serialise(),
                      tgbot::InputMessageContent().serialise());
        }

        inline_query_result_video.set_mime_type("csdhfc");
        ASSERT_EQ(inline_query_result_video.get_mime_type(), "csdhfc");

        inline_query_result_video.set_parse_mode("hscbsdfhd");
        ASSERT_EQ(inline_query_result_video.get_parse_mode(), "hscbsdfhd");

        inline_query_result_video.set_reply_markup(std::make_shared<tgbot::InlineKeyboardMarkup>());

        if(inline_query_result_video.get_reply_markup().has_value())
        {
            ASSERT_EQ(inline_query_result_video.get_reply_markup().value()->serialise(),
                      tgbot::InlineKeyboardMarkup().serialise());
        }

        inline_query_result_video.set_thumbnail_url("dbsdfhsc");
        ASSERT_EQ(inline_query_result_video.get_thumbnail_url(), "dbsdfhsc");

        inline_query_result_video.set_title("fhokmyxcg");
        ASSERT_EQ(inline_query_result_video.get_title(), "fhokmyxcg");

        inline_query_result_video.set_type("erhoiajd");
        ASSERT_EQ(inline_query_result_video.get_type(), "erhoiajd");

        inline_query_result_video.set_video_duration(-681486);
        ASSERT_EQ(inline_query_result_video.get_video_duration(), -681486);

        inline_query_result_video.set_video_height(6814864);
        ASSERT_EQ(inline_query_result_video.get_video_height(), 6814864);

        inline_query_result_video.set_video_url("thlksjg");
        ASSERT_EQ(inline_query_result_video.get_video_url(), "thlksjg");

        inline_query_result_video.set_video_width(-85168);
        ASSERT_EQ(inline_query_result_video.get_video_width(), -85168);

        ASSERT_EQ(inline_query_result_video.serialise(), serialised_doc);
    }
}

TEST(InlineQueryResultVideo, errors)
{
    ASSERT_THROW(tgbot::InlineQueryResultVideo(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 6; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["id"] = "owihf";
        }

        if(i != 1)
        {
            doc["mime_type"] = "diopqb";
        }

        if(i != 2)
        {
            doc["thumbnail_url"] = "wrqoijsd";
        }

        if(i != 3)
        {
            doc["title"] = "qdfpoibj";
        }

        if(i != 4)
        {
            doc["type"] = "aoisjgldfgs";
        }

        if(i != 5)
        {
            doc["video_url"] = "icqun";
        }

        ASSERT_THROW(tgbot::InlineQueryResultVideo(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 15; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["caption"] = "djlhqg" : doc["caption"] = 4168;
        i != 1 ?
          doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())} :
          doc["caption_entities"] = 4168;
        i != 2 ? doc["description"] = "djlhqg" : doc["description"] = 4168;
        i != 3 ? doc["id"] = "owihf" : doc["id"] = 65;
        i != 4 ?
          doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise()) :
          doc["input_message_content"] = 49;
        i != 5 ? doc["mime_type"] = "opqgd" : doc["mime_type"] = 486;
        i != 6 ? doc["parse_mode"] = "djlhqg" : doc["parse_mode"] = 4168;
        i != 7 ?
          doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise()) :
          doc["reply_markup"] = 49;
        i != 8 ? doc["thumbnail_url"] = "opqgd" : doc["thumbnail_url"] = 486;
        i != 9 ? doc["title"] = "cxpjih" : doc["title"] = 739;
        i != 10 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;
        i != 11 ? doc["video_duration"] = 64 : doc["video_duration"] = "iho";
        i != 12 ? doc["video_height"] = 789 : doc["video_height"] = "cjha";
        i != 13 ? doc["video_url"] = "opqgd" : doc["video_url"] = 486;
        i != 14 ? doc["video_width"] = 789 : doc["video_width"] = "cjha";

        ASSERT_THROW(tgbot::InlineQueryResultVideo(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["caption_entities"] = {52315};
        doc["id"] = "owihf";
        doc["mime_type"] = "diopqb";
        doc["thumbnail_url"] = "qefhopijewr";
        doc["title"] = "diopqb";
        doc["type"] = "aoisjgldfgs";
        doc["video_url"] = "icqun";

        ASSERT_THROW(tgbot::InlineQueryResultVideo(doc.dump()), std::invalid_argument);
    }
}
