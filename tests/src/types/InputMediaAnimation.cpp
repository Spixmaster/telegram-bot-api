#include "tgbot/types/InputMediaAnimation.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InputMediaAnimation, normal)
{
    ASSERT_NO_THROW(tgbot::InputMediaAnimation(tgbot::InputMediaAnimation().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["caption"] = "twlhjc";
    doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())};
    doc["duration"] = -6875;
    doc["has_spoiler"] = false;
    doc["height"] = -851;
    doc["media"] = "comuojec";
    doc["parse_mode"] = "thlkjisc";
    doc["thumbnail"] = "rhzwoüimkc";
    doc["type"] = "xćojniowc";
    doc["width"] = 8518652;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InputMediaAnimation(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InputMediaAnimation input_media_animation;

        input_media_animation.set_caption("twlhjc");
        ASSERT_EQ(input_media_animation.get_caption(), "twlhjc");

        input_media_animation.set_caption_entities(std::vector<tgbot::MessageEntity::ptr> {
          std::make_shared<tgbot::MessageEntity>()});

        if(input_media_animation.get_caption_entities().has_value())
        {
            ASSERT_EQ(input_media_animation.get_caption_entities().value().at(0)->serialise(),
                      tgbot::MessageEntity().serialise());
        }

        input_media_animation.set_duration(-6875);
        ASSERT_EQ(input_media_animation.get_duration(), -6875);

        input_media_animation.set_has_spoiler(false);
        ASSERT_EQ(input_media_animation.get_has_spoiler(), false);

        input_media_animation.set_height(-851);
        ASSERT_EQ(input_media_animation.get_height(), -851);

        input_media_animation.set_media("comuojec");
        ASSERT_EQ(input_media_animation.get_media(), "comuojec");

        input_media_animation.set_parse_mode("thlkjisc");
        ASSERT_EQ(input_media_animation.get_parse_mode(), "thlkjisc");

        input_media_animation.set_thumbnail("rhzwoüimkc");
        ASSERT_EQ(input_media_animation.get_thumbnail(), "rhzwoüimkc");

        input_media_animation.set_type("xćojniowc");
        ASSERT_EQ(input_media_animation.get_type(), "xćojniowc");

        input_media_animation.set_width(8518652);
        ASSERT_EQ(input_media_animation.get_width(), 8518652);

        ASSERT_EQ(input_media_animation.serialise(), serialised_doc);
    }
}

TEST(InputMediaAnimation, errors)
{
    ASSERT_THROW(tgbot::InputMediaAnimation(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["media"] = "owihf";
        }

        if(i != 1)
        {
            doc["type"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::InputMediaAnimation(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 10; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["caption"] = "djlhqg" : doc["caption"] = 4168;
        i != 1 ?
          doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())} :
          doc["caption_entities"] = 4168;
        i != 2 ? doc["duration"] = 984 : doc["duration"] = "copij";
        i != 3 ? doc["has_spoiler"] = false : doc["has_spoiler"] = 97151;
        i != 4 ? doc["height"] = 984 : doc["height"] = "copij";
        i != 5 ? doc["media"] = "owihf" : doc["media"] = 65;
        i != 6 ? doc["parse_mode"] = "djlhqg" : doc["parse_mode"] = 4168;
        i != 7 ? doc["thumbnail"] = "opqgd" : doc["thumbnail"] = 486;
        i != 8 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;
        i != 9 ? doc["width"] = 984 : doc["width"] = "copij";

        ASSERT_THROW(tgbot::InputMediaAnimation(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["caption_entities"] = {79841};
        doc["media"] = "owihf";
        doc["type"] = "aoisjgldfgs";

        ASSERT_THROW(tgbot::InputMediaAnimation(doc.dump()), std::invalid_argument);
    }
}
