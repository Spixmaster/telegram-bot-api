#include "tgbot/types/ShippingQuery.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(ShippingQuery, normal)
{
    ASSERT_NO_THROW(tgbot::ShippingQuery(tgbot::ShippingQuery().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["from"] = nlohmann::json::parse(tgbot::User().serialise());
    doc["id"] = "söodvnb";
    doc["invoice_payload"] = "sfebyfpz";
    doc["shipping_address"] = nlohmann::json::parse(tgbot::ShippingAddress().serialise());

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ShippingQuery(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::ShippingQuery shipping_query;

        shipping_query.set_from(std::make_shared<tgbot::User>());
        ASSERT_EQ(shipping_query.get_from()->serialise(), tgbot::User().serialise());

        shipping_query.set_id("söodvnb");
        ASSERT_EQ(shipping_query.get_id(), "söodvnb");

        shipping_query.set_invoice_payload("sfebyfpz");
        ASSERT_EQ(shipping_query.get_invoice_payload(), "sfebyfpz");

        shipping_query.set_shipping_address(std::make_shared<tgbot::ShippingAddress>());
        ASSERT_EQ(shipping_query.get_shipping_address()->serialise(), tgbot::ShippingAddress().serialise());

        ASSERT_EQ(shipping_query.serialise(), serialised_doc);
    }
}

TEST(ShippingQuery, errors)
{
    ASSERT_THROW(tgbot::ShippingQuery(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["from"] = nlohmann::json::parse(tgbot::User().serialise());
        }

        if(i != 1)
        {
            doc["id"] = "aoisjgldfgs";
        }

        if(i != 2)
        {
            doc["invoice_payload"] = "twouz";
        }

        if(i != 3)
        {
            doc["shipping_address"] = nlohmann::json::parse(tgbot::ShippingAddress().serialise());
        }

        ASSERT_THROW(tgbot::ShippingQuery(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["from"] = nlohmann::json::parse(tgbot::User().serialise()) : doc["from"] = 468;
        i != 1 ? doc["id"] = "aoisjgldfgs" : doc["id"] = 286;
        i != 2 ? doc["invoice_payload"] = "rxashid" : doc["invoice_payload"] = 712657;
        i != 3 ?
          doc["shipping_address"] = nlohmann::json::parse(tgbot::ShippingAddress().serialise()) :
          doc["shipping_address"] = 468;

        ASSERT_THROW(tgbot::ShippingQuery(doc.dump()), std::invalid_argument);
    }
}
