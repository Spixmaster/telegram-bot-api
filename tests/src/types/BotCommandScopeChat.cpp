#include "tgbot/types/BotCommandScopeChat.h"

#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(BotCommandScopeChat, normal)
{
    ASSERT_NO_THROW(tgbot::BotCommandScopeChat(tgbot::BotCommandScopeChat().serialise()));

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i == 0 ? doc["chat_id"] = 84821 : doc["chat_id"] = "thofijpsiot";
        doc["type"] = "fcqopihfdg";

        const std::string serialised_doc = doc.dump();
        ASSERT_EQ(tgbot::BotCommandScopeChat(serialised_doc).serialise(), serialised_doc);

        {
            tgbot::BotCommandScopeChat bot_command_scope_chat;

            if(i == 0)
            {
                bot_command_scope_chat.set_chat_id(84821);
                ASSERT_EQ(std::get<std::int64_t>(bot_command_scope_chat.get_chat_id()), 84821);
            }
            else
            {
                bot_command_scope_chat.set_chat_id("thofijpsiot");
                ASSERT_EQ(std::get<std::string>(bot_command_scope_chat.get_chat_id()), "thofijpsiot");
            }

            bot_command_scope_chat.set_type("fcqopihfdg");
            ASSERT_EQ(bot_command_scope_chat.get_type(), "fcqopihfdg");

            ASSERT_EQ(bot_command_scope_chat.serialise(), serialised_doc);
        }
    }
}

TEST(BotCommandScopeChat, errors)
{
    ASSERT_THROW(tgbot::BotCommandScopeChat(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["chat_id"] = 86135;
        }

        if(i != 1)
        {
            doc["type"] = "daawe";
        }

        ASSERT_THROW(tgbot::BotCommandScopeChat(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["chat_id"] = 1871 : doc["chat_id"] = false;
        i != 1 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;

        ASSERT_THROW(tgbot::BotCommandScopeChat(doc.dump()), std::invalid_argument);
    }
}
