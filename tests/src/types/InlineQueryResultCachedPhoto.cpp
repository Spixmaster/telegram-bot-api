#include "tgbot/types/InlineQueryResultCachedPhoto.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InlineQueryResultCachedPhoto, normal)
{
    ASSERT_NO_THROW(tgbot::InlineQueryResultCachedPhoto(tgbot::InlineQueryResultCachedPhoto().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["caption"] = "sdfijnhdfg";
    doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())};
    doc["description"] = "sfbomijrg";
    doc["id"] = "ewjfg";
    doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise());
    doc["parse_mode"] = "sdfpogimqerg";
    doc["photo_file_id"] = "wtehpmoijsdf";
    doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise());
    doc["title"] = "goier";
    doc["type"] = "sadbpüsdfg";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InlineQueryResultCachedPhoto(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InlineQueryResultCachedPhoto inline_query_result_cached_photo;

        inline_query_result_cached_photo.set_caption("sdfijnhdfg");
        ASSERT_EQ(inline_query_result_cached_photo.get_caption(), "sdfijnhdfg");

        inline_query_result_cached_photo.set_caption_entities(std::vector<tgbot::MessageEntity::ptr> {
          std::make_shared<tgbot::MessageEntity>()});

        if(inline_query_result_cached_photo.get_caption_entities().has_value())
        {
            ASSERT_EQ(inline_query_result_cached_photo.get_caption_entities().value().at(0)->serialise(),
                      tgbot::MessageEntity().serialise());
        }

        inline_query_result_cached_photo.set_description("sfbomijrg");
        ASSERT_EQ(inline_query_result_cached_photo.get_description(), "sfbomijrg");

        inline_query_result_cached_photo.set_id("ewjfg");
        ASSERT_EQ(inline_query_result_cached_photo.get_id(), "ewjfg");

        inline_query_result_cached_photo.set_input_message_content(std::make_shared<tgbot::InputMessageContent>());

        if(inline_query_result_cached_photo.get_input_message_content().has_value())
        {
            ASSERT_EQ(inline_query_result_cached_photo.get_input_message_content().value()->serialise(),
                      tgbot::InputMessageContent().serialise());
        }

        inline_query_result_cached_photo.set_parse_mode("sdfpogimqerg");
        ASSERT_EQ(inline_query_result_cached_photo.get_parse_mode(), "sdfpogimqerg");

        inline_query_result_cached_photo.set_photo_file_id("wtehpmoijsdf");
        ASSERT_EQ(inline_query_result_cached_photo.get_photo_file_id(), "wtehpmoijsdf");

        inline_query_result_cached_photo.set_reply_markup(std::make_shared<tgbot::InlineKeyboardMarkup>());

        if(inline_query_result_cached_photo.get_reply_markup().has_value())
        {
            ASSERT_EQ(inline_query_result_cached_photo.get_reply_markup().value()->serialise(),
                      tgbot::InlineKeyboardMarkup().serialise());
        }

        inline_query_result_cached_photo.set_title("goier");
        ASSERT_EQ(inline_query_result_cached_photo.get_title(), "goier");

        inline_query_result_cached_photo.set_type("sadbpüsdfg");
        ASSERT_EQ(inline_query_result_cached_photo.get_type(), "sadbpüsdfg");

        ASSERT_EQ(inline_query_result_cached_photo.serialise(), serialised_doc);
    }
}

TEST(InlineQueryResultCachedPhoto, errors)
{
    ASSERT_THROW(tgbot::InlineQueryResultCachedPhoto(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["id"] = "owihf";
        }

        if(i != 1)
        {
            doc["photo_file_id"] = "icqun";
        }

        if(i != 2)
        {
            doc["type"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::InlineQueryResultCachedPhoto(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 10; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["caption"] = "djlhqg" : doc["caption"] = 4168;
        i != 1 ?
          doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())} :
          doc["caption_entities"] = 616;
        i != 2 ? doc["description"] = "cxpjih" : doc["description"] = 739;
        i != 3 ? doc["id"] = "owihf" : doc["id"] = 65;
        i != 4 ?
          doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise()) :
          doc["input_message_content"] = 1687;
        i != 5 ? doc["parse_mode"] = "djlhqg" : doc["parse_mode"] = 64818;
        i != 6 ? doc["photo_file_id"] = "opqgd" : doc["photo_file_id"] = 486;
        i != 7 ?
          doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise()) :
          doc["reply_markup"] = 49;
        i != 8 ? doc["title"] = "cxpjih" : doc["title"] = 739;
        i != 9 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;

        ASSERT_THROW(tgbot::InlineQueryResultCachedPhoto(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["caption_entities"] = {14642684};
        doc["id"] = "owihf";
        doc["photo_file_id"] = "icqun";
        doc["type"] = "aoisjgldfgs";

        ASSERT_THROW(tgbot::InlineQueryResultCachedPhoto(doc.dump()), std::invalid_argument);
    }
}
