#include "tgbot/types/InlineQueryResultCachedSticker.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"
#include "tgbot/types/InlineKeyboardMarkup.h"
#include "tgbot/types/InputMessageContent.h"

TEST(InlineQueryResultCachedSticker, normal)
{
    ASSERT_NO_THROW(tgbot::InlineQueryResultCachedSticker(tgbot::InlineQueryResultCachedSticker().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["id"] = "whomksf";
    doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise());
    doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise());
    doc["sticker_file_id"] = "sefbijnsdfg";
    doc["type"] = "aehokmjas";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InlineQueryResultCachedSticker(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InlineQueryResultCachedSticker inline_query_result_cached_sticker;

        inline_query_result_cached_sticker.set_id("whomksf");
        ASSERT_EQ(inline_query_result_cached_sticker.get_id(), "whomksf");

        inline_query_result_cached_sticker.set_input_message_content(std::make_shared<tgbot::InputMessageContent>());

        if(inline_query_result_cached_sticker.get_input_message_content().has_value())
        {
            ASSERT_EQ(inline_query_result_cached_sticker.get_input_message_content().value()->serialise(),
                      tgbot::InputMessageContent().serialise());
        }

        inline_query_result_cached_sticker.set_reply_markup(std::make_shared<tgbot::InlineKeyboardMarkup>());

        if(inline_query_result_cached_sticker.get_reply_markup().has_value())
        {
            ASSERT_EQ(inline_query_result_cached_sticker.get_reply_markup().value()->serialise(),
                      tgbot::InlineKeyboardMarkup().serialise());
        }

        inline_query_result_cached_sticker.set_sticker_file_id("sefbijnsdfg");
        ASSERT_EQ(inline_query_result_cached_sticker.get_sticker_file_id(), "sefbijnsdfg");

        inline_query_result_cached_sticker.set_type("aehokmjas");
        ASSERT_EQ(inline_query_result_cached_sticker.get_type(), "aehokmjas");

        ASSERT_EQ(inline_query_result_cached_sticker.serialise(), serialised_doc);
    }
}

TEST(InlineQueryResultCachedSticker, errors)
{
    ASSERT_THROW(tgbot::InlineQueryResultCachedSticker(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["id"] = "owihf";
        }

        if(i != 1)
        {
            doc["sticker_file_id"] = "icqun";
        }

        if(i != 2)
        {
            doc["type"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::InlineQueryResultCachedSticker(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 5; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["id"] = "owihf" : doc["id"] = 65;
        i != 1 ?
          doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise()) :
          doc["input_message_content"] = 49;
        i != 2 ?
          doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise()) :
          doc["reply_markup"] = 49;
        i != 3 ? doc["sticker_file_id"] = "opqgd" : doc["sticker_file_id"] = 486;
        i != 4 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;

        ASSERT_THROW(tgbot::InlineQueryResultCachedSticker(doc.dump()), std::invalid_argument);
    }
}
