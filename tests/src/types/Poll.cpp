#include "tgbot/types/Poll.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(Poll, normal)
{
    ASSERT_NO_THROW(tgbot::Poll(tgbot::Poll().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["allows_multiple_answers"] = false;
    doc["close_date"] = 87168;
    doc["correct_option_id"] = 641531;
    doc["explanation"] = "tbunsdg";
    doc["explanation_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())};
    doc["id"] = "erfbkjsodfjg";
    doc["is_anonymous"] = true;
    doc["is_closed"] = false;
    doc["open_period"] = 682184;
    doc["options"] = {nlohmann::json::parse(tgbot::PollOption().serialise())};
    doc["question"] = "wepfihgsdf";
    doc["total_voter_count"] = 68154;
    doc["type"] = "wefgjöosidjfg";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::Poll(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::Poll poll;

        poll.set_allows_multiple_answers(false);
        ASSERT_EQ(poll.get_allows_multiple_answers(), false);

        poll.set_close_date(87168);
        ASSERT_EQ(poll.get_close_date(), 87168);

        poll.set_correct_option_id(641531);
        ASSERT_EQ(poll.get_correct_option_id(), 641531);

        poll.set_explanation("tbunsdg");
        ASSERT_EQ(poll.get_explanation(), "tbunsdg");

        poll.set_explanation_entities(std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()});

        if(poll.get_explanation_entities().has_value())
        {
            ASSERT_EQ(poll.get_explanation_entities().value().at(0)->serialise(), tgbot::MessageEntity().serialise());
        }

        poll.set_id("erfbkjsodfjg");
        ASSERT_EQ(poll.get_id(), "erfbkjsodfjg");

        poll.set_is_anonymous(true);
        ASSERT_EQ(poll.get_is_anonymous(), true);

        poll.set_is_closed(false);
        ASSERT_EQ(poll.get_is_closed(), false);

        poll.set_open_period(682184);
        ASSERT_EQ(poll.get_open_period(), 682184);

        poll.set_options({std::make_shared<tgbot::PollOption>()});
        ASSERT_EQ(poll.get_options().at(0)->serialise(), tgbot::PollOption().serialise());

        poll.set_question("wepfihgsdf");
        ASSERT_EQ(poll.get_question(), "wepfihgsdf");

        poll.set_total_voter_count(68154);
        ASSERT_EQ(poll.get_total_voter_count(), 68154);

        poll.set_type("wefgjöosidjfg");
        ASSERT_EQ(poll.get_type(), "wefgjöosidjfg");

        ASSERT_EQ(poll.serialise(), serialised_doc);
    }
}

TEST(Poll, errors)
{
    ASSERT_THROW(tgbot::Poll(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 8; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["allows_multiple_answers"] = false;
        }

        if(i != 1)
        {
            doc["id"] = "aoisjgldfgs";
        }

        if(i != 2)
        {
            doc["is_anonymous"] = true;
        }

        if(i != 3)
        {
            doc["is_closed"] = true;
        }

        if(i != 4)
        {
            doc["options"] = {nlohmann::json::parse(tgbot::PollOption().serialise())};
        }

        if(i != 5)
        {
            doc["question"] = "owihf";
        }

        if(i != 6)
        {
            doc["total_voter_count"] = 87;
        }

        if(i != 7)
        {
            doc["type"] = "coiuhqhr";
        }

        ASSERT_THROW(tgbot::Poll(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 13; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["allows_multiple_answers"] = true : doc["allows_multiple_answers"] = 716875;
        i != 1 ? doc["close_date"] = 6844 : doc["close_date"] = "sdfg";
        i != 2 ? doc["correct_option_id"] = 76187 : doc["correct_option_id"] = "rophvas";
        i != 3 ? doc["explanation"] = "fpoijqrh" : doc["explanation"] = 974;
        i != 4 ?
          doc["explanation_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())} :
          doc["explanation_entities"] = 974;
        i != 5 ? doc["id"] = "aoisjgldfgs" : doc["id"] = 286;
        i != 6 ? doc["is_anonymous"] = true : doc["is_anonymous"] = 768744;
        i != 7 ? doc["is_closed"] = false : doc["is_closed"] = 32464;
        i != 8 ? doc["open_period"] = 6844 : doc["open_period"] = "sdfg";
        i != 9 ? doc["options"] = {nlohmann::json::parse(tgbot::PollOption().serialise())} : doc["options"] = 65;
        i != 10 ? doc["question"] = "owihf" : doc["question"] = 65;
        i != 11 ? doc["total_voter_count"] = 71687 : doc["total_voter_count"] = "rhoisdvaa";
        i != 12 ? doc["type"] = "qrpiuz" : doc["type"] = 72;

        ASSERT_THROW(tgbot::Poll(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["allows_multiple_answers"] = false;

        if(i == 0)
        {
            doc["explanation_entities"] = {689921};
        }

        doc["id"] = "aoisjgldfgs";
        doc["is_anonymous"] = true;
        doc["is_closed"] = true;

        if(i == 1)
        {
            doc["options"] = {146462814};
        }
        else
        {
            doc["options"] = {nlohmann::json::parse(tgbot::PollOption().serialise())};
        }

        doc["question"] = "owihf";
        doc["total_voter_count"] = 87;
        doc["type"] = "coiuhqhr";

        ASSERT_THROW(tgbot::Poll(doc.dump()), std::invalid_argument);
    }
}
