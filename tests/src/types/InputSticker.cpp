#include "tgbot/types/InputSticker.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InputSticker, normal)
{
    ASSERT_NO_THROW(tgbot::InputSticker(tgbot::InputSticker().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["emoji_list"] = {"xnpoijqsökj"};
    doc["keywords"] = {"uijqpümxyklmsh"};
    doc["mask_position"] = nlohmann::json::parse(tgbot::MaskPosition().serialise());
    doc["sticker"] = "oqomicshre";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InputSticker(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InputSticker input_sticker;

        input_sticker.set_emoji_list({"xnpoijqsökj"});
        ASSERT_EQ(input_sticker.get_emoji_list(), std::vector<std::string> {"xnpoijqsökj"});

        input_sticker.set_keywords({"uijqpümxyklmsh"});
        ASSERT_EQ(input_sticker.get_keywords(), std::vector<std::string> {"uijqpümxyklmsh"});

        input_sticker.set_mask_position(std::make_shared<tgbot::MaskPosition>());

        if(input_sticker.get_mask_position().has_value())
        {
            ASSERT_EQ(input_sticker.get_mask_position().value()->serialise(), tgbot::MaskPosition().serialise());
        }

        input_sticker.set_sticker("oqomicshre");
        ASSERT_EQ(input_sticker.get_sticker(), "oqomicshre");

        ASSERT_EQ(input_sticker.serialise(), serialised_doc);
    }
}

TEST(InputSticker, errors)
{
    ASSERT_THROW(tgbot::InputSticker(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["emoji_list"] = {"ziaspdzuw"};
        }

        if(i != 1)
        {
            doc["keywords"] = {"zuqpimoejt"};
        }

        if(i != 2)
        {
            doc["sticker"] = "yüpjhoiqw";
        }

        ASSERT_THROW(tgbot::InputSticker(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["emoji_list"] = {"xmeoijh"} : doc["emoji_list"] = 1687;
        i != 1 ? doc["keywords"] = {"qmvpnoij"} : doc["keywords"] = 17354;
        i != 2 ?
          doc["mask_position"] = nlohmann::json::parse(tgbot::MaskPosition().serialise()) :
          doc["mask_position"] = 718347;
        i != 3 ? doc["sticker"] = "xphoijq" : doc["sticker"] = 97318;

        ASSERT_THROW(tgbot::InputSticker(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["emoji_list"] = {"hrpouqfh"} : doc["emoji_list"] = {8168735};
        i != 1 ? doc["keywords"] = {"ghsaoidh"} : doc["keywords"] = {57135};
        doc["sticker"] = "izasnpoidh";

        ASSERT_THROW(tgbot::InputSticker(doc.dump()), std::invalid_argument);
    }
}
