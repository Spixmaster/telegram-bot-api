#include "tgbot/types/InputMediaPhoto.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InputMediaPhoto, normal)
{
    ASSERT_NO_THROW(tgbot::InputMediaPhoto(tgbot::InputMediaPhoto().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["caption"] = "üoidfbc";
    doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())};
    doc["has_spoiler"] = true;
    doc["media"] = "xxnpuahsfdg";
    doc["parse_mode"] = "cmxpoks";
    doc["type"] = "aojjsdgdf";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InputMediaPhoto(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InputMediaPhoto input_media_photo;

        input_media_photo.set_caption("üoidfbc");
        ASSERT_EQ(input_media_photo.get_caption(), "üoidfbc");

        input_media_photo.set_caption_entities(std::vector<tgbot::MessageEntity::ptr> {
          std::make_shared<tgbot::MessageEntity>()});

        if(input_media_photo.get_caption_entities().has_value())
        {
            ASSERT_EQ(
              input_media_photo.get_caption_entities().value().at(0)->serialise(), tgbot::MessageEntity().serialise());
        }

        input_media_photo.set_has_spoiler(true);
        ASSERT_EQ(input_media_photo.get_has_spoiler(), true);

        input_media_photo.set_media("xxnpuahsfdg");
        ASSERT_EQ(input_media_photo.get_media(), "xxnpuahsfdg");

        input_media_photo.set_parse_mode("cmxpoks");
        ASSERT_EQ(input_media_photo.get_parse_mode(), "cmxpoks");

        input_media_photo.set_type("aojjsdgdf");
        ASSERT_EQ(input_media_photo.get_type(), "aojjsdgdf");

        ASSERT_EQ(input_media_photo.serialise(), serialised_doc);
    }
}

TEST(InputMediaPhoto, errors)
{
    ASSERT_THROW(tgbot::InputMediaPhoto(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["media"] = "owihf";
        }

        if(i != 1)
        {
            doc["type"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::InputMediaPhoto(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 6; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["caption"] = "djlhqg" : doc["caption"] = 4168;
        i != 1 ?
          doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())} :
          doc["caption_entities"] = 4168;
        i != 2 ? doc["has_spoiler"] = false : doc["has_spoiler"] = 4861;
        i != 3 ? doc["media"] = "owihf" : doc["media"] = 65;
        i != 4 ? doc["parse_mode"] = "djlhqg" : doc["parse_mode"] = 4168;
        i != 5 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;

        ASSERT_THROW(tgbot::InputMediaPhoto(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["caption_entities"] = {7982315};
        doc["media"] = "owihf";
        doc["type"] = "aoisjgldfgs";

        ASSERT_THROW(tgbot::InputMediaPhoto(doc.dump()), std::invalid_argument);
    }
}
