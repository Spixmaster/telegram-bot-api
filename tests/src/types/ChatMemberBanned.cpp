#include "tgbot/types/ChatMemberBanned.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(ChatMemberBanned, normal)
{
    ASSERT_NO_THROW(tgbot::ChatMemberBanned(tgbot::ChatMemberBanned().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["status"] = "rohjpoifg";
    doc["until_date"] = 49194945;
    doc["user"] = nlohmann::json::parse(tgbot::User().serialise());

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ChatMemberBanned(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::ChatMemberBanned chat_member_banned;

        chat_member_banned.set_status("rohjpoifg");
        ASSERT_EQ(chat_member_banned.get_status(), "rohjpoifg");

        chat_member_banned.set_until_date(49194945);
        ASSERT_EQ(chat_member_banned.get_until_date(), 49194945);

        chat_member_banned.set_user(std::make_shared<tgbot::User>());
        ASSERT_EQ(chat_member_banned.get_user()->serialise(), tgbot::User().serialise());

        ASSERT_EQ(chat_member_banned.serialise(), serialised_doc);
    }
}

TEST(ChatMemberBanned, errors)
{
    ASSERT_THROW(tgbot::ChatMemberBanned(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["status"] = "owihf";
        }

        if(i != 1)
        {
            doc["until_date"] = 984;
        }

        if(i != 2)
        {
            doc["user"] = nlohmann::json::parse(tgbot::User().serialise());
        }

        ASSERT_THROW(tgbot::ChatMemberBanned(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["status"] = "owihf" : doc["status"] = 65;
        i != 1 ? doc["until_date"] = 89 : doc["until_date"] = "üriojhz";
        i != 2 ? doc["user"] = nlohmann::json::parse(tgbot::User().serialise()) : doc["user"] = 286;

        ASSERT_THROW(tgbot::ChatMemberBanned(doc.dump()), std::invalid_argument);
    }
}
