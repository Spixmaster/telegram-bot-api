#include "tgbot/types/InlineQueryResultCachedGif.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InlineQueryResultCachedGif, normal)
{
    ASSERT_NO_THROW(tgbot::InlineQueryResultCachedGif(tgbot::InlineQueryResultCachedGif().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["caption"] = "cpokjmdfh";
    doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())};
    doc["gif_file_id"] = "cpowimndf";
    doc["id"] = "copiwnfdb";
    doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise());
    doc["parse_mode"] = "coiuansdfg";
    doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise());
    doc["title"] = "coijmnsdfh";
    doc["type"] = "cposijdfh";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InlineQueryResultCachedGif(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InlineQueryResultCachedGif inline_query_result_cached_gif;

        inline_query_result_cached_gif.set_caption("cpokjmdfh");
        ASSERT_EQ(inline_query_result_cached_gif.get_caption(), "cpokjmdfh");

        inline_query_result_cached_gif.set_caption_entities(std::vector<tgbot::MessageEntity::ptr> {
          std::make_shared<tgbot::MessageEntity>()});

        if(inline_query_result_cached_gif.get_caption_entities().has_value())
        {
            ASSERT_EQ(inline_query_result_cached_gif.get_caption_entities().value().at(0)->serialise(),
                      tgbot::MessageEntity().serialise());
        }

        inline_query_result_cached_gif.set_gif_file_id("cpowimndf");
        ASSERT_EQ(inline_query_result_cached_gif.get_gif_file_id(), "cpowimndf");

        inline_query_result_cached_gif.set_id("copiwnfdb");
        ASSERT_EQ(inline_query_result_cached_gif.get_id(), "copiwnfdb");

        inline_query_result_cached_gif.set_input_message_content(std::make_shared<tgbot::InputMessageContent>());

        if(inline_query_result_cached_gif.get_input_message_content().has_value())
        {
            ASSERT_EQ(inline_query_result_cached_gif.get_input_message_content().value()->serialise(),
                      tgbot::InputMessageContent().serialise());
        }

        inline_query_result_cached_gif.set_parse_mode("coiuansdfg");
        ASSERT_EQ(inline_query_result_cached_gif.get_parse_mode(), "coiuansdfg");

        inline_query_result_cached_gif.set_reply_markup(std::make_shared<tgbot::InlineKeyboardMarkup>());

        if(inline_query_result_cached_gif.get_reply_markup().has_value())
        {
            ASSERT_EQ(inline_query_result_cached_gif.get_reply_markup().value()->serialise(),
                      tgbot::InlineKeyboardMarkup().serialise());
        }

        inline_query_result_cached_gif.set_title("coijmnsdfh");
        ASSERT_EQ(inline_query_result_cached_gif.get_title(), "coijmnsdfh");

        inline_query_result_cached_gif.set_type("cposijdfh");
        ASSERT_EQ(inline_query_result_cached_gif.get_type(), "cposijdfh");

        ASSERT_EQ(inline_query_result_cached_gif.serialise(), serialised_doc);
    }
}

TEST(InlineQueryResultCachedGif, errors)
{
    ASSERT_THROW(tgbot::InlineQueryResultCachedGif(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["gif_file_id"] = "icqun";
        }

        if(i != 1)
        {
            doc["id"] = "owihf";
        }

        if(i != 2)
        {
            doc["type"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::InlineQueryResultCachedGif(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 9; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["caption"] = "djlhqg" : doc["caption"] = 4168;
        i != 1 ?
          doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())} :
          doc["caption_entities"] = 4168;
        i != 2 ? doc["gif_file_id"] = "opqgd" : doc["gif_file_id"] = 486;
        i != 3 ? doc["id"] = "owihf" : doc["id"] = 65;
        i != 4 ?
          doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise()) :
          doc["input_message_content"] = 49;
        i != 5 ? doc["parse_mode"] = "djlhqg" : doc["parse_mode"] = 4168;
        i != 6 ?
          doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise()) :
          doc["reply_markup"] = 49;
        i != 7 ? doc["title"] = "cxpjih" : doc["title"] = 165;
        i != 8 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;

        ASSERT_THROW(tgbot::InlineQueryResultCachedGif(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["caption_entities"] = {28641};
        doc["gif_file_id"] = "icqun";
        doc["id"] = "owihf";
        doc["type"] = "aoisjgldfgs";

        ASSERT_THROW(tgbot::InlineQueryResultCachedGif(doc.dump()), std::invalid_argument);
    }
}
