#include "tgbot/types/VideoChatScheduled.h"

#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(VideoChatScheduled, normal)
{
    ASSERT_NO_THROW(tgbot::VideoChatScheduled(tgbot::VideoChatScheduled().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["start_date"] = 68154341;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::VideoChatScheduled(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::VideoChatScheduled voice_chat_scheduled;

        voice_chat_scheduled.set_start_date(68154341);
        ASSERT_EQ(voice_chat_scheduled.get_start_date(), 68154341);

        ASSERT_EQ(voice_chat_scheduled.serialise(), serialised_doc);
    }
}

TEST(VideoChatScheduled, errors)
{
    ASSERT_THROW(tgbot::VideoChatScheduled(nlohmann::json::array().dump()), std::invalid_argument);

    {
        const nlohmann::json doc = nlohmann::json::object();

        ASSERT_THROW(tgbot::VideoChatScheduled(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 1; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["start_date"] = 6844 : doc["start_date"] = "sdfg";

        ASSERT_THROW(tgbot::VideoChatScheduled(doc.dump()), std::invalid_argument);
    }
}
