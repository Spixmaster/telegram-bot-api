#include "tgbot/types/VideoNote.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(VideoNote, normal)
{
    ASSERT_NO_THROW(tgbot::VideoNote(tgbot::VideoNote().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["duration"] = 768864;
    doc["file_id"] = "oweifsdf";
    doc["file_size"] = -6821685;
    doc["file_unique_id"] = "oösiflkndf";
    doc["length"] = -78952615;
    doc["thumbnail"] = nlohmann::json::parse(tgbot::PhotoSize().serialise());

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::VideoNote(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::VideoNote video_note;

        video_note.set_duration(768864);
        ASSERT_EQ(video_note.get_duration(), 768864);

        video_note.set_file_id("oweifsdf");
        ASSERT_EQ(video_note.get_file_id(), "oweifsdf");

        video_note.set_file_size(-6821685);
        ASSERT_EQ(video_note.get_file_size(), -6821685);

        video_note.set_file_unique_id("oösiflkndf");
        ASSERT_EQ(video_note.get_file_unique_id(), "oösiflkndf");

        video_note.set_length(-78952615);
        ASSERT_EQ(video_note.get_length(), -78952615);

        video_note.set_thumbnail(std::make_shared<tgbot::PhotoSize>());

        if(video_note.get_thumbnail().has_value())
        {
            ASSERT_EQ(video_note.get_thumbnail().value()->serialise(), tgbot::PhotoSize().serialise());
        }

        ASSERT_EQ(video_note.serialise(), serialised_doc);
    }
}

TEST(VideoNote, errors)
{
    ASSERT_THROW(tgbot::VideoNote(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["duration"] = 7981684;
        }

        if(i != 1)
        {
            doc["file_id"] = "aoisjgldfgs";
        }

        if(i != 2)
        {
            doc["file_unique_id"] = "owihf";
        }

        if(i != 3)
        {
            doc["length"] = 6844;
        }

        ASSERT_THROW(tgbot::VideoNote(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 6; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["duration"] = 7981684 : doc["duration"] = "bsefbsdfh";
        i != 1 ? doc["file_id"] = "aoisjgldfgs" : doc["file_id"] = 286;
        i != 2 ? doc["file_size"] = 4561 : doc["file_size"] = "sdfhsdrg";
        i != 3 ? doc["file_unique_id"] = "owihf" : doc["file_unique_id"] = 65;
        i != 4 ? doc["length"] = 6844 : doc["length"] = "sdfg";
        i != 5 ? doc["thumbnail"] = nlohmann::json::parse(tgbot::PhotoSize().serialise()) : doc["thumbnail"] = 468;

        ASSERT_THROW(tgbot::VideoNote(doc.dump()), std::invalid_argument);
    }
}
