#include "tgbot/types/InlineQueryResultMpeg4Gif.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InlineQueryResultMpeg4Gif, normal)
{
    ASSERT_NO_THROW(tgbot::InlineQueryResultMpeg4Gif(tgbot::InlineQueryResultMpeg4Gif().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["caption"] = "thpomanoijsh";
    doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())};
    doc["id"] = "paoiunsodig";
    doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise());
    doc["mpeg4_duration"] = -8461;
    doc["mpeg4_height"] = 516286;
    doc["mpeg4_url"] = "qxnpoijsdfg";
    doc["mpeg4_width"] = -1654972;
    doc["parse_mode"] = "trhwpijmkcg";
    doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise());
    doc["thumbnail_mime_type"] = "qdjmgsc";
    doc["thumbnail_url"] = "ögsjmer";
    doc["title"] = "hiojmlköcsg";
    doc["type"] = "ösoijoiewfg";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InlineQueryResultMpeg4Gif(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InlineQueryResultMpeg4Gif inline_query_result_mpeg4_gif;

        inline_query_result_mpeg4_gif.set_caption("thpomanoijsh");
        ASSERT_EQ(inline_query_result_mpeg4_gif.get_caption(), "thpomanoijsh");

        inline_query_result_mpeg4_gif.set_caption_entities(std::vector<tgbot::MessageEntity::ptr> {
          std::make_shared<tgbot::MessageEntity>()});

        if(inline_query_result_mpeg4_gif.get_caption_entities().has_value())
        {
            ASSERT_EQ(inline_query_result_mpeg4_gif.get_caption_entities().value().at(0)->serialise(),
                      tgbot::MessageEntity().serialise());
        }

        inline_query_result_mpeg4_gif.set_id("paoiunsodig");
        ASSERT_EQ(inline_query_result_mpeg4_gif.get_id(), "paoiunsodig");

        inline_query_result_mpeg4_gif.set_input_message_content(std::make_shared<tgbot::InputMessageContent>());

        if(inline_query_result_mpeg4_gif.get_input_message_content().has_value())
        {
            ASSERT_EQ(inline_query_result_mpeg4_gif.get_input_message_content().value()->serialise(),
                      tgbot::InputMessageContent().serialise());
        }

        inline_query_result_mpeg4_gif.set_mpeg4_duration(-8461);
        ASSERT_EQ(inline_query_result_mpeg4_gif.get_mpeg4_duration(), -8461);

        inline_query_result_mpeg4_gif.set_mpeg4_height(516286);
        ASSERT_EQ(inline_query_result_mpeg4_gif.get_mpeg4_height(), 516286);

        inline_query_result_mpeg4_gif.set_mpeg4_url("qxnpoijsdfg");
        ASSERT_EQ(inline_query_result_mpeg4_gif.get_mpeg4_url(), "qxnpoijsdfg");

        inline_query_result_mpeg4_gif.set_mpeg4_width(-1654972);
        ASSERT_EQ(inline_query_result_mpeg4_gif.get_mpeg4_width(), -1654972);

        inline_query_result_mpeg4_gif.set_parse_mode("trhwpijmkcg");
        ASSERT_EQ(inline_query_result_mpeg4_gif.get_parse_mode(), "trhwpijmkcg");

        inline_query_result_mpeg4_gif.set_reply_markup(std::make_shared<tgbot::InlineKeyboardMarkup>());

        if(inline_query_result_mpeg4_gif.get_reply_markup().has_value())
        {
            ASSERT_EQ(inline_query_result_mpeg4_gif.get_reply_markup().value()->serialise(),
                      tgbot::InlineKeyboardMarkup().serialise());
        }

        inline_query_result_mpeg4_gif.set_thumbnail_mime_type("qdjmgsc");
        ASSERT_EQ(inline_query_result_mpeg4_gif.get_thumbnail_mime_type(), "qdjmgsc");

        inline_query_result_mpeg4_gif.set_thumbnail_url("ögsjmer");
        ASSERT_EQ(inline_query_result_mpeg4_gif.get_thumbnail_url(), "ögsjmer");

        inline_query_result_mpeg4_gif.set_title("hiojmlköcsg");
        ASSERT_EQ(inline_query_result_mpeg4_gif.get_title(), "hiojmlköcsg");

        inline_query_result_mpeg4_gif.set_type("ösoijoiewfg");
        ASSERT_EQ(inline_query_result_mpeg4_gif.get_type(), "ösoijoiewfg");

        ASSERT_EQ(inline_query_result_mpeg4_gif.serialise(), serialised_doc);
    }
}

TEST(InlineQueryResultMpeg4Gif, errors)
{
    ASSERT_THROW(tgbot::InlineQueryResultMpeg4Gif(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["id"] = "owihf";
        }

        if(i != 1)
        {
            doc["mpeg4_url"] = "icqun";
        }

        if(i != 2)
        {
            doc["thumbnail_url"] = "diopqb";
        }

        if(i != 3)
        {
            doc["type"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::InlineQueryResultMpeg4Gif(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 14; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["caption"] = "djlhqg" : doc["caption"] = 4168;
        i != 1 ?
          doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())} :
          doc["caption_entities"] = 4168;
        i != 2 ? doc["id"] = "owihf" : doc["id"] = 65;
        i != 3 ?
          doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise()) :
          doc["input_message_content"] = 49;
        i != 4 ? doc["mpeg4_duration"] = 618 : doc["mpeg4_duration"] = "cipo";
        i != 5 ? doc["mpeg4_height"] = 167354 : doc["mpeg4_height"] = "rhpoif";
        i != 6 ? doc["mpeg4_url"] = "opqgd" : doc["mpeg4_url"] = 486;
        i != 7 ? doc["mpeg4_width"] = 3678531 : doc["mpeg4_width"] = "ekajsdg";
        i != 8 ? doc["parse_mode"] = "djlhqg" : doc["parse_mode"] = 4168;
        i != 9 ?
          doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise()) :
          doc["reply_markup"] = 49;
        i != 10 ? doc["thumbnail_mime_type"] = "qwürogijerh" : doc["thumbnail_mime_type"] = 75141;
        i != 11 ? doc["thumbnail_url"] = "ewqpoisrh" : doc["thumbnail_url"] = 744;
        i != 12 ? doc["title"] = "cxpjih" : doc["title"] = 739;
        i != 13 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;

        ASSERT_THROW(tgbot::InlineQueryResultMpeg4Gif(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["caption_entities"] = {314562};
        doc["id"] = "owihf";
        doc["mpeg4_url"] = "icqun";
        doc["thumbnail_url"] = "diopqb";
        doc["type"] = "aoisjgldfgs";

        ASSERT_THROW(tgbot::InlineQueryResultMpeg4Gif(doc.dump()), std::invalid_argument);
    }
}
