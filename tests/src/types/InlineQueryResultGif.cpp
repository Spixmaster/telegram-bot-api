#include "tgbot/types/InlineQueryResultGif.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InlineQueryResultGif, normal)
{
    ASSERT_NO_THROW(tgbot::InlineQueryResultGif(tgbot::InlineQueryResultGif().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["caption"] = "thoijmc";
    doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())};
    doc["gif_duration"] = -682;
    doc["gif_height"] = 6842168;
    doc["gif_url"] = "thsücoimbk";
    doc["gif_width"] = -6812;
    doc["id"] = "trhpomsc";
    doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise());
    doc["parse_mode"] = "whimoc";
    doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise());
    doc["thumbnail_mime_type"] = "thlkjmscg";
    doc["thumbnail_url"] = "chstehwer";
    doc["title"] = "wrthijmc";
    doc["type"] = "wethökjmasg";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InlineQueryResultGif(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InlineQueryResultGif inline_Query_result_gif;

        inline_Query_result_gif.set_caption("thoijmc");
        ASSERT_EQ(inline_Query_result_gif.get_caption(), "thoijmc");

        inline_Query_result_gif.set_caption_entities(std::vector<tgbot::MessageEntity::ptr> {
          std::make_shared<tgbot::MessageEntity>()});

        if(inline_Query_result_gif.get_caption_entities().has_value())
        {
            ASSERT_EQ(inline_Query_result_gif.get_caption_entities().value().at(0)->serialise(),
                      tgbot::MessageEntity().serialise());
        }

        inline_Query_result_gif.set_gif_duration(-682);
        ASSERT_EQ(inline_Query_result_gif.get_gif_duration(), -682);

        inline_Query_result_gif.set_gif_height(6842168);
        ASSERT_EQ(inline_Query_result_gif.get_gif_height(), 6842168);

        inline_Query_result_gif.set_gif_url("thsücoimbk");
        ASSERT_EQ(inline_Query_result_gif.get_gif_url(), "thsücoimbk");

        inline_Query_result_gif.set_gif_width(-6812);
        ASSERT_EQ(inline_Query_result_gif.get_gif_width(), -6812);

        inline_Query_result_gif.set_id("trhpomsc");
        ASSERT_EQ(inline_Query_result_gif.get_id(), "trhpomsc");

        inline_Query_result_gif.set_input_message_content(std::make_shared<tgbot::InputMessageContent>());

        if(inline_Query_result_gif.get_input_message_content().has_value())
        {
            ASSERT_EQ(inline_Query_result_gif.get_input_message_content().value()->serialise(),
                      tgbot::InputMessageContent().serialise());
        }

        inline_Query_result_gif.set_parse_mode("whimoc");
        ASSERT_EQ(inline_Query_result_gif.get_parse_mode(), "whimoc");

        inline_Query_result_gif.set_reply_markup(std::make_shared<tgbot::InlineKeyboardMarkup>());

        if(inline_Query_result_gif.get_reply_markup().has_value())
        {
            ASSERT_EQ(inline_Query_result_gif.get_reply_markup().value()->serialise(),
                      tgbot::InlineKeyboardMarkup().serialise());
        }

        inline_Query_result_gif.set_thumbnail_mime_type("thlkjmscg");
        ASSERT_EQ(inline_Query_result_gif.get_thumbnail_mime_type(), "thlkjmscg");

        inline_Query_result_gif.set_thumbnail_url("chstehwer");
        ASSERT_EQ(inline_Query_result_gif.get_thumbnail_url(), "chstehwer");

        inline_Query_result_gif.set_title("wrthijmc");
        ASSERT_EQ(inline_Query_result_gif.get_title(), "wrthijmc");

        inline_Query_result_gif.set_type("wethökjmasg");
        ASSERT_EQ(inline_Query_result_gif.get_type(), "wethökjmasg");

        ASSERT_EQ(inline_Query_result_gif.serialise(), serialised_doc);
    }
}

TEST(InlineQueryResultGif, errors)
{
    ASSERT_THROW(tgbot::InlineQueryResultGif(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["gif_url"] = "icqun";
        }

        if(i != 1)
        {
            doc["id"] = "owihf";
        }

        if(i != 2)
        {
            doc["thumbnail_url"] = "diopqb";
        }

        if(i != 3)
        {
            doc["type"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::InlineQueryResultGif(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 14; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["caption"] = "djlhqg" : doc["caption"] = 4168;
        i != 1 ?
          doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())} :
          doc["caption_entities"] = 4168;
        i != 2 ? doc["gif_duration"] = 198 : doc["gif_duration"] = "coj";
        i != 3 ? doc["gif_height"] = 8167 : doc["gif_height"] = "rhwoifjdg";
        i != 4 ? doc["gif_url"] = "opqgd" : doc["gif_url"] = 486;
        i != 5 ? doc["gif_width"] = 23482456 : doc["gif_width"] = "qpoixcher";
        i != 6 ? doc["id"] = "owihf" : doc["id"] = 65;
        i != 7 ?
          doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise()) :
          doc["input_message_content"] = 732545644;
        i != 8 ? doc["parse_mode"] = "djlhqg" : doc["parse_mode"] = 4168;
        i != 9 ?
          doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise()) :
          doc["reply_markup"] = 49;
        i != 10 ? doc["thumbnail_mime_type"] = "cxpjih" : doc["thumbnail_mime_type"] = 739;
        i != 11 ? doc["thumbnail_url"] = "rpoadshg" : doc["thumbnail_url"] = 734687;
        i != 12 ? doc["title"] = "cxpjih" : doc["title"] = 987354;
        i != 13 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;

        ASSERT_THROW(tgbot::InlineQueryResultGif(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["caption_entities"] = {76264};
        doc["gif_url"] = "icqun";
        doc["id"] = "owihf";
        doc["thumbnail_url"] = "diopqb";
        doc["type"] = "aoisjgldfgs";

        ASSERT_THROW(tgbot::InlineQueryResultGif(doc.dump()), std::invalid_argument);
    }
}
