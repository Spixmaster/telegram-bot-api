#include "tgbot/types/WebAppData.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(WebAppData, normal)
{
    ASSERT_NO_THROW(tgbot::WebAppData(tgbot::WebAppData().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["button_text"] = "thkmoa";
    doc["data"] = "roixagr";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::WebAppData(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::WebAppData web_app_data;

        web_app_data.set_button_text("thkmoa");
        ASSERT_EQ(web_app_data.get_button_text(), "thkmoa");

        web_app_data.set_data("roixagr");
        ASSERT_EQ(web_app_data.get_data(), "roixagr");

        ASSERT_EQ(web_app_data.serialise(), serialised_doc);
    }
}

TEST(WebAppData, errors)
{
    ASSERT_THROW(tgbot::WebAppData(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["button_text"] = "rhpoierlhlk";
        }

        if(i != 1)
        {
            doc["data"] = "trohab";
        }

        ASSERT_THROW(tgbot::WebAppData(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["button_text"] = "rgpoijad" : doc["button_text"] = 1498732;
        i != 1 ? doc["data"] = "hrxijseht" : doc["data"] = 645786;

        ASSERT_THROW(tgbot::WebAppData(doc.dump()), std::invalid_argument);
    }
}
