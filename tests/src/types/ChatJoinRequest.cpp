#include "tgbot/types/ChatJoinRequest.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(ChatJoinRequest, normal)
{
    ASSERT_NO_THROW(tgbot::ChatJoinRequest(tgbot::ChatJoinRequest().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["bio"] = "fhpijqdig";
    doc["chat"] = nlohmann::json::parse(tgbot::Chat().serialise());
    doc["date"] = 14174;
    doc["from"] = nlohmann::json::parse(tgbot::User().serialise());
    doc["invite_link"] = nlohmann::json::parse(tgbot::ChatInviteLink().serialise());
    doc["user_chat_id"] = 56716567;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ChatJoinRequest(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::ChatJoinRequest chat_join_request;

        chat_join_request.set_bio("fhpijqdig");
        ASSERT_EQ(chat_join_request.get_bio(), "fhpijqdig");

        chat_join_request.set_chat(std::make_shared<tgbot::Chat>());
        ASSERT_EQ(chat_join_request.get_chat()->serialise(), tgbot::Chat().serialise());

        chat_join_request.set_date(14174);
        ASSERT_EQ(chat_join_request.get_date(), 14174);

        chat_join_request.set_from(std::make_shared<tgbot::User>());
        ASSERT_EQ(chat_join_request.get_from()->serialise(), tgbot::User().serialise());

        chat_join_request.set_invite_link(std::make_shared<tgbot::ChatInviteLink>());

        if(chat_join_request.get_invite_link().has_value())
        {
            ASSERT_EQ(chat_join_request.get_invite_link().value()->serialise(), tgbot::ChatInviteLink().serialise());
        }

        chat_join_request.set_user_chat_id(56716567);
        ASSERT_EQ(chat_join_request.get_user_chat_id(), 56716567);

        ASSERT_EQ(chat_join_request.serialise(), serialised_doc);
    }
}

TEST(ChatJoinRequest, errors)
{
    ASSERT_THROW(tgbot::ChatJoinRequest(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["chat"] = nlohmann::json::parse(tgbot::Chat().serialise());
        }

        if(i != 1)
        {
            doc["date"] = 76135;
        }

        if(i != 2)
        {
            doc["from"] = nlohmann::json::parse(tgbot::User().serialise());
        }

        if(i != 3)
        {
            doc["user_chat_id"] = 2687516;
        }

        ASSERT_THROW(tgbot::ChatJoinRequest(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 6; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["bio"] = "rdqoincr" : doc["bio"] = 543;
        i != 1 ? doc["chat"] = nlohmann::json::parse(tgbot::Chat().serialise()) : doc["chat"] = 41;
        i != 2 ? doc["date"] = 781 : doc["date"] = "rhwfv";
        i != 3 ? doc["from"] = nlohmann::json::parse(tgbot::User().serialise()) : doc["from"] = 93;
        i != 4 ?
          doc["invite_link"] = nlohmann::json::parse(tgbot::ChatInviteLink().serialise()) :
          doc["invite_link"] = 718;
        i != 5 ? doc["user_chat_id"] = 46871 : doc["user_chat_id"] = "ogaopuhsdg";

        ASSERT_THROW(tgbot::ChatJoinRequest(doc.dump()), std::invalid_argument);
    }
}
