#include "tgbot/types/ChatMemberAdministrator.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(ChatMemberAdministrator, normal)
{
    ASSERT_NO_THROW(tgbot::ChatMemberAdministrator(tgbot::ChatMemberAdministrator().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["can_be_edited"] = false;
    doc["can_change_info"] = false;
    doc["can_delete_messages"] = true;
    doc["can_edit_messages"] = false;
    doc["can_invite_users"] = true;
    doc["can_manage_chat"] = true;
    doc["can_manage_topics"] = false;
    doc["can_manage_video_chats"] = false;
    doc["can_pin_messages"] = false;
    doc["can_post_messages"] = true;
    doc["can_promote_members"] = true;
    doc["can_restrict_members"] = true;
    doc["custom_title"] = "fhoandf";
    doc["is_anonymous"] = true;
    doc["status"] = "wfodijmns";
    doc["user"] = nlohmann::json::parse(tgbot::User().serialise());

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ChatMemberAdministrator(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::ChatMemberAdministrator chat_member_administrator;

        chat_member_administrator.set_can_be_edited(false);
        ASSERT_EQ(chat_member_administrator.get_can_be_edited(), false);

        chat_member_administrator.set_can_change_info(false);
        ASSERT_EQ(chat_member_administrator.get_can_change_info(), false);

        chat_member_administrator.set_can_delete_messages(true);
        ASSERT_EQ(chat_member_administrator.get_can_delete_messages(), true);

        chat_member_administrator.set_can_edit_messages(false);
        ASSERT_EQ(chat_member_administrator.get_can_edit_messages(), false);

        chat_member_administrator.set_can_invite_users(true);
        ASSERT_EQ(chat_member_administrator.get_can_invite_users(), true);

        chat_member_administrator.set_can_manage_chat(true);
        ASSERT_EQ(chat_member_administrator.get_can_manage_chat(), true);

        chat_member_administrator.set_can_manage_topics(false);
        ASSERT_EQ(chat_member_administrator.get_can_manage_topics(), false);

        chat_member_administrator.set_can_manage_video_chats(false);
        ASSERT_EQ(chat_member_administrator.get_can_manage_video_chats(), false);

        chat_member_administrator.set_can_pin_messages(false);
        ASSERT_EQ(chat_member_administrator.get_can_pin_messages(), false);

        chat_member_administrator.set_can_post_messages(true);
        ASSERT_EQ(chat_member_administrator.get_can_post_messages(), true);

        chat_member_administrator.set_can_promote_members(true);
        ASSERT_EQ(chat_member_administrator.get_can_promote_members(), true);

        chat_member_administrator.set_can_restrict_members(true);
        ASSERT_EQ(chat_member_administrator.get_can_restrict_members(), true);

        chat_member_administrator.set_custom_title("fhoandf");
        ASSERT_EQ(chat_member_administrator.get_custom_title(), "fhoandf");

        chat_member_administrator.set_is_anonymous(true);
        ASSERT_EQ(chat_member_administrator.get_is_anonymous(), true);

        chat_member_administrator.set_status("wfodijmns");
        ASSERT_EQ(chat_member_administrator.get_status(), "wfodijmns");

        chat_member_administrator.set_user(std::make_shared<tgbot::User>());
        ASSERT_EQ(chat_member_administrator.get_user()->serialise(), tgbot::User().serialise());

        ASSERT_EQ(chat_member_administrator.serialise(), serialised_doc);
    }
}

TEST(ChatMemberAdministrator, errors)
{
    ASSERT_THROW(tgbot::ChatMemberAdministrator(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 11; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["can_be_edited"] = true;
        }

        if(i != 1)
        {
            doc["can_change_info"] = false;
        }

        if(i != 2)
        {
            doc["can_delete_messages"] = false;
        }

        if(i != 3)
        {
            doc["can_invite_users"] = true;
        }

        if(i != 4)
        {
            doc["can_manage_chat"] = false;
        }

        if(i != 5)
        {
            doc["can_manage_video_chats"] = true;
        }

        if(i != 6)
        {
            doc["can_promote_members"] = true;
        }

        if(i != 7)
        {
            doc["can_restrict_members"] = false;
        }

        if(i != 8)
        {
            doc["is_anonymous"] = true;
        }

        if(i != 9)
        {
            doc["status"] = "owihf";
        }

        if(i != 10)
        {
            doc["user"] = nlohmann::json::parse(tgbot::User().serialise());
        }

        ASSERT_THROW(tgbot::ChatMemberAdministrator(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 16; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["can_be_edited"] = true : doc["can_be_edited"] = 6856;
        i != 1 ? doc["can_change_info"] = false : doc["can_change_info"] = 38715;
        i != 2 ? doc["can_delete_messages"] = true : doc["can_delete_messages"] = 9872;
        i != 3 ? doc["can_edit_messages"] = false : doc["can_edit_messages"] = 7928416;
        i != 4 ? doc["can_invite_users"] = true : doc["can_invite_users"] = 8431464;
        i != 5 ? doc["can_manage_chat"] = false : doc["can_manage_chat"] = 79131;
        i != 6 ? doc["can_manage_topics"] = false : doc["can_manage_topics"] = 7962431;
        i != 7 ? doc["can_manage_video_chats"] = false : doc["can_manage_video_chats"] = 71354897;
        i != 8 ? doc["can_pin_messages"] = false : doc["can_pin_messages"] = 7311549;
        i != 9 ? doc["can_post_messages"] = false : doc["can_post_messages"] = 965712;
        i != 10 ? doc["can_promote_members"] = true : doc["can_promote_members"] = 7134897;
        i != 11 ? doc["can_restrict_members"] = false : doc["can_restrict_members"] = 9132478;
        i != 12 ? doc["custom_title"] = "pnw" : doc["custom_title"] = 684;
        i != 13 ? doc["is_anonymous"] = false : doc["is_anonymous"] = 67316;
        i != 14 ? doc["status"] = "owihf" : doc["status"] = 65;
        i != 15 ? doc["user"] = nlohmann::json::parse(tgbot::User().serialise()) : doc["user"] = 286;

        ASSERT_THROW(tgbot::ChatMemberAdministrator(doc.dump()), std::invalid_argument);
    }
}
