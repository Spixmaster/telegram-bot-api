#include "tgbot/types/InlineKeyboardMarkup.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InlineKeyboardMarkup, normal)
{
    ASSERT_NO_THROW(tgbot::InlineKeyboardMarkup(tgbot::InlineKeyboardMarkup().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["inline_keyboard"] = {{nlohmann::json::parse(tgbot::InlineKeyboardButton().serialise())}};

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InlineKeyboardMarkup(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InlineKeyboardMarkup inline_keyboard_markup;

        inline_keyboard_markup.set_inline_keyboard({{std::make_shared<tgbot::InlineKeyboardButton>()}});
        ASSERT_EQ(inline_keyboard_markup.get_inline_keyboard().at(0).at(0)->serialise(),
                  tgbot::InlineKeyboardButton().serialise());

        ASSERT_EQ(inline_keyboard_markup.serialise(), serialised_doc);
    }

    {
        ASSERT_NO_THROW(tgbot::InlineKeyboardMarkup(std::vector<std::vector<tgbot::InlineKeyboardButton::ptr>> {}));
    }
}

TEST(InlineKeyboardMarkup, errors)
{
    ASSERT_THROW(tgbot::InlineKeyboardMarkup(nlohmann::json::array().dump()), std::invalid_argument);

    {
        const nlohmann::json doc = nlohmann::json::object();

        ASSERT_THROW(tgbot::InlineKeyboardMarkup(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 1; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ?
          doc["inline_keyboard"] = {{nlohmann::json::parse(tgbot::InlineKeyboardButton().serialise())}} :
          doc["inline_keyboard"] = 468;

        ASSERT_THROW(tgbot::InlineKeyboardMarkup(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["inline_keyboard"] = {47827};

        ASSERT_THROW(tgbot::InlineKeyboardMarkup(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["inline_keyboard"] = {{3284}};

        ASSERT_THROW(tgbot::InlineKeyboardMarkup(doc.dump()), std::invalid_argument);
    }
}
