#include "tgbot/types/InputInvoiceMessageContent.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InputInvoiceMessageContent, normal)
{
    ASSERT_NO_THROW(tgbot::InputInvoiceMessageContent(tgbot::InputInvoiceMessageContent().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["currency"] = "ciojmfh";
    doc["description"] = "sokkjht";
    doc["is_flexible"] = false;
    doc["max_tip_amount"] = 746184;
    doc["need_email"] = false;
    doc["need_name"] = false;
    doc["need_phone_number"] = true;
    doc["need_shipping_address"] = false;
    doc["payload"] = "wocnujht";
    doc["photo_height"] = 513535;
    doc["photo_size"] = 6165;
    doc["photo_url"] = "oufhr";
    doc["photo_width"] = 339814;
    doc["prices"] = {nlohmann::json::parse(tgbot::LabeledPrice().serialise())};
    doc["provider_data"] = "wpoimjnbwf";
    doc["provider_token"] = "wpmböoiwjf";
    doc["send_email_to_provider"] = true;
    doc["send_phone_number_to_provider"] = true;
    doc["suggested_tip_amounts"] = {2651};
    doc["title"] = "jmioflmbk";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InputInvoiceMessageContent(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InputInvoiceMessageContent input_invoice_message_content;

        input_invoice_message_content.set_currency("ciojmfh");
        ASSERT_EQ(input_invoice_message_content.get_currency(), "ciojmfh");

        input_invoice_message_content.set_description("sokkjht");
        ASSERT_EQ(input_invoice_message_content.get_description(), "sokkjht");

        input_invoice_message_content.set_is_flexible(false);
        ASSERT_EQ(input_invoice_message_content.get_is_flexible(), false);

        input_invoice_message_content.set_max_tip_amount(746184);
        ASSERT_EQ(input_invoice_message_content.get_max_tip_amount(), 746184);

        input_invoice_message_content.set_need_email(false);
        ASSERT_EQ(input_invoice_message_content.get_need_email(), false);

        input_invoice_message_content.set_need_name(false);
        ASSERT_EQ(input_invoice_message_content.get_need_name(), false);

        input_invoice_message_content.set_need_phone_number(true);
        ASSERT_EQ(input_invoice_message_content.get_need_phone_number(), true);

        input_invoice_message_content.set_need_shipping_address(false);
        ASSERT_EQ(input_invoice_message_content.get_need_shipping_address(), false);

        input_invoice_message_content.set_payload("wocnujht");
        ASSERT_EQ(input_invoice_message_content.get_payload(), "wocnujht");

        input_invoice_message_content.set_photo_height(513535);
        ASSERT_EQ(input_invoice_message_content.get_photo_height(), 513535);

        input_invoice_message_content.set_photo_size(6165);
        ASSERT_EQ(input_invoice_message_content.get_photo_size(), 6165);

        input_invoice_message_content.set_photo_url("oufhr");
        ASSERT_EQ(input_invoice_message_content.get_photo_url(), "oufhr");

        input_invoice_message_content.set_photo_width(339814);
        ASSERT_EQ(input_invoice_message_content.get_photo_width(), 339814);

        input_invoice_message_content.set_prices({std::make_shared<tgbot::LabeledPrice>()});
        ASSERT_EQ(input_invoice_message_content.get_prices().at(0)->serialise(), tgbot::LabeledPrice().serialise());

        input_invoice_message_content.set_provider_data("wpoimjnbwf");
        ASSERT_EQ(input_invoice_message_content.get_provider_data(), "wpoimjnbwf");

        input_invoice_message_content.set_provider_token("wpmböoiwjf");
        ASSERT_EQ(input_invoice_message_content.get_provider_token(), "wpmböoiwjf");

        input_invoice_message_content.set_send_email_to_provider(true);
        ASSERT_EQ(input_invoice_message_content.get_send_email_to_provider(), true);

        input_invoice_message_content.set_send_phone_number_to_provider(true);
        ASSERT_EQ(input_invoice_message_content.get_send_phone_number_to_provider(), true);

        input_invoice_message_content.set_suggested_tip_amounts(std::vector<std::int32_t> {2651});
        ASSERT_EQ(input_invoice_message_content.get_suggested_tip_amounts(), std::vector<std::int32_t> {2651});

        input_invoice_message_content.set_title("jmioflmbk");
        ASSERT_EQ(input_invoice_message_content.get_title(), "jmioflmbk");

        ASSERT_EQ(input_invoice_message_content.serialise(), serialised_doc);
    }
}

TEST(InputInvoiceMessageContent, errors)
{
    ASSERT_THROW(tgbot::InputInvoiceMessageContent(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 6; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["currency"] = "hüpok";
        }

        if(i != 1)
        {
            doc["description"] = "owihf";
        }

        if(i != 2)
        {
            doc["payload"] = "wuhfb";
        }

        if(i != 3)
        {
            doc["prices"] = {nlohmann::json::parse(tgbot::LabeledPrice().serialise())};
        }

        if(i != 4)
        {
            doc["provider_token"] = "prkjgh";
        }

        if(i != 5)
        {
            doc["title"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::InputInvoiceMessageContent(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 20; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["currency"] = "cöoqg" : doc["currency"] = 961;
        i != 1 ? doc["description"] = "owihf" : doc["description"] = 65;
        i != 2 ? doc["is_flexible"] = false : doc["is_flexible"] = 768187;
        i != 3 ? doc["max_tip_amount"] = 1678 : doc["max_tip_amount"] = "ciohh";
        i != 4 ? doc["need_email"] = false : doc["need_email"] = 4531;
        i != 5 ? doc["need_name"] = false : doc["need_name"] = 71654;
        i != 6 ? doc["need_phone_number"] = false : doc["need_phone_number"] = 71378;
        i != 7 ? doc["need_shipping_address"] = false : doc["need_shipping_address"] = 73157;
        i != 8 ? doc["payload"] = "cpoihq" : doc["payload"] = 674;
        i != 9 ? doc["photo_height"] = 4561 : doc["photo_height"] = "sdfhsdrg";
        i != 10 ? doc["photo_size"] = 4561 : doc["photo_size"] = "sdfhsdrg";
        i != 11 ? doc["photo_url"] = "woöinefmoib" : doc["photo_url"] = 616842;
        i != 12 ? doc["photo_width"] = 4561 : doc["photo_width"] = "sdfhsdrg";
        i != 13 ? doc["prices"] = {nlohmann::json::parse(tgbot::LabeledPrice().serialise())} : doc["prices"] = 468;
        i != 14 ? doc["provider_data"] = "wuefhbnjdfb" : doc["provider_data"] = 651;
        i != 15 ? doc["provider_token"] = "coiqg" : doc["provider_token"] = 936;
        i != 16 ? doc["send_email_to_provider"] = false : doc["send_email_to_provider"] = 76871;
        i != 17 ? doc["send_phone_number_to_provider"] = false : doc["send_phone_number_to_provider"] = 71871;
        i != 18 ? doc["suggested_tip_amounts"] = {2651} : doc["suggested_tip_amounts"] = 74;
        i != 19 ? doc["title"] = "aoisjgldfgs" : doc["title"] = 286;

        ASSERT_THROW(tgbot::InputInvoiceMessageContent(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["currency"] = "qiuggf";
        doc["description"] = "owihf";
        doc["payload"] = "tijgh";

        if(i == 0)
        {
            doc["prices"] = {4786068516};
        }
        else
        {
            doc["prices"] = {nlohmann::json::parse(tgbot::LabeledPrice().serialise())};
        }

        doc["provider_token"] = "fiojt";

        if(i == 1)
        {
            doc["suggested_tip_amounts"] = {"cölskdfh"};
        }

        doc["title"] = "aoisjgldfgs";

        ASSERT_THROW(tgbot::InputInvoiceMessageContent(doc.dump()), std::invalid_argument);
    }
}
