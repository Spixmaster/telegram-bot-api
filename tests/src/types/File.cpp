#include "tgbot/types/File.h"

#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(File, normal)
{
    ASSERT_NO_THROW(tgbot::File(tgbot::File().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["file_id"] = "apomisdfj";
    doc["file_path"] = "hsidfjg";
    doc["file_size"] = 6841;
    doc["file_unique_id"] = "gnpoksmd";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::File(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::File file;

        file.set_file_id("apomisdfj");
        ASSERT_EQ(file.get_file_id(), "apomisdfj");

        file.set_file_path("hsidfjg");
        ASSERT_EQ(file.get_file_path(), "hsidfjg");

        file.set_file_size(6841);
        ASSERT_EQ(file.get_file_size(), 6841);

        file.set_file_unique_id("gnpoksmd");
        ASSERT_EQ(file.get_file_unique_id(), "gnpoksmd");

        ASSERT_EQ(file.serialise(), serialised_doc);
    }
}

TEST(File, errors)
{
    ASSERT_THROW(tgbot::File(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["file_id"] = "aoisjgldfgs";
        }

        if(i != 1)
        {
            doc["file_unique_id"] = "relxcanbf";
        }

        ASSERT_THROW(tgbot::File(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["file_id"] = "aoisjgldfgs" : doc["file_id"] = 38613;
        i != 1 ? doc["file_path"] = "wuvtn" : doc["file_path"] = 4168;
        i != 2 ? doc["file_size"] = 4561 : doc["file_size"] = "sdfhsdrg";
        i != 3 ? doc["file_unique_id"] = "rqposd" : doc["file_unique_id"] = 798135;

        ASSERT_THROW(tgbot::File(doc.dump()), std::invalid_argument);
    }
}
