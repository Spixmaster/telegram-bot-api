#include "tgbot/types/InputMediaVideo.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InputMediaVideo, normal)
{
    ASSERT_NO_THROW(tgbot::InputMediaVideo(tgbot::InputMediaVideo().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["caption"] = "cmionjs";
    doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())};
    doc["duration"] = -9148;
    doc["has_spoiler"] = false;
    doc["height"] = 6826825;
    doc["media"] = "xosdfg";
    doc["parse_mode"] = "ppimxsxfg";
    doc["supports_streaming"] = false;
    doc["thumbnail"] = "hoksjdf";
    doc["type"] = "poiuhsfd";
    doc["width"] = 16851;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InputMediaVideo(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InputMediaVideo input_media_video;

        input_media_video.set_caption("cmionjs");
        ASSERT_EQ(input_media_video.get_caption(), "cmionjs");

        input_media_video.set_caption_entities(std::vector<tgbot::MessageEntity::ptr> {
          std::make_shared<tgbot::MessageEntity>()});

        if(input_media_video.get_caption_entities().has_value())
        {
            ASSERT_EQ(
              input_media_video.get_caption_entities().value().at(0)->serialise(), tgbot::MessageEntity().serialise());
        }

        input_media_video.set_duration(-9148);
        ASSERT_EQ(input_media_video.get_duration(), -9148);

        input_media_video.set_has_spoiler(false);
        ASSERT_EQ(input_media_video.get_has_spoiler(), false);

        input_media_video.set_height(6826825);
        ASSERT_EQ(input_media_video.get_height(), 6826825);

        input_media_video.set_media("xosdfg");
        ASSERT_EQ(input_media_video.get_media(), "xosdfg");

        input_media_video.set_parse_mode("ppimxsxfg");
        ASSERT_EQ(input_media_video.get_parse_mode(), "ppimxsxfg");

        input_media_video.set_supports_streaming(false);
        ASSERT_EQ(input_media_video.get_supports_streaming(), false);

        input_media_video.set_thumbnail("hoksjdf");
        ASSERT_EQ(input_media_video.get_thumbnail(), "hoksjdf");

        input_media_video.set_type("poiuhsfd");
        ASSERT_EQ(input_media_video.get_type(), "poiuhsfd");

        input_media_video.set_width(16851);
        ASSERT_EQ(input_media_video.get_width(), 16851);

        ASSERT_EQ(input_media_video.serialise(), serialised_doc);
    }
}

TEST(InputMediaVideo, errors)
{
    ASSERT_THROW(tgbot::InputMediaVideo(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["media"] = "owihf";
        }

        if(i != 1)
        {
            doc["type"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::InputMediaVideo(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 11; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["caption"] = "djlhqg" : doc["caption"] = 4168;
        i != 1 ?
          doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())} :
          doc["caption_entities"] = 4168;
        i != 2 ? doc["duration"] = 984 : doc["duration"] = "copij";
        i != 3 ? doc["has_spoiler"] = false : doc["has_spoiler"] = 471384;
        i != 4 ? doc["height"] = 984 : doc["height"] = "copij";
        i != 5 ? doc["media"] = "owihf" : doc["media"] = 65;
        i != 6 ? doc["parse_mode"] = "djlhqg" : doc["parse_mode"] = 4168;
        i != 7 ? doc["supports_streaming"] = true : doc["supports_streaming"] = 76874;
        i != 8 ? doc["thumbnail"] = "opqgd" : doc["thumbnail"] = 486;
        i != 9 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;
        i != 10 ? doc["width"] = 984 : doc["width"] = "copij";

        ASSERT_THROW(tgbot::InputMediaVideo(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["caption_entities"] = {9841984};
        doc["media"] = "owihf";
        doc["type"] = "aoisjgldfgs";

        ASSERT_THROW(tgbot::InputMediaVideo(doc.dump()), std::invalid_argument);
    }
}
