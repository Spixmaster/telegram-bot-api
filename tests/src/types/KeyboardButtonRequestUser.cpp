#include "tgbot/types/KeyboardButtonRequestUser.h"

#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(KeyboardButtonRequestUser, normal)
{
    ASSERT_NO_THROW(tgbot::KeyboardButtonRequestUser(tgbot::KeyboardButtonRequestUser().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["request_id"] = 156987;
    doc["user_is_bot"] = false;
    doc["user_is_premium"] = true;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::KeyboardButtonRequestUser(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::KeyboardButtonRequestUser keyboard_button_request_user;

        keyboard_button_request_user.set_request_id(156987);
        ASSERT_EQ(keyboard_button_request_user.get_request_id(), 156987);

        keyboard_button_request_user.set_user_is_bot(false);
        ASSERT_EQ(keyboard_button_request_user.get_user_is_bot(), false);

        keyboard_button_request_user.set_user_is_premium(true);
        ASSERT_EQ(keyboard_button_request_user.get_user_is_premium(), true);

        ASSERT_EQ(keyboard_button_request_user.serialise(), serialised_doc);
    }
}

TEST(KeyboardButtonRequestUser, errors)
{
    ASSERT_THROW(tgbot::KeyboardButtonRequestUser(nlohmann::json::array().dump()), std::invalid_argument);

    {
        const nlohmann::json doc = nlohmann::json::object();

        ASSERT_THROW(tgbot::KeyboardButtonRequestUser(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["request_id"] = 168678 : doc["request_id"] = "epoiajsdh";
        i != 1 ? doc["user_is_bot"] = true : doc["user_is_bot"] = 971354;
        i != 2 ? doc["user_is_premium"] = false : doc["user_is_premium"] = 187;

        ASSERT_THROW(tgbot::KeyboardButtonRequestUser(doc.dump()), std::invalid_argument);
    }
}
