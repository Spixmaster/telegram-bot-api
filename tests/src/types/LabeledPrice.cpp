#include "tgbot/types/LabeledPrice.h"

#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(LabeledPrice, normal)
{
    ASSERT_NO_THROW(tgbot::LabeledPrice(tgbot::LabeledPrice().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["amount"] = -6841;
    doc["label"] = "höslkjdgf";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::LabeledPrice(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::LabeledPrice labeled_price;

        labeled_price.set_amount(-6841);
        ASSERT_EQ(labeled_price.get_amount(), -6841);

        labeled_price.set_label("höslkjdgf");
        ASSERT_EQ(labeled_price.get_label(), "höslkjdgf");

        ASSERT_EQ(labeled_price.serialise(), serialised_doc);
    }
}

TEST(LabeledPrice, errors)
{
    ASSERT_THROW(tgbot::LabeledPrice(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["amount"] = 984;
        }

        if(i != 1)
        {
            doc["label"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::LabeledPrice(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["amount"] = 7981684 : doc["amount"] = "bsefbsdfh";
        i != 1 ? doc["label"] = "aoisjgldfgs" : doc["label"] = 286;

        ASSERT_THROW(tgbot::LabeledPrice(doc.dump()), std::invalid_argument);
    }
}
