#include "tgbot/types/InlineQueryResultContact.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"
#include "tgbot/types/InlineKeyboardMarkup.h"
#include "tgbot/types/InputMessageContent.h"

TEST(InlineQueryResultContact, normal)
{
    ASSERT_NO_THROW(tgbot::InlineQueryResultContact(tgbot::InlineQueryResultContact().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["first_name"] = "sdfhpoimj";
    doc["id"] = "kljhkjh";
    doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise());
    doc["last_name"] = "erghpmpkscg";
    doc["phone_number"] = "tkjmionjc";
    doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise());
    doc["thumbnail_height"] = -687145;
    doc["thumbnail_url"] = "erwhpomic";
    doc["thumbnail_width"] = 681544;
    doc["type"] = "hwoijmnc";
    doc["vcard"] = "trhpomjnc";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InlineQueryResultContact(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InlineQueryResultContact inline_query_result_contact;

        inline_query_result_contact.set_first_name("sdfhpoimj");
        ASSERT_EQ(inline_query_result_contact.get_first_name(), "sdfhpoimj");

        inline_query_result_contact.set_id("kljhkjh");
        ASSERT_EQ(inline_query_result_contact.get_id(), "kljhkjh");

        inline_query_result_contact.set_input_message_content(std::make_shared<tgbot::InputMessageContent>());

        if(inline_query_result_contact.get_input_message_content().has_value())
        {
            ASSERT_EQ(inline_query_result_contact.get_input_message_content().value()->serialise(),
                      tgbot::InputMessageContent().serialise());
        }

        inline_query_result_contact.set_last_name("erghpmpkscg");
        ASSERT_EQ(inline_query_result_contact.get_last_name(), "erghpmpkscg");

        inline_query_result_contact.set_phone_number("tkjmionjc");
        ASSERT_EQ(inline_query_result_contact.get_phone_number(), "tkjmionjc");

        inline_query_result_contact.set_reply_markup(std::make_shared<tgbot::InlineKeyboardMarkup>());

        if(inline_query_result_contact.get_reply_markup().has_value())
        {
            ASSERT_EQ(inline_query_result_contact.get_reply_markup().value()->serialise(),
                      tgbot::InlineKeyboardMarkup().serialise());
        }

        inline_query_result_contact.set_thumbnail_height(-687145);
        ASSERT_EQ(inline_query_result_contact.get_thumbnail_height(), -687145);

        inline_query_result_contact.set_thumbnail_url("erwhpomic");
        ASSERT_EQ(inline_query_result_contact.get_thumbnail_url(), "erwhpomic");

        inline_query_result_contact.set_thumbnail_width(681544);
        ASSERT_EQ(inline_query_result_contact.get_thumbnail_width(), 681544);

        inline_query_result_contact.set_type("hwoijmnc");
        ASSERT_EQ(inline_query_result_contact.get_type(), "hwoijmnc");

        inline_query_result_contact.set_vcard("trhpomjnc");
        ASSERT_EQ(inline_query_result_contact.get_vcard(), "trhpomjnc");

        ASSERT_EQ(inline_query_result_contact.serialise(), serialised_doc);
    }
}

TEST(InlineQueryResultContact, errors)
{
    ASSERT_THROW(tgbot::InlineQueryResultContact(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["first_name"] = "diopqb";
        }

        if(i != 1)
        {
            doc["id"] = "owihf";
        }

        if(i != 2)
        {
            doc["phone_number"] = "icqun";
        }

        if(i != 3)
        {
            doc["type"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::InlineQueryResultContact(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 11; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["first_name"] = "cxpjih" : doc["first_name"] = 739;
        i != 1 ? doc["id"] = "owihf" : doc["id"] = 65;
        i != 2 ?
          doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise()) :
          doc["input_message_content"] = 49;
        i != 3 ? doc["last_name"] = "djlhqg" : doc["last_name"] = 4168;
        i != 4 ? doc["phone_number"] = "opqgd" : doc["phone_number"] = 486;
        i != 5 ?
          doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise()) :
          doc["reply_markup"] = 49;
        i != 6 ? doc["thumbnail_height"] = 48 : doc["thumbnail_height"] = "kja";
        i != 7 ? doc["thumbnail_url"] = "cxpjih" : doc["thumbnail_url"] = 739;
        i != 8 ? doc["thumbnail_width"] = 82 : doc["thumbnail_width"] = "guih";
        i != 9 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;
        i != 10 ? doc["vcard"] = "djlhqg" : doc["vcard"] = 4168;

        ASSERT_THROW(tgbot::InlineQueryResultContact(doc.dump()), std::invalid_argument);
    }
}
