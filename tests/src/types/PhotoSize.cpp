#include "tgbot/types/PhotoSize.h"

#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(PhotoSize, normal)
{
    ASSERT_NO_THROW(tgbot::PhotoSize(tgbot::PhotoSize().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["file_id"] = "regnsfd";
    doc["file_size"] = -9638;
    doc["file_unique_id"] = "erzjpsd";
    doc["height"] = 18648215;
    doc["width"] = 8614628;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::PhotoSize(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::PhotoSize photo_size;

        photo_size.set_file_id("regnsfd");
        ASSERT_EQ(photo_size.get_file_id(), "regnsfd");

        photo_size.set_file_size(-9638);
        ASSERT_EQ(photo_size.get_file_size(), -9638);

        photo_size.set_file_unique_id("erzjpsd");
        ASSERT_EQ(photo_size.get_file_unique_id(), "erzjpsd");

        photo_size.set_height(18648215);
        ASSERT_EQ(photo_size.get_height(), 18648215);

        photo_size.set_width(8614628);
        ASSERT_EQ(photo_size.get_width(), 8614628);

        ASSERT_EQ(photo_size.serialise(), serialised_doc);
    }
}

TEST(PhotoSize, errors)
{
    ASSERT_THROW(tgbot::PhotoSize(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["file_id"] = "aoisjgldfgs";
        }

        if(i != 1)
        {
            doc["file_unique_id"] = "owihf";
        }

        if(i != 2)
        {
            doc["height"] = 87;
        }

        if(i != 3)
        {
            doc["width"] = 6844;
        }

        ASSERT_THROW(tgbot::PhotoSize(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 5; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["file_id"] = "aoisjgldfgs" : doc["file_id"] = 286;
        i != 1 ? doc["file_size"] = 7981684 : doc["file_size"] = "bsefbsdfh";
        i != 2 ? doc["file_unique_id"] = "owihf" : doc["file_unique_id"] = 65;
        i != 3 ? doc["height"] = 87 : doc["height"] = "herthsdf";
        i != 4 ? doc["width"] = 6844 : doc["width"] = "sdfg";

        ASSERT_THROW(tgbot::PhotoSize(doc.dump()), std::invalid_argument);
    }
}
