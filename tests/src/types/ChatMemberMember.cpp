#include "tgbot/types/ChatMemberMember.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(ChatMemberMember, normal)
{
    ASSERT_NO_THROW(tgbot::ChatMemberMember(tgbot::ChatMemberMember().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["status"] = "cmpcwhrh";
    doc["user"] = nlohmann::json::parse(tgbot::User().serialise());

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ChatMemberMember(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::ChatMemberMember chat_member_member;

        chat_member_member.set_status("cmpcwhrh");
        ASSERT_EQ(chat_member_member.get_status(), "cmpcwhrh");

        chat_member_member.set_user(std::make_shared<tgbot::User>());
        ASSERT_EQ(chat_member_member.get_user()->serialise(), tgbot::User().serialise());

        ASSERT_EQ(chat_member_member.serialise(), serialised_doc);
    }
}

TEST(ChatMemberMember, errors)
{
    ASSERT_THROW(tgbot::ChatMemberMember(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["status"] = "owihf";
        }

        if(i != 1)
        {
            doc["user"] = nlohmann::json::parse(tgbot::User().serialise());
        }

        ASSERT_THROW(tgbot::ChatMemberMember(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["status"] = "owihf" : doc["status"] = 65;
        i != 1 ? doc["user"] = nlohmann::json::parse(tgbot::User().serialise()) : doc["user"] = 79165;

        ASSERT_THROW(tgbot::ChatMemberMember(doc.dump()), std::invalid_argument);
    }
}
