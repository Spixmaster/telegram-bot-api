#include "tgbot/types/PassportFile.h"

#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(PassportFile, normal)
{
    ASSERT_NO_THROW(tgbot::PassportFile(tgbot::PassportFile().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["file_date"] = 681852;
    doc["file_id"] = "roijsdfg";
    doc["file_size"] = -2182;
    doc["file_unique_id"] = "üosijdfx";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::PassportFile(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::PassportFile passport_file;

        passport_file.set_file_date(681852);
        ASSERT_EQ(passport_file.get_file_date(), 681852);

        passport_file.set_file_id("roijsdfg");
        ASSERT_EQ(passport_file.get_file_id(), "roijsdfg");

        passport_file.set_file_size(-2182);
        ASSERT_EQ(passport_file.get_file_size(), -2182);

        passport_file.set_file_unique_id("üosijdfx");
        ASSERT_EQ(passport_file.get_file_unique_id(), "üosijdfx");

        ASSERT_EQ(passport_file.serialise(), serialised_doc);
    }
}

TEST(PassportFile, errors)
{
    ASSERT_THROW(tgbot::PassportFile(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["file_date"] = 87;
        }

        if(i != 1)
        {
            doc["file_id"] = "aoisjgldfgs";
        }

        if(i != 2)
        {
            doc["file_size"] = 6844;
        }

        if(i != 3)
        {
            doc["file_unique_id"] = "owihf";
        }

        ASSERT_THROW(tgbot::PassportFile(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["file_date"] = 87 : doc["file_date"] = "herthsdf";
        i != 1 ? doc["file_id"] = "aoisjgldfgs" : doc["file_id"] = 286;
        i != 2 ? doc["file_size"] = 6844 : doc["file_size"] = "sdfg";
        i != 3 ? doc["file_unique_id"] = "owihf" : doc["file_unique_id"] = 65;

        ASSERT_THROW(tgbot::PassportFile(doc.dump()), std::invalid_argument);
    }
}
