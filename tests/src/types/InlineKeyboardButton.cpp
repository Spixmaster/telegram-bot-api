#include "tgbot/types/InlineKeyboardButton.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InlineKeyboardButton, normal)
{
    ASSERT_NO_THROW(tgbot::InlineKeyboardButton(tgbot::InlineKeyboardButton().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["callback_data"] = "wopiujbf";
    doc["callback_game"] = nlohmann::json::parse(tgbot::CallbackGame().serialise());
    doc["login_url"] = nlohmann::json::parse(tgbot::LoginUrl().serialise());
    doc["pay"] = false;
    doc["switch_inline_query"] = "sdfboijmr";
    doc["switch_inline_query_current_chat"] = "fpowunfb";
    doc["text"] = "cousndfh";
    doc["url"] = "cbosimdfh";
    doc["web_app"] = nlohmann::json::parse(tgbot::WebAppInfo().serialise());

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InlineKeyboardButton(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InlineKeyboardButton inline_keyboard_button;

        inline_keyboard_button.set_callback_data("wopiujbf");
        ASSERT_EQ(inline_keyboard_button.get_callback_data(), "wopiujbf");

        inline_keyboard_button.set_callback_game(std::make_shared<tgbot::CallbackGame>());

        if(inline_keyboard_button.get_callback_game().has_value())
        {
            ASSERT_EQ(
              inline_keyboard_button.get_callback_game().value()->serialise(), tgbot::CallbackGame().serialise());
        }

        inline_keyboard_button.set_login_url(std::make_shared<tgbot::LoginUrl>());

        if(inline_keyboard_button.get_login_url().has_value())
        {
            ASSERT_EQ(inline_keyboard_button.get_login_url().value()->serialise(), tgbot::LoginUrl().serialise());
        }

        inline_keyboard_button.set_pay(false);
        ASSERT_EQ(inline_keyboard_button.get_pay(), false);

        inline_keyboard_button.set_switch_inline_query("sdfboijmr");
        ASSERT_EQ(inline_keyboard_button.get_switch_inline_query(), "sdfboijmr");

        inline_keyboard_button.set_switch_inline_query_current_chat("fpowunfb");
        ASSERT_EQ(inline_keyboard_button.get_switch_inline_query_current_chat(), "fpowunfb");

        inline_keyboard_button.set_text("cousndfh");
        ASSERT_EQ(inline_keyboard_button.get_text(), "cousndfh");

        inline_keyboard_button.set_url("cbosimdfh");
        ASSERT_EQ(inline_keyboard_button.get_url(), "cbosimdfh");

        inline_keyboard_button.set_web_app(std::make_shared<tgbot::WebAppInfo>());

        if(inline_keyboard_button.get_web_app().has_value())
        {
            ASSERT_EQ(inline_keyboard_button.get_web_app().value()->serialise(), tgbot::WebAppInfo().serialise());
        }

        ASSERT_EQ(inline_keyboard_button.serialise(), serialised_doc);
    }
}

TEST(InlineKeyboardButton, errors)
{
    ASSERT_THROW(tgbot::InlineKeyboardButton(nlohmann::json::array().dump()), std::invalid_argument);

    {
        const nlohmann::json doc = nlohmann::json::object();

        ASSERT_THROW(tgbot::InlineKeyboardButton(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 9; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["callback_data"] = "üüomn" : doc["callback_data"] = 841;
        i != 1 ?
          doc["callback_game"] = nlohmann::json::parse(tgbot::CallbackGame().serialise()) :
          doc["callback_game"] = 6547;
        i != 2 ? doc["login_url"] = nlohmann::json::parse(tgbot::LoginUrl().serialise()) : doc["login_url"] = 6547;
        i != 3 ? doc["pay"] = false : doc["pay"] = 735467;
        i != 4 ? doc["switch_inline_query"] = "jcökb" : doc["switch_inline_query"] = 8495;
        i != 5 ? doc["switch_inline_query_current_chat"] = "jcökb" : doc["switch_inline_query_current_chat"] = 8495;
        i != 6 ? doc["text"] = "aoisjgldfgs" : doc["text"] = 286;
        i != 7 ? doc["url"] = "owihf" : doc["url"] = 71357;
        i != 8 ? doc["web_app"] = nlohmann::json::parse(tgbot::WebAppInfo().serialise()) : doc["web_app"] = 51687321;

        ASSERT_THROW(tgbot::InlineKeyboardButton(doc.dump()), std::invalid_argument);
    }
}
