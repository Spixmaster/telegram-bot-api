#include "tgbot/types/Audio.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(Audio, normal)
{
    ASSERT_NO_THROW(tgbot::Audio(tgbot::Audio().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["duration"] = 86298971;
    doc["file_id"] = "äsvknjmscv";
    doc["file_name"] = "sbviodjb";
    doc["file_size"] = -62418;
    doc["file_unique_id"] = "svdlkmb";
    doc["mime_type"] = "vbpimsfb";
    doc["performer"] = "snokjmsc";
    doc["thumbnail"] = nlohmann::json::parse(tgbot::PhotoSize().serialise());
    doc["title"] = "südovibjsi";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::Audio(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::Audio audio;

        audio.set_duration(86298971);
        ASSERT_EQ(audio.get_duration(), 86298971);

        audio.set_file_id("äsvknjmscv");
        ASSERT_EQ(audio.get_file_id(), "äsvknjmscv");

        audio.set_file_name("sbviodjb");
        ASSERT_EQ(audio.get_file_name(), "sbviodjb");

        audio.set_file_size(-62418);
        ASSERT_EQ(audio.get_file_size(), -62418);

        audio.set_file_unique_id("svdlkmb");
        ASSERT_EQ(audio.get_file_unique_id(), "svdlkmb");

        audio.set_mime_type("vbpimsfb");
        ASSERT_EQ(audio.get_mime_type(), "vbpimsfb");

        audio.set_performer("snokjmsc");
        ASSERT_EQ(audio.get_performer(), "snokjmsc");

        audio.set_thumbnail(std::make_shared<tgbot::PhotoSize>());

        if(audio.get_thumbnail().has_value())
        {
            ASSERT_EQ(audio.get_thumbnail().value()->serialise(), tgbot::PhotoSize().serialise());
        }

        audio.set_title("südovibjsi");
        ASSERT_EQ(audio.get_title(), "südovibjsi");

        ASSERT_EQ(audio.serialise(), serialised_doc);
    }
}

TEST(Audio, errors)
{
    ASSERT_THROW(tgbot::Audio(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["duration"] = 8156;
        }

        if(i != 1)
        {
            doc["file_id"] = "qoiubb";
        }

        if(i != 2)
        {
            doc["file_unique_id"] = "üijch";
        }

        ASSERT_THROW(tgbot::Audio(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 9; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["duration"] = 76 : doc["duration"] = "srjsdbtr";
        i != 1 ? doc["file_id"] = "cpouih" : doc["file_id"] = 1468;
        i != 2 ? doc["file_name"] = "cwrhrg" : doc["file_name"] = 6868258;
        i != 3 ? doc["file_size"] = 1684 : doc["file_size"] = "öoijnsdf";
        i != 4 ? doc["file_unique_id"] = "wfd" : doc["file_unique_id"] = 3818;
        i != 5 ? doc["mime_type"] = "csrhrg" : doc["mime_type"] = 825184;
        i != 6 ? doc["performer"] = "hwfv" : doc["performer"] = 862845;
        i != 7 ? doc["thumbnail"] = nlohmann::json::parse(tgbot::PhotoSize().serialise()) : doc["thumbnail"] = 928;
        i != 8 ? doc["title"] = "whoijf" : doc["title"] = 62518;

        ASSERT_THROW(tgbot::Audio(doc.dump()), std::invalid_argument);
    }
}
