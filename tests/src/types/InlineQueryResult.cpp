#include "tgbot/types/InlineQueryResult.h"

#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(InlineQueryResult, normal)
{
    ASSERT_NO_THROW(tgbot::InlineQueryResult(tgbot::InlineQueryResult().serialise()));

    const nlohmann::json doc = nlohmann::json::object();

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InlineQueryResult().serialise(), serialised_doc);

    {
        const tgbot::InlineQueryResult inline_query_result;

        ASSERT_EQ(inline_query_result.serialise(), serialised_doc);
    }
}

TEST(InlineQueryResult, errors)
{
    ASSERT_THROW(tgbot::InlineQueryResult(nlohmann::json::array().dump()), std::invalid_argument);
}
