#include "tgbot/types/Message.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(Message, normal)
{
    ASSERT_NO_THROW(tgbot::Message(tgbot::Message().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["animation"] = nlohmann::json::parse(tgbot::Animation().serialise());
    doc["audio"] = nlohmann::json::parse(tgbot::Audio().serialise());
    doc["author_signature"] = "rhoisnfdg";
    doc["caption"] = "etblkjnasfg";
    doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())};
    doc["channel_chat_created"] = true;
    doc["chat"] = nlohmann::json::parse(tgbot::Chat().serialise());
    doc["chat_shared"] = nlohmann::json::parse(tgbot::ChatShared().serialise());
    doc["connected_website"] = "bosihdfg";
    doc["contact"] = nlohmann::json::parse(tgbot::Contact().serialise());
    doc["date"] = 78218164;
    doc["delete_chat_photo"] = false;
    doc["dice"] = nlohmann::json::parse(tgbot::Dice().serialise());
    doc["document"] = nlohmann::json::parse(tgbot::Document().serialise());
    doc["edit_date"] = 65186252;
    doc["entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())};
    doc["forum_topic_closed"] = nlohmann::json::parse(tgbot::ForumTopicClosed().serialise());
    doc["forum_topic_created"] = nlohmann::json::parse(tgbot::ForumTopicCreated().serialise());
    doc["forum_topic_edited"] = nlohmann::json::parse(tgbot::ForumTopicEdited().serialise());
    doc["forum_topic_reopened"] = nlohmann::json::parse(tgbot::ForumTopicReopened().serialise());
    doc["forward_date"] = 68855;
    doc["forward_from"] = nlohmann::json::parse(tgbot::User().serialise());
    doc["forward_from_chat"] = nlohmann::json::parse(tgbot::Chat().serialise());
    doc["forward_from_message_id"] = -685;
    doc["forward_sender_name"] = "erhpisjdfg";
    doc["forward_signature"] = "hoisjndf";
    doc["from"] = nlohmann::json::parse(tgbot::User().serialise());
    doc["game"] = nlohmann::json::parse(tgbot::Game().serialise());
    doc["general_forum_topic_hidden"] = nlohmann::json::parse(tgbot::GeneralForumTopicHidden().serialise());
    doc["general_forum_topic_unhidden"] = nlohmann::json::parse(tgbot::GeneralForumTopicUnhidden().serialise());
    doc["group_chat_created"] = true;
    doc["has_media_spoiler"] = false;
    doc["has_protected_content"] = false;
    doc["invoice"] = nlohmann::json::parse(tgbot::Invoice().serialise());
    doc["is_automatic_forward"] = true;
    doc["is_topic_message"] = true;
    doc["left_chat_member"] = nlohmann::json::parse(tgbot::User().serialise());
    doc["location"] = nlohmann::json::parse(tgbot::Location().serialise());
    doc["media_group_id"] = "rtöhlksndf";
    doc["message_auto_delete_timer_changed"] = nlohmann::json::parse(tgbot::MessageAutoDeleteTimerChanged().serialise());
    doc["message_id"] = 65268518;
    doc["message_thread_id"] = 71674;
    doc["migrate_from_chat_id"] = 686828;
    doc["migrate_to_chat_id"] = 8482568;
    doc["new_chat_members"] = {nlohmann::json::parse(tgbot::User().serialise())};
    doc["new_chat_photo"] = {nlohmann::json::parse(tgbot::PhotoSize().serialise())};
    doc["new_chat_title"] = "hlskjdfokger";
    doc["passport_data"] = nlohmann::json::parse(tgbot::PassportData().serialise());
    doc["photo"] = {nlohmann::json::parse(tgbot::PhotoSize().serialise())};
    doc["pinned_message"] = nlohmann::json::parse(tgbot::Message().serialise());
    doc["poll"] = nlohmann::json::parse(tgbot::Poll().serialise());
    doc["proximity_alert_triggered"] = nlohmann::json::parse(tgbot::ProximityAlertTriggered().serialise());
    doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise());
    doc["reply_to_message"] = nlohmann::json::parse(tgbot::Message().serialise());
    doc["sender_chat"] = nlohmann::json::parse(tgbot::Chat().serialise());
    doc["sticker"] = nlohmann::json::parse(tgbot::Sticker().serialise());
    doc["successful_payment"] = nlohmann::json::parse(tgbot::SuccessfulPayment().serialise());
    doc["supergroup_chat_created"] = false;
    doc["text"] = "erhoisndfg";
    doc["user_shared"] = nlohmann::json::parse(tgbot::UserShared().serialise());
    doc["venue"] = nlohmann::json::parse(tgbot::Venue().serialise());
    doc["via_bot"] = nlohmann::json::parse(tgbot::User().serialise());
    doc["video"] = nlohmann::json::parse(tgbot::Video().serialise());
    doc["video_chat_ended"] = nlohmann::json::parse(tgbot::VideoChatEnded().serialise());
    doc["video_chat_participants_invited"] = nlohmann::json::parse(tgbot::VideoChatParticipantsInvited().serialise());
    doc["video_chat_scheduled"] = nlohmann::json::parse(tgbot::VideoChatScheduled().serialise());
    doc["video_chat_started"] = nlohmann::json::parse(tgbot::VideoChatStarted().serialise());
    doc["video_note"] = nlohmann::json::parse(tgbot::VideoNote().serialise());
    doc["voice"] = nlohmann::json::parse(tgbot::Voice().serialise());
    doc["web_app_data"] = nlohmann::json::parse(tgbot::WebAppData().serialise());
    doc["write_access_allowed"] = nlohmann::json::parse(tgbot::WriteAccessAllowed().serialise());

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::Message(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::Message message;

        message.set_animation(std::make_shared<tgbot::Animation>());

        if(message.get_animation().has_value())
        {
            ASSERT_EQ(message.get_animation().value()->serialise(), tgbot::Animation().serialise());
        }

        message.set_audio(std::make_shared<tgbot::Audio>());

        if(message.get_audio().has_value())
        {
            ASSERT_EQ(message.get_audio().value()->serialise(), tgbot::Audio().serialise());
        }

        message.set_author_signature("rhoisnfdg");
        ASSERT_EQ(message.get_author_signature(), "rhoisnfdg");

        message.set_caption("etblkjnasfg");
        ASSERT_EQ(message.get_caption(), "etblkjnasfg");

        message.set_caption_entities(std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()});

        if(message.get_caption_entities().has_value())
        {
            ASSERT_EQ(message.get_caption_entities().value().at(0)->serialise(), tgbot::MessageEntity().serialise());
        }

        message.set_channel_chat_created(true);
        ASSERT_EQ(message.get_channel_chat_created(), true);

        message.set_chat(std::make_shared<tgbot::Chat>());
        ASSERT_EQ(message.get_chat()->serialise(), tgbot::Chat().serialise());

        message.set_chat_shared(std::make_shared<tgbot::ChatShared>());

        if(message.get_chat_shared().has_value())
        {
            ASSERT_EQ(message.get_chat_shared().value()->serialise(), tgbot::ChatShared().serialise());
        }

        message.set_connected_website("bosihdfg");
        ASSERT_EQ(message.get_connected_website(), "bosihdfg");

        message.set_contact(std::make_shared<tgbot::Contact>());

        if(message.get_contact().has_value())
        {
            ASSERT_EQ(message.get_contact().value()->serialise(), tgbot::Contact().serialise());
        }

        message.set_date(78218164);
        ASSERT_EQ(message.get_date(), 78218164);

        message.set_delete_chat_photo(false);
        ASSERT_EQ(message.get_delete_chat_photo(), false);

        message.set_dice(std::make_shared<tgbot::Dice>());

        if(message.get_dice().has_value())
        {
            ASSERT_EQ(message.get_dice().value()->serialise(), tgbot::Dice().serialise());
        }

        message.set_document(std::make_shared<tgbot::Document>());

        if(message.get_document().has_value())
        {
            ASSERT_EQ(message.get_document().value()->serialise(), tgbot::Document().serialise());
        }

        message.set_edit_date(65186252);
        ASSERT_EQ(message.get_edit_date(), 65186252);

        message.set_entities(std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()});

        if(message.get_entities().has_value())
        {
            ASSERT_EQ(message.get_entities().value().at(0)->serialise(), tgbot::MessageEntity().serialise());
        }

        message.set_forum_topic_closed(std::make_shared<tgbot::ForumTopicClosed>());

        if(message.get_forum_topic_closed().has_value())
        {
            ASSERT_EQ(message.get_forum_topic_closed().value()->serialise(), tgbot::ForumTopicClosed().serialise());
        }

        message.set_forum_topic_created(std::make_shared<tgbot::ForumTopicCreated>());

        if(message.get_forum_topic_created().has_value())
        {
            ASSERT_EQ(message.get_forum_topic_created().value()->serialise(), tgbot::ForumTopicCreated().serialise());
        }

        message.set_forum_topic_edited(std::make_shared<tgbot::ForumTopicEdited>());

        if(message.get_forum_topic_edited().has_value())
        {
            ASSERT_EQ(message.get_forum_topic_edited().value()->serialise(), tgbot::ForumTopicEdited().serialise());
        }

        message.set_forum_topic_reopened(std::make_shared<tgbot::ForumTopicReopened>());

        if(message.get_forum_topic_reopened().has_value())
        {
            ASSERT_EQ(message.get_forum_topic_reopened().value()->serialise(), tgbot::ForumTopicReopened().serialise());
        }

        message.set_forward_date(68855);
        ASSERT_EQ(message.get_forward_date(), 68855);

        message.set_forward_from(std::make_shared<tgbot::User>());

        if(message.get_forward_from().has_value())
        {
            ASSERT_EQ(message.get_forward_from().value()->serialise(), tgbot::User().serialise());
        }

        message.set_forward_from_chat(std::make_shared<tgbot::Chat>());

        if(message.get_forward_from_chat().has_value())
        {
            ASSERT_EQ(message.get_forward_from_chat().value()->serialise(), tgbot::Chat().serialise());
        }

        message.set_forward_from_message_id(-685);
        ASSERT_EQ(message.get_forward_from_message_id(), -685);

        message.set_forward_sender_name("erhpisjdfg");
        ASSERT_EQ(message.get_forward_sender_name(), "erhpisjdfg");

        message.set_forward_signature("hoisjndf");
        ASSERT_EQ(message.get_forward_signature(), "hoisjndf");

        message.set_from(std::make_shared<tgbot::User>());

        if(message.get_from().has_value())
        {
            ASSERT_EQ(message.get_from().value()->serialise(), tgbot::User().serialise());
        }

        message.set_game(std::make_shared<tgbot::Game>());

        if(message.get_game().has_value())
        {
            ASSERT_EQ(message.get_game().value()->serialise(), tgbot::Game().serialise());
        }

        message.set_general_forum_topic_hidden(std::make_shared<tgbot::GeneralForumTopicHidden>());

        if(message.get_general_forum_topic_hidden().has_value())
        {
            ASSERT_EQ(message.get_general_forum_topic_hidden().value()->serialise(),
                      tgbot::GeneralForumTopicHidden().serialise());
        }

        message.set_general_forum_topic_unhidden(std::make_shared<tgbot::GeneralForumTopicUnhidden>());

        if(message.get_general_forum_topic_unhidden().has_value())
        {
            ASSERT_EQ(message.get_general_forum_topic_unhidden().value()->serialise(),
                      tgbot::GeneralForumTopicUnhidden().serialise());
        }

        message.set_group_chat_created(true);
        ASSERT_EQ(message.get_group_chat_created(), true);

        message.set_has_media_spoiler(false);
        ASSERT_EQ(message.get_has_media_spoiler(), false);

        message.set_has_protected_content(false);
        ASSERT_EQ(message.get_has_protected_content(), false);

        message.set_invoice(std::make_shared<tgbot::Invoice>());

        if(message.get_invoice().has_value())
        {
            ASSERT_EQ(message.get_invoice().value()->serialise(), tgbot::Invoice().serialise());
        }

        message.set_is_automatic_forward(true);
        ASSERT_EQ(message.get_is_automatic_forward(), true);

        message.set_is_topic_message(true);
        ASSERT_EQ(message.get_is_topic_message(), true);

        message.set_left_chat_member(std::make_shared<tgbot::User>());

        if(message.get_left_chat_member().has_value())
        {
            ASSERT_EQ(message.get_left_chat_member().value()->serialise(), tgbot::User().serialise());
        }

        message.set_location(std::make_shared<tgbot::Location>());

        if(message.get_location().has_value())
        {
            ASSERT_EQ(message.get_location().value()->serialise(), tgbot::Location().serialise());
        }

        message.set_media_group_id("rtöhlksndf");
        ASSERT_EQ(message.get_media_group_id(), "rtöhlksndf");

        message.set_message_auto_delete_timer_changed(std::make_shared<tgbot::MessageAutoDeleteTimerChanged>());

        if(message.get_message_auto_delete_timer_changed().has_value())
        {
            ASSERT_EQ(message.get_message_auto_delete_timer_changed().value()->serialise(),
                      tgbot::MessageAutoDeleteTimerChanged().serialise());
        }

        message.set_message_id(65268518);
        ASSERT_EQ(message.get_message_id(), 65268518);

        message.set_message_thread_id(71674);
        ASSERT_EQ(message.get_message_thread_id(), 71674);

        message.set_migrate_from_chat_id(686828);
        ASSERT_EQ(message.get_migrate_from_chat_id(), 686828);

        message.set_migrate_to_chat_id(8482568);
        ASSERT_EQ(message.get_migrate_to_chat_id(), 8482568);

        message.set_new_chat_members(std::vector<tgbot::User::ptr> {std::make_shared<tgbot::User>()});

        if(message.get_new_chat_members().has_value())
        {
            ASSERT_EQ(message.get_new_chat_members().value().at(0)->serialise(), tgbot::User().serialise());
        }

        message.set_new_chat_photo(std::vector<tgbot::PhotoSize::ptr> {std::make_shared<tgbot::PhotoSize>()});

        if(message.get_new_chat_photo().has_value())
        {
            ASSERT_EQ(message.get_new_chat_photo().value().at(0)->serialise(), tgbot::PhotoSize().serialise());
        }

        message.set_new_chat_title("hlskjdfokger");
        ASSERT_EQ(message.get_new_chat_title(), "hlskjdfokger");

        message.set_passport_data(std::make_shared<tgbot::PassportData>());

        if(message.get_passport_data().has_value())
        {
            ASSERT_EQ(message.get_passport_data().value()->serialise(), tgbot::PassportData().serialise());
        }

        message.set_photo(std::vector<tgbot::PhotoSize::ptr> {std::make_shared<tgbot::PhotoSize>()});

        if(message.get_photo().has_value())
        {
            ASSERT_EQ(message.get_photo().value().at(0)->serialise(), tgbot::PhotoSize().serialise());
        }

        message.set_pinned_message(std::make_shared<tgbot::Message>());

        if(message.get_pinned_message().has_value())
        {
            ASSERT_EQ(message.get_pinned_message().value()->serialise(), tgbot::Message().serialise());
        }

        message.set_poll(std::make_shared<tgbot::Poll>());

        if(message.get_poll().has_value())
        {
            ASSERT_EQ(message.get_poll().value()->serialise(), tgbot::Poll().serialise());
        }

        message.set_proximity_alert_triggered(std::make_shared<tgbot::ProximityAlertTriggered>());

        if(message.get_proximity_alert_triggered().has_value())
        {
            ASSERT_EQ(message.get_proximity_alert_triggered().value()->serialise(),
                      tgbot::ProximityAlertTriggered().serialise());
        }

        message.set_reply_markup(std::make_shared<tgbot::InlineKeyboardMarkup>());

        if(message.get_reply_markup().has_value())
        {
            ASSERT_EQ(message.get_reply_markup().value()->serialise(), tgbot::InlineKeyboardMarkup().serialise());
        }

        message.set_reply_to_message(std::make_shared<tgbot::Message>());

        if(message.get_reply_to_message().has_value())
        {
            ASSERT_EQ(message.get_reply_to_message().value()->serialise(), tgbot::Message().serialise());
        }

        message.set_sender_chat(std::make_shared<tgbot::Chat>());

        if(message.get_sender_chat().has_value())
        {
            ASSERT_EQ(message.get_sender_chat().value()->serialise(), tgbot::Chat().serialise());
        }

        message.set_sticker(std::make_shared<tgbot::Sticker>());

        if(message.get_sticker().has_value())
        {
            ASSERT_EQ(message.get_sticker().value()->serialise(), tgbot::Sticker().serialise());
        }

        message.set_successful_payment(std::make_shared<tgbot::SuccessfulPayment>());

        if(message.get_successful_payment().has_value())
        {
            ASSERT_EQ(message.get_successful_payment().value()->serialise(), tgbot::SuccessfulPayment().serialise());
        }

        message.set_supergroup_chat_created(false);
        ASSERT_EQ(message.get_supergroup_chat_created(), false);

        message.set_text("erhoisndfg");
        ASSERT_EQ(message.get_text(), "erhoisndfg");

        message.set_user_shared(std::make_shared<tgbot::UserShared>());

        if(message.get_user_shared().has_value())
        {
            ASSERT_EQ(message.get_user_shared().value()->serialise(), tgbot::UserShared().serialise());
        }

        message.set_venue(std::make_shared<tgbot::Venue>());

        if(message.get_venue().has_value())
        {
            ASSERT_EQ(message.get_venue().value()->serialise(), tgbot::Venue().serialise());
        }

        message.set_via_bot(std::make_shared<tgbot::User>());

        if(message.get_via_bot().has_value())
        {
            ASSERT_EQ(message.get_via_bot().value()->serialise(), tgbot::User().serialise());
        }

        message.set_video(std::make_shared<tgbot::Video>());

        if(message.get_video().has_value())
        {
            ASSERT_EQ(message.get_video().value()->serialise(), tgbot::Video().serialise());
        }

        message.set_video_chat_ended(std::make_shared<tgbot::VideoChatEnded>());

        if(message.get_video_chat_ended().has_value())
        {
            ASSERT_EQ(message.get_video_chat_ended().value()->serialise(), tgbot::VideoChatEnded().serialise());
        }

        message.set_video_chat_participants_invited(std::make_shared<tgbot::VideoChatParticipantsInvited>());

        if(message.get_video_chat_participants_invited().has_value())
        {
            ASSERT_EQ(message.get_video_chat_participants_invited().value()->serialise(),
                      tgbot::VideoChatParticipantsInvited().serialise());
        }

        message.set_video_chat_scheduled(std::make_shared<tgbot::VideoChatScheduled>());

        if(message.get_video_chat_scheduled().has_value())
        {
            ASSERT_EQ(message.get_video_chat_scheduled().value()->serialise(), tgbot::VideoChatScheduled().serialise());
        }

        message.set_video_chat_started(std::make_shared<tgbot::VideoChatStarted>());

        if(message.get_video_chat_started().has_value())
        {
            ASSERT_EQ(message.get_video_chat_started().value()->serialise(), tgbot::VideoChatStarted().serialise());
        }

        message.set_video_note(std::make_shared<tgbot::VideoNote>());

        if(message.get_video_note().has_value())
        {
            ASSERT_EQ(message.get_video_note().value()->serialise(), tgbot::VideoNote().serialise());
        }

        message.set_voice(std::make_shared<tgbot::Voice>());

        if(message.get_voice().has_value())
        {
            ASSERT_EQ(message.get_voice().value()->serialise(), tgbot::Voice().serialise());
        }

        message.set_web_app_data(std::make_shared<tgbot::WebAppData>());

        if(message.get_web_app_data().has_value())
        {
            ASSERT_EQ(message.get_web_app_data().value()->serialise(), tgbot::WebAppData().serialise());
        }

        message.set_write_access_allowed(std::make_shared<tgbot::WriteAccessAllowed>());

        if(message.get_write_access_allowed().has_value())
        {
            ASSERT_EQ(message.get_write_access_allowed().value()->serialise(), tgbot::WriteAccessAllowed().serialise());
        }

        ASSERT_EQ(message.serialise(), serialised_doc);
    }
}

TEST(Message, errors)
{
    ASSERT_THROW(tgbot::Message(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["chat"] = nlohmann::json::parse(tgbot::Chat().serialise());
        }

        if(i != 1)
        {
            doc["date"] = 971;
        }

        if(i != 2)
        {
            doc["message_id"] = 894;
        }

        ASSERT_THROW(tgbot::Message(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 71; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["animation"] = nlohmann::json::parse(tgbot::Animation().serialise()) : doc["animation"] = 468;
        i != 1 ? doc["audio"] = nlohmann::json::parse(tgbot::Audio().serialise()) : doc["audio"] = 783414;
        i != 2 ? doc["author_signature"] = "üiwcb" : doc["author_signature"] = 8456;
        i != 3 ? doc["caption"] = "ergqsdvk" : doc["caption"] = 8456;
        i != 4 ?
          doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())} :
          doc["caption_entities"] = 468;
        i != 5 ? doc["channel_chat_created"] = true : doc["channel_chat_created"] = 71678648;
        i != 6 ? doc["chat"] = nlohmann::json::parse(tgbot::Chat().serialise()) : doc["chat"] = 468;
        i != 7 ?
          doc["chat_shared"] = nlohmann::json::parse(tgbot::ChatShared().serialise()) :
          doc["chat_shared"] = 731387;
        i != 8 ? doc["connected_website"] = "cipohqh" : doc["connected_website"] = 481;
        i != 9 ? doc["contact"] = nlohmann::json::parse(tgbot::Contact().serialise()) : doc["contact"] = 68713;
        i != 10 ? doc["date"] = 6844 : doc["date"] = "sdfg";
        i != 11 ? doc["delete_chat_photo"] = true : doc["delete_chat_photo"] = 7135781;
        i != 12 ? doc["dice"] = nlohmann::json::parse(tgbot::Dice().serialise()) : doc["dice"] = 9513763;
        i != 13 ? doc["document"] = nlohmann::json::parse(tgbot::Document().serialise()) : doc["document"] = 71687;
        i != 14 ? doc["edit_date"] = 894 : doc["edit_date"] = "fpiuhq";
        i != 15 ? doc["entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())} : doc["entities"] = 468;
        i != 16 ?
          doc["forum_topic_closed"] = nlohmann::json::parse(tgbot::ForumTopicClosed().serialise()) :
          doc["forum_topic_closed"] = 456657;
        i != 17 ?
          doc["forum_topic_created"] = nlohmann::json::parse(tgbot::ForumTopicCreated().serialise()) :
          doc["forum_topic_created"] = 72687;
        i != 18 ?
          doc["forum_topic_edited"] = nlohmann::json::parse(tgbot::ForumTopicEdited().serialise()) :
          doc["forum_topic_edited"] = 971354;
        i != 19 ?
          doc["forum_topic_reopened"] = nlohmann::json::parse(tgbot::ForumTopicReopened().serialise()) :
          doc["forum_topic_reopened"] = 26442;
        i != 20 ? doc["forward_date"] = 894 : doc["forward_date"] = "fpiuhq";
        i != 21 ? doc["forward_from"] = nlohmann::json::parse(tgbot::User().serialise()) : doc["forward_from"] = 468;
        i != 22 ?
          doc["forward_from_chat"] = nlohmann::json::parse(tgbot::Chat().serialise()) :
          doc["forward_from_chat"] = 468;
        i != 23 ? doc["forward_from_message_id"] = 894 : doc["forward_from_message_id"] = "fpiuhq";
        i != 24 ? doc["forward_sender_name"] = "wqwpoi" : doc["forward_sender_name"] = 8456;
        i != 25 ? doc["forward_signature"] = "üiwcb" : doc["forward_signature"] = 8456;
        i != 26 ? doc["from"] = nlohmann::json::parse(tgbot::User().serialise()) : doc["from"] = 468;
        i != 27 ? doc["game"] = nlohmann::json::parse(tgbot::Game().serialise()) : doc["game"] = 843684;
        i != 28 ?
          doc["general_forum_topic_hidden"] = nlohmann::json::parse(tgbot::GeneralForumTopicHidden().serialise()) :
          doc["general_forum_topic_hidden"] = 9726;
        i != 29 ?
          doc["general_forum_topic_unhidden"] = nlohmann::json::parse(tgbot::GeneralForumTopicUnhidden().serialise()) :
          doc["general_forum_topic_unhidden"] = 98471622;
        i != 30 ? doc["group_chat_created"] = true : doc["group_chat_created"] = 71687;
        i != 31 ? doc["has_media_spoiler"] = false : doc["has_media_spoiler"] = 6813876;
        i != 32 ? doc["has_protected_content"] = false : doc["has_protected_content"] = 11234;
        i != 33 ? doc["invoice"] = nlohmann::json::parse(tgbot::Invoice().serialise()) : doc["invoice"] = 7164;
        i != 34 ? doc["is_automatic_forward"] = false : doc["is_automatic_forward"] = 738135;
        i != 35 ? doc["is_topic_message"] = false : doc["is_topic_message"] = 9875313;
        i != 36 ?
          doc["left_chat_member"] = nlohmann::json::parse(tgbot::User().serialise()) :
          doc["left_chat_member"] = 468;
        i != 37 ? doc["location"] = nlohmann::json::parse(tgbot::Location().serialise()) : doc["location"] = 468;
        i != 38 ? doc["media_group_id"] = "qoihth" : doc["media_group_id"] = 941;
        i != 39 ?
          doc["message_auto_delete_timer_changed"] =
            nlohmann::json::parse(tgbot::MessageAutoDeleteTimerChanged().serialise()) :
          doc["message_auto_delete_timer_changed"] = 468;
        i != 40 ? doc["message_id"] = 894 : doc["message_id"] = "fpiuhq";
        i != 41 ? doc["message_thread_id"] = 7184552 : doc["message_thread_id"] = "dpowpoe";
        i != 42 ? doc["migrate_from_chat_id"] = 1894 : doc["migrate_from_chat_id"] = "dfjhoi";
        i != 43 ? doc["migrate_to_chat_id"] = 1894 : doc["migrate_to_chat_id"] = "dfjhoi";
        i != 44 ?
          doc["new_chat_members"] = {nlohmann::json::parse(tgbot::User().serialise())} :
          doc["new_chat_members"] = 468;
        i != 45 ?
          doc["new_chat_photo"] = {nlohmann::json::parse(tgbot::PhotoSize().serialise())} :
          doc["new_chat_photo"] = 71687;
        i != 46 ? doc["new_chat_title"] = "üiwcb" : doc["new_chat_title"] = 8456;
        i != 47 ?
          doc["passport_data"] = nlohmann::json::parse(tgbot::PassportData().serialise()) :
          doc["passport_data"] = 798154;
        i != 48 ? doc["photo"] = {nlohmann::json::parse(tgbot::PhotoSize().serialise())} : doc["photo"] = 468;
        i != 49 ?
          doc["pinned_message"] = nlohmann::json::parse(tgbot::Message().serialise()) :
          doc["pinned_message"] = 7135798;
        i != 50 ? doc["poll"] = nlohmann::json::parse(tgbot::Poll().serialise()) : doc["poll"] = 468;
        i != 51 ?
          doc["proximity_alert_triggered"] = nlohmann::json::parse(tgbot::ProximityAlertTriggered().serialise()) :
          doc["proximity_alert_triggered"] = 468;
        i != 52 ?
          doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise()) :
          doc["reply_markup"] = 7981354;
        i != 53 ?
          doc["reply_to_message"] = nlohmann::json::parse(tgbot::Message().serialise()) :
          doc["reply_to_message"] = 7654744;
        i != 54 ? doc["sender_chat"] = nlohmann::json::parse(tgbot::Chat().serialise()) : doc["sender_chat"] = 7984387;
        i != 55 ? doc["sticker"] = nlohmann::json::parse(tgbot::Sticker().serialise()) : doc["sticker"] = 468;
        i != 56 ?
          doc["successful_payment"] = nlohmann::json::parse(tgbot::SuccessfulPayment().serialise()) :
          doc["successful_payment"] = 99;
        i != 57 ? doc["supergroup_chat_created"] = true : doc["supergroup_chat_created"] = 9873145;
        i != 58 ? doc["text"] = "üiwcb" : doc["text"] = 8456;
        i != 59 ?
          doc["user_shared"] = nlohmann::json::parse(tgbot::UserShared().serialise()) :
          doc["user_shared"] = 2664534;
        i != 60 ? doc["venue"] = nlohmann::json::parse(tgbot::Venue().serialise()) : doc["venue"] = 71687;
        i != 61 ? doc["via_bot"] = nlohmann::json::parse(tgbot::User().serialise()) : doc["via_bot"] = 1134;
        i != 62 ? doc["video"] = nlohmann::json::parse(tgbot::Video().serialise()) : doc["video"] = -681;
        i != 63 ?
          doc["video_chat_ended"] = nlohmann::json::parse(tgbot::VideoChatEnded().serialise()) :
          doc["video_chat_ended"] = 7115;
        i != 64 ?
          doc["video_chat_participants_invited"] =
            nlohmann::json::parse(tgbot::VideoChatParticipantsInvited().serialise()) :
          doc["video_chat_participants_invited"] = 7;
        i != 65 ?
          doc["video_chat_scheduled"] = nlohmann::json::parse(tgbot::VideoChatScheduled().serialise()) :
          doc["video_chat_scheduled"] = -7651;
        i != 66 ?
          doc["video_chat_started"] = nlohmann::json::parse(tgbot::VideoChatStarted().serialise()) :
          doc["video_chat_started"] = -7;
        i != 67 ? doc["video_note"] = nlohmann::json::parse(tgbot::VideoNote().serialise()) : doc["video_note"] = 6265;
        i != 68 ? doc["voice"] = nlohmann::json::parse(tgbot::Voice().serialise()) : doc["voice"] = 951645;
        i != 69 ?
          doc["web_app_data"] = nlohmann::json::parse(tgbot::WebAppData().serialise()) :
          doc["web_app_data"] = 684;
        i != 70 ?
          doc["write_access_allowed"] = nlohmann::json::parse(tgbot::WriteAccessAllowed().serialise()) :
          doc["write_access_allowed"] = -354687;

        ASSERT_THROW(tgbot::Message(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 5; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i == 0)
        {
            doc["caption_entities"] = {36249814};
        }

        doc["chat"] = nlohmann::json::parse(tgbot::Chat().serialise());
        doc["date"] = 971;

        if(i == 1)
        {
            doc["entities"] = {68464426};
        }

        doc["message_id"] = 894;

        if(i == 2)
        {
            doc["new_chat_members"] = {6168248};
        }

        if(i == 3)
        {
            doc["new_chat_photo"] = {3678184};
        }

        if(i == 4)
        {
            doc["photo"] = {6429841};
        }

        ASSERT_THROW(tgbot::Message(doc.dump()), std::invalid_argument);
    }
}
