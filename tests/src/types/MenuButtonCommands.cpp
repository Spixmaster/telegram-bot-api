#include "tgbot/types/MenuButtonCommands.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(MenuButtonCommands, normal)
{
    ASSERT_NO_THROW(tgbot::MenuButtonCommands(tgbot::MenuButtonCommands().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["type"] = "hxaoijhr";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::MenuButtonCommands(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::MenuButtonCommands menu_button_commands;

        menu_button_commands.set_type("hxaoijhr");
        ASSERT_EQ(menu_button_commands.get_type(), "hxaoijhr");

        ASSERT_EQ(menu_button_commands.serialise(), serialised_doc);
    }
}

TEST(MenuButtonCommands, errors)
{
    ASSERT_THROW(tgbot::MenuButtonCommands(nlohmann::json::array().dump()), std::invalid_argument);

    {
        const nlohmann::json doc = nlohmann::json::object();

        ASSERT_THROW(tgbot::MenuButtonCommands(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 1; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["type"] = "pozüpjq" : doc["type"] = 81354;

        ASSERT_THROW(tgbot::MenuButtonCommands(doc.dump()), std::invalid_argument);
    }
}
