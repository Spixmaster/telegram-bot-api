#include "tgbot/types/ForceReply.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(ForceReply, normal)
{
    ASSERT_NO_THROW(tgbot::ForceReply(tgbot::ForceReply().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["force_reply"] = true;
    doc["input_field_placeholder"] = "trqoiu";
    doc["selective"] = false;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ForceReply(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::ForceReply force_reply;

        force_reply.set_force_reply(true);
        ASSERT_EQ(force_reply.get_force_reply(), true);

        force_reply.set_input_field_placeholder("trqoiu");
        ASSERT_EQ(force_reply.get_input_field_placeholder(), "trqoiu");

        force_reply.set_selective(false);
        ASSERT_EQ(force_reply.get_selective(), false);

        ASSERT_EQ(force_reply.serialise(), serialised_doc);
    }
}

TEST(ForceReply, errors)
{
    ASSERT_THROW(tgbot::ForceReply(nlohmann::json::array().dump()), std::invalid_argument);

    {
        const nlohmann::json doc = nlohmann::json::object();

        ASSERT_THROW(tgbot::ForceReply(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["force_reply"] = true : doc["force_reply"] = 7351;
        i != 1 ? doc["input_field_placeholder"] = "tuihwh" : doc["input_field_placeholder"] = 79;
        i != 2 ? doc["selective"] = true : doc["selective"] = 753154;

        ASSERT_THROW(tgbot::ForceReply(doc.dump()), std::invalid_argument);
    }
}
