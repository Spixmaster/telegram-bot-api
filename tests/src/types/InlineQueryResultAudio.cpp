#include "tgbot/types/InlineQueryResultAudio.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InlineQueryResultAudio, normal)
{
    ASSERT_NO_THROW(tgbot::InlineQueryResultAudio(tgbot::InlineQueryResultAudio().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["audio_duration"] = -98148;
    doc["audio_url"] = "cboijfh";
    doc["caption"] = "cbwiojmdfh";
    doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())};
    doc["id"] = "cbowmdfg";
    doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise());
    doc["parse_mode"] = "fhwoiefjnb";
    doc["performer"] = "poeiurngb";
    doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise());
    doc["title"] = "cbopwiuenr";
    doc["type"] = "cbmowiefg";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InlineQueryResultAudio(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InlineQueryResultAudio inline_query_result_audio;

        inline_query_result_audio.set_audio_duration(-98148);
        ASSERT_EQ(inline_query_result_audio.get_audio_duration(), -98148);

        inline_query_result_audio.set_audio_url("cboijfh");
        ASSERT_EQ(inline_query_result_audio.get_audio_url(), "cboijfh");

        inline_query_result_audio.set_caption("cbwiojmdfh");
        ASSERT_EQ(inline_query_result_audio.get_caption(), "cbwiojmdfh");

        inline_query_result_audio.set_caption_entities(std::vector<tgbot::MessageEntity::ptr> {
          std::make_shared<tgbot::MessageEntity>()});

        if(inline_query_result_audio.get_caption_entities().has_value())
        {
            ASSERT_EQ(inline_query_result_audio.get_caption_entities().value().at(0)->serialise(),
                      tgbot::MessageEntity().serialise());
        }

        inline_query_result_audio.set_id("cbowmdfg");
        ASSERT_EQ(inline_query_result_audio.get_id(), "cbowmdfg");

        inline_query_result_audio.set_input_message_content(std::make_shared<tgbot::InputMessageContent>());

        if(inline_query_result_audio.get_input_message_content().has_value())
        {
            ASSERT_EQ(inline_query_result_audio.get_input_message_content().value()->serialise(),
                      tgbot::InputMessageContent().serialise());
        }

        inline_query_result_audio.set_parse_mode("fhwoiefjnb");
        ASSERT_EQ(inline_query_result_audio.get_parse_mode(), "fhwoiefjnb");

        inline_query_result_audio.set_performer("poeiurngb");
        ASSERT_EQ(inline_query_result_audio.get_performer(), "poeiurngb");

        inline_query_result_audio.set_reply_markup(std::make_shared<tgbot::InlineKeyboardMarkup>());

        if(inline_query_result_audio.get_reply_markup().has_value())
        {
            ASSERT_EQ(inline_query_result_audio.get_reply_markup().value()->serialise(),
                      tgbot::InlineKeyboardMarkup().serialise());
        }

        inline_query_result_audio.set_title("cbopwiuenr");
        ASSERT_EQ(inline_query_result_audio.get_title(), "cbopwiuenr");

        inline_query_result_audio.set_type("cbmowiefg");
        ASSERT_EQ(inline_query_result_audio.get_type(), "cbmowiefg");

        ASSERT_EQ(inline_query_result_audio.serialise(), serialised_doc);
    }
}

TEST(InlineQueryResultAudio, errors)
{
    ASSERT_THROW(tgbot::InlineQueryResultAudio(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["audio_url"] = "icqun";
        }

        if(i != 1)
        {
            doc["id"] = "owihf";
        }

        if(i != 2)
        {
            doc["title"] = "diopqb";
        }

        if(i != 3)
        {
            doc["type"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::InlineQueryResultAudio(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 11; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["audio_duration"] = 64 : doc["audio_duration"] = "iho";
        i != 1 ? doc["audio_url"] = "opqgd" : doc["audio_url"] = 486;
        i != 2 ? doc["caption"] = "djlhqg" : doc["caption"] = 4168;
        i != 3 ?
          doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())} :
          doc["caption_entities"] = 4168;
        i != 4 ? doc["id"] = "qpoidsag" : doc["id"] = 65;
        i != 5 ?
          doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise()) :
          doc["input_message_content"] = 49;
        i != 6 ? doc["parse_mode"] = "djlhqg" : doc["parse_mode"] = 4168;
        i != 7 ? doc["performer"] = "wuefhbnjdfb" : doc["performer"] = 651;
        i != 8 ?
          doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise()) :
          doc["reply_markup"] = 49;
        i != 9 ? doc["title"] = "cxpjih" : doc["title"] = 149;
        i != 10 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;

        ASSERT_THROW(tgbot::InlineQueryResultAudio(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["audio_url"] = "icqun";
        doc["caption_entities"] = {41624};
        doc["id"] = "owihf";
        doc["title"] = "diopqb";
        doc["type"] = "aoisjgldfgs";

        ASSERT_THROW(tgbot::InlineQueryResultAudio(doc.dump()), std::invalid_argument);
    }
}
