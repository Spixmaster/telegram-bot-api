#include "tgbot/types/BotCommand.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(BotCommand, normal)
{
    ASSERT_NO_THROW(tgbot::BotCommand(tgbot::BotCommand().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["command"] = "aoijcnb";
    doc["description"] = "gnpoimjsc";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::BotCommand(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::BotCommand bot_command;

        bot_command.set_command("aoijcnb");
        ASSERT_EQ(bot_command.get_command(), "aoijcnb");

        bot_command.set_description("gnpoimjsc");
        ASSERT_EQ(bot_command.get_description(), "gnpoimjsc");

        ASSERT_EQ(bot_command.serialise(), serialised_doc);
    }
}

TEST(BotCommand, errors)
{
    ASSERT_THROW(tgbot::BotCommand(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["command"] = "daawe";
        }

        if(i != 1)
        {
            doc["description"] = "seher";
        }

        ASSERT_THROW(tgbot::BotCommand(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["command"] = "aoisjgldfgs" : doc["command"] = 286;
        i != 1 ? doc["description"] = "owihf" : doc["description"] = 65;

        ASSERT_THROW(tgbot::BotCommand(doc.dump()), std::invalid_argument);
    }
}
