#include "tgbot/types/InputVenueMessageContent.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(InputVenueMessageContent, normal)
{
    ASSERT_NO_THROW(tgbot::InputVenueMessageContent(tgbot::InputVenueMessageContent().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["address"] = "ölijmx";
    doc["foursquare_id"] = "raposiijgerg";
    doc["foursquare_type"] = "hpoijsdf";
    doc["google_place_id"] = "hölkjasdg";
    doc["google_place_type"] = "hskjdfg";
    doc["latitude"] = 574.4F;
    doc["longitude"] = -54.46F;
    doc["title"] = "üplknsdf";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InputVenueMessageContent(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InputVenueMessageContent input_venue_message_content;

        input_venue_message_content.set_address("ölijmx");
        ASSERT_EQ(input_venue_message_content.get_address(), "ölijmx");

        input_venue_message_content.set_foursquare_id("raposiijgerg");
        ASSERT_EQ(input_venue_message_content.get_foursquare_id(), "raposiijgerg");

        input_venue_message_content.set_foursquare_type("hpoijsdf");
        ASSERT_EQ(input_venue_message_content.get_foursquare_type(), "hpoijsdf");

        input_venue_message_content.set_google_place_id("hölkjasdg");
        ASSERT_EQ(input_venue_message_content.get_google_place_id(), "hölkjasdg");

        input_venue_message_content.set_google_place_type("hskjdfg");
        ASSERT_EQ(input_venue_message_content.get_google_place_type(), "hskjdfg");

        input_venue_message_content.set_latitude(574.4F);
        ASSERT_EQ(input_venue_message_content.get_latitude(), 574.4F);

        input_venue_message_content.set_longitude(-54.46F);
        ASSERT_EQ(input_venue_message_content.get_longitude(), -54.46F);

        input_venue_message_content.set_title("üplknsdf");
        ASSERT_EQ(input_venue_message_content.get_title(), "üplknsdf");

        ASSERT_EQ(input_venue_message_content.serialise(), serialised_doc);
    }
}

TEST(InputVenueMessageContent, errors)
{
    ASSERT_THROW(tgbot::InputVenueMessageContent(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["address"] = "owihf";
        }

        if(i != 1)
        {
            doc["latitude"] = 81.8;
        }

        if(i != 2)
        {
            doc["longitude"] = 79.3;
        }

        if(i != 3)
        {
            doc["title"] = "owihf";
        }

        ASSERT_THROW(tgbot::InputVenueMessageContent(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 8; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["address"] = "djlhqg" : doc["address"] = 4168;
        i != 1 ? doc["foursquare_id"] = "qijsd" : doc["foursquare_id"] = 6851;
        i != 2 ? doc["foursquare_type"] = "thwopisdv" : doc["foursquare_type"] = 89654;
        i != 3 ? doc["google_place_id"] = "erpgoisqd" : doc["google_place_id"] = 4168;
        i != 4 ? doc["google_place_type"] = "djlhqg" : doc["google_place_type"] = 714;
        i != 5 ? doc["latitude"] = 1687.1 : doc["latitude"] = "eojsfh";
        i != 6 ? doc["longitude"] = 79.6 : doc["longitude"] = "üpjvn";
        i != 7 ? doc["title"] = "owihf" : doc["title"] = 65;

        ASSERT_THROW(tgbot::InputVenueMessageContent(doc.dump()), std::invalid_argument);
    }
}
