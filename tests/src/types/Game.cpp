#include "tgbot/types/Game.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(Game, normal)
{
    ASSERT_NO_THROW(tgbot::Game(tgbot::Game().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["animation"] = nlohmann::json::parse(tgbot::Animation().serialise());
    doc["description"] = "fnüiowmjfg";
    doc["photo"] = {nlohmann::json::parse(tgbot::PhotoSize().serialise())};
    doc["text"] = "spodimbdf";
    doc["text_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())};
    doc["title"] = "dfopimjsdfh";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::Game(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::Game game;

        game.set_animation(std::make_shared<tgbot::Animation>());

        if(game.get_animation().has_value())
        {
            ASSERT_EQ(game.get_animation().value()->serialise(), tgbot::Animation().serialise());
        }

        game.set_description("fnüiowmjfg");
        ASSERT_EQ(game.get_description(), "fnüiowmjfg");

        game.set_photo({std::make_shared<tgbot::PhotoSize>()});
        ASSERT_EQ(game.get_photo().at(0)->serialise(), tgbot::PhotoSize().serialise());

        game.set_text("spodimbdf");
        ASSERT_EQ(game.get_text(), "spodimbdf");

        game.set_text_entities(std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()});

        if(game.get_text_entities().has_value())
        {
            ASSERT_EQ(game.get_text_entities().value().at(0)->serialise(), tgbot::MessageEntity().serialise());
        }

        game.set_title("dfopimjsdfh");
        ASSERT_EQ(game.get_title(), "dfopimjsdfh");

        ASSERT_EQ(game.serialise(), serialised_doc);
    }
}

TEST(Game, errors)
{
    ASSERT_THROW(tgbot::Game(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["description"] = "owihf";
        }

        if(i != 1)
        {
            doc["photo"] = {nlohmann::json::parse(tgbot::PhotoSize().serialise())};
        }

        if(i != 2)
        {
            doc["title"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::Game(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 6; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["animation"] = nlohmann::json::parse(tgbot::Animation().serialise()) : doc["animation"] = 468;
        i != 1 ? doc["description"] = "owihf" : doc["description"] = 68741;
        i != 2 ? doc["photo"] = {nlohmann::json::parse(tgbot::PhotoSize().serialise())} : doc["photo"] = 4;
        i != 3 ? doc["text"] = "iniwb" : doc["text"] = 819;
        i != 4 ?
          doc["text_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())} :
          doc["text_entities"] = 617;
        i != 5 ? doc["title"] = "aoisjgldfgs" : doc["title"] = 286;

        ASSERT_THROW(tgbot::Game(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        doc["description"] = "owihf";

        if(i == 0)
        {
            doc["photo"] = {4164824};
        }
        else
        {
            doc["photo"] = {nlohmann::json::parse(tgbot::PhotoSize().serialise())};
        }

        if(i == 1)
        {
            doc["text_entities"] = {16461};
        }

        doc["title"] = "aoisjgldfgs";

        ASSERT_THROW(tgbot::Game(doc.dump()), std::invalid_argument);
    }
}
