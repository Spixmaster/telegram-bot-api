#include "tgbot/types/Venue.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(Venue, normal)
{
    ASSERT_NO_THROW(tgbot::Venue(tgbot::Venue().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["address"] = "roöimnskdlf";
    doc["foursquare_id"] = "yhsfs";
    doc["foursquare_type"] = "epoifnb";
    doc["google_place_id"] = "änpjgbspuhdfg";
    doc["google_place_type"] = "böijflkjfs";
    doc["location"] = nlohmann::json::parse(tgbot::Location().serialise());
    doc["title"] = "lkyxcmöoisjdfg";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::Venue(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::Venue venue;

        venue.set_address("roöimnskdlf");
        ASSERT_EQ(venue.get_address(), "roöimnskdlf");

        venue.set_foursquare_id("yhsfs");
        ASSERT_EQ(venue.get_foursquare_id(), "yhsfs");

        venue.set_foursquare_type("epoifnb");
        ASSERT_EQ(venue.get_foursquare_type(), "epoifnb");

        venue.set_google_place_id("änpjgbspuhdfg");
        ASSERT_EQ(venue.get_google_place_id(), "änpjgbspuhdfg");

        venue.set_google_place_type("böijflkjfs");
        ASSERT_EQ(venue.get_google_place_type(), "böijflkjfs");

        venue.set_location(std::make_shared<tgbot::Location>());
        ASSERT_EQ(venue.get_location()->serialise(), tgbot::Location().serialise());

        venue.set_title("lkyxcmöoisjdfg");
        ASSERT_EQ(venue.get_title(), "lkyxcmöoisjdfg");

        ASSERT_EQ(venue.serialise(), serialised_doc);
    }
}

TEST(Venue, errors)
{
    ASSERT_THROW(tgbot::Venue(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["address"] = "woifhn";
        }

        if(i != 1)
        {
            doc["location"] = nlohmann::json::parse(tgbot::Location().serialise());
        }

        if(i != 2)
        {
            doc["title"] = "owihf";
        }

        ASSERT_THROW(tgbot::Venue(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 7; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["address"] = "owihf" : doc["address"] = 65;
        i != 1 ? doc["foursquare_id"] = "wuefhbnjdfb" : doc["foursquare_id"] = 651;
        i != 2 ? doc["foursquare_type"] = "woöinefmoib" : doc["foursquare_type"] = 616842;
        i != 3 ? doc["google_place_id"] = "rhpoiauhx" : doc["google_place_id"] = 687653214;
        i != 4 ? doc["google_place_type"] = "werposav" : doc["google_place_type"] = 6822624;
        i != 5 ? doc["location"] = nlohmann::json::parse(tgbot::Location().serialise()) : doc["location"] = 468;
        i != 6 ? doc["title"] = "aoisjgldfgs" : doc["title"] = 286;

        ASSERT_THROW(tgbot::Venue(doc.dump()), std::invalid_argument);
    }
}
