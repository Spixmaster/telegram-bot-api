#include "tgbot/types/BotCommandScopeDefault.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(BotCommandScopeDefault, normal)
{
    ASSERT_NO_THROW(tgbot::BotCommandScopeDefault(tgbot::BotCommandScopeDefault().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["type"] = "täpijcb";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::BotCommandScopeDefault(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::BotCommandScopeDefault bot_command_scope_default;

        bot_command_scope_default.set_type("täpijcb");
        ASSERT_EQ(bot_command_scope_default.get_type(), "täpijcb");

        ASSERT_EQ(bot_command_scope_default.serialise(), serialised_doc);
    }
}

TEST(BotCommandScopeDefault, errors)
{
    ASSERT_THROW(tgbot::BotCommandScopeDefault(nlohmann::json::array().dump()), std::invalid_argument);

    {
        const nlohmann::json doc = nlohmann::json::object();

        ASSERT_THROW(tgbot::BotCommandScopeDefault(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 1; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;

        ASSERT_THROW(tgbot::BotCommandScopeDefault(doc.dump()), std::invalid_argument);
    }
}
