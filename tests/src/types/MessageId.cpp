#include "tgbot/types/MessageId.h"

#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(MessageId, normal)
{
    ASSERT_NO_THROW(tgbot::MessageId(tgbot::MessageId().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["message_id"] = -878285;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::MessageId(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::MessageId message_id;

        message_id.set_message_id(-878285);
        ASSERT_EQ(message_id.get_message_id(), -878285);

        ASSERT_EQ(message_id.serialise(), serialised_doc);
    }
}

TEST(MessageId, errors)
{
    ASSERT_THROW(tgbot::MessageId(nlohmann::json::array().dump()), std::invalid_argument);

    {
        const nlohmann::json doc = nlohmann::json::object();

        ASSERT_THROW(tgbot::MessageId(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 1; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["message_id"] = 87 : doc["message_id"] = "herthsdf";

        ASSERT_THROW(tgbot::MessageId(doc.dump()), std::invalid_argument);
    }
}
