#include "tgbot/types/ResponseParameters.h"

#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(ResponseParameters, normal)
{
    ASSERT_NO_THROW(tgbot::ResponseParameters(tgbot::ResponseParameters().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["migrate_to_chat_id"] = -685468;
    doc["retry_after"] = 36984521;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ResponseParameters(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::ResponseParameters response_parameters;

        response_parameters.set_migrate_to_chat_id(-685468);
        ASSERT_EQ(response_parameters.get_migrate_to_chat_id(), -685468);

        response_parameters.set_retry_after(36984521);
        ASSERT_EQ(response_parameters.get_retry_after(), 36984521);

        ASSERT_EQ(response_parameters.serialise(), serialised_doc);
    }
}

TEST(ResponseParameters, errors)
{
    ASSERT_THROW(tgbot::ResponseParameters(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["migrate_to_chat_id"] = 4561 : doc["migrate_to_chat_id"] = "sdfhsdrg";
        i != 1 ? doc["retry_after"] = 4561 : doc["retry_after"] = "sdfhsdrg";

        ASSERT_THROW(tgbot::ResponseParameters(doc.dump()), std::invalid_argument);
    }
}
