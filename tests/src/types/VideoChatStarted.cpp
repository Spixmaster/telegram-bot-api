#include "tgbot/types/VideoChatStarted.h"

#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(VideoChatStarted, normal)
{
    ASSERT_NO_THROW(tgbot::VideoChatStarted(tgbot ::VideoChatStarted().serialise()));

    const nlohmann::json doc = nlohmann::json::object();

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::VideoChatStarted(serialised_doc).serialise(), serialised_doc);

    {
        const tgbot::VideoChatStarted voice_chat_started;

        ASSERT_EQ(voice_chat_started.serialise(), serialised_doc);
    }
}

TEST(VideoChatStarted, errors)
{
    ASSERT_THROW(tgbot::VideoChatStarted(nlohmann::json::array().dump()), std::invalid_argument);
}
