#include "tgbot/types/ReplyKeyboardMarkup.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(ReplyKeyboardMarkup, normal)
{
    ASSERT_NO_THROW(tgbot::ReplyKeyboardMarkup(tgbot::ReplyKeyboardMarkup().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["input_field_placeholder"] = "wpoinv";
    doc["is_persistent"] = true;
    doc["keyboard"] = {{nlohmann::json::parse(tgbot::KeyboardButton().serialise())}};
    doc["one_time_keyboard"] = true;
    doc["resize_keyboard"] = false;
    doc["selective"] = false;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ReplyKeyboardMarkup(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::ReplyKeyboardMarkup reply_keyboard_markup;

        reply_keyboard_markup.set_input_field_placeholder("wpoinv");
        ASSERT_EQ(reply_keyboard_markup.get_input_field_placeholder(), "wpoinv");

        reply_keyboard_markup.set_is_persistent(true);
        ASSERT_EQ(reply_keyboard_markup.get_is_persistent(), true);

        reply_keyboard_markup.set_keyboard({{std::make_shared<tgbot::KeyboardButton>()}});
        ASSERT_EQ(reply_keyboard_markup.get_keyboard().at(0).at(0)->serialise(), tgbot::KeyboardButton().serialise());

        reply_keyboard_markup.set_one_time_keyboard(true);
        ASSERT_EQ(reply_keyboard_markup.get_one_time_keyboard(), true);

        reply_keyboard_markup.set_resize_keyboard(false);
        ASSERT_EQ(reply_keyboard_markup.get_resize_keyboard(), false);

        reply_keyboard_markup.set_selective(false);
        ASSERT_EQ(reply_keyboard_markup.get_selective(), false);

        ASSERT_EQ(reply_keyboard_markup.serialise(), serialised_doc);
    }

    {
        const std::vector<std::vector<tgbot::KeyboardButton::ptr>> keyboard;
        ASSERT_EQ(tgbot::ReplyKeyboardMarkup(keyboard).get_keyboard(), keyboard);
    }
}

TEST(ReplyKeyboardMarkup, errors)
{
    ASSERT_THROW(tgbot::ReplyKeyboardMarkup(nlohmann::json::array().dump()), std::invalid_argument);

    {
        const nlohmann::json doc = nlohmann::json::object();

        ASSERT_THROW(tgbot::ReplyKeyboardMarkup(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 6; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["input_field_placeholder"] = "ohjwfg" : doc["input_field_placeholder"] = 486;
        i != 1 ? doc["is_persistent"] = false : doc["is_persistent"] = 198;
        i != 2 ?
          doc["keyboard"] = {{nlohmann::json::parse(tgbot::KeyboardButton().serialise())}} :
          doc["keyboard"] = 286;
        i != 3 ? doc["one_time_keyboard"] = false : doc["one_time_keyboard"] = 1354;
        i != 4 ? doc["resize_keyboard"] = true : doc["resize_keyboard"] = 991;
        i != 5 ? doc["selective"] = false : doc["selective"] = 564;

        ASSERT_THROW(tgbot::ReplyKeyboardMarkup(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["keyboard"] = {98732};

        ASSERT_THROW(tgbot::ReplyKeyboardMarkup(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["keyboard"] = {{1682468}};

        ASSERT_THROW(tgbot::ReplyKeyboardMarkup(doc.dump()), std::invalid_argument);
    }
}
