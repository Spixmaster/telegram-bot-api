#include "tgbot/types/Contact.h"

#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(Contact, normal)
{
    ASSERT_NO_THROW(tgbot::Contact(tgbot::Contact().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["first_name"] = "tpwihmunc";
    doc["last_name"] = "sfgnsdfv";
    doc["phone_number"] = "xćpoinswfh";
    doc["user_id"] = -144;
    doc["vcard"] = "wpoijfgpoisdfg";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::Contact(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::Contact contact;

        contact.set_first_name("tpwihmunc");
        ASSERT_EQ(contact.get_first_name(), "tpwihmunc");

        contact.set_last_name("sfgnsdfv");
        ASSERT_EQ(contact.get_last_name(), "sfgnsdfv");

        contact.set_phone_number("xćpoinswfh");
        ASSERT_EQ(contact.get_phone_number(), "xćpoinswfh");

        contact.set_user_id(-144);
        ASSERT_EQ(contact.get_user_id(), -144);

        contact.set_vcard("wpoijfgpoisdfg");
        ASSERT_EQ(contact.get_vcard(), "wpoijfgpoisdfg");

        ASSERT_EQ(contact.serialise(), serialised_doc);
    }
}

TEST(Contact, errors)
{
    ASSERT_THROW(tgbot::Contact(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["first_name"] = "owihf";
        }

        if(i != 1)
        {
            doc["phone_number"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::Contact(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 5; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["first_name"] = "owihf" : doc["first_name"] = 761;
        i != 1 ? doc["last_name"] = "qefob" : doc["last_name"] = 31331;
        i != 2 ? doc["phone_number"] = "aoisjgldfgs" : doc["phone_number"] = 98513;
        i != 3 ? doc["user_id"] = 984 : doc["user_id"] = "oihh";
        i != 4 ? doc["vcard"] = "hwpoij" : doc["vcard"] = 8751;

        ASSERT_THROW(tgbot::Contact(doc.dump()), std::invalid_argument);
    }
}
