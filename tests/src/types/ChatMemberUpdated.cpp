#include "tgbot/types/ChatMemberUpdated.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(ChatMemberUpdated, normal)
{
    ASSERT_NO_THROW(tgbot::ChatMemberUpdated(tgbot::ChatMemberUpdated().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["chat"] = nlohmann::json::parse(tgbot::Chat().serialise());
    doc["date"] = 6841687;
    doc["from"] = nlohmann::json::parse(tgbot::User().serialise());
    doc["invite_link"] = nlohmann::json::parse(tgbot::ChatInviteLink().serialise());
    doc["new_chat_member"] = nlohmann::json::parse(tgbot::ChatMember().serialise());
    doc["old_chat_member"] = nlohmann::json::parse(tgbot::ChatMember().serialise());

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ChatMemberUpdated(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::ChatMemberUpdated chat_member_updated;

        chat_member_updated.set_chat(std::make_shared<tgbot::Chat>());
        ASSERT_EQ(chat_member_updated.get_chat()->serialise(), tgbot::Chat().serialise());

        chat_member_updated.set_date(6841687);
        ASSERT_EQ(chat_member_updated.get_date(), 6841687);

        chat_member_updated.set_from(std::make_shared<tgbot::User>());
        ASSERT_EQ(chat_member_updated.get_from()->serialise(), tgbot::User().serialise());

        chat_member_updated.set_invite_link(std::make_shared<tgbot::ChatInviteLink>());

        if(chat_member_updated.get_invite_link().has_value())
        {
            ASSERT_EQ(chat_member_updated.get_invite_link().value()->serialise(), tgbot::ChatInviteLink().serialise());
        }

        chat_member_updated.set_new_chat_member(std::make_shared<tgbot::ChatMember>());
        ASSERT_EQ(chat_member_updated.get_new_chat_member()->serialise(), tgbot::ChatMember().serialise());

        chat_member_updated.set_old_chat_member(std::make_shared<tgbot::ChatMember>());
        ASSERT_EQ(chat_member_updated.get_old_chat_member()->serialise(), tgbot::ChatMember().serialise());

        ASSERT_EQ(chat_member_updated.serialise(), serialised_doc);
    }
}

TEST(ChatMemberUpdated, errors)
{
    ASSERT_THROW(tgbot::ChatMemberUpdated(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 5; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["chat"] = nlohmann::json::parse(tgbot::Chat().serialise());
        }

        if(i != 1)
        {
            doc["date"] = 6844;
        }

        if(i != 2)
        {
            doc["from"] = nlohmann::json::parse(tgbot::User().serialise());
        }

        if(i != 3)
        {
            doc["new_chat_member"] = nlohmann::json::parse(tgbot::ChatMember().serialise());
        }

        if(i != 4)
        {
            doc["old_chat_member"] = nlohmann::json::parse(tgbot::ChatMember().serialise());
        }

        ASSERT_THROW(tgbot::ChatMemberUpdated(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 6; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["chat"] = nlohmann::json::parse(tgbot::Chat().serialise()) : doc["chat"] = 9165;
        i != 1 ? doc["date"] = 684 : doc["date"] = "dag";
        i != 2 ? doc["from"] = nlohmann::json::parse(tgbot::User().serialise()) : doc["from"] = 911;
        i != 3 ?
          doc["invite_link"] = nlohmann::json::parse(tgbot::ChatInviteLink().serialise()) :
          doc["invite_link"] = 36715;
        i != 4 ?
          doc["new_chat_member"] = nlohmann::json::parse(tgbot::ChatMember().serialise()) :
          doc["new_chat_member"] = 68152;
        i != 5 ?
          doc["old_chat_member"] = nlohmann::json::parse(tgbot::ChatMember().serialise()) :
          doc["old_chat_member"] = 8416357;

        ASSERT_THROW(tgbot::ChatMemberUpdated(doc.dump()), std::invalid_argument);
    }
}
