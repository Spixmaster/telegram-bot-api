#include "tgbot/types/GeneralForumTopicHidden.h"

#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(GeneralForumTopicHidden, normal)
{
    ASSERT_NO_THROW(tgbot::GeneralForumTopicHidden(tgbot::GeneralForumTopicHidden().serialise()));

    const nlohmann::json doc = nlohmann::json::object();

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::GeneralForumTopicHidden(serialised_doc).serialise(), serialised_doc);

    {
        const tgbot::GeneralForumTopicHidden general_forum_topic_hidden;

        ASSERT_EQ(general_forum_topic_hidden.serialise(), serialised_doc);
    }
}

TEST(GeneralForumTopicHidden, errors)
{
    ASSERT_THROW(tgbot::GeneralForumTopicHidden(nlohmann::json::array().dump()), std::invalid_argument);
}
