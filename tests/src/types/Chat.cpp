#include "tgbot/types/Chat.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"
#include "tgbot/types/Message.h"

TEST(Chat, normal)
{
    ASSERT_NO_THROW(tgbot::Chat(tgbot::Chat().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["active_usernames"] = {"toxpaio"};
    doc["bio"] = "vboimjs";
    doc["can_set_sticker_set"] = false;
    doc["description"] = "bpomjwndg";
    doc["emoji_status_custom_emoji_id"] = "fpoiasd";
    doc["first_name"] = "vbäskmncv";
    doc["has_aggressive_anti_spam_enabled"] = true;
    doc["has_hidden_members"] = false;
    doc["has_private_forwards"] = false;
    doc["has_protected_content"] = true;
    doc["has_restricted_voice_and_video_messages"] = true;
    doc["id"] = 81654;
    doc["invite_link"] = "üpomiou";
    doc["is_forum"] = false;
    doc["join_by_request"] = false;
    doc["join_to_send_messages"] = true;
    doc["last_name"] = "vbiojpo";
    doc["linked_chat_id"] = -148;
    doc["location"] = nlohmann::json::parse(tgbot::ChatLocation().serialise());
    doc["message_auto_delete_time"] = -68418;
    doc["permissions"] = nlohmann::json::parse(tgbot::ChatPermissions().serialise());
    doc["photo"] = nlohmann::json::parse(tgbot::ChatPhoto().serialise());
    doc["pinned_message"] = nlohmann::json::parse(tgbot::Message().serialise());
    doc["slow_mode_delay"] = 98149;
    doc["sticker_set_name"] = "vboisjb";
    doc["title"] = "fbpomsc";
    doc["type"] = "üoijmsd";
    doc["username"] = "vbosidf";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::Chat(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::Chat chat;

        chat.set_active_usernames(std::vector<std::string> {"toxpaio"});
        ASSERT_EQ(chat.get_active_usernames(), std::vector<std::string> {"toxpaio"});

        chat.set_bio("vboimjs");
        ASSERT_EQ(chat.get_bio(), "vboimjs");

        chat.set_can_set_sticker_set(false);
        ASSERT_EQ(chat.get_can_set_sticker_set(), false);

        chat.set_description("bpomjwndg");
        ASSERT_EQ(chat.get_description(), "bpomjwndg");

        chat.set_emoji_status_custom_emoji_id("fpoiasd");
        ASSERT_EQ(chat.get_emoji_status_custom_emoji_id(), "fpoiasd");

        chat.set_first_name("vbäskmncv");
        ASSERT_EQ(chat.get_first_name(), "vbäskmncv");

        chat.set_has_aggressive_anti_spam_enabled(true);
        ASSERT_EQ(chat.get_has_aggressive_anti_spam_enabled(), true);

        chat.set_has_hidden_members(false);
        ASSERT_EQ(chat.get_has_hidden_members(), false);

        chat.set_has_private_forwards(false);
        ASSERT_EQ(chat.get_has_private_forwards(), false);

        chat.set_has_protected_content(true);
        ASSERT_EQ(chat.get_has_protected_content(), true);

        chat.set_has_restricted_voice_and_video_messages(true);
        ASSERT_EQ(chat.get_has_restricted_voice_and_video_messages(), true);

        chat.set_id(81654);
        ASSERT_EQ(chat.get_id(), 81654);

        chat.set_invite_link("üpomiou");
        ASSERT_EQ(chat.get_invite_link(), "üpomiou");

        chat.set_is_forum(false);
        ASSERT_EQ(chat.get_is_forum(), false);

        chat.set_join_by_request(false);
        ASSERT_EQ(chat.get_join_by_request(), false);

        chat.set_join_to_send_messages(true);
        ASSERT_EQ(chat.get_join_to_send_messages(), true);

        chat.set_last_name("vbiojpo");
        ASSERT_EQ(chat.get_last_name(), "vbiojpo");

        chat.set_linked_chat_id(-148);
        ASSERT_EQ(chat.get_linked_chat_id(), -148);

        chat.set_location(std::make_shared<tgbot::ChatLocation>());

        if(chat.get_location().has_value())
        {
            ASSERT_EQ(chat.get_location().value()->serialise(), tgbot::ChatLocation().serialise());
        }

        chat.set_message_auto_delete_time(-68418);
        ASSERT_EQ(chat.get_message_auto_delete_time(), -68418);

        chat.set_permissions(std::make_shared<tgbot::ChatPermissions>());

        if(chat.get_permissions().has_value())
        {
            ASSERT_EQ(chat.get_permissions().value()->serialise(), tgbot::ChatPermissions().serialise());
        }

        chat.set_photo(std::make_shared<tgbot::ChatPhoto>());

        if(chat.get_photo().has_value())
        {
            ASSERT_EQ(chat.get_photo().value()->serialise(), tgbot::ChatPhoto().serialise());
        }

        chat.set_pinned_message(std::make_shared<tgbot::Message>());

        if(chat.get_pinned_message().has_value())
        {
            ASSERT_EQ(chat.get_pinned_message().value()->serialise(), tgbot::Message().serialise());
        }

        chat.set_slow_mode_delay(98149);
        ASSERT_EQ(chat.get_slow_mode_delay(), 98149);

        chat.set_sticker_set_name("vboisjb");
        ASSERT_EQ(chat.get_sticker_set_name(), "vboisjb");

        chat.set_title("fbpomsc");
        ASSERT_EQ(chat.get_title(), "fbpomsc");

        chat.set_type("üoijmsd");
        ASSERT_EQ(chat.get_type(), "üoijmsd");

        chat.set_username("vbosidf");
        ASSERT_EQ(chat.get_username(), "vbosidf");

        ASSERT_EQ(chat.serialise(), serialised_doc);
    }
}

TEST(Chat, errors)
{
    ASSERT_THROW(tgbot::Chat(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["id"] = 8514651;
        }

        if(i != 1)
        {
            doc["type"] = "owihf";
        }

        ASSERT_THROW(tgbot::Chat(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 28; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["active_usernames"] = {"hodieg"} : doc["active_usernames"] = 416156;
        i != 1 ? doc["bio"] = "wuefhbnjdfb" : doc["bio"] = 651;
        i != 2 ? doc["can_set_sticker_set"] = false : doc["can_set_sticker_set"] = 8971353;
        i != 3 ? doc["description"] = "woöinefmoib" : doc["description"] = 616842;
        i != 4 ? doc["emoji_status_custom_emoji_id"] = "ncpwlxkh" : doc["emoji_status_custom_emoji_id"] = 7928;
        i != 5 ? doc["first_name"] = "coij" : doc["first_name"] = 4;
        i != 6 ? doc["has_aggressive_anti_spam_enabled"] = false : doc["has_aggressive_anti_spam_enabled"] = 617452;
        i != 7 ? doc["has_hidden_members"] = false : doc["has_hidden_members"] = 41068;
        i != 8 ? doc["has_private_forwards"] = false : doc["has_private_forwards"] = 8162651;
        i != 9 ? doc["has_protected_content"] = true : doc["has_protected_content"] = 98192;
        i != 10 ?
          doc["has_restricted_voice_and_video_messages"] = true :
          doc["has_restricted_voice_and_video_messages"] = 1658;
        i != 11 ? doc["id"] = 8514651 : doc["id"] = "pjnokmo";
        i != 12 ? doc["invite_link"] = "toiqufh" : doc["invite_link"] = 962;
        i != 13 ? doc["is_forum"] = true : doc["is_forum"] = 1685115;
        i != 14 ? doc["join_by_request"] = true : doc["join_by_request"] = 87632116;
        i != 15 ? doc["join_to_send_messages"] = false : doc["join_to_send_messages"] = 87632116;
        i != 16 ? doc["last_name"] = "cüijweh" : doc["last_name"] = 678;
        i != 17 ? doc["linked_chat_id"] = 4561 : doc["linked_chat_id"] = "sdfhsdrg";
        i != 18 ? doc["location"] = nlohmann::json::parse(tgbot::ChatLocation().serialise()) : doc["location"] = 68761;
        i != 19 ? doc["message_auto_delete_time"] = 4561 : doc["message_auto_delete_time"] = "sdfhsdrg";
        i != 20 ?
          doc["permissions"] = nlohmann::json::parse(tgbot::ChatPermissions().serialise()) :
          doc["permissions"] = 61;
        i != 21 ? doc["photo"] = nlohmann::json::parse(tgbot::ChatPhoto().serialise()) : doc["photo"] = 468;
        i != 22 ?
          doc["pinned_message"] = nlohmann::json::parse(tgbot::Message().serialise()) :
          doc["pinned_message"] = 18;
        i != 23 ? doc["slow_mode_delay"] = 4561 : doc["slow_mode_delay"] = "sdfhsdrg";
        i != 24 ? doc["sticker_set_name"] = "poihc" : doc["sticker_set_name"] = 7312;
        i != 25 ? doc["title"] = "ahetzk" : doc["title"] = 13;
        i != 26 ? doc["type"] = "owihf" : doc["type"] = 65;
        i != 27 ? doc["username"] = "cwpoc" : doc["username"] = 817;

        ASSERT_THROW(tgbot::Chat(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["active_usernames"] = {9716554};
        doc["id"] = 617321;
        doc["type"] = "ismbipn";

        ASSERT_THROW(tgbot::Chat(doc.dump()), std::invalid_argument);
    }
}
