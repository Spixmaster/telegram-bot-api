#include "tgbot/types/InlineQueryResultPhoto.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InlineQueryResultPhoto, normal)
{
    ASSERT_NO_THROW(tgbot::InlineQueryResultPhoto(tgbot::InlineQueryResultPhoto().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["caption"] = "göoijmx";
    doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())};
    doc["description"] = "rhlöajklsd";
    doc["id"] = "rhökjaf";
    doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise());
    doc["parse_mode"] = "ijqaüsidojg";
    doc["photo_height"] = -6828;
    doc["photo_url"] = "wtohijmkla";
    doc["photo_width"] = 861895;
    doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise());
    doc["thumbnail_url"] = "wethjmansdg";
    doc["title"] = "sdfhgwdfbv";
    doc["type"] = "erhakjsdg";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InlineQueryResultPhoto(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InlineQueryResultPhoto inline_query_result_photo;

        inline_query_result_photo.set_caption("göoijmx");
        ASSERT_EQ(inline_query_result_photo.get_caption(), "göoijmx");

        inline_query_result_photo.set_caption_entities(std::vector<tgbot::MessageEntity::ptr> {
          std::make_shared<tgbot::MessageEntity>()});

        if(inline_query_result_photo.get_caption_entities().has_value())
        {
            ASSERT_EQ(inline_query_result_photo.get_caption_entities().value().at(0)->serialise(),
                      tgbot::MessageEntity().serialise());
        }

        inline_query_result_photo.set_description("rhlöajklsd");
        ASSERT_EQ(inline_query_result_photo.get_description(), "rhlöajklsd");

        inline_query_result_photo.set_id("rhökjaf");
        ASSERT_EQ(inline_query_result_photo.get_id(), "rhökjaf");

        inline_query_result_photo.set_input_message_content(std::make_shared<tgbot::InputMessageContent>());

        if(inline_query_result_photo.get_input_message_content().has_value())
        {
            ASSERT_EQ(inline_query_result_photo.get_input_message_content().value()->serialise(),
                      tgbot::InputMessageContent().serialise());
        }

        inline_query_result_photo.set_parse_mode("ijqaüsidojg");
        ASSERT_EQ(inline_query_result_photo.get_parse_mode(), "ijqaüsidojg");

        inline_query_result_photo.set_photo_height(-6828);
        ASSERT_EQ(inline_query_result_photo.get_photo_height(), -6828);

        inline_query_result_photo.set_photo_url("wtohijmkla");
        ASSERT_EQ(inline_query_result_photo.get_photo_url(), "wtohijmkla");

        inline_query_result_photo.set_photo_width(861895);
        ASSERT_EQ(inline_query_result_photo.get_photo_width(), 861895);

        inline_query_result_photo.set_reply_markup(std::make_shared<tgbot::InlineKeyboardMarkup>());

        if(inline_query_result_photo.get_reply_markup().has_value())
        {
            ASSERT_EQ(inline_query_result_photo.get_reply_markup().value()->serialise(),
                      tgbot::InlineKeyboardMarkup().serialise());
        }

        inline_query_result_photo.set_thumbnail_url("wethjmansdg");
        ASSERT_EQ(inline_query_result_photo.get_thumbnail_url(), "wethjmansdg");

        inline_query_result_photo.set_title("sdfhgwdfbv");
        ASSERT_EQ(inline_query_result_photo.get_title(), "sdfhgwdfbv");

        inline_query_result_photo.set_type("erhakjsdg");
        ASSERT_EQ(inline_query_result_photo.get_type(), "erhakjsdg");

        ASSERT_EQ(inline_query_result_photo.serialise(), serialised_doc);
    }
}

TEST(InlineQueryResultPhoto, errors)
{
    ASSERT_THROW(tgbot::InlineQueryResultPhoto(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["id"] = "owihf";
        }

        if(i != 1)
        {
            doc["photo_url"] = "icqun";
        }

        if(i != 2)
        {
            doc["thumbnail_url"] = "diopqb";
        }

        if(i != 3)
        {
            doc["type"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::InlineQueryResultPhoto(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 13; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["caption"] = "djlhqg" : doc["caption"] = 4168;
        i != 1 ?
          doc["caption_entities"] = {nlohmann::json::parse(tgbot::MessageEntity().serialise())} :
          doc["caption_entities"] = 4168;
        i != 2 ? doc["description"] = "cxpjih" : doc["description"] = 739;
        i != 3 ? doc["id"] = "owihf" : doc["id"] = 65;
        i != 4 ?
          doc["input_message_content"] = nlohmann::json::parse(tgbot::InputMessageContent().serialise()) :
          doc["input_message_content"] = 49;
        i != 5 ? doc["parse_mode"] = "djlhqg" : doc["parse_mode"] = 4168;
        i != 6 ? doc["photo_height"] = 816 : doc["photo_height"] = "doih";
        i != 7 ? doc["photo_url"] = "opqgd" : doc["photo_url"] = 486;
        i != 8 ? doc["photo_width"] = 816 : doc["photo_width"] = "doih";
        i != 9 ?
          doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise()) :
          doc["reply_markup"] = 49;
        i != 10 ? doc["thumbnail_url"] = "opqgd" : doc["thumbnail_url"] = 486;
        i != 11 ? doc["title"] = "cxpjih" : doc["title"] = 739;
        i != 12 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;

        ASSERT_THROW(tgbot::InlineQueryResultPhoto(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["caption_entities"] = {716251};
        doc["id"] = "owihf";
        doc["photo_url"] = "icqun";
        doc["thumbnail_url"] = "diopqb";
        doc["type"] = "aoisjgldfgs";

        ASSERT_THROW(tgbot::InlineQueryResultPhoto(doc.dump()), std::invalid_argument);
    }
}
