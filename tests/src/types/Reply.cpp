#include "tgbot/types/Reply.h"

#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(Reply, normal)
{
    ASSERT_NO_THROW(tgbot::Reply(tgbot::Reply().serialise()));

    const nlohmann::json doc = nlohmann::json::object();

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::Reply().serialise(), serialised_doc);

    {
        const tgbot::Reply reply;

        ASSERT_EQ(reply.serialise(), serialised_doc);
    }
}

TEST(Reply, errors)
{
    ASSERT_THROW(tgbot::Reply(nlohmann::json::array().dump()), std::invalid_argument);
}
