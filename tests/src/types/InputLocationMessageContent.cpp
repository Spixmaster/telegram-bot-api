#include "tgbot/types/InputLocationMessageContent.h"

#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(InputLocationMessageContent, normal)
{
    ASSERT_NO_THROW(tgbot::InputLocationMessageContent(tgbot::InputLocationMessageContent().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["heading"] = -64812;
    doc["horizontal_accuracy"] = 51.6510F;
    doc["latitude"] = -681.51F;
    doc["live_period"] = 918;
    doc["longitude"] = -818.5;
    doc["proximity_alert_radius"] = 16841;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InputLocationMessageContent(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InputLocationMessageContent input_location_message_content;

        input_location_message_content.set_heading(-64812);
        ASSERT_EQ(input_location_message_content.get_heading(), -64812);

        input_location_message_content.set_horizontal_accuracy(51.6510F);
        ASSERT_EQ(input_location_message_content.get_horizontal_accuracy(), 51.6510F);

        input_location_message_content.set_latitude(-681.51F);
        ASSERT_EQ(input_location_message_content.get_latitude(), -681.51F);

        input_location_message_content.set_live_period(918);
        ASSERT_EQ(input_location_message_content.get_live_period(), 918);

        input_location_message_content.set_longitude(-818.5);
        ASSERT_EQ(input_location_message_content.get_longitude(), -818.5);

        input_location_message_content.set_proximity_alert_radius(16841);
        ASSERT_EQ(input_location_message_content.get_proximity_alert_radius(), 16841);

        ASSERT_EQ(input_location_message_content.serialise(), serialised_doc);
    }
}

TEST(InputLocationMessageContent, errors)
{
    ASSERT_THROW(tgbot::InputLocationMessageContent(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["latitude"] = 189.65;
        }

        if(i != 1)
        {
            doc["longitude"] = 79.63;
        }

        ASSERT_THROW(tgbot::InputLocationMessageContent(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 6; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["heading"] = 7981684 : doc["heading"] = "bsefbsdfh";
        i != 1 ? doc["horizontal_accuracy"] = 6844.9 : doc["horizontal_accuracy"] = "sdfg";
        i != 2 ? doc["latitude"] = 981.654 : doc["latitude"] = "cökagef";
        i != 3 ? doc["live_period"] = 87 : doc["live_period"] = "herthsdf";
        i != 4 ? doc["longitude"] = 981.654 : doc["longitude"] = "cökagef";
        i != 5 ? doc["proximity_alert_radius"] = 7981684 : doc["proximity_alert_radius"] = "bsefbsdfh";

        ASSERT_THROW(tgbot::InputLocationMessageContent(doc.dump()), std::invalid_argument);
    }
}
