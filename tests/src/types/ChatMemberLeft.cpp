#include "tgbot/types/ChatMemberLeft.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(ChatMemberLeft, normal)
{
    ASSERT_NO_THROW(tgbot::ChatMemberLeft(tgbot::ChatMemberLeft().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["status"] = "rhmvbsd";
    doc["user"] = nlohmann::json::parse(tgbot::User().serialise());

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ChatMemberLeft(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::ChatMemberLeft chat_member_left;

        chat_member_left.set_status("rhmvbsd");
        ASSERT_EQ(chat_member_left.get_status(), "rhmvbsd");

        chat_member_left.set_user(std::make_shared<tgbot::User>());
        ASSERT_EQ(chat_member_left.get_user()->serialise(), tgbot::User().serialise());

        ASSERT_EQ(chat_member_left.serialise(), serialised_doc);
    }
}

TEST(ChatMemberLeft, errors)
{
    ASSERT_THROW(tgbot::ChatMemberLeft(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["status"] = "owihf";
        }

        if(i != 1)
        {
            doc["user"] = nlohmann::json::parse(tgbot::User().serialise());
        }

        ASSERT_THROW(tgbot::ChatMemberLeft(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["status"] = "owihf" : doc["status"] = 65;
        i != 1 ? doc["user"] = nlohmann::json::parse(tgbot::User().serialise()) : doc["user"] = 286;

        ASSERT_THROW(tgbot::ChatMemberLeft(doc.dump()), std::invalid_argument);
    }
}
