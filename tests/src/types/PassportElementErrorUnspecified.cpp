#include "tgbot/types/PassportElementErrorUnspecified.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(PassportElementErrorUnspecified, normal)
{
    ASSERT_NO_THROW(tgbot::PassportElementErrorUnspecified(tgbot::PassportElementErrorUnspecified().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["element_hash"] = "apouihc";
    doc["message"] = "xśpodifjgs";
    doc["source"] = "npyoijdfg";
    doc["type"] = "rgnopisjdfg";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::PassportElementErrorUnspecified(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::PassportElementErrorUnspecified passport_element_error_unspecified;

        passport_element_error_unspecified.set_element_hash("apouihc");
        ASSERT_EQ(passport_element_error_unspecified.get_element_hash(), "apouihc");

        passport_element_error_unspecified.set_message("xśpodifjgs");
        ASSERT_EQ(passport_element_error_unspecified.get_message(), "xśpodifjgs");

        passport_element_error_unspecified.set_source("npyoijdfg");
        ASSERT_EQ(passport_element_error_unspecified.get_source(), "npyoijdfg");

        passport_element_error_unspecified.set_type("rgnopisjdfg");
        ASSERT_EQ(passport_element_error_unspecified.get_type(), "rgnopisjdfg");

        ASSERT_EQ(passport_element_error_unspecified.serialise(), serialised_doc);
    }
}

TEST(PassportElementErrorUnspecified, errors)
{
    ASSERT_THROW(tgbot::PassportElementErrorUnspecified(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["element_hash"] = "pobunfb";
        }

        if(i != 1)
        {
            doc["message"] = "yperoih";
        }

        if(i != 2)
        {
            doc["source"] = "aoisjgldfgs";
        }

        if(i != 3)
        {
            doc["type"] = "owihf";
        }

        ASSERT_THROW(tgbot::PassportElementErrorUnspecified(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["element_hash"] = "rhijasv" : doc["element_hash"] = 7351;
        i != 1 ? doc["message"] = "hroiyxa" : doc["message"] = -1234;
        i != 2 ? doc["source"] = "rzoijasdv" : doc["source"] = 774;
        i != 3 ? doc["type"] = "qsoihgqr" : doc["type"] = -8;

        ASSERT_THROW(tgbot::PassportElementErrorUnspecified(doc.dump()), std::invalid_argument);
    }
}
