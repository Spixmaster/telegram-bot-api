#include "tgbot/types/InlineQueryResultGame.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(InlineQueryResultGame, normal)
{
    ASSERT_NO_THROW(tgbot::InlineQueryResultGame(tgbot::InlineQueryResultGame().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["game_short_name"] = "rhopimnx";
    doc["id"] = "ghpijmc";
    doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise());
    doc["type"] = "hkonsfg";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::InlineQueryResultGame(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::InlineQueryResultGame inline_query_result_game;

        inline_query_result_game.set_game_short_name("rhopimnx");
        ASSERT_EQ(inline_query_result_game.get_game_short_name(), "rhopimnx");

        inline_query_result_game.set_id("ghpijmc");
        ASSERT_EQ(inline_query_result_game.get_id(), "ghpijmc");

        inline_query_result_game.set_reply_markup(std::make_shared<tgbot::InlineKeyboardMarkup>());

        if(inline_query_result_game.get_reply_markup().has_value())
        {
            ASSERT_EQ(inline_query_result_game.get_reply_markup().value()->serialise(),
                      tgbot::InlineKeyboardMarkup().serialise());
        }

        inline_query_result_game.set_type("hkonsfg");
        ASSERT_EQ(inline_query_result_game.get_type(), "hkonsfg");

        ASSERT_EQ(inline_query_result_game.serialise(), serialised_doc);
    }
}

TEST(InlineQueryResultGame, errors)
{
    ASSERT_THROW(tgbot::InlineQueryResultGame(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["game_short_name"] = "icqun";
        }

        if(i != 1)
        {
            doc["id"] = "owihf";
        }

        if(i != 2)
        {
            doc["type"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::InlineQueryResultGame(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["game_short_name"] = "opqgd" : doc["game_short_name"] = 486;
        i != 1 ? doc["id"] = "owihf" : doc["id"] = 65;
        i != 2 ?
          doc["reply_markup"] = nlohmann::json::parse(tgbot::InlineKeyboardMarkup().serialise()) :
          doc["reply_markup"] = 49;
        i != 3 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;

        ASSERT_THROW(tgbot::InlineQueryResultGame(doc.dump()), std::invalid_argument);
    }
}
