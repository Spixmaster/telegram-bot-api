#include "tgbot/types/Invoice.h"

#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(Invoice, normal)
{
    ASSERT_NO_THROW(tgbot::Invoice(tgbot::Invoice().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["currency"] = "hüsjdfg";
    doc["description"] = "oüijqfb";
    doc["start_parameter"] = "ohijsdfg";
    doc["title"] = "näpyojsdfg";
    doc["total_amount"] = -68518;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::Invoice(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::Invoice invoice;

        invoice.set_currency("hüsjdfg");
        ASSERT_EQ(invoice.get_currency(), "hüsjdfg");

        invoice.set_description("oüijqfb");
        ASSERT_EQ(invoice.get_description(), "oüijqfb");

        invoice.set_start_parameter("ohijsdfg");
        ASSERT_EQ(invoice.get_start_parameter(), "ohijsdfg");

        invoice.set_title("näpyojsdfg");
        ASSERT_EQ(invoice.get_title(), "näpyojsdfg");

        invoice.set_total_amount(-68518);
        ASSERT_EQ(invoice.get_total_amount(), -68518);

        ASSERT_EQ(invoice.serialise(), serialised_doc);
    }
}

TEST(Invoice, errors)
{
    ASSERT_THROW(tgbot::Invoice(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 5; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["currency"] = "qropijxcyb";
        }

        if(i != 1)
        {
            doc["description"] = "roijchw";
        }

        if(i != 2)
        {
            doc["start_parameter"] = "owihf";
        }

        if(i != 3)
        {
            doc["title"] = "aoisjgldfgs";
        }

        if(i != 4)
        {
            doc["total_amount"] = 97159;
        }

        ASSERT_THROW(tgbot::Invoice(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 5; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["currency"] = "djlhqg" : doc["currency"] = 4168;
        i != 1 ? doc["description"] = "owihf" : doc["description"] = 65;
        i != 2 ? doc["start_parameter"] = "opqgd" : doc["start_parameter"] = 486;
        i != 3 ? doc["title"] = "aoisjgldfgs" : doc["title"] = 286;
        i != 4 ? doc["total_amount"] = 7156 : doc["total_amount"] = "üoijf";

        ASSERT_THROW(tgbot::Invoice(doc.dump()), std::invalid_argument);
    }
}
