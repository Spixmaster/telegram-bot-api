#include "tgbot/types/PollOption.h"

#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(PollOption, normal)
{
    ASSERT_NO_THROW(tgbot::PollOption(tgbot::PollOption().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["text"] = "pqoiwemgldg";
    doc["voter_count"] = 684168;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::PollOption(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::PollOption poll_option;

        poll_option.set_text("pqoiwemgldg");
        ASSERT_EQ(poll_option.get_text(), "pqoiwemgldg");

        poll_option.set_voter_count(684168);
        ASSERT_EQ(poll_option.get_voter_count(), 684168);

        ASSERT_EQ(poll_option.serialise(), serialised_doc);
    }
}

TEST(PollOption, errors)
{
    ASSERT_THROW(tgbot::PollOption(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["text"] = "aoisjgldfgs";
        }

        if(i != 1)
        {
            doc["voter_count"] = 6844;
        }

        ASSERT_THROW(tgbot::PollOption(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["text"] = "owihf" : doc["text"] = 65;
        i != 1 ? doc["voter_count"] = 6844 : doc["voter_count"] = "sdfg";

        ASSERT_THROW(tgbot::PollOption(doc.dump()), std::invalid_argument);
    }
}
