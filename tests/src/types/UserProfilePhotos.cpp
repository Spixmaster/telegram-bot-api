#include "tgbot/types/UserProfilePhotos.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(UserProfilePhotos, normal)
{
    ASSERT_NO_THROW(tgbot::UserProfilePhotos(tgbot::UserProfilePhotos().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["photos"] = {{nlohmann::json::parse(tgbot::PhotoSize().serialise())}};
    doc["total_count"] = -7123654;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::UserProfilePhotos(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::UserProfilePhotos user_profile_photos;

        user_profile_photos.set_photos({{std::make_shared<tgbot::PhotoSize>()}});
        ASSERT_EQ(user_profile_photos.get_photos().at(0).at(0)->serialise(), tgbot::PhotoSize().serialise());

        user_profile_photos.set_total_count(-7123654);
        ASSERT_EQ(user_profile_photos.get_total_count(), -7123654);

        ASSERT_EQ(user_profile_photos.serialise(), serialised_doc);
    }
}

TEST(UserProfilePhotos, errors)
{
    ASSERT_THROW(tgbot::UserProfilePhotos(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["photos"] = {{nlohmann::json::parse(tgbot::PhotoSize().serialise())}};
        }

        if(i != 1)
        {
            doc["total_count"] = 84;
        }

        ASSERT_THROW(tgbot::UserProfilePhotos(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["photos"] = {{nlohmann::json::parse(tgbot::PhotoSize().serialise())}} : doc["photos"] = 468;
        i != 1 ? doc["total_count"] = 87 : doc["total_count"] = "herthsdf";

        ASSERT_THROW(tgbot::UserProfilePhotos(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["photos"] = {1462401};
        doc["total_count"] = 16871;

        ASSERT_THROW(tgbot::UserProfilePhotos(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["photos"] = {{46824}};
        doc["total_count"] = 84;

        ASSERT_THROW(tgbot::UserProfilePhotos(doc.dump()), std::invalid_argument);
    }
}
