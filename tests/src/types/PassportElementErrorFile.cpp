#include "tgbot/types/PassportElementErrorFile.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(PassportElementErrorFile, normal)
{
    ASSERT_NO_THROW(tgbot::PassportElementErrorFile(tgbot::PassportElementErrorFile().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["file_hash"] = "älkjfgs";
    doc["message"] = "qwefgbdf";
    doc["source"] = "zjpjyxc";
    doc["type"] = "mbndfg";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::PassportElementErrorFile(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::PassportElementErrorFile passport_element_error_file;

        passport_element_error_file.set_file_hash("älkjfgs");
        ASSERT_EQ(passport_element_error_file.get_file_hash(), "älkjfgs");

        passport_element_error_file.set_message("qwefgbdf");
        ASSERT_EQ(passport_element_error_file.get_message(), "qwefgbdf");

        passport_element_error_file.set_source("zjpjyxc");
        ASSERT_EQ(passport_element_error_file.get_source(), "zjpjyxc");

        passport_element_error_file.set_type("mbndfg");
        ASSERT_EQ(passport_element_error_file.get_type(), "mbndfg");

        ASSERT_EQ(passport_element_error_file.serialise(), serialised_doc);
    }
}

TEST(PassportElementErrorFile, errors)
{
    ASSERT_THROW(tgbot::PassportElementErrorFile(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["file_hash"] = "pobunfb";
        }

        if(i != 1)
        {
            doc["message"] = "yperoih";
        }

        if(i != 2)
        {
            doc["source"] = "aoisjgldfgs";
        }

        if(i != 3)
        {
            doc["type"] = "owihf";
        }

        ASSERT_THROW(tgbot::PassportElementErrorFile(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["file_hash"] = "qrwgosdv" : doc["file_hash"] = 9734;
        i != 1 ? doc["message"] = "qijds" : doc["message"] = 798;
        i != 2 ? doc["source"] = "aoisjgldfgs" : doc["source"] = 7951;
        i != 3 ? doc["type"] = "qeroijsv" : doc["type"] = 7168;

        ASSERT_THROW(tgbot::PassportElementErrorFile(doc.dump()), std::invalid_argument);
    }
}
