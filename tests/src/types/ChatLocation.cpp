#include "tgbot/types/ChatLocation.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(ChatLocation, normal)
{
    ASSERT_NO_THROW(tgbot::ChatLocation(tgbot::ChatLocation().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["address"] = "rewoinc";
    doc["location"] = nlohmann::json::parse(tgbot::Location().serialise());

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ChatLocation(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::ChatLocation chat_location;

        chat_location.set_address("rewoinc");
        ASSERT_EQ(chat_location.get_address(), "rewoinc");

        chat_location.set_location(std::make_shared<tgbot::Location>());
        ASSERT_EQ(chat_location.get_location()->serialise(), tgbot::Location().serialise());

        ASSERT_EQ(chat_location.serialise(), serialised_doc);
    }
}

TEST(ChatLocation, errors)
{
    ASSERT_THROW(tgbot::ChatLocation(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["address"] = "owihf";
        }

        if(i != 1)
        {
            doc["location"] = nlohmann::json::parse(tgbot::Location().serialise());
        }

        ASSERT_THROW(tgbot::ChatLocation(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["address"] = "owihf" : doc["address"] = 65;
        i != 1 ? doc["location"] = nlohmann::json::parse(tgbot::Location().serialise()) : doc["location"] = 286;

        ASSERT_THROW(tgbot::ChatLocation(doc.dump()), std::invalid_argument);
    }
}
