#include "tgbot/types/MaskPosition.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(MaskPosition, normal)
{
    ASSERT_NO_THROW(tgbot::MaskPosition(tgbot::MaskPosition().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["point"] = "gsprphoija";
    doc["scale"] = -17.681F;
    doc["x_shift"] = 7.185F;
    doc["y_shift"] = -574.4F;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::MaskPosition(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::MaskPosition mask_position;

        mask_position.set_point("gsprphoija");
        ASSERT_EQ(mask_position.get_point(), "gsprphoija");

        mask_position.set_scale(-17.681F);
        ASSERT_EQ(mask_position.get_scale(), -17.681F);

        mask_position.set_x_shift(7.185F);
        ASSERT_EQ(mask_position.get_x_shift(), 7.185F);

        mask_position.set_y_shift(-574.4F);
        ASSERT_EQ(mask_position.get_y_shift(), -574.4F);

        ASSERT_EQ(mask_position.serialise(), serialised_doc);
    }
}

TEST(MaskPosition, errors)
{
    ASSERT_THROW(tgbot::MaskPosition(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["point"] = "aoisjgldfgs";
        }

        if(i != 1)
        {
            doc["scale"] = 87.8;
        }

        if(i != 2)
        {
            doc["x_shift"] = 6816.4;
        }

        if(i != 3)
        {
            doc["y_shift"] = 6844.79;
        }

        ASSERT_THROW(tgbot::MaskPosition(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 4; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["point"] = "aoisjgldfgs" : doc["point"] = 286;
        i != 1 ? doc["scale"] = 87.17 : doc["scale"] = "eouasöf";
        i != 2 ? doc["x_shift"] = 684.789 : doc["x_shift"] = "cobw";
        i != 3 ? doc["y_shift"] = 6844.951 : doc["y_shift"] = "sdfg";

        ASSERT_THROW(tgbot::MaskPosition(doc.dump()), std::invalid_argument);
    }
}
