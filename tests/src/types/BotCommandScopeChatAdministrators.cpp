#include "tgbot/types/BotCommandScopeChatAdministrators.h"

#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(BotCommandScopeChatAdministrators, normal)
{
    ASSERT_NO_THROW(tgbot::BotCommandScopeChatAdministrators(tgbot::BotCommandScopeChatAdministrators().serialise()));

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i == 0 ? doc["chat_id"] = 18675 : doc["chat_id"] = "thofijpsiot";
        doc["type"] = "fcqopihfdg";

        const std::string serialised_doc = doc.dump();
        ASSERT_EQ(tgbot::BotCommandScopeChatAdministrators(serialised_doc).serialise(), serialised_doc);

        {
            tgbot::BotCommandScopeChatAdministrators bot_command_scope_chat_administrators;

            if(i == 0)
            {
                bot_command_scope_chat_administrators.set_chat_id(18675);
                ASSERT_EQ(std::get<std::int64_t>(bot_command_scope_chat_administrators.get_chat_id()), 18675);
            }
            else
            {
                bot_command_scope_chat_administrators.set_chat_id("thofijpsiot");
                ASSERT_EQ(std::get<std::string>(bot_command_scope_chat_administrators.get_chat_id()), "thofijpsiot");
            }

            bot_command_scope_chat_administrators.set_type("fcqopihfdg");
            ASSERT_EQ(bot_command_scope_chat_administrators.get_type(), "fcqopihfdg");

            ASSERT_EQ(bot_command_scope_chat_administrators.serialise(), serialised_doc);
        }
    }
}

TEST(BotCommandScopeChatAdministrators, errors)
{
    ASSERT_THROW(tgbot::BotCommandScopeChatAdministrators(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["chat_id"] = 9731;
        }

        if(i != 1)
        {
            doc["type"] = "daawe";
        }

        ASSERT_THROW(tgbot::BotCommandScopeChatAdministrators(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["chat_id"] = 15631 : doc["chat_id"] = true;
        i != 1 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;

        ASSERT_THROW(tgbot::BotCommandScopeChatAdministrators(doc.dump()), std::invalid_argument);
    }
}
