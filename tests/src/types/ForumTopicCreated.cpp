#include "tgbot/types/ForumTopicCreated.h"

#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(ForumTopicCreated, normal)
{
    ASSERT_NO_THROW(tgbot::ForumTopicCreated(tgbot::ForumTopicCreated().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["icon_color"] = 91654;
    doc["icon_custom_emoji_id"] = "thxapiu";
    doc["name"] = "saoihdb";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ForumTopicCreated(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::ForumTopicCreated forum_topic_created;

        forum_topic_created.set_icon_color(91654);
        ASSERT_EQ(forum_topic_created.get_icon_color(), 91654);

        forum_topic_created.set_icon_custom_emoji_id("thxapiu");
        ASSERT_EQ(forum_topic_created.get_icon_custom_emoji_id(), "thxapiu");

        forum_topic_created.set_name("saoihdb");
        ASSERT_EQ(forum_topic_created.get_name(), "saoihdb");

        ASSERT_EQ(forum_topic_created.serialise(), serialised_doc);
    }
}

TEST(ForumTopicCreated, errors)
{
    ASSERT_THROW(tgbot::ForumTopicCreated(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["icon_color"] = 837864;
        }

        if(i != 1)
        {
            doc["name"] = "üpminag";
        }

        ASSERT_THROW(tgbot::ForumTopicCreated(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["icon_color"] = 7924 : doc["icon_color"] = "ljcnkjbs";
        i != 1 ? doc["icon_custom_emoji_id"] = "npoanzoij" : doc["icon_custom_emoji_id"] = 468716;
        i != 2 ? doc["name"] = "hopijhwnx" : doc["name"] = 76167;

        ASSERT_THROW(tgbot::ForumTopicCreated(doc.dump()), std::invalid_argument);
    }
}
