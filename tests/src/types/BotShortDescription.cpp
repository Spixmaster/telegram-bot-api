#include "tgbot/types/BotShortDescription.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(BotShortDescription, normal)
{
    ASSERT_NO_THROW(tgbot::BotShortDescription(tgbot::BotShortDescription().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["short_description"] = "qcaüoijhwer";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::BotShortDescription(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::BotShortDescription bot_short_description;

        bot_short_description.set_short_description("qcaüoijhwer");
        ASSERT_EQ(bot_short_description.get_short_description(), "qcaüoijhwer");

        ASSERT_EQ(bot_short_description.serialise(), serialised_doc);
    }
}

TEST(BotShortDescription, errors)
{
    ASSERT_THROW(tgbot::BotShortDescription(nlohmann::json::array().dump()), std::invalid_argument);

    {
        const nlohmann::json doc = nlohmann::json::object();

        ASSERT_THROW(tgbot::BotShortDescription(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 1; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["short_description"] = "hcnösudfh" : doc["short_description"] = 71687;

        ASSERT_THROW(tgbot::BotShortDescription(doc.dump()), std::invalid_argument);
    }
}
