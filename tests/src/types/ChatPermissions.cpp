#include "tgbot/types/ChatPermissions.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(ChatPermissions, normal)
{
    ASSERT_NO_THROW(tgbot::ChatPermissions(tgbot::ChatPermissions().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["can_add_web_page_previews"] = true;
    doc["can_change_info"] = false;
    doc["can_invite_users"] = false;
    doc["can_manage_topics"] = true;
    doc["can_pin_messages"] = true;
    doc["can_send_audios"] = false;
    doc["can_send_documents"] = true;
    doc["can_send_messages"] = true;
    doc["can_send_other_messages"] = false;
    doc["can_send_photos"] = false;
    doc["can_send_polls"] = false;
    doc["can_send_video_notes"] = true;
    doc["can_send_videos"] = true;
    doc["can_send_voice_notes"] = false;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ChatPermissions(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::ChatPermissions chat_permissions;

        chat_permissions.set_can_add_web_page_previews(true);
        ASSERT_EQ(chat_permissions.get_can_add_web_page_previews(), true);

        chat_permissions.set_can_change_info(false);
        ASSERT_EQ(chat_permissions.get_can_change_info(), false);

        chat_permissions.set_can_invite_users(false);
        ASSERT_EQ(chat_permissions.get_can_invite_users(), false);

        chat_permissions.set_can_manage_topics(true);
        ASSERT_EQ(chat_permissions.get_can_manage_topics(), true);

        chat_permissions.set_can_pin_messages(true);
        ASSERT_EQ(chat_permissions.get_can_pin_messages(), true);

        chat_permissions.set_can_send_audios(false);
        ASSERT_EQ(chat_permissions.get_can_send_audios(), false);

        chat_permissions.set_can_send_documents(true);
        ASSERT_EQ(chat_permissions.get_can_send_documents(), true);

        chat_permissions.set_can_send_messages(true);
        ASSERT_EQ(chat_permissions.get_can_send_messages(), true);

        chat_permissions.set_can_send_other_messages(false);
        ASSERT_EQ(chat_permissions.get_can_send_other_messages(), false);

        chat_permissions.set_can_send_photos(false);
        ASSERT_EQ(chat_permissions.get_can_send_photos(), false);

        chat_permissions.set_can_send_polls(false);
        ASSERT_EQ(chat_permissions.get_can_send_polls(), false);

        chat_permissions.set_can_send_video_notes(true);
        ASSERT_EQ(chat_permissions.get_can_send_video_notes(), true);

        chat_permissions.set_can_send_videos(true);
        ASSERT_EQ(chat_permissions.get_can_send_videos(), true);

        chat_permissions.set_can_send_voice_notes(false);
        ASSERT_EQ(chat_permissions.get_can_send_voice_notes(), false);

        ASSERT_EQ(chat_permissions.serialise(), serialised_doc);
    }
}

TEST(ChatPermissions, errors)
{
    ASSERT_THROW(tgbot::ChatPermissions(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 14; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["can_add_web_page_previews"] = false : doc["can_add_web_page_previews"] = 5138;
        i != 1 ? doc["can_change_info"] = false : doc["can_change_info"] = 81987;
        i != 2 ? doc["can_invite_users"] = false : doc["can_invite_users"] = 79165;
        i != 3 ? doc["can_manage_topics"] = true : doc["can_manage_topics"] = 71235;
        i != 4 ? doc["can_pin_messages"] = true : doc["can_pin_messages"] = 5654516;
        i != 5 ? doc["can_send_audios"] = false : doc["can_send_audios"] = 1951;
        i != 6 ? doc["can_send_documents"] = true : doc["can_send_documents"] = 4786145;
        i != 7 ? doc["can_send_messages"] = true : doc["can_send_messages"] = 15981;
        i != 8 ? doc["can_send_other_messages"] = false : doc["can_send_other_messages"] = 681354;
        i != 9 ? doc["can_send_photos"] = true : doc["can_send_photos"] = 871657;
        i != 10 ? doc["can_send_polls"] = false : doc["can_send_polls"] = 571682465;
        i != 11 ? doc["can_send_video_notes"] = false : doc["can_send_video_notes"] = 789515;
        i != 12 ? doc["can_send_videos"] = true : doc["can_send_videos"] = 579861;
        i != 13 ? doc["can_send_voice_notes"] = false : doc["can_send_voice_notes"] = 46821;

        ASSERT_THROW(tgbot::ChatPermissions(doc.dump()), std::invalid_argument);
    }
}
