#include "tgbot/types/VideoChatParticipantsInvited.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(VideoChatParticipantsInvited, normal)
{
    ASSERT_NO_THROW(tgbot::VideoChatParticipantsInvited(tgbot::VideoChatParticipantsInvited().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["users"] = {nlohmann::json::parse(tgbot::User().serialise())};

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::VideoChatParticipantsInvited(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::VideoChatParticipantsInvited video_chat_participants_invited;

        video_chat_participants_invited.set_users({std::make_shared<tgbot::User>()});
        ASSERT_EQ(video_chat_participants_invited.get_users().at(0)->serialise(), tgbot::User().serialise());

        ASSERT_EQ(video_chat_participants_invited.serialise(), serialised_doc);
    }
}

TEST(VideoChatParticipantsInvited, errors)
{
    ASSERT_THROW(tgbot::VideoChatParticipantsInvited(nlohmann::json::array().dump()), std::invalid_argument);

    {
        const nlohmann::json doc = nlohmann::json::object();

        ASSERT_THROW(tgbot::VideoChatParticipantsInvited(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 1; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["users"] = {nlohmann::json::parse(tgbot::User().serialise())} : doc["users"] = 468;

        ASSERT_THROW(tgbot::VideoChatParticipantsInvited(doc.dump()), std::invalid_argument);
    }

    {
        nlohmann::json doc = nlohmann::json::object();
        doc["users"] = {162451};

        ASSERT_THROW(tgbot::VideoChatParticipantsInvited(doc.dump()), std::invalid_argument);
    }
}
