#include "tgbot/types/MessageEntity.h"

#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(MessageEntity, normal)
{
    ASSERT_NO_THROW(tgbot::MessageEntity(tgbot::MessageEntity().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["custom_emoji"] = "wcxopihnwet";
    doc["language"] = "toöhijsoidfg";
    doc["length"] = 6818582;
    doc["offset"] = -981621;
    doc["type"] = "sdfhdf";
    doc["url"] = "öoijosdfg";
    doc["user"] = nlohmann::json::parse(tgbot::User().serialise());

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::MessageEntity(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::MessageEntity message_entity;

        message_entity.set_custom_emoji("wcxopihnwet");
        ASSERT_EQ(message_entity.get_custom_emoji(), "wcxopihnwet");

        message_entity.set_language("toöhijsoidfg");
        ASSERT_EQ(message_entity.get_language(), "toöhijsoidfg");

        message_entity.set_length(6818582);
        ASSERT_EQ(message_entity.get_length(), 6818582);

        message_entity.set_offset(-981621);
        ASSERT_EQ(message_entity.get_offset(), -981621);

        message_entity.set_type("sdfhdf");
        ASSERT_EQ(message_entity.get_type(), "sdfhdf");

        message_entity.set_url("öoijosdfg");
        ASSERT_EQ(message_entity.get_url(), "öoijosdfg");

        message_entity.set_user(std::make_shared<tgbot::User>());

        if(message_entity.get_user().has_value())
        {
            ASSERT_EQ(message_entity.get_user().value()->serialise(), tgbot::User().serialise());
        }

        ASSERT_EQ(message_entity.serialise(), serialised_doc);
    }
}

TEST(MessageEntity, errors)
{
    ASSERT_THROW(tgbot::MessageEntity(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["length"] = 6844;
        }

        if(i != 1)
        {
            doc["offset"] = 941;
        }

        if(i != 2)
        {
            doc["type"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::MessageEntity(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 7; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["custom_emoji"] = "rwmzposdf" : doc["custom_emoji"] = 579;
        i != 1 ? doc["language"] = "woöinefmoib" : doc["language"] = 616842;
        i != 2 ? doc["length"] = 87 : doc["length"] = "herthsdf";
        i != 3 ? doc["offset"] = 6844 : doc["offset"] = "sdfg";
        i != 4 ? doc["type"] = "aoisjgldfgs" : doc["type"] = 286;
        i != 5 ? doc["url"] = "wuefhbnjdfb" : doc["url"] = 651;
        i != 6 ? doc["user"] = nlohmann::json::parse(tgbot::User().serialise()) : doc["user"] = 468;

        ASSERT_THROW(tgbot::MessageEntity(doc.dump()), std::invalid_argument);
    }
}
