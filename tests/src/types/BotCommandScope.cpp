#include "tgbot/types/BotCommandScope.h"

#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(BotCommandScope, normal)
{
    ASSERT_NO_THROW(tgbot::BotCommandScope(tgbot::BotCommandScope().serialise()));

    const nlohmann::json doc = nlohmann::json::object();

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::BotCommandScope().serialise(), serialised_doc);

    {
        const tgbot::BotCommandScope bot_command_scope;

        ASSERT_EQ(bot_command_scope.serialise(), serialised_doc);
    }
}

TEST(BotCommandScope, errors)
{
    ASSERT_THROW(tgbot::BotCommandScope(nlohmann::json::array().dump()), std::invalid_argument);
}
