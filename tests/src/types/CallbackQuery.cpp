#include "tgbot/types/CallbackQuery.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(CallbackQuery, normal)
{
    ASSERT_NO_THROW(tgbot::CallbackQuery(tgbot::CallbackQuery().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["chat_instance"] = "fwübiojm";
    doc["data"] = "füqijmcb";
    doc["from"] = nlohmann::json::parse(tgbot::User().serialise());
    doc["game_short_name"] = "sobvpijmpsoic";
    doc["id"] = "frgnvb";
    doc["inline_message_id"] = "sdfbscv";
    doc["message"] = nlohmann::json::parse(tgbot::Message().serialise());

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::CallbackQuery(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::CallbackQuery callback_query;

        callback_query.set_chat_instance("fwübiojm");
        ASSERT_EQ(callback_query.get_chat_instance(), "fwübiojm");

        callback_query.set_data("füqijmcb");
        ASSERT_EQ(callback_query.get_data(), "füqijmcb");

        callback_query.set_from(std::make_shared<tgbot::User>());
        ASSERT_EQ(callback_query.get_from()->serialise(), tgbot::User().serialise());

        callback_query.set_game_short_name("sobvpijmpsoic");
        ASSERT_EQ(callback_query.get_game_short_name(), "sobvpijmpsoic");

        callback_query.set_id("frgnvb");
        ASSERT_EQ(callback_query.get_id(), "frgnvb");

        callback_query.set_inline_message_id("sdfbscv");
        ASSERT_EQ(callback_query.get_inline_message_id(), "sdfbscv");

        callback_query.set_message(std::make_shared<tgbot::Message>());

        if(callback_query.get_message().has_value())
        {
            ASSERT_EQ(callback_query.get_message().value()->serialise(), tgbot::Message().serialise());
        }

        ASSERT_EQ(callback_query.serialise(), serialised_doc);
    }
}

TEST(CallbackQuery, errors)
{
    ASSERT_THROW(tgbot::CallbackQuery(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 3; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["chat_instance"] = "owihf";
        }

        if(i != 1)
        {
            doc["from"] = nlohmann::json::parse(tgbot::User().serialise());
        }

        if(i != 2)
        {
            doc["id"] = "aoisjgldfgs";
        }

        ASSERT_THROW(tgbot::CallbackQuery(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 7; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["chat_instance"] = "jwfhth" : doc["chat_instance"] = 674;
        i != 1 ? doc["data"] = "wüoihfh" : doc["data"] = 35;
        i != 2 ? doc["from"] = nlohmann::json::parse(tgbot::User().serialise()) : doc["from"] = 65;
        i != 3 ? doc["game_short_name"] = "wefhe" : doc["game_short_name"] = 167;
        i != 4 ? doc["id"] = "aoisjgldfgs" : doc["id"] = 286;
        i != 5 ? doc["inline_message_id"] = "pgimpwoefb" : doc["inline_message_id"] = 674;
        i != 6 ? doc["message"] = nlohmann::json::parse(tgbot::Message().serialise()) : doc["message"] = 27;

        ASSERT_THROW(tgbot::CallbackQuery(doc.dump()), std::invalid_argument);
    }
}
