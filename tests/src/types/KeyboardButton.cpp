#include "tgbot/types/KeyboardButton.h"

#include <cstdint>
#include <initializer_list>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>
#include <vector>

#include "gtest/gtest.h"

TEST(KeyboardButton, normal)
{
    ASSERT_NO_THROW(tgbot::KeyboardButton(tgbot::KeyboardButton().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["request_chat"] = nlohmann::json::parse(tgbot::KeyboardButtonRequestChat().serialise());
    doc["request_contact"] = false;
    doc["request_location"] = true;
    doc["request_poll"] = nlohmann::json::parse(tgbot::KeyboardButtonPollType().serialise());
    doc["request_user"] = nlohmann::json::parse(tgbot::KeyboardButtonRequestUser().serialise());
    doc["text"] = "hösoijdfg";
    doc["web_app"] = nlohmann::json::parse(tgbot::WebAppInfo().serialise());

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::KeyboardButton(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::KeyboardButton keyboard_button;

        keyboard_button.set_request_chat(std::make_shared<tgbot::KeyboardButtonRequestChat>());

        if(keyboard_button.get_request_chat().has_value())
        {
            ASSERT_EQ(
              keyboard_button.get_request_chat().value()->serialise(), tgbot::KeyboardButtonRequestChat().serialise());
        }

        keyboard_button.set_request_contact(false);
        ASSERT_EQ(keyboard_button.get_request_contact(), false);

        keyboard_button.set_request_location(true);
        ASSERT_EQ(keyboard_button.get_request_location(), true);

        keyboard_button.set_request_poll(std::make_shared<tgbot::KeyboardButtonPollType>());

        if(keyboard_button.get_request_poll().has_value())
        {
            ASSERT_EQ(
              keyboard_button.get_request_poll().value()->serialise(), tgbot::KeyboardButtonPollType().serialise());
        }

        keyboard_button.set_request_user(std::make_shared<tgbot::KeyboardButtonRequestUser>());

        if(keyboard_button.get_request_user().has_value())
        {
            ASSERT_EQ(
              keyboard_button.get_request_user().value()->serialise(), tgbot::KeyboardButtonRequestUser().serialise());
        }

        keyboard_button.set_text("hösoijdfg");
        ASSERT_EQ(keyboard_button.get_text(), "hösoijdfg");

        keyboard_button.set_web_app(std::make_shared<tgbot::WebAppInfo>());

        if(keyboard_button.get_web_app().has_value())
        {
            ASSERT_EQ(keyboard_button.get_web_app().value()->serialise(), tgbot::WebAppInfo().serialise());
        }

        ASSERT_EQ(keyboard_button.serialise(), serialised_doc);
    }
}

TEST(KeyboardButton, errors)
{
    ASSERT_THROW(tgbot::KeyboardButton(nlohmann::json::array().dump()), std::invalid_argument);

    {
        const nlohmann::json doc = nlohmann::json::object();

        ASSERT_THROW(tgbot::KeyboardButton(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 7; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ?
          doc["request_chat"] = nlohmann::json::parse(tgbot::KeyboardButtonRequestChat().serialise()) :
          doc["request_chat"] = 967123;
        i != 1 ? doc["request_contact"] = false : doc["request_contact"] = 7618784;
        i != 2 ? doc["request_location"] = false : doc["request_location"] = 8611;
        i != 3 ?
          doc["request_poll"] = nlohmann::json::parse(tgbot::KeyboardButtonPollType().serialise()) :
          doc["request_poll"] = 71684;
        i != 4 ?
          doc["request_user"] = nlohmann::json::parse(tgbot::KeyboardButtonRequestUser().serialise()) :
          doc["request_user"] = 168751;
        i != 5 ? doc["text"] = "aoisjgldfgs" : doc["text"] = 286;
        i != 6 ? doc["web_app"] = nlohmann::json::parse(tgbot::WebAppInfo().serialise()) : doc["web_app"] = 8592687;

        ASSERT_THROW(tgbot::KeyboardButton(doc.dump()), std::invalid_argument);
    }
}
