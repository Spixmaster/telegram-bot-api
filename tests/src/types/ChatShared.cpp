#include "tgbot/types/ChatShared.h"

#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(ChatShared, normal)
{
    ASSERT_NO_THROW(tgbot::ChatShared(tgbot::ChatShared().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["chat_id"] = 5678135;
    doc["request_id"] = 116876;

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ChatShared(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::ChatShared chat_shared;

        chat_shared.set_chat_id(5678135);
        ASSERT_EQ(chat_shared.get_chat_id(), 5678135);

        chat_shared.set_request_id(116876);
        ASSERT_EQ(chat_shared.get_request_id(), 116876);

        ASSERT_EQ(chat_shared.serialise(), serialised_doc);
    }
}

TEST(ChatShared, errors)
{
    ASSERT_THROW(tgbot::ChatShared(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();

        if(i != 0)
        {
            doc["chat_id"] = 687515;
        }

        if(i != 1)
        {
            doc["request_id"] = 12387156;
        }

        ASSERT_THROW(tgbot::ChatShared(doc.dump()), std::invalid_argument);
    }

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["chat_id"] = 1378656 : doc["chat_id"] = "ujmosudb";
        i != 1 ? doc["request_id"] = 715346 : doc["request_id"] = "ewxoüaijr";

        ASSERT_THROW(tgbot::ChatShared(doc.dump()), std::invalid_argument);
    }
}
