#include "tgbot/types/ForumTopicEdited.h"

#include <cstdint>
#include <map>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <stdexcept>

#include "gtest/gtest.h"

TEST(ForumTopicEdited, normal)
{
    ASSERT_NO_THROW(tgbot::ForumTopicEdited(tgbot::ForumTopicEdited().serialise()));

    nlohmann::json doc = nlohmann::json::object();
    doc["icon_custom_emoji_id"] = "hxüpqepoh";
    doc["name"] = "thpoimcw";

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::ForumTopicEdited(serialised_doc).serialise(), serialised_doc);

    {
        tgbot::ForumTopicEdited forum_topic_edited;

        forum_topic_edited.set_icon_custom_emoji_id("hxüpqepoh");
        ASSERT_EQ(forum_topic_edited.get_icon_custom_emoji_id(), "hxüpqepoh");

        forum_topic_edited.set_name("thpoimcw");
        ASSERT_EQ(forum_topic_edited.get_name(), "thpoimcw");

        ASSERT_EQ(forum_topic_edited.serialise(), serialised_doc);
    }
}

TEST(ForumTopicEdited, errors)
{
    ASSERT_THROW(tgbot::ForumTopicEdited(nlohmann::json::array().dump()), std::invalid_argument);

    for(std::int8_t i = 0; i < 2; ++i)
    {
        nlohmann::json doc = nlohmann::json::object();
        i != 0 ? doc["icon_custom_emoji_id"] = "hxqpoinr" : doc["icon_custom_emoji_id"] = 7561;
        i != 1 ? doc["name"] = "ohdq" : doc["name"] = 76254;

        ASSERT_THROW(tgbot::ForumTopicEdited(doc.dump()), std::invalid_argument);
    }
}
