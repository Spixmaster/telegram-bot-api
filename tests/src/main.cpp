#define CPPHTTPLIB_OPENSSL_SUPPORT
#define CPPHTTPLIB_ZLIB_SUPPORT

#include <cstdint>
#include <filesystem>
#include <gtest/gtest.h>
#include <httplib.h>
#include <initializer_list>
#include <istream>
#include <iterator>
#include <map>
#include <memory>
#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <string>
#include <thread>
#include <vector>

#include "tgbot/types/BotCommand.h"
#include "tgbot/types/BotCommandScope.h"
#include "tgbot/types/BotDescription.h"
#include "tgbot/types/BotShortDescription.h"
#include "tgbot/types/Chat.h"
#include "tgbot/types/ChatAdministratorRights.h"
#include "tgbot/types/ChatInviteLink.h"
#include "tgbot/types/ChatMemberAdministrator.h"
#include "tgbot/types/ChatMemberBanned.h"
#include "tgbot/types/ChatMemberLeft.h"
#include "tgbot/types/ChatMemberMember.h"
#include "tgbot/types/ChatMemberOwner.h"
#include "tgbot/types/ChatMemberRestricted.h"
#include "tgbot/types/ChatPermissions.h"
#include "tgbot/types/File.h"
#include "tgbot/types/ForumTopic.h"
#include "tgbot/types/GameHighScore.h"
#include "tgbot/types/InlineKeyboardMarkup.h"
#include "tgbot/types/InlineQueryResult.h"
#include "tgbot/types/InputMediaAnimation.h"
#include "tgbot/types/InputMediaAudio.h"
#include "tgbot/types/InputMediaDocument.h"
#include "tgbot/types/InputMediaPhoto.h"
#include "tgbot/types/InputMediaVideo.h"
#include "tgbot/types/InputSticker.h"
#include "tgbot/types/LabeledPrice.h"
#include "tgbot/types/MaskPosition.h"
#include "tgbot/types/MenuButton.h"
#include "tgbot/types/Message.h"
#include "tgbot/types/MessageEntity.h"
#include "tgbot/types/PassportElementError.h"
#include "tgbot/types/Poll.h"
#include "tgbot/types/Reply.h"
#include "tgbot/types/SentWebAppMessage.h"
#include "tgbot/types/ShippingOption.h"
#include "tgbot/types/Sticker.h"
#include "tgbot/types/StickerSet.h"
#include "tgbot/types/Update.h"
#include "tgbot/types/User.h"
#include "tgbot/types/UserProfilePhotos.h"
#include "tgbot/types/WebhookInfo.h"

std::int32_t main(std::int32_t argc, char **argv)
{
    /*
     * HTTP responses for value "test"
     * "0": Not a JSON object
     * "1": No key "result"
     * "2": Key "result" wrong type
     */
    httplib::SSLServer server("../ssl-certificates/cert.pem", "../ssl-certificates/cert.key");
    const std::string test_file = "/tmp/test.txt";

    server.Post(
      "/bottoken/addStickerToSet",
      [&test_file](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              const tgbot::InputSticker input_sticker1;

              tgbot::InputSticker input_sticker2;
              input_sticker2.set_sticker("attach://" + std::filesystem::path(test_file).string());

              std::string upload_data;

              if(std::filesystem::is_regular_file(request.get_file_value(test_file).filename))
              {
                  std::ifstream inf(
                    std::filesystem::path(request.get_file_value(test_file).filename).string(), std::ios::binary);
                  upload_data = std::string(std::istreambuf_iterator<char>(inf), {});
              }

              if(! request.has_file("test") && request.get_file_value("user_id").content == "1788415" &&
                 request.get_file_value("name").content == "füpcmiowfh" &&
                 (request.get_file_value("sticker").content ==
                    nlohmann::json::parse(input_sticker1.serialise()).dump() ||
                  (request.get_file_value("sticker").content ==
                     nlohmann::json::parse(input_sticker2.serialise()).dump() &&
                   request.get_file_value(test_file).content == upload_data)))
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/answerCallbackQuery",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("callback_query_id").content == "cpoijwfh" &&
                 request.get_file_value("text").content == "howlkcmh" &&
                 request.get_file_value("show_alert").content == "1" &&
                 request.get_file_value("url").content == "tpoimwlfg" &&
                 request.get_file_value("cache_time").content == "7651489")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/answerInlineQuery",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") &&
                 request.get_file_value("inline_query_id").content == "pfkcnjwoiufhmkfg" &&
                 request.get_file_value("results").content ==
                   nlohmann::json::array({nlohmann::json::parse(tgbot::InlineQueryResult().serialise())}).dump() &&
                 request.get_file_value("cache_time").content == "7651" &&
                 request.get_file_value("is_personal").content == "1" &&
                 request.get_file_value("next_offset").content == "dlfhjmnbvc" &&
                 request.get_file_value("switch_pm_text").content == "öldjhjvmfwb" &&
                 request.get_file_value("switch_pm_parameter").content == "wicnhwojfg")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/answerPreCheckoutQuery",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("pre_checkout_query_id").content == "ufwbmlkfg" &&
                 request.get_file_value("ok").content == "1" &&
                 request.get_file_value("error_message").content == "ösölmfhr")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/answerShippingQuery",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("shipping_query_id").content == "tofölmkwjfdg" &&
                 request.get_file_value("ok").content == "1" &&
                 request.get_file_value("shipping_options").content ==
                   nlohmann::json::array({nlohmann::json::parse(tgbot::ShippingOption().serialise())}).dump() &&
                 request.get_file_value("error_message").content == "kwlmblmsf")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/answerWebAppQuery",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("web_app_query_id").content == "hljasdth" &&
                 request.get_file_value("result").content == tgbot::InlineQueryResult().serialise())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::SentWebAppMessage().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/approveChatJoinRequest",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "7315" &&
                 request.get_file_value("user_id").content == "4164284")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/banChatMember",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "15316" &&
                 request.get_file_value("user_id").content == "681435" &&
                 request.get_file_value("until_date").content == "681534" &&
                 request.get_file_value("revoke_messages").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/banChatSenderChat",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "715617" &&
                 request.get_file_value("sender_chat_id").content == "282546")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Get(
      "/bottoken/close",
      [](const httplib::Request &, httplib::Response &response)
      {
          nlohmann::json doc = nlohmann::json::object();
          doc["result"] = true;

          response.status = 200;
          response.set_content(doc.dump(), "application/json");
      });

    server.Post(
      "/bottoken/close",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/closeForumTopic",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "8168762" &&
                 request.get_file_value("message_thread_id").content == "4351687")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/closeGeneralForumTopic",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "168476")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/copyMessage",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "688261" &&
                 request.get_file_value("message_thread_id").content == "816421" &&
                 request.get_file_value("from_chat_id").content == "146842614" &&
                 request.get_file_value("message_id").content == "16546214" &&
                 request.get_file_value("caption").content == "söldfmbjsdf" &&
                 request.get_file_value("parse_mode").content == "söfgsdfg" &&
                 request.get_file_value("caption_entities").content ==
                   nlohmann::json::array({nlohmann::json::parse(tgbot::MessageEntity().serialise())}).dump() &&
                 request.get_file_value("disable_notification").content == "0" &&
                 request.get_file_value("protect_content").content == "0" &&
                 request.get_file_value("reply_to_message_id").content == "414620" &&
                 request.get_file_value("allow_sending_without_reply").content == "1" &&
                 request.get_file_value("reply_markup").content == tgbot::Reply().serialise())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Message().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/createChatInviteLink",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "8312015" &&
                 request.get_file_value("name").content == "fhjpoicjw" &&
                 request.get_file_value("expire_date").content == "618435" &&
                 request.get_file_value("member_limit").content == "812461" &&
                 request.get_file_value("creates_join_request").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::ChatInviteLink().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/createForumTopic",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "52384" &&
                 request.get_file_value("name").content == "uoinawdg" &&
                 request.get_file_value("icon_color").content == "7354984" &&
                 request.get_file_value("icon_custom_emoji_id").content == "esopihwer")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::ForumTopic().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/createInvoiceLink",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(
                ! request.has_file("test") && request.get_file_value("title").content == "zopxijar" &&
                request.get_file_value("description").content == "hpoiascv" &&
                request.get_file_value("payload").content == "thpuhcasg" &&
                request.get_file_value("provider_token").content == "thpoikjnsfeh" &&
                request.get_file_value("currency").content == "sdjrtn" &&
                request.get_file_value("prices").content ==
                  nlohmann::json::array({nlohmann::json::parse(tgbot::LabeledPrice().serialise())}).dump() &&
                request.get_file_value("max_tip_amount").content == "3678" &&
                request.get_file_value("suggested_tip_amounts").content == nlohmann::json::array({23573}).dump() &&
                request.get_file_value("provider_data").content == "rhonjvs" &&
                request.get_file_value("photo_url").content == "cxnosodj" &&
                request.get_file_value("photo_size").content == "1684531" &&
                request.get_file_value("photo_width").content == "38151" &&
                request.get_file_value("photo_height").content == "7681354" &&
                request.get_file_value("need_name").content == "0" &&
                request.get_file_value("need_phone_number").content == "1" &&
                request.get_file_value("need_email").content == "0" &&
                request.get_file_value("need_shipping_address").content == "0" &&
                request.get_file_value("send_phone_number_to_provider").content == "1" &&
                request.get_file_value("send_email_to_provider").content == "1" &&
                request.get_file_value("is_flexible").content == "0")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = "";

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/createNewStickerSet",
      [&test_file](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              const tgbot::InputSticker input_sticker1;

              tgbot::InputSticker input_sticker2;
              input_sticker2.set_sticker("attach://" + std::filesystem::path(test_file).string());

              std::string upload_data;

              if(std::filesystem::is_regular_file(request.get_file_value(test_file).filename))
              {
                  std::ifstream inf(
                    std::filesystem::path(request.get_file_value(test_file).filename).string(), std::ios::binary);
                  upload_data = std::string(std::istreambuf_iterator<char>(inf), {});
              }

              if(! request.has_file("test") && request.get_file_value("user_id").content == "17684" &&
                 request.get_file_value("name").content == "irhwomlkjf" &&
                 request.get_file_value("title").content == "fjmlqd" &&
                 (request.get_file_value("stickers").content ==
                    nlohmann::json::array({nlohmann::json::parse(input_sticker1.serialise())}).dump() ||
                  (request.get_file_value("stickers").content ==
                     nlohmann::json::array({nlohmann::json::parse(input_sticker2.serialise())}).dump() &&
                   request.get_file_value(test_file).content == upload_data)) &&
                 request.get_file_value("sticker_format").content == "cpoqmöfg" &&
                 request.get_file_value("sticker_type").content == "ixpuhwhe" &&
                 request.get_file_value("needs_repainting").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/declineChatJoinRequest",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "57234" &&
                 request.get_file_value("user_id").content == "96347")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/deleteChatPhoto",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "954146")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/deleteChatStickerSet",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "17985614")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/deleteForumTopic",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "19357" &&
                 request.get_file_value("message_thread_id").content == "945374")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/deleteMessage",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "7194156" &&
                 request.get_file_value("message_id").content == "7949514")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/deleteMyCommands",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") &&
                 request.get_file_value("scope").content == tgbot::BotCommandScope().serialise() &&
                 request.get_file_value("language_code").content == "sfhjpijh")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/deleteStickerFromSet",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("sticker").content == "iwoicojowmfdhjoi")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/deleteStickerSet",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("name").content == "düioeruoüasnd")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Get(
      "/bottoken/deleteWebhook",
      [](const httplib::Request &, httplib::Response &response)
      {
          nlohmann::json doc = nlohmann::json::object();
          doc["result"] = true;

          response.status = 200;
          response.set_content(doc.dump(), "application/json");
      });

    server.Post(
      "/bottoken/deleteWebhook",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("drop_pending_updates").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/editChatInviteLink",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "61181811" &&
                 request.get_file_value("invite_link").content == "djöfwgkljc" &&
                 request.get_file_value("name").content == "rhpoijv" &&
                 request.get_file_value("expire_date").content == "4814354" &&
                 request.get_file_value("member_limit").content == "89614" &&
                 request.get_file_value("creates_join_request").content == "0")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::ChatInviteLink().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/editForumTopic",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "71357" &&
                 request.get_file_value("message_thread_id").content == "15946812" &&
                 request.get_file_value("name").content == "ciuwhesvhjerpu" &&
                 request.get_file_value("icon_custom_emoji_id").content == "aspduetdvs")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/editGeneralForumTopic",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "91732156" &&
                 request.get_file_value("name").content == "dioapoxmpierh")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/editMessageCaption",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "816354" &&
                 request.get_file_value("message_id").content == "79925156" &&
                 request.get_file_value("inline_message_id").content == "powlmkjlf" &&
                 request.get_file_value("caption").content == "poicmkqölkjh" &&
                 request.get_file_value("parse_mode").content == "jmhpojiwcg" &&
                 request.get_file_value("caption_entities").content ==
                   nlohmann::json::array({nlohmann::json::parse(tgbot::MessageEntity().serialise())}).dump() &&
                 request.get_file_value("reply_markup").content == tgbot::InlineKeyboardMarkup().serialise())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "3")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Message().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/editMessageLiveLocation",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "17831531" &&
                 request.get_file_value("message_id").content == "781435324" &&
                 request.get_file_value("inline_message_id").content == "cimjwklfdhw" &&
                 std::to_string(std::stoi(request.get_file_value("latitude").content)) == "681745" &&
                 std::to_string(std::stoi(request.get_file_value("longitude").content)) == "68453" &&
                 std::to_string(std::stoi(request.get_file_value("horizontal_accuracy").content)) == "38145" &&
                 request.get_file_value("heading").content == "678145" &&
                 request.get_file_value("proximity_alert_radius").content == "61832" &&
                 request.get_file_value("reply_markup").content == tgbot::InlineKeyboardMarkup().serialise())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "3")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Message().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/editMessageMedia",
      [&test_file](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              const tgbot::InputMediaAnimation input_media_animation1;

              tgbot::InputMediaAnimation input_media_animation2;
              input_media_animation2.set_media("attach://" + std::filesystem::path(test_file).string());
              input_media_animation2.set_thumbnail("attach://" + std::filesystem::path(test_file).string());

              const tgbot::InputMediaAudio input_media_audio1;

              tgbot::InputMediaAudio input_media_audio2;
              input_media_audio2.set_media("attach://" + std::filesystem::path(test_file).string());
              input_media_audio2.set_thumbnail("attach://" + std::filesystem::path(test_file).string());

              const tgbot::InputMediaDocument input_media_document1;

              tgbot::InputMediaDocument input_media_document2;
              input_media_document2.set_media("attach://" + std::filesystem::path(test_file).string());
              input_media_document2.set_thumbnail("attach://" + std::filesystem::path(test_file).string());

              const tgbot::InputMediaPhoto input_media_photo1;

              tgbot::InputMediaPhoto input_media_photo2;
              input_media_photo2.set_media("attach://" + std::filesystem::path(test_file).string());

              const tgbot::InputMediaVideo input_media_video1;

              tgbot::InputMediaVideo input_media_video2;
              input_media_video2.set_media("attach://" + std::filesystem::path(test_file).string());
              input_media_video2.set_thumbnail("attach://" + std::filesystem::path(test_file).string());

              std::string upload_data;

              if(std::filesystem::is_regular_file(request.get_file_value(test_file).filename))
              {
                  std::ifstream inf(
                    std::filesystem::path(request.get_file_value(test_file).filename).string(), std::ios::binary);
                  upload_data = std::string(std::istreambuf_iterator<char>(inf), {});
              }

              if(
                ! request.has_file("test") && request.get_file_value("chat_id").content == "8135354" &&
                request.get_file_value("message_id").content == "7922146" &&
                request.get_file_value("inline_message_id").content == "opijtwhmof" &&
                (request.get_file_value("media").content ==
                   nlohmann::json::parse(input_media_animation1.serialise()).dump() ||
                 (request.get_file_value("media").content ==
                    nlohmann::json::parse(input_media_animation2.serialise()).dump() &&
                  request.get_file_value(test_file).content == upload_data) ||
                 request.get_file_value("media").content ==
                   nlohmann::json::parse(input_media_audio1.serialise()).dump() ||
                 (request.get_file_value("media").content ==
                    nlohmann::json::parse(input_media_audio2.serialise()).dump() &&
                  request.get_file_value(test_file).content == upload_data) ||
                 request.get_file_value("media").content ==
                   nlohmann::json::parse(input_media_document1.serialise()).dump() ||
                 (request.get_file_value("media").content ==
                    nlohmann::json::parse(input_media_document2.serialise()).dump() &&
                  request.get_file_value(test_file).content == upload_data) ||
                 request.get_file_value("media").content ==
                   nlohmann::json::parse(input_media_photo1.serialise()).dump() ||
                 (request.get_file_value("media").content ==
                    nlohmann::json::parse(input_media_photo2.serialise()).dump() &&
                  request.get_file_value(test_file).content == upload_data) ||
                 request.get_file_value("media").content ==
                   nlohmann::json::parse(input_media_video1.serialise()).dump() ||
                 (request.get_file_value("media").content ==
                    nlohmann::json::parse(input_media_video2.serialise()).dump() &&
                  request.get_file_value(test_file).content == upload_data)) &&
                request.get_file_value("reply_markup").content == tgbot::InlineKeyboardMarkup().serialise())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "3")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Message().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/editMessageReplyMarkup",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "81546" &&
                 request.get_file_value("message_id").content == "14682" &&
                 request.get_file_value("inline_message_id").content == "powkjmfhj" &&
                 request.get_file_value("reply_markup").content == tgbot::InlineKeyboardMarkup().serialise())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "3")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Message().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/editMessageText",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "8413124" &&
                 request.get_file_value("message_id").content == "7951354" &&
                 request.get_file_value("inline_message_id").content == "doipqmhf" &&
                 request.get_file_value("text").content == "pwlnilcg" &&
                 request.get_file_value("parse_mode").content == "poicnqh" &&
                 request.get_file_value("entities").content ==
                   nlohmann::json::array({nlohmann::json::parse(tgbot::MessageEntity().serialise())}).dump() &&
                 request.get_file_value("disable_web_page_preview").content == "1" &&
                 request.get_file_value("reply_markup").content == tgbot::InlineKeyboardMarkup().serialise())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "3")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Message().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/exportChatInviteLink",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "4941331")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = "";

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/forwardMessage",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "6814624" &&
                 request.get_file_value("from_chat_id").content == "416428" &&
                 request.get_file_value("disable_notification").content == "0" &&
                 request.get_file_value("protect_content").content == "1" &&
                 request.get_file_value("message_id").content == "41354")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Message().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/getChat",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "17683241")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Chat().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/getChatAdministrators",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "914162")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  tgbot::ChatMemberAdministrator chat_member_administrator;
                  chat_member_administrator.set_status("administrator");
                  doc["result"] = {nlohmann::json::parse(chat_member_administrator.serialise())};

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "3")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = {1};

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "4")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = {nlohmann::json::object()};

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "5")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = {
                    nlohmann::json::object({{"status", 1}}
                    )
                  };

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "6")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = {
                    nlohmann::json::object({{"status", "peonvf"}}
                    )
                  };

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "administrator")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  tgbot::ChatMemberAdministrator chat_member_administrator;
                  chat_member_administrator.set_status("administrator");
                  doc["result"] = {nlohmann::json::parse(chat_member_administrator.serialise())};

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "kicked")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  tgbot::ChatMemberBanned chat_member_banned;
                  chat_member_banned.set_status("kicked");
                  doc["result"] = {nlohmann::json::parse(chat_member_banned.serialise())};

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "left")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  tgbot::ChatMemberLeft chat_member_left;
                  chat_member_left.set_status("left");
                  doc["result"] = {nlohmann::json::parse(chat_member_left.serialise())};

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "member")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  tgbot::ChatMemberMember chat_member_member;
                  chat_member_member.set_status("member");
                  doc["result"] = {nlohmann::json::parse(chat_member_member.serialise())};

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "creator")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  tgbot::ChatMemberOwner chat_member_owner;
                  chat_member_owner.set_status("creator");
                  doc["result"] = {nlohmann::json::parse(chat_member_owner.serialise())};

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "restricted")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  tgbot::ChatMemberRestricted chat_member_restricted;
                  chat_member_restricted.set_status("restricted");
                  doc["result"] = {nlohmann::json::parse(chat_member_restricted.serialise())};

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/getChatMember",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "14951" &&
                 request.get_file_value("user_id").content == "841354")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  tgbot::ChatMemberAdministrator chat_member_administrator;
                  chat_member_administrator.set_status("administrator");
                  doc["result"] = nlohmann::json::parse(chat_member_administrator.serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "3")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::object();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "4")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = {
                    {"status", 1}
                  };

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "5")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = {
                    {"status", "trqüpyxp"}
                  };

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "administrator")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  tgbot::ChatMemberAdministrator chat_member_administrator;
                  chat_member_administrator.set_status("administrator");
                  doc["result"] = nlohmann::json::parse(chat_member_administrator.serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "kicked")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  tgbot::ChatMemberBanned chat_member_banned;
                  chat_member_banned.set_status("kicked");
                  doc["result"] = nlohmann::json::parse(chat_member_banned.serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "left")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  tgbot::ChatMemberLeft chat_member_left;
                  chat_member_left.set_status("left");
                  doc["result"] = nlohmann::json::parse(chat_member_left.serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "member")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  tgbot::ChatMemberMember chat_member_member;
                  chat_member_member.set_status("member");
                  doc["result"] = nlohmann::json::parse(chat_member_member.serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "creator")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  tgbot::ChatMemberOwner chat_member_owner;
                  chat_member_owner.set_status("creator");
                  doc["result"] = nlohmann::json::parse(chat_member_owner.serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "restricted")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  tgbot::ChatMemberRestricted chat_member_restricted;
                  chat_member_restricted.set_status("restricted");
                  doc["result"] = nlohmann::json::parse(chat_member_restricted.serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/getChatMemberCount",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "63218498")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = "";

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Get(
      "/bottoken/getChatMenuButton",
      [](const httplib::Request &, httplib::Response &response)
      {
          nlohmann::json doc = nlohmann::json::object();
          doc["result"] = nlohmann::json::parse(tgbot::MenuButton().serialise());

          response.status = 200;
          response.set_content(doc.dump(), "application/json");
      });

    server.Post(
      "/bottoken/getChatMenuButton",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "465798")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::MenuButton().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/getCustomEmojiStickers",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") &&
                 request.get_file_value("custom_emoji_ids").content == nlohmann::json::array({"xcavpnoiet"}).dump())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = {nlohmann::json::parse(tgbot::Sticker().serialise())};

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "3")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = {1};

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/getFile",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("file_id").content == "coijpjoim")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::File().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Get(
      "/bottoken/getForumTopicIconStickers",
      [](const httplib::Request &, httplib::Response &response)
      {
          nlohmann::json doc = nlohmann::json::object();
          doc["result"] = {nlohmann::json::parse(tgbot::Sticker().serialise())};

          response.status = 200;
          response.set_content(doc.dump(), "application/json");
      });

    server.Post(
      "/bottoken/getForumTopicIconStickers",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "3")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = {1};

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/getGameHighScores",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("user_id").content == "8167315" &&
                 request.get_file_value("chat_id").content == "61832" &&
                 request.get_file_value("message_id").content == "186915" &&
                 request.get_file_value("inline_message_id").content == "embfmölwölr")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = {nlohmann::json::parse(tgbot::GameHighScore().serialise())};

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "3")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = {1};

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Get(
      "/bottoken/getMe",
      [](const httplib::Request &, httplib::Response &response)
      {
          nlohmann::json doc = nlohmann::json::object();
          doc["result"] = nlohmann::json::parse(tgbot::User().serialise());

          response.status = 200;
          response.set_content(doc.dump(), "application/json");
      });

    server.Post(
      "/bottoken/getMe",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Get(
      "/bottoken/getMyCommands",
      [](const httplib::Request &, httplib::Response &response)
      {
          nlohmann::json doc = nlohmann::json::object();
          doc["result"] = {nlohmann::json::parse(tgbot::BotCommand().serialise())};

          response.status = 200;
          response.set_content(doc.dump(), "application/json");
      });

    server.Post(
      "/bottoken/getMyCommands",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") &&
                 request.get_file_value("scope").content == tgbot::BotCommandScope().serialise() &&
                 request.get_file_value("language_code").content == "bnioufnhgds")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::array({nlohmann::json::parse(tgbot::BotCommand().serialise())});

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "3")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = {1};

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Get(
      "/bottoken/getMyDefaultAdministratorRights",
      [](const httplib::Request &, httplib::Response &response)
      {
          nlohmann::json doc = nlohmann::json::object();
          doc["result"] = nlohmann::json::parse(tgbot::ChatAdministratorRights().serialise());

          response.status = 200;
          response.set_content(doc.dump(), "application/json");
      });

    server.Post(
      "/bottoken/getMyDefaultAdministratorRights",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("for_channels").content == "0")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::ChatAdministratorRights().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Get(
      "/bottoken/getMyDescription",
      [](const httplib::Request &, httplib::Response &response)
      {
          nlohmann::json doc = nlohmann::json::object();
          doc["result"] = nlohmann::json::parse(tgbot::BotDescription().serialise());

          response.status = 200;
          response.set_content(doc.dump(), "application/json");
      });

    server.Post(
      "/bottoken/getMyDescription",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("language_code").content == "hnxmaüprq")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::BotDescription().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Get(
      "/bottoken/getMyShortDescription",
      [](const httplib::Request &, httplib::Response &response)
      {
          nlohmann::json doc = nlohmann::json::object();
          doc["result"] = nlohmann::json::parse(tgbot::BotShortDescription().serialise());

          response.status = 200;
          response.set_content(doc.dump(), "application/json");
      });

    server.Post(
      "/bottoken/getMyShortDescription",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("language_code").content == "pqwymgqwpoegsdj")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::BotShortDescription().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/getStickerSet",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("name").content == "qpoijmcg")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::StickerSet().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Get(
      "/bottoken/getUpdates",
      [](const httplib::Request &, httplib::Response &response)
      {
          nlohmann::json doc = nlohmann::json::object();
          tgbot::Update update;
          update.set_message(std::make_shared<tgbot::Message>());
          doc["result"] = {nlohmann::json::parse(update.serialise())};

          response.status = 200;
          response.set_content(doc.dump(), "application/json");
      });

    server.Post(
      "/bottoken/getUpdates",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("offset").content == "64224" &&
                 request.get_file_value("limit").content == "31841" &&
                 request.get_file_value("timeout").content == "183142" &&
                 request.get_file_value("allowed_updates").content == nlohmann::json::array({"lerlkjsmd"}).dump())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = {nlohmann::json::parse(tgbot::Update().serialise())};

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "3")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = {1};

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/getUserProfilePhotos",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("user_id").content == "861354634" &&
                 request.get_file_value("offset").content == "6841" &&
                 request.get_file_value("limit").content == "6844531")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::UserProfilePhotos().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Get(
      "/bottoken/getWebhookInfo",
      [](const httplib::Request &, httplib::Response &response)
      {
          nlohmann::json doc = nlohmann::json::object();
          doc["result"] = nlohmann::json::parse(tgbot::WebhookInfo().serialise());

          response.status = 200;
          response.set_content(doc.dump(), "application/json");
      });

    server.Post(
      "/bottoken/getWebhookInfo",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/hideGeneralForumTopic",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "89215")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/leaveChat",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "7198341")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Get(
      "/bottoken/logOut",
      [](const httplib::Request &, httplib::Response &response)
      {
          nlohmann::json doc = nlohmann::json::object();
          doc["result"] = true;

          response.status = 200;
          response.set_content(doc.dump(), "application/json");
      });

    server.Post(
      "/bottoken/logOut",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/pinChatMessage",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "49812" &&
                 request.get_file_value("message_id").content == "781624" &&
                 request.get_file_value("disable_notification").content == "0")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/promoteChatMember",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "681351" &&
                 request.get_file_value("user_id").content == "354511" &&
                 request.get_file_value("is_anonymous").content == "1" &&
                 request.get_file_value("can_manage_chat").content == "0" &&
                 request.get_file_value("can_post_messages").content == "1" &&
                 request.get_file_value("can_edit_messages").content == "0" &&
                 request.get_file_value("can_delete_messages").content == "0" &&
                 request.get_file_value("can_manage_video_chats").content == "1" &&
                 request.get_file_value("can_restrict_members").content == "1" &&
                 request.get_file_value("can_promote_members").content == "0" &&
                 request.get_file_value("can_change_info").content == "1" &&
                 request.get_file_value("can_invite_users").content == "0" &&
                 request.get_file_value("can_pin_messages").content == "0")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/reopenForumTopic",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "546816654" &&
                 request.get_file_value("message_thread_id").content == "665421365")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/reopenGeneralForumTopic",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "9137465")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/restrictChatMember",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "83131" &&
                 request.get_file_value("user_id").content == "68171453" &&
                 request.get_file_value("permissions").content == tgbot::ChatPermissions().serialise() &&
                 request.get_file_value("use_independent_chat_permissions").content == "1" &&
                 request.get_file_value("until_date").content == "614531")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/revokeChatInviteLink",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "165412" &&
                 request.get_file_value("invite_link").content == "dpckmwpjohf")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::ChatInviteLink().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/sendAnimation",
      [&test_file](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              std::string animation_data;

              if(std::filesystem::is_regular_file(request.get_file_value("animation").filename))
              {
                  std::ifstream inf(
                    std::filesystem::path(request.get_file_value("animation").filename).string(), std::ios::binary);
                  animation_data = std::string(std::istreambuf_iterator<char>(inf), {});
              }

              std::string upload_data;

              if(std::filesystem::is_regular_file(request.get_file_value(test_file).filename))
              {
                  std::ifstream inf(
                    std::filesystem::path(request.get_file_value(test_file).filename).string(), std::ios::binary);
                  upload_data = std::string(std::istreambuf_iterator<char>(inf), {});
              }

              if(
                ! request.has_file("test") && request.get_file_value("chat_id").content == "148942" &&
                request.get_file_value("message_thread_id").content == "46516" &&
                (request.get_file_value("animation").content == "cöoiqpfhth" ||
                 request.get_file_value("animation").content == animation_data) &&
                request.get_file_value("duration").content == "14892" &&
                request.get_file_value("width").content == "14253" &&
                request.get_file_value("height").content == "7899642" &&
                (request.get_file_value("thumbnail").content == "wpoifmwteh" ||
                 (request.get_file_value("thumbnail").content ==
                    "attach://" + std::filesystem::path(test_file).string() &&
                  request.get_file_value(test_file).content == upload_data)) &&
                request.get_file_value("caption").content == "woimpchw" &&
                request.get_file_value("parse_mode").content == "cpomiwfhwf" &&
                request.get_file_value("caption_entities").content ==
                  nlohmann::json::array({nlohmann::json::parse(tgbot::MessageEntity().serialise())}).dump() &&
                request.get_file_value("has_spoiler").content == "0" &&
                request.get_file_value("disable_notification").content == "0" &&
                request.get_file_value("protect_content").content == "1" &&
                request.get_file_value("reply_to_message_id").content == "6184684" &&
                request.get_file_value("allow_sending_without_reply").content == "1" &&
                request.get_file_value("reply_markup").content == tgbot::Reply().serialise())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Message().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/sendAudio",
      [&test_file](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              std::string audio_data;

              if(std::filesystem::is_regular_file(request.get_file_value("audio").filename))
              {
                  std::ifstream inf(
                    std::filesystem::path(request.get_file_value("audio").filename).string(), std::ios::binary);
                  audio_data = std::string(std::istreambuf_iterator<char>(inf), {});
              }

              std::string upload_data;

              if(std::filesystem::is_regular_file(request.get_file_value(test_file).filename))
              {
                  std::ifstream inf(
                    std::filesystem::path(request.get_file_value(test_file).filename).string(), std::ios::binary);
                  upload_data = std::string(std::istreambuf_iterator<char>(inf), {});
              }

              if(
                ! request.has_file("test") && request.get_file_value("chat_id").content == "442315" &&
                request.get_file_value("message_thread_id").content == "94354" &&
                (request.get_file_value("audio").content == "össdfhweg" ||
                 request.get_file_value("audio").content == audio_data) &&
                request.get_file_value("caption").content == "öslkmcerh" &&
                request.get_file_value("parse_mode").content == "wöölgkmmlke" &&
                request.get_file_value("caption_entities").content ==
                  nlohmann::json::array({nlohmann::json::parse(tgbot::MessageEntity().serialise())}).dump() &&
                request.get_file_value("duration").content == "176841" &&
                request.get_file_value("performer").content == "lcsnlkdrhf" &&
                request.get_file_value("title").content == "vsijniodjhfg" &&
                (request.get_file_value("thumbnail").content == "slömbdfhth" ||
                 (request.get_file_value("thumbnail").content ==
                    "attach://" + std::filesystem::path(test_file).string() &&
                  request.get_file_value(test_file).content == upload_data)) &&
                request.get_file_value("disable_notification").content == "0" &&
                request.get_file_value("protect_content").content == "0" &&
                request.get_file_value("reply_to_message_id").content == "146825" &&
                request.get_file_value("allow_sending_without_reply").content == "1" &&
                request.get_file_value("reply_markup").content == tgbot::Reply().serialise())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Message().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/sendChatAction",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "681315" &&
                 request.get_file_value("message_thread_id").content == "8942" &&
                 request.get_file_value("action").content == "qopijölmcag")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/sendContact",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "81315321" &&
                 request.get_file_value("phone_number").content == "wöoijömfhw" &&
                 request.get_file_value("first_name").content == "cjömökfhw" &&
                 request.get_file_value("last_name").content == "cpoiiopmnjwwfh" &&
                 request.get_file_value("vcard").content == "cljjmwfd" &&
                 request.get_file_value("disable_notification").content == "1" &&
                 request.get_file_value("protect_content").content == "0" &&
                 request.get_file_value("reply_to_message_id").content == "64534" &&
                 request.get_file_value("allow_sending_without_reply").content == "1" &&
                 request.get_file_value("reply_markup").content == tgbot::Reply().serialise())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Message().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/sendDice",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "861351" &&
                 request.get_file_value("emoji").content == "poiwmöhf" &&
                 request.get_file_value("disable_notification").content == "0" &&
                 request.get_file_value("protect_content").content == "0" &&
                 request.get_file_value("reply_to_message_id").content == "687131" &&
                 request.get_file_value("allow_sending_without_reply").content == "1" &&
                 request.get_file_value("reply_markup").content == tgbot::Reply().serialise())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Message().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/sendDocument",
      [&test_file](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              std::string document_data;

              if(std::filesystem::is_regular_file(request.get_file_value("document").filename))
              {
                  std::ifstream inf(
                    std::filesystem::path(request.get_file_value("document").filename).string(), std::ios::binary);
                  document_data = std::string(std::istreambuf_iterator<char>(inf), {});
              }

              std::string upload_data;

              if(std::filesystem::is_regular_file(request.get_file_value(test_file).filename))
              {
                  std::ifstream inf(
                    std::filesystem::path(request.get_file_value(test_file).filename).string(), std::ios::binary);
                  upload_data = std::string(std::istreambuf_iterator<char>(inf), {});
              }

              if(
                ! request.has_file("test") && request.get_file_value("chat_id").content == "1468425" &&
                request.get_file_value("message_thread_id").content == "49985" &&
                (request.get_file_value("document").content == "völkmsdth" ||
                 request.get_file_value("document").content == document_data) &&
                (request.get_file_value("thumbnail").content == "öiomfbsdfh" ||
                 (request.get_file_value("thumbnail").content ==
                    "attach://" + std::filesystem::path(test_file).string() &&
                  request.get_file_value(test_file).content == upload_data)) &&
                request.get_file_value("caption").content == "cqpjfmkbjsdfg" &&
                request.get_file_value("parse_mode").content == "coisdseh" &&
                request.get_file_value("caption_entities").content ==
                  nlohmann::json::array({nlohmann::json::parse(tgbot::MessageEntity().serialise())}).dump() &&
                request.get_file_value("disable_content_type_detection").content == "0" &&
                request.get_file_value("disable_notification").content == "1" &&
                request.get_file_value("protect_content").content == "0" &&
                request.get_file_value("reply_to_message_id").content == "4165245" &&
                request.get_file_value("allow_sending_without_reply").content == "0" &&
                request.get_file_value("reply_markup").content == tgbot::Reply().serialise())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Message().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/sendGame",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "71632165" &&
                 request.get_file_value("game_short_name").content == "wpoifmwg" &&
                 request.get_file_value("disable_notification").content == "0" &&
                 request.get_file_value("protect_content").content == "1" &&
                 request.get_file_value("reply_to_message_id").content == "681354" &&
                 request.get_file_value("allow_sending_without_reply").content == "1" &&
                 request.get_file_value("reply_markup").content == tgbot::InlineKeyboardMarkup().serialise())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Message().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/sendInvoice",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(
                ! request.has_file("test") && request.get_file_value("chat_id").content == "13488134" &&
                request.get_file_value("message_thread_id").content == "76254" &&
                request.get_file_value("title").content == "qpofhöwtkjhlk" &&
                request.get_file_value("description").content == "wjfoilökjsfd" &&
                request.get_file_value("payload").content == "wjeflkcslgkj" &&
                request.get_file_value("provider_token").content == "cjowmpjofh" &&
                request.get_file_value("currency").content == "tjlmsvkml" &&
                request.get_file_value("prices").content ==
                  nlohmann::json::array({nlohmann::json::parse(tgbot::LabeledPrice().serialise())}).dump() &&
                request.get_file_value("max_tip_amount").content == "8940131" &&
                request.get_file_value("suggested_tip_amounts").content == nlohmann::json::array({810350}).dump() &&
                request.get_file_value("start_parameter").content == "cjllkfg" &&
                request.get_file_value("provider_data").content == "iemamdg" &&
                request.get_file_value("photo_url").content == "cijoojmfg" &&
                request.get_file_value("photo_size").content == "41665" &&
                request.get_file_value("photo_width").content == "561" &&
                request.get_file_value("photo_height").content == "8654164" &&
                request.get_file_value("need_name").content == "1" &&
                request.get_file_value("need_phone_number").content == "0" &&
                request.get_file_value("need_email").content == "1" &&
                request.get_file_value("need_shipping_address").content == "0" &&
                request.get_file_value("send_phone_number_to_provider").content == "0" &&
                request.get_file_value("send_email_to_provider").content == "1" &&
                request.get_file_value("is_flexible").content == "1" &&
                request.get_file_value("disable_notification").content == "0" &&
                request.get_file_value("protect_content").content == "0" &&
                request.get_file_value("reply_to_message_id").content == "166551" &&
                request.get_file_value("allow_sending_without_reply").content == "1" &&
                request.get_file_value("reply_markup").content == tgbot::InlineKeyboardMarkup().serialise())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Message().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/sendLocation",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "416453" &&
                 std::to_string(std::stoi(request.get_file_value("latitude").content)) == "83173" &&
                 std::to_string(std::stoi(request.get_file_value("longitude").content)) == "81646" &&
                 std::to_string(std::stoi(request.get_file_value("horizontal_accuracy").content)) == "174" &&
                 request.get_file_value("live_period").content == "534534" &&
                 request.get_file_value("heading").content == "6781543" &&
                 request.get_file_value("proximity_alert_radius").content == "1342418" &&
                 request.get_file_value("disable_notification").content == "0" &&
                 request.get_file_value("protect_content").content == "1" &&
                 request.get_file_value("reply_to_message_id").content == "344351" &&
                 request.get_file_value("allow_sending_without_reply").content == "1" &&
                 request.get_file_value("reply_markup").content == tgbot::Reply().serialise())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Message().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/sendMediaGroup",
      [&test_file](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              const tgbot::InputMediaAudio input_media_audio1;

              tgbot::InputMediaAudio input_media_audio2;
              input_media_audio2.set_media("attach://" + std::filesystem::path(test_file).string());
              input_media_audio2.set_thumbnail("attach://" + std::filesystem::path(test_file).string());

              const tgbot::InputMediaDocument input_media_document1;

              tgbot::InputMediaDocument input_media_document2;
              input_media_document2.set_media("attach://" + std::filesystem::path(test_file).string());
              input_media_document2.set_thumbnail("attach://" + std::filesystem::path(test_file).string());

              const tgbot::InputMediaPhoto input_media_photo1;

              tgbot::InputMediaPhoto input_media_photo2;
              input_media_photo2.set_media("attach://" + std::filesystem::path(test_file).string());

              const tgbot::InputMediaVideo input_media_video1;

              tgbot::InputMediaVideo input_media_video2;
              input_media_video2.set_media("attach://" + std::filesystem::path(test_file).string());
              input_media_video2.set_thumbnail("attach://" + std::filesystem::path(test_file).string());

              std::string upload_data;

              if(std::filesystem::is_regular_file(request.get_file_value(test_file).filename))
              {
                  std::ifstream inf(
                    std::filesystem::path(request.get_file_value(test_file).filename).string(), std::ios::binary);
                  upload_data = std::string(std::istreambuf_iterator<char>(inf), {});
              }

              if(
                ! request.has_file("test") && request.get_file_value("chat_id").content == "14645" &&
                (request.get_file_value("media").content ==
                   nlohmann::json::array({nlohmann::json::parse(input_media_audio1.serialise())}).dump() ||
                 (request.get_file_value("media").content ==
                    nlohmann::json::array({nlohmann::json::parse(input_media_audio2.serialise())}).dump() &&
                  request.get_file_value(test_file).content == upload_data) ||
                 request.get_file_value("media").content ==
                   nlohmann::json::array({nlohmann::json::parse(input_media_document1.serialise())}).dump() ||
                 (request.get_file_value("media").content ==
                    nlohmann::json::array({nlohmann::json::parse(input_media_document2.serialise())}).dump() &&
                  request.get_file_value(test_file).content == upload_data) ||
                 request.get_file_value("media").content ==
                   nlohmann::json::array({nlohmann::json::parse(input_media_photo1.serialise())}).dump() ||
                 (request.get_file_value("media").content ==
                    nlohmann::json::array({nlohmann::json::parse(input_media_photo2.serialise())}).dump() &&
                  request.get_file_value(test_file).content == upload_data) ||
                 request.get_file_value("media").content ==
                   nlohmann::json::array({nlohmann::json::parse(input_media_video1.serialise())}).dump() ||
                 (request.get_file_value("media").content ==
                    nlohmann::json::array({nlohmann::json::parse(input_media_video2.serialise())}).dump() &&
                  request.get_file_value(test_file).content == upload_data)) &&
                request.get_file_value("disable_notification").content == "1" &&
                request.get_file_value("protect_content").content == "0" &&
                request.get_file_value("reply_to_message_id").content == "6817543" &&
                request.get_file_value("allow_sending_without_reply").content == "0")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = {nlohmann::json::parse(tgbot::Message().serialise())};

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "3")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = {1};

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/sendMessage",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "61564256" &&
                 request.get_file_value("message_thread_id").content == "79518" &&
                 request.get_file_value("text").content == "södjfgoisg" &&
                 request.get_file_value("parse_mode").content == "öodjfgsdfg" &&
                 request.get_file_value("entities").content ==
                   nlohmann::json::array({nlohmann::json::parse(tgbot::MessageEntity().serialise())}).dump() &&
                 request.get_file_value("disable_web_page_preview").content == "0" &&
                 request.get_file_value("disable_notification").content == "1" &&
                 request.get_file_value("protect_content").content == "0" &&
                 request.get_file_value("reply_to_message_id").content == "451641" &&
                 request.get_file_value("allow_sending_without_reply").content == "0" &&
                 request.get_file_value("reply_markup").content == tgbot::Reply().serialise())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Message().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/sendPhoto",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              std::string photo_data;

              if(std::filesystem::is_regular_file(request.get_file_value("photo").filename))
              {
                  std::ifstream inf(
                    std::filesystem::path(request.get_file_value("photo").filename).string(), std::ios::binary);
                  photo_data = std::string(std::istreambuf_iterator<char>(inf), {});
              }

              if(! request.has_file("test") && request.get_file_value("chat_id").content == "618462841" &&
                 request.get_file_value("message_thread_id").content == "54923" &&
                 (request.get_file_value("photo").content == "sldmfgsdfg" ||
                  request.get_file_value("photo").content == photo_data) &&
                 request.get_file_value("caption").content == "söoifsdfhrth" &&
                 request.get_file_value("parse_mode").content == "slkdjfheh" &&
                 request.get_file_value("caption_entities").content ==
                   nlohmann::json::array({nlohmann::json::parse(tgbot::MessageEntity().serialise())}).dump() &&
                 request.get_file_value("has_spoiler").content == "0" &&
                 request.get_file_value("disable_notification").content == "1" &&
                 request.get_file_value("protect_content").content == "1" &&
                 request.get_file_value("reply_to_message_id").content == "146814" &&
                 request.get_file_value("allow_sending_without_reply").content == "1" &&
                 request.get_file_value("reply_markup").content == tgbot::Reply().serialise())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Message().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/sendPoll",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(
                ! request.has_file("test") && request.get_file_value("chat_id").content == "6813131" &&
                request.get_file_value("question").content == "qoipjoiöfmmb" &&
                request.get_file_value("options").content == nlohmann::json::array({"wpmbmwm"}).dump() &&
                request.get_file_value("is_anonymous").content == "0" &&
                request.get_file_value("type").content == "wöoijmöclmökx" &&
                request.get_file_value("allows_multiple_answers").content == "1" &&
                request.get_file_value("correct_option_id").content == "8716435" &&
                request.get_file_value("explanation").content == "qmölmölag" &&
                request.get_file_value("explanation_parse_mode").content == "qpoijmfiog" &&
                request.get_file_value("explanation_entities").content ==
                  nlohmann::json::array({nlohmann::json::parse(tgbot::MessageEntity().serialise())}).dump() &&
                request.get_file_value("open_period").content == "89761" &&
                request.get_file_value("close_date").content == "98642" &&
                request.get_file_value("is_closed").content == "0" &&
                request.get_file_value("disable_notification").content == "1" &&
                request.get_file_value("protect_content").content == "1" &&
                request.get_file_value("reply_to_message_id").content == "87914" &&
                request.get_file_value("allow_sending_without_reply").content == "0" &&
                request.get_file_value("reply_markup").content == tgbot::Reply().serialise())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Message().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/sendSticker",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              std::string sticker_data;

              if(std::filesystem::is_regular_file(request.get_file_value("sticker").filename))
              {
                  std::ifstream inf(
                    std::filesystem::path(request.get_file_value("sticker").filename).string(), std::ios::binary);
                  sticker_data = std::string(std::istreambuf_iterator<char>(inf), {});
              }

              if(! request.has_file("test") && request.get_file_value("chat_id").content == "8614665" &&
                 (request.get_file_value("sticker").content == "wofkmwc" ||
                  request.get_file_value("sticker").content == sticker_data) &&
                 request.get_file_value("disable_notification").content == "1" &&
                 request.get_file_value("protect_content").content == "0" &&
                 request.get_file_value("reply_to_message_id").content == "713424" &&
                 request.get_file_value("allow_sending_without_reply").content == "1" &&
                 request.get_file_value("reply_markup").content == tgbot::Reply().serialise())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Message().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/sendVenue",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "18631351" &&
                 std::to_string(std::stoi(request.get_file_value("latitude").content)) == "618731" &&
                 std::to_string(std::stoi(request.get_file_value("longitude").content)) == "1835" &&
                 request.get_file_value("title").content == "öoiswvm" &&
                 request.get_file_value("address").content == "sllvslköv" &&
                 request.get_file_value("foursquare_id").content == "coijmwiomjdfh" &&
                 request.get_file_value("foursquare_type").content == "qolmwfg" &&
                 request.get_file_value("google_place_id").content == "clkjkmdfh" &&
                 request.get_file_value("google_place_type").content == "öwimwöim" &&
                 request.get_file_value("disable_notification").content == "0" &&
                 request.get_file_value("protect_content").content == "1" &&
                 request.get_file_value("reply_to_message_id").content == "68174" &&
                 request.get_file_value("allow_sending_without_reply").content == "1" &&
                 request.get_file_value("reply_markup").content == tgbot::Reply().serialise())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Message().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/sendVideo",
      [&test_file](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              std::string video_data;

              if(std::filesystem::is_regular_file(request.get_file_value("video").filename))
              {
                  std::ifstream inf(
                    std::filesystem::path(request.get_file_value("video").filename).string(), std::ios::binary);
                  video_data = std::string(std::istreambuf_iterator<char>(inf), {});
              }

              std::string upload_data;

              if(std::filesystem::is_regular_file(request.get_file_value(test_file).filename))
              {
                  std::ifstream inf(
                    std::filesystem::path(request.get_file_value(test_file).filename).string(), std::ios::binary);
                  upload_data = std::string(std::istreambuf_iterator<char>(inf), {});
              }

              if(
                ! request.has_file("test") && request.get_file_value("chat_id").content == "168251" &&
                request.get_file_value("message_thread_id").content == "98416" &&
                (request.get_file_value("video").content == "cöosmdfh" ||
                 request.get_file_value("video").content == video_data) &&
                request.get_file_value("duration").content == "415324351" &&
                request.get_file_value("width").content == "145245" &&
                request.get_file_value("height").content == "176842498" &&
                (request.get_file_value("thumbnail").content == "ösoetheg" ||
                 (request.get_file_value("thumbnail").content ==
                    "attach://" + std::filesystem::path(test_file).string() &&
                  request.get_file_value(test_file).content == upload_data)) &&
                request.get_file_value("caption").content == "cimjwerhqpv" &&
                request.get_file_value("parse_mode").content == "cöjmerhqoijc" &&
                request.get_file_value("caption_entities").content ==
                  nlohmann::json::array({nlohmann::json::parse(tgbot::MessageEntity().serialise())}).dump() &&
                request.get_file_value("has_spoiler").content == "1" &&
                request.get_file_value("supports_streaming").content == "0" &&
                request.get_file_value("disable_notification").content == "1" &&
                request.get_file_value("protect_content").content == "0" &&
                request.get_file_value("reply_to_message_id").content == "146542" &&
                request.get_file_value("allow_sending_without_reply").content == "1" &&
                request.get_file_value("reply_markup").content == tgbot::Reply().serialise())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Message().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/sendVideoNote",
      [&test_file](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              std::string video_note_data;

              if(std::filesystem::is_regular_file(request.get_file_value("video_note").filename))
              {
                  std::ifstream inf(
                    std::filesystem::path(request.get_file_value("video_note").filename).string(), std::ios::binary);
                  video_note_data = std::string(std::istreambuf_iterator<char>(inf), {});
              }

              std::string upload_data;

              if(std::filesystem::is_regular_file(request.get_file_value(test_file).filename))
              {
                  std::ifstream inf(
                    std::filesystem::path(request.get_file_value(test_file).filename).string(), std::ios::binary);
                  upload_data = std::string(std::istreambuf_iterator<char>(inf), {});
              }

              if(! request.has_file("test") && request.get_file_value("chat_id").content == "8662351" &&
                 (request.get_file_value("video_note").content == "linhsjlkf" ||
                  request.get_file_value("video_note").content == video_note_data) &&
                 request.get_file_value("duration").content == "6165425" &&
                 request.get_file_value("length").content == "6313454" &&
                 (request.get_file_value("thumbnail").content == "vsdlsjoi" ||
                  (request.get_file_value("thumbnail").content ==
                     "attach://" + std::filesystem::path(test_file).string() &&
                   request.get_file_value(test_file).content == upload_data)) &&
                 request.get_file_value("disable_notification").content == "1" &&
                 request.get_file_value("protect_content").content == "1" &&
                 request.get_file_value("reply_to_message_id").content == "63181324" &&
                 request.get_file_value("allow_sending_without_reply").content == "0" &&
                 request.get_file_value("reply_markup").content == tgbot::Reply().serialise())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Message().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/sendVoice",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              std::string voice_data;

              if(std::filesystem::is_regular_file(request.get_file_value("voice").filename))
              {
                  std::ifstream inf(
                    std::filesystem::path(request.get_file_value("voice").filename).string(), std::ios::binary);
                  voice_data = std::string(std::istreambuf_iterator<char>(inf), {});
              }

              if(! request.has_file("test") && request.get_file_value("chat_id").content == "17893254" &&
                 request.get_file_value("message_thread_id").content == "86516651" &&
                 (request.get_file_value("voice").content == "cöoiwqnpfh" ||
                  request.get_file_value("voice").content == voice_data) &&
                 request.get_file_value("caption").content == "wümthcsm" &&
                 request.get_file_value("parse_mode").content == "qpoimcsdfh" &&
                 request.get_file_value("caption_entities").content ==
                   nlohmann::json::array({nlohmann::json::parse(tgbot::MessageEntity().serialise())}).dump() &&
                 request.get_file_value("duration").content == "68164" &&
                 request.get_file_value("disable_notification").content == "0" &&
                 request.get_file_value("protect_content").content == "0" &&
                 request.get_file_value("reply_to_message_id").content == "6151" &&
                 request.get_file_value("allow_sending_without_reply").content == "1" &&
                 request.get_file_value("reply_markup").content == tgbot::Reply().serialise())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Message().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/setChatAdministratorCustomTitle",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "6814351" &&
                 request.get_file_value("user_id").content == "681351" &&
                 request.get_file_value("custom_title").content == "gjlkmlwg")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/setChatDescription",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "716546" &&
                 request.get_file_value("description").content == "cijopijomfwh")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Get(
      "/bottoken/setChatMenuButton",
      [](const httplib::Request &, httplib::Response &response)
      {
          nlohmann::json doc = nlohmann::json::object();
          doc["result"] = true;

          response.status = 200;
          response.set_content(doc.dump(), "application/json");
      });

    server.Post(
      "/bottoken/setChatMenuButton",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "2815384" &&
                 request.get_file_value("menu_button").content ==
                   nlohmann::json::parse(tgbot::MenuButton().serialise()).dump())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/setChatPermissions",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "317681351" &&
                 request.get_file_value("permissions").content == tgbot::ChatPermissions().serialise() &&
                 request.get_file_value("use_independent_chat_permissions").content == "0")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/setChatPhoto",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              std::string photo_data;

              if(std::filesystem::is_regular_file(request.get_file_value("photo").filename))
              {
                  std::ifstream inf(
                    std::filesystem::path(request.get_file_value("photo").filename).string(), std::ios::binary);
                  photo_data = std::string(std::istreambuf_iterator<char>(inf), {});
              }

              if(! request.has_file("test") && request.get_file_value("chat_id").content == "145145489" &&
                 request.get_file_value("photo").content == photo_data)
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/setChatStickerSet",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "19541" &&
                 request.get_file_value("sticker_set_name").content == "dpqinckjer")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/setChatTitle",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "144982" &&
                 request.get_file_value("title").content == "lqiojfhjj")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/setCustomEmojiStickerSetThumbnail",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("name").content == "upqüpdmwoueh" &&
                 request.get_file_value("custom_emoji_id").content == "hgajüqwrijzsaqüowrj")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/setGameScore",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("user_id").content == "76156154" &&
                 request.get_file_value("score").content == "716541" &&
                 request.get_file_value("force").content == "0" &&
                 request.get_file_value("disable_edit_message").content == "1" &&
                 request.get_file_value("chat_id").content == "716835" &&
                 request.get_file_value("message_id").content == "681153" &&
                 request.get_file_value("inline_message_id").content == "wpofimjwkölf")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "3")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Message().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/setMyCommands",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") &&
                 request.get_file_value("commands").content ==
                   nlohmann::json::array({nlohmann::json::parse(tgbot::BotCommand().serialise())}).dump() &&
                 request.get_file_value("scope").content == tgbot::BotCommandScope().serialise() &&
                 request.get_file_value("language_code").content == "de")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Get(
      "/bottoken/setMyDefaultAdministratorRights",
      [](const httplib::Request &, httplib::Response &response)
      {
          nlohmann::json doc = nlohmann::json::object();
          doc["result"] = true;

          response.status = 200;
          response.set_content(doc.dump(), "application/json");
      });

    server.Post(
      "/bottoken/setMyDefaultAdministratorRights",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") &&
                 request.get_file_value("rights").content == tgbot::ChatAdministratorRights().serialise() &&
                 request.get_file_value("for_channels").content == "0")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Get(
      "/bottoken/setMyDescription",
      [](const httplib::Request &, httplib::Response &response)
      {
          nlohmann::json doc = nlohmann::json::object();
          doc["result"] = true;

          response.status = 200;
          response.set_content(doc.dump(), "application/json");
      });

    server.Post(
      "/bottoken/setMyDescription",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("description").content == "eanjäpewiklancb" &&
                 request.get_file_value("language_code").content == "ebjnwpoidjüpwjh")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Get(
      "/bottoken/setMyShortDescription",
      [](const httplib::Request &, httplib::Response &response)
      {
          nlohmann::json doc = nlohmann::json::object();
          doc["result"] = true;

          response.status = 200;
          response.set_content(doc.dump(), "application/json");
      });

    server.Post(
      "/bottoken/setMyShortDescription",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("short_description").content == "xpwethjnasdv" &&
                 request.get_file_value("language_code").content == "bnaüpcmkhitu")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/setPassportDataErrors",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("user_id").content == "8716345" &&
                 request.get_file_value("errors").content ==
                   nlohmann::json::array({nlohmann::json::parse(tgbot::PassportElementError().serialise())}).dump())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/setStickerEmojiList",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("sticker").content == "rhjaüosidjh" &&
                 request.get_file_value("emoji_list").content == nlohmann::json::array({"cmjüpowqspdke"}).dump())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/setStickerKeywords",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("sticker").content == "iuaoipsdtrj" &&
                 request.get_file_value("keywords").content == nlohmann::json::array({"rpnaosjhtjhi"}).dump())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/setStickerMaskPosition",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("sticker").content == "rhaspietalxkyoijer" &&
                 request.get_file_value("mask_position").content == tgbot::MaskPosition().serialise())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/setStickerPositionInSet",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("sticker").content == "ckoinwfh" &&
                 request.get_file_value("position").content == "8314")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/setStickerSetThumbnail",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              std::string thumbnail_data;

              if(std::filesystem::is_regular_file(request.get_file_value("thumbnail").filename))
              {
                  std::ifstream inf(
                    std::filesystem::path(request.get_file_value("thumbnail").filename).string(), std::ios::binary);
                  thumbnail_data = std::string(std::istreambuf_iterator<char>(inf), {});
              }

              if(! request.has_file("test") && request.get_file_value("name").content == "pqpclöotwhkxcnuher" &&
                 request.get_file_value("user_id").content == "413542984" &&
                 (request.get_file_value("thumbnail").content == "cppfmwüpithm" ||
                  request.get_file_value("thumbnail").content == thumbnail_data))
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/setStickerSetTitle",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("name").content == "xoepirhsdv" &&
                 request.get_file_value("title").content == "ziqüsiomwepothj")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/setWebhook",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              std::string certificate_data;

              if(std::filesystem::is_regular_file(request.get_file_value("certificate").filename))
              {
                  std::ifstream inf(
                    std::filesystem::path(request.get_file_value("certificate").filename).string(), std::ios::binary);
                  certificate_data = std::string(std::istreambuf_iterator<char>(inf), {});
              }

              if(! request.has_file("test") && request.get_file_value("url").content == "gamgwerg" &&
                 request.get_file_value("certificate").content == certificate_data &&
                 request.get_file_value("ip_address").content == "wleofoiwjefgr" &&
                 request.get_file_value("max_connections").content == "168481" &&
                 request.get_file_value("allowed_updates").content == nlohmann::json::array({"flbkshndf"}).dump() &&
                 request.get_file_value("drop_pending_updates").content == "0" &&
                 request.get_file_value("secret_token").content == "hqdgserg")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/stopMessageLiveLocation",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "1435" &&
                 request.get_file_value("message_id").content == "413541321" &&
                 request.get_file_value("inline_message_id").content == "csoijdfhmwth" &&
                 request.get_file_value("reply_markup").content == tgbot::InlineKeyboardMarkup().serialise())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "3")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Message().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/stopPoll",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "7651465" &&
                 request.get_file_value("message_id").content == "78915" &&
                 request.get_file_value("reply_markup").content == tgbot::InlineKeyboardMarkup().serialise())
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::Poll().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/unbanChatMember",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "16153" &&
                 request.get_file_value("user_id").content == "68113" &&
                 request.get_file_value("only_if_banned").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/unbanChatSenderChat",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "914351" &&
                 request.get_file_value("sender_chat_id").content == "50194")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/unhideGeneralForumTopic",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "16849")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/unpinAllChatMessages",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "195413")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/unpinAllForumTopicMessages",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "2891354" &&
                 request.get_file_value("message_thread_id").content == "8615")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/unpinChatMessage",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              if(! request.has_file("test") && request.get_file_value("chat_id").content == "7811561234" &&
                 request.get_file_value("message_id").content == "4186425")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = true;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    server.Post(
      "/bottoken/uploadStickerFile",
      [](const httplib::Request &request, httplib::Response &response)
      {
          if(request.is_multipart_form_data())
          {
              std::string sticker_data;

              if(std::filesystem::is_regular_file(request.get_file_value("sticker").filename))
              {
                  std::ifstream inf(
                    std::filesystem::path(request.get_file_value("sticker").filename).string(), std::ios::binary);
                  sticker_data = std::string(std::istreambuf_iterator<char>(inf), {});
              }

              if(! request.has_file("test") && request.get_file_value("user_id").content == "17654" &&
                 request.get_file_value("sticker").content == sticker_data &&
                 request.get_file_value("sticker_format").content == "xqwpoihsdf")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = nlohmann::json::parse(tgbot::File().serialise());

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "0")
              {
                  const nlohmann::json doc = nlohmann::json::array();

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "1")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["noresult"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
              else if(request.get_file_value("test").content == "2")
              {
                  nlohmann::json doc = nlohmann::json::object();
                  doc["result"] = 1;

                  response.status = 200;
                  response.set_content(doc.dump(), "application/json");
              }
          }
      });

    std::thread thread = std::thread(
      [&server]()
      {
          server.listen("localhost", 5286);
      });
    thread.detach();
    std::ofstream outf(test_file);
    outf << "rgpqoihsdf";
    outf.close();

    testing::InitGoogleTest(&argc, argv);
    const std::int32_t test_result = RUN_ALL_TESTS();

    server.stop();
    std::filesystem::remove(test_file);
    return test_result;
}
