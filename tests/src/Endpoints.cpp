#include "tgbot/Endpoints.h"

#include <initializer_list>
#include <memory>
#include <optional>
#include <stdexcept>
#include <string>
#include <variant>
#include <vector>

#include "gtest/gtest.h"
#include "tgbot/Bot.h"
#include "tgbot/types/BotCommand.h"
#include "tgbot/types/BotCommandScope.h"
#include "tgbot/types/ChatMember.h"
#include "tgbot/types/ChatMemberAdministrator.h"
#include "tgbot/types/ChatMemberBanned.h"
#include "tgbot/types/ChatMemberLeft.h"
#include "tgbot/types/ChatMemberMember.h"
#include "tgbot/types/ChatMemberOwner.h"
#include "tgbot/types/ChatMemberRestricted.h"
#include "tgbot/types/ChatPermissions.h"
#include "tgbot/types/InlineKeyboardMarkup.h"
#include "tgbot/types/InlineQueryResult.h"
#include "tgbot/types/InputMediaAnimation.h"
#include "tgbot/types/InputMediaAudio.h"
#include "tgbot/types/InputMediaDocument.h"
#include "tgbot/types/InputMediaPhoto.h"
#include "tgbot/types/InputMediaVideo.h"
#include "tgbot/types/LabeledPrice.h"
#include "tgbot/types/MessageEntity.h"
#include "tgbot/types/PassportElementError.h"
#include "tgbot/types/Reply.h"
#include "tgbot/types/ShippingOption.h"

/*
 * Test patterns:
 * 1. Test name "normal"
 * 1.1 Send valid requests to cover all correct cases.
 * In case that all values are correct, the expected object is returned or a default value.
 * Default bool: true
 * Default int: 1
 * Default string: ""
 *
 * 2. Test name "errors"
 * 2.1 Send a request to a non-existent address.
 * 2.2 (optional) Cover all remaining cases.
 */

const std::string test_file = "/tmp/test.txt";

TEST(EndpointsaddStickerToSet, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);

    const tgbot::InputSticker::ptr input_sticker = std::make_shared<tgbot::InputSticker>();
    input_sticker->set_sticker(test_file);

    ASSERT_NO_THROW(bot.get_endpoints()->addStickerToSet(1788415, "füpcmiowfh", input_sticker));
}

TEST(EndpointsaddStickerToSet, errors)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);

    const tgbot::InputSticker::ptr input_sticker = std::make_shared<tgbot::InputSticker>();
    input_sticker->set_sticker(test_file);

    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->addStickerToSet(1788415, "füpcmiowfh", input_sticker), std::runtime_error);
    }
}

TEST(EndpointsanswerCallbackQuery, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->answerCallbackQuery("cpoijwfh", "howlkcmh", true, "tpoimwlfg", 7651489));
}

TEST(EndpointsanswerCallbackQuery, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->answerCallbackQuery("cpoijwfh", "howlkcmh", true, "tpoimwlfg", 7651489),
                     std::runtime_error);
    }
}

TEST(EndpointsanswerInlineQuery, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->answerInlineQuery(
      "pfkcnjwoiufhmkfg", {std::make_shared<tgbot::InlineQueryResult>()}, 7651, true, "dlfhjmnbvc", "öldjhjvmfwb",
      "wicnhwojfg"));
}

TEST(EndpointsanswerInlineQuery, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->answerInlineQuery(
            "pfkcnjwoiufhmkfg", {std::make_shared<tgbot::InlineQueryResult>()}, 7651, true, "dlfhjmnbvc", "öldjhjvmfwb",
            "wicnhwojfg"),
          std::runtime_error);
    }
}

TEST(EndpointsanswerPreCheckoutQuery, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->answerPreCheckoutQuery("ufwbmlkfg", true, "ösölmfhr"));
}

TEST(EndpointsanswerPreCheckoutQuery, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->answerPreCheckoutQuery("ufwbmlkfg", true, "ösölmfhr"), std::runtime_error);
    }
}

TEST(EndpointsanswerShippingQuery, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->answerShippingQuery(
      "tofölmkwjfdg", true, std::vector<tgbot::ShippingOption::ptr> {std::make_shared<tgbot::ShippingOption>()},
      "kwlmblmsf"));
}

TEST(EndpointsanswerShippingQuery, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->answerShippingQuery(
            "tofölmkwjfdg", true, std::vector<tgbot::ShippingOption::ptr> {std::make_shared<tgbot::ShippingOption>()},
            "kwlmblmsf"),
          std::runtime_error);
    }
}

TEST(EndpointsanswerWebAppQuery, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->answerWebAppQuery("hljasdth", std::make_shared<tgbot::InlineQueryResult>()));
}

TEST(EndpointsanswerWebAppQuery, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->answerWebAppQuery("hljasdth", std::make_shared<tgbot::InlineQueryResult>()),
                     std::runtime_error);
    }
}

TEST(EndpointsapproveChatJoinRequest, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->approveChatJoinRequest(7315, 4164284));
    ASSERT_NO_THROW(bot.get_endpoints()->approveChatJoinRequest("7315", 4164284));
}

TEST(EndpointsapproveChatJoinRequest, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->approveChatJoinRequest(7315, 4164284), std::runtime_error);
    }
}

TEST(EndpointsbanChatMember, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->banChatMember(15316, 681435, 681534, true));
    ASSERT_NO_THROW(bot.get_endpoints()->banChatMember("15316", 681435, 681534, true));
}

TEST(EndpointsbanChatMember, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->banChatMember(15316, 681435, 681534, true), std::runtime_error);
    }
}

TEST(EndpointsbanChatSenderChat, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->banChatSenderChat(715617, 282546));
    ASSERT_NO_THROW(bot.get_endpoints()->banChatSenderChat("715617", 282546));
}

TEST(EndpointsbanChatSenderChat, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->banChatSenderChat(715617, 282546), std::runtime_error);
    }
}

TEST(Endpointsclose, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->close());
}

TEST(Endpointsclose, errors)
{
    const tgbot::Bot bot("error-domain", 5286, "token", std::nullopt, false);
    ASSERT_THROW(bot.get_endpoints()->close(), std::runtime_error);

    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->close(), std::runtime_error);
    }
}

TEST(EndpointscloseForumTopic, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->closeForumTopic(8168762, 4351687));
    ASSERT_NO_THROW(bot.get_endpoints()->closeForumTopic("8168762", 4351687));
}

TEST(EndpointscloseForumTopic, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->closeForumTopic(8168762, 4351687), std::runtime_error);
    }
}

TEST(EndpointscloseGeneralForumTopic, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->closeGeneralForumTopic(168476));
    ASSERT_NO_THROW(bot.get_endpoints()->closeGeneralForumTopic("168476"));
}

TEST(EndpointscloseGeneralForumTopic, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->closeGeneralForumTopic(168476), std::runtime_error);
    }
}

TEST(EndpointscopyMessage, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->copyMessage(
      688261, 816421, 146842614, 16546214, "söldfmbjsdf", "söfgsdfg",
      std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, false, false, 414620, true,
      std::make_shared<tgbot::Reply>()));
    ASSERT_NO_THROW(bot.get_endpoints()->copyMessage(
      "688261", 816421, "146842614", 16546214, "söldfmbjsdf", "söfgsdfg",
      std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, false, false, 414620, true,
      std::make_shared<tgbot::Reply>()));
}

TEST(EndpointscopyMessage, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->copyMessage(
            688261, 816421, 146842614, 16546214, "söldfmbjsdf", "söfgsdfg",
            std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, false, false, 414620,
            true, std::make_shared<tgbot::Reply>()),
          std::runtime_error);
    }
}

TEST(EndpointscreateChatInviteLink, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->createChatInviteLink(8312015, "fhjpoicjw", 618435, 812461, true));
    ASSERT_NO_THROW(bot.get_endpoints()->createChatInviteLink("8312015", "fhjpoicjw", 618435, 812461, true));
}

TEST(EndpointscreateChatInviteLink, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->createChatInviteLink(8312015, "fhjpoicjw", 618435, 812461, true), std::runtime_error);
    }
}

TEST(EndpointscreateForumTopic, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->createForumTopic(52384, "uoinawdg", 7354984, "esopihwer"));
    ASSERT_NO_THROW(bot.get_endpoints()->createForumTopic("52384", "uoinawdg", 7354984, "esopihwer"));
}

TEST(EndpointscreateForumTopic, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->createForumTopic(52384, "uoinawdg", 7354984, "esopihwer"), std::runtime_error);
    }
}

TEST(EndpointscreateInvoiceLink, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->createInvoiceLink(
      "zopxijar", "hpoiascv", "thpuhcasg", "thpoikjnsfeh", "sdjrtn", {std::make_shared<tgbot::LabeledPrice>()}, 3678,
      std::vector<std::int32_t> {23573}, "rhonjvs", "cxnosodj", 1684531, 38151, 7681354, false, true, false, false,
      true, true, false));
}

TEST(EndpointscreateInvoiceLink, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->createInvoiceLink(
            "zopxijar", "hpoiascv", "thpuhcasg", "thpoikjnsfeh", "sdjrtn", {std::make_shared<tgbot::LabeledPrice>()},
            3678, std::vector<std::int32_t> {23573}, "rhonjvs", "cxnosodj", 1684531, 38151, 7681354, false, true, false,
            false, true, true, false),
          std::runtime_error);
    }
}

TEST(EndpointscreateNewStickerSet, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);

    const tgbot::InputSticker::ptr input_sticker1 = std::make_shared<tgbot::InputSticker>();

    const tgbot::InputSticker::ptr input_sticker2 = std::make_shared<tgbot::InputSticker>();
    input_sticker2->set_sticker(test_file);

    for(const tgbot::InputSticker::ptr &input_sticker : {input_sticker1, input_sticker2})
    {
        ASSERT_NO_THROW(bot.get_endpoints()->createNewStickerSet(
          17684, "irhwomlkjf", "fjmlqd", {input_sticker}, "cpoqmöfg", "ixpuhwhe", true));
    }
}

TEST(EndpointscreateNewStickerSet, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->createNewStickerSet(
            17684, "irhwomlkjf", "fjmlqd", {std::make_shared<tgbot::InputSticker>()}, "cpoqmöfg", "ixpuhwhe", true),
          std::runtime_error);
    }
}

TEST(EndpointsdeclineChatJoinRequest, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->declineChatJoinRequest(57234, 96347));
    ASSERT_NO_THROW(bot.get_endpoints()->declineChatJoinRequest("57234", 96347));
}

TEST(EndpointsdeclineChatJoinRequest, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->declineChatJoinRequest(57234, 96347), std::runtime_error);
    }
}

TEST(EndpointsdeleteChatPhoto, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->deleteChatPhoto(954146));
    ASSERT_NO_THROW(bot.get_endpoints()->deleteChatPhoto("954146"));
}

TEST(EndpointsdeleteChatPhoto, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->deleteChatPhoto(954146), std::runtime_error);
    }
}

TEST(EndpointsdeleteChatStickerSet, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->deleteChatStickerSet(17985614));
    ASSERT_NO_THROW(bot.get_endpoints()->deleteChatStickerSet("17985614"));
}

TEST(EndpointsdeleteChatStickerSet, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->deleteChatStickerSet(17985614), std::runtime_error);
    }
}

TEST(EndpointsdeleteForumTopic, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->deleteForumTopic(19357, 945374));
    ASSERT_NO_THROW(bot.get_endpoints()->deleteForumTopic("19357", 945374));
}

TEST(EndpointsdeleteForumTopic, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->deleteForumTopic(19357, 945374), std::runtime_error);
    }
}

TEST(EndpointsdeleteMessage, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->deleteMessage(7194156, 7949514));
    ASSERT_NO_THROW(bot.get_endpoints()->deleteMessage("7194156", 7949514));
}

TEST(EndpointsdeleteMessage, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->deleteMessage(7194156, 7949514), std::runtime_error);
    }
}

TEST(EndpointsdeleteMyCommands, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->deleteMyCommands(std::make_shared<tgbot::BotCommandScope>(), "sfhjpijh"));
}

TEST(EndpointsdeleteMyCommands, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->deleteMyCommands(std::make_shared<tgbot::BotCommandScope>(), "sfhjpijh"),
                     std::runtime_error);
    }
}

TEST(EndpointsdeleteStickerFromSet, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->deleteStickerFromSet("iwoicojowmfdhjoi"));
}

TEST(EndpointsdeleteStickerFromSet, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->deleteStickerFromSet("iwoicojowmfdhjoi"), std::runtime_error);
    }
}

TEST(EndpointsdeleteStickerSet, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->deleteStickerSet("düioeruoüasnd"));
}

TEST(EndpointsdeleteStickerSet, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->deleteStickerSet("düioeruoüasnd"), std::runtime_error);
    }
}

TEST(EndpointsdeleteWebhook, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->deleteWebhook(std::nullopt));
    ASSERT_NO_THROW(bot.get_endpoints()->deleteWebhook(true));
}

TEST(EndpointsdeleteWebhook, errors)
{
    const tgbot::Bot bot("error-domain", 5286, "token", std::nullopt, false);
    ASSERT_THROW(bot.get_endpoints()->deleteWebhook(std::nullopt), std::runtime_error);

    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->deleteWebhook(std::nullopt), std::runtime_error);
    }
}

TEST(EndpointseditChatInviteLink, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->editChatInviteLink(61181811, "djöfwgkljc", "rhpoijv", 4814354, 89614, false));
    ASSERT_NO_THROW(bot.get_endpoints()->editChatInviteLink("61181811", "djöfwgkljc", "rhpoijv", 4814354, 89614, false));
}

TEST(EndpointseditChatInviteLink, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->editChatInviteLink(61181811, "djöfwgkljc", "rhpoijv", 4814354, 89614, false),
                     std::runtime_error);
    }
}

TEST(EndpointseditForumTopic, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->editForumTopic(71357, 15946812, "ciuwhesvhjerpu", "aspduetdvs"));
    ASSERT_NO_THROW(bot.get_endpoints()->editForumTopic("71357", 15946812, "ciuwhesvhjerpu", "aspduetdvs"));
}

TEST(EndpointseditForumTopic, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->editForumTopic(71357, 15946812, "ciuwhesvhjerpu", "aspduetdvs"), std::runtime_error);
    }
}

TEST(EndpointseditGeneralForumTopic, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->editGeneralForumTopic(91732156, "dioapoxmpierh"));
    ASSERT_NO_THROW(bot.get_endpoints()->editGeneralForumTopic("91732156", "dioapoxmpierh"));
}

TEST(EndpointseditGeneralForumTopic, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->editGeneralForumTopic(91732156, "dioapoxmpierh"), std::runtime_error);
    }
}

TEST(EndpointseditMessageCaption, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->editMessageCaption(
      816354, 79925156, "powlmkjlf", "poicmkqölkjh", "jmhpojiwcg",
      std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()},
      std::make_shared<tgbot::InlineKeyboardMarkup>()));
    ASSERT_NO_THROW(bot.get_endpoints()->editMessageCaption(
      "816354", 79925156, "powlmkjlf", "poicmkqölkjh", "jmhpojiwcg",
      std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()},
      std::make_shared<tgbot::InlineKeyboardMarkup>()));

    const tgbot::Bot bot1("localhost", 5286, "token", "3", false);
    ASSERT_NO_THROW(bot1.get_endpoints()->editMessageCaption(
      816354, 79925156, "powlmkjlf", "poicmkqölkjh", "jmhpojiwcg",
      std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()},
      std::make_shared<tgbot::InlineKeyboardMarkup>()));
}

TEST(EndpointseditMessageCaption, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->editMessageCaption(
            816354, 79925156, "powlmkjlf", "poicmkqölkjh", "jmhpojiwcg",
            std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()},
            std::make_shared<tgbot::InlineKeyboardMarkup>()),
          std::runtime_error);
    }
}

TEST(EndpointseditMessageLiveLocation, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->editMessageLiveLocation(
      17831531, 781435324, "cimjwklfdhw", 681745.64834, 68453.8314, 38145.3145, 678145, 61832,
      std::make_shared<tgbot::InlineKeyboardMarkup>()));
    ASSERT_NO_THROW(bot.get_endpoints()->editMessageLiveLocation(
      "17831531", 781435324, "cimjwklfdhw", 681745.64834, 68453.8314, 38145.3145, 678145, 61832,
      std::make_shared<tgbot::InlineKeyboardMarkup>()));

    const tgbot::Bot bot1("localhost", 5286, "token", "3", false);
    ASSERT_NO_THROW(bot1.get_endpoints()->editMessageLiveLocation(
      17831531, 781435324, "cimjwklfdhw", 681745.64834, 68453.8314, 38145.3145, 678145, 61832,
      std::make_shared<tgbot::InlineKeyboardMarkup>()));
}

TEST(EndpointseditMessageLiveLocation, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->editMessageLiveLocation(
            17831531, 781435324, "cimjwklfdhw", 681745.64834, 68453.8314, 38145.3145, 678145, 61832,
            std::make_shared<tgbot::InlineKeyboardMarkup>()),
          std::runtime_error);
    }
}

TEST(EndpointseditMessageMedia, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);

    {
        const tgbot::InputMediaAnimation::ptr input_media_animation1 = std::make_shared<tgbot::InputMediaAnimation>();

        const tgbot::InputMediaAnimation::ptr input_media_animation2 = std::make_shared<tgbot::InputMediaAnimation>();
        input_media_animation2->set_media(test_file);
        input_media_animation2->set_thumbnail(test_file);

        ASSERT_NO_THROW(bot.get_endpoints()->editMessageMedia(
          8135354, 7922146, "opijtwhmof", input_media_animation1, std::make_shared<tgbot::InlineKeyboardMarkup>()));
        ASSERT_NO_THROW(bot.get_endpoints()->editMessageMedia(
          "8135354", 7922146, "opijtwhmof", input_media_animation2, std::make_shared<tgbot::InlineKeyboardMarkup>()));
    }

    {
        const tgbot::InputMediaAudio::ptr input_media_audio1 = std::make_shared<tgbot::InputMediaAudio>();

        const tgbot::InputMediaAudio::ptr input_media_audio2 = std::make_shared<tgbot::InputMediaAudio>();
        input_media_audio2->set_media(test_file);
        input_media_audio2->set_thumbnail(test_file);

        for(const tgbot::InputMediaAudio::ptr &input_media_audio : {input_media_audio1, input_media_audio2})
        {
            ASSERT_NO_THROW(bot.get_endpoints()->editMessageMedia(
              8135354, 7922146, "opijtwhmof", input_media_audio, std::make_shared<tgbot::InlineKeyboardMarkup>()));
        }
    }

    {
        const tgbot::InputMediaDocument::ptr input_media_document1 = std::make_shared<tgbot::InputMediaDocument>();

        const tgbot::InputMediaDocument::ptr input_media_document2 = std::make_shared<tgbot::InputMediaDocument>();
        input_media_document2->set_media(test_file);
        input_media_document2->set_thumbnail(test_file);

        for(const tgbot::InputMediaDocument::ptr &input_media_document : {input_media_document1, input_media_document2})
        {
            ASSERT_NO_THROW(bot.get_endpoints()->editMessageMedia(
              8135354, 7922146, "opijtwhmof", input_media_document, std::make_shared<tgbot::InlineKeyboardMarkup>()));
        }
    }

    {
        const tgbot::InputMediaPhoto::ptr input_media_photo1 = std::make_shared<tgbot::InputMediaPhoto>();

        const tgbot::InputMediaPhoto::ptr input_media_photo2 = std::make_shared<tgbot::InputMediaPhoto>();
        input_media_photo2->set_media(test_file);

        for(const tgbot::InputMediaPhoto::ptr &input_media_photo : {input_media_photo1, input_media_photo2})
        {
            ASSERT_NO_THROW(bot.get_endpoints()->editMessageMedia(
              8135354, 7922146, "opijtwhmof", input_media_photo, std::make_shared<tgbot::InlineKeyboardMarkup>()));
        }
    }

    {
        const tgbot::InputMediaVideo::ptr input_media_video1 = std::make_shared<tgbot::InputMediaVideo>();

        const tgbot::InputMediaVideo::ptr input_media_video2 = std::make_shared<tgbot::InputMediaVideo>();
        input_media_video2->set_media(test_file);
        input_media_video2->set_thumbnail(test_file);

        for(const tgbot::InputMediaVideo::ptr &input_media_video : {input_media_video1, input_media_video2})
        {
            ASSERT_NO_THROW(bot.get_endpoints()->editMessageMedia(
              8135354, 7922146, "opijtwhmof", input_media_video, std::make_shared<tgbot::InlineKeyboardMarkup>()));
        }
    }

    const tgbot::Bot bot1("localhost", 5286, "token", "3", false);
    ASSERT_NO_THROW(bot1.get_endpoints()->editMessageMedia(
      8135354, 7922146, "opijtwhmof", std::make_shared<tgbot::InputMediaAnimation>(),
      std::make_shared<tgbot::InlineKeyboardMarkup>()));
}

TEST(EndpointseditMessageMedia, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->editMessageMedia(
            8135354, 7922146, "opijtwhmof", std::make_shared<tgbot::InputMediaAnimation>(),
            std::make_shared<tgbot::InlineKeyboardMarkup>()),
          std::runtime_error);
    }
}

TEST(EndpointseditMessageReplyMarkup, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->editMessageReplyMarkup(
      81546, 14682, "powkjmfhj", std::make_shared<tgbot::InlineKeyboardMarkup>()));
    ASSERT_NO_THROW(bot.get_endpoints()->editMessageReplyMarkup(
      "81546", 14682, "powkjmfhj", std::make_shared<tgbot::InlineKeyboardMarkup>()));

    const tgbot::Bot bot1("localhost", 5286, "token", "3", false);
    ASSERT_NO_THROW(bot1.get_endpoints()->editMessageReplyMarkup(
      81546, 14682, "powkjmfhj", std::make_shared<tgbot::InlineKeyboardMarkup>()));
}

TEST(EndpointseditMessageReplyMarkup, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->editMessageReplyMarkup(
            81546, 14682, "powkjmfhj", std::make_shared<tgbot::InlineKeyboardMarkup>()),
          std::runtime_error);
    }
}

TEST(EndpointseditMessageText, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->editMessageText(
      8413124, 7951354, "doipqmhf", "pwlnilcg", "poicnqh",
      std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, true,
      std::make_shared<tgbot::InlineKeyboardMarkup>()));
    ASSERT_NO_THROW(bot.get_endpoints()->editMessageText(
      "8413124", 7951354, "doipqmhf", "pwlnilcg", "poicnqh",
      std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, true,
      std::make_shared<tgbot::InlineKeyboardMarkup>()));

    const tgbot::Bot bot1("localhost", 5286, "token", "3", false);
    ASSERT_NO_THROW(bot1.get_endpoints()->editMessageText(
      8413124, 7951354, "doipqmhf", "pwlnilcg", "poicnqh",
      std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, true,
      std::make_shared<tgbot::InlineKeyboardMarkup>()));
}

TEST(EndpointseditMessageText, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->editMessageText(
            8413124, 7951354, "doipqmhf", "pwlnilcg", "poicnqh",
            std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, true,
            std::make_shared<tgbot::InlineKeyboardMarkup>()),
          std::runtime_error);
    }
}

TEST(EndpointsexportChatInviteLink, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->exportChatInviteLink(4941331));
    ASSERT_NO_THROW(bot.get_endpoints()->exportChatInviteLink("4941331"));
}

TEST(EndpointsexportChatInviteLink, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->exportChatInviteLink(4941331), std::runtime_error);
    }
}

TEST(EndpointsforwardMessage, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->forwardMessage(6814624, 816421, 416428, false, true, 41354));
    ASSERT_NO_THROW(bot.get_endpoints()->forwardMessage("6814624", 816421, "416428", false, true, 41354));
}

TEST(EndpointsforwardMessage, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->forwardMessage(6814624, 816421, 416428, false, true, 41354), std::runtime_error);
    }
}

TEST(EndpointsgetChat, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->getChat(17683241));
    ASSERT_NO_THROW(bot.get_endpoints()->getChat("17683241"));
}

TEST(EndpointsgetChat, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->getChat(17683241), std::runtime_error);
    }
}

TEST(EndpointsgetChatAdministrators, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->getChatAdministrators(914162));
    ASSERT_NO_THROW(bot.get_endpoints()->getChatAdministrators("914162"));

    {
        const tgbot::Bot bot("localhost", 5286, "token", "administrator", false);
        const std::vector<tgbot::ChatMember::ptr> chat_administrators =
          bot.get_endpoints()->getChatAdministrators(914162);
        ASSERT_EQ(chat_administrators.size(), 1);

        tgbot::ChatMemberAdministrator chat_member_administrator;
        chat_member_administrator.set_status("administrator");
        ASSERT_EQ(chat_administrators.at(0)->serialise(), chat_member_administrator.serialise());
    }

    {
        const tgbot::Bot bot("localhost", 5286, "token", "kicked", false);
        const std::vector<tgbot::ChatMember::ptr> chat_administrators =
          bot.get_endpoints()->getChatAdministrators(914162);
        ASSERT_EQ(chat_administrators.size(), 1);

        tgbot::ChatMemberBanned chat_member_banned;
        chat_member_banned.set_status("kicked");
        ASSERT_EQ(chat_administrators.at(0)->serialise(), chat_member_banned.serialise());
    }

    {
        const tgbot::Bot bot("localhost", 5286, "token", "left", false);
        const std::vector<tgbot::ChatMember::ptr> chat_administrators =
          bot.get_endpoints()->getChatAdministrators(914162);
        ASSERT_EQ(chat_administrators.size(), 1);

        tgbot::ChatMemberLeft chat_member_left;
        chat_member_left.set_status("left");
        ASSERT_EQ(chat_administrators.at(0)->serialise(), chat_member_left.serialise());
    }

    {
        const tgbot::Bot bot("localhost", 5286, "token", "member", false);
        const std::vector<tgbot::ChatMember::ptr> chat_administrators =
          bot.get_endpoints()->getChatAdministrators(914162);
        ASSERT_EQ(chat_administrators.size(), 1);

        tgbot::ChatMemberMember chat_member_member;
        chat_member_member.set_status("member");
        ASSERT_EQ(chat_administrators.at(0)->serialise(), chat_member_member.serialise());
    }

    {
        const tgbot::Bot bot("localhost", 5286, "token", "creator", false);
        const std::vector<tgbot::ChatMember::ptr> chat_administrators =
          bot.get_endpoints()->getChatAdministrators(914162);
        ASSERT_EQ(chat_administrators.size(), 1);

        tgbot::ChatMemberOwner chat_member_owner;
        chat_member_owner.set_status("creator");
        ASSERT_EQ(chat_administrators.at(0)->serialise(), chat_member_owner.serialise());
    }

    {
        const tgbot::Bot bot("localhost", 5286, "token", "restricted", false);
        const std::vector<tgbot::ChatMember::ptr> chat_administrators =
          bot.get_endpoints()->getChatAdministrators(914162);
        ASSERT_EQ(chat_administrators.size(), 1);

        tgbot::ChatMemberRestricted chat_member_restricted;
        chat_member_restricted.set_status("restricted");
        ASSERT_EQ(chat_administrators.at(0)->serialise(), chat_member_restricted.serialise());
    }
}

TEST(EndpointsgetChatAdministrators, errors)
{
    for(std::int8_t i = -1; i < 7; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->getChatAdministrators(914162), std::runtime_error);
    }
}

TEST(EndpointsgetChatMember, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->getChatMember(14951, 841354));
    ASSERT_NO_THROW(bot.get_endpoints()->getChatMember("14951", 841354));

    {
        const tgbot::Bot bot("localhost", 5286, "token", "administrator", false);
        tgbot::ChatMemberAdministrator chat_member_administrator;
        chat_member_administrator.set_status("administrator");
        ASSERT_EQ(bot.get_endpoints()->getChatMember(14951, 841354)->serialise(), chat_member_administrator.serialise());
    }

    {
        const tgbot::Bot bot("localhost", 5286, "token", "kicked", false);
        tgbot::ChatMemberBanned chat_member_banned;
        chat_member_banned.set_status("kicked");
        ASSERT_EQ(bot.get_endpoints()->getChatMember(14951, 841354)->serialise(), chat_member_banned.serialise());
    }

    {
        const tgbot::Bot bot("localhost", 5286, "token", "left", false);
        tgbot::ChatMemberLeft chat_member_left;
        chat_member_left.set_status("left");
        ASSERT_EQ(bot.get_endpoints()->getChatMember(14951, 841354)->serialise(), chat_member_left.serialise());
    }

    {
        const tgbot::Bot bot("localhost", 5286, "token", "member", false);
        tgbot::ChatMemberMember chat_member_member;
        chat_member_member.set_status("member");
        ASSERT_EQ(bot.get_endpoints()->getChatMember(14951, 841354)->serialise(), chat_member_member.serialise());
    }

    {
        const tgbot::Bot bot("localhost", 5286, "token", "creator", false);
        tgbot::ChatMemberOwner chat_member_owner;
        chat_member_owner.set_status("creator");
        ASSERT_EQ(bot.get_endpoints()->getChatMember(14951, 841354)->serialise(), chat_member_owner.serialise());
    }

    {
        const tgbot::Bot bot("localhost", 5286, "token", "restricted", false);
        tgbot::ChatMemberRestricted chat_member_restricted;
        chat_member_restricted.set_status("restricted");
        ASSERT_EQ(bot.get_endpoints()->getChatMember(14951, 841354)->serialise(), chat_member_restricted.serialise());
    }
}

TEST(EndpointsgetChatMember, errors)
{
    for(std::int8_t i = -1; i < 6; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->getChatMember(14951, 841354), std::runtime_error);
    }
}

TEST(EndpointsgetChatMemberCount, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->getChatMemberCount(63218498));
    ASSERT_NO_THROW(bot.get_endpoints()->getChatMemberCount("63218498"));
}

TEST(EndpointsgetChatMemberCount, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->getChatMemberCount(63218498), std::runtime_error);
    }
}

TEST(EndpointsgetChatMenuButton, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->getChatMenuButton(std::nullopt));
    ASSERT_NO_THROW(bot.get_endpoints()->getChatMenuButton(465798));
}

TEST(EndpointsgetChatMenuButton, errors)
{
    const tgbot::Bot bot("error-domain", 5286, "token", std::nullopt, false);
    ASSERT_THROW(bot.get_endpoints()->getChatMenuButton(std::nullopt), std::runtime_error);

    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->getChatMenuButton(std::nullopt), std::runtime_error);
    }
}

TEST(EndpointsgetCustomEmojiStickers, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->getCustomEmojiStickers({"xcavpnoiet"}));
}

TEST(EndpointsgetCustomEmojiStickers, errors)
{
    for(std::int8_t i = -1; i < 4; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->getCustomEmojiStickers({"xcavpnoiet"}), std::runtime_error);
    }
}

TEST(EndpointsgetFile, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->getFile("coijpjoim"));
}

TEST(EndpointsgetFile, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->getFile("coijpjoim"), std::runtime_error);
    }
}

TEST(EndpointsgetForumTopicIconStickers, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->getForumTopicIconStickers());
}

TEST(EndpointsgetForumTopicIconStickers, errors)
{
    const tgbot::Bot bot("error-domain", 5286, "token", std::nullopt, false);
    ASSERT_THROW(bot.get_endpoints()->getForumTopicIconStickers(), std::runtime_error);

    for(std::int8_t i = -1; i < 4; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->getForumTopicIconStickers(), std::runtime_error);
    }
}

TEST(EndpointsgetGameHighScores, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->getGameHighScores(8167315, 61832, 186915, "embfmölwölr"));
}

TEST(EndpointsgetGameHighScores, errors)
{
    for(std::int8_t i = -1; i < 4; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->getGameHighScores(8167315, 61832, 186915, "embfmölwölr"), std::runtime_error);
    }
}

TEST(EndpointsgetMe, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->getMe());
}

TEST(EndpointsgetMe, errors)
{
    const tgbot::Bot bot("error-domain", 5286, "token", std::nullopt, false);
    ASSERT_THROW(bot.get_endpoints()->getMe(), std::runtime_error);

    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->getMe(), std::runtime_error);
    }
}

TEST(EndpointsgetMyCommands, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->getMyCommands(std::nullopt, std::nullopt));
    ASSERT_NO_THROW(bot.get_endpoints()->getMyCommands(
      std::make_shared<tgbot::BotCommandScope>(),
      "bnioufnhgd"
      "s"));
}

TEST(EndpointsgetMyCommands, errors)
{
    const tgbot::Bot bot("error-domain", 5286, "token", std::nullopt, false);
    ASSERT_THROW(bot.get_endpoints()->getMyCommands(std::nullopt, std::nullopt), std::runtime_error);

    for(std::int8_t i = -1; i < 4; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->getMyCommands(std::nullopt, std::nullopt), std::runtime_error);
    }
}

TEST(EndpointsgetMyDefaultAdministratorRights, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->getMyDefaultAdministratorRights(std::nullopt));
    ASSERT_NO_THROW(bot.get_endpoints()->getMyDefaultAdministratorRights(false));
}

TEST(EndpointsgetMyDefaultAdministratorRights, errors)
{
    const tgbot::Bot bot("error-domain", 5286, "token", std::nullopt, false);
    ASSERT_THROW(bot.get_endpoints()->getMyDefaultAdministratorRights(std::nullopt), std::runtime_error);

    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->getMyDefaultAdministratorRights(std::nullopt), std::runtime_error);
    }
}

TEST(EndpointsgetMyDescription, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->getMyDescription(std::nullopt));
    ASSERT_NO_THROW(bot.get_endpoints()->getMyDescription("hnxmaüprq"));
}

TEST(EndpointsgetMyDescription, errors)
{
    const tgbot::Bot bot("error-domain", 5286, "token", std::nullopt, false);
    ASSERT_THROW(bot.get_endpoints()->getMyDescription(std::nullopt), std::runtime_error);

    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->getMyDescription(std::nullopt), std::runtime_error);
    }
}

TEST(EndpointsgetMyShortDescription, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->getMyShortDescription(std::nullopt));
    ASSERT_NO_THROW(bot.get_endpoints()->getMyShortDescription("pqwymgqwpoegsdj"));
}

TEST(EndpointsgetMyShortDescription, errors)
{
    const tgbot::Bot bot("error-domain", 5286, "token", std::nullopt, false);
    ASSERT_THROW(bot.get_endpoints()->getMyShortDescription(std::nullopt), std::runtime_error);

    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->getMyShortDescription(std::nullopt), std::runtime_error);
    }
}

TEST(EndpointsgetStickerSet, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->getStickerSet("qpoijmcg"));
}

TEST(EndpointsgetStickerSet, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->getStickerSet("qpoijmcg"), std::runtime_error);
    }
}

TEST(EndpointsgetUpdates, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->getUpdates(std::nullopt, std::nullopt, std::nullopt, std::nullopt));
    ASSERT_NO_THROW(bot.get_endpoints()->getUpdates(64224, 31841, 183142, std::vector<std::string> {"lerlkjsmd"}));
}

TEST(EndpointsgetUpdates, errors)
{
    const tgbot::Bot bot("error-domain", 5286, "token", std::nullopt, false);
    ASSERT_THROW(
      bot.get_endpoints()->getUpdates(std::nullopt, std::nullopt, std::nullopt, std::nullopt), std::runtime_error);

    for(std::int8_t i = -1; i < 4; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->getUpdates(std::nullopt, std::nullopt, std::nullopt, std::nullopt), std::runtime_error);
    }
}

TEST(EndpointsgetUserProfilePhotos, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->getUserProfilePhotos(861354634, 6841, 6844531));
}

TEST(EndpointsgetUserProfilePhotos, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->getUserProfilePhotos(861354634, 6841, 6844531), std::runtime_error);
    }
}

TEST(EndpointsgetWebhookInfo, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->getWebhookInfo());
}

TEST(EndpointsgetWebhookInfo, errors)
{
    const tgbot::Bot bot("error-domain", 5286, "token", std::nullopt, false);
    ASSERT_THROW(bot.get_endpoints()->getWebhookInfo(), std::runtime_error);

    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->getWebhookInfo(), std::runtime_error);
    }
}

TEST(EndpointshideGeneralForumTopic, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->hideGeneralForumTopic(89215));
    ASSERT_NO_THROW(bot.get_endpoints()->hideGeneralForumTopic("89215"));
}

TEST(EndpointshideGeneralForumTopic, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->hideGeneralForumTopic(89215), std::runtime_error);
    }
}

TEST(EndpointsleaveChat, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->leaveChat(7198341));
    ASSERT_NO_THROW(bot.get_endpoints()->leaveChat("7198341"));
}

TEST(EndpointsleaveChat, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->leaveChat(7198341), std::runtime_error);
    }
}

TEST(EndpointslogOut, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->logOut());
}

TEST(EndpointslogOut, errors)
{
    const tgbot::Bot bot("error-domain", 5286, "token", std::nullopt, false);
    ASSERT_THROW(bot.get_endpoints()->logOut(), std::runtime_error);

    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->logOut(), std::runtime_error);
    }
}

TEST(EndpointspinChatMessage, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->pinChatMessage(49812, 781624, false));
    ASSERT_NO_THROW(bot.get_endpoints()->pinChatMessage("49812", 781624, false));
}

TEST(EndpointspinChatMessage, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->pinChatMessage(49812, 781624, false), std::runtime_error);
    }
}

TEST(EndpointspromoteChatMember, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->promoteChatMember(
      681351, 354511, true, false, true, false, false, true, true, false, true, false, false, true));
    ASSERT_NO_THROW(bot.get_endpoints()->promoteChatMember(
      "681351", 354511, true, false, true, false, false, true, true, false, true, false, false, true));
}

TEST(EndpointspromoteChatMember, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->promoteChatMember(
            681351, 354511, true, false, true, false, false, true, true, false, true, false, false, true),
          std::runtime_error);
    }
}

TEST(EndpointsreopenForumTopic, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->reopenForumTopic(546816654, 665421365));
    ASSERT_NO_THROW(bot.get_endpoints()->reopenForumTopic("546816654", 665421365));
}

TEST(EndpointsreopenForumTopic, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->reopenForumTopic(546816654, 665421365), std::runtime_error);
    }
}

TEST(EndpointsreopenGeneralForumTopic, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->reopenGeneralForumTopic(9137465));
    ASSERT_NO_THROW(bot.get_endpoints()->reopenGeneralForumTopic("9137465"));
}

TEST(EndpointsreopenGeneralForumTopic, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->reopenGeneralForumTopic(9137465), std::runtime_error);
    }
}

TEST(EndpointsrestrictChatMember, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->restrictChatMember(
      83131, 68171453, std::make_shared<tgbot::ChatPermissions>(), true, 614531));
    ASSERT_NO_THROW(bot.get_endpoints()->restrictChatMember(
      "83131", 68171453, std::make_shared<tgbot::ChatPermissions>(), true, 614531));
}

TEST(EndpointsrestrictChatMember, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->restrictChatMember(
            83131, 68171453, std::make_shared<tgbot::ChatPermissions>(), true, 614531),
          std::runtime_error);
    }
}

TEST(EndpointsrevokeChatInviteLink, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->revokeChatInviteLink(165412, "dpckmwpjohf"));
    ASSERT_NO_THROW(bot.get_endpoints()->revokeChatInviteLink("165412", "dpckmwpjohf"));
}

TEST(EndpointsrevokeChatInviteLink, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->revokeChatInviteLink(165412, "dpckmwpjohf"), std::runtime_error);
    }
}

TEST(EndpointssendAnimation, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->sendAnimation(
      148942, 46516, "cöoiqpfhth", 14892, 14253, 7899642, "wpoifmwteh", "woimpchw", "cpomiwfhwf",
      std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, false, false, true, 6184684,
      true, std::make_shared<tgbot::Reply>()));
    ASSERT_NO_THROW(bot.get_endpoints()->sendAnimation(
      "148942", 46516, test_file, 14892, 14253, 7899642, test_file, "woimpchw", "cpomiwfhwf",
      std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, false, false, true, 6184684,
      true, std::make_shared<tgbot::Reply>()));
}

TEST(EndpointssendAnimation, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->sendAnimation(
            148942, 46516, "cöoiqpfhth", 14892, 14253, 7899642, "wpoifmwteh", "woimpchw", "cpomiwfhwf",
            std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, false, false, true,
            6184684, true, std::make_shared<tgbot::Reply>()),
          std::runtime_error);
    }
}

TEST(EndpointssendAudio, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->sendAudio(
      442315, 94354, "össdfhweg", "öslkmcerh", "wöölgkmmlke",
      std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, 176841, "lcsnlkdrhf",
      "vsijniodjhfg", "slömbdfhth", false, false, 146825, true, std::make_shared<tgbot::Reply>()));
    ASSERT_NO_THROW(bot.get_endpoints()->sendAudio(
      "442315", 94354, test_file, "öslkmcerh", "wöölgkmmlke",
      std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, 176841, "lcsnlkdrhf",
      "vsijniodjhfg", test_file, false, false, 146825, true, std::make_shared<tgbot::Reply>()));
}

TEST(EndpointssendAudio, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->sendAudio(
            442315, 94354, "össdfhweg", "öslkmcerh", "wöölgkmmlke",
            std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, 176841, "lcsnlkdrhf",
            "vsijniodjhfg", "slömbdfhth", false, false, 146825, true, std::make_shared<tgbot::Reply>()),
          std::runtime_error);
    }
}

TEST(EndpointssendChatAction, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->sendChatAction(681315, 8942, "qopijölmcag"));
    ASSERT_NO_THROW(bot.get_endpoints()->sendChatAction("681315", 8942, "qopijölmcag"));
}

TEST(EndpointssendChatAction, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->sendChatAction(681315, 8942, "qopijölmcag"), std::runtime_error);
    }
}

TEST(EndpointssendContact, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->sendContact(
      81315321, 98165, "wöoijömfhw", "cjömökfhw", "cpoiiopmnjwwfh", "cljjmwfd", true, false, 64534, true,
      std::make_shared<tgbot::Reply>()));
    ASSERT_NO_THROW(bot.get_endpoints()->sendContact(
      "81315321", 98165, "wöoijömfhw", "cjömökfhw", "cpoiiopmnjwwfh", "cljjmwfd", true, false, 64534, true,
      std::make_shared<tgbot::Reply>()));
}

TEST(EndpointssendContact, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->sendContact(
            81315321, 98165, "wöoijömfhw", "cjömökfhw", "cpoiiopmnjwwfh", "cljjmwfd", true, false, 64534, true,
            std::make_shared<tgbot::Reply>()),
          std::runtime_error);
    }
}

TEST(EndpointssendDice, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->sendDice(
      861351, 613644, "poiwmöhf", false, false, 687131, true, std::make_shared<tgbot::Reply>()));
    ASSERT_NO_THROW(bot.get_endpoints()->sendDice(
      "861351", 613644, "poiwmöhf", false, false, 687131, true, std::make_shared<tgbot::Reply>()));
}

TEST(EndpointssendDice, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->sendDice(
            861351, 613644, "poiwmöhf", false, false, 687131, true, std::make_shared<tgbot::Reply>()),
          std::runtime_error);
    }
}

TEST(EndpointssendDocument, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->sendDocument(
      1468425, 49985, "völkmsdth", "öiomfbsdfh", "cqpjfmkbjsdfg", "coisdseh",
      std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, false, true, false, 4165245,
      false, std::make_shared<tgbot::Reply>()));
    ASSERT_NO_THROW(bot.get_endpoints()->sendDocument(
      "1468425", 49985, test_file, test_file, "cqpjfmkbjsdfg", "coisdseh",
      std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, false, true, false, 4165245,
      false, std::make_shared<tgbot::Reply>()));
}

TEST(EndpointssendDocument, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->sendDocument(
            1468425, 49985, "völkmsdth", "öiomfbsdfh", "cqpjfmkbjsdfg", "coisdseh",
            std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, false, true, false,
            4165245, false, std::make_shared<tgbot::Reply>()),
          std::runtime_error);
    }
}

TEST(EndpointssendGame, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->sendGame(
      71632165, 416456, "wpoifmwg", false, true, 681354, true, std::make_shared<tgbot::InlineKeyboardMarkup>()));
}

TEST(EndpointssendGame, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->sendGame(
            71632165, 416456, "wpoifmwg", false, true, 681354, true, std::make_shared<tgbot::InlineKeyboardMarkup>()),
          std::runtime_error);
    }
}

TEST(EndpointssendInvoice, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->sendInvoice(
      13488134, 76254, "qpofhöwtkjhlk", "wjfoilökjsfd", "wjeflkcslgkj", "cjowmpjofh", "tjlmsvkml",
      std::vector<tgbot::LabeledPrice::ptr> {std::make_shared<tgbot::LabeledPrice>()}, 8940131,
      std::vector<std::int32_t> {810350}, "cjllkfg", "iemamdg", "cijoojmfg", 41665, 561, 8654164, true, false, true,
      false, false, true, true, false, false, 166551, true, std::make_shared<tgbot::InlineKeyboardMarkup>()));
    ASSERT_NO_THROW(bot.get_endpoints()->sendInvoice(
      "13488134", 76254, "qpofhöwtkjhlk", "wjfoilökjsfd", "wjeflkcslgkj", "cjowmpjofh", "tjlmsvkml",
      std::vector<tgbot::LabeledPrice::ptr> {std::make_shared<tgbot::LabeledPrice>()}, 8940131,
      std::vector<std::int32_t> {810350}, "cjllkfg", "iemamdg", "cijoojmfg", 41665, 561, 8654164, true, false, true,
      false, false, true, true, false, false, 166551, true, std::make_shared<tgbot::InlineKeyboardMarkup>()));
}

TEST(EndpointssendInvoice, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->sendInvoice(
            13488134, 76254, "qpofhöwtkjhlk", "wjfoilökjsfd", "wjeflkcslgkj", "cjowmpjofh", "tjlmsvkml",
            std::vector<tgbot::LabeledPrice::ptr> {std::make_shared<tgbot::LabeledPrice>()}, 8940131,
            std::vector<std::int32_t> {810350}, "cjllkfg", "iemamdg", "cijoojmfg", 41665, 561, 8654164, true, false,
            true, false, false, true, true, false, false, 166551, true, std::make_shared<tgbot::InlineKeyboardMarkup>()),
          std::runtime_error);
    }
}

TEST(EndpointssendLocation, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->sendLocation(
      416453, 781, 83173.3418, 81646.6416, 174.314, 534534, 6781543, 1342418, false, true, 344351, true,
      std::make_shared<tgbot::Reply>()));
    ASSERT_NO_THROW(bot.get_endpoints()->sendLocation(
      "416453", 781, 83173.3418, 81646.6416, 174.314, 534534, 6781543, 1342418, false, true, 344351, true,
      std::make_shared<tgbot::Reply>()));
}

TEST(EndpointssendLocation, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->sendLocation(
            416453, 781, 83173.3418, 81646.6416, 174.314, 534534, 6781543, 1342418, false, true, 344351, true,
            std::make_shared<tgbot::Reply>()),
          std::runtime_error);
    }
}

TEST(EndpointssendMediaGroup, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);

    {
        const tgbot::InputMediaAudio::ptr input_media_audio1 = std::make_shared<tgbot::InputMediaAudio>();

        const tgbot::InputMediaAudio::ptr input_media_audio2 = std::make_shared<tgbot::InputMediaAudio>();
        input_media_audio2->set_media(test_file);
        input_media_audio2->set_thumbnail(test_file);

        ASSERT_NO_THROW(bot.get_endpoints()->sendMediaGroup(
          14645, 9515,
          std::vector<std::variant<tgbot::InputMediaAudio::ptr, tgbot::InputMediaDocument::ptr,
                                   tgbot::InputMediaPhoto::ptr, tgbot::InputMediaVideo::ptr>> {input_media_audio1},
          true, false, 6817543, false));
        ASSERT_NO_THROW(bot.get_endpoints()->sendMediaGroup(
          "14645", 9515,
          std::vector<std::variant<tgbot::InputMediaAudio::ptr, tgbot::InputMediaDocument::ptr,
                                   tgbot::InputMediaPhoto::ptr, tgbot::InputMediaVideo::ptr>> {input_media_audio2},
          true, false, 6817543, false));
    }

    {
        const tgbot::InputMediaDocument::ptr input_media_document1 = std::make_shared<tgbot::InputMediaDocument>();

        const tgbot::InputMediaDocument::ptr input_media_document2 = std::make_shared<tgbot::InputMediaDocument>();
        input_media_document2->set_media(test_file);
        input_media_document2->set_thumbnail(test_file);

        for(const tgbot::InputMediaDocument::ptr &input_media_document : {input_media_document1, input_media_document2})
        {
            ASSERT_NO_THROW(bot.get_endpoints()->sendMediaGroup(
              14645, 9515,
              std::vector<std::variant<tgbot::InputMediaAudio::ptr, tgbot::InputMediaDocument::ptr,
                                       tgbot::InputMediaPhoto::ptr, tgbot::InputMediaVideo::ptr>> {input_media_document},
              true, false, 6817543, false));
        }
    }

    {
        const tgbot::InputMediaPhoto::ptr input_media_photo1 = std::make_shared<tgbot::InputMediaPhoto>();

        const tgbot::InputMediaPhoto::ptr input_media_photo2 = std::make_shared<tgbot::InputMediaPhoto>();
        input_media_photo2->set_media(test_file);

        for(const tgbot::InputMediaPhoto::ptr &input_media_photo : {input_media_photo1, input_media_photo2})
        {
            ASSERT_NO_THROW(bot.get_endpoints()->sendMediaGroup(
              14645, 9515,
              std::vector<std::variant<tgbot::InputMediaAudio::ptr, tgbot::InputMediaDocument::ptr,
                                       tgbot::InputMediaPhoto::ptr, tgbot::InputMediaVideo::ptr>> {input_media_photo},
              true, false, 6817543, false));
        }
    }

    {
        const tgbot::InputMediaVideo::ptr input_media_video1 = std::make_shared<tgbot::InputMediaVideo>();

        const tgbot::InputMediaVideo::ptr input_media_video2 = std::make_shared<tgbot::InputMediaVideo>();
        input_media_video2->set_media(test_file);
        input_media_video2->set_thumbnail(test_file);

        for(const tgbot::InputMediaVideo::ptr &input_media_video : {input_media_video1, input_media_video2})
        {
            ASSERT_NO_THROW(bot.get_endpoints()->sendMediaGroup(
              14645, 9515,
              std::vector<std::variant<tgbot::InputMediaAudio::ptr, tgbot::InputMediaDocument::ptr,
                                       tgbot::InputMediaPhoto::ptr, tgbot::InputMediaVideo::ptr>> {input_media_video},
              true, false, 6817543, false));
        }
    }
}

TEST(EndpointssendMediaGroup, errors)
{
    for(std::int8_t i = -1; i < 4; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->sendMediaGroup(
            14645, 9515,
            std::vector<std::variant<tgbot::InputMediaAudio::ptr, tgbot::InputMediaDocument::ptr,
                                     tgbot::InputMediaPhoto::ptr, tgbot::InputMediaVideo::ptr>> {
              std::make_shared<tgbot::InputMediaAudio>()},
            true, false, 6817543, false),
          std::runtime_error);
    }
}

TEST(EndpointssendMessage, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->sendMessage(
      61564256, 79518, "södjfgoisg", "öodjfgsdfg",
      std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, false, true, false, 451641,
      false, std::make_shared<tgbot::Reply>()));
    ASSERT_NO_THROW(bot.get_endpoints()->sendMessage(
      "61564256", 79518, "södjfgoisg", "öodjfgsdfg",
      std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, false, true, false, 451641,
      false, std::make_shared<tgbot::Reply>()));
}

TEST(EndpointssendMessage, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->sendMessage(
            61564256, 79518, "södjfgoisg", "öodjfgsdfg",
            std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, false, true, false,
            451641, false, std::make_shared<tgbot::Reply>()),
          std::runtime_error);
    }
}

TEST(EndpointssendPhoto, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->sendPhoto(
      618462841, 54923, "sldmfgsdfg", "söoifsdfhrth", "slkdjfheh",
      std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, false, true, true, 146814,
      true, std::make_shared<tgbot::Reply>()));
    ASSERT_NO_THROW(bot.get_endpoints()->sendPhoto(
      "618462841", 54923, test_file, "söoifsdfhrth", "slkdjfheh",
      std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, false, true, true, 146814,
      true, std::make_shared<tgbot::Reply>()));
}

TEST(EndpointssendPhoto, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->sendPhoto(
            618462841, 54923, "sldmfgsdfg", "söoifsdfhrth", "slkdjfheh",
            std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, false, true, true,
            146814, true, std::make_shared<tgbot::Reply>()),
          std::runtime_error);
    }
}

TEST(EndpointssendPoll, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->sendPoll(
      6813131, 781681, "qoipjoiöfmmb", {"wpmbmwm"}, false, "wöoijmöclmökx", true, 8716435, "qmölmölag", "qpoijmfiog",
      std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, 89761, 98642, false, true,
      true, 87914, false, std::make_shared<tgbot::Reply>()));
    ASSERT_NO_THROW(bot.get_endpoints()->sendPoll(
      "6813131", 781681, "qoipjoiöfmmb", {"wpmbmwm"}, false, "wöoijmöclmökx", true, 8716435, "qmölmölag", "qpoijmfiog",
      std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, 89761, 98642, false, true,
      true, 87914, false, std::make_shared<tgbot::Reply>()));
}

TEST(EndpointssendPoll, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->sendPoll(
            6813131, 781681, "qoipjoiöfmmb", {"wpmbmwm"}, false, "wöoijmöclmökx", true, 8716435, "qmölmölag",
            "qpoijmfiog", std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, 89761,
            98642, false, true, true, 87914, false, std::make_shared<tgbot::Reply>()),
          std::runtime_error);
    }
}

TEST(EndpointssendSticker, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->sendSticker(
      8614665, 91519, "wofkmwc", "oxnaipsurh", true, false, 713424, true, std::make_shared<tgbot::Reply>()));
    ASSERT_NO_THROW(bot.get_endpoints()->sendSticker(
      "8614665", 91519, test_file, "oxnaipsurh", true, false, 713424, true, std::make_shared<tgbot::Reply>()));
}

TEST(EndpointssendSticker, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->sendSticker(
            8614665, 91519, "wofkmwc", "oxnaipsurh", true, false, 713424, true, std::make_shared<tgbot::Reply>()),
          std::runtime_error);
    }
}

TEST(EndpointssendVenue, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->sendVenue(
      18631351, 986618, 618731.188, 1835.145, "öoiswvm", "sllvslköv", "coijmwiomjdfh", "qolmwfg", "clkjkmdfh",
      "öwimwöim", false, true, 68174, true, std::make_shared<tgbot::Reply>()));
    ASSERT_NO_THROW(bot.get_endpoints()->sendVenue(
      "18631351", 986618, 618731.188, 1835.145, "öoiswvm", "sllvslköv", "coijmwiomjdfh", "qolmwfg", "clkjkmdfh",
      "öwimwöim", false, true, 68174, true, std::make_shared<tgbot::Reply>()));
}

TEST(EndpointssendVenue, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->sendVenue(
            18631351, 986618, 618731.188, 1835.145, "öoiswvm", "sllvslköv", "coijmwiomjdfh", "qolmwfg", "clkjkmdfh",
            "öwimwöim", false, true, 68174, true, std::make_shared<tgbot::Reply>()),
          std::runtime_error);
    }
}

TEST(EndpointssendVideo, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->sendVideo(
      168251, 98416, "cöosmdfh", 415324351, 145245, 176842498, "ösoetheg", "cimjwerhqpv", "cöjmerhqoijc",
      std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, true, false, true, false,
      146542, true, std::make_shared<tgbot::Reply>()));
    ASSERT_NO_THROW(bot.get_endpoints()->sendVideo(
      "168251", 98416, test_file, 415324351, 145245, 176842498, test_file, "cimjwerhqpv", "cöjmerhqoijc",
      std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, true, false, true, false,
      146542, true, std::make_shared<tgbot::Reply>()));
}

TEST(EndpointssendVideo, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->sendVideo(
            168251, 98416, "cöosmdfh", 415324351, 145245, 176842498, "ösoetheg", "cimjwerhqpv", "cöjmerhqoijc",
            std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, true, false, true, false,
            146542, true, std::make_shared<tgbot::Reply>()),
          std::runtime_error);
    }
}

TEST(EndpointssendVideoNote, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->sendVideoNote(
      8662351, 9861564, "linhsjlkf", 6165425, 6313454, "vsdlsjoi", true, true, 63181324, false,
      std::make_shared<tgbot::Reply>()));
    ASSERT_NO_THROW(bot.get_endpoints()->sendVideoNote(
      "8662351", 9861564, test_file, 6165425, 6313454, test_file, true, true, 63181324, false,
      std::make_shared<tgbot::Reply>()));
}

TEST(EndpointssendVideoNote, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->sendVideoNote(
            8662351, 9861564, "linhsjlkf", 6165425, 6313454, "vsdlsjoi", true, true, 63181324, false,
            std::make_shared<tgbot::Reply>()),
          std::runtime_error);
    }
}

TEST(EndpointssendVoice, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->sendVoice(
      17893254, 86516651, "cöoiwqnpfh", "wümthcsm", "qpoimcsdfh",
      std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, 68164, false, false, 6151,
      true, std::make_shared<tgbot::Reply>()));
    ASSERT_NO_THROW(bot.get_endpoints()->sendVoice(
      "17893254", 86516651, test_file, "wümthcsm", "qpoimcsdfh",
      std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, 68164, false, false, 6151,
      true, std::make_shared<tgbot::Reply>()));
}

TEST(EndpointssendVoice, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->sendVoice(
            17893254, 86516651, "cöoiwqnpfh", "wümthcsm", "qpoimcsdfh",
            std::vector<tgbot::MessageEntity::ptr> {std::make_shared<tgbot::MessageEntity>()}, 68164, false, false,
            6151, true, std::make_shared<tgbot::Reply>()),
          std::runtime_error);
    }
}

TEST(EndpointssetChatAdministratorCustomTitle, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->setChatAdministratorCustomTitle(6814351, 681351, "gjlkmlwg"));
    ASSERT_NO_THROW(bot.get_endpoints()->setChatAdministratorCustomTitle("6814351", 681351, "gjlkmlwg"));
}

TEST(EndpointssetChatAdministratorCustomTitle, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->setChatAdministratorCustomTitle(6814351, 681351, "gjlkmlwg"), std::runtime_error);
    }
}

TEST(EndpointssetChatDescription, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->setChatDescription(716546, "cijopijomfwh"));
    ASSERT_NO_THROW(bot.get_endpoints()->setChatDescription("716546", "cijopijomfwh"));
}

TEST(EndpointssetChatDescription, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->setChatDescription(716546, "cijopijomfwh"), std::runtime_error);
    }
}

TEST(EndpointssetChatMenuButton, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->setChatMenuButton(std::nullopt, std::nullopt));
    ASSERT_NO_THROW(bot.get_endpoints()->setChatMenuButton(2815384, std::make_shared<tgbot::MenuButton>()));
}

TEST(EndpointssetChatMenuButton, errors)
{
    const tgbot::Bot bot("error-domain", 5286, "token", std::nullopt, false);
    ASSERT_THROW(bot.get_endpoints()->setChatMenuButton(std::nullopt, std::nullopt), std::runtime_error);

    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->setChatMenuButton(std::nullopt, std::nullopt), std::runtime_error);
    }
}

TEST(EndpointssetChatPermissions, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(
      bot.get_endpoints()->setChatPermissions(317681351, std::make_shared<tgbot::ChatPermissions>(), false));
    ASSERT_NO_THROW(
      bot.get_endpoints()->setChatPermissions("317681351", std::make_shared<tgbot::ChatPermissions>(), false));
}

TEST(EndpointssetChatPermissions, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->setChatPermissions(317681351, std::make_shared<tgbot::ChatPermissions>(), false),
          std::runtime_error);
    }
}

TEST(EndpointssetChatPhoto, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->setChatPhoto(145145489, test_file));
    ASSERT_NO_THROW(bot.get_endpoints()->setChatPhoto("145145489", test_file));
}

TEST(EndpointssetChatPhoto, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->setChatPhoto(145145489, test_file), std::runtime_error);
    }
}

TEST(EndpointssetChatStickerSet, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->setChatStickerSet(19541, "dpqinckjer"));
    ASSERT_NO_THROW(bot.get_endpoints()->setChatStickerSet("19541", "dpqinckjer"));
}

TEST(EndpointssetChatStickerSet, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->setChatStickerSet(19541, "dpqinckjer"), std::runtime_error);
    }
}

TEST(EndpointssetChatTitle, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->setChatTitle(144982, "lqiojfhjj"));
    ASSERT_NO_THROW(bot.get_endpoints()->setChatTitle("144982", "lqiojfhjj"));
}

TEST(EndpointssetChatTitle, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->setChatTitle(144982, "lqiojfhjj"), std::runtime_error);
    }
}

TEST(EndpointssetCustomEmojiStickerSetThumbnail, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->setCustomEmojiStickerSetThumbnail("upqüpdmwoueh", "hgajüqwrijzsaqüowrj"));
}

TEST(EndpointssetCustomEmojiStickerSetThumbnail, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->setCustomEmojiStickerSetThumbnail("upqüpdmwoueh", "hgajüqwrijzsaqüowrj"),
                     std::runtime_error);
    }
}

TEST(EndpointssetGameScore, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->setGameScore(76156154, 716541, false, true, 716835, 681153, "wpofimjwkölf"));

    const tgbot::Bot bot1("localhost", 5286, "token", "3", false);
    ASSERT_NO_THROW(bot1.get_endpoints()->setGameScore(76156154, 716541, false, true, 716835, 681153, "wpofimjwkölf"));
}

TEST(EndpointssetGameScore, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->setGameScore(76156154, 716541, false, true, 716835, 681153, "wpofimjwkölf"),
                     std::runtime_error);
    }
}

TEST(EndpointssetMyCommands, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->setMyCommands(
      {std::make_shared<tgbot::BotCommand>()}, std::make_shared<tgbot::BotCommandScope>(), "de"));
}

TEST(EndpointssetMyCommands, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->setMyCommands(
            {std::make_shared<tgbot::BotCommand>()}, std::make_shared<tgbot::BotCommandScope>(), "de"),
          std::runtime_error);
    }
}

TEST(EndpointssetMyDefaultAdministratorRights, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->setMyDefaultAdministratorRights(std::nullopt, std::nullopt));
    ASSERT_NO_THROW(
      bot.get_endpoints()->setMyDefaultAdministratorRights(std::make_shared<tgbot::ChatAdministratorRights>(), false));
}

TEST(EndpointssetMyDefaultAdministratorRights, errors)
{
    const tgbot::Bot bot("error-domain", 5286, "token", std::nullopt, false);
    ASSERT_THROW(bot.get_endpoints()->setMyDefaultAdministratorRights(std::nullopt, std::nullopt), std::runtime_error);

    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->setMyDefaultAdministratorRights(std::nullopt, std::nullopt), std::runtime_error);
    }
}

TEST(EndpointssetMyDescription, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->setMyDescription(std::nullopt, std::nullopt));
    ASSERT_NO_THROW(bot.get_endpoints()->setMyDescription("eanjäpewiklancb", "ebjnwpoidjüpwjh"));
}

TEST(EndpointssetMyDescription, errors)
{
    const tgbot::Bot bot("error-domain", 5286, "token", std::nullopt, false);
    ASSERT_THROW(bot.get_endpoints()->setMyDescription(std::nullopt, std::nullopt), std::runtime_error);

    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->setMyDescription(std::nullopt, std::nullopt), std::runtime_error);
    }
}

TEST(EndpointssetMyShortDescription, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->setMyShortDescription(std::nullopt, std::nullopt));
    ASSERT_NO_THROW(bot.get_endpoints()->setMyShortDescription("xpwethjnasdv", "bnaüpcmkhitu"));
}

TEST(EndpointssetMyShortDescription, errors)
{
    const tgbot::Bot bot("error-domain", 5286, "token", std::nullopt, false);
    ASSERT_THROW(bot.get_endpoints()->setMyShortDescription(std::nullopt, std::nullopt), std::runtime_error);

    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->setMyShortDescription(std::nullopt, std::nullopt), std::runtime_error);
    }
}

TEST(EndpointssetPassportDataErrors, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(
      bot.get_endpoints()->setPassportDataErrors(8716345, {std::make_shared<tgbot::PassportElementError>()}));
}

TEST(EndpointssetPassportDataErrors, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->setPassportDataErrors(8716345, {std::make_shared<tgbot::PassportElementError>()}),
          std::runtime_error);
    }
}

TEST(EndpointssetStickerEmojiList, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->setStickerEmojiList("rhjaüosidjh", {"cmjüpowqspdke"}));
}

TEST(EndpointssetStickerEmojiList, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->setStickerEmojiList("rhjaüosidjh", {"cmjüpowqspdke"}), std::runtime_error);
    }
}

TEST(EndpointssetStickerKeywords, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->setStickerKeywords("iuaoipsdtrj", std::vector<std::string> {"rpnaosjhtjhi"}));
}

TEST(EndpointssetStickerKeywords, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->setStickerKeywords("iuaoipsdtrj", std::vector<std::string> {"rpnaosjhtjhi"}),
                     std::runtime_error);
    }
}

TEST(EndpointssetStickerMaskPosition, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(
      bot.get_endpoints()->setStickerMaskPosition("rhaspietalxkyoijer", std::make_shared<tgbot::MaskPosition>()));
}

TEST(EndpointssetStickerMaskPosition, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->setStickerMaskPosition("rhaspietalxkyoijer", std::make_shared<tgbot::MaskPosition>()),
          std::runtime_error);
    }
}

TEST(EndpointssetStickerPositionInSet, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->setStickerPositionInSet("ckoinwfh", 8314));
}

TEST(EndpointssetStickerPositionInSet, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->setStickerPositionInSet("ckoinwfh", 8314), std::runtime_error);
    }
}

TEST(EndpointssetStickerSetThumbnail, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->setStickerSetThumbnail("pqpclöotwhkxcnuher", 413542984, "cppfmwüpithm"));
    ASSERT_NO_THROW(bot.get_endpoints()->setStickerSetThumbnail("pqpclöotwhkxcnuher", 413542984, test_file));
}

TEST(EndpointssetStickerSetThumbnail, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->setStickerSetThumbnail("pqpclöotwhkxcnuher", 413542984, "cppfmwüpithm"),
                     std::runtime_error);
    }
}

TEST(EndpointssetStickerSetTitle, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->setStickerSetTitle("xoepirhsdv", "ziqüsiomwepothj"));
}

TEST(EndpointssetStickerSetTitle, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->setStickerSetTitle("xoepirhsdv", "ziqüsiomwepothj"), std::runtime_error);
    }
}

TEST(EndpointssetWebhook, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->setWebhook(
      "gamgwerg", test_file, "wleofoiwjefgr", 168481, std::vector<std::string> {"flbkshndf"}, false, "hqdgserg"));
}

TEST(EndpointssetWebhook, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->setWebhook(
            "gamgwerg", test_file, "wleofoiwjefgr", 168481, std::vector<std::string> {"flbkshndf"}, false, "hqdgserg"),
          std::runtime_error);
    }
}

TEST(EndpointsstopMessageLiveLocation, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->stopMessageLiveLocation(
      1435, 413541321, "csoijdfhmwth", std::make_shared<tgbot::InlineKeyboardMarkup>()));
    ASSERT_NO_THROW(bot.get_endpoints()->stopMessageLiveLocation(
      "1435", 413541321, "csoijdfhmwth", std::make_shared<tgbot::InlineKeyboardMarkup>()));

    const tgbot::Bot bot1("localhost", 5286, "token", "3", false);
    ASSERT_NO_THROW(bot1.get_endpoints()->stopMessageLiveLocation(
      1435, 413541321, "csoijdfhmwth", std::make_shared<tgbot::InlineKeyboardMarkup>()));
}

TEST(EndpointsstopMessageLiveLocation, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(
          bot.get_endpoints()->stopMessageLiveLocation(
            1435, 413541321, "csoijdfhmwth", std::make_shared<tgbot::InlineKeyboardMarkup>()),
          std::runtime_error);
    }
}

TEST(EndpointsstopPoll, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->stopPoll(7651465, 78915, std::make_shared<tgbot::InlineKeyboardMarkup>()));
    ASSERT_NO_THROW(bot.get_endpoints()->stopPoll("7651465", 78915, std::make_shared<tgbot::InlineKeyboardMarkup>()));
}

TEST(EndpointsstopPoll, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->stopPoll(7651465, 78915, std::make_shared<tgbot::InlineKeyboardMarkup>()),
                     std::runtime_error);
    }
}

TEST(EndpointsunbanChatMember, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->unbanChatMember(16153, 68113, true));
    ASSERT_NO_THROW(bot.get_endpoints()->unbanChatMember("16153", 68113, true));
}

TEST(EndpointsunbanChatMember, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->unbanChatMember(16153, 68113, true), std::runtime_error);
    }
}

TEST(EndpointsunbanChatSenderChat, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->unbanChatSenderChat(914351, 50194));
    ASSERT_NO_THROW(bot.get_endpoints()->unbanChatSenderChat("914351", 50194));
}

TEST(EndpointsunbanChatSenderChat, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->unbanChatSenderChat(914351, 50194), std::runtime_error);
    }
}

TEST(EndpointsunhideGeneralForumTopic, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->unhideGeneralForumTopic(16849));
    ASSERT_NO_THROW(bot.get_endpoints()->unhideGeneralForumTopic("16849"));
}

TEST(EndpointsunhideGeneralForumTopic, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->unhideGeneralForumTopic(16849), std::runtime_error);
    }
}

TEST(EndpointsunpinAllChatMessages, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->unpinAllChatMessages(195413));
    ASSERT_NO_THROW(bot.get_endpoints()->unpinAllChatMessages("195413"));
}

TEST(EndpointsunpinAllChatMessages, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->unpinAllChatMessages(195413), std::runtime_error);
    }
}

TEST(EndpointsunpinAllForumTopicMessages, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->unpinAllForumTopicMessages(2891354, 8615));
    ASSERT_NO_THROW(bot.get_endpoints()->unpinAllForumTopicMessages("2891354", 8615));
}

TEST(EndpointsunpinAllForumTopicMessages, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->unpinAllForumTopicMessages(2891354, 8615), std::runtime_error);
    }
}

TEST(EndpointsunpinChatMessage, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->unpinChatMessage(7811561234, 4186425));
    ASSERT_NO_THROW(bot.get_endpoints()->unpinChatMessage("7811561234", 4186425));
}

TEST(EndpointsunpinChatMessage, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->unpinChatMessage(7811561234, 4186425), std::runtime_error);
    }
}

TEST(EndpointsuploadStickerFile, normal)
{
    const tgbot::Bot bot("localhost", 5286, "token", std::nullopt, false);
    ASSERT_NO_THROW(bot.get_endpoints()->uploadStickerFile(17654, test_file, "xqwpoihsdf"));
}

TEST(EndpointsuploadStickerFile, errors)
{
    for(std::int8_t i = -1; i < 3; ++i)
    {
        const tgbot::Bot bot((i == -1 ? "error-domain" : "localhost"), 5286, "token", std::to_string(i), false);
        ASSERT_THROW(bot.get_endpoints()->uploadStickerFile(17654, test_file, "xqwpoihsdf"), std::runtime_error);
    }
}

TEST(Endpointsgetterandsetter, normal)
{
    tgbot::Endpoints endpoints("localhost", 5286, "token", std::nullopt);

    endpoints.set_certificate_verification(false);
    ASSERT_EQ(endpoints.get_certificate_verification(), false);

    endpoints.set_host("wohfmelmbs");
    ASSERT_EQ(endpoints.get_host(), "wohfmelmbs");

    endpoints.set_port(14764);
    ASSERT_EQ(endpoints.get_port(), 14764);

    endpoints.set_test_value("pmjwfgmfgw");
    ASSERT_EQ(endpoints.get_test_value(), "pmjwfgmfgw");

    endpoints.set_token("cwmjjsfdg");
    ASSERT_EQ(endpoints.get_token(), "cwmjjsfdg");
}
