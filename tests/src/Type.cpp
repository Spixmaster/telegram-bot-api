#include "tgbot/Type.h"

#include <nlohmann/json.hpp>
#include <nlohmann/json_fwd.hpp>
#include <utility>

#include "gtest/gtest.h"

TEST(Type, normal)
{
    tgbot::Type type0;
    tgbot::Type type1;
    const tgbot::Type type2(tgbot::Type);
    const tgbot::Type type3(std::move(type0));
    const tgbot::Type type4 = tgbot::Type();
    const tgbot::Type type5 = std::move(type1);

    const nlohmann::json doc = nlohmann::json::object();

    const std::string serialised_doc = doc.dump();
    ASSERT_EQ(tgbot::Type().serialise(), serialised_doc);
}
