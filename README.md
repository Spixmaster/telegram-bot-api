# telegram-bot-api

A C++ library for the Telegram bot API.\
Current state: Bot API version 6.6.\
See the [Telegram documentation](https://core.telegram.org/bots/api) for changelogs and documentation. I copied the
documentation into the source code according to the Doxygen syntax, but the website is the original source.

## Installation

### Dependencies

- `sudo pacman -S boost` (make)
- `sudo pacman -S clang` (optional)
- `sudo pacman -S cmake` (make)
- `paru -S cpp-httplib` (make)
- `sudo pacman -S doxygen` (optional)
- `sudo pacman -S fmt` (make)
- `sudo pacman -S gcc` (make)
- `sudo pacman -S gettext` (optional)
- `sudo pacman -S git` (optional)
- `sudo pacman -S glibc` (make)
- `sudo pacman -S gtest` (optional)
- `sudo pacman -S lcov` (optional)
- `sudo pacman -S make` (make)
- `sudo pacman -S nlohmann-json` (make)
- `sudo pacman -S openssl` (make)
- `sudo pacman -S zlib` (make)

### Makefile

```sh
mkdir /path/to/project/build/
cd /path/to/project/build/
cmake ..
make
sudo make install
```

## Usage

A C++ library.

### Documentation

It needs to be generated with Doxygen and will then be located in the directory *doc/*.

## Changelog

The changelog can be seen on the documentation's home page.
