#!/usr/bin/env bash
# This script creates the code coverage reports.
echo -e "\033[0;32mThis script has to be executed in the working directory \"build/\".\033[0m"
../build/telegram-bot-api-tests

# Create the code coverage report for the software itself.
cd ../build/CMakeFiles/telegram-bot-api-tests.dir/src/ || exit
gcov Bot.cpp.gcno
lcov -c -d . -o code-coverage.info
genhtml -o ../../../../code-coverage/software/ code-coverage.info

# Create the code coverage report for the tests.
cd ../tests/src/ || exit
gcov main.cpp.gcno
lcov -c -d . -o code-coverage.info
genhtml -o ../../../../../code-coverage/tests/ code-coverage.info
