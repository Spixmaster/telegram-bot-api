#!/usr/bin/env bash
# This script creates the .pot for the messages of the software.
xgettext --add-comments --copyright-holder="Matheus" --keyword=i18n --language=C++ --msgid-bugs-address=matheusgwdl@protonmail.com -o ../../po/messages.pot --package-name=telegram-bot-api --sort-output ../../src/constants/messages.cpp
